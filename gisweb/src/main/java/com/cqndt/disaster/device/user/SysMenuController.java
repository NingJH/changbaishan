package com.cqndt.disaster.device.user;

import com.cqndt.disaster.device.common.util.AbstractController;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.user.service.SysMenuService;
import com.cqndt.disaster.device.vo.TabUserVo;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/2/27  16:07
 * Description:菜单管理
 */
@RestController
@RequestMapping("/sysMenu")
@Api(value = "用户导航菜单", description = "用户导航菜单")
public class SysMenuController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(SysMenuController.class);
    @Autowired
    private SysMenuService sysMenuService;
    /**
     * 导航菜单
     * @return
     */
//    @HandleLog("获取导航菜单")
    @PostMapping("navigationList")
    @ApiOperation(value = "获取导航菜单", notes = "获取导航菜单")
    public Result navigationList() {
        TabUserVo tabUserVo = getUser();
        String userId = tabUserVo.getId().toString();
        List<Map<String,Object>> list = sysMenuService.navigationList(userId,"2");
       // List<Map<String,Object>> list = sysMenuService.navigationList("1","1");
        return new Result().success(list);
    }
}
