package com.cqndt.disaster.device.equip;

import com.cqndt.disaster.device.common.service.TabAttachmentService;
import com.cqndt.disaster.device.common.util.AbstractController;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.equip.service.TabAlarmInfoService;
import com.cqndt.disaster.device.equip.service.TabDeviceService;
import com.cqndt.disaster.device.project.service.TabProjectService;
import com.cqndt.disaster.device.domain.*;
import com.cqndt.disaster.device.common.service.TabStaticTypeService;
import com.cqndt.disaster.device.vo.TabAlarmInfoVo;
import com.cqndt.disaster.device.vo.TabDeviceVo;
import com.cqndt.disaster.device.vo.TabUserVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
//import javafx.scene.control.Tab;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.rmi.MarshalledObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created By marc
 * Date: 2019/4/9  14:53
 * Description:
 */
@RestController
@RequestMapping("/tabDevice")
@Api(value = "设备管理", description = "设备管理")
public class TabDeviceController extends AbstractController {
    @Autowired
    private TabDeviceService tabDeviceService;
    @Autowired
    private TabAttachmentService tabAttachmentService;
    @Autowired
    private TabProjectService tabProjectService;
    @Autowired
    private TabStaticTypeService tabStaticTypeService;
    @Autowired
    private TabAlarmInfoService tabAlarmInfoService;

//    @HandleLog("设备栽点")
    @PostMapping("pointTabDevice")
    @ApiOperation(value = "设备栽点", notes = "设备栽点")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "project", value = "所属项目", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceName", value = "设备名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceType", value = "设备类型", defaultValue = "", required = false, paramType = "query")
    })
    public Result pointTabDevice(TabDeviceVo vo){
        TabUserVo userVo = getUser();
        vo.setUserId(userVo.getId());
        if(null == vo.getLevel()|| vo.getLevel()==""){
            vo.setLevel(userVo.getLevel());
            vo.setAreaCode(userVo.getAreaCode());
        }
        List<TabStaticType> listStatic = tabStaticTypeService.selectByStaticNum1("11");
        if(listStatic.size()>0){
            vo.setStateTime(listStatic.get(0).getStaticKeyval());
        }
        List<TabDeviceVo> list = tabDeviceService.listTabDevice(vo);
        Result result = new Result();
        result.setData(list);
        return result;
    }

//    @HandleLog("设备栽点")
    @PostMapping("listTabDevice")
    @ApiOperation(value = "设备列表", notes = "设备列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "project", value = "所属项目", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceName", value = "设备名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceType", value = "设备类型", defaultValue = "", required = false, paramType = "query")
    })
    public Result listTabDevice(TabDeviceVo vo){
        TabUserVo userVo = getUser();
        vo.setUserId(userVo.getId());
        if(null == vo.getLevel()|| vo.getLevel()==""){
            vo.setLevel(userVo.getLevel());
            vo.setAreaCode(userVo.getAreaCode());
        }
        List<TabStaticType> list = tabStaticTypeService.selectByStaticNum1("11");
        if(list.size()>0){
            vo.setStateTime(list.get(0).getStaticKeyval());
        }
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<TabDeviceVo> pageInfo = new PageInfo<TabDeviceVo>(tabDeviceService.listTabDevice(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
//    @HandleLog("设备基础信息")
    @PostMapping("getTabDevice")
    @ApiOperation(value = "设备基础信息", notes = "设备基础信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "设备id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabDevice(Integer id){
        Result result = new Result();
        TabDeviceVo vo = tabDeviceService.getTabDevice(id);
        result.setData(vo);
        return result;
    }
//    @HandleLog("设备巡查记录")
    @PostMapping("getTabDeviceCheck")
    @ApiOperation(value = "设备巡查记录", notes = "设备巡查记录")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabDevice(String deviceNo){
        Result result = new Result();
        List<TabDeviceCheck> check =  tabDeviceService.selectByDeviceNo(deviceNo);
        result.setData(check);
        return result;
    }
//    @HandleLog("设备安装信息")
    @PostMapping("getTabDeviceInstall")
    @ApiOperation(value = "设备安装信息", notes = "设备安装信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabDeviceInstall(String deviceNo) throws ParseException {
        Result result = new Result();
        TabDeviceInstallWithBLOBs install = tabDeviceService.selectInstallByDeviceNo(deviceNo);
        Map<String, Object> map =  tabDeviceService.selectintervalsTime(deviceNo);
        List<String> list = new ArrayList<>(Collections.singleton(String.valueOf(map.values())));
        String times = list.get(0);
        install.setIntervalsTime("--");
        if (times.length() > 90) {
            String time1 = times.substring(8,27);
            String time2 = times.substring(65, 84);
            System.out.println("time1: " + time1 + "time2: " + time2);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Date d1 = format.parse(time1);
            Date d2 = format.parse(time2);
            long diff = d1.getTime() - d2.getTime();
            if (diff > (1000 * 60 * 60)){
                long hour = diff / (1000 * 60 * 60);
                if(hour > 24){
                    install.setIntervalsTime("--");
                } else {
                    install.setIntervalsTime(hour + "小时" + ((diff / (1000 * 60)) - hour * 60) + "分钟");
                }
            } else {
                install.setIntervalsTime(diff / (1000 * 60) + "分钟");
            }
        }
        result.setData(install);

        return result;
    }
//    @HandleLog("设备图片")
    @PostMapping("getTabDeviceImg")
    @ApiOperation(value = "设备图片", notes = "设备图片")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabDeviceImg(String deviceNo){
        Result result = new Result();
        TabDeviceInstallWithBLOBs install = tabDeviceService.selectInstallByDeviceNo(deviceNo);
        if(null != install){
            List<TabAttachment> list = tabAttachmentService.selectAttachments(install.getImgIds());
            result.setData(list);
        }
        return result;
    }
//    @HandleLog("设备安装图片")
    @PostMapping("getTabDeviceInstallImg")
    @ApiOperation(value = "设备安装图片", notes = "设备安装信息图片")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabDeviceInstallImg(String deviceNo){
        Result result = new Result();
        TabDeviceInstallWithBLOBs install = tabDeviceService.selectInstallByDeviceNo(deviceNo);
        if(null != install){
            List<TabAttachment> list = tabAttachmentService.selectAttachments(install.getInstallImg());
            result.setData(list);
        }
        return result;
    }
//    @HandleLog("获取当前用户所属项目")
    @PostMapping("getTabProjects")
    @ApiOperation(value = "获取当前用户所属项目", notes = "获取当前用户所属项目")
    public Result getTabProjects(){
        Result result = new Result();
        Integer userId = getUser().getId();
        List<Map<String,Object>> list = tabProjectService.selectByUserId(userId);
        result.setData(list);
        return result;
    }
//    @HandleLog("根据监测点id获取相应的告警")
    @PostMapping("getAlarmsByMonitorId")
    @ApiOperation(value = "根据监测点id获取相应的告警", notes = "根据监测点id获取相应的告警")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "monitorId", value = "监测点id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getAlarmsByMonitorId(TabAlarmInfoVo vo){
        TabUserVo userVo = getUser();
        vo.setUserId(userVo.getId());
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<TabAlarmInfoVo> pageInfo = new PageInfo<TabAlarmInfoVo>(tabAlarmInfoService.listTabAlarmInfo(vo));

        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

//    @HandleLog("获取设备类型")
    @PostMapping("getSensorType")
    @ApiOperation(value = "获取设备类型", notes = "获取设备类型")
    public Result getSensorType(){
        return tabDeviceService.getSensorType();
    }

    @PostMapping("getTabProject")
    @ApiOperation(value = "获取当前设备所属项目", notes = "项目信息")
    public Result getTabProject(String deviceNo){
        Result result = new Result();
        Map<String,Object> list = tabProjectService.getTabProject(deviceNo);
        result.setData(list);
        return result;
    }
}



