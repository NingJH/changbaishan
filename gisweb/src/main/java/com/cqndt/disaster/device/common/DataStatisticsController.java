package com.cqndt.disaster.device.common;

import com.cqndt.disaster.device.common.service.DataStatisticsService;
import com.cqndt.disaster.device.common.util.AbstractController;
import com.cqndt.disaster.device.common.util.Result;
//import com.cqndt.disaster.device.domain.TabNPerson;
import com.cqndt.disaster.device.vo.TabUserVo;
//import com.ut.uttools.annos.HandleLog;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/13  9:52
 * Description:数据统计
 */
@RestController
@RequestMapping("data")
@Slf4j
@Api(value = "数据统计", description = "数据统计")
public class DataStatisticsController extends AbstractController {
    @Autowired
    private DataStatisticsService dataStatisticsService;

//    @HandleLog("综合统计(灾害点、项目、安装布置点、设备、传感器)")
    @PostMapping("countBaseNum")
    @ApiOperation(value = "综合统计(灾害点、项目、安装布置点、设备、传感器)", notes = "综合统计(灾害点、项目、安装布置点、设备、传感器)")
    public Result countBaseNum() {
        Result result = new Result();
        TabUserVo userVo = getUser();
        log.info("当前用户信息"+userVo);
        userVo.setAreaCode(userVo.getAreaCode());
        userVo.setLevel(userVo.getLevel());
        userVo.setId(userVo.getId());
        Map<String,Object> map = dataStatisticsService.countBaseNum(userVo);
        result.setData(map);
        return result;
    }

//    @HandleLog("监测方法（设备类型）统计")
    @PostMapping("countMonitorTypeNum")
    @ApiOperation(value = "监测方法（设备类型）统计", notes = "监测方法（设备类型）统计")
    public Result countMonitorTypeNum() {
        Result result = new Result();
        List<Map<String,Object>> list = dataStatisticsService.countMonitorTypeNum(getUserId());
        result.setData(list);
        return result;
    }

//    @HandleLog("害点类型统计")
    @PostMapping("countBasicTypeNum")
    @ApiOperation(value = "害点类型统计", notes = "害点类型统计")
    public Result countBasicTypeNum() {
        Result result = new Result();
        TabUserVo userVo = getUser();
        userVo.setAreaCode(userVo.getAreaCode());
        userVo.setLevel(userVo.getLevel());
        userVo.setId(userVo.getId());
        List<Map<String,Object>> list = dataStatisticsService.countBasicTypeNum(userVo);
        result.setData(list);
        return result;
    }

//    @HandleLog("状态统计（1日无数据、2日无数据、3日无数据、4日及以上无数据、正常）")
    @PostMapping("countInstrumentNum")
    @ApiOperation(value = "状态统计（1日无数据、2日无数据、3日无数据、4日及以上无数据、正常）", notes = "状态统计（1日无数据、2日无数据、3日无数据、4日及以上无数据、正常）")
    public Result countInstrumentNum() {
        Result result = new Result();
        Map<String,Object> map = dataStatisticsService.countInstrumentNum(getUserId());
        result.setData(map);
        return result;
    }

//    @HandleLog("设备在线离线统计")
    @PostMapping("countDeviceStateNum")
    @ApiOperation(value = "设备在线离线统计", notes = "设备在线离线统计")
    public Result countDeviceStateNum() {
        Result result = new Result();
        List<Map<String,Object>> list = dataStatisticsService.countDeviceStateNum(getUserId());
        result.setData(list);
        return result;
    }

//    @HandleLog("设备累计告警统计")
    @PostMapping("countDeviceAlarmNum")
    @ApiOperation(value = "设备累计告警统计", notes = "设备累计告警统计")
    public Result countDeviceAlarmNum() {
        Result result = new Result();
        List<Map<String,Object>> list = dataStatisticsService.countDeviceAlarmNum(81);
        if(!list.isEmpty()){
            for(Map<String,Object> map : list){
                map.put("level_name",map.get("level_name")==null?"0":map.get("level_name"));
                map.put("level",map.get("level")==null?"0":map.get("level"));
                map.put("num",map.get("num")==null?"0":map.get("num"));
            }
        }else {
            String levelName = "红";
            for(int i=1;i<=4;i++){
                Map<String,Object> map = new HashMap<>();
                map.put("level_name",levelName);
                map.put("level",i);
                map.put("num","0");
                list.add(map);
                switch (i){
                    case 2:
                        levelName = "橙";
                        break;
                    case 3:
                        levelName = "黄";
                        break;
                    case 4:
                        levelName = "蓝";
                        break;
                }
            }
        }
        result.setData(list);
        return result;
    }

//    @HandleLog("新版在线离线统计")
    @PostMapping("countAllDeviceStateOnline")
    @ApiOperation(value = "新版在线离线统计", notes = "新版在线离线统计")
    public  Result countAllDeviceStateOnline(){
        Result result = new Result();
        Map<String,Object> list = dataStatisticsService.countAllDeviceStateOnline(getUserId());
        result.setData(list);
        return  result;
    }


//    @HandleLog("测试接口")
    @PostMapping("countDeviceStateNum1")
    @ApiOperation(value = "测试接口", notes = "测试接口")
    public Result countDeviceStateNum1(Integer page, Integer size) {
        Result result = new Result();
        Result list = dataStatisticsService.countDeviceStateNum1(getUserId(), page, size);
        result.setData(list);
        return result;
    }

    @GetMapping("equipmentCondition")
    @ApiOperation(value = "设备工况", notes = "设备工况")
    public Result equipmentCondition() {
        Result result = new Result();
        List<Map<String,Object>> list = dataStatisticsService.equipmentCondition(getUserId());
        result.setData(list);
        return result;
    }

    @GetMapping("statistics")
    @ApiOperation(value = "数据统计", notes = "数据统计")
    public Result statistics() {
        Result result = new Result();
        Map<String,Object> list = dataStatisticsService.statistics(getUserId());
        result.setData(list);
        return result;
    }

    @PostMapping("warnMsgList")
    @ApiOperation(value = "告警信息列表", notes = "告警信息列表")
    public Result warnMsgList(int page,int limit,String startTime, String endTime) {
        Result result = new Result();
        if(page==-1){
            result.setData(dataStatisticsService.warnMsgList(startTime,endTime,getUserId()));
            return result;
        }
        PageHelper.startPage(page, limit);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(dataStatisticsService.warnMsgList(startTime,endTime,getUserId()));
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    @PostMapping("warnMsgInfo")
    @ApiOperation(value = "单个告警信息列表", notes = "单个告警信息列表")
    public Result warnMsgInfo(int page,int limit,String deviceNo) {
        PageHelper.startPage(page, limit);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(dataStatisticsService.warnMsgInfo(deviceNo));
        Result result = new Result();
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    @GetMapping("warnMsgDetails")
    @ApiOperation(value = "告警详情", notes = "告警详情")
    public Result warnMsgDetails(int id) {
        Result result = new Result();
        Map<String,Object> list = dataStatisticsService.warnMsgDetails(id);
        result.setData(list);
        return result;
    }

    @GetMapping("warnMsgDevice")
    @ApiOperation(value = "告警关联设备", notes = "告警关联设备")
    public Result warnMsgDevice(String deviceNo) {
        Result result = new Result();
        Map<String,Object> list = dataStatisticsService.warnMsgDevice(deviceNo);
        result.setData(list);
        return result;
    }

    @GetMapping("warnMsgListCount")
    @ApiOperation(value = "告警信息数量", notes = "告警信息数量")
    public Result warnMsgListCount() {
        Result result = new Result();
        Map<String,Object> list = dataStatisticsService.warnMsgListCount();
        result.setData(list);
        return result;
    }

    @GetMapping("meteorological")
    @ApiOperation(value = "气象预警", notes = "气象预警")
    public Result meteorological() {
        Result result = new Result();
        Map<String,Object> list = dataStatisticsService.meteorological();
        result.setData(list);
        return result;
    }

    @PostMapping("getNPerson")
    @ApiOperation(value = "人员管理", notes = "人员管理")
    public Result getNPerson(int page,int limit,int pType,String name,String wPhone) {
        Result result = new Result();
        PageHelper.startPage(page, limit);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(dataStatisticsService.getNPerson(pType,name,wPhone));
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    @PostMapping("getPointNPerson")
    @ApiOperation(value = "人员栽点管理", notes = "人员栽点管理")
    public Result getPointNPerson(int pType,String name,String wPhone) {
        Result result = new Result();
        List<Map<String,Object>> list = dataStatisticsService.getPointNPerson(pType,name,wPhone);
        result.setData(list);
        return result;
    }

    @PostMapping("getNPersonInfo")
    @ApiOperation(value = "人员详情", notes = "人员详情")
    public Result getNPersonInfo(int id) {
        Result result = new Result();
        Map<String, Object> list = dataStatisticsService.getNPersonInfo(id);
        result.setData(list);
        return result;
    }

    @PostMapping("getBasicNperson")
    @ApiOperation(value = "灾害点下面人员信息", notes = "灾害点下面人员信息")
    public Result getBasicNperson(String disNo) {
        Result result = new Result();
        List<Map<String, Object>> list = dataStatisticsService.getBasicNperson(disNo);
        result.setData(list);
        return result;
    }
}
