package com.cqndt.disaster.device.sms;

import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.sms.service.LonAndLatService;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @User : ningjunhao
 * @Mail : 15823526611@139.com
 * @Time : 2019-08-05 09:56
 **/
@RestController
@RequestMapping("/sms")
@Api(value = "短信", description = "短信")
public class LonAndLatController {

    @Autowired
    LonAndLatService lonAndLatService;


//    @HandleLog("框选经纬度")
    @PostMapping("fsLonAndLat")
    @ApiOperation(value = "框选经纬度", notes = "框选经纬度")
    public Result fsLonAndLat(int userId,String[] lonLat) {
        Result result = new Result();
        List<Map<String,Object>> data = lonAndLatService.fsLonAndLat(userId,lonLat);
        result.setData(data);
        return result;
    }

//    @HandleLog("通过设备编号找到检测人")
    @PostMapping("findPersion")
    @ApiOperation(value = "通过设备编号找到检测人", notes = "通过设备编号找到检测人")
    public Result findPersion(String deviceId) {
        Result result = new Result();
        List<Map<String,Object>> data = lonAndLatService.findPersion(deviceId);
        result.setData(data);
        return result;
    }

//    @HandleLog("发送短信给检测人")
    @PostMapping("sendSms")
    @ApiOperation(value = "发送短信给检测人", notes = "发送短信给检测人")
    public Result sendSms(String mobile,String content,int userId) {
        Result result = new Result();
        boolean data = lonAndLatService.sendSms(mobile,content,userId);
        result.setData(data);
        return result;
    }

}
