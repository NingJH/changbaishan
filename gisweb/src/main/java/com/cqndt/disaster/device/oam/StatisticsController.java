package com.cqndt.disaster.device.oam;

import com.cqndt.disaster.device.common.util.AbstractController;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.equip.service.TabDeviceService;
//import com.ut.uttools.annos.HandleLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Frog
 */
@Slf4j
@RestController
@RequestMapping(value = "count")
public class StatisticsController extends AbstractController {

    @Autowired
    private TabDeviceService tabDeviceService;

    /**
     * 设备离在线
     *
     * @return
     */
//    @HandleLog("设备离在线")
    @GetMapping(value = "getAllDeviceOnline")
    public Result getAllDeviceOnline() {
        return tabDeviceService.getAllDeviceOnline(getUserId());
    }

    /**
     * 设备离在线 按厂商分类
     *
     * @return
     */
//    @HandleLog("设备离在线 按厂商分类")
    @PostMapping(value = "getDeviceOnlineByCompany")
    public Result getDeviceOnlineByCompany(String ids) {
        return tabDeviceService.getDeviceOnlineByCompany(ids, getUserId());
    }

    /**
     * 获取当前账号下设备厂商
     *
     * @return
     */
//    @HandleLog("获取当前账号下设备厂商")
    @GetMapping(value = "getCompanys")
    public Result getCompanys() {
        return tabDeviceService.getCompanys(getUserId());
    }

    /**
     * 设备离在线 按项目分类
     *
     * @return
     */
//    @HandleLog("设备离在线 按项目分类")
    @PostMapping(value = "getDeviceOnlineByProject")
    public Result getDeviceOnlineByProject(String ids) {
        return tabDeviceService.getDeviceOnlineByProject(ids,getUserId());
    }

    /**
     * 设备离在线 按设备类型分类
     *
     * @return
     */
//    @HandleLog("设备离在线 按设备类型分类")
    @GetMapping(value = "getDeviceOnlineByType")
    public Result getDeviceOnlineByType(){
        return tabDeviceService.getDeviceOnlineByType(getUserId());

    }
}
