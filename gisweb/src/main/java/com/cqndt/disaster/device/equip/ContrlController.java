package com.cqndt.disaster.device.equip;

import com.cqndt.disaster.device.common.exception.ContrlException;
import com.cqndt.disaster.device.equip.service.TabOrderLogService;
import com.cqndt.disaster.device.common.shrio.ShiroUtils;
import com.cqndt.disaster.device.common.util.Constant;
import com.cqndt.disaster.device.common.util.ContrlUtil;
import com.cqndt.disaster.device.common.util.NumberChangeUtil;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.equip.service.ContrlService;
import com.cqndt.disaster.device.record.SysLog;
import com.cqndt.disaster.device.vo.TabOrderLogVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 智能报警器指令
 */
@RestController
@RequestMapping("/contrl")
@Api(value = "智能报警器指令", description = "智能报警器指令")
public class ContrlController {
    @Autowired
    private TabOrderLogService tabOrderLogService;

    @Autowired
    private ContrlService contrlService;

//    @HandleLog("发送智能报警指令")
    @SysLog("发送智能报警指令")
    @PostMapping("order")
    @ApiOperation(value = "智能报警器指令", notes = "智能报警器指令")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceId", value = "平台设备id", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "sn", value = "设备sn", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "voiceType", value = "声音类型", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "voiceTypeName", value = "声音类型名称", defaultValue = "1", required = true, paramType = "query")
    })
    public Result order(String deviceId,String sn,Integer voiceType,String voiceTypeName) {
        ShiroUtils.setSessionAttribute(Constant.DEVICE_ID,deviceId);
        ShiroUtils.setSessionAttribute(Constant.SN,sn);
        ShiroUtils.setSessionAttribute(Constant.VOICE_TYPE_NAME,voiceTypeName);
        ShiroUtils.setSessionAttribute(Constant.VOICE_TYPE,voiceType);
        //申请变量
        try {
            String setShoudLight =  ContrlUtil.setAlarm(sn,voiceType);//声光告警
            //发送请求onenet
            contrlService.postCommand(deviceId, Base64.encodeBase64String(NumberChangeUtil.stringToByte(setShoudLight)));
        } catch (ContrlException e) {
            return new Result().failure(-1,deviceId + e.getMessage());
        }
        //响应
        return new Result().success("发送成功");
    }
//    @HandleLog("已发送指令列表")
    @PostMapping("listOrderLog")
    @ApiOperation(value = "已发送指令列表", notes = "已发送指令列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceId", value = "平台设备id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result listOrderLog(TabOrderLogVo vo) {
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<TabOrderLogVo> pageInfo = new PageInfo<TabOrderLogVo>(tabOrderLogService.listOrderLog(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
}
