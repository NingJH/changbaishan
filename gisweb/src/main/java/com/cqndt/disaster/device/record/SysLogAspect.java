package com.cqndt.disaster.device.record;

import com.alibaba.fastjson.JSON;
import com.cqndt.disaster.device.equip.service.TabOrderLogService;
import com.cqndt.disaster.device.common.shrio.ShiroUtils;
import com.cqndt.disaster.device.common.util.Constant;
import com.cqndt.disaster.device.common.util.HttpContextUtils;
import com.cqndt.disaster.device.common.util.IPUtils;
import com.cqndt.disaster.device.domain.TabOrderLog;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * 智能报警，切面处理类
 */
@Aspect
@Component
public class SysLogAspect {
	@Autowired
	private TabOrderLogService tabOrderLogService;
	
	@Pointcut("@annotation(com.cqndt.disaster.device.record.SysLog)")
	public void logPointCut() { 
		
	}

	@Around("logPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		long beginTime = System.currentTimeMillis();
		//执行方法
		Object result = point.proceed();
		//执行时长(毫秒)
		long time = System.currentTimeMillis() - beginTime;

		//保存日志
		saveSysLog(point, time);

		return result;
	}

	private void saveSysLog(ProceedingJoinPoint joinPoint, long time) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();

		TabOrderLog tabOrderLog = new TabOrderLog();
		SysLog syslog = method.getAnnotation(SysLog.class);
		if(syslog != null){
			//注解上的描述
			tabOrderLog.setOperation(syslog.value());
		}

		//请求的方法名
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = signature.getName();
		tabOrderLog.setMethod(className + "." + methodName + "()");

		//请求的参数
		Object[] args = joinPoint.getArgs();
		try{
			String params = JSON.toJSONString(args[0]);
			tabOrderLog.setParams(params);
		}catch (Exception e){

		}
		//获取request
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		//设置IP地址
		tabOrderLog.setIp(IPUtils.getIpAddr(request));
		//获取当前登录用户名
		String username = ShiroUtils.getUserEntity().getUserName();
		tabOrderLog.setUsername(username);

		String deviceId = (String)ShiroUtils.getSession().getAttribute(Constant.DEVICE_ID);
		String sn = (String)ShiroUtils.getSession().getAttribute(Constant.SN);
		String voiceTypeName = (String)ShiroUtils.getSession().getAttribute(Constant.VOICE_TYPE_NAME);
		Integer voiceType = (Integer)ShiroUtils.getSession().getAttribute(Constant.VOICE_TYPE);
		tabOrderLog.setDeviceId(deviceId);
		tabOrderLog.setSn(sn);
		tabOrderLog.setVoiceType(voiceType);
		tabOrderLog.setPlatType(0);
		tabOrderLog.setVoiceTypeName(voiceTypeName);

		tabOrderLog.setTime(time);
		tabOrderLog.setCreateDate(new Date());
		//保存系统日志
		tabOrderLogService.save(tabOrderLog);
	}
}
