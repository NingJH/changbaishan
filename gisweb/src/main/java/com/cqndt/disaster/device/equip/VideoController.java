/**
 * 
 */
package com.cqndt.disaster.device.equip;

import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.equip.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * 视频监控云台控制
 * @author lhy
 * @date 2017年10月26日
 *
 */
@Controller
@RequestMapping("/video")
public class VideoController {

	@Autowired
	VideoService videoService;
	
	/**
	 * 操作云台
	 * @param deviceId 设备id
	 * @param direction 操作命令：0-上，1-下，2-左，3-右，4-左上，5-左下，6-右上，7-右下，8-放大，9-缩小，10-近焦距，11-远焦距
	 * @param speed 云台速度：0-慢，1-适中，2-快
	 * @return
	 */
	@RequestMapping(value = "/startPtz/{deviceId}/{deviceSerial}/{channelNo}/{direction}/{speed}", method = RequestMethod.GET)
	public @ResponseBody
	Result startPtz(@PathVariable int deviceId, @PathVariable String deviceSerial, @PathVariable int channelNo, @PathVariable int direction, @PathVariable int speed){
		String result  = null;
		try {
			result = videoService.startPtz(deviceId, deviceSerial, channelNo, direction, speed);
		} catch (IOException e) {
			result = "{\"code\": \"200\",\"msg\": \"操作失败:"+e.getLocalizedMessage()+"!\"}";
			e.printStackTrace();
		}
		return new Result().success(result);
	}
	
	
	/**
	 * 关闭云台操作
	 * @param deviceSerial 设备序列号
	 * @param channelNo 设备通道
	 * @return
	 */
	@RequestMapping(value = "/stopPtz/{deviceSerial}/{channelNo}/{direction}", method = RequestMethod.POST )
	public @ResponseBody
	Result stopPtz(@PathVariable String deviceSerial, @PathVariable int channelNo, @PathVariable int direction){
		
		String result  = null;
		try {
			result = videoService.stopPtz(deviceSerial, channelNo, direction);
		} catch (IOException e) {
			result = "{\"code\": \"200\",\"msg\": \"操作失败:"+e.getLocalizedMessage()+"!\"}";
			e.printStackTrace();
		}
		return new Result().success(result);
	}
	
	
	@RequestMapping(value = "/capture/{deviceId}/{deviceSerial}/{channelNo}", method = RequestMethod.POST )
	public @ResponseBody
	Result capture(@PathVariable int deviceId, @PathVariable String deviceSerial, @PathVariable int channelNo){
		
		String result  = null;
		try {
			result = videoService.capture(deviceId, deviceSerial, channelNo);
		} catch (IOException e) {
			result = "{\"code\": \"1000\",\"msg\": \"操作失败:"+e.getLocalizedMessage()+"!\"}";
			e.printStackTrace();
		}
		return new Result().success(result);
	}
}
