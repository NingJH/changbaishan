package com.cqndt.disaster.device.basic;

import com.cqndt.disaster.device.common.service.SearchService;
import com.cqndt.disaster.device.common.service.TabAttachmentService;
import com.cqndt.disaster.device.basic.service.TabBasicService;
import com.cqndt.disaster.device.common.util.AbstractController;
import com.cqndt.disaster.device.common.util.JsonArrayToList;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.domain.*;
import com.cqndt.disaster.device.vo.TabBasicVo;
import com.cqndt.disaster.device.vo.TabHumanVo;
import com.cqndt.disaster.device.vo.TabUserVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description:灾害点管理
 */
@RestController
@RequestMapping("/tabBasic")
@Api(value = "灾害点管理", description = "灾害点管理")
public class TabBasicController extends AbstractController {
    @Autowired
    private TabBasicService tabBasicService;
    @Autowired
    private TabAttachmentService tabAttachmentService;
    @Autowired
    private SearchService searchService;

//    @HandleLog("地图栽点")
    @PostMapping("pointTabBasic")
    @ApiOperation(value = "地图栽点", notes = "地图栽点")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "disType", value = "灾害点类型", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "sqlStr", value = "json条件", defaultValue = "[]", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "", required = true, paramType = "query")
    })
    public Result pointTabBasic(String disType, String sqlStr,String areaCode,String level){
        TabBasicVo vo = new TabBasicVo();
        TabUserVo userVo = getUser();
        vo.setLevel(null == level || level ==""? userVo.getLevel():level);
        vo.setAreaCode(null == level || level ==""? userVo.getAreaCode():areaCode);
        vo.setDisType(disType);
        vo.setSqlStr(JsonArrayToList.parseJSON2List(sqlStr));
        Result result = new Result();
        List<TabBasicVo> list = tabBasicService.selectTabBasic(vo);
        result.setData(list);
        return result;
    }
//    @HandleLog("灾害点列表")
    @PostMapping("listTabBasic")
    @ApiOperation(value = "灾害点列表", notes = "灾害点列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "disType", value = "灾害点类型", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "sqlStr", value = "json条件", defaultValue = "[]", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "", required = true, paramType = "query")
    })
    public Result listTabBasic(String disType, String sqlStr, Integer page, Integer limit, String areaCode, String level){
        TabBasicVo vo = new TabBasicVo();
        TabUserVo userVo = getUser();
        vo.setLevel(null == level || level ==""? userVo.getLevel():level);
        vo.setAreaCode(null == level || level ==""? userVo.getAreaCode():areaCode);
        vo.setDisType(disType);
        vo.setSqlStr(JsonArrayToList.parseJSON2List(sqlStr));
        PageHelper.startPage(page,limit);
        PageInfo<TabBasicVo> pageInfo = new PageInfo<TabBasicVo>(tabBasicService.selectTabBasic(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
//    @HandleLog("灾害点基础信息")
    @PostMapping("getTabBasic")
    @ApiOperation(value = "灾害点基础信息", notes = "灾害点基础信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "灾害点id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabBasic(Integer id){
        Result result = new Result();
        TabBasicVo vo = tabBasicService.selectTabBasicById(id);
        result.setData(vo);
        return result;
    }

//    @HandleLog("根据灾害点id获取相应的视频、图片")
    @PostMapping("getAttachmentForBasic")
    @ApiOperation(value = "根据灾害点id获取相应的视频、图片", notes = "根据灾害点id获取相应的视频、图片")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "灾害点id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getVideoForBasic(Integer id){
        Result result = new Result();
        TabBasicWithBLOBs tabBasicWithBLOBs = tabBasicService.selectById(id);
        List<TabAttachment> imgs = tabAttachmentService.selectAttachments(tabBasicWithBLOBs.getImgIds());
        List<TabAttachment> videos = tabAttachmentService.selectAttachments(tabBasicWithBLOBs.getVideoIds());
        Map<String,Object> map = new HashMap<>();
        map.put("imgs",imgs);
        map.put("videos",videos);
        result.setData(map);
        return result;
    }
//    @HandleLog("根据灾害点编号获取两卡一案(防灾明白卡、避险明白卡、防灾预案)")
    @PostMapping("getCardsForBasic")
    @ApiOperation(value = "根据灾害点编号获取两卡一案(防灾明白卡、避险明白卡、防灾预案)", notes = "根据灾害点编号获取两卡一案(防灾明白卡、避险明白卡、防灾预案)")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "disNo", value = "灾害点编号", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getCardsForBasic(String disNo){
        Result result = new Result();
        Map<String,Object> map = tabBasicService.selectCards(disNo);
        result.setData(map);
        return result;
    }

//    @HandleLog("获取人员信息")
    @PostMapping(value = "getHumanByDisNo")
    @ApiOperation(value = "获取人员信息",notes = "获取人员信息")
    @ApiImplicitParam(dataType = "String", name = "disNo", value = "灾害点编号", defaultValue = "", required = true, paramType = "query")
    public Result getHumanByDisNo(String disNo){
        Result result = new Result();
        List<Map<String, Object>> humanList = tabBasicService.selectHumanByDisNo(disNo);
        result.setData(humanList);
        return result;
    }

//    @HandleLog("条件信息")
    @PostMapping("listColumns")
    @ApiOperation(value = "条件信息", notes = "条件信息")
    @ApiImplicitParam(dataType = "String", name = "tableName", value = "表名", defaultValue = "tab_basic", required = true, paramType = "query")
    public Result listColumns(String tableName) {
        if (null == tableName || tableName == "") {
            tableName = "tab_basic";
        }
        return new Result().success(searchService.getColumns(tableName));
    }

//    @HandleLog("根据灾害点编号查询设备")
    @PostMapping("getDeviceByDisNo")
    @ApiOperation(value = "根据灾害点编号查询设备", notes = "根据灾害点编号查询设备")
    @ApiImplicitParam(dataType = "String", name = "disNo", value = "灾害点编号", defaultValue = "", required = true, paramType = "query")
    public Result getDeviceByDisNo(String disNo) {
        return new Result().success(tabBasicService.getDeviceByDisNo(disNo));
    }
}
