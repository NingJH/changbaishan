package com.cqndt.disaster.device.common;

import com.cqndt.disaster.device.basic.service.TabBasicScopeService;
import com.cqndt.disaster.device.common.service.TabAreaService;
import com.cqndt.disaster.device.common.service.TabPhotographyService;
import com.cqndt.disaster.device.common.service.TabStaticTypeService;
import com.cqndt.disaster.device.common.service.TabTwoRoundService;
import com.cqndt.disaster.device.common.util.AbstractController;
//import com.cqndt.disaster.device.common.util.FileView;
import com.cqndt.disaster.device.common.util.FileView;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.domain.*;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/12  9:20
 * Description:获取基础数据
 */
@RestController
@RequestMapping("/comm")
@Api(value = "公用信息", description = "公用信息")
public class CommController extends AbstractController {
    @Autowired
    private TabStaticTypeService tabStaticTypeService;
    @Autowired
    private TabAreaService tabAreaService;
    @Autowired
    private TabTwoRoundService tabTwoRoundService;
    @Autowired
    private TabPhotographyService tabPhotographyService;
    @Autowired
    private TabBasicScopeService tabBasicScopeService;
    @Value("${attachmentUrlSet}")
    private String attachmentUrlSet;
    @Value("${open-office-path}")
    private String openOfficePath;
    @Value("${open-office-host}")
    private String openOfficeHost;

//    @HandleLog("获取静态值")
    @PostMapping("getTabStatic")
    @ApiOperation(value = "获取静态值", notes = "获取静态值")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "staticNum", value = "静态值类型值", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabDevice(String staticNum){
        Result result = new Result();
        List<TabStaticType> list = tabStaticTypeService.selectByStaticNum(staticNum);
        result.setData(list);
        return result;
    }
//    @HandleLog("获取区域")
    @PostMapping("getTabArea")
    @ApiOperation(value = "获取区域", notes = "获取区域")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabArea(String areaCode){
        Result result = new Result();
        List<TabArea> list = tabAreaService.selectByAreaCode(areaCode);
        result.setData(list);
        return result;
    }
//    @HandleLog("根据编号(项目编号、灾害点编号)获取倾斜摄影")
    @PostMapping("getTabPhotography")
    @ApiOperation(value = "根据编号(项目编号、灾害点编号)获取倾斜摄影", notes = "根据编号(项目编号、灾害点编号)获取倾斜摄影")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "relationNo", value = "编号", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "modelType", value = "查询项目或灾害点的倾斜摄影(1灾害点 2项目)", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabPhotography(String relationNo,String modelType){
        Result result = new Result();
        List<TabPhotography> list = tabPhotographyService.selectByRelationNo(relationNo,modelType);
        result.setData(list);
        return result;
    }
//    @HandleLog("根据倾斜摄影id获取影响范围等")
    @PostMapping("getTabBasicScope")
    @ApiOperation(value = "根据倾斜摄影id获取影响范围等", notes = "根据倾斜摄影id获取影响范围等")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "photoId", value = "编号", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabBasicScope(String photoId){
        Result result = new Result();
        List<TabBasicScope> list = tabBasicScopeService.selectByPhotoId(photoId);
        result.setData(list);
        return result;
    }

//    @HandleLog("根据编号(项目编号、灾害点编号、设备编号)查询全景图")
    @PostMapping("/getTabTwoRound")
    @ApiOperation(value = "根据编号(项目编号、灾害点编号、设备编号)查询全景图", notes = "根据编号(项目编号、灾害点编号、设备编号)查询全景图")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "relationNo", value = "项目编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "modelType", value = "查询项目、灾害点或设备的全景图(1灾害点、 2项目、 3设备)", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabTwoRound(String relationNo,String modelType){
        Result result = new Result();
        List<TabTwoRound> list = tabTwoRoundService.selectByRelationNo(relationNo,modelType);
        result.setData(list);
        return result;
    }
    /**
     * 预览文件
     * @param attachmentUrl 文件路径
     * @param attachmentType 文件类型
     * @param response
     */
//    @HandleLog("预览文件")
    @RequestMapping(value = "readByIo", method = RequestMethod.GET)
    public void readByIo(String attachmentUrl,String attachmentType,HttpServletResponse response) {
        try {
            FileView.readByIo(attachmentUrl, attachmentType, response,attachmentUrlSet,openOfficePath,openOfficeHost,0);
        }catch (Exception e){
            logger.error("文件预览失败");
            e.printStackTrace();
        }
    }
}
