package com.cqndt.disaster.device.equip;

import com.cqndt.disaster.device.common.util.AbstractController;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.domain.TabAttachment;
import com.cqndt.disaster.device.common.service.TabAttachmentService;
import com.cqndt.disaster.device.equip.service.TabVideoMonitoringService;
import com.cqndt.disaster.device.vo.TabUserVo;
import com.cqndt.disaster.device.vo.TabVideoMonitoringVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/17  17:05
 * Description: 视频监控设备
 */
@RestController
@RequestMapping("/tabVideo")
@Api(value = "视频监控设备管理", description = "视频监控设备管理")
public class TabVideoDeviceController extends AbstractController {
    @Autowired
    private TabVideoMonitoringService tabVideoMonitoringService;
    @Autowired
    private TabAttachmentService tabAttachmentService;

//    @HandleLog("视频监控设备栽点")
    @PostMapping("pointTabVideo")
    @ApiOperation(value = "视频监控设备栽点", notes = "视频监控设备栽点")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "所属项目", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceName", value = "设备名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "staticKey", value = "设备厂商", defaultValue = "", required = false, paramType = "query")
    })
    public Result pointTabVideo(TabVideoMonitoringVo vo){
        TabUserVo userVo = getUser();
        vo.setUserId(userVo.getId());
        if(null == vo.getLevel()|| vo.getLevel()==""){
            vo.setLevel(userVo.getLevel());
            vo.setAreaCode(userVo.getAreaCode());
        }
        List<TabVideoMonitoringVo> list = tabVideoMonitoringService.getListByCondition(vo);
        Result result = new Result();
        result.setData(list);
        return result;
    }

//    @HandleLog("设备列表")
    @PostMapping("listTabVideoMonitoring")
    @ApiOperation(value = "设备列表", notes = "设备列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "所属项目", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceName", value = "设备名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "staticKey", value = "设备厂商（静态值staticNum=12）", defaultValue = "", required = false, paramType = "query")
    })
    public Result listTabVideoMonitoring(TabVideoMonitoringVo vo){
        TabUserVo userVo = getUser();
        vo.setUserId(userVo.getId());
        if(null == vo.getLevel()|| vo.getLevel()==""){
            vo.setLevel(userVo.getLevel());
            vo.setAreaCode(userVo.getAreaCode());
        }
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<TabVideoMonitoringVo> pageInfo = new PageInfo<TabVideoMonitoringVo>(tabVideoMonitoringService.getListByCondition(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
//    @HandleLog("设备基础信息、安装图片、视频图片")
    @PostMapping("getTabVideoMonitoring")
    @ApiOperation(value = "设备基础信息、安装图片、视频图片", notes = "设备基础信息、安装图片、视频图片")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "视频监控id", defaultValue = "", required = true, paramType = "query")
    })
    public Result getTabVideoMonitoring(Integer id){
        Result result = new Result();
        TabVideoMonitoringVo vo = tabVideoMonitoringService.getVideoById(id);
        List<TabAttachment> installImg = tabAttachmentService.selectAttachments(null == vo.getInstallPic()?null:vo.getInstallPic());
        List<TabAttachment> videoImg = tabAttachmentService.selectAttachments(null == vo.getResultPic()?null:vo.getResultPic());
        Map<String,Object> map = new HashMap<>();
        map.put("vo",vo);
        map.put("installImg",installImg);
        map.put("videoImg",videoImg);
        result.setData(map);
        return result;
    }

    @PostMapping("listMonitorInformation")
    @ApiOperation(value = "单个视频监控信息", notes = "单个视频监控信息")
    @ApiImplicitParam(dataType = "Integer", name = "deviceId", value = "设备ID", defaultValue = "1", required = true, paramType = "query")
    public Result listMonitorInformation(Integer deviceId) {
        return new Result().success(tabVideoMonitoringService.listMonitorInformation(deviceId));
    }
}
