package com.cqndt.disaster.device.oam;

import com.cqndt.disaster.device.common.util.AbstractController;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.deviceExp.service.TabDeviceExceptionService;
import com.cqndt.disaster.device.vo.TabDeviceExceptionVo;
import com.cqndt.disaster.device.vo.TabProjectVo;
import com.cqndt.disaster.device.vo.TabUserVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created By marc
 * Date: 2019/6/7  16:18
 * Description: 异常设备管理
 */
@RestController
@RequestMapping("/tabDeviceException")
@Api(value = "异常设备管理", description = "异常设备管理")
public class TabDeviceExceptionController extends AbstractController {
    @Autowired
    private TabDeviceExceptionService tabDeviceExceptionService;
    @Value("${attachmentUrlSet}")
    private String attachmentUrlSet;
    @Value("${file-ip}")
    private String fileIp;

//    @HandleLog("设备异常列表")
    @PostMapping("listTabDeviceException")
    @ApiOperation(value = "设备异常列表", notes = "设备异常列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "exceptionType", value = "设备异常类型(1设备离线 2数据异常)", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceName", value = "设备名称", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceType", value = "设备类型", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectId", value = "项目id", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceFactory", value = "设备厂商", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query")
    })
    public Result listTabDeviceException(TabDeviceExceptionVo vo){
        vo.setUserId(getUserId().toString());
        //vo.setUserId("6");
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<TabDeviceExceptionVo> pageInfo = new PageInfo<TabDeviceExceptionVo>(tabDeviceExceptionService.getListByCondition(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
//    @HandleLog("忽略操作")
    @PostMapping("ignoreException")
    @ApiOperation(value = "忽略操作", notes = "忽略操作")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "operatePersonName", value = "处置人员", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "dealBasic", value = "处置依据", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号(多条用逗号隔开)", defaultValue = "1", required = false, paramType = "query")
    })
    public Result ignoreException(TabDeviceExceptionVo vo){
        tabDeviceExceptionService.ignoreException(vo);
        return new Result().success("操作成功");
    }

//    @HandleLog("人工处置操作")
    @PostMapping("dealException")
    @ApiOperation(value = "人工处置操作", notes = "人工处置操作")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "operatePersonName", value = "处置人员", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "dealBasic", value = "处置依据", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号(多条用逗号隔开)", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "List<Map<String,Object>>", name = "listHuman", value = "选择的维护人员(包含姓名humanName和电话phone)", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "content", value = "短信内容", defaultValue = "1", required = false, paramType = "query")
    })
    public Result dealException(TabDeviceExceptionVo vo){
        tabDeviceExceptionService.dealException(vo);
        return new Result().success("操作成功");
    }
//    @HandleLog("填写维护信息")
    @PostMapping("fillDefendMessage")
    @ApiOperation(value = "填写维护信息", notes = "填写维护信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "维护设备编号", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "dealPersonName", value = "维护人员", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "dealTimeFrom", value = "维护开始时间", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "dealTimeTo", value = "维护结束时间", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "defendRemark", value = "维护详情", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "defendImgs", value = "维护图片", defaultValue = "1", required = false, paramType = "query")
    })
    public Result fillDefendMessage(TabDeviceExceptionVo vo){
        tabDeviceExceptionService.fillDefendMessage(vo);
        return new Result().success("操作成功");
    }

//    @HandleLog("当前数据异动查看")
    @PostMapping("listDataException")
    @ApiOperation(value = "当前数据异动查看", notes = "当前数据异动查看")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query")
    })
    public Result listDataException(TabDeviceExceptionVo vo){
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<Map<String,Object>>(tabDeviceExceptionService.listDataException(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

//    @HandleLog("处置记录查看")
    @PostMapping("listDealRecord")
    @ApiOperation(value = "处置记录查看", notes = "处置记录查看")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "exceptionType", value = "异常类别", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "dealType", value = "处置方式", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "operatePersonName", value = "处置人员", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "startFrom", value = "搜索开始时间", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "startTo", value = "搜索结束时间", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query")
    })
    public Result listDealRecord(TabDeviceExceptionVo vo){
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<Map<String,Object>>(tabDeviceExceptionService.listDealRecord(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

//    @HandleLog("处置记录查看详情")
    @PostMapping("listDealRecordDetail")
    @ApiOperation(value = "处置记录查看详情", notes = "处置记录查看详情")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceNo", value = "设备编号", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "operateTime", value = "处置时间", defaultValue = "1", required = false, paramType = "query"),
    })
    public Result listDealRecordDetail(String deviceNo,String operateTime){
        Map<String,Object> map = tabDeviceExceptionService.listDealRecordDetail(deviceNo,operateTime);
        return new Result().success(map);
    }

    /**
     * 文件上传
     */
//    @HandleLog("文件上传")
    @ResponseBody
    @RequestMapping(value = "fileUpload", method = RequestMethod.POST)
    public Result fileUpload(MultipartFile file) {
        // 获取文件名
        String fileName = file.getOriginalFilename();
        // 获取文件的后缀名
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        if(!isEquals(suffixName)){
            return new Result().failure(-1,"请上传正确的类型");
        }
        // 解决文件名重复问题 -- 解决中文问题，liunx下中文路径，图片显示问题
        String attachmentId = UUID.randomUUID().toString();
        String filePath = "img/tabDeviceException/"+attachmentId + suffixName;
        File dest = new File(attachmentUrlSet + File.separator + filePath);
        // 检测是否存在目录
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        // 存储文件
        try {
            file.transferTo(dest);
        } catch (Exception e) {
            return new Result().failure(-1,"上传失败,请重新上传");
        }
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", fileName);
        map.put("lookUrl", fileIp+File.separator+filePath);
        map.put("filePath", filePath);
        return new Result().success(map);
    }
    private static Boolean isEquals(String suffixName){
        Boolean flag = suffixName.equalsIgnoreCase(".jpg")||suffixName.equalsIgnoreCase(".png")
                    ||suffixName.equalsIgnoreCase(".gif")||suffixName.equalsIgnoreCase(".tif")
                    ||suffixName.equalsIgnoreCase(".jpeg")||suffixName.equalsIgnoreCase(".svg")
                    ||suffixName.equalsIgnoreCase(".bmp")||suffixName.equalsIgnoreCase(".webp");
        return flag;
    }


}
