package com.cqndt.disaster.device.project;

import com.cqndt.disaster.device.common.service.TabAttachmentService;
import com.cqndt.disaster.device.common.util.AbstractController;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.project.service.TabProjectService;
import com.cqndt.disaster.device.domain.*;
import com.cqndt.disaster.device.project.service.TabReportingFileService;
import com.cqndt.disaster.device.vo.TabProjectVo;
import com.cqndt.disaster.device.vo.TabReportFileVo;
import com.cqndt.disaster.device.vo.TabUserVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/12  9:41
 * Description:项目管理
 */
@RestController
@RequestMapping("/project")
@Api(value = "项目管理", description = "项目管理")
public class TabProjectController extends AbstractController {
    @Autowired
    private TabProjectService tabProjectService;
    @Autowired
    private TabAttachmentService tabAttachmentService;
    @Autowired
    private TabReportingFileService tabReportingFileService;


//    @HandleLog("项目栽点")
    @PostMapping("pointTabProject")
    @ApiOperation(value = "项目栽点", notes = "项目栽点")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectNo", value = "项目编号", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectName", value = "项目名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "disasterNo", value = "灾害点编号", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "disType", value = "灾害类型", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "proIsEnd", value = "是否结束（1已结束 2未结束）", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "monitorUnit", value = "监测单位id", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "manualAuto", value = "人工/自动(1:人工,2:自动)", defaultValue = "", required = false, paramType = "query")
    })
    public Result pointTabProject(TabProjectVo vo){
        TabUserVo userVo = getUser();
        vo.setUserId(userVo.getId());
        if(null == vo.getLevel()|| vo.getLevel()==""){
            vo.setLevel(userVo.getLevel());
            vo.setAreaCode(userVo.getAreaCode());
        }
        List<TabProjectVo> list = tabProjectService.selectByCondition(vo);
        Result result = new Result();
        result.setData(list);
        return result;
    }

//    @HandleLog("项目列表")
    @PostMapping("listTabProject")
    @ApiOperation(value = "项目列表", notes = "项目列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectNo", value = "项目编号", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectName", value = "项目名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "disasterNo", value = "灾害点编号", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "disType", value = "灾害类型", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "proIsEnd", value = "是否结束（1已结束 2未结束）", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "monitorUnit", value = "监测单位id", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "manualAuto", value = "人工/自动(1:人工,2:自动)", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query")
    })
    public Result listTabProject(TabProjectVo vo){
        TabUserVo userVo = getUser();
        vo.setUserId(userVo.getId());
        if(null == vo.getLevel()|| vo.getLevel()==""){
            vo.setLevel(userVo.getLevel());
            vo.setAreaCode(userVo.getAreaCode());
        }
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<TabProjectVo> pageInfo = new PageInfo<TabProjectVo>(tabProjectService.selectByCondition(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
//    @HandleLog("项目基础信息(参见单位、项目班子、图片、视频)")
    @PostMapping("getTabProject")
    @ApiOperation(value = "项目基础信息(参见单位、项目班子、图片、视频)", notes = "项目基础信息(参见单位、项目班子、图片、视频)")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "项目id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabDevice(TabProjectVo vo){
        Map<String,Object> map = new HashMap<>();
        Result result = new Result();
        List<TabProjectVo> list = tabProjectService.selectByCondition(vo);
        if(list.size()>0){
            TabProjectVo tabProject = list.get(0);
            map.put("tabProject",tabProject);
            List<TabAttachment> projectImg = tabAttachmentService.selectAttachments(tabProject.getProjectImg());
            List<TabAttachment> projectVideo = tabAttachmentService.selectAttachments(tabProject.getProjectVideo());
            map.put("projectImg",projectImg);
            map.put("projectVideo",projectVideo);
        }
        List<Map<String, Object>> cjdw = tabProjectService.selectCjdwByProjectId(vo.getId());
        List<Map<String, Object>> xmbz = tabProjectService.selectXmbzByProjectId(vo.getId());
        map.put("cjdw",cjdw);
        map.put("xmbz",xmbz);
        result.setData(map);
        return result;
    }

//    @HandleLog("根据项目id查询项目文档")
    @PostMapping("/getTabProjectDatum")
    @ApiOperation(value = "根据项目id查询项目文档", notes = "根据项目id查询项目文档")
    @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query")
    public Result getTabProjectDatum(Integer projectId){
        Result result = new Result();
        List<Map<String,Object>> list = tabProjectService.selectProjectDatum(projectId);
        result.setData(list);
        return result;
    }
//    @HandleLog("根据项目id获取设备")
    @PostMapping("/getDeviceByProjectId")
    @ApiOperation(value = "根据项目id获取设备", notes = "根据项目id获取设备")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query")
    })
    public Result getDeviceByProjectId(Integer projectId){
        Result result = new Result();
        List<Map<String,Object>> list  = tabProjectService.getDeviceByProjectId(getUserId(),projectId);
        result.setData(list);
        return result;
    }

//    @HandleLog("根据项目id获取设备数量、在线数量")
    @PostMapping("/getDeviceNumByProjectId")
    @ApiOperation(value = "根据项目id获取设备数量、在线数量", notes = "根据项目id获取设备数量、在线数量")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query")
    })
    public Result getDeviceNumByProjectId(Integer projectId){
        Result result = new Result();
        Map<String,Object> map  = tabProjectService.getDeviceNumByProjectId(getUserId(),projectId);
        result.setData(map);
        return result;
    }

//    @HandleLog("根据项目id获取报表")
    @PostMapping("/listReportingFile")
    @ApiOperation(value = "根据项目id获取报表", notes = "根据项目id获取报表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "startTime", value = "开始时间", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "endTime", value = "结束时间", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "fileType", value = "报表类型", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query")
    })
    public Result listReportingFile(TabReportFileVo tabReportFileVo){
        PageHelper.startPage(tabReportFileVo.getPage(),tabReportFileVo.getLimit());
        PageInfo<TabReportFileVo> pageInfo = new PageInfo<TabReportFileVo>(tabReportingFileService.listReportingFile(tabReportFileVo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
}
