package com.cqndt.disaster.device.map;

import com.cqndt.disaster.device.common.util.Result;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tabMap")
@Api(value = "底图查询", description = "底图查询")
public class TabMapController {

    @Autowired
    private TabMapService tabMapService;

//    @HandleLog("查询所有底图")
    @ApiOperation(value = "查询所有底图", notes = "查询所有底图")
    @PostMapping("getAllMap")
    public Result getAllMap() {
        return tabMapService.getAllMap();
    }
}
