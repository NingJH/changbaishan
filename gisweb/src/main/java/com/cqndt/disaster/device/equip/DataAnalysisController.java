package com.cqndt.disaster.device.equip;

import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.domain.TabWarnSetting;
import com.cqndt.disaster.device.equip.service.AnalysisService;
import com.cqndt.disaster.device.equip.service.TabDeviceService;
import com.cqndt.disaster.device.vo.DeviceInProjectVo;
import com.cqndt.disaster.device.vo.SearchVo;
import com.cqndt.disaster.device.vo.TabSensorVo;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/10  15:26
 * Description:
 */
@Controller
@RequestMapping("/analysis")
@Api(value = "数据分析", description = "数据分析")
public class DataAnalysisController {
    @Autowired
    private TabDeviceService tabDeviceService;
    @Autowired
    private AnalysisService analysisService;

//    @HandleLog("安装布置点对应的传感器")
    @PostMapping("listTabSensor")
    @ApiOperation(value = "安装布置点对应的传感器", notes = "安装布置点对应的传感器")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "monitorNo", value = "安装布置点编号", defaultValue = "1", required = true, paramType = "query")
    })
    @ResponseBody
    public Result listTabSensor(String monitorNo){
        List<TabSensorVo> list = tabDeviceService.getSensorByMonitorNo(monitorNo);
        Result result = new Result();
        result.setData(list);
        return result;
    }
//    @HandleLog("数据曲线")
    @PostMapping("echart")
    @ApiOperation(value = "数据曲线", notes = "数据曲线")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "sensorNo", value = "传感器编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "sensorType", value = "传感器类型", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "startTime", value = "开始时间", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "endTime", value = "结束时间", defaultValue = "", required = true, paramType = "query")
    })
    @ResponseBody
    public Result echart(SearchVo vo){
        Result result = new Result();
        Map<String, Object> map = analysisService.selecteChart(vo);
        result.setData(map);
        return result;
    }

//    @HandleLog("修改设备刻度")
    @PostMapping("updateDeviceById")
    @ApiOperation(value = "修改设备刻度", notes = "修改设备刻度")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceScale", value = "刻度", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "设备id", defaultValue = "", required = true, paramType = "query"),
    })
    @ResponseBody
    public Result updateDeviceById(Integer id, String deviceScale) {
        return tabDeviceService.updateDeviceById(id, deviceScale);
    }

//    @HandleLog("项目下所有的设备")
    @PostMapping("selectDeviceByProjectId")
    @ApiOperation(value = "项目下所有的设备", notes = "根据项目id查询所有的设备")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "projectId", value = "安装布置点编号", defaultValue = "1", required = true, paramType = "query")
    })
    @ResponseBody
    public Result selectDeviceByProjectId(String projectId) {
        Result result = new Result();
        List<DeviceInProjectVo> list = analysisService.selectDeviceByProjectId(projectId);
        result.setData(list);
        return result;
    }
//    @HandleLog("根据传感器编号获取相应的阈值")
    @PostMapping("getSettingBySensorNo")
    @ApiOperation(value = "根据传感器编号获取相应的阈值", notes = "根据传感器编号获取相应的阈值")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "sensorNo", value = "传感器编号", defaultValue = "1", required = true, paramType = "query")
    })
    @ResponseBody
    public Result getSettingBySensorNo(String sensorNo){
        TabWarnSetting setting = analysisService.getSettingBySensorNo(sensorNo);
        Result result = new Result();
        result.setData(setting);
        return result;
    }

//    @HandleLog("数据导出")
    @GetMapping("dataExport")
    @ApiOperation(value = "数据导出", notes = "数据导出")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "monitorNos", value = "监测点编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "startTime", value = "开始时间", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "endTime", value = "结束时间", defaultValue = "", required = true, paramType = "query")
    })
    public void dataExport(String monitorNos, String startTime, String endTime, HttpServletRequest request, HttpServletResponse response){
        try{
            analysisService.dataExport(monitorNos,startTime,endTime,request,response);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @PostMapping("updateScale")
    @ApiOperation(value = "修改刻度", notes = "修改刻度")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "type", value = "设备类型", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "scale", value = "刻度", defaultValue = "", required = true, paramType = "query")
    })
    @ResponseBody
    public Result updateScale(Integer typeId,String scale) {
        return tabDeviceService.updateScale(typeId,scale);
    }

    @PostMapping("selectDeviceType")
    @ApiOperation(value = "通过设备类型查询设备类型", notes = "查询设备类型")
    @ResponseBody
    public Result selectDeviceType(Integer deviceType,String monitorNo) {
        return tabDeviceService.selectDeviceType(deviceType, monitorNo);
    }

    @PostMapping("selectType")
    @ApiOperation(value = "通过项目查询设备类型", notes = "查询设备类型")
    @ResponseBody
    public Result selectDeviceType(String projectId) {
        return tabDeviceService.selectDeviceType(projectId);
    }

}
