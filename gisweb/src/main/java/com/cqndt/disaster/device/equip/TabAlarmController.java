package com.cqndt.disaster.device.equip;

import com.cqndt.disaster.device.common.util.AbstractController;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.equip.service.TabAlarmInfoService;
import com.cqndt.disaster.device.vo.TabAlarmInfoVo;
import com.cqndt.disaster.device.vo.TabUserVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
//import com.ut.uttools.annos.HandleLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/17  17:09
 * Description:告警管理
 */
@RestController
@RequestMapping("/tabAlarm")
@Api(value = "告警管理", description = "告警管理")
public class TabAlarmController extends AbstractController {
    @Autowired
    private TabAlarmInfoService tabAlarmInfoService;

//    @HandleLog("告警管理列表")
    @PostMapping("listTabAlarmInfo")
    @ApiOperation(value = "告警管理列表", notes = "告警管理列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectId", value = "项目id", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceName", value = "设备名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "alarmType", value = "告警类型", defaultValue = "1", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "warmLevel", value = "告警等级", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "startTime", value = "开始时间", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "endTime", value = "结束时间", defaultValue = "", required = true, paramType = "query")
    })
    public Result listTabAlarmInfo(TabAlarmInfoVo vo){
        TabUserVo userVo = getUser();
        vo.setUserId(userVo.getId());
       // vo.setUserId(2);
        if(null == vo.getLevel()){
            vo.setAreaLevel(userVo.getLevel());
            vo.setAreaCode(userVo.getAreaCode());
        }else{
            vo.setAreaLevel(vo.getLevel().toString());
            vo.setAreaCode(vo.getAreaCode());
        }
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<TabAlarmInfoVo> pageInfo = new PageInfo<TabAlarmInfoVo>(tabAlarmInfoService.listTabAlarmInfo(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
//    @HandleLog("根据监测点id查询告警详情")
    @PostMapping("getDetailByMonitorId")
    @ApiOperation(value = "根据监测点id查询告警详情", notes = "根据监测点id查询告警详情")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "monitorId", value = "监测点id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getDetailByMonitorId(TabAlarmInfoVo vo){
        TabUserVo userVo = getUser();
        vo.setUserId(userVo.getId());
        vo.setAreaLevel(userVo.getLevel());
        vo.setAreaCode(userVo.getAreaCode());
        TabAlarmInfoVo infoVo = tabAlarmInfoService.getDetailByMonitorId(vo);
        Result result = new Result();
        result.setData(infoVo);
        return result;
    }
//    @HandleLog("根据告警id查询告警详情")
    @PostMapping("getDetailById")
    @ApiOperation(value = "根据告警id查询告警详情", notes = "根据告警id查询告警详情")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "告警id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getDetailById(String id){
        TabAlarmInfoVo infoVo = tabAlarmInfoService.getDetailById(id);
        Result result = new Result();
        result.setData(infoVo);
        return result;
    }

//    @HandleLog("根据告警id修改告警状态")
    @PostMapping("updateAlarmByStatusSee")
    @ApiOperation(value = "根据告警id修改告警状态，已查看未处置", notes = "根据告警id修改告警状态，已查看未处置")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "alarmId", value = "告警id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result updateAlarmByStatusSee(Integer alarmId) {
        return tabAlarmInfoService.updateAlarmByStatusSee(alarmId);
    }

//    @HandleLog("根据告警id修改告警状态，告警误报")
    @PostMapping("updateAlarmByStatusMistake")
    @ApiOperation(value = "根据告警id修改告警状态，告警误报", notes = "根据告警id修改告警状态，告警误报")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "alarmId", value = "告警id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result updateAlarmByStatusMistake(Integer alarmId) {
        return tabAlarmInfoService.updateAlarmByStatusMistake(alarmId);
    }

//    @HandleLog("根据告警id修改告警状态，成功")
    @PostMapping("updateAlarmByStatusSuccess")
    @ApiOperation(value = "根据告警id修改告警状态，成功", notes = "根据告警id修改告警状态，成功")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "alarmId", value = "告警id", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "content", value = "内容", defaultValue = "1", required = true, paramType = "query")
    })
    public Result updateAlarmByStatusSuccess(Integer alarmId, String content) {
        return tabAlarmInfoService.updateAlarmByStatusSuccess(alarmId, content);
    }

//    @HandleLog("根据告警id上报")
    @PostMapping("updateAlarmByUploadSms")
    @ApiOperation(value = "根据告警id上报", notes = "根据告警id上报")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "alarmId", value = "告警id", defaultValue = "1", required = true, paramType = "query"),
    })
    public Result updateAlarmByUploadSms(Integer alarmId) {
        return tabAlarmInfoService.updateAlarmByUploadSms(alarmId);
    }

//    @HandleLog("根据告警id查询联合告警列表")
    @PostMapping("queryTabAlarmInfoList")
    @ApiOperation(value = "根据告警id查询联合告警列表", notes = "根据告警id查询联合告警列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "alarmId", value = "告警id", defaultValue = "1", required = true, paramType = "query"),
    })
    public Result queryTabAlarmInfoList(TabAlarmInfoVo vo) {
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(tabAlarmInfoService.queryTabAlarmInfoList(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

//    @HandleLog("报警器")
    @PostMapping("alarm")
    @ApiOperation(value = "查询设备厂商", notes = "查询设备厂商")
    public Result alarm(String deviceFactory,String deviceNo,int isOnline) {
        Result result = new Result();
        Result list = tabAlarmInfoService.alarm(deviceFactory,deviceNo,isOnline);
        result.setData(list);
        return result;
    }
}
