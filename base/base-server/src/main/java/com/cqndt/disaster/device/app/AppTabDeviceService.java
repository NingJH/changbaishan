package com.cqndt.disaster.device.app;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/13  10:31
 * Description:
 */
public interface AppTabDeviceService {
    /**
     * 设备总数
     */
    Integer selectCountDevice(String userId);

    /**
     * 在线设备数
     */
    Integer selectOnlineCountDevice(String userId);
    /**
     * 离线设备数
     */
    Integer selectOfflineCountDevice(String userId);
    /**
     * 项目所有设备在线情况
     */
    List<Map<String, Object>> selectOnlineRateByProject(String userId, String seachText);

    /**
     * 查询某个项目下的设备情况及负责人
     */
    List<Map<String, Object>> selectOnlineDeviceByProjectID(Integer projectID);
    /**
     * 项目的项目班子
     */
    List<Map<String, Object>> selectPerson(Integer projectID);

    /**
     * 根据设备id查询所在项目的监测点
     */
    Map<String, Object> selectDeviceById(int deviceId);

    /**
     * 据项目id获取所有监测点的坐标
     */
    List<Map<String, Object>> deviceCoordinateByProjectId(int id);

    /**
     * 根据设备id获取对应的传感器
     * @param deviceId
     * @return
     */
    List<Map<String,Object>> listTabSensor(String deviceId);

    /**
     * 设备类型统计
     */
    List<Map<String, Object>> selectDeviceTypeStatistics(String userId);
    /**
     * 设备类型总数
     */
   Integer selectDeviceTypeStatisticsTotal(String userId);
    /**
     * 根据设备类型统计项目
     */
    List<Map<String, Object>> selectProjectByDeviceType(String userId,String deviceTypeId,String projectName);
    /**
     * 根据设备类型统计项目总数
     */
    Integer selectProjectByDeviceTypeCount(String userId,String deviceTypeId,String projectName);

    /**
     * 根据设备类型和projectId查看监测点
     */
    List<Map<String, Object>> projectByDeviceTypeAndId(String deviceTypeId,String projectId);

    /**
     * 数据曲线查询
     */
    List<Map<String, Object>> monitorStatistics(String sensorType, String type, String sensorNo);
}
