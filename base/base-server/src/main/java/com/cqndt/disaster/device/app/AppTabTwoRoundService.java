package com.cqndt.disaster.device.app;

import com.cqndt.disaster.device.domain.TabTwoRound;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/12  15:43
 * Description:获取全景图
 */
public interface AppTabTwoRoundService {
    /**
     * 项目全景图
     * @param projectId
     * @param modelType
     * @return
     */
    Map<String,Object> selectByProjectId(Integer projectId, String modelType);
}
