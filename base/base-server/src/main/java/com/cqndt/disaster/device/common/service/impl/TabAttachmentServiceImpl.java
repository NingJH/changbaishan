package com.cqndt.disaster.device.common.service.impl;

import com.cqndt.disaster.device.domain.TabAttachment;
import com.cqndt.disaster.device.dao.TabAttachmentMapper;
import com.cqndt.disaster.device.common.service.TabAttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/12  15:20
 * Description:获取附件
 */
@Service
public class TabAttachmentServiceImpl implements TabAttachmentService {
    @Autowired
    private TabAttachmentMapper tabAttachmentMapper;
    @Value("${file-ip}")
    private String fileIp;

    @Override
    public List<TabAttachment> selectAttachments(String imgIds){
        List<TabAttachment> list = new ArrayList<>();
        if(null != imgIds && imgIds !=""){
            String[] ids = imgIds.split(",");
            list = tabAttachmentMapper.selectByIds(ids);
            for (TabAttachment tabAttachment:list) {
                switch (tabAttachment.getAttachmentType()){
                    case "1":
                        tabAttachment.setAttachmentUrl(fileIp+File.separator+"img"+File.separator+tabAttachment.getAttachmentUrl());
                        break;
                    case "2":
                        tabAttachment.setAttachmentUrl(fileIp+File.separator+"video"+File.separator+tabAttachment.getAttachmentUrl());
                        break;
                    case "3":
                        tabAttachment.setAttachmentUrl(fileIp+File.separator+"file"+File.separator+tabAttachment.getAttachmentUrl());
                        break;
                }
            }
        }
        return list;
    }
}
