package com.cqndt.disaster.device.app;

import com.cqndt.disaster.device.vo.Monitor;
import com.cqndt.disaster.device.vo.TabProjectVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/12  10:31
 * Description:
 */
public interface AppTabProjectService {
    /**
     *  项目列表
     */
    List<Map<String,Object>> selectByCondition(String userId,String level,String areaCode, String id, String search);
    /**
     * 获取项目详情
     * @param projectId
     * @return
     */
    Map<String,Object> selectByIdDetails(Integer projectId,String userId);

    /**
     * 查询监测项目置顶的列表
     */
    List<Map<String,Object>> selectAllPush(String userId);

    /**
     * 项目文档
     * @param projectId
     * @return
     */
    List<Map<String, Object>> selectProjectDatum(Integer projectId) ;

    /**
     * 查询项目下所有设备
     * @return
     */
    Map<String,Object> getByProjectId(Integer projectId,String search,Integer monitorType);

    /**
     * 根据监测点id查询相关设备数据
     * @param monitorId   监测点id
     * @return
     */
    Monitor getMonitorDataById(Integer monitorId);

    /**
     * 获取项目图片
     * @param projectId
     * @return
     */
    List<Map<String,Object>> selectAttachmentImgs(Integer projectId,String userId);
    /**
     * 获取项目视频
     * @param projectId
     * @return
     */
    List<Map<String,Object>> selectAttachmentVideos(Integer projectId,String userId);

}
