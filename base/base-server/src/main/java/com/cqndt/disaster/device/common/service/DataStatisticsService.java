package com.cqndt.disaster.device.common.service;

import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.domain.TabNPerson;
import com.cqndt.disaster.device.vo.TabUserVo;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/13  10:14
 * Description:数据统计
 */
public interface DataStatisticsService {

    /**
     * 基础数据统计
     * @param vo 用户信息
     * @return
     */
    Map<String,Object> countBaseNum(TabUserVo vo);

    /**
     * 监测方法统计
     * @param userId
     * @return
     */
    List<Map<String,Object>> countMonitorTypeNum(Integer  userId);

    /**
     * 灾害点类型统计
     * @param vo
     * @return
     */
    List<Map<String,Object>> countBasicTypeNum(TabUserVo vo);

    /**
     * 仪器状态统计（1.1日无数据 2.2日无数据 3.3日无数据 4.4日及以上无数据 5.正常）
     * @param userId
     * @return
     */
    Map<String,Object> countInstrumentNum(Integer userId);

    /**
     * 设备在线离线统计
     * @param userId
     * @return
     */
    List<Map<String,Object>> countDeviceStateNum(Integer userId);

    /**
     * 设备累计告警统计
     * @param userId
     * @return
     */
    List<Map<String,Object>> countDeviceAlarmNum(Integer userId);

    /**
     * 前台显示所有设备总在线、离线情况
     * @param userId
     * @return
     */
    Map<String,Object> countAllDeviceStateOnline(Integer userId);

    Result countDeviceStateNum1(Integer userId, Integer page, Integer size);

    List<Map<String, Object>> equipmentCondition(Integer userId);

    Map<String, Object> statistics(Integer userId);

    List<Map<String, Object>> warnMsgList(String startTime, String endTime,Integer userId);

    Map<String, Object> meteorological();

    List<Map<String, Object>> warnMsgInfo(String deviceNo);

    Map<String, Object> warnMsgDetails(int id);

    Map<String, Object> warnMsgDevice(String deviceNo);

    Map<String, Object> warnMsgListCount();

    List<Map<String, Object>> getNPerson(int pType,String name,String wPhone);

    List<Map<String, Object>> getPointNPerson(int pType,String name,String wPhone);

    Map<String, Object> getNPersonInfo(int id);

    List<Map<String, Object>> getBasicNperson(String disNo);
}
