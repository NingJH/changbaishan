package com.cqndt.disaster.device.common.service.impl;

import com.cqndt.disaster.device.common.shrio.ShiroUtils;
import com.cqndt.disaster.device.domain.TabStaticType;
import com.cqndt.disaster.device.dao.TabStaticTypeMapper;
import com.cqndt.disaster.device.common.service.TabStaticTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/12  9:33
 * Description:
 */
@Service
public class TabStaticTypeServiceImpl implements TabStaticTypeService {
    @Autowired
    private TabStaticTypeMapper tabStaticTypeMapper;
    @Override
    public List<TabStaticType> selectByStaticNum(String staticNum){
        return tabStaticTypeMapper.selectByStaticNum1(ShiroUtils.getUserId().toString());
    }

    @Override
    public List<TabStaticType> selectByStaticNum1(String staticNum){
        return tabStaticTypeMapper.selectByStaticNum(ShiroUtils.getUserId().toString());
    }
}
