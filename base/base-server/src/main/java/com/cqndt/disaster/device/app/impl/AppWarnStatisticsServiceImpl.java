package com.cqndt.disaster.device.app.impl;

import com.cqndt.disaster.device.app.AppWarnStatisticsService;
import com.cqndt.disaster.device.common.util.DateFormatUtil;
import com.cqndt.disaster.device.dao.app.AppWarnStatisticsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created By marc
 * Date: 2019/5/12  15:11
 * Description:
 */
@Service
public class AppWarnStatisticsServiceImpl implements AppWarnStatisticsService {
    @Autowired
    private AppWarnStatisticsMapper appWarnStatisticsMapper;

    @Override
    public List<Map<String, Object>> findWarnStatictics(Map<String, Object> map) {
        map.put("areaCodeSplit",map.get("areaCode").toString().substring(0,2));
        map = getSearchData(map);
        return appWarnStatisticsMapper.findWarnStatictics(map);
    }

    @Override
    public List<Map<String, Object>> findWarnMsg(Map<String, Object> map) {
        map.put("areaCodeSplit",map.get("areaCode").toString().substring(0,2));
        map = getSearchData(map);
        return appWarnStatisticsMapper.findWarnMsg(map);
    }

    @Override
    public List<Map<String, Object>> findWarnCount(Map<String, Object> map) {
        map.put("areaCodeSplit",map.get("areaCode").toString().substring(0,2));
        map = getSearchData(map);
        List<Map<String, Object>> list = appWarnStatisticsMapper.findWarnCount(map);
        BigDecimal type1=BigDecimal.ZERO;
        BigDecimal type2=BigDecimal.ZERO;
        BigDecimal type3=BigDecimal.ZERO;
        for (Map<String, Object> mapResult:list) {
            BigDecimal typeo = (BigDecimal)mapResult.get("type1");
            BigDecimal typet = (BigDecimal)mapResult.get("type2");
            BigDecimal typeth = (BigDecimal)mapResult.get("type3");
            type1 = type1.add(typeo);
            type2 = type2.add(typet);
            type3 = type3.add(typeth);
            mapResult.put("sum",typeo.add(typet).add(typeth));
        }
        Map<String, Object> typeMap = new HashMap<>();
        typeMap.put("type1",type1);
        typeMap.put("type2",type2);
        typeMap.put("type3",type3);
        typeMap.put("sum",type1.add(type2).add(type3));
        typeMap.put("warn_level","合计");
        list.add(typeMap);
        return list;
    }

    /**
     * 组装查询条件
     */
    private Map<String, Object> getSearchData(Map<String, Object> map){
        if("1".equals(map.get("type"))){
            map.put("start_time",new SimpleDateFormat("yyyy-MM-dd").format( new Date())+" 00:00:00");
            map.put("end_time",new SimpleDateFormat("yyyy-MM-dd").format( new Date())+" 23:59:59");
        }else if("2".equals(map.get("type"))){
            Map<String,Object> timeNum = DateFormatUtil.getMonSumTime();
            map.put("start_time",timeNum.get("mon"));
            map.put("end_time",timeNum.get("sun"));
        }else{
            Map<String,Object> timeNum = DateFormatUtil.getMonthFirstAndEndDay();
            map.put("start_time",timeNum.get("first"));
            map.put("end_time",timeNum.get("end"));
        }
        return map;
    }


}
