package com.cqndt.disaster.device.data.serivce.impl;

import com.cqndt.disaster.device.data.serivce.TbsDataReciverService;
import com.cqndt.disaster.device.jdbc.dao.ReciverDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TbsDataReciverServiceImpl implements TbsDataReciverService{



    @Autowired
    private ReciverDao reciverDao;

    @Override
    public void saveBatch(List tbsData) {
        //以事务方式批量提交数据
        tbsData.forEach(data->{
            reciverDao.autoSave(data);
        });

    }



}
