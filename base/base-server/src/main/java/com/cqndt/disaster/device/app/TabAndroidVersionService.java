package com.cqndt.disaster.device.app;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/11  13:06
 * Description:
 */
public interface TabAndroidVersionService {

    List<Map<String,Object>> findNewVersion();
}
