package com.cqndt.disaster.device.user.service.impl;

import com.cqndt.disaster.device.dao.TabLoginLogMapper;
import com.cqndt.disaster.device.domain.TabLoginLog;
import com.cqndt.disaster.device.user.service.TabLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created By marc
 * Date: 2019/5/20  15:49
 * Description:
 */
@Service
public class TabLoginServiceImpl implements TabLoginService {
    @Autowired
    private TabLoginLogMapper tabLoginLogMapper;
    @Override
    public void insert(TabLoginLog log) {
        tabLoginLogMapper.insertSelective(log);
    }
}
