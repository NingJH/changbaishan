package com.cqndt.disaster.device.user.service;

import com.cqndt.disaster.device.domain.TabUser;
import com.cqndt.disaster.device.vo.TabUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

public interface TabUserService {

    TabUserVo getUserByUserName(TabUser user);
}
