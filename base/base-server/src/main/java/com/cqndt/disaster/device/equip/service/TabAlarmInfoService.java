package com.cqndt.disaster.device.equip.service;

import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.vo.TabAlarmInfoVo;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/22  15:04
 * Description:
 */
public interface TabAlarmInfoService {
    /**
     * 告警列表查询
     * @param vo
     * @return
     */
    List<TabAlarmInfoVo> listTabAlarmInfo(TabAlarmInfoVo vo);

    /**
     * 告警列表查询
     * @return
     */
    List<Map<String, Object>> queryTabAlarmInfoList(TabAlarmInfoVo vo);

    /**
     * 根据监测点id查询告警详情信息
     * @param vo
     * @return
     */
    TabAlarmInfoVo getDetailByMonitorId(TabAlarmInfoVo vo);

    /**
     * 根据告警id查询详情
     * @param id
     * @return
     */
    TabAlarmInfoVo getDetailById(String id);

    /**
     * 根据监测点id获取告警列表
     * @param vo
     * @return
     */
    List<TabAlarmInfoVo> getAlarmsByMonitorId(TabAlarmInfoVo vo);

    /**
     * 修改告警状态
     * @return
     */
    Result updateAlarmByStatusSee(Integer alarmId);

    /**
     * 修改误报状态
     * @param alarmId
     * @return
     */
    Result updateAlarmByStatusMistake(Integer alarmId);

    /**
     * 修改成功状态
     * @param alarmId
     * @return
     */
    Result updateAlarmByStatusSuccess(Integer alarmId, String content);

    /**
     * 上报第二批次人员
     * @param alarmId
     * @return
     */
    Result updateAlarmByUploadSms(Integer alarmId);

    Result alarm(String deviceFactory,String deviceNo,int isOnline);
}
