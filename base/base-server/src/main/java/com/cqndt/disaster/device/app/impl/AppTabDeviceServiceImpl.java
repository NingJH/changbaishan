package com.cqndt.disaster.device.app.impl;

import com.cqndt.disaster.device.app.AppTabDeviceService;
import com.cqndt.disaster.device.common.influxdb.InfluxDBConnect;
import com.cqndt.disaster.device.common.util.DateFormatUtil;
import com.cqndt.disaster.device.dao.TabSensorInitMapper;
import com.cqndt.disaster.device.dao.TabStaticTypeMapper;
import com.cqndt.disaster.device.dao.app.AppTabDeviceMapper;
import com.cqndt.disaster.device.dao.app.AppTabStaticTypeMapper;
import com.cqndt.disaster.device.domain.TabStaticType;
import com.cqndt.disaster.device.vo.SearchVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/13  10:31
 * Description:
 */
@Service
public class AppTabDeviceServiceImpl implements AppTabDeviceService {
    @Autowired
    private AppTabDeviceMapper appTabDeviceMapper;
    @Autowired
    private AppTabStaticTypeMapper appTabStaticTypeMapper;
    @Autowired
    private InfluxDBConnect influxDBConnect;
    @Autowired
    private TabSensorInitMapper tabSensorInitMapper;

    private BigDecimal maxData = null;
    private BigDecimal minData = null;

    @Override
    public Integer selectCountDevice(String userId) {
        return appTabDeviceMapper.selectCountDevice(userId);
    }

    @Override
    public Integer selectOnlineCountDevice(String userId) {
        List<Map<String,Object>> list = appTabStaticTypeMapper.selectByStaticNum("11");
        //离线时间判断
        Integer num = (Integer)list.get(0).get("id");
        Integer zxNum = appTabDeviceMapper.countDeviceStateZx(userId,num);
        return zxNum;
    }

    @Override
    public Integer selectOfflineCountDevice(String userId) {
        List<Map<String,Object>> list = appTabStaticTypeMapper.selectByStaticNum("11");
        //离线时间判断
        Integer num = (Integer)list.get(0).get("id");
        Integer lxNum = appTabDeviceMapper.countDeviceStateLx(userId,num);
        return lxNum;
    }

    @Override
    public List<Map<String, Object>> selectOnlineRateByProject(String userId, String seachText) {
        List<Map<String,Object>> list = appTabStaticTypeMapper.selectByStaticNum("11");
        //离线时间判断
        Integer num = (Integer)list.get(0).get("id");
        return appTabDeviceMapper.selectOnlineRateByProject(userId,num,seachText);
    }

    @Override
    public List<Map<String, Object>> selectOnlineDeviceByProjectID(Integer projectID) {
        List<Map<String,Object>> list = appTabStaticTypeMapper.selectByStaticNum("11");
        //离线时间判断
        Integer num = (Integer)list.get(0).get("id");
        return appTabDeviceMapper.selectOnlineDeviceByProjectID(projectID,num);
    }

    @Override
    public List<Map<String, Object>> selectPerson(Integer projectID) {
        return appTabDeviceMapper.selectPerson(projectID);
    }

    @Override
    public Map<String, Object> selectDeviceById(int deviceId) {
        List<Map<String,Object>> list = appTabStaticTypeMapper.selectByStaticNum("11");
        //离线时间判断
        Integer num = (Integer)list.get(0).get("id");
        return appTabDeviceMapper.selectDeviceById(deviceId,num);
    }

    @Override
    public List<Map<String, Object>> deviceCoordinateByProjectId(int id) {
        return appTabDeviceMapper.deviceCoordinateByProjectId(id);
    }

    @Override
    public List<Map<String, Object>> listTabSensor(String deviceId) {
        return appTabDeviceMapper.listTabSensor(deviceId);
    }

    @Override
    public List<Map<String, Object>> selectDeviceTypeStatistics(String userId) {
        return appTabDeviceMapper.selectDeviceTypeStatistics(userId);
    }

    @Override
    public Integer selectDeviceTypeStatisticsTotal(String userId) {
        return appTabDeviceMapper.selectDeviceTypeStatisticsTotal(userId);
    }

    @Override
    public List<Map<String, Object>> selectProjectByDeviceType(String userId, String deviceTypeId, String projectName) {
        return appTabDeviceMapper.selectProjectByDeviceType(userId,deviceTypeId,projectName);
    }

    @Override
    public Integer selectProjectByDeviceTypeCount(String userId, String deviceTypeId, String projectName) {
        return appTabDeviceMapper.selectProjectByDeviceTypeCount(userId,deviceTypeId,projectName);
    }

    @Override
    public List<Map<String, Object>> projectByDeviceTypeAndId(String deviceTypeId, String projectId) {
        return appTabDeviceMapper.projectByDeviceTypeAndId(deviceTypeId,projectId);
    }


    @Override
    public List<Map<String, Object>> monitorStatistics(String sensorType, String type, String sensorNo) {
//        BigDecimal maxData = null;
//        BigDecimal minData = null;
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        String startTime = null;
        String endTime = null;
        String orderStr = "order by time";
        switch (type){
            case "today":
                startTime = (String)DateFormatUtil.getToday().get("start_time");
                endTime = (String)DateFormatUtil.getToday().get("end_time");
                break;
            case "threeDay":
                startTime = (String)DateFormatUtil.getPreToday(-2).get("start_time");
                endTime = (String)DateFormatUtil.getPreToday(-2).get("end_time");
                orderStr = "order by time desc";
                break;
            case "week":
                startTime = (String)DateFormatUtil.getMonSumTime().get("mon");
                endTime = (String)DateFormatUtil.getMonSumTime().get("sun");
                break;
            case "month":
                startTime = (String)DateFormatUtil.getMonthFirstAndEndDay().get("first");
                endTime = (String)DateFormatUtil.getMonthFirstAndEndDay().get("end");
                break;
            case "3month":
                startTime = (String)DateFormatUtil.getPreThreeMonth(-2).get("first");
                endTime = (String)DateFormatUtil.getPreThreeMonth(-2).get("end");
                break;
        }
        SearchVo vo = new SearchVo();
        vo.setDateType(type);
        vo.setStartTime(startTime);
        vo.setEndTime(endTime);
        vo.setSensorNo(sensorNo);
        Integer sType = Integer.valueOf(sensorType);
        Map<String, Object> unitMap = appTabStaticTypeMapper.selectByStaticNumAndValue("1",sType.toString());
        String unit = unitMap.get("remark").toString();
        switch (sType) {
            //裂缝智能测报仪\裂缝计\裂缝位移计tbs_lfwy
            case 1:
                List<Map<String, Object>>  tbsLfwy = appTabDeviceMapper.tbsLfwy(vo);
               // getParam(tbsLfwy,map,"value","temp");
                result.add(parseData(maxData,minData,tbsLfwy,"param1","位移","time",unit,sensorNo,"value"));
                result.add(parseData(maxData,minData,tbsLfwy,"param2","温度","time",unit,sensorNo,"temp"));
                break;
            //微变形倾角测量仪tbs_qjy_wbx
            case 2:
                List<Map<String, Object>>  tbsQjyWbx = appTabDeviceMapper.tbsQjyWbx(vo);
               // getParam(tbsQjyWbx,map,"qxjd","temp");
                result.add(parseData(maxData,minData,tbsQjyWbx,"param1","倾斜角度","time",unit,sensorNo,"qxjd"));
                result.add(parseData(maxData,minData,tbsQjyWbx,"param2","温度","time",unit,sensorNo,"temp"));
                break;
            //滑坡地表变形测量仪tbs_dbbx_hp
            case 3:
                List<Map<String, Object>>  tbsDbbxHp = appTabDeviceMapper.tbsDbbxHp(vo);
                //getParam(tbsDbbxHp,map,"x","y","z","temp");
                result.add(parseData(maxData,minData,tbsDbbxHp,"param1","x","time",unit,sensorNo,"x"));
                result.add(parseData(maxData,minData,tbsDbbxHp,"param2","y","time",unit,sensorNo,"y"));
                result.add(parseData(maxData,minData,tbsDbbxHp,"param3","z","time",unit,sensorNo,"z"));
                result.add(parseData(maxData,minData,tbsDbbxHp,"param4","温度","time",unit,sensorNo,"temp"));
                break;
            //GNSS位移\地表位移(GPS)tbs_gnss
            case 4:
                List<Map<String, Object>>  tbsGnss = appTabDeviceMapper.tbsGnss(vo);
               // getParam(tbsGnss,map,"x","y","swy","cwy");
                result.add(parseData(maxData,minData,tbsGnss,"param1","东西方向偏移值(正数代表向东偏移，负数代表向西偏移)","time",unit,sensorNo,"x1"));
                result.add(parseData(maxData,minData,tbsGnss,"param2","南北方向偏移值(正数代表向北偏移，负数代表向南偏移)","time",unit,sensorNo,"y1"));
                result.add(parseData(maxData,minData,tbsGnss,"param3","水平方向位移值(当前位置距离初始位置的水平距离)","time",unit,sensorNo,"swy"));
                result.add(parseData(maxData,minData,tbsGnss,"param4","垂直方向位移值(正数表示向上抬升，负数表示向下沉降)","time",unit,sensorNo,"cwy"));
                result.add(parseData(maxData,minData,tbsGnss,"param5","水平变化速率(一个小时的水平位移变化量)移","time","mm/h",sensorNo,"swysd"));
                result.add(parseData(maxData,minData,tbsGnss,"param6","垂直变化速率(一个小时的垂直位移变化量)","time","mm/h",sensorNo,"cwysd"));
                break;
            //深部位移\深部位移(倾斜仪)tbs_sbwy_qxy
            case 5:
                List<Map<String, Object>>  tbsSbwyQxy = appTabDeviceMapper.tbsSbwyQxy(vo);
               // getParam(tbsSbwyQxy,map,"x_jd","y_jd","z_jd","x","y","wy","jd");
                result.add(parseData(maxData,minData,tbsSbwyQxy,"param1","X方向倾角","time",unit,sensorNo,"x_jd"));
                result.add(parseData(maxData,minData,tbsSbwyQxy,"param2","Y方向倾角","time",unit,sensorNo,"y_jd"));
                result.add(parseData(maxData,minData,tbsSbwyQxy,"param3","Z方向倾角","time",unit,sensorNo,"z_jd"));
                result.add(parseData(maxData,minData,tbsSbwyQxy,"param4","x方向偏移值","time",unit,sensorNo,"x"));
                result.add(parseData(maxData,minData,tbsSbwyQxy,"param5","y方向偏移值","time",unit,sensorNo,"y"));
                result.add(parseData(maxData,minData,tbsSbwyQxy,"param6","时间位移","time",unit,sensorNo,"wy"));
                result.add(parseData(maxData,minData,tbsSbwyQxy,"param7","偏移角度值","time",unit,sensorNo,"jd"));
                break;
            //沉降/土体沉降 tbs_chenjiang
            case 6:
            case 35:
                List<Map<String, Object>>  tbsChenjiang = appTabDeviceMapper.tbsChenjiang(vo);
               // getParam(tbsChenjiang,map,"x","y","z","temp");
                result.add(parseData(maxData,minData,tbsChenjiang,"param1","X向倾斜角度","time",unit,sensorNo,"x"));
                result.add(parseData(maxData,minData,tbsChenjiang,"param2","y向倾斜角度","time",unit,sensorNo,"y"));
                result.add(parseData(maxData,minData,tbsChenjiang,"param3","Z向倾斜角度","time",unit,sensorNo,"z"));
                result.add(parseData(maxData,minData,tbsChenjiang,"param4","温度","time",unit,sensorNo,"temp"));
                break;
            //孔隙水压 tbs_kongxishuiya
            case 7:
                List<Map<String, Object>>  tbsKongxishuiya = appTabDeviceMapper.tbsKongxishuiya(vo);
               // getParam(tbsKongxishuiya,map,"value");
                result.add(parseData(maxData,minData,tbsKongxishuiya,"param1","孔隙水压值","time",unit,sensorNo,"value"));
                break;
            //钢筋计 tbs_gangjinji
            case 8:
                List<Map<String, Object>>  tbsGangjinji = appTabDeviceMapper.tbsGangjinji(vo);
               // getParam(tbsGangjinji,map,"value");
                result.add(parseData(maxData,minData,tbsGangjinji,"param1","应力值","time",unit,sensorNo,"value"));
                break;
            //土压力tbs_tyl
            case 9:
                List<Map<String, Object>>  tbsTyl = appTabDeviceMapper.tbsTyl(vo);
                //getParam(tbsTyl,map,"value");
                result.add(parseData(maxData,minData,tbsTyl,"param1","监测值","time",unit,sensorNo,"value"));
                break;
            //雨量tbs_yl
            case 10:
                List<Map<String, Object>>  tbsYl = appTabDeviceMapper.tbsYl(vo);
                //getParam(tbsYl,map,"value");
                result.add(parseData(maxData,minData,tbsYl,"param1","雨量值","time",unit,sensorNo,"value"));
                break;
            //土壤湿度\土壤含水率tbs_hsl
            case 11:
            case 39:
                List<Map<String, Object>>  tbsHsl = appTabDeviceMapper.tbsHsl(vo);
                //getParam(tbsHsl,map,"hsl","deep");
                result.add(parseData(maxData,minData,tbsHsl,"param1","含水率","time",unit,sensorNo,"hsl"));
                result.add(parseData(maxData,minData,tbsHsl,"param2","深度","time",unit,sensorNo,"deep"));
                break;
            //水位计tbs_shuiweiji
            case 12:
                List<Map<String, Object>>  tbsShuiweiji = appTabDeviceMapper.tbsShuiweiji(vo);
                //getParam(tbsShuiweiji,map,"value");
                result.add(parseData(maxData,minData,tbsShuiweiji,"param1","水位值","time",unit,sensorNo,"value"));
                break;
            //应变 tbs_yingbian
            case 13:
                List<Map<String, Object>>  tbsYingbian = appTabDeviceMapper.tbsYingbian(vo);
                //getParam(tbsYingbian,map,"value");
                result.add(parseData(maxData,minData,tbsYingbian,"param1","应变值","time",unit,sensorNo,"value"));
                break;
            //温湿度tbs_wenshidu
            case 14:
                List<Map<String, Object>>  tbsWenshidu = appTabDeviceMapper.tbsWenshidu(vo);
                //getParam(tbsWenshidu,map,"humidity","temp");
                result.add(parseData(maxData,minData,tbsWenshidu,"param1","湿度","time","%RH",sensorNo,"humidity"));
                result.add(parseData(maxData,minData,tbsWenshidu,"param2","温度","time","℃",sensorNo,"temp"));
                break;
            //泥位tbs_niweiji
            case 15:
                List<Map<String, Object>>  tbsNiweiji = appTabDeviceMapper.tbsNiweiji(vo);
                //getParam(tbsNiweiji,map,"value");
                result.add(parseData(maxData,minData,tbsNiweiji,"param1","监测值","time",unit,sensorNo,"value"));
                break;
            //振动tbs_zhendong
            case 16:
                List<Map<String, Object>>  tbsZhendong = appTabDeviceMapper.tbsZhendong(vo);
                //getParam(tbsZhendong,map,"zf","pl");
                result.add(parseData(maxData,minData,tbsZhendong,"param1","振幅","time",unit,sensorNo,"zf"));
                result.add(parseData(maxData,minData,tbsZhendong,"param2","频率","time",unit,sensorNo,"pl"));
                break;
            //地表位移(拉线式)tbs_lxdbd
            case 17:
                List<Map<String, Object>>  tbsLxdbd = appTabDeviceMapper.tbsLxdbd(vo);
                //getParam(tbsLxdbd,map,"value");
                result.add(parseData(maxData,minData,tbsLxdbd,"param1","累计位移","time",unit,sensorNo,"ljwy"));
                break;
            //地下水(地下水位)tbs_dxsw
            case 18:
                List<Map<String, Object>>  tbsDxsw = appTabDeviceMapper.tbsDxsw(vo);
               // getParam(tbsDxsw,map,"value");
                result.add(parseData(maxData,minData,tbsDxsw,"param1","水位高程","time",unit,sensorNo,"value"));
                break;
            //全站仪tbs_total_station
            case 19:
                List<Map<String, Object>>  tbsTotalStation = appTabDeviceMapper.tbsTotalStation(vo);
                //getParam(tbsTotalStation,map,"x","y","z");
                result.add(parseData(maxData,minData,tbsTotalStation,"param1","x原始值","time",unit,sensorNo,"x"));
                result.add(parseData(maxData,minData,tbsTotalStation,"param2","y原始值","time",unit,sensorNo,"y"));
                result.add(parseData(maxData,minData,tbsTotalStation,"param3","高程原始值","time",unit,sensorNo,"h"));
                break;
            //人工水准仪tbs_rgsz
            case 20:
                List<Map<String, Object>>  tbsRgsz = appTabDeviceMapper.tbsRgsz(vo);
                //getParam(tbsRgsz,map,"value");
                result.add(parseData(maxData,minData,tbsRgsz,"param1","高程","time",unit,sensorNo,"czgc"));
                break;
            //倾斜仪(WMT)tbs_qxy
            case 21:
                List<Map<String, Object>>  tbsQxy = appTabDeviceMapper.tbsQxy(vo);
                //getParam(tbsQxy,map,"one","two","three");
                result.add(parseData(maxData,minData,tbsQxy,"param1","一号合成位移","time",unit,sensorNo,"one"));
                result.add(parseData(maxData,minData,tbsQxy,"param2","二号合成位移","time",unit,sensorNo,"two"));
                result.add(parseData(maxData,minData,tbsQxy,"param3","三号合成位移","time",unit,sensorNo,"three"));
                break;
            //深部位移(拉线)tbs_sbwy_lx
            case 22:
                List<Map<String, Object>>  tbsSbwyLx = appTabDeviceMapper.tbsSbwyLx(vo);
                //getParam(tbsSbwyLx,map,"value");
                result.add(parseData(maxData,minData,tbsSbwyLx,"param1","位移","time",unit,sensorNo,"value"));
                break;
            // 深部位移(测斜)tbs_sbwy_cx
            case 23:
                List<Map<String, Object>>  tbsSbwyCx = appTabDeviceMapper.tbsSbwyCx(vo);
                //getParam(tbsSbwyCx,map,"x","y");
                result.add(parseData(maxData,minData,tbsSbwyCx,"param1","x位移","time",unit,sensorNo,"x"));
                result.add(parseData(maxData,minData,tbsSbwyCx,"param2","y位移","time",unit,sensorNo,"y"));
                break;
            //三项测缝计tbs_sxcfj
            case 24:
                List<Map<String, Object>>  tbsSxcfj = appTabDeviceMapper.tbsSxcfj(vo);
                //getParam(tbsSxcfj,map,"value");
                result.add(parseData(maxData,minData,tbsSxcfj,"param1","位移","time",unit,sensorNo,"value"));
                break;
            //倾角仪tbs_qjy
            case 25:
                List<Map<String, Object>>  tbsQjy = appTabDeviceMapper.tbsQjy(vo);
                //getParam(tbsQjy,map,"x","y","z");
                result.add(parseData(maxData,minData,tbsQjy,"param1","一号合成位移","time",unit,sensorNo,"x"));
                result.add(parseData(maxData,minData,tbsQjy,"param2","二号合成位移","time",unit,sensorNo,"y"));
                result.add(parseData(maxData,minData,tbsQjy,"param3","三号合成位移","time",unit,sensorNo,"z"));
                break;
            // 振弦式应力计tbs_zxsylj
            case 26:
                List<Map<String, Object>>  tbsZxsylj = appTabDeviceMapper.tbsZxsylj(vo);
                //getParam(tbsZxsylj,map,"value");
                result.add(parseData(maxData,minData,tbsZxsylj,"param1","位移","time",unit,sensorNo,"value"));
                break;
            //地声tbs_disheng
            case 27:
                List<Map<String, Object>>  tbsDisheng = appTabDeviceMapper.tbsDisheng(vo);
                //getParamSpecial(tbsDisheng,map,"x","y","z");
                result.add(parseData(maxData,minData,tbsDisheng,"param1","x值","time",unit,sensorNo,"x"));
                result.add(parseData(maxData,minData,tbsDisheng,"param2","y值","time",unit,sensorNo,"y"));
                result.add(parseData(maxData,minData,tbsDisheng,"param3","z值","time",unit,sensorNo,"z"));
                break;
            //次声tbs_cisheng
            case 28:
                List<Map<String, Object>>  tbsCisheng = appTabDeviceMapper.tbsCisheng(vo);
                //getParamSpecial(tbsCisheng, map,"value");
                result.add(parseData(maxData,minData,tbsCisheng,"param1","监测值","time",unit,sensorNo,"value"));
                break;
//            //地表位移（拉线式）tbs_lxdbd
//            case 29:
//                break;
            //物位计tbs_wwj
            case 30:
                List<Map<String, Object>>  tbsWwj = appTabDeviceMapper.tbsWwj(vo);
               // getParam(tbsWwj,map,"value");
                result.add(parseData(maxData,minData,tbsWwj,"param1","监测值","time",unit,sensorNo,"value"));
                break;
            //固定测斜tbs_gdqx
            case 31:
                List<Map<String, Object>>  tbsGdqx = appTabDeviceMapper.tbsGdqx(vo);
               // getParam(tbsGdqx,map,"value");
                result.add(parseData(maxData,minData,tbsGdqx,"param1","监测值","time",unit,sensorNo,"value"));
                break;
            //表面位移tbs_bmwy
            case 32:
                List<Map<String, Object>>  tbsBmwy = appTabDeviceMapper.tbsBmwy(vo);
               // getParam(tbsBmwy,map,"value");
                result.add(parseData(maxData,minData,tbsBmwy,"param1","监测值","time",unit,sensorNo,"value"));
                break;
            //流速仪tbs_lsy
            case 33:
                List<Map<String, Object>>  tbsLsy = appTabDeviceMapper.tbsLsy(vo);
               // getParam(tbsLsy,map,"value");
                result.add(parseData(maxData,minData,tbsLsy,"param1","监测值","time",unit,sensorNo,"value"));
                break;
            //自动激光测距仪
            case 34:
                break;
            //弦式表面应变计
            case 36:
                break;
            //测斜传感器
            case 37:
                break;
            //滑动变位计
            case 38:
                break;
            //裂缝倾角(地表裂缝监测仪/墙裂缝监测仪——倾角) tbs_liefeng_qj
            case 40:
                String lfX = "select time,sensor_no,value as param1 from tbs_liefeng_qj_x where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> lf_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(lfX));
                result.add(parseData(maxData,minData,lf_x,"param1","x轴角度","time",unit,sensorNo,"x_jd"));
                String lfY = "select time,sensor_no,value as param1 from tbs_liefeng_qj_y where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> lf_y = influxDBConnect.queryResultToListMap(influxDBConnect.query(lfY));
                result.add(parseData(maxData,minData,lf_y,"param1","y轴角度","time",unit,sensorNo,"y_jd"));
                String lfZ = "select time,sensor_no,value as param1 from tbs_liefeng_qj_z where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> lf_z = influxDBConnect.queryResultToListMap(influxDBConnect.query(lfZ));
                result.add(parseData(maxData,minData,lf_z,"param1","z轴角度","time",unit,sensorNo,"z_jd"));
                break;
            //地面倾斜监测仪tbs_dm_qxy
            case 41:
                String dmQxyX = "select time,sensor_no,value as param1 from tbs_dm_qxy_x where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> x = influxDBConnect.queryResultToListMap(influxDBConnect.query(dmQxyX));
                result.add(parseData(maxData,minData,x,"param1","x轴角度","time",unit,sensorNo,"x_jd"));
                String dmQxyY = "select time,sensor_no,value as param1 from tbs_dm_qxy_y where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> y = influxDBConnect.queryResultToListMap(influxDBConnect.query(dmQxyY));
                result.add(parseData(maxData,minData,y,"param1","y轴角度","time",unit,sensorNo,"y_jd"));
                String dmQxyZ = "select time,sensor_no,value as param1 from tbs_dm_qxy_z where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> z = influxDBConnect.queryResultToListMap(influxDBConnect.query(dmQxyZ));
                result.add(parseData(maxData,minData,z,"param1","z轴角度","time",unit,sensorNo,"z_jd"));
                break;
            //坡体浅层变形仪-位移 tbs_ptqcbxy
            case 42:
                String lxX = "select time,sensor_no,value as param1 from tbs_ptqcbxy_lx_x where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> lx_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(lxX));
                result.add(parseData(maxData,minData,lx_x,"param1","水平面内x方向(主滑方向)位移","time",unit,sensorNo,"x"));
                String lxY = "select time,sensor_no,value as param1 from tbs_ptqcbxy_lx_y where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> lx_y = influxDBConnect.queryResultToListMap(influxDBConnect.query(lxY));
                result.add(parseData(maxData,minData,lx_y,"param1","水平面内y方向(与主滑方向垂直)位移","time",unit,sensorNo,"y"));
                String lxZ = "select time,sensor_no,value as param1 from tbs_ptqcbxy_lx_z where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> lx_z = influxDBConnect.queryResultToListMap(influxDBConnect.query(lxZ));
                result.add(parseData(maxData,minData,lx_z,"param1","垂直方向沉降位移值","time",unit,sensorNo,"z"));
                break;
            //坡体浅层变形仪-倾角 tbs_ptqcbxy
            case 44:
                String qxX = "select time,sensor_no,value as param1 from tbs_ptqcbxy_wy_x where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> qx_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(qxX));
                result.add(parseData(maxData,minData,qx_x,"param1","水平面内X轴(主滑方向)与水平面夹角","time","°(度)",sensorNo,"x_jd"));
                String qxY = "select time,sensor_no,value as param1 from tbs_ptqcbxy_wy_y where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> qx_y = influxDBConnect.queryResultToListMap(influxDBConnect.query(qxY));
                result.add(parseData(maxData,minData,qx_y,"param1","水平面内Y轴(与主滑方向垂直)与水平面的倾斜夹角","time","°(度)",sensorNo,"y_jd"));
                String qxZ = "select time,sensor_no,value as param1 from tbs_ptqcbxy_wy_z where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> qx_z = influxDBConnect.queryResultToListMap(influxDBConnect.query(qxZ));
                result.add(parseData(maxData,minData,qx_z,"param1","水平面内Z轴与水平面的倾斜夹角","time","°(度)",sensorNo,"z_jd"));
                break;
            //裂缝拉线(地表裂缝监测仪/墙裂缝监测仪——拉线) tbs_liefeng_lx
            case 43:
                String liefengLx = "select time,sensor_no,value as param1 from tbs_liefeng_lx where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' "+orderStr;
                List<Map<String, Object>> lief_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(liefengLx));
                result.add(parseData(maxData,minData,lief_x,"param1","拉线","time",unit,sensorNo,"lx"));
                break;
        }
        return result;
    }
    /**
     * 功能描述：解析数据
     */
    private Map<String, Object> parseData(BigDecimal maxData,BigDecimal minData,List<Map<String, Object>> list, String itemKey, String itemName, String dataKey,String unit,String sensorNo,String filedName) {
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        Map<String,Object> initValueMap = tabSensorInitMapper.getDataByFiledName(sensorNo,filedName);
        for (Map<String, Object> mapList:list) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            BigDecimal initValue = BigDecimal.ZERO;
            if(null != initValueMap && null != initValueMap.get("init_value")){
                initValue = new BigDecimal(initValueMap.get("init_value").toString());
            }
            BigDecimal value = new BigDecimal(mapList.get(itemKey).toString()).subtract(initValue);
            maxData = maxData == null? value: maxData.max(value);
            minData = minData == null? value: minData.min(value);
            map.put("x", dateFormat(mapList.get(dataKey).toString()));
            map.put("y", value);
            resultList.add(map);
        }
//        list.forEach(item -> {
//            HashMap<String, Object> map = new HashMap<String, Object>();
//            BigDecimal initValue = BigDecimal.ZERO;
//            if(null != initValueMap && null != initValueMap.get("init_value")){
//                initValue = new BigDecimal(initValueMap.get("init_value").toString());
//            }
//            map.put("x", dateFormat(item.get(dataKey).toString()));
//            BigDecimal value = new BigDecimal(item.get(itemKey).toString()).subtract(initValue);
//            map.put("y", value);
//            resultList.add(map);
//        });
        resultMap.put("name", itemName);
        resultMap.put("type", itemKey);
        resultMap.put("data", resultList);
        resultMap.put("unit",unit);
        resultMap.put("maxData", maxData==null?BigDecimal.ZERO:maxData);
        resultMap.put("minData",minData==null?BigDecimal.ZERO:minData);
        return resultMap;
    }
    private  String dateFormat(String str) {
        return str.replace("Z", "").replace("T"," ");
    }

}
