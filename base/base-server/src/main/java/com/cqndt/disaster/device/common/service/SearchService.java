package com.cqndt.disaster.device.common.service;


import com.cqndt.disaster.device.vo.Column;

import java.util.List;

public interface SearchService {
    /**
     * 根据表名获取对应列信息
     *
     * @param tableName 数据表名
     * @return
     */
    List<Column> getColumns(String tableName);
}
