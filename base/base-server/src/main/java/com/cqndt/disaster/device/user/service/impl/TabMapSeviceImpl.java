package com.cqndt.disaster.device.user.service.impl;

import com.cqndt.disaster.device.dao.TabMapserviceMapper;
import com.cqndt.disaster.device.domain.TabMapservice;
import com.cqndt.disaster.device.user.service.TabMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created By marc
 * Date: 2019/4/18  17:10
 * Description:
 */
@Service
public class TabMapSeviceImpl implements TabMapService {
    @Autowired
    private TabMapserviceMapper tabMapserviceMapper;
    @Override
    public TabMapservice getByUserId(Integer userId) {
        return tabMapserviceMapper.getByUserId(userId);
    }
}
