package com.cqndt.disaster.device.user.service;
import java.util.List;
import java.util.Map;

public interface SysMenuService {
    /**
     * 获取导航菜单
     * @param userId
     * @param frontBack
     * @return
     */
    List<Map<String, Object>> navigationList(String userId, String frontBack);
}