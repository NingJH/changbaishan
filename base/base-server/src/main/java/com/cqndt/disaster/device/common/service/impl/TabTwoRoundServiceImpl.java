package com.cqndt.disaster.device.common.service.impl;

import com.cqndt.disaster.device.domain.TabTwoRound;
import com.cqndt.disaster.device.dao.TabTwoRoundMapper;
import com.cqndt.disaster.device.common.service.TabTwoRoundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/12  15:43
 * Description:获取全景图
 */
@Service
public class TabTwoRoundServiceImpl implements TabTwoRoundService {
    @Autowired
    private TabTwoRoundMapper tabTwoRoundMapper;
    @Value("${file-ip}")
    private String fileIp;

    @Override
    public List<TabTwoRound> selectByRelationNo(String relationNo,String modelType){
        List<TabTwoRound> list = tabTwoRoundMapper.selectByRelationNo(relationNo,modelType);
        for (TabTwoRound round:list) {
            round.setUrl(fileIp+ File.separator+round.getUrl());
        }
        return list;
    }
}
