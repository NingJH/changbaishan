package com.cqndt.disaster.device.common.service.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * properties工具类
 */
public class PropertyUtil {
    /**
     * 加载classPath下的properties文件
     * @param properties
     * @return
     */
    public static Properties loadProperties(String properties){
        Properties props = new Properties();
        InputStream in = null;
        try {
            in = PropertyUtil.class.getClassLoader().getResourceAsStream("translate.properties");
            //in = PropertyUtil.class.getResourceAsStream(properties);
            props.load(in);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(null != in) {
                    in.close();
                }
            } catch (IOException e) {
               e.printStackTrace();
            }
        }
        return props;
    }
}
