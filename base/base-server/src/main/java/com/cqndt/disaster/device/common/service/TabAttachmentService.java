package com.cqndt.disaster.device.common.service;

import com.cqndt.disaster.device.domain.TabAttachment;
import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/12  15:20
 * Description:获取附件
 */
public interface TabAttachmentService {
    /**
     * 获取相应的附件
     * @param imgIds
     * @return
     */
    List<TabAttachment> selectAttachments(String imgIds);
}
