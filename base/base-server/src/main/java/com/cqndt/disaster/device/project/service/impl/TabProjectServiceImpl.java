package com.cqndt.disaster.device.project.service.impl;

import com.cqndt.disaster.device.dao.*;
import com.cqndt.disaster.device.domain.*;
import com.cqndt.disaster.device.project.service.TabProjectService;
import com.cqndt.disaster.device.vo.TabProjectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/12  10:15
 * Description:
 */
@Service
public class TabProjectServiceImpl implements TabProjectService {
    @Autowired
    private TabProjectMapper tabProjectMapper;
    @Autowired
    private TabProjectXmbzMapper tabProjectXmbzMapper;
    @Autowired
    private TabProjectCjdwMapper tabProjectCjdwMapper;
    @Autowired
    private TabProjectDatumMapper tabProjectDatumMapper;
    @Autowired
    private TabMonitorMapper tabMonitorMapper;
    @Autowired
    private TabStaticTypeMapper tabStaticTypeMapper;

    @Value("${file-ip}")
    private String fileIp;

    @Override
    public TabProject getById(Integer projectId) {
        return tabProjectMapper.selectByPrimaryKey(projectId);
    }

    @Override
    public List<TabProjectVo> selectByCondition(TabProjectVo vo){
        return tabProjectMapper.selectByCondition(vo);
    }
    @Override
    public List<Map<String, Object>> selectCjdwByProjectId(Integer projectId){
        return tabProjectCjdwMapper.getProjectUnitList(projectId);
    }
    @Override
    public List<Map<String, Object>> selectXmbzByProjectId(Integer projectId){
        return tabProjectXmbzMapper.selectXmbzByProjectId(projectId);
    }
    @Override
    public List<Map<String, Object>> selectProjectDatum(Integer projectId) {
        return  getAllDatumList(projectId);
    }
    /**
     * 获取所有文档
     */
    private List<Map<String,Object>> getAllDatumList(Integer projectId){
        //查询根文档
        List<Map<String,Object>> datumList = tabProjectDatumMapper.selectDatumForProject(0,projectId);
        if (datumList.size() > 0) {
            for (Map<String, Object> map : datumList) {
                map.put("pdf",fileIp+File.separator+"file"+File.separator+ map.get("pdf"));
                //System.out.println(map.get("pdf"));
            }

        }
        //递归获取子菜单
        getDatumTreeList(datumList,projectId);
        return datumList;
    }
    /**
     * 递归
     */
    private List<Map<String,Object>> getDatumTreeList(List<Map<String,Object>> datumList,Integer projectId){
        List<Map<String,Object>> subMenuList = new ArrayList<Map<String,Object>>();
        for(Map<String,Object> map : datumList){
            //目录
            if("1".equals(map.get("file_type"))){
                List<Map<String,Object>> list = tabProjectDatumMapper.selectDatumForProject((Integer)map.get("id"),projectId);
                for (Map<String, Object> map1 : list) {
                    map1.put("pdf",fileIp+File.separator+"file"+File.separator+ map1.get("pdf"));
                }
                map.put("children",getDatumTreeList(list,projectId));
                //System.out.println("------------"+map.get("pdf"));
            }else{
                map.put("children",new ArrayList<Map<String,Object>>());
                //System.out.println("!!!!!!!!!!!!!"+map.get("pdf"));
            }
            subMenuList.add(map);
        }
        return subMenuList;
    }
    @Override
    public List<Map<String,Object>> selectByUserId(Integer userId){
        return tabProjectMapper.selectByUserId(userId);
    }

    @Override
    public List<Map<String, Object>> getDeviceByProjectId(Integer userId,Integer projectId) {
        List<TabStaticType> list = tabStaticTypeMapper.selectByStaticNum("11");
        //离线时间判断
        Integer num = list.get(0).getStaticKeyval();
        return tabProjectMapper.getDeviceByProjectId(userId,projectId,num);
    }

    @Override
    public Map<String, Object> getDeviceNumByProjectId(Integer userId,Integer projectId) {
        List<TabStaticType> list = tabStaticTypeMapper.selectByStaticNum("11");
        //离线时间判断
        Integer num = list.get(0).getStaticKeyval();
        return tabProjectMapper.getDeviceNumByProjectId(userId,projectId,num);
    }

    @Override
    public Map<String, Object> getTabProject(String deviceNo) {
        return tabProjectMapper.getTabProject(deviceNo);
    }
}
