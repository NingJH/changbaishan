package com.cqndt.disaster.device.user.service.impl;

import com.cqndt.disaster.device.dao.SysMenuMapper;
import com.cqndt.disaster.device.user.service.SysMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class SysMenuServiceImpl implements SysMenuService {
    private static final Logger logger = LoggerFactory.getLogger(SysMenuServiceImpl.class);
    @Autowired
    private SysMenuMapper sysMenuMapper;
    @Override
    public List<Map<String, Object>> navigationList(String userId, String frontBack) {
        //超级管理员
//        if(userId.equals(Constant.SUPER_ADMIN.toString())){
//            return getAllMenuList(null,frontBack);
//        }
        return getAllMenuList(userId,frontBack);
    }
    /**
     * 获取所有菜单列表
     */
    private List<Map<String,Object>> getAllMenuList(String userId, String frontBack){
        //查询根菜单列表
        List<Map<String,Object>> menuList;
        if(null == userId){
            menuList = sysMenuMapper.queryMenuForAdmin(frontBack,"0");
        }else{
            menuList = sysMenuMapper.queryMenuForUser(userId,frontBack,"0");
        }
        //递归获取子菜单
        getMenuTreeList(menuList,frontBack,userId);
        return menuList;
    }
    /**
     * 递归
     */
    private List<Map<String,Object>> getMenuTreeList(List<Map<String,Object>> menuList,String frontBack,String userId){
        List<Map<String,Object>> subMenuList = new ArrayList<Map<String,Object>>();
        for(Map<String,Object> map : menuList){
            //目录
            if("0".equals(map.get("hassub"))||0 == Integer.valueOf(map.get("hassub").toString())){
                List<Map<String,Object>> list;
                if(null == userId){
                    list = sysMenuMapper.queryMenuForAdmin(frontBack,map.get("id").toString());
                }else{
                    list = sysMenuMapper.queryMenuForUser(userId,frontBack,map.get("id").toString());
                }
                map.put("children",getMenuTreeList(list,frontBack,userId));
                map.put("disAbled",true);
            }else{
                map.put("children",new ArrayList<Map<String,Object>>());
            }
            subMenuList.add(map);
        }
        return subMenuList;
    }

}