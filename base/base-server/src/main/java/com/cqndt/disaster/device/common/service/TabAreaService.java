package com.cqndt.disaster.device.common.service;

import com.cqndt.disaster.device.domain.TabArea;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/15  12:09
 * Description:
 */
public interface TabAreaService {
    /**
     * 获取下级区域
     * @param areaCode
     * @return
     */
    List<TabArea> selectByAreaCode(String areaCode);
}
