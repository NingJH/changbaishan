package com.cqndt.disaster.device.common.service.impl;

import com.cqndt.disaster.device.common.service.DataStatisticsService;
import com.cqndt.disaster.device.common.shrio.ShiroUtils;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.dao.*;
import com.cqndt.disaster.device.domain.TabNPerson;
import com.cqndt.disaster.device.domain.TabStaticType;
import com.cqndt.disaster.device.vo.TabUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/13  10:14
 * Description:数据统计
 */
@Service
public class DataStatisticsServiceImpl implements DataStatisticsService {
    @Autowired
    private TabBasicMapper tabBasicMapper;
    @Autowired
    private TabProjectMapper tabProjectMapper;
    @Autowired
    private TabMonitorMapper tabMonitorMapper;
    @Autowired
    private TabDeviceMapper tabDeviceMapper;
    @Autowired
    private TabSensorMapper tabSensorMapper;
    @Autowired
    private TabStaticTypeMapper tabStaticTypeMapper;
    @Autowired
    private TabVideoMonitoringMapper tabVideoMonitoringMapper;

    private static final Integer page_start = 1;

    private static final Integer size_start = 10;

    /**
     * 基础数据统计
     *
     * @param vo 用户信息
     * @return
     */
    @Override
    public Map<String, Object> countBaseNum(TabUserVo vo) {
        Map<String, Object> map = new HashMap<>();
        String areaCodeSplit = vo.getAreaCode().substring(0, 2);
        //灾害点总数
        //Integer basicNum = tabBasicMapper.countDisasterNumber(vo.getAreaCode(), vo.getLevel(), areaCodeSplit);
        Integer basicNum = tabBasicMapper.countDisasterNumberByUserId(vo.getId().toString());
        //项目总数
        Integer projectNum = tabProjectMapper.countProjectNumber(vo.getId());
        //安装布置点总数
        Integer monitorNum = tabMonitorMapper.countMonitorNum(vo.getId());
        //监测设备总数
        Integer deviceNum = tabDeviceMapper.countDeviceNum(vo.getId());
        //传感器总数
        Integer sensorNum = tabSensorMapper.countSensorNum(vo.getId());
        //摄像头总数
        Integer videoNum = tabVideoMonitoringMapper.countVideoNum(ShiroUtils.getUserId());

        map.put("basicNum", basicNum);
        map.put("projectNum", projectNum);
        map.put("monitorNum", monitorNum);
        map.put("deviceNum", deviceNum);
        map.put("sensorNum", sensorNum);
        map.put("videoNum", videoNum);
        return map;
    }

    /**
     * 监测类型统计
     *
     * @param userId
     * @return
     */
    @Override
    public List<Map<String, Object>> countMonitorTypeNum(Integer userId) {
        return tabDeviceMapper.countMonitorTypeNum(userId);
    }

    /**
     * 灾害点类型统计
     *
     * @param vo
     * @return
     */
    @Override
    public List<Map<String, Object>> countBasicTypeNum(TabUserVo vo) {
//        String areaCodeSplit = vo.getAreaCode().substring(0, 2);

        return tabBasicMapper.countDisasterTypeNumber(ShiroUtils.getUserId());
    }

    /**
     * 仪器状态统计（1.1日无数据 2.2日无数据 3.3日无数据 4.4日及以上无数据 5.正常）
     *
     * @param userId
     * @return
     */
    @Override
    public Map<String, Object> countInstrumentNum(Integer userId) {
        List<TabStaticType> list = tabStaticTypeMapper.selectByStaticNum("11");
        if(list.isEmpty()){
            return null;
        }
        //离线时间判断
        Integer num = list.get(0).getStaticKeyval();
        Map<String, Object> map = tabDeviceMapper.countInstrumentNum(userId, num);
        return map;
    }

    /**
     * 设备在线离线统计
     *
     * @param userId
     * @return
     */
    @Override
    public List<Map<String, Object>> countDeviceStateNum(Integer userId) {
        List<Map<String, Object>> resultList = new ArrayList<>();

        List<TabStaticType> list = tabStaticTypeMapper.selectByStaticNum("11");
        if(list.isEmpty()){
            return null;
        }
        //离线时间判断
        Integer num = list.get(0).getStaticKeyval();
        //查询用户对应的所有设备类型
        List<Map<String, Object>> listType = tabDeviceMapper.selectAllType(userId);
        for (Map<String, Object> map : listType) {
            Map<String, Object> resultMap = new HashMap<>();
            Integer zx = tabDeviceMapper.countDeviceStateZx(userId, map.get("device_type").toString(), num);
            Integer lx = tabDeviceMapper.countDeviceStateLx(userId, map.get("device_type").toString(), num);
            resultMap.put("zx", zx);
            resultMap.put("lx", lx);
            resultMap.put("deviceName", map.get("static_name"));
            resultList.add(resultMap);
        }
        return resultList;
    }

    @Override
    public List<Map<String, Object>> countDeviceAlarmNum(Integer userId) {
        return tabDeviceMapper.countDeviceAlarmNum(userId);
    }

    @Override
    public Map<String, Object> countAllDeviceStateOnline(Integer userId) {


        Map<String, Object> map1 = new HashMap<>();

        //获取静态离线时间阈值
        List<TabStaticType> list = tabStaticTypeMapper.selectByStaticNum("11");
        Integer num = list.get(0).getStaticKeyval();

        //获取该用户下设备总数
        Integer allDevice = tabDeviceMapper.selectAllDevice(userId);

        //设备在线总数
        int zaiX = 0;
        //设备离线总数
        int liX = 0;

        //查询用户对应的所有设备类型
        List<Map<String, Object>> listType = tabDeviceMapper.selectAllType(userId);
        //List<Map<String, Object>> listDevice = tabDeviceMapper.selectAllDevice();

        System.out.println(zaiX);
        for (Map<String, Object> map : listType) {
            Integer zx = tabDeviceMapper.countDeviceStateZx(userId, map.get("device_type").toString(), num);
            Integer lx = tabDeviceMapper.countDeviceStateLx(userId, map.get("device_type").toString(), num);
            zaiX = zaiX + zx;


        }
        liX = allDevice - zaiX;

        // 创建一个数值格式化对象
        NumberFormat numberFormat = NumberFormat.getInstance();
        // 设置精确到小数点后2位
        numberFormat.setMaximumFractionDigits(0);
        String zxl = numberFormat.format((float) zaiX / (float) allDevice * 100);
        String result1 = zxl + "%";

        String lxl = numberFormat.format((float) liX / (float) allDevice * 100);
        String result2 = lxl + "%";

        map1.put("total", allDevice);
        map1.put("online", zaiX);
        map1.put("offline", liX);
        map1.put("onlineRate", result1);
        map1.put("offlineRate", result2);

        return map1;
    }




    /**
     * 新版设备在线离线统计
     *
     * @param userId
     * @return
     */
    @Override
    public Result countDeviceStateNum1(Integer userId, Integer page, Integer size) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        List<Map<String, Object>> pageList = new ArrayList<>(16);
        Result result = new Result();
        List<TabStaticType> list = tabStaticTypeMapper.selectByStaticNum("11");
        //离线时间判断
        Integer num = list.get(0).getStaticKeyval();
        //查询用户对应的所有设备类型
        List<Map<String, Object>> listType = tabDeviceMapper.selectAllType(userId);
        //TODO   各设备类型总条数
        int count = 0;
        for (Map<String, Object> map : listType) {
            Map<String, Object> resultMap = new HashMap<>();
            Integer zx = tabDeviceMapper.countDeviceStateZx(userId, map.get("device_type").toString(), num);
            Integer lx = tabDeviceMapper.countDeviceStateLx(userId, map.get("device_type").toString(), num);
            resultMap.put("zx", zx);
            resultMap.put("lx", lx);
            count=zx+lx;
            resultMap.put("totalCount",count);
            resultMap.put("deviceName", map.get("static_name"));
            resultList.add(resultMap);
        }
        int i = (page - 1) * size;
        int limit = size * page - 1;

        int counT=i;
        for (int i1 = i; i1 <= limit; i1++) {
            if (i1 == counT && i1 <= resultList.size()-1){
                pageList.add(resultList.get(i1));
            }
            counT++;
        }
        result.setData(pageList);
        result.setCount(resultList.size());
        result.setCode(200);
        result.setMsg("success");
        //TODO   返回Result
        return result;
    }

    @Override
    public List<Map<String, Object>> equipmentCondition(Integer userId) {
        List<Map<String, Object>> equipmentCondition = tabDeviceMapper.equipmentCondition(userId);
        equipmentCondition.forEach(e->{
            e.put("dyz",e.get("dyz")==null?"0":e.get("dyz").toString()+"v");
            e.put("device_sn",e.get("device_sn")==null?"0":e.get("device_sn").toString()+"dBm");
        });
        return equipmentCondition;
    }

    @Override
    public Map<String, Object> statistics(Integer userId) {
        Map<String,Object> map = new HashMap<>();
        int countBastic = tabDeviceMapper.countBastic();//灾害点数量
        int countMonitor = tabDeviceMapper.countMonitor(userId);//监测点数量
        int countDevice = tabDeviceMapper.countDevice(userId);//设备数量
        int countPersion1 = tabDeviceMapper.countPersion(1);//群测群防人员数量
        int countPersion2 = tabDeviceMapper.countPersion(2);//片区负责人数量
        int countPersion3 = tabDeviceMapper.countPersion(3);//驻守人员数量
        int countPersion4 = tabDeviceMapper.countPersion(4);//地环站人员数量
        map.put("countBastic",countBastic);
        map.put("countMonitor",countMonitor);
        map.put("countDevice",countDevice);
        map.put("countPersion1",countPersion1);
        map.put("countPersion2",countPersion2);
        map.put("countPersion3",countPersion3);
        map.put("countPersion4",countPersion4);
        return map;
    }

    @Override
    public List<Map<String, Object>> warnMsgList(String startTime, String endTime,Integer userId) {
        List<Map<String,Object>> warnInfoList = tabDeviceMapper.warnMsgList(startTime,endTime,userId);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        warnInfoList.forEach(warnInfo->{
            warnInfo.put("sendTime",sdf.format(warnInfo.get("sendTime")));
            switch (warnInfo.get("warnLevel").toString()){
                case "1":
                    warnInfo.put("warnName","一级告警");
                    break;
                case "2":
                    warnInfo.put("warnName","二级告警");
                    break;
                case "3":
                    warnInfo.put("warnName","三级告警");
                    break;
                case "4":
                    warnInfo.put("warnName","四级告警");
                    break;
            }
        });
        return warnInfoList;
    }

    @Override
    public Map<String, Object> meteorological() {
        return tabDeviceMapper.meteorological();
    }

    @Override
    public List<Map<String, Object>> warnMsgInfo(String deviceNo) {
        List<Map<String, Object>> warnMsgInfo = tabDeviceMapper.warnMsgInfo(deviceNo);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        warnMsgInfo.forEach(warnInfo-> {
            warnInfo.put("send_time", sdf.format(warnInfo.get("send_time")));
        });
        return warnMsgInfo;
    }

    @Override
    public Map<String, Object> warnMsgDetails(int id) {
        Map<String, Object> warnMsgDetails = tabDeviceMapper.warnMsgDetails(id);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        warnMsgDetails.put("sendTime", sdf.format(warnMsgDetails.get("sendTime")));
        return warnMsgDetails;
    }

    @Override
    public Map<String, Object> warnMsgDevice(String deviceNo) {
        return tabDeviceMapper.warnMsgDevice(deviceNo);
    }

    @Override
    public Map<String, Object> warnMsgListCount() {
        Map<String,Object> map = new HashMap<>();
        int red = tabDeviceMapper.warnMsgListCount(1);
        int orange = tabDeviceMapper.warnMsgListCount(2);
        int yellow = tabDeviceMapper.warnMsgListCount(3);
        int blue = tabDeviceMapper.warnMsgListCount(4);
        map.put("red",red);
        map.put("orange",orange);
        map.put("yellow",yellow);
        map.put("blue",blue);
        return map;
    }

    @Override
    public List<Map<String, Object>> getNPerson(int pType,String name,String wPhone) {
        List<Map<String, Object>> nPerson = tabDeviceMapper.getNPerson(pType,name,wPhone);
        nPerson.forEach(t->{
            switch (t.get("pType").toString()){
                case "1":
                    t.put("typeName","群测群防");
                    break;
                case "2":
                    t.put("typeName","片区负责人");
                    break;
                case "3":
                    t.put("typeName","驻守人员");
                    break;
                case "4":
                    t.put("typeName","地环站人员");
                    break;
            }
        });
        return nPerson;
    }

    @Override
    public List<Map<String, Object>> getPointNPerson(int pType,String name,String wPhone) {
        List<Map<String, Object>> pointNPersonList = tabDeviceMapper.getNPerson(pType,name,wPhone);
        pointNPersonList.forEach(pointNPerson->{
            String[] coordinates = pointNPerson.get("coordinates").toString().split(",");
            pointNPerson.put("longitude",coordinates[0]);
            pointNPerson.put("latitude",coordinates[1]);
        });
        return pointNPersonList;
    }

    @Override
    public Map<String,Object> getNPersonInfo(int id) {
        Map<String,Object> nPersonInfo = tabDeviceMapper.getNPersonInfo(id);
        if(nPersonInfo!=null){
            nPersonInfo.put("longitude",nPersonInfo.get("coordinates").toString().split(",")[0]);
            nPersonInfo.put("latitude",nPersonInfo.get("coordinates").toString().split(",")[1]);
            nPersonInfo.put("tabNPersonBasic",tabBasicMapper.getBasicByNPerson(id));
        }
        return nPersonInfo;
    }

    @Override
    public List<Map<String, Object>> getBasicNperson(String disNo) {
        List<Map<String, Object>> basicNperson = tabBasicMapper.getBasicNperson(disNo);
        basicNperson.forEach(b->{
            switch (b.get("pType").toString()){
                case "1":
                    b.put("typeName","群测群防");
                    break;
                case "2":
                    b.put("typeName","片区负责人");
                    break;
                case "3":
                    b.put("typeName","驻守人员");
                    break;
                case "4":
                    b.put("typeName","地环站人员");
                    break;
            }
            b.put("longitude",b.get("coordinates").toString().split(",")[0]);
            b.put("latitude",b.get("coordinates").toString().split(",")[1]);
        });
        return basicNperson;
    }


}
