package com.cqndt.disaster.device.user.service;

import com.cqndt.disaster.device.domain.TabLoginLog;

/**
 * Created By marc
 * Date: 2019/5/20  15:48
 * Description:
 */
public interface TabLoginService {

    void insert(TabLoginLog log);
}
