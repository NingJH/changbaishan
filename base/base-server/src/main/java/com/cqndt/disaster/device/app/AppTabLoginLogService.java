package com.cqndt.disaster.device.app;

import com.cqndt.disaster.device.domain.TabLoginLog;

/**
 * Created By marc
 * Date: 2019/5/20  14:34
 * Description:
 */
public interface AppTabLoginLogService {

    void insert(TabLoginLog log);

    TabLoginLog getByUserNameAndPlat(String userName,Integer platType);

}
