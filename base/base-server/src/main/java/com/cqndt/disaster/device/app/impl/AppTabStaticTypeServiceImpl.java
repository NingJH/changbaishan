package com.cqndt.disaster.device.app.impl;

import com.cqndt.disaster.device.app.AppTabStaticTypeService;
import com.cqndt.disaster.device.dao.app.AppTabStaticTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/12  14:50
 * Description:
 */
@Service
public class AppTabStaticTypeServiceImpl implements AppTabStaticTypeService {
    @Autowired
    private AppTabStaticTypeMapper appTabStaticTypeMapper;

    @Override
    public List<Map<String, Object>> selectByStaticNum(String staticNum) {
        return appTabStaticTypeMapper.selectByStaticNum(staticNum);
    }
}
