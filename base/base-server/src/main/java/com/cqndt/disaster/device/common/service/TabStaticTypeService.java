package com.cqndt.disaster.device.common.service;

import com.cqndt.disaster.device.domain.TabStaticType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/12  9:33
 * Description:
 */
public interface TabStaticTypeService {
    /**
     * 根据静态值类型获取静态值
     * @param staticNum
     * @return
     */
    List<TabStaticType> selectByStaticNum(String staticNum);

    List<TabStaticType> selectByStaticNum1(String staticNum);
}
