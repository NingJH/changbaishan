package com.cqndt.disaster.device.user.service;

import com.cqndt.disaster.device.domain.TabMapservice;

/**
 * Created By marc
 * Date: 2019/4/18  17:09
 * Description:
 */
public interface TabMapService {

    TabMapservice getByUserId(Integer userId);
}
