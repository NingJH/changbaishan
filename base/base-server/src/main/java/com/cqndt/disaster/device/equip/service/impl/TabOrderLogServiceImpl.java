package com.cqndt.disaster.device.equip.service.impl;

import com.cqndt.disaster.device.equip.service.TabOrderLogService;
import com.cqndt.disaster.device.dao.TabOrderLogMapper;
import com.cqndt.disaster.device.domain.TabOrderLog;
import com.cqndt.disaster.device.vo.TabOrderLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/5/7  20:36
 * Description:
 */
@Service
public class TabOrderLogServiceImpl implements TabOrderLogService {
    @Autowired
    private TabOrderLogMapper tabOrderLogMapper;

    @Override
    public void save(TabOrderLog tabOrderLog) {
        tabOrderLogMapper.insertSelective(tabOrderLog);
    }

    @Override
    public List<TabOrderLogVo> listOrderLog(TabOrderLogVo vo) {
        return tabOrderLogMapper.listOrderLog(vo);
    }
}
