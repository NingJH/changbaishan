package com.cqndt.disaster.device.map.Impl;

import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.dao.TabMapMapper;
import com.cqndt.disaster.device.domain.TabMap;
import com.cqndt.disaster.device.map.TabMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TabMapServiceImpl implements TabMapService {

    @Autowired
    private TabMapMapper tabMapMapper;

    @Override
    public Result getAllMap() {
        List<TabMap> allMap = tabMapMapper.getAllMap();

        Result result=new Result();
        result.setData(allMap);
        result.setMsg("查询成功");
        result.setCode(0);
        return result;
    }
}
