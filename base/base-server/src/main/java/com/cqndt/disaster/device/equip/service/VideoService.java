/**
 * 
 */
package com.cqndt.disaster.device.equip.service;

import org.apache.http.client.ClientProtocolException;

import java.io.IOException;

/**
 * 视频监控云台控制
 * @author lhy
 * @date 2017年10月26日
 *
 */
public interface VideoService {
	
	public static final String APPKY = "89e3f593ab564d1f8b0bfd3ab768d725";
	public static final String SECRET = "c1fab69fc065f8e2715479086806091e";
	public static String ACCESS_TOKEN = "https://open.ys7.com/api/lapp/token/get";
	public static String ADD_DEVICE = "https://open.ys7.com/api/lapp/device/add";
	public static String DELETE_DEVICE = "https://open.ys7.com/api/lapp/device/delete";
	public static String UPDATE_NAME = "https://open.ys7.com/api/lapp/device/name/update";
	public static String CAPTURE = "https://open.ys7.com/api/lapp/device/capture";
	public static String DEVICE_LIST = "https://open.ys7.com/api/lapp/device/list";
	public static String START_PTZ = "https://open.ys7.com/api/lapp/device/ptz/start";
	public static String STOP_PTZ = "https://open.ys7.com/api/lapp/device/ptz/stop";
	
	/**
	 * 根据appKey和secret获取accessToken
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException
	 */
	String getAccessToken() throws ClientProtocolException, IOException;
	/**
	 * 根据appKey和secret获取accessToken
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException
	 */
	String getAccessTokenByParam(int deviceId) throws ClientProtocolException, IOException;
	/**
	 * 添加设备到账号下{@link http://open.ys7.com/doc/zh/book/index/device_option.html#device_option-api1}
	 * @param deviceSerial 设备序列号
	 * @param validateCode 设备验证码，设备机身上的六位大写字母
	 * @return
	 */
	String addDevice(String deviceSerial, String validateCode) throws ClientProtocolException, IOException;;
	
	/**
	 * 删除设备 {@link http://open.ys7.com/doc/zh/book/index/device_option.html#device_option-api2}
	 * @param deviceSerial 设备序列号
	 * @return
	 */
	String deleteDevice(String deviceSerial) throws ClientProtocolException, IOException;;
	
	/**
	 * 修改设备名称  {@link http://open.ys7.com/doc/zh/book/index/device_option.html#device_option-api3}
	 * @param deviceSerial 设备序列号
	 * @param deviceName 修改后使用的名称
	 * @return
	 */
	String updateName(String deviceSerial, String deviceName) throws ClientProtocolException, IOException;;
	
	/**
	 * 设备拍照 {@link http://open.ys7.com/doc/zh/book/index/device_option.html#device_option-api4}
	 * @param deviceSerial 设备序列号
	 * @param channelNo 通道号，IPC设备填写1
	 * @return 照片路径
	 */
	String capture(int deviceId, String deviceSerial, int channelNo) throws ClientProtocolException, IOException;;
	
	/**
	 * 获取设备列表 {@link http://open.ys7.com/doc/zh/book/index/device_select.html#device_select-api1}
	 * @param pageStart 分页起始页，从0开始
	 * @param pageSize 分页大小，默认为10，最大为50
	 * @return
	 */
	String deviceList(int pageStart, int pageSize) throws ClientProtocolException, IOException;;
	
	/**
	 * 对设备进行开始云台控制，开始云台控制之后必须先调用停止云台控制接口才能进行其他操作，包括其他方向的云台转动
	 * {@link http://open.ys7.com/doc/zh/book/index/device_ptz.html#device_ptz-api1}
	 * @param deviceSerial 设备序列号
	 * @param channelNo 通道号 
	 * @param direction 操作命令：0-上，1-下，2-左，3-右，4-左上，5-左下，6-右上，7-右下，8-放大，9-缩小，10-近焦距，11-远焦距
	 * @param speed 云台速度：0-慢，1-适中，2-快
	 * @return
	 */
	String startPtz(int deviceId, String deviceSerial, int channelNo, int direction, int speed) throws ClientProtocolException, IOException;;
	
	/**
	 * 停止云台 {@link http://open.ys7.com/doc/zh/book/index/device_ptz.html#device_ptz-api2}
	 * @param deviceSerial 设备序列号
	 * @param channelNo 通道号 
	 */
	String stopPtz(String deviceSerial, int channelNo, int direction) throws ClientProtocolException, IOException;;
	
	
	
}
