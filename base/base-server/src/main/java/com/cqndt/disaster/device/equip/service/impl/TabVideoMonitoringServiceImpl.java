package com.cqndt.disaster.device.equip.service.impl;

import com.cqndt.disaster.device.common.shrio.ShiroUtils;
import com.cqndt.disaster.device.dao.TabVideoMonitoringMapper;
import com.cqndt.disaster.device.equip.service.TabVideoMonitoringService;
import com.cqndt.disaster.device.vo.TabVideoMonitoringVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/18  16:53
 * Description:
 */
@Service
public class TabVideoMonitoringServiceImpl implements TabVideoMonitoringService {
    @Autowired
    private TabVideoMonitoringMapper tabVideoMonitoringMapper;
    @Override
    public List<TabVideoMonitoringVo> getListByCondition(TabVideoMonitoringVo vo) {
        return tabVideoMonitoringMapper.getListByCondition(ShiroUtils.getUserId());
    }

    @Override
    public TabVideoMonitoringVo getVideoById(Integer id) {
        return tabVideoMonitoringMapper.getVideoById(id);
    }

    @Override
    public List<Map<String, Object>> getAreaMsgByAreacode(Map<String, Object> map) {
        return tabVideoMonitoringMapper.getAreaMsgByAreacode( map );
    }

    @Override
    public List<Map<String, Object>> getVideoMsgByAreacode(List<Integer> area_code) {
        return tabVideoMonitoringMapper.getVideoMsgByAreacode( area_code );
    }

    @Override
    public List<Map<String, Object>> getLevelByAreacode(String area_code) {
        return tabVideoMonitoringMapper.getLevelByAreacode( area_code );
    }

    @Override
    public List<Map<String, Object>> getAllVideoMsgFromAreacodePre(String area_code) {
        return tabVideoMonitoringMapper.getAllVideoMsgFromAreacodePre( area_code );
    }
    @Override
    public  List<Map<String,Object>> getVideoMonitorMsgByAreacode(TabVideoMonitoringVo vo){
        return tabVideoMonitoringMapper.getVideoMonitorMsgByAreacode(vo);
    }

    @Override
    public List listMonitorInformation(Integer id) {
        return tabVideoMonitoringMapper.listMonitorInformation(id);
    }
}
