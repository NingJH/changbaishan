package com.cqndt.disaster.device.deviceExp.service.impl;

import com.cqndt.disaster.device.dao.TabDeviceExceptionMapper;
import com.cqndt.disaster.device.deviceExp.service.TabDeviceExceptionService;
import com.cqndt.disaster.device.domain.TabDeviceException;
import com.cqndt.disaster.device.vo.TabDeviceExceptionVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.*;

/**
 * Created By marc
 * Date: 2019/6/7  16:35
 * Description:
 */
@Service
public class TabDeviceExceptionServiceImpl implements TabDeviceExceptionService {
    @Autowired
    private TabDeviceExceptionMapper tabDeviceExceptionMapper;
    @Value("${file-ip}")
    private String fileIp;

    @Override
    public List<TabDeviceExceptionVo> getListByCondition(TabDeviceExceptionVo vo) {
        return tabDeviceExceptionMapper.getListByCondition(vo);
    }

    @Override
    public void ignoreException(TabDeviceExceptionVo vo) {
        String[] deviceNos = vo.getDeviceNo().split(",");
        Date date = new Date();
        List<TabDeviceException> list = tabDeviceExceptionMapper.getListByDeviceNo(deviceNos,"1");
        for (TabDeviceException excption:list) {
            excption.setOperateTime(date);
            excption.setOperatePersonName(vo.getOperatePersonName());
            excption.setDealBasic(vo.getDealBasic());
            excption.setDealType("2");
            excption.setDealState("3");
            tabDeviceExceptionMapper.updateByPrimaryKeySelective(excption);
        }
    }

    @Override
    public void dealException(TabDeviceExceptionVo vo) {
        String[] deviceNos = vo.getDeviceNo().split(",");
        Date date = new Date();
        List<TabDeviceException> list = tabDeviceExceptionMapper.getListByDeviceNo(deviceNos,"1");
        for (TabDeviceException excption:list) {
            excption.setOperateTime(date);
            excption.setOperatePersonName(vo.getOperatePersonName());
            excption.setDealBasic(vo.getDealBasic());
            excption.setDealType("1");
            excption.setDealState("2");
            tabDeviceExceptionMapper.updateByPrimaryKeySelective(excption);
        }
        //TODO 发送短信至相应的人员
        List<Map<String,Object>> listHuman = vo.getListHuman();
    }

    @Override
    public void fillDefendMessage(TabDeviceExceptionVo vo) {
        String[] deviceNos = vo.getDeviceNo().split(",");
        Date date = new Date();
        List<TabDeviceException> list = tabDeviceExceptionMapper.getListByDeviceNo(deviceNos,"2");
        for (TabDeviceException excption:list) {
            excption.setDefendEnterTime(date);
            excption.setDefendRemark(vo.getDefendRemark());
            excption.setDealPersonName(vo.getDealPersonName());
            excption.setDealTimeFrom(vo.getDealTimeFrom());
            excption.setDealTimeTo(vo.getDealTimeTo());
            excption.setDealState("3");
            excption.setDefendImgs(vo.getDefendImgs());
            tabDeviceExceptionMapper.updateByPrimaryKeySelective(excption);
        }
    }

    @Override
    public List<Map<String, Object>> listDataException(TabDeviceExceptionVo vo) {
        return tabDeviceExceptionMapper.listDataException(vo.getDeviceNo());
    }

    @Override
    public List<Map<String, Object>> listDealRecord(TabDeviceExceptionVo vo) {
        return tabDeviceExceptionMapper.listDealRecord(vo);
    }

    @Override
    public Map<String, Object> listDealRecordDetail(String deviceNo,String operateTime) {
        Map<String, Object> map = new HashMap<>();
        //异常情况
        Map<String,Object> exceptionDetail = tabDeviceExceptionMapper.exceptionDetail(deviceNo,operateTime);
        //数据异常列表
        List<Map<String,Object>> dataExceptionData = tabDeviceExceptionMapper.listDataExcetptionByDeviceNo(deviceNo,operateTime);
        exceptionDetail.put("dataException",dataExceptionData);
        //处置记录
        Map<String,Object> operateDetail = tabDeviceExceptionMapper.operateDetail(deviceNo,operateTime);
        //维护详情
        Map<String,Object> defendDetail =  tabDeviceExceptionMapper.defendDetail(deviceNo,operateTime);
        //图片
        if(null != defendDetail && null != defendDetail.get("defendImgs")){
            String[] imgs = defendDetail.get("defendImgs").toString().split(",");
            List<String> list = new ArrayList<>();
            for (String str:imgs) {
                list.add(fileIp+File.separator+str);
            }
            map.put("img",list);
        }
        map.put("exceptionDetail",exceptionDetail);
        map.put("operateDetail",operateDetail);
        map.put("defendDetail",defendDetail);
        return map;
    }
}
