package com.cqndt.disaster.device.basic.service.impl;

import com.cqndt.disaster.device.domain.TabBasicScope;
import com.cqndt.disaster.device.dao.TabBasicScopeMapper;
import com.cqndt.disaster.device.basic.service.TabBasicScopeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/15  17:21
 * Description:
 */
@Service
public class TabBasicScopeServiceImpl implements TabBasicScopeService {
    @Autowired
    private TabBasicScopeMapper tabBasicScopeMapper;

    @Override
    public List<TabBasicScope> selectByPhotoId(String photoId){
        return tabBasicScopeMapper.selectByPhotoId(photoId);
    }
}
