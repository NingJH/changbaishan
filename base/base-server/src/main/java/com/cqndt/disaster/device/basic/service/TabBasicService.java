package com.cqndt.disaster.device.basic.service;

import com.cqndt.disaster.device.domain.*;
import com.cqndt.disaster.device.vo.TabBasicVo;
import com.cqndt.disaster.device.vo.TabDeviceVo;
import com.cqndt.disaster.device.vo.TabHumanVo;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/15  15:28
 * Description:灾害点
 */
public interface TabBasicService {
    /**
     * 灾害点列表
     * @param tabBasic
     * @return
     */
    List<TabBasicVo> selectTabBasic(TabBasicVo tabBasic);

    /**
     * 根据id获取基础信息
     * @param id
     * @return
     */
    TabBasicWithBLOBs selectById(Integer id);

    /**
     * 获取两卡一案
     * @param disNo
     * @return
     */
    Map<String,Object> selectCards(String disNo);

    /**
     * 获取灾害点人员信息
     * @param disNo
     * @return
     */
    List<Map<String, Object>> selectHumanByDisNo(String disNo);

    /**
     * 根据灾害点id基础信息
     * @param id
     * @return
     */
    TabBasicVo selectTabBasicById(Integer id);

    List<TabDeviceVo> getDeviceByDisNo(String disNo);

}
