package com.cqndt.disaster.device.equip.service;

import com.cqndt.disaster.device.vo.TabVideoMonitoringVo;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/18  16:52
 * Description:
 */
public interface TabVideoMonitoringService {
    /**
     * 视频监测设备列表
     * @param vo
     * @return
     */
    List<TabVideoMonitoringVo> getListByCondition(TabVideoMonitoringVo vo);

    /**
     * 根据id单条记录
     * @param id
     * @return
     */
    TabVideoMonitoringVo getVideoById(Integer id);

    /**
     * 根据area_code查询 该area_code的所有下属区域
     * @param map
     * @return
     */
    List<Map<String,Object>> getAreaMsgByAreacode(Map<String,Object> map);

    /**
     * 根据area_code查询视频监测信息
     * @param area_code
     * @return
     */
    List<Map<String,Object>> getVideoMsgByAreacode(List<Integer> area_code);

    /**
     * 根据 area_code查询level
     * @param area_code
     * @return
     */
    List<Map<String,Object>> getLevelByAreacode(String area_code);

    /**
     * 根据area_code前缀查询该区域下所有视频信息，用于省级level的查询
     * @param area_code
     * @return
     */
    List<Map<String,Object>> getAllVideoMsgFromAreacodePre(String area_code);

    /**
     * app获取视频监控
     * @param vo
     * @return
     */
    List<Map<String,Object>> getVideoMonitorMsgByAreacode(TabVideoMonitoringVo vo);

    /**
     * 单个视频监控信息
     *
     * @param id
     * @return
     */
    List listMonitorInformation(Integer id);
}
