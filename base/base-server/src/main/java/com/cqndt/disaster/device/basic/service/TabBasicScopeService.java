package com.cqndt.disaster.device.basic.service;

import com.cqndt.disaster.device.domain.TabBasicScope;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/15  17:21
 * Description:
 */
public interface TabBasicScopeService {
    /**
     * 根据倾斜摄影id获取相应的灾害范围等
     * @param photoId
     * @return
     */
    List<TabBasicScope> selectByPhotoId(String photoId);
}
