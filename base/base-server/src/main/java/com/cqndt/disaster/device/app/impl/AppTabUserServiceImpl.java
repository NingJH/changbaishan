package com.cqndt.disaster.device.app.impl;

import com.cqndt.disaster.device.app.AppTabUserService;
import com.cqndt.disaster.device.dao.app.AppTabUserMapper;
import com.cqndt.disaster.device.domain.TabUser;
import com.cqndt.disaster.device.vo.TabUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created By marc
 * Date: 2019/5/12  10:26
 * Description:
 */
@Service
public class AppTabUserServiceImpl implements AppTabUserService {
    @Autowired
    private AppTabUserMapper appTabUserMapper;

    @Override
    public TabUserVo getUserByUserName(TabUserVo user) {
        return appTabUserMapper.getUserByUserName(user);
    }
}
