package com.cqndt.disaster.device.user.service.impl;

import com.cqndt.disaster.device.domain.TabUser;
import com.cqndt.disaster.device.dao.TabUserMapper;
import com.cqndt.disaster.device.user.service.TabUserService;
import com.cqndt.disaster.device.vo.TabUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TabUserServiceImpl implements TabUserService {
    @Autowired
    private TabUserMapper tabUserMapper;
    @Override
    public TabUserVo getUserByUserName(TabUser user){
        TabUserVo userVo = tabUserMapper.getUserByUserName(user);
        return userVo;
    }


}
