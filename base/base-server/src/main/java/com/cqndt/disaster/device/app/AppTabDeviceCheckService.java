package com.cqndt.disaster.device.app;

import com.cqndt.disaster.device.domain.TabDevice;
import com.cqndt.disaster.device.domain.TabDeviceCheck;
import com.cqndt.disaster.device.vo.Data;
import com.cqndt.disaster.device.vo.TabDeviceCheckVo;
import com.cqndt.disaster.device.vo.TabDeviceVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface AppTabDeviceCheckService {

    /**
     * 通过项目id查询设备下拉列表
     * @param projectNo
     * @return
     */
    List<Map<String,String>> listForDevice(String projectNo, String deviceName);

    /**
     * 新增
     * @param tabDeviceCheck
     * @return
     */
    int save(TabDeviceCheck tabDeviceCheck);

    /**
     * 上传APP图片
     * @param file
     * @return
     */
    String uploadDeviceCheckImg(MultipartFile file);

    /**
     * 上传APP视频
     * @param file
     * @return
     */
    String[] uploadDeviceCheckVideo(MultipartFile file);

    /**
     * 查询巡查记录详情
     * @param tabDeviceCheckVo
     * @return
     */
    List<TabDeviceCheckVo> listForDeviceCheck(TabDeviceCheckVo tabDeviceCheckVo);

    /**
     * 下载巡查记录
     * @return
     */
    Map<String,String> downloadDeviceCheck(TabDeviceCheckVo tabDeviceCheckVo);
}
