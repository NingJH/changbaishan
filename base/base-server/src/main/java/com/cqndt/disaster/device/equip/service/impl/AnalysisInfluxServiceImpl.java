package com.cqndt.disaster.device.equip.service.impl;

import com.cqndt.disaster.device.common.influxdb.InfluxDBConnect;
import com.cqndt.disaster.device.common.shrio.ShiroUtils;
import com.cqndt.disaster.device.dao.*;
import com.cqndt.disaster.device.domain.*;
import com.cqndt.disaster.device.equip.service.AnalysisService;
import com.cqndt.disaster.device.vo.DeviceInProjectVo;
import com.cqndt.disaster.device.vo.DeviceMonitorVo;
import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created By marc
 * Date: 2019/5/24  10:32
 * Description:
 */
@Service
public class AnalysisInfluxServiceImpl implements AnalysisService {

    private Logger log = LoggerFactory.getLogger(AnalysisInfluxServiceImpl.class);

    @Autowired
    private TbsLfwyMapper tbsLfwyMapper;
    @Autowired
    private TbsQjyWbxMapper tbsQjyWbxMapper;
    @Autowired
    private TbsDbbxHpMapper tbsDbbxHpMapper;
    @Autowired
    private TbsGnssMapper tbsGnssMapper;
    @Autowired
    private TbsSbwyQxyMapper tbsSbwyQxyMapper;
    @Autowired
    private TbsTtcjMapper tbsTtcjMapper;
    @Autowired
    private TbsChenjiangMapper tbsChenjiangMapper;
    @Autowired
    private TbsKongxishuiyaMapper tbsKongxishuiyaMapper;
    @Autowired
    private TbsGangjinjiMapper tbsGangjinjiMapper;
    @Autowired
    private TbsTylMapper tbsTylMapper;
    @Autowired
    private TbsYlMapper tbsYlMapper;
    @Autowired
    private TbsNsldxMapper tbsNsldxMapper;
    @Autowired
    private TbsShuiweijiMapper tbsShuiweijiMapper;
    @Autowired
    private TbsYingbianMapper tbsYingbianMapper;
    @Autowired
    private TbsWenshiduMapper tbsWenshiduMapper;
    @Autowired
    private TbsNiweijiMapper tbsNiweijiMapper;
    @Autowired
    private TbsZhendongMapper tbsZhendongMapper;
    @Autowired
    private TbsLxdbdMapper tbsLxdbdMapper;
    @Autowired
    private TbsDxswMapper tbsDxswMapper;
    @Autowired
    private TbsTotalStationMapper tbsTotalStationMapper;
    @Autowired
    private TbsRgszMapper tbsRgszMapper;
    @Autowired
    private TbsQxyMapper tbsQxyMapper;
    @Autowired
    private TbsSbwyLxMapper tbsSbwyLxMapper;
    @Autowired
    private TbsSbwyCxMapper tbsSbwyCxMapper;
    @Autowired
    private TbsSxcfjMapper tbsSxcfjMapper;
    @Autowired
    private TbsQjyMapper tbsQjyMapper;
    @Autowired
    private TbsZxsyljMapper tbsZxsyljMapper;
    @Autowired
    private TbsDishengMapper tbsDishengMapper;
    @Autowired
    private TbsCishengMapper tbsCishengMapper;
    @Autowired
    private TbsWwjMapper tbsWwjMapper;
    @Autowired
    private  TbsGdqxMapper tbsGdqxMapper;
    @Autowired
    private TbsBmwyMapper tbsBmwyMapper;
    @Autowired
    private TbsLsyMapper  tbsLsyMapper;
    @Autowired
    private TbsHslMapper tbsHslMapper;
    @Autowired
    private TabDeviceMapper tabDeviceMapper;
    @Autowired
    private TabWarnSettingMapper tabWarnSettingMapper;
    @Autowired
    private TbsDmQxyMapper tbsDmQxyMapper;
    @Autowired
    private TbsPtqcbxyMapper tbsPtqcbxyMapper;
    @Autowired
    private TbsLiefengLxMapper tbsLiefengLxMapper;
    @Autowired
    private TbsLiefengQjMapper tbsLiefengQjMapper;
    @Autowired
    private TabSensorInitMapper tabSensorInitMapper;
    @Autowired
    private TabSensorMapper tabSensorMapper;
    @Autowired
    private TabDeviceTypeMapper tabDeviceTypeMapper;
    @Autowired
    private TbsJlszyMapper tbsJlszyMapper;
    @Autowired
    private TbsQyjcMapper tbsQyjcMapper;
    @Autowired
    private TbsKswMapper tbsKswMapper;
    @Autowired
    private TbsLljMapper tbsLljMapper;
    @Autowired
    private  TbsHwzdMapper tbsHwzdMapper;
    @Autowired
    private TbsYsylMapper tbsYsylMapper;
    @Autowired
    private TbsHptlMapper tbsHptlMapper;
    @Autowired
    private TbsLdswjMapper tbsLdswjMapper;
    //雨量数据标题
    private static final String[] ylBt = new String[]{"雨量数据上传时间","雨量值(mm)"};
    //地下水位数据标题
    private static final String[] dxswBt = new String[]{"地下水位上传时间","水位值(m)"};
    //倾角数据标题
    private static final String[] qjyBt = new String[]{"倾角计","x角度(分)","y角度(分)","z角度(分)"};
    //裂缝计
    private static final String[] lfwyBt = new String[]{"裂缝计","裂缝(mm)"};
    //深部位移
    private static final String[] sbwyBt = new String[]{"深部位移","位移(mm)"};
    //静力水准仪
    private static final String[] jlszyBt = new String[]{"静力水准仪","水准(mm)"};
    //气压计
    private static final String[] qyjBt = new String[]{"气压计","气压(kPa)"};
    //水压力计
    private static final String[] syljBt = new String[]{"水压力计","水压(kPa)"};
    //气压计
    private static final String[] kswBt = new String[]{"雷达水位计","水位(m)"};
    //裂缝拉线数据标题
    private static final String[] lxBt = new String[]{"拉线数据上传时间","拉线(mm)"};
    //裂缝倾角数据标题
    private static final String[] lfqjBt = new String[]{"数据上传时间","x轴角度","y轴角度","z轴角度"};
    //地面倾斜数据标题
    private static final String[] dmqxyBt = new String[]{"数据上传时间","x轴角度","y轴角度","z轴角度"};
    //坡浅层位移数据标题
    private static final String[] pqcwyBt = new String[]{"数据上传时间","水平面内x方向(主滑方向)位移(mm)","水平面内y方向(与主滑方向垂直)位移(mm)","垂直方向沉降位移值(mm)"};
    //坡浅层倾角数据标题
    private static final String[] pqcqjBt = new String[]{"数据上传时间","水平面内X轴(主滑方向)与水平面夹角","水平面内Y轴(与主滑方向垂直)与水平面的倾斜夹角","水平面内Z轴与水平面的倾斜夹角"};
    //gnss数据标题
    private static final String[]gnssBt = new String[]{"GNSS数据上传时间","东西方向偏移值(mm)","南北方向偏移值(mm)","水平方向位移值(mm)","垂直方向位移值(mm)",
            "水平变化速率(mm/h)","垂直变化速率(mm/h)"};

    @Autowired
    private InfluxDBConnect influxDBConnect;

    @Override
    public Map<String, Object> selecteChart(SearchVo vo) {
        Map<String, Object> map = new HashMap<>();
        Integer type = Integer.valueOf(vo.getSensorType());
        String deviceNo = null;
        String monitorNo = null;
        List<TabDeviceType> tabDeviceType = null;
        TabDevice tabDevice = null;
        switch (type) {
            //滑动变位计 tbs_lfwy
            case 1:
//                deviceNo = tabSensorMapper.selectDeviceNoBySensorNo(vo.getSensorNo());
//                tabDevice = tabDeviceMapper.getDevice(deviceNo);
//                monitorNo = tabDeviceTypeMapper.selectMonitorNoBySensorNo(vo.getSensorNo());
//                tabDeviceType = tabDeviceTypeMapper.getDeviceType(monitorNo);
                List<Map<String, Object>>  tbsLfwy = tbsLfwyMapper.tbsLfwy(vo);
                getParamExplain2(vo.getSensorNo(),tbsLfwy,map, null,"value");
                break;
            //泥位计tbs_niweiji
            case 5:
//                monitorNo = tabDeviceTypeMapper.selectMonitorNoBySensorNo(vo.getSensorNo());
//                tabDeviceType = tabDeviceTypeMapper.getDeviceType(monitorNo);
                List<Map<String, Object>>  tbsNiweiji = tbsNiweijiMapper.tbsNiweiji(vo);
                getParamExplain2(vo.getSensorNo(),tbsNiweiji,map,null,"value");
                break;
            //土壤含水率 tbs_hsl
            case 6:
//                monitorNo = tabDeviceTypeMapper.selectMonitorNoBySensorNo(vo.getSensorNo());
//                tabDeviceType = tabDeviceTypeMapper.getDeviceType(monitorNo);
                List<Map<String, Object>>  tbsHsl = tbsHslMapper.tbsHsl(vo);
                getParamExplain2(vo.getSensorNo(),tbsHsl,map,null,"value");
                break;
            //次声tbs_cisheng
            case 8:
//                monitorNo = tabDeviceTypeMapper.selectMonitorNoBySensorNo(vo.getSensorNo());
//                tabDeviceType = tabDeviceTypeMapper.getDeviceType(monitorNo);
                List<Map<String, Object>>  tbsCisheng = tbsCishengMapper.tbsCisheng(vo);
                getParamExplain2(vo.getSensorNo(),tbsCisheng, map,null,"amplitude","frequency");
                break;
            //雨量tbs_yl
            case 9:
//                monitorNo = tabDeviceTypeMapper.selectMonitorNoBySensorNo(vo.getSensorNo());
//                tabDeviceType = tabDeviceTypeMapper.getDeviceType(monitorNo);
                List<Map<String, Object>>  tbsYl = tbsYlMapper.tbsYl(vo);
                getParamExplain2(vo.getSensorNo(),tbsYl,map,null, "value");
                break;
            //泥石流断线tbs_nsldx
            case 10:
//                monitorNo = tabDeviceTypeMapper.selectMonitorNoBySensorNo(vo.getSensorNo());
//                tabDeviceType = tabDeviceTypeMapper.getDeviceType(monitorNo);
                List<Map<String, Object>>  tbsNsldx = tbsNsldxMapper.tbsNsldx(vo);
                getParamExplain2(vo.getSensorNo(),tbsNsldx,map,null, "value");
                break;
            //地下水(地下水位)tbs_dxsw
            case 18:
//                monitorNo = tabDeviceTypeMapper.selectMonitorNoBySensorNo(vo.getSensorNo());
//                tabDeviceType = tabDeviceTypeMapper.getDeviceType(monitorNo);
                List<Map<String, Object>>  tbsDxsw = tbsDxswMapper.tbsDxsw(vo);
                getParamExplain2(vo.getSensorNo(),tbsDxsw,map,null,"value");
                break;
            //gnss tbs_gnss
            case 4:
//                monitorNo = tabDeviceTypeMapper.selectMonitorNoBySensorNo(vo.getSensorNo());
//                tabDeviceType = tabDeviceTypeMapper.getDeviceType(monitorNo);
//                List<Map<String, Object>>  tbsDxsw = tbsDxswMapper.tbsDxsw(vo);
//                getParamExplain2(vo.getSensorNo(),tbsDxsw,map,null,"value");
                break;
        }
        return map;
    }

    @Override
    public List<DeviceInProjectVo> selectDeviceByProjectId(String projectId) {
        List<DeviceInProjectVo> list = tabDeviceMapper.selectByProjectId(projectId);
        if(list.size()>0){
            for (DeviceInProjectVo vo: list) {
                List<DeviceMonitorVo> listNo = tabDeviceMapper.selectByPIdAndDtype(projectId,vo.getDeviceType(),ShiroUtils.getUserId());
                if(!listNo.isEmpty()){
                    vo.setListNo(listNo);

                }
            }
        }
        return list;
    }

    @Override
    public TabWarnSetting getSettingBySensorNo(String sensorNo) {
        return tabWarnSettingMapper.getSettingBySensorNo(sensorNo);
    }

    @Override
    public void dataExport(String deviceNos, String startTime, String endTime, HttpServletRequest request, HttpServletResponse response) {
        //创建poi导出数据对象
        SXSSFWorkbook sxssfWorkbook = new SXSSFWorkbook();
        String[] nos = deviceNos.split(",");
        List<TabSensor> list = tabSensorMapper.selectByDeviceNo(nos);
        int i = 0;
        for (TabSensor tabSensor:list) {
            //创建sheet页
            SXSSFSheet sheet = sxssfWorkbook.createSheet("Sheet"+i);
            //创建表头
            SXSSFRow headRow = sheet.createRow(0);
            SearchVo vo = new SearchVo();
            vo.setSensorNo(tabSensor.getSensorNo());
            vo.setSensorType(tabSensor.getSensorType());
            vo.setStartTime(startTime);
            vo.setEndTime(endTime);
            vo.setOrderDesc(" order by time desc");
            Integer type = Integer.valueOf(tabSensor.getSensorType());
            switch (type) {
                //裂缝计
                case 1:
                    List<Map<String, Object>>  tbsLfwy = tbsLfwyMapper.tbsLfwy(vo);
                    sxssfWorkbook.setSheetName(i,"裂缝计-"+tabSensor.getSensorNo());
                    for(int a=0;a<lfwyBt.length;a++){
                        //设置表头信息
                        headRow.createCell(a).setCellValue(lfwyBt[a]);
                    }
                    for (Map<String,Object> value : tbsLfwy){
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        dataRow.createCell(0).setCellValue(String.valueOf(value.get("time")));//数据上传时间
                        dataRow.createCell(1).setCellValue(String.valueOf(value.get("param1")));
                    }
                    i++;
                    break;
                //GNSS位移\地表位移(GPS)tbs_gnss
                case 4:
                    log.info("GNSS位移:"+vo.toString());
                    List<Map<String, Object>>  tbsGnss = tbsGnssMapper.tbsGnss(vo);
                    sxssfWorkbook.setSheetName(i,"GNSS-"+tabSensor.getSensorNo());
                    for(int a=0;a<gnssBt.length;a++){
                        //设置表头信息
                        headRow.createCell(a).setCellValue(gnssBt[a]);
                    }
                    for (Map<String,Object> value : tbsGnss){
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        dataRow.createCell(0).setCellValue(String.valueOf(value.get("time")));//数据上传时间
                        dataRow.createCell(1).setCellValue(String.valueOf(value.get("param1")));//东西方向偏移值(正数代表向东偏移，负数代表向西偏移)
                        dataRow.createCell(2).setCellValue(String.valueOf(value.get("param2")));//南北方向偏移值(正数代表向北偏移，负数代表向南偏移)
                        dataRow.createCell(3).setCellValue(String.valueOf(value.get("param3")));//水平方向位移值(当前位置距离初始位置的水平距离)
                        dataRow.createCell(4).setCellValue(String.valueOf(value.get("param4")));//垂直方向位移值(正数表示向上抬升，负数表示向下沉降)
                        dataRow.createCell(5).setCellValue(String.valueOf(value.get("param5")));//水平变化速率(一个小时的水平位移变化量)移
                        dataRow.createCell(6).setCellValue(String.valueOf(value.get("param6")));//垂直变化速率(一个小时的垂直位移变化量)
                    }
                    i++;
                    break;
                //水压力tbs_kongxishuiya
                case 7:
                    List<Map<String, Object>>  tbsSy = tbsKongxishuiyaMapper.tbsKongxishuiya(vo);
                    sxssfWorkbook.setSheetName(i,"雨量计-"+tabSensor.getSensorNo());
                    for(int a=0;a<syljBt.length;a++){
                        //设置表头信息
                        headRow.createCell(a).setCellValue(syljBt[a]);
                    }
                    for (Map<String,Object> value : tbsSy){
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        dataRow.createCell(0).setCellValue(String.valueOf(value.get("time")));//数据上传时间
                        dataRow.createCell(1).setCellValue(String.valueOf(value.get("param1")));
                    }
                    i++;
                    break;
                //雨量tbs_yl
                case 10:
                    List<Map<String, Object>>  tbsYl = tbsYlMapper.tbsYl(vo);
                    sxssfWorkbook.setSheetName(i,"雨量计-"+tabSensor.getSensorNo());
                    for(int a=0;a<ylBt.length;a++){
                        //设置表头信息
                        headRow.createCell(a).setCellValue(ylBt[a]);
                    }
                    for (Map<String,Object> value : tbsYl){
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        dataRow.createCell(0).setCellValue(String.valueOf(value.get("time")));//数据上传时间
                        dataRow.createCell(1).setCellValue(String.valueOf(value.get("param1")));
                    }
                    i++;
                    break;
                //地下水位tbs_dxsw
                case 18:
                    List<Map<String,Object>> tbsDxsw = tbsDxswMapper.tbsDxsw(vo);
                    sxssfWorkbook.setSheetName(i,"地下水位-"+tabSensor.getSensorNo());
                    for(int j = 0;j<dxswBt.length;j++){
                        //设置表头信息
                        headRow.createCell(j).setCellValue(dxswBt[j]);
                    }
                    for(Map<String,Object> value : tbsDxsw){
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        dataRow.createCell(0).setCellValue(String.valueOf(value.get("time")));//数据上传时间
                        dataRow.createCell(1).setCellValue(String.valueOf(value.get("param1")));
                    }
                    i++;
                    break;
                //倾角计tbs_qjy
                case 25:
                    List<Map<String,Object>> tbsQjy = tbsQjyMapper.tbsQjy(vo);
                    sxssfWorkbook.setSheetName(i,"倾角计-"+tabSensor.getSensorNo());
                    for(int j = 0;j<qjyBt.length;j++){
                        //设置表头信息
                        headRow.createCell(j).setCellValue(qjyBt[j]);
                    }
                    for(Map<String,Object> value : tbsQjy){
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        dataRow.createCell(0).setCellValue(String.valueOf(value.get("time")));//数据上传时间
                        dataRow.createCell(1).setCellValue(String.valueOf(value.get("param1")));//x
                        dataRow.createCell(2).setCellValue(String.valueOf(value.get("param2")));//y
                        dataRow.createCell(3).setCellValue(String.valueOf(value.get("param3")));//z
                    }
                    i++;
                    break;
                //深部位移tbs_ttcj
                case 35:
                    List<Map<String,Object>> tbsSbwy = tbsTtcjMapper.tbsTtcj(vo);
                    sxssfWorkbook.setSheetName(i,"深部位移-"+tabSensor.getSensorNo());
                    for(int j = 0;j<sbwyBt.length;j++){
                        //设置表头信息
                        headRow.createCell(j).setCellValue(sbwyBt[j]);
                    }
                    for(Map<String,Object> value : tbsSbwy){
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        dataRow.createCell(0).setCellValue(String.valueOf(value.get("time")));//数据上传时间
                        dataRow.createCell(1).setCellValue(String.valueOf(value.get("param1")));
                    }
                    i++;
                    break;
                //静力水准仪tbs_jlszy
                case 50:
                    List<Map<String,Object>> tbsJlszy = tbsJlszyMapper.tbsJlszy(vo);
                    sxssfWorkbook.setSheetName(i,"深部位移-"+tabSensor.getSensorNo());
                    for(int j = 0;j<jlszyBt.length;j++){
                        //设置表头信息
                        headRow.createCell(j).setCellValue(jlszyBt[j]);
                    }
                    for(Map<String,Object> value : tbsJlszy){
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        dataRow.createCell(0).setCellValue(String.valueOf(value.get("time")));//数据上传时间
                        dataRow.createCell(1).setCellValue(String.valueOf(value.get("param1")));
                    }
                    i++;
                    break;
                //气压tbs_qyjc
                case 51:
                    List<Map<String,Object>> tbsQy = tbsQyjcMapper.tbsQyjc(vo);
                    sxssfWorkbook.setSheetName(i,"深部位移-"+tabSensor.getSensorNo());
                    for(int j = 0;j<qyjBt.length;j++){
                        //设置表头信息
                        headRow.createCell(j).setCellValue(qyjBt[j]);
                    }
                    for(Map<String,Object> value : tbsQy){
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        dataRow.createCell(0).setCellValue(String.valueOf(value.get("time")));//数据上传时间
                        dataRow.createCell(1).setCellValue(String.valueOf(value.get("param1")));
                    }
                    i++;
                    break;
                //库水位（雷达水位）tbs_ksw
                case 52:
                    List<Map<String,Object>> tbsKsw = tbsKswMapper.tbsKsw(vo);
                    sxssfWorkbook.setSheetName(i,"深部位移-"+tabSensor.getSensorNo());
                    for(int j = 0;j<kswBt.length;j++){
                        //设置表头信息
                        headRow.createCell(j).setCellValue(kswBt[j]);
                    }
                    for(Map<String,Object> value : tbsKsw){
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        dataRow.createCell(0).setCellValue(String.valueOf(value.get("time")));//数据上传时间
                        dataRow.createCell(1).setCellValue(String.valueOf(value.get("param1")));
                    }
                    i++;
                    break;
                //裂缝倾角(地表裂缝监测仪/墙裂缝监测仪——倾角) tbs_liefeng_qj
                case 40:
                    log.info("查询裂缝倾角"+vo.toString());
                    String lfX = "select time,sensor_no,value as param1 from tbs_liefeng_qj_x where sensor_no ='"+tabSensor.getSensorNo()+"' AND time >='"+startTime+"' AND time <='"+endTime+"' ORDER BY time desc";
                    List<Map<String, Object>> lf_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(lfX));
                    String lfY = "select time,sensor_no,value as param1 from tbs_liefeng_qj_y where sensor_no ='"+tabSensor.getSensorNo()+"' AND time >='"+startTime+"' AND time <='"+endTime+"' ORDER BY time desc";
                    List<Map<String, Object>> lf_y = influxDBConnect.queryResultToListMap(influxDBConnect.query(lfY));
                    String lfZ = "select time,sensor_no,value as param1 from tbs_liefeng_qj_z where sensor_no ='"+tabSensor.getSensorNo()+"' AND time >='"+startTime+"' AND time <='"+endTime+"' ORDER BY time desc";
                    List<Map<String, Object>> lf_z = influxDBConnect.queryResultToListMap(influxDBConnect.query(lfZ));
                    sxssfWorkbook.setSheetName(i,"裂缝倾角-"+vo.getSensorNo());
                    for(int a=0;a<lfqjBt.length;a++){
                        //设置表头信息
                        headRow.createCell(a).setCellValue(lfqjBt[a]);
                    }
                    //x倾角
                    int lfXSize  = lf_x.size();
                    //y倾角
                    int lfYSize  = lf_y.size();
                    //z倾角
                    int lfZSize  = lf_z.size();
                    int max = lfXSize;
                    List<Map<String, Object>> maxList = lf_x;
                    if(lfYSize>max){
                        max = lfYSize;
                        maxList = lf_y;
                    }
                    if(lfZSize>max){
                        max = lfZSize;
                        maxList = lf_z;
                    }
                    for(int q=0;q<max;q++){
                        Map<String,Object> mapMax = maxList.get(q);
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        String time = mapMax.get("time").toString();
                        String mydate = dateFormat(time);
                        dataRow.createCell(0).setCellValue(mydate);//数据上传时间
                        if(q<lfXSize){
                            Map<String,Object> mapx = lf_x.get(q);
                            if(mapx.get("time").toString().equals(time)){
                                dataRow.createCell(1).setCellValue(String.valueOf(mapx.get("param1")));
                            }else{
                                dataRow.createCell(1).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(1).setCellValue("--");
                        }
                        if(q<lfYSize){
                            Map<String,Object> mapy = lf_y.get(q);
                            if(mapy.get("time").toString().equals(time)){
                                dataRow.createCell(2).setCellValue(String.valueOf(mapy.get("param1")));
                            }else{
                                dataRow.createCell(2).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(2).setCellValue("--");
                        }
                        if(q<lfZSize){
                            Map<String,Object> mapz = lf_z.get(q);
                            if(mapz.get("time").toString().equals(time)){
                                dataRow.createCell(3).setCellValue(String.valueOf(mapz.get("param1")));
                            }else{
                                dataRow.createCell(3).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(3).setCellValue("--");
                        }
                    }
                    i++;
                    break;
                //地面倾斜监测仪tbs_dm_qxy
                case 41:
                    log.info("查询地面倾斜:"+vo.toString());
                    String dmQxyX = "select time,sensor_no,value as param1 from tbs_dm_qxy_x where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' ORDER BY time desc";
                    List<Map<String, Object>> x = influxDBConnect.queryResultToListMap(influxDBConnect.query(dmQxyX));
                    String dmQxyY = "select time,sensor_no,value as param1 from tbs_dm_qxy_y where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' ORDER BY time desc";
                    List<Map<String, Object>> y = influxDBConnect.queryResultToListMap(influxDBConnect.query(dmQxyY));
                    String dmQxyZ = "select time,sensor_no,value as param1 from tbs_dm_qxy_z where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' ORDER BY time desc";
                    List<Map<String, Object>> z = influxDBConnect.queryResultToListMap(influxDBConnect.query(dmQxyZ));
                    sxssfWorkbook.setSheetName(i,"地面倾斜监测-"+vo.getSensorNo());
                    for(int a=0;a<dmqxyBt.length;a++){
                        //设置表头信息
                        headRow.createCell(a).setCellValue(dmqxyBt[a]);
                    }
                    int qxyxSize  = x.size();
                    int qxyySize  = y.size();
                    int qxyzSize  = z.size();
                    int maxQxy = qxyxSize;
                    List<Map<String, Object>> maxQxyList = x;
                    if(qxyySize>maxQxy){
                        maxQxy = qxyySize;
                        maxQxyList = y;
                    }
                    if(qxyzSize>maxQxy){
                        maxQxy = qxyzSize;
                        maxQxyList = z;
                    }
                    for(int q=0;q<maxQxy;q++){
                        Map<String,Object> mapMax = maxQxyList.get(q);
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        String time = mapMax.get("time").toString();
                        String mydate = dateFormat(time);
                        dataRow.createCell(0).setCellValue(mydate);//数据上传时间
                        if(q<qxyxSize){
                            Map<String,Object> mapx = x.get(q);
                            if(mapx.get("time").toString().equals(time)){
                                dataRow.createCell(1).setCellValue(String.valueOf(mapx.get("param1")));
                            }else{
                                dataRow.createCell(1).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(1).setCellValue("--");
                        }
                        if(q<qxyySize){
                            Map<String,Object> mapy = y.get(q);
                            if(mapy.get("time").toString().equals(time)){
                                dataRow.createCell(2).setCellValue(String.valueOf(mapy.get("param1")));
                            }else{
                                dataRow.createCell(2).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(2).setCellValue("--");
                        }
                        if(q<qxyzSize){
                            Map<String,Object> mapz = z.get(q);
                            if(mapz.get("time").toString().equals(time)){
                                dataRow.createCell(3).setCellValue(String.valueOf(mapz.get("param1")));
                            }else{
                                dataRow.createCell(3).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(3).setCellValue("--");
                        }
                    }
                    i++;
                    break;
                //坡体浅层变形仪-拉线 tbs_ptqcbxy
                case 42:
                    log.info("查询坡体浅层变形仪-拉线:"+vo.toString());
                    String lxX = "select time,sensor_no,value as param1 from tbs_ptqcbxy_lx_x where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' ORDER BY time";
                    List<Map<String, Object>> lx_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(lxX));
                    String lxY = "select time,sensor_no,value as param1 from tbs_ptqcbxy_lx_y where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' ORDER BY time";
                    List<Map<String, Object>> lx_y = influxDBConnect.queryResultToListMap(influxDBConnect.query(lxY));
                    String lxZ = "select time,sensor_no,value as param1 from tbs_ptqcbxy_lx_z where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' ORDER BY time";
                    List<Map<String, Object>> lx_z = influxDBConnect.queryResultToListMap(influxDBConnect.query(lxZ));
                    sxssfWorkbook.setSheetName(i,"坡体浅层位移-"+vo.getSensorNo());
                    for(int a=0;a<pqcwyBt.length;a++){
                        //设置表头信息
                        headRow.createCell(a).setCellValue(pqcwyBt[a]);
                    }
                    int lxxSize  = lx_x.size();
                    int lxySize  = lx_y.size();
                    int lxzSize  = lx_z.size();
                    int maxLx = lxxSize;
                    List<Map<String, Object>> maxLxList = lx_x;
                    if(lxySize>maxLx){
                        maxLx = lxySize;
                        maxLxList = lx_y;
                    }
                    if(lxzSize>maxLx){
                        maxLx = lxzSize;
                        maxLxList = lx_z;
                    }
                    for(int q=0;q<maxLx;q++){
                        Map<String,Object> mapMax = maxLxList.get(q);
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        String time = mapMax.get("time").toString();
                        String mydate = dateFormat(time);
                        dataRow.createCell(0).setCellValue(mydate);//数据上传时间
                        if(q<lxxSize){
                            Map<String,Object> mapx = lx_x.get(q);
                            if(mapx.get("time").toString().equals(time)){
                                dataRow.createCell(1).setCellValue(String.valueOf(mapx.get("param1")));
                            }else{
                                dataRow.createCell(1).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(1).setCellValue("--");
                        }
                        if(q<lxySize){
                            Map<String,Object> mapy = lx_y.get(q);
                            if(mapy.get("time").toString().equals(time)){
                                dataRow.createCell(2).setCellValue(String.valueOf(mapy.get("param1")));
                            }else{
                                dataRow.createCell(2).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(2).setCellValue("--");
                        }
                        if(q<lxzSize){
                            Map<String,Object> mapz = lx_z.get(q);
                            if(mapz.get("time").toString().equals(time)){
                                dataRow.createCell(3).setCellValue(String.valueOf(mapz.get("param1")));
                            }else{
                                dataRow.createCell(3).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(3).setCellValue("--");
                        }
                    }
                    i++;
                    break;
                //坡体浅层变形仪-倾角 tbs_ptqcbxy
                case 44:
                    log.info("坡体浅层变形仪-倾角:"+vo.toString());
                    String qxX = "select time,sensor_no,value as param1 from tbs_ptqcbxy_wy_x where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' ORDER BY time";
                    List<Map<String, Object>> qx_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(qxX));
                    String qxY = "select time,sensor_no,value as param1 from tbs_ptqcbxy_wy_y where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' ORDER BY time";
                    List<Map<String, Object>> qx_y = influxDBConnect.queryResultToListMap(influxDBConnect.query(qxY));
                    String qxZ = "select time,sensor_no,value as param1 from tbs_ptqcbxy_wy_z where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' ORDER BY time";
                    List<Map<String, Object>> qx_z = influxDBConnect.queryResultToListMap(influxDBConnect.query(qxZ));
                    sxssfWorkbook.setSheetName(i,"坡体浅层倾角-"+vo.getSensorNo());
                    for(int a=0;a<pqcqjBt.length;a++){
                        //设置表头信息
                        headRow.createCell(a).setCellValue(pqcqjBt[a]);
                    }
                    int qxxSize  = qx_x.size();
                    int qxySize  = qx_y.size();
                    int qxzSize  = qx_z.size();
                    int maxQx = qxxSize;
                    List<Map<String, Object>> maxQxList = qx_x;
                    if(qxySize>maxQx){
                        maxQx = qxySize;
                        maxQxList = qx_y;
                    }
                    if(qxzSize>maxQx){
                        maxQx = qxzSize;
                        maxQxList = qx_z;
                    }
                    for(int q=0;q<maxQx;q++){
                        Map<String,Object> mapMax = maxQxList.get(q);
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        String time = mapMax.get("time").toString();
                        String mydate = dateFormat(time);
                        dataRow.createCell(0).setCellValue(mydate);//数据上传时间
                        if(q<qxxSize){
                            Map<String,Object> mapx = qx_x.get(q);
                            if(mapx.get("time").toString().equals(time)){
                                dataRow.createCell(1).setCellValue(String.valueOf(mapx.get("param1")));
                            }else{
                                dataRow.createCell(1).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(1).setCellValue("--");
                        }
                        if(q<qxySize){
                            Map<String,Object> mapy = qx_y.get(q);
                            if(mapy.get("time").toString().equals(time)){
                                dataRow.createCell(2).setCellValue(String.valueOf(mapy.get("param1")));
                            }else{
                                dataRow.createCell(2).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(2).setCellValue("--");
                        }
                        if(q<qxzSize){
                            Map<String,Object> mapz = qx_z.get(q);
                            if(mapz.get("time").toString().equals(time)){
                                dataRow.createCell(3).setCellValue(String.valueOf(mapz.get("param1")));
                            }else{
                                dataRow.createCell(3).setCellValue("--");
                            }
                        }else{
                            dataRow.createCell(3).setCellValue("--");
                        }
                    }
                    i++;
                    break;
                //裂缝拉线(地表裂缝监测仪/墙裂缝监测仪——拉线) tbs_liefeng_lx
                case 43:
                    log.info("裂缝拉线:"+vo.toString());
                    String liefengLx = "select time,sensor_no,value as param1 from tbs_liefeng_lx where sensor_no ='"+vo.getSensorNo()+"' AND time >='"+vo.getStartTime()+"' AND time <='"+vo.getEndTime()+"' ORDER BY time";
                    List<Map<String, Object>> lief_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(liefengLx));
                    sxssfWorkbook.setSheetName(i,"拉线-"+vo.getSensorNo());
                    for(int a=0;a<lxBt.length;a++){
                        //设置表头信息
                        headRow.createCell(a).setCellValue(lxBt[a]);
                    }
                    for (Map<String,Object> value : lief_x){
                        SXSSFRow dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
                        String time = String.valueOf(value.get("time"));
                        time = dateFormat(time);
                        dataRow.createCell(0).setCellValue(time);//数据上传时间
                        dataRow.createCell(1).setCellValue(String.valueOf(value.get("param1")));//拉线值
                    }
                    i++;
                    break;
            }
        }
        // 下载导出
        String filename = "设备数据分析"+new SimpleDateFormat("yyyyMMdd").format(new Date());
        // 设置头信息
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/vnd.ms-excel");
        //一定要设置成xlsx格式
        try {
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(filename + ".xlsx", "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            //创建一个输出流
            OutputStream outputStream = response.getOutputStream();
            //写入数据
            sxssfWorkbook.write(outputStream);
            // 关闭
            outputStream.flush();
            outputStream.close();
            sxssfWorkbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 转换对应的结果集
     * @param results
     * @param map
     * @param name
     */
    private Map<String, Object> getParamInfluxExplain(String sensorNo,List<Map<String, Object>> results, Map<String, Object> map,TabDevice tabDevice,String ... name){
        if (results.size() <= 0) {
            return map;
        }
        BigDecimal maxData = null!=map && null!=map.get("maxData")?new BigDecimal(map.get("maxData").toString()):null;
        BigDecimal minData = null!=map && null!=map.get("minData")?new BigDecimal(map.get("minData").toString()):null;

        for(int i=0;i<name.length;i++){
            List<Map<String, Object>> mapList = new ArrayList<>();
            List<List<Object>> list = new ArrayList<>();
            String[] names = name[i].split("-");
            Map<String,Object> initValueMap = tabSensorInitMapper.getDataByFiledName(sensorNo,names[0]);

            for (Map<String, Object> mapResult:results) {
                String time = mapResult.get("time").toString();
                String mydate = dateFormat(time);
                if (mapResult.get("param"+(i+1)) != null && mapResult.get("param"+(i+1)) !="") {
                    BigDecimal initValue = BigDecimal.ZERO;
                    if(null != initValueMap && null != initValueMap.get("init_value")){
                        initValue = new BigDecimal(initValueMap.get("init_value").toString());
                    }
                    BigDecimal value = new BigDecimal(mapResult.get("param"+(i+1)).toString()).subtract(initValue);
                    maxData = maxData == null? value: maxData.max(value);
                    minData = minData == null? value: minData.min(value);
                    Map<String, Object> map1 = new HashMap<>();
                    map1.put("name", mydate);

                    if(tabDevice != null){
                        map1.put("x", tabDevice.getXaDirection());
                        map1.put("-x", tabDevice.getX_Direction());
                        map1.put("y", tabDevice.getYaDirection());
                        map1.put("-y", tabDevice.getY_Direction());
                        map1.put("z", tabDevice.getZaDirection());
                        map1.put("-z", tabDevice.getZ_Direction());
                    }

                    List<Object> list1 = new ArrayList<>();
                    list1.add(mydate);
                    list1.add(value);
                    list.add(list1);
                    map1.put("value", list1);
                    mapList.add(map1);
                }
            }
            if(mapList.size()>0){
                map.put("maxData", maxData);
                map.put("minData", minData);
                map.put(names[1],mapList);

                if(tabDevice != null){
                    map.put("deviceScale", tabDevice.getDeviceScale() == null ? 0 : tabDevice.getDeviceScale());
                }
            }
        }
        return map;
    }

    private Map<String, Object> getParamInfluxExplain2(String sensorNo,List<Map<String, Object>> results, Map<String, Object> map,TabDevice tabDevice,List<TabDeviceType> tabDeviceType,String ... name){
        if (results.size() <= 0) {
            return map;
        }
        BigDecimal maxData = null!=map && null!=map.get("maxData")?new BigDecimal(map.get("maxData").toString()):null;
        BigDecimal minData = null!=map && null!=map.get("minData")?new BigDecimal(map.get("minData").toString()):null;

        for(int i=0;i<name.length;i++){
            List<Map<String, Object>> mapList = new ArrayList<>();
            List<List<Object>> list = new ArrayList<>();
            String[] names = name[i].split("-");
            Map<String,Object> initValueMap = tabSensorInitMapper.getDataByFiledName(sensorNo,names[0]);

            for (Map<String, Object> mapResult:results) {
                String time = mapResult.get("time").toString();
                String mydate = dateFormat(time);
                if (mapResult.get("param"+(i+1)) != null && mapResult.get("param"+(i+1)) !="") {
                    BigDecimal initValue = BigDecimal.ZERO;
                    if(null != initValueMap && null != initValueMap.get("init_value")){
                        initValue = new BigDecimal(initValueMap.get("init_value").toString());
                    }
                    BigDecimal value = new BigDecimal(mapResult.get("param"+(i+1)).toString()).subtract(initValue);
                    maxData = maxData == null? value: maxData.max(value);
                    minData = minData == null? value: minData.min(value);
                    Map<String, Object> map1 = new HashMap<>();
                    map1.put("name", mydate);

                    if(tabDevice != null){
                        map1.put("x", tabDevice.getXaDirection());
                        map1.put("-x", tabDevice.getX_Direction());
                        map1.put("y", tabDevice.getYaDirection());
                        map1.put("-y", tabDevice.getY_Direction());
                        map1.put("z", tabDevice.getZaDirection());
                        map1.put("-z", tabDevice.getZ_Direction());
                    }

                    List<Object> list1 = new ArrayList<>();
                    list1.add(mydate);
                    list1.add(value);
                    list.add(list1);
                    map1.put("value", list1);
                    mapList.add(map1);
                }
            }
            if(mapList.size()>0){
                map.put("maxData", maxData);
                map.put("minData", minData);
                map.put(names[1],mapList);

                List<Map<String,Object>> list2 = new ArrayList<>();
                if(tabDeviceType.isEmpty()) {
                    Map<String, Object> map2 = new HashMap<>();
                    map2.put("scale", 0);
                    map2.put("type", 0);
                    list2.add(map2);
                }else {
                    for(TabDeviceType tabDeviceType1: tabDeviceType){
                        if(tabDeviceType1 != null){
//                        map.put("deviceScale", tabDeviceType1.getScale() == null ? 0 : tabDeviceType1.getScale());
                            Map<String,Object> map2 = new HashMap<>();
                            map2.put("scale", StringUtils.isEmpty(tabDeviceType1.getScale()) ? 0 : tabDeviceType1.getScale());
                            map2.put("type", StringUtils.isEmpty(tabDeviceType1.getType()) ? 0 : tabDeviceType1.getType());
                            list2.add(map2);
                        }
                    }
                }
                map.put("scale",list2);
            }
        }
        return map;
    }
    /**
     * 地声、次声转换对应的结果集
     * @param results
     * @param map
     * @param name
     */
    private void getParamSpecial(List<Map<String, Object>> results, Map<String, Object> map,String ... name){
        if (results.size() <= 0) {
            return ;
        }
        BigDecimal maxData = null;
        BigDecimal minData = null;
        for(int i=0;i<name.length;i++){
            List<Map<String, Object>> mapList = new ArrayList<>();
            List<List<Object>> list = new ArrayList<>();
            for (Map<String, Object> mapResult:results) {
                Object time = mapResult.get("time");
                if (mapResult.get("param"+(i+1)) != null && mapResult.get("param"+(i+1)) !="") {
                    String  value = (String)mapResult.get("param"+(i+1));
                    String[] values = value.split(",");
                    for (String str:values) {
                        if(null==str||str.equals("")){
                            continue;
                        }
                        BigDecimal num = new BigDecimal(str);
                        maxData = maxData == null? num: maxData.max(num);
                        minData = minData == null? num: minData.min(num);
                    }
                    Map<String, Object> map1 = new HashMap<>();
                    map1.put("name", time);
                    List<Object> list1 = new ArrayList<>();
                    list1.add(time);
                    list1.add(value);
                    list.add(list1);
                    map1.put("value", list1);
                    mapList.add(map1);
                }
            }
            map.put("maxData", maxData);
            map.put("minData", minData);
            if(mapList.size()>0){
                map.put(name[i],mapList);
            }
        }
    }

    private void getParamSpecia2(List<Map<String, Object>> results, Map<String, Object> map,List<TabDeviceType> tabDeviceType,String ... name){
        if (results.size() <= 0) {
            return ;
        }
        BigDecimal maxData = null;
        BigDecimal minData = null;
        for(int i=0;i<name.length;i++){
            List<Map<String, Object>> mapList = new ArrayList<>();
            List<List<Object>> list = new ArrayList<>();
            for (Map<String, Object> mapResult:results) {
                Object time = mapResult.get("time");
                if (mapResult.get("param"+(i+1)) != null && mapResult.get("param"+(i+1)) !="") {
                    String  value = (String)mapResult.get("param"+(i+1));
                    String[] values = value.split(",");
                    for (String str:values) {
                        if(null==str||str.equals("")){
                            continue;
                        }
                        BigDecimal num = new BigDecimal(str);
                        maxData = maxData == null? num: maxData.max(num);
                        minData = minData == null? num: minData.min(num);
                    }
                    Map<String, Object> map1 = new HashMap<>();
                    map1.put("name", time);
                    List<Object> list1 = new ArrayList<>();
                    list1.add(time);
                    list1.add(value);
                    list.add(list1);
                    map1.put("value", list1);
                    mapList.add(map1);
                }
            }
            map.put("maxData", maxData);
            map.put("minData", minData);
//            if(mapList.size()>0){
//                map.put(name[i],mapList);
//                List<Map<String,Object>> list2 = new ArrayList<>();
//                if(tabDeviceType.isEmpty()){
//                    Map<String,Object> map2 = new HashMap<>();
//                    map2.put("scale",  0);
//                    map2.put("type", 0);
//                    list2.add(map2);
//                }else {
//                    for(TabDeviceType tabDeviceType1: tabDeviceType){
//                        if(tabDeviceType1 != null){
////                        map.put("deviceScale", tabDeviceType1.getScale() == null ? 0 : tabDeviceType1.getScale());
//                            Map<String,Object> map2 = new HashMap<>();
//                            map2.put("scale", StringUtils.isEmpty(tabDeviceType1.getScale()) ? 0 : tabDeviceType1.getScale());
//                            map2.put("type", StringUtils.isEmpty(tabDeviceType1.getType()) ? 0 : tabDeviceType1.getType());
//                            list2.add(map2);
//                        }
//                    }
//                }
//                map.put("scale",list2);
//            }
        }
    }

    /**
     * 转换对应的结果集
     * @param results
     * @param map
     * @param name
     */
    private void getParamExplain(String sensorNo,List<Map<String, Object>> results, Map<String, Object> map, TabDevice tabDevice,String ... name){
        if (results.size() <= 0) {
            return ;
        }
        BigDecimal maxData = null;
        BigDecimal minData = null;
        for(int i=0;i<name.length;i++){
            List<Map<String, Object>> mapList = new ArrayList<>();
            List<List<Object>> list = new ArrayList<>();
            String[] names = name[i].split("-");
            Map<String,Object> initValueMap = tabSensorInitMapper.getDataByFiledName(sensorNo,names[0]);
            for (Map<String, Object> mapResult:results) {
                Object time = mapResult.get("time");
                if (mapResult.get("param"+(i+1)) != null && mapResult.get("param"+(i+1)) !="") {
                    BigDecimal initValue = BigDecimal.ZERO;
                    if(null != initValueMap && null != initValueMap.get("init_value")){
                        initValue = new BigDecimal(initValueMap.get("init_value").toString());
                    }
                    BigDecimal value = new BigDecimal(mapResult.get("param"+(i+1)).toString()).subtract(initValue);
                    maxData = maxData == null? value: maxData.max(value);
                    minData = minData == null? value: minData.min(value);
                    Map<String, Object> map1 = new HashMap<>();
                    map1.put("name", time);
                    List<Object> list1 = new ArrayList<>();
                    list1.add(time);
                    list1.add(value);
                    list.add(list1);
                    map1.put("value", list1);
                    mapList.add(map1);
                }
            }
            if(mapList.size()>0){
                map.put("maxData", maxData);
                map.put("minData", minData);
                if(tabDevice != null){
                    map.put("deviceScale", tabDevice.getDeviceScale() == null ? 0 : tabDevice.getDeviceScale());
                }
                map.put(names.length == 1 ? names[0] : names[1],mapList);
            }
        }
    }

    /**
     * 转换对应的结果集
     * @param results
     * @param map
     * @param name
     */
    private void  getParamExplain2(String sensorNo,List<Map<String, Object>> results, Map<String, Object> map, List<TabDeviceType> tabDeviceType,String ... name){
        if (results.size() <= 0) {
            return ;
        }
        BigDecimal maxData = null;
        BigDecimal minData = null;
        for(int i=0;i<name.length;i++){
            List<Map<String, Object>> mapList = new ArrayList<>();
            List<List<Object>> list = new ArrayList<>();
            String[] names = name[i].split("-");
            Map<String,Object> initValueMap = tabSensorInitMapper.getDataByFiledName(sensorNo,names[0]);
            for (Map<String, Object> mapResult:results) {
                Object time = mapResult.get("time");
                if (mapResult.get("param"+(i+1)) != null && mapResult.get("param"+(i+1)) !="") {
                    BigDecimal initValue = BigDecimal.ZERO;
                    if(null != initValueMap && null != initValueMap.get("init_value")){
                        initValue = new BigDecimal(initValueMap.get("init_value").toString());
                    }
                    BigDecimal value = new BigDecimal((mapResult.get("param"+(i+1)).toString()).trim()).subtract(initValue);
                    maxData = maxData == null? value: maxData.max(value);
                    minData = minData == null? value: minData.min(value);
                    Map<String, Object> map1 = new HashMap<>();
                    map1.put("name", time);
                    List<Object> list1 = new ArrayList<>();
                    list1.add(time);
                    list1.add(value);
                    list.add(list1);
                    map1.put("value", list1);
                    mapList.add(map1);
                }
            }
            if(mapList.size()>0){
                map.put("maxData", maxData);
                map.put("minData", minData);
//                List<Map<String,Object>> list2 = new ArrayList<>();
//                if(tabDeviceType.isEmpty()){
//                    Map<String,Object> map2 = new HashMap<>();
//                    map2.put("scale",  0);
//                    map2.put("type", 0);
//                    list2.add(map2);
//                }else {
//                    for(TabDeviceType tabDeviceType1: tabDeviceType){
//                        if(tabDeviceType1 != null){
////                        map.put("deviceScale", tabDeviceType1.getScale() == null ? 0 : tabDeviceType1.getScale());
//                            Map<String,Object> map2 = new HashMap<>();
//                            map2.put("scale", StringUtils.isEmpty(tabDeviceType1.getScale()) ? 0 : tabDeviceType1.getScale());
//                            map2.put("type", StringUtils.isEmpty(tabDeviceType1.getType()) ? 0 : tabDeviceType1.getType());
//                            list2.add(map2);
//                        }
//                    }
//                }
                map.put(names.length == 1 ? names[0] : names[1],mapList);
//                map.put("scale",list2);
            }
        }
    }

    /**
     * 转换对应的结果集
     * @param results
     * @param map
     * @param name
     */
    private void getParam(String sensorNo,List<Map<String, Object>> results, Map<String, Object> map,String ... name){
        if (results.size() <= 0) {
            return ;
        }
        BigDecimal maxData = null;
        BigDecimal minData = null;
        for(int i=0;i<name.length;i++){
            List<Map<String, Object>> mapList = new ArrayList<>();
            List<List<Object>> list = new ArrayList<>();
            Map<String,Object> initValueMap = tabSensorInitMapper.getDataByFiledName(sensorNo,name[i]);
            for (Map<String, Object> mapResult:results) {
                Object time = mapResult.get("time");
                if (mapResult.get("param"+(i+1)) != null && mapResult.get("param"+(i+1)) !="") {
                    BigDecimal initValue = BigDecimal.ZERO;
                    if(null != initValueMap && null != initValueMap.get("init_value")){
                        initValue = new BigDecimal(initValueMap.get("init_value").toString());
                    }
                    BigDecimal value = new BigDecimal(mapResult.get("param"+(i+1)).toString()).subtract(initValue);
                    maxData = maxData == null? value: maxData.max(value);
                    minData = minData == null? value: minData.min(value);
                    Map<String, Object> map1 = new HashMap<>();
                    map1.put("name", time);
                    List<Object> list1 = new ArrayList<>();
                    list1.add(time);
                    list1.add(value);
                    list.add(list1);
                    map1.put("value", list1);
                    mapList.add(map1);
                }
            }
            if(mapList.size()>0){
                map.put("maxData", maxData);
                map.put("minData", minData);
                map.put(name[i],mapList);
            }
        }
    }
    private  String dateFormat(String str) {
        return str.replace("Z", "").replace("T"," ");
    }

    /**
     * SELECT DISTINCT DATE_FORMAT(adate_time,'%Y-%m-%d') as time, value ,value1 as  height   FROM tab_shenbuweiyi_new
     * WHERE device_no = #{deviceNo}
     * 深部位移结果处理
     * @return
     */
    private Map<String, Object> getShenBuMap(String name1, List<Map<String, Object>> results, Map<String, Object> map) {
        if (results.size() <= 0) {
            return null;
        }
        List<Map<String, Object>> mapList = new ArrayList<>();
        int i = 0;
        //循环所有时间
        for (Map result : results) {
            i++;
            //得到日期
            String time = String.valueOf(result.get("time"));
            String onTime = "";
            if (i > 1){
                onTime = String.valueOf(results.get(i-2).get("time"));
            }
            if (i == 1 || !time.equals(onTime)){
                Map<String, Object> map1 = new HashMap();
                map1.put("day",time);
                List<Object> values = new ArrayList<>();
                List<Object> heights = new ArrayList<>();
                for (int k = i-1; k < results.size(); k++ ){
                    Map<String,Object> res = results.get(k);
                    if (time.equals(res.get("time"))){
                        values.add(res.get("value"));
                        heights.add(res.get("height"));
                    }else{
                        break;
                    }

                }
                map1.put("values",values);
                map1.put("heights",heights);
                mapList.add(map1);
            }else{
                continue;
            }

        }
        map.put("shenbuweiyi",mapList);
        return map;
    }
}
