package com.cqndt.disaster.device.map;

import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.domain.TabMap;

import java.util.List;

public interface TabMapService {
    /**
     *获取所有底图信息
     */
    Result getAllMap();
}
