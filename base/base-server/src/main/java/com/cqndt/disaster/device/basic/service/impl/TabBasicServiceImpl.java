package com.cqndt.disaster.device.basic.service.impl;

import com.cqndt.disaster.device.basic.service.TabBasicService;
import com.cqndt.disaster.device.dao.*;
import com.cqndt.disaster.device.domain.*;
import com.cqndt.disaster.device.vo.TabBasicVo;
import com.cqndt.disaster.device.vo.TabDeviceVo;
import com.cqndt.disaster.device.vo.TabHumanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/15  15:28
 * Description:灾害点
 */
@Service
public class TabBasicServiceImpl implements TabBasicService {
    @Autowired
    private TabBasicMapper tabBasicMapper;
    @Autowired
    private TabPlanInfoMapper tabPlanInfoMapper;
    @Autowired
    private TabDisasterCardMapper tabDisasterCardMapper;
    @Autowired
    private TabHedgeCardMapper tabHedgeCardMapper;
    @Autowired
    private TabHumanMapper tabHumanMapper;
    @Autowired
    private TabDeviceMapper tabDeviceMapper;
    @Value("${file-ip}")
    private String fileIp;

    @Override
    public List<TabBasicVo> selectTabBasic(TabBasicVo tabBasic) {
        return tabBasicMapper.selectTabBasic(tabBasic);
    }

    @Override
    public TabBasicWithBLOBs selectById(Integer id){
        return tabBasicMapper.selectByPrimaryKey(id);
    }
    @Override
    public Map<String,Object> selectCards(String disNo){
        Map<String,Object> map = new HashMap<>();
        List<TabPlanInfoWithBLOBs> planInfos = tabPlanInfoMapper.selectByDisNo(disNo);
        if(planInfos.size()>0){
            for (TabPlanInfoWithBLOBs info:planInfos) {
                if(null != info.getpLinePic()){
                    info.setpLinePic(fileIp+File.separator+"img"+File.separator+info.getpLinePic());
                }
            }
        }
        List<TabDisasterCard> disasterCards = tabDisasterCardMapper.selectByDisNo(disNo);
        List<TabHedgeCard> hedgeCards = tabHedgeCardMapper.selectByDisNo(disNo);
        map.put("planInfos",planInfos);
        map.put("disasterCards",disasterCards);
        map.put("hedgeCards",hedgeCards);
        return map;
    }
    @Override
    public List<Map<String, Object>> selectHumanByDisNo(String disNo){
        List<Map<String, Object>> humanList = tabHumanMapper.selectByDisNo(disNo);
        return humanList;
    }

    @Override
    public TabBasicVo selectTabBasicById(Integer id) {
        return tabBasicMapper.selectTabBasicById(id);
    }

    @Override
    public List<TabDeviceVo> getDeviceByDisNo(String disNo) {
        return tabDeviceMapper.getDeviceByDisNo(disNo);
    }

}
