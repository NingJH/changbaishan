package com.cqndt.disaster.device.equip.service.impl;

import com.cqndt.disaster.device.common.util.HttpClientUtil;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.common.util.SudasSmsUtil;
import com.cqndt.disaster.device.dao.*;
import com.cqndt.disaster.device.domain.TabAlarmInfo;
import com.cqndt.disaster.device.domain.TabDevice;
import com.cqndt.disaster.device.equip.service.TabAlarmInfoService;
import com.cqndt.disaster.device.vo.TabAlarmInfoVo;
import com.cqndt.disaster.device.vo.TabMsgRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created By marc
 * Date: 2019/4/22  15:05
 * Description:
 */
@Service
public class TabAlarmInfoServiceImpl implements TabAlarmInfoService {

    @Autowired
    private TabAlarmInfoMapper tabAlarmInfoMapper;

    @Autowired
    private TabDeviceMapper tabDeviceMapper;

    @Autowired
    TabMsgRecordMapper tabMsgRecordMapper;

    @Autowired
    TabUnionMapper tabUnionMapper;

    @Override
    public List<TabAlarmInfoVo> listTabAlarmInfo(TabAlarmInfoVo vo) {
        return tabAlarmInfoMapper.listTabAlarmInfo(vo);
    }

    @Override
    public List<Map<String, Object>> queryTabAlarmInfoList(TabAlarmInfoVo vo) {
        return tabAlarmInfoMapper.queryTabAlarmInfoList(vo);
    }

    @Override
    public TabAlarmInfoVo getDetailByMonitorId(TabAlarmInfoVo vo) {
        return tabAlarmInfoMapper.getDetailByMonitorId(vo);
    }

    @Override
    public TabAlarmInfoVo getDetailById(String id) {
        return tabAlarmInfoMapper.getDetailById(id);
    }

    @Override
    public List<TabAlarmInfoVo> getAlarmsByMonitorId(TabAlarmInfoVo vo) {
        return tabAlarmInfoMapper.listTabAlarmInfo(vo);
    }

    @Override
    public Result updateAlarmByStatusSee(Integer alarmId) {
        tabAlarmInfoMapper.updateByStatusView(alarmId);
        return new Result().success();
    }

    @Override
    public Result updateAlarmByStatusMistake(Integer alarmId) {
        tabAlarmInfoMapper.updateAlarmByStatusMistake(alarmId);
        return new Result().success();
    }

    @Override
    public Result updateAlarmByStatusSuccess(Integer alarmId, String content) {

        tabAlarmInfoMapper.updateAlarmByStatusSuccess(alarmId, content);

        return new Result().success();
    }

    @Override
    public Result updateAlarmByUploadSms(Integer alarmId) {

        TabAlarmInfo tabAlarmInfo = tabAlarmInfoMapper.selectByPrimaryKey(alarmId);

        if(tabAlarmInfo == null){
            return new Result().failure(-1, "告警记录未发现！");
        }

        List<Map<String, Object>> personsList = tabAlarmInfoMapper.getAlarmPersons();

        Set<String> pList = personsList.stream().filter(map -> ("6".equals(map.get("type")) && Integer.parseInt(map.get("level").toString()) == tabAlarmInfo.getLevel())).map(map -> map.get("phone").toString()).collect(Collectors.toSet());

        if(tabAlarmInfo.getAlarmType() == 3){
            // 联合告警
            List<String> deviceIds = tabUnionMapper.getDeviceNoList(Integer.parseInt(tabAlarmInfo.getSensorNo()));

            List<Map<String, Object>> monitorPersonList = tabDeviceMapper.getMonitorPersionByDeviceId(deviceIds);

            for(Map<String, Object> monitorPerson : monitorPersonList){
                pList.add(monitorPerson.get("phone").toString());
            }

        } else {
            String deviceNo = tabAlarmInfo.getSensorNo().substring(0, tabAlarmInfo.getSensorNo().length()-6);

            TabDevice tabDevice = tabDeviceMapper.getDevice(deviceNo);

            if(tabDevice == null){
                return new Result().failure(-1, "该告警对应的设备不存在！");
            }

            String monitorPerson = tabDevice.getMonitorPerson();

            String phone = tabDeviceMapper.getMonitorPersonPhoneByDevice(monitorPerson);
            if(!StringUtils.isEmpty(phone)){
                pList.add(phone);
            }
        }

        SudasSmsUtil.send(pList.stream().collect(Collectors.joining(",")), tabAlarmInfo.getSmsContent());

        for(String p : pList){
            TabMsgRecord tabMsgRecord = new TabMsgRecord();
            tabMsgRecord.setSendPerson("yunnanAdmin");
            tabMsgRecord.setPhone(p);
            tabMsgRecord.setSendState(1);
            tabMsgRecord.setContent(tabAlarmInfo.getSmsContent());
            tabMsgRecord.setSendWay(2);
            tabMsgRecordMapper.addTabMsgRecord(tabMsgRecord);
        }

        int i = tabAlarmInfoMapper.updateAlarmBySms(alarmId);

        return new Result().success();
    }

    @Override
    public Result alarm(String deviceFactory,String deviceNo,int isOnline) {
//        Map<String,Object> map = new HashMap<>();
//        map.put("deviceNo",deviceNo);
//        String param = JSON.toJSONString(map);//设备编号
        Object obj = null;
        String url = null;
        String param = "[{\"deviceNo\":\""+deviceNo+"\"}]";

        if("13".equals(deviceFactory)||"14".equals(deviceFactory)){
            url = "http://localhost:9072/api1.0/contrl/alarm?shoudLight="+isOnline;//0,关，1,开
            obj = HttpClientUtil.post(url,param,"");

        }else {
            url = "http://172.21.203.6:8108/api/1.0/command/voice/"+deviceNo+"/"+isOnline;
            obj = HttpClientUtil.post(url);//
        }

        return new Result().success(obj);
    }

}
