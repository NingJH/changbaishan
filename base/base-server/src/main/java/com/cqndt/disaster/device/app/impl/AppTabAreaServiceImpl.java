package com.cqndt.disaster.device.app.impl;

import com.cqndt.disaster.device.app.AppTabAreaService;
import com.cqndt.disaster.device.dao.app.AppTabAreaMapper;
import com.cqndt.disaster.device.vo.TabAreaVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By marc
 * Date: 2019/5/12  17:05
 * Description:
 */
@Service
public class AppTabAreaServiceImpl implements AppTabAreaService {
    @Autowired
    private AppTabAreaMapper appTabAreaMapper;

    @Override
    public TabAreaVo cityLinkage(String areaCode, String level) {
        TabAreaVo vo = new TabAreaVo();
        List<TabAreaVo> areas = new ArrayList<>();
        switch (level) {
            case "1":
                areas = appTabAreaMapper.getNextArea(areaCode);
                areas.forEach(city -> {
                    List<TabAreaVo> area = appTabAreaMapper.getNextArea(city.getAreaCode().toString());
                    city.setAreaList(area);
                });
                List<TabAreaVo> list = appTabAreaMapper.getArea(areaCode);
                vo = list.get(0);
                vo.setAreaList(areas);
                break;
            case "2":
                List<TabAreaVo> list1 = appTabAreaMapper.getPreArea(areaCode);
                vo = list1.get(0);
                List<TabAreaVo> listArea = appTabAreaMapper.getArea(areaCode);
                listArea.forEach(city -> {
                    List<TabAreaVo> area = appTabAreaMapper.getNextArea(city.getAreaCode().toString());
                    city.setAreaList(area);
                });
                vo.setAreaList(listArea);
                break;
            case "3":
                List<TabAreaVo> list2 = appTabAreaMapper.getPreArea(areaCode);
                list2.forEach(city -> {
                    List<TabAreaVo> area = appTabAreaMapper.getNextArea(city.getAreaCode().toString());
                    city.setAreaList(area);
                });
                List<TabAreaVo> listp = appTabAreaMapper.getPreArea(list2.get(0).getAreaCode().toString());
                vo = listp.get(0);
                vo.setAreaList(list2);
                break;
        }
        return vo;
    }
}
