package com.cqndt.disaster.device.equip.service;

import com.cqndt.disaster.device.domain.TabWarnSetting;
import com.cqndt.disaster.device.vo.DeviceInProjectVo;
import com.cqndt.disaster.device.vo.SearchVo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/10  17:52
 * Description:获取曲线数据
 */
public interface AnalysisService {
    /**
     * 获取数据曲线
     * @param vo
     * @return
     */
    Map<String, Object> selecteChart(SearchVo vo);

    /**
     * 根据项目id查询该项目下所有的设备
     * @param projectId
     * @return
     */
    List<DeviceInProjectVo> selectDeviceByProjectId(String projectId);

    TabWarnSetting getSettingBySensorNo(String sensorNo);

    void dataExport(String deviceNos, String startTime, String endTime, HttpServletRequest request, HttpServletResponse response);
}
