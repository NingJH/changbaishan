package com.cqndt.disaster.device.app.impl;

import com.cqndt.disaster.device.app.AppTabPhotographyService;
import com.cqndt.disaster.device.common.service.TabPhotographyService;
import com.cqndt.disaster.device.dao.TabPhotographyMapper;
import com.cqndt.disaster.device.dao.app.AppTabPhotographyMapper;
import com.cqndt.disaster.device.domain.TabPhotography;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/12  15:39
 * Description:获取倾斜摄影
 */
@Service
public class AppTabPhotographyServiceImpl implements AppTabPhotographyService {
    @Autowired
    private AppTabPhotographyMapper tabPhotographyMapper;
    @Value("${file-ip}")
    private String fileIp;

    @Override
    public Map<String,Object> selectByProjectId(Integer projectId, String modelType) {
        Map<String,Object> map = tabPhotographyMapper.selectByProjectId(projectId,modelType);
        map.put("Panorama",fileIp+"/"+map.get("Panorama").toString());
        return map;
    }
}
