package com.cqndt.disaster.device.app.impl;

import com.cqndt.disaster.device.app.AppTabReportingFileService;
import com.cqndt.disaster.device.dao.TabReportingFileMapper;
import com.cqndt.disaster.device.dao.app.AppTabReportingFileMapper;
import com.cqndt.disaster.device.project.service.TabReportingFileService;
import com.cqndt.disaster.device.vo.TabDoc;
import com.cqndt.disaster.device.vo.TabDocCollection;
import com.cqndt.disaster.device.vo.TabReportFileVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

/**
 * Created By marc
 * Date: 2019/5/11  9:47
 * Description:
 */
@Service
public class AppTabReportingFileServiceImpl implements AppTabReportingFileService {
    @Autowired
    private AppTabReportingFileMapper tabReportingFileMapper;
    @Value("${file-ip}")
    private String fileIp;

    @Override
    public TabDocCollection listReportingFile(TabReportFileVo vo) {
        vo.setPage((vo.getPage() - 1) * vo.getLimit());
        List<TabDoc> list = tabReportingFileMapper.listReportingFile(vo);
        for (TabDoc tabDoc:list) {
            tabDoc.setPreviewUrl(fileIp+ File.separator+"file"+File.separator+tabDoc.getDocUrl());
        }
        TabDocCollection tabDocCollection = new TabDocCollection();
        tabDocCollection.setTabDocList(list);
        tabDocCollection.setTotalCount(list.size());
        return tabDocCollection;
    }
}
