package com.cqndt.disaster.device.app;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/12  15:10
 * Description:
 */
public interface AppWarnStatisticsService {
    /**
     * 查询告警各等级的总数
     * @param map
     * @return
     */
    List<Map<String,Object>> findWarnStatictics(Map<String,Object> map);

    /**
     * 综合统计-预警告警详情列表接口：统计当天、本周、当月的告警详情数据（项目名称，设备名称，设备类型，告警等级，设备经纬度）；
     * @param map
     * @return
     */
    List<Map<String,Object>>  findWarnMsg(Map<String,Object> map);

    /**
     *  查询各类型、等级告警的总数
     * @param map
     * @return
     */
    List<Map<String,Object>> findWarnCount(Map<String,Object> map);

}
