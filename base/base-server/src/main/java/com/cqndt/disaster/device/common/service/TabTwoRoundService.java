package com.cqndt.disaster.device.common.service;

import com.cqndt.disaster.device.domain.TabTwoRound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/12  15:43
 * Description:获取全景图
 */
public interface TabTwoRoundService {
    /**
     * 全景图
     * @param relationNo
     * @return
     */
    List<TabTwoRound> selectByRelationNo(String relationNo,String modelType);
}
