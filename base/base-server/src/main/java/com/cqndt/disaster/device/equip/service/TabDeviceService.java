package com.cqndt.disaster.device.equip.service;

import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.domain.*;
import com.cqndt.disaster.device.vo.TabDeviceVo;
import com.cqndt.disaster.device.vo.TabSensorVo;
import com.cqndt.disaster.device.vo.TabUserVo;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/9  16:04
 * Description:
 */
public interface TabDeviceService {
    /**
     * 获取设备列表
     *
     * @param vo
     * @return
     */
    List<TabDeviceVo> listTabDevice(TabDeviceVo vo);

    /**
     * 获取单个设备
     *
     * @param id
     * @return
     */
    TabDeviceVo getTabDevice(Integer id);

    /**
     * 根据设备编号获取设备巡查信息
     *
     * @param deviceNo
     * @return
     */
    List<TabDeviceCheck> selectByDeviceNo(String deviceNo);

    /**
     * 根据设备编号获取设备安装信息
     *
     * @param deviceNo
     * @return
     */
    TabDeviceInstallWithBLOBs selectInstallByDeviceNo(String deviceNo);

    /**
     * 根据监测点编号获取设备对应的传感器
     *
     * @param monitorNo
     * @return
     */
    List<TabSensorVo> getSensorByMonitorNo(String monitorNo);

    Result getAllDeviceOnline(Integer id);

    Result getDeviceOnlineByCompany(String ids, Integer id);

    Result getCompanys(Integer id);

    Result getDeviceOnlineByProject(String ids, Integer id);

    Result getDeviceOnlineByType(Integer id);

    /**
     * 修改设备刻度
     * @param id
     * @param deviceScale
     * @return
     */
    Result updateDeviceById(Integer id, String deviceScale);

    Result updateScale(Integer typeId,String scale);

    Result selectDeviceType(String projectId);

    Result selectDeviceType(Integer deviceType,String monitorNo);

    Result getSensorType();

    Map<String, Object> selectintervalsTime(String deviceNo);
}
