package com.cqndt.disaster.device.app;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/12  14:49
 * Description:
 */
public interface AppTabStaticTypeService {

    List<Map<String,Object>> selectByStaticNum(String staticNum);
}
