package com.cqndt.disaster.device.equip.service;

import com.cqndt.disaster.device.domain.TabOrderLog;
import com.cqndt.disaster.device.vo.TabOrderLogVo;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/5/7  20:34
 * Description:
 */
public interface TabOrderLogService {
    /**
     * 增加
     * @param tabOrderLog
     */
    void save(TabOrderLog tabOrderLog);

    /**
     * 已发送指令列表
     * @param vo
     * @return
     */
    List<TabOrderLogVo> listOrderLog(TabOrderLogVo vo);
}
