package com.cqndt.disaster.device.equip.service.impl;

import com.cqndt.disaster.device.common.influxdb.InfluxDBConnect;
import com.cqndt.disaster.device.common.shrio.ShiroUtils;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.dao.*;
import com.cqndt.disaster.device.domain.*;
import com.cqndt.disaster.device.equip.service.TabDeviceService;
import com.cqndt.disaster.device.vo.TabDeviceVo;
import com.cqndt.disaster.device.vo.TabSensorVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created By marc
 * Date: 2019/4/9  16:04
 * Description:
 */
@Service
public class TabDeviceServiceImpl implements TabDeviceService {
    @Autowired
    private TabDeviceMapper tabDeviceMapper;
    @Autowired
    private TabDeviceCheckMapper tabDeviceCheckMapper;
    @Autowired
    private TabDeviceInstallMapper tabDeviceInstallMapper;
    @Autowired
    private TabSensorMapper tabSensorMapper;
    @Autowired
    private TabStaticTypeMapper tabStaticTypeMapper;
    @Autowired
    private TabProjectMapper tabProjectMapper;
    @Autowired
    private TabDeviceTypeMapper tabDeviceTypeMapper;
    @Autowired
    private TbsLfwyMapper tbsLfwyMapper;
    @Autowired
    private TbsQjyWbxMapper tbsQjyWbxMapper;
    @Autowired
    private TbsDbbxHpMapper tbsDbbxHpMapper;
    @Autowired
    private TbsGnssMapper tbsGnssMapper;
    @Autowired
    private TbsSbwyQxyMapper tbsSbwyQxyMapper;
    @Autowired
    private TbsChenjiangMapper tbsChenjiangMapper;
    @Autowired
    private TbsKongxishuiyaMapper tbsKongxishuiyaMapper;
    @Autowired
    private TbsGangjinjiMapper tbsGangjinjiMapper;
    @Autowired
    private TbsTylMapper tbsTylMapper;
    @Autowired
    private TbsYlMapper tbsYlMapper;
    @Autowired
    private TbsShuiweijiMapper tbsShuiweijiMapper;
    @Autowired
    private TbsYingbianMapper tbsYingbianMapper;
    @Autowired
    private TbsWenshiduMapper tbsWenshiduMapper;
    @Autowired
    private TbsNiweijiMapper tbsNiweijiMapper;
    @Autowired
    private TbsZhendongMapper tbsZhendongMapper;
    @Autowired
    private TbsLxdbdMapper tbsLxdbdMapper;
    @Autowired
    private TbsDxswMapper tbsDxswMapper;
    @Autowired
    private TbsTotalStationMapper tbsTotalStationMapper;
    @Autowired
    private TbsRgszMapper tbsRgszMapper;
    @Autowired
    private TbsQxyMapper tbsQxyMapper;
    @Autowired
    private TbsSbwyLxMapper tbsSbwyLxMapper;
    @Autowired
    private TbsSbwyCxMapper tbsSbwyCxMapper;
    @Autowired
    private TbsSxcfjMapper tbsSxcfjMapper;
    @Autowired
    private TbsQjyMapper tbsQjyMapper;
    @Autowired
    private TbsZxsyljMapper tbsZxsyljMapper;
    @Autowired
    private TbsDishengMapper tbsDishengMapper;
    @Autowired
    private TbsCishengMapper tbsCishengMapper;
    @Autowired
    private TbsWwjMapper tbsWwjMapper;
    @Autowired
    private TbsGdqxMapper tbsGdqxMapper;
    @Autowired
    private TbsBmwyMapper tbsBmwyMapper;
    @Autowired
    private TbsLsyMapper tbsLsyMapper;
    @Autowired
    private TbsHslMapper tbsHslMapper;

    @Autowired
    private InfluxDBConnect influxDBConnect;


    @Override
    public List<TabDeviceVo> listTabDevice(TabDeviceVo vo) {
       /* if(null != vo.getAreaCode()){
            vo.setAreaCodeSplit(vo.getAreaCode().substring(0,2));
        }*/
        List<TabDeviceVo> tabDeviceVoList = tabDeviceMapper.listTabDevice(vo);
//        tabDeviceVoList.forEach(tabDeviceVo -> {
//            if(tabDeviceVo.getDeviceNo().contains("_")){
//                tabDeviceVo.setDeviceNo(tabDeviceVo.getDeviceNo().substring(0,tabDeviceVo.getDeviceNo().length()-5));
//            }
//        });
        return tabDeviceVoList;
    }

    @Override
    public TabDeviceVo getTabDevice(Integer id) {
        List<TabStaticType> list = tabStaticTypeMapper.selectByStaticNum("11");
        Integer stateTime = 24;
        if (list.size() > 0) {
            stateTime = list.get(0).getStaticKeyval();
        }
        return tabDeviceMapper.getTabDevice(id, stateTime);
    }

    @Override
    public List<TabDeviceCheck> selectByDeviceNo(String deviceNo) {
        return tabDeviceCheckMapper.selectByDeviceNo(deviceNo);
    }

    @Override
    public TabDeviceInstallWithBLOBs selectInstallByDeviceNo(String deviceNo) {
        return tabDeviceInstallMapper.selectInstallByDeviceNo(deviceNo);
    }

    @Override
    public List<TabSensorVo> getSensorByMonitorNo(String monitorNo) {
        return tabSensorMapper.getSensorByMonitorNo(monitorNo);
    }

    @Override
    public Result getAllDeviceOnline(Integer id) {
        int onlineTemp = 0;
        Map<String, Object> map = new HashMap<>(16);
        Double onlineCount = tabDeviceMapper.getAllDeviceOnline(id);
        onlineCount += onlineTemp;
        Double offlineCount = tabDeviceMapper.getAllDeviceOffline(id);
        Double onlineProportion = onlineCount / (onlineCount + offlineCount);
        onlineProportion = (double) Math.round(onlineProportion * 100) / 100;
        map.put("online", onlineCount);
        map.put("offline", offlineCount);
        map.put("proportion", onlineProportion);
        return new Result().success(map);
    }

    @Override
    public Result getDeviceOnlineByCompany(String ids, Integer id) {
        if ("".equals(ids) || null == ids) {
            List<String> companys = tabDeviceMapper.getCompany(id);
            List<Map<String, Object>> maplist = new ArrayList<>(16);
            List<Map<String, Object>> maplist3 = new ArrayList<>(16);
            Map<String, Object> mapAll = new HashMap<>(16);
            for (String company : companys) {
                String companyName = tabDeviceMapper.selectCompanyNameByS(company);

                Map<String, Object> map2 = new HashMap<>(16);

                Map<String, Object> mapOnline = new HashMap<>(16);
                List<Map<String, Object>> list = new ArrayList<>(16);
                Double allCount = tabDeviceMapper.getDeviceAllByCompany(id, company);
                Double offlineCount = tabDeviceMapper.getDeviceOfflineByCompany(id, company);
                Double offlineProportion = offlineCount / allCount;
                Double onlineProportion = (allCount - offlineCount) / allCount;
                offlineProportion = (double) Math.round(offlineProportion * 100) / 100;
                onlineProportion = (double) Math.round(onlineProportion * 100) / 100;
                //在线率
                mapOnline.put("onlineProportion", onlineProportion);
                //离线率
                mapOnline.put("offlineProportion", offlineProportion);
                //在线数
                mapOnline.put("online", allCount - offlineCount);
                //离线数
                mapOnline.put("offline", offlineCount);

                mapOnline.put("companyName", companyName);


                //设备类型分类
                List<Map<String, Object>> deviceList = tabDeviceMapper.getDeviceTypeByCompany(id, company);
                List<Map<String, Object>> maplist2 = new ArrayList<>(16);
                List<Map<String, Object>> maplist4 = new ArrayList<>(16);
                for (Map<String, Object> deviceInfo : deviceList) {
                    String deviceName = deviceInfo.get("device_type_name").toString();
                    String deviceType = deviceInfo.get("device_type").toString();
                    Map<String, Object> map1 = new HashMap<>(16);
                    Double allCountType = tabDeviceMapper.getAllDeviceByTypeCompany(id, deviceType, company);
                    Double online = tabDeviceMapper.getDeviceOnlineByTypeCompany(id, deviceType, company);
                    Double offline = allCountType - online;
                    Double offlineProportionType = offline / allCountType;
                    Double onlineProportionType = online / allCountType;
                    offlineProportionType = (double) Math.round(offlineProportionType * 100) / 100;
                    onlineProportionType = (double) Math.round(onlineProportionType * 100) / 100;
                    map1.put("offlineProportionDType", offlineProportionType);
                    map1.put("onlineProportionDType", onlineProportionType);
                    map1.put("onlineDTpye", online);
                    map1.put("offlineDTpye", offline);
                    map1.put("deviceName", deviceName);
                    maplist2.add(map1);
                }
                map2.put("companyName", companyName);
                map2.put("deviceInfo", maplist2);
                maplist4.add(map2);

                list.add(mapOnline);
                maplist.addAll(list);

                maplist3.addAll(maplist4);
            }
            mapAll.put("company", maplist);
            mapAll.put("deviceTpye", maplist3);
            return new Result().success(mapAll);
        } else {
            String[] split = ids.split(",");
            List<Map<String, Object>> maplist = new ArrayList<>(16);
            List<Map<String, Object>> maplist3 = new ArrayList<>(16);
            Map<String, Object> mapAll = new HashMap<>(16);
            for (String company : split) {
                String companyName = tabDeviceMapper.selectCompanyNameByS(company);

                Map<String, Object> map2 = new HashMap<>(16);

                Map<String, Object> mapOnline = new HashMap<>(16);
                List<Map<String, Object>> list = new ArrayList<>(16);
                Double allCount = tabDeviceMapper.getDeviceAllByCompany(id, company);
                Double offlineCount = tabDeviceMapper.getDeviceOfflineByCompany(id, company);
                Double offlineProportion = offlineCount / allCount;
                Double onlineProportion = (allCount - offlineCount) / allCount;
                offlineProportion = (double) Math.round(offlineProportion * 100) / 100;
                onlineProportion = (double) Math.round(onlineProportion * 100) / 100;
                //在线率
                mapOnline.put("onlineProportion", onlineProportion);
                //离线率
                mapOnline.put("offlineProportion", offlineProportion);
                //在线数
                mapOnline.put("online", allCount - offlineCount);
                //离线数
                mapOnline.put("offline", offlineCount);

                mapOnline.put("companyName", companyName);


                //设备类型分类
                List<Map<String, Object>> deviceList = tabDeviceMapper.getDeviceTypeByCompany(id, company);
                List<Map<String, Object>> maplist2 = new ArrayList<>(16);
                List<Map<String, Object>> maplist4 = new ArrayList<>(16);
                for (Map<String, Object> deviceInfo : deviceList) {
                    String deviceName = deviceInfo.get("device_type_name").toString();
                    String deviceType = deviceInfo.get("device_type").toString();
                    Map<String, Object> map1 = new HashMap<>(16);
                    Double allCountType = tabDeviceMapper.getAllDeviceByTypeCompany(id, deviceType, company);
                    Double online = tabDeviceMapper.getDeviceOnlineByTypeCompany(id, deviceType, company);
                    Double offline = allCountType - online;
                    Double offlineProportionType = offline / allCountType;
                    Double onlineProportionType = online / allCountType;
                    offlineProportionType = (double) Math.round(offlineProportionType * 100) / 100;
                    onlineProportionType = (double) Math.round(onlineProportionType * 100) / 100;
                    map1.put("offlineProportionDType", offlineProportionType);
                    map1.put("onlineProportionDType", onlineProportionType);
                    map1.put("onlineDTpye", online);
                    map1.put("offlineDTpye", offline);
                    map1.put("deviceName", deviceName);
                    maplist2.add(map1);
                }
                map2.put("companyName", companyName);
                map2.put("deviceInfo", maplist2);
                maplist4.add(map2);

                list.add(mapOnline);
                maplist.addAll(list);

                maplist3.addAll(maplist4);
            }
            mapAll.put("company", maplist);
            mapAll.put("deviceTpye", maplist3);
            return new Result().success(mapAll);
        }
    }

    @Override
    public Result getCompanys(Integer id) {
        List<Map<String, Object>> companys = tabDeviceMapper.getCompanys(id);
        return new Result().success(companys);
    }

    @Override
    public Result getDeviceOnlineByProject(String ids, Integer id) {
        List<Integer> projectIds = tabProjectMapper.getAllProject(id);
        List<Map<String, Object>> maplist = new ArrayList<>(16);
        if ("".equals(ids) || null == ids) {
            for (Integer projectId : projectIds) {
                TabProject tabProject = tabProjectMapper.selectByPrimaryKey(projectId);
                String projectName = tabProject.getProjectName();

                Map<String, Object> mapOnline = new HashMap<>(16);
                Double all = tabDeviceMapper.getAllDeviceByProject(projectId, id);
                Double online = tabDeviceMapper.getDeviceOnlineByProject(projectId, id);
                Double onlineProportion = online / all;
                Double offlineProportion = (all - online) / all;

                onlineProportion = (double) Math.round(onlineProportion * 100) / 100;
                offlineProportion = (double) Math.round(offlineProportion * 100) / 100;
                mapOnline.put("onlineProportion", onlineProportion);
                mapOnline.put("offlineProportion", offlineProportion);
                mapOnline.put("online", online);
                mapOnline.put("offline", all - online);
                mapOnline.put("projectName", projectName);

                if (online == 0) {
                    mapOnline.put("offlineProportion", 1);
                }
                maplist.add(mapOnline);
            }
            return new Result().success(maplist);
        } else {
            String[] companys = ids.split(",");
            for (Integer projectId : projectIds) {
                for (String company : companys) {
                    TabProject tabProject = tabProjectMapper.selectByPrimaryKey(projectId);
                    String projectName = tabProject.getProjectName();

                    Map<String, Object> mapOnline = new HashMap<>(16);
                    Double all = tabDeviceMapper.getAllDeviceByProjectC(company, projectId, id);
                    if (all > 0) {
                        Double online = tabDeviceMapper.getDeviceOnlineByProjectC(company, projectId, id);
                        Double onlineProportion = online / all;
                        Double offlineProportion = (all - online) / all;

                        onlineProportion = (double) Math.round(onlineProportion * 100) / 100;
                        offlineProportion = (double) Math.round(offlineProportion * 100) / 100;
                        mapOnline.put("onlineProportion", onlineProportion);
                        mapOnline.put("offlineProportion", offlineProportion);
                        mapOnline.put("online", online);
                        mapOnline.put("offline", all - online);
                        mapOnline.put("projectName", projectName);

                        if (online == 0) {
                            mapOnline.put("offlineProportion", 1);
                        }
                        maplist.add(mapOnline);
                    }
                }
            }
            return new Result().success(maplist);
        }
    }

    @Override
    public Result getDeviceOnlineByType(Integer id) {
        List<Map<String, Object>> mapList = tabDeviceMapper.getDeviceType(id);
        List<Map<String, Object>> list = new ArrayList<>(16);
        for (Map<String, Object> map : mapList) {
            Map<String, Object> countMap = new HashMap<>(16);
            Map<String, Object> myMap = new HashMap<>(16);
            String deviceName = map.get("device_type_name").toString();
            Double allCount = tabDeviceMapper.getAllDeviceByType(id, map.get("device_type").toString());
            Double online = tabDeviceMapper.getDeviceOnlineByType(id, map.get("device_type").toString());
            Double offline = allCount - online;
            Double onlineProportion = online / allCount;
            onlineProportion = (double) Math.round(onlineProportion * 100) / 100;
            countMap.put("online", online);
            countMap.put("offline", offline);
            countMap.put("onlineProportion", onlineProportion);
            myMap.put(deviceName, countMap);
            list.add(myMap);
        }
        return new Result().success(list);
    }

    @Override
    public Result updateDeviceById(Integer id, String deviceScale) {

        TabDevice tabDevice = tabDeviceMapper.selectByPrimaryKey(id);

        if(tabDevice == null){
            return new Result().failure(-1, "当前设备不存在");
        }

        tabDevice.setDeviceScale(deviceScale);
        tabDeviceMapper.updateByPrimaryKey(tabDevice);

        return new Result().success();
    }

    @Override
    public Result updateScale(Integer typeId,String scale) {
        return new Result().success(tabDeviceTypeMapper.updateScale(typeId,scale));
    }

    @Override
    public Result selectDeviceType(String projectId) {
        List<Map<String,Object>> deviceTypeName = tabDeviceTypeMapper.getDeviceTypeName(projectId,ShiroUtils.getUserId());
        for (Map<String, Object> map : deviceTypeName) {
            String type1 = map.get("deviceType").toString();
            List<Map<String,Object>> monitorName = tabDeviceTypeMapper.getMonitorName(projectId,type1);
            for (Map<String, Object> mn : monitorName) {
                String deviceType = mn.get("deviceType").toString();
                List<Map<String,Object>> type = tabDeviceTypeMapper.getType(deviceType);
                mn.put("children",type);
            }
            map.put("children",monitorName);
        }
        return new Result().success(deviceTypeName);
    }

    @Override
    public Result selectDeviceType(Integer deviceType,String monitorNo) {
        List<TabDeviceType> tabDeviceTypes =  tabDeviceTypeMapper.selectDeviceType1(deviceType,monitorNo);
        return new Result().success(tabDeviceTypes);
    }

    @Override
    public Result getSensorType() {
        return new Result().success(tabDeviceTypeMapper.getSensorType());
    }

    @Override
    public Map<String, Object> selectintervalsTime(String deviceNo) {
        List<TabSensor> tabSensorList = tabSensorMapper.getSensorByDeviceId(deviceNo);
        String sensorNo = tabSensorList.get(0).getSensorNo();
        String sensorType = tabDeviceTypeMapper.selectSensorTypeBySensorNo(sensorNo);
        if (sensorType == null || sensorType.equals("")){
            return new HashMap<>();
        }
        Map<String, Object> map = new HashMap<>();
        Integer type = Integer.valueOf(sensorType);
        switch (type) {
            //裂缝智能测报仪\裂缝计\裂缝位移计\滑动变位计 tbs_lfwy
            //滑动变位计 tbs_lfwy
            case 38:
            case 1:
                List<Map<String, Object>> tbsLfwy = tbsLfwyMapper.tbsLfwyIntervals(sensorNo);
                getParamSpecial(tbsLfwy, map, "value");
                break;
            //微变形倾角测量仪tbs_qjy_wbx
            case 2:
                List<Map<String, Object>> tbsQjyWbx = tbsQjyWbxMapper.tbsQjyWbxIntervals(sensorNo);
                getParamSpecial(tbsQjyWbx, map, "value");
                break;
            //滑坡地表变形测量仪tbs_dbbx_hp
            case 3:
                List<Map<String, Object>> tbsDbbxHp = tbsDbbxHpMapper.tbsDbbxHpIntervals(sensorNo);
                getParamSpecial(tbsDbbxHp, map, "value");
                break;
            //GNSS位移\地表位移(GPS)tbs_gnss
            case 4:
                List<Map<String, Object>> tbsGnss = tbsGnssMapper.tbsGnssIntervals(sensorNo);
                getParamSpecial(tbsGnss, map, "value");
                break;
            //深部位移\深部位移(倾斜仪)tbs_sbwy_qxy
            case 5:
                List<Map<String, Object>> tbsSbwyQxy = tbsSbwyQxyMapper.tbsSbwyQxyIntervals(sensorNo);
                getParamSpecial(tbsSbwyQxy, map, "value");
                break;
            //沉降/土体沉降 tbs_chenjiang
            case 6:
            case 35:
                List<Map<String, Object>> tbsChenjiang = tbsChenjiangMapper.tbsChenjiangIntervals(sensorNo);
                getParamSpecial(tbsChenjiang, map, "value");
                break;
            //孔隙水压 tbs_kongxishuiya
            case 7:
                List<Map<String, Object>> tbsKongxishuiya = tbsKongxishuiyaMapper.tbsKongxishuiyaIntervals(sensorNo);
                getParamSpecial(tbsKongxishuiya, map, "value");
                break;
            //钢筋计 tbs_gangjinji
            case 8:
                List<Map<String, Object>> tbsGangjinji = tbsGangjinjiMapper.tbsGangjinjiIntervals(sensorNo);
                getParamSpecial(tbsGangjinji, map, "value");
                break;
            //土压力tbs_tyl
            case 9:
                List<Map<String, Object>> tbsTyl = tbsTylMapper.tbsTylIntervals(sensorNo);
                getParamSpecial(tbsTyl, map, "value");
                break;
            //雨量tbs_yl
            case 10:
                List<Map<String, Object>> tbsYl = tbsYlMapper.tbsYlIntervals(sensorNo);
                getParamSpecial(tbsYl, map, "value");
                break;
            //土壤湿度\土壤含水率tbs_hsl
            case 11:
            case 39:
                List<Map<String, Object>> tbsHsl = tbsHslMapper.tbsHslIntervals(sensorNo);
                getParamSpecial(tbsHsl, map, "value");
                break;
            //水位计tbs_shuiweiji
            case 12:
                List<Map<String, Object>> tbsShuiweiji = tbsShuiweijiMapper.tbsShuiweijiIntervals(sensorNo);
                getParamSpecial(tbsShuiweiji, map, "value");
                break;
            //应变 tbs_yingbian
            case 13:
                List<Map<String, Object>> tbsYingbian = tbsYingbianMapper.tbsYingbianIntervals(sensorNo);
                getParamSpecial(tbsYingbian, map, "value");
                break;
            //温湿度tbs_wenshidu
            case 14:
                List<Map<String, Object>> tbsWenshidu = tbsWenshiduMapper.tbsWenshiduIntervals(sensorNo);
                getParamSpecial(tbsWenshidu, map, "value");
                break;
            //泥位tbs_niweiji
            case 15:
                List<Map<String, Object>> tbsNiweiji = tbsNiweijiMapper.tbsNiweijiIntervals(sensorNo);
                getParamSpecial(tbsNiweiji, map, "value");
                break;
            //振动tbs_zhendong
            case 16:
                List<Map<String, Object>> tbsZhendong = tbsZhendongMapper.tbsZhendongIntervals(sensorNo);
                getParamSpecial(tbsZhendong, map, "value");
                break;
            //地表位移(拉线式)tbs_lxdbd
            case 17:
                List<Map<String, Object>> tbsLxdbd = tbsLxdbdMapper.tbsLxdbdIntervals(sensorNo);
                getParamSpecial(tbsLxdbd, map, "value");
                break;
            //地下水(地下水位)tbs_dxsw
            case 18:
                List<Map<String, Object>> tbsDxsw = tbsDxswMapper.tbsDxswIntervals(sensorNo);
                getParamSpecial(tbsDxsw, map, "value");
                break;
            //全站仪tbs_total_station
            case 19:
                List<Map<String, Object>> tbsTotalStation = tbsTotalStationMapper.tbsTotalStationIntervals(sensorNo);
                getParamSpecial(tbsTotalStation, map, "value");
                break;
            //人工水准仪tbs_rgsz
            case 20:
                List<Map<String, Object>> tbsRgsz = tbsRgszMapper.tbsRgszIntervals(sensorNo);
                getParamSpecial(tbsRgsz, map, "value");
                break;
            //倾斜仪(WMT)tbs_qxy
            case 21:
                List<Map<String, Object>> tbsQxy = tbsQxyMapper.tbsQxyIntervals(sensorNo);
                getParamSpecial(tbsQxy, map, "value");
                break;
            //深部位移(拉线)tbs_sbwy_lx
            case 22:
                List<Map<String, Object>> tbsSbwyLx = tbsSbwyLxMapper.tbsSbwyLxIntervals(sensorNo);
                getParamSpecial(tbsSbwyLx, map, "value");
                break;
            // 深部位移(测斜)tbs_sbwy_cx
            case 23:
                List<Map<String, Object>> tbsSbwyCx = tbsSbwyCxMapper.tbsSbwyCxIntervals(sensorNo);
                getParamSpecial(tbsSbwyCx, map, "value");
                break;
            //三项测缝计tbs_sxcfj
            case 24:
                List<Map<String, Object>> tbsSxcfj = tbsSxcfjMapper.tbsSxcfjIntervals(sensorNo);
                getParamSpecial(tbsSxcfj, map, "value");
                break;
            //倾角仪tbs_qjy
            case 25:
                List<Map<String, Object>> tbsQjy = tbsQjyMapper.tbsQjyIntervals(sensorNo);
                getParamSpecial(tbsQjy, map, "value");
                break;
            // 振弦式应力计tbs_zxsylj
            case 26:
                List<Map<String, Object>> tbsZxsylj = tbsZxsyljMapper.tbsZxsyljIntervals(sensorNo);
                getParamSpecial(tbsZxsylj, map, "value");
                break;
            //地声tbs_disheng
            case 27:
                List<Map<String, Object>> tbsDisheng = tbsDishengMapper.tbsDishengIntervals(sensorNo);
                getParamSpecial(tbsDisheng, map, "value");
                break;
            //次声tbs_cisheng
            case 28:
                List<Map<String, Object>> tbsCisheng = tbsCishengMapper.tbsCishengIntervals(sensorNo);
                getParamSpecial(tbsCisheng, map, "value");
                break;
//            //地表位移（拉线式）tbs_lxdbd
//            case 29:
//                break;
            //物位计tbs_wwj
            case 30:
                List<Map<String, Object>> tbsWwj = tbsWwjMapper.tbsWwjIntervals(sensorNo);
                getParamSpecial(tbsWwj, map, "value");
                break;
            //固定测斜tbs_gdqx
            case 31:
                List<Map<String, Object>> tbsGdqx = tbsGdqxMapper.tbsGdqxIntervals(sensorNo);
                getParamSpecial(tbsGdqx, map, "value");
                break;
            //表面位移tbs_bmwy
            case 32:
                List<Map<String, Object>> tbsBmwy = tbsBmwyMapper.tbsBmwyIntervals(sensorNo);
                getParamSpecial(tbsBmwy, map, "value");
                break;
            //流速仪tbs_lsy
            case 33:
                List<Map<String, Object>> tbsLsy = tbsLsyMapper.tbsLsyIntervals(sensorNo);
                getParamSpecial(tbsLsy, map, "value");
                break;
            //自动激光测距仪
            case 34:
                break;
            //弦式表面应变计
            case 36:
                break;
            //测斜传感器
            case 37:
                break;
            //裂缝倾角(地表裂缝监测仪/墙裂缝监测仪——倾角) tbs_liefeng_qj
            case 40:
                String lfX = "select time from tbs_liefeng_qj_x where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> lf_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(lfX));
                getParamSpecial(lf_x, map, "x_jd-x轴角度");
                String lfY = "select time from tbs_liefeng_qj_y where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> lf_y = influxDBConnect.queryResultToListMap(influxDBConnect.query(lfY));
                getParamSpecial(lf_y, map, "y_jd-y轴角度");
                String lfZ = "select time from tbs_liefeng_qj_z where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> lf_z = influxDBConnect.queryResultToListMap(influxDBConnect.query(lfZ));
                getParamSpecial(lf_z, map, "z_jd-z轴角度");
                break;
            //地面倾斜监测仪tbs_dm_qxy
            case 41:
                String dmQxyX = "select time from tbs_dm_qxy_x where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> x = influxDBConnect.queryResultToListMap(influxDBConnect.query(dmQxyX));
                getParamSpecial(x, map, "x_jd-x轴角度");
                String dmQxyY = "select time from tbs_dm_qxy_y where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> y = influxDBConnect.queryResultToListMap(influxDBConnect.query(dmQxyY));
                getParamSpecial(y, map, "y_jd-y轴角度");
                String dmQxyZ = "select time from tbs_dm_qxy_z where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> z = influxDBConnect.queryResultToListMap(influxDBConnect.query(dmQxyZ));
                getParamSpecial(z, map, "z_jd-z轴角度");
                break;
            //坡体浅层变形仪-拉线 tbs_ptqcbxy
            case 42:
                String lxX = "select time from tbs_ptqcbxy_lx_x where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> lx_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(lxX));
                getParamSpecial(lx_x, map, "x-水平面内x方向(主滑方向)位移");
                String lxY = "select time from tbs_ptqcbxy_lx_y where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> lx_y = influxDBConnect.queryResultToListMap(influxDBConnect.query(lxY));
                getParamSpecial(lx_y, map, "y-水平面内y方向(与主滑方向垂直)位移");
                String lxZ = "select time from tbs_ptqcbxy_lx_z where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> lx_z = influxDBConnect.queryResultToListMap(influxDBConnect.query(lxZ));
                getParamSpecial(lx_z, map, "z-垂直方向沉降位移值");
                break;
            //坡体浅层变形仪-倾角 tbs_ptqcbxy
            case 44:
                String qxX = "select time from tbs_ptqcbxy_wy_x where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> qx_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(qxX));
                getParamSpecial(qx_x, map, "x_jd-水平面内x轴与水平面夹角");
                String qxY = "select time from tbs_ptqcbxy_wy_y where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> qx_y = influxDBConnect.queryResultToListMap(influxDBConnect.query(qxY));
                getParamSpecial(qx_y, map, "y_jd-水平面内y轴与水平面的倾斜夹角");
                String qxZ = "select time from tbs_ptqcbxy_wy_z where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> qx_z = influxDBConnect.queryResultToListMap(influxDBConnect.query(qxZ));
                getParamSpecial(qx_z, map, "z_jd-水平面内z轴与水平面的倾斜夹角");
                break;
            //裂缝拉线(地表裂缝监测仪/墙裂缝监测仪——拉线) tbs_liefeng_lx
            case 43:

                String liefengLx = "select time from tbs_liefeng_lx where sensor_no ='" + sensorNo + "' ORDER BY time";
                List<Map<String, Object>> lief_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(liefengLx));
                getParamSpecial(lief_x, map, "lx-拉线");
                break;
        }
        return map;
    }

    private void getParamSpecial(List<Map<String, Object>> results, Map<String, Object> map, String... name) {
        if (results.size() <= 0) {
            return;
        }
//        BigDecimal maxData = null;
//        BigDecimal minData = null;
        for (int i = 0; i < name.length; i++) {
            List<Map<String, Object>> mapList = new ArrayList<>();
            List<List<Object>> list = new ArrayList<>();
            for (Map<String, Object> mapResult : results) {
                Object time = mapResult.get("time");
//                if (mapResult.get("param"+(i+1)) != null && mapResult.get("param"+(i+1)) !="") {
//                String value = (String) mapResult.get("param" + (i + 1));
//                String[] values = value.split(",");
//                for (String str : values) {
//                    if (null == str || str.equals("")) {
//                        continue;
//                    }
//                    BigDecimal num = new BigDecimal(str);
//                    maxData = maxData == null ? num : maxData.max(num);
//                    minData = minData == null ? num : minData.min(num);
//                }

                Map<String, Object> map1 = new HashMap<>();
                map1.put("name", time);
                List<Object> list1 = new ArrayList<>();
                list1.add(time);
//                list1.add(value);
                list.add(list1);
                map1.put("value", list1);
                mapList.add(map1);
//                }
            }
//            map.put("maxData", maxData);
//            map.put("minData", minData);
            if (mapList.size() > 0) {
                map.put(name[i], mapList);
            }
        }
    }
}
