package com.cqndt.disaster.device.project.service;

import com.cqndt.disaster.device.vo.TabReportFileVo;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/5/11  9:45
 * Description:
 */
public interface TabReportingFileService {
    /**
     * 根据项目id查询相应的报表
     * @param vo
     * @return
     */
    List<TabReportFileVo> listReportingFile(TabReportFileVo vo);
}
