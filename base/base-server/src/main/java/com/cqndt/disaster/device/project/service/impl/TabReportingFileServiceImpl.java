package com.cqndt.disaster.device.project.service.impl;

import com.cqndt.disaster.device.dao.TabReportingFileMapper;
import com.cqndt.disaster.device.project.service.TabReportingFileService;
import com.cqndt.disaster.device.vo.TabReportFileVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/11  9:47
 * Description:
 */
@Service
public class TabReportingFileServiceImpl implements TabReportingFileService {
    @Autowired
    private TabReportingFileMapper tabReportingFileMapper;
    @Value("${file-ip}")
    private String fileIp;

    @Override
    public List<TabReportFileVo> listReportingFile(TabReportFileVo vo) {
        List<TabReportFileVo> list = tabReportingFileMapper.listReportingFile(vo);
//        for (TabReportFileVo fileVo : list) {
//            fileVo.setUploadUrl(fileIp + File.separator + fileVo.getUploadUrl());
//        }
        return list;
    }
}
