package com.cqndt.disaster.device.app;

import com.cqndt.disaster.device.domain.TabPhotography;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/12  15:39
 * Description:获取倾斜摄影
 */
public interface AppTabPhotographyService {
    /**
     * 根据项目id查询倾斜摄影
     * @param projectId
     * @param modelType
     * @return
     */
    Map<String,Object> selectByProjectId(Integer projectId, String modelType);
}
