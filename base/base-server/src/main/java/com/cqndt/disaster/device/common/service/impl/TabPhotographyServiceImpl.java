package com.cqndt.disaster.device.common.service.impl;

import com.cqndt.disaster.device.domain.TabPhotography;
import com.cqndt.disaster.device.dao.TabPhotographyMapper;
import com.cqndt.disaster.device.common.service.TabPhotographyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/12  15:39
 * Description:获取倾斜摄影
 */
@Service
public class TabPhotographyServiceImpl implements TabPhotographyService {
    @Autowired
    private TabPhotographyMapper tabPhotographyMapper;

    @Override
    public List<TabPhotography> selectByRelationNo(String relationNo,String modelType){
        return tabPhotographyMapper.selectByRelationNo(relationNo,modelType);
    }
}
