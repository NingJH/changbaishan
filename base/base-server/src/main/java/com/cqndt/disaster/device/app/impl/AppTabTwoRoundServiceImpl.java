package com.cqndt.disaster.device.app.impl;

import com.cqndt.disaster.device.app.AppTabTwoRoundService;
import com.cqndt.disaster.device.common.service.TabTwoRoundService;
import com.cqndt.disaster.device.dao.TabTwoRoundMapper;
import com.cqndt.disaster.device.dao.app.AppTabTwoRoundMapper;
import com.cqndt.disaster.device.domain.TabTwoRound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/12  15:43
 * Description:获取全景图
 */
@Service
public class AppTabTwoRoundServiceImpl implements AppTabTwoRoundService {
    @Autowired
    private AppTabTwoRoundMapper tabTwoRoundMapper;
    @Value("${file-ip}")
    private String fileIp;

    @Override
    public Map<String,Object> selectByProjectId(Integer projectId, String modelType) {
        Map<String,Object> round = tabTwoRoundMapper.selectByProjectId(projectId,modelType);
        if(null!=round && null != round.get("Panorama")){
            round.put("Panorama",fileIp+"/"+round.get("Panorama").toString());
        }
        return round;
    }
}
