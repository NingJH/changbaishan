package com.cqndt.disaster.device.app.impl;

import com.cqndt.disaster.device.app.TabAndroidVersionService;
import com.cqndt.disaster.device.dao.app.TabAndroidVersionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/11  13:08
 * Description:
 */
@Service
public class TabAndroidVersionServiceImpl implements TabAndroidVersionService {
    @Autowired
    private TabAndroidVersionMapper tabAndroidVersionMapper;

    @Override
    public List<Map<String, Object>> findNewVersion() {
        return tabAndroidVersionMapper.findNewVersion();
    }
}
