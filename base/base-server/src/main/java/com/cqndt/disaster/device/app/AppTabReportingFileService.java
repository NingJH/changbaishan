package com.cqndt.disaster.device.app;

import com.cqndt.disaster.device.vo.TabDocCollection;
import com.cqndt.disaster.device.vo.TabReportFileVo;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/5/11  9:45
 * Description:
 */
public interface AppTabReportingFileService {
    /**
     * 根据项目id查询相应的报表
     * @param vo
     * @return
     */
    TabDocCollection listReportingFile(TabReportFileVo vo);
}
