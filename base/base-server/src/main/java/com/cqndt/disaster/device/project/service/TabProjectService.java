package com.cqndt.disaster.device.project.service;

import com.cqndt.disaster.device.domain.TabProject;
import com.cqndt.disaster.device.vo.TabProjectVo;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/12  10:15
 * Description:
 */

public interface TabProjectService {
    /**
     * 根据id相应的项目
     * @param projectId
     * @return
     */
    TabProject getById(Integer projectId);
    /**
     *  项目列表
     * @param vo
     * @return
     */
    List<TabProjectVo> selectByCondition(TabProjectVo vo);

    /**
     * 参建单位
     * @param projectId
     * @return
     */
    List<Map<String, Object>> selectCjdwByProjectId(Integer projectId);

    /**
     * 项目班子
     * @param projectId
     * @return
     */
    List<Map<String, Object>> selectXmbzByProjectId(Integer projectId);

    /**
     * 项目文档
     * @param projectId
     * @return
     */
    List<Map<String, Object>> selectProjectDatum(Integer projectId) ;

    /**
     * 用户已分配的项目
     * @param userId
     * @return
     */
    List<Map<String,Object>> selectByUserId(Integer userId);

    /**
     * 根据项目id获取该项目下的设备
     * @param projectId
     * @return
     */
    List<Map<String,Object>> getDeviceByProjectId(Integer userId,Integer projectId);

    Map<String,Object> getDeviceNumByProjectId(Integer userId,Integer projectId);

    Map<String, Object> getTabProject(String deviceNo);
}
