package com.cqndt.disaster.device.app;

import com.cqndt.disaster.device.domain.TabUser;
import com.cqndt.disaster.device.vo.TabUserVo;

/**
 * Created By marc
 * Date: 2019/5/12  10:25
 * Description:
 */
public interface AppTabUserService {

    TabUserVo getUserByUserName(TabUserVo user);
}
