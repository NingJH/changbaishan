package com.cqndt.disaster.device.deviceExp.service;

import com.cqndt.disaster.device.domain.TabDeviceException;
import com.cqndt.disaster.device.vo.TabDeviceExceptionVo;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/6/7  16:32
 * Description:
 */
public interface TabDeviceExceptionService {
    /**
     * 设备异常处理
     * @param vo
     * @return
     */
    List<TabDeviceExceptionVo> getListByCondition(TabDeviceExceptionVo vo);

    /**
     * 忽略处置
     * @param vo
     */
    void ignoreException(TabDeviceExceptionVo vo);
    /**
     * 人工处置
     * @param vo
     */
    void dealException(TabDeviceExceptionVo vo);

    /**
     * 填写维护信息
     * @param vo
     */
    void fillDefendMessage(TabDeviceExceptionVo vo);

    /**
     * 查看当前数据异常
     * @return
     */
    List<Map<String,Object>> listDataException(TabDeviceExceptionVo vo);

    /**
     * 查看处置记录
     * @param vo
     * @return
     */
    List<Map<String,Object>> listDealRecord(TabDeviceExceptionVo vo);
    /**
     * 处置记录详情
     * @return
     */
    Map<String,Object> listDealRecordDetail(String deviceNo,String operateTime);
}
