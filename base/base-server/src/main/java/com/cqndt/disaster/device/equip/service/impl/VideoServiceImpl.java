/**
 * 
 */
package com.cqndt.disaster.device.equip.service.impl;

import com.cqndt.disaster.device.common.util.AccessToken;
import com.cqndt.disaster.device.common.util.HttpUtil;
import com.cqndt.disaster.device.dao.TabVideoMonitoringMapper;
import com.cqndt.disaster.device.domain.TabVideoMonitoring;
import com.cqndt.disaster.device.equip.service.VideoService;
import org.apache.http.client.ClientProtocolException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 视频监控云台控制
 * @author lhy
 * @date 2017年10月26日
 *
 */
@Service("videoService")
public class VideoServiceImpl implements VideoService {
	
	@Autowired
	TabVideoMonitoringMapper spjkMapper;
	/**
	 * {"data":{"accessToken":"at.7vxpld08dnow74yy8ksofh5taj1q9pk8-22z042wnzy-12clc95-7tm4ppeer","expireTime":1505125643134},"code":"200","msg":"操作成功!"}
	 * @throws IOException 
	 * @throws ClientProtocolException
	 * 
	 * @see org.moon.demo.service.VideoService#getAccessToken()
	 */
	@Override
	public String getAccessToken() throws ClientProtocolException, IOException {

		List<String> params = new ArrayList<>();
		List<String> values = new ArrayList<>();

		params.add("appKey");
		params.add("secret");

		values.add(APPKY);
		values.add(SECRET);
		String reslut = null;
		String response = HttpUtil.doPost(ACCESS_TOKEN, params, values);
		Map<String, Map<String, String>> maps = HttpUtil.json2Object(response, Map.class);
		if (!maps.get("data").isEmpty()) {
			reslut = maps.get("data").get("accessToken");
		}

		return reslut;
	}

	/**
	 * {"data":{"accessToken":"at.7vxpld08dnow74yy8ksofh5taj1q9pk8-22z042wnzy-12clc95-7tm4ppeer","expireTime":1505125643134},"code":"200","msg":"操作成功!"}
	 * @throws IOException 
	 * @throws ClientProtocolException
	 * 
	 * @see org.moon.demo.service.VideoService#getAccessToken()
	 */
	@Override
	public String getAccessTokenByParam(int deviceId) throws ClientProtocolException, IOException {

		List<String> params = new ArrayList<>();
		List<String> values = new ArrayList<>();

		params.add("appKey");
		params.add("appSecret");

		TabVideoMonitoring device = spjkMapper.selectByPrimaryKey(deviceId);
		String appKey = device.getAppkey();
		String secret = device.getAppSecret();
		String token = device.getToken();//获取访问令牌
		Long dueTime =0L;
		if(device.getDueTime()!=null && "".equals(device.getDueTime())){
			dueTime=  Long.parseLong(device.getDueTime());//获取到期时间
		}
		Long nowTime = System.currentTimeMillis();
		//当数据库中无token以及到期时间已到时,重新获取token及到期时间
		if(StringUtils.isEmpty(token)||dueTime<nowTime){
			values.add(appKey);
			values.add(secret);
			String response = HttpUtil.doPost(ACCESS_TOKEN, params, values);//重新获取token及到期时间并更新数据库
			Map<String, Map<String, String>> maps = HttpUtil.json2Object(response, Map.class);
			if(!maps.get("data").isEmpty()){
				token = maps.get("data").get("accessToken");
				AccessToken.expireTime =Long.valueOf(String.valueOf(maps.get("data").get("expireTime")));
				device.setToken(token);
				device.setDueTime(String.valueOf(maps.get("data").get("expireTime")));
				spjkMapper.updateByPrimaryKey(device);
			}
		}
		AccessToken.accessToken=token;//赋值给token静态变量用于停止视频
		return token;
	}

	
	@Override
	public String addDevice(String deviceSerial, String validateCode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String deleteDevice(String deviceSerial) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updateName(String deviceSerial, String deviceName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String capture(int deviceId, String deviceSerial, int channelNo) throws ClientProtocolException, IOException {
		List<String> params = new ArrayList<>();
		List<String> values = new ArrayList<>();
		String token = getAccessTokenByParam(deviceId);
		
		params.add("accessToken");
		params.add("deviceSerial");
		params.add("channelNo");
		
		values.add(token);
		values.add(deviceSerial);
		values.add(String.valueOf(channelNo));
		
		String response  = HttpUtil.doPost(CAPTURE, params, values);
		return response;
	}

	@Override
	public String deviceList(int pageStart, int pageSize) throws ClientProtocolException, IOException {
		List<String> params = new ArrayList<>();
		List<String> values = new ArrayList<>();
		
		params.add("accessToken");
		params.add("pageStart");
		params.add("pageSize");
		
		values.add(AccessToken.accessToken);
		values.add(String.valueOf(pageStart));
		values.add(String.valueOf(pageSize));
		
		String response  = HttpUtil.doPost(DEVICE_LIST, params, values);
		return response;
	}

	@Override
	public String startPtz(int deviceId,String deviceSerial,int channelNo, int direction,int speed) throws ClientProtocolException, IOException {
		
		List<String> params = new ArrayList<>();
		List<String> values = new ArrayList<>();
		
		String token = getAccessTokenByParam(deviceId);
		
		params.add("accessToken");
		params.add("deviceSerial");
		params.add("channelNo");
		params.add("direction");
		params.add("speed");
		
		values.add(token);
		values.add(deviceSerial);
		values.add(String.valueOf(channelNo));
		values.add(String.valueOf(direction));
		values.add(String.valueOf(speed));
//		String response="";
		String response  = HttpUtil.doPost(START_PTZ, params, values);
		return response;
	}

	@Override
	public String stopPtz(String deviceSerial, int channelNo, int direction) throws ClientProtocolException, IOException {
		List<String> params = new ArrayList<>();
		List<String> values = new ArrayList<>();
		
		params.add("accessToken");
		params.add("deviceSerial");
		params.add("channelNo");
		params.add("direction");
		values.add(AccessToken.accessToken); 
		values.add(deviceSerial);
		values.add(String.valueOf(channelNo));
		values.add(String.valueOf(direction));
		
		String response  = HttpUtil.doPost(STOP_PTZ, params, values);
		return response;
	}

}
