package com.cqndt.disaster.device.sms.service;

import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @User : ningjunhao
 * @Mail : 15823526611@139.com
 * @Time : 2019-08-05 09:59
 **/
public interface LonAndLatService {

    List<Map<String,Object>> fsLonAndLat(int userId,String[] lonLat);

    List<Map<String,Object>> findPersion(String deviceId);

    boolean sendSms(String mobile,String content,int userId);
}
