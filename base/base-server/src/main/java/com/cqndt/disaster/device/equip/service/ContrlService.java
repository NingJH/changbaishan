package com.cqndt.disaster.device.equip.service;

import com.iotplatform.client.dto.PostDeviceCommandOutDTO2;

/**
 * Created By marc
 * Date: 2019/5/7  19:16
 * Description:
 */
public interface ContrlService {

    PostDeviceCommandOutDTO2 postCommand(String deviceId, String payload);

}
