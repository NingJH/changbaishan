package com.cqndt.disaster.device.app.impl;

import com.cqndt.disaster.device.app.AppTabLoginLogService;
import com.cqndt.disaster.device.dao.TabLoginLogMapper;
import com.cqndt.disaster.device.domain.TabLoginLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created By marc
 * Date: 2019/5/20  14:35
 * Description:
 */
@Service
public class AppTabLoginLogServiceImpl implements AppTabLoginLogService {
    @Autowired
    private TabLoginLogMapper tabLoginLogMapper;

    @Override
    @Transactional
    public void insert(TabLoginLog log) {
        tabLoginLogMapper.insertSelective(log);
    }

    @Override
    public TabLoginLog getByUserNameAndPlat(String userName, Integer platType) {
        return tabLoginLogMapper.getByUserNameAndPlat(userName,platType);
    }
}
