package com.cqndt.disaster.device.app;

import com.cqndt.disaster.device.vo.TabAreaVo;

/**
 * Created By marc
 * Date: 2019/5/12  17:04
 * Description:
 */
public interface AppTabAreaService {
    TabAreaVo cityLinkage(String areaCode, String level);
}
