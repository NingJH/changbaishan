package com.cqndt.disaster.device.app.impl;

import com.cqndt.disaster.device.app.AppTabProjectService;
import com.cqndt.disaster.device.dao.TabAttachmentMapper;
import com.cqndt.disaster.device.dao.app.AppTabProjectMapper;
import com.cqndt.disaster.device.dao.app.AppTabStaticTypeMapper;
import com.cqndt.disaster.device.domain.TabAttachment;
import com.cqndt.disaster.device.vo.AppTabDevice;
import com.cqndt.disaster.device.vo.Monitor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/12  10:32
 * Description:
 */
@Service
public class AppTabProjectServiceImpl implements AppTabProjectService {
    @Autowired
    private AppTabProjectMapper appTabProjectMapper;
    @Value("${file-ip}")
    private String fileIp;
    @Autowired
    private AppTabStaticTypeMapper appTabStaticTypeMapper;

    @Override
    public List<Map<String,Object>> selectByCondition(String userId, String level, String areaCode, String id, String search) {
        String areaCodeSplit = areaCode.substring(0,2);
        List<Map<String,Object>> list = appTabProjectMapper.selectByCondition(userId,level,areaCode,id,search,areaCodeSplit);
        for (Map<String,Object> map:list) {
            if(null != map.get("projectImg")&& !("").equals(map.get("projectImg"))){
                String photos = (String)map.get("projectImg");
                String[] ids = photos.split(",");
                List<Map<String,Object>> attachments = appTabProjectMapper.selectAttachmentIds(ids);
                List<String> listStr = new ArrayList<>();
                for (Map<String,Object> mapList:attachments) {
                    listStr.add((String)mapList.get("phUrl"));
                }
                map.put("photo",listStr);
            }else{
                map.put("photo",new ArrayList<>());
            }
        }
        return list;
    }

    @Override
    public Map<String, Object> selectByIdDetails(Integer projectId,String userId) {
        Map<String, Object> map = appTabProjectMapper.selectByIdDetails(projectId,userId);
        if(null != map.get("projectImg") && !("").equals(map.get("projectImg"))){
            String photos = (String)map.get("projectImg");
            String[] ids = photos.split(",");
            List<Map<String,Object>> attachments = appTabProjectMapper.selectAttachmentIds(ids);
            List<String> listStr = new ArrayList<>();
            for (Map<String,Object> mapList:attachments) {
                listStr.add((String)mapList.get("phUrl"));
            }
            map.put("photo",listStr);
        }else{
            map.put("photo",new ArrayList<>());
        }
        if(null != map.get("projectVideo") && !("").equals(map.get("projectVideo"))){
            String photos = (String)map.get("projectVideo");
            String[] ids = photos.split(",");
            List<Map<String,Object>> attachments = appTabProjectMapper.selectAttachmentIds(ids);
            List<String> listStr = new ArrayList<>();
            for (Map<String,Object> mapList:attachments) {
                listStr.add((String)mapList.get("phUrl"));
            }
            map.put("video",listStr);
        }else{
            map.put("video",new ArrayList<>());
        }
        return map;
    }

    @Override
    public List<Map<String, Object>> selectAllPush(String userId) {
        List<Map<String, Object>> list = appTabProjectMapper.selectAllPush(userId);
        for (Map<String,Object> map:list) {
            if(null != map.get("projectImg") && !("").equals(map.get("projectImg"))){
                String photos = (String)map.get("projectImg");
                String[] ids = photos.split(",");
                List<Map<String,Object>> attachments = appTabProjectMapper.selectAttachmentIds(ids);
                List<String> listStr = new ArrayList<>();
                for (Map<String,Object> mapList:attachments) {
                    listStr.add((String)mapList.get("phUrl"));
                }
                map.put("photo",listStr);
            }else{
                map.put("photo",new ArrayList<>());
            }
        }
        return list;
    }

    @Override
    public List<Map<String, Object>> selectProjectDatum(Integer projectId) {
        return appTabProjectMapper.selectProjectDatum(projectId);
    }

    @Override
    public Map<String, Object> getByProjectId(Integer projectId,String search, Integer monitorType) {
        Map<String, Object> map = new HashMap<>();
        List<Map<String,Object>> list = appTabStaticTypeMapper.selectByStaticNum("11");
        //离线时间判断
        Integer num = (Integer)list.get(0).get("id");
        List<Map<String, Object>> listData = appTabProjectMapper.getByProjectId(projectId,search,monitorType,num);
        Integer zxNum = appTabProjectMapper.countDeviceZxForProject(projectId,num);
        Integer totalNum = listData.size();
        map.put("listData",listData);
        map.put("zxNum",zxNum);
        map.put("totalNum",totalNum);

        return map;
    }

    @Override
    public Monitor getMonitorDataById(Integer monitorId) {
        Monitor monitor = appTabProjectMapper.getMonitorDataById(monitorId);

        List<Map<String,Object>> list = appTabStaticTypeMapper.selectByStaticNum("11");
        //离线时间判断
        Integer num = (Integer)list.get(0).get("id");

        AppTabDevice device = appTabProjectMapper.selectDeviceById(monitor.getDeviceId(),num);

        monitor.setDevice(device);

        return monitor;
    }

    @Override
    public List<Map<String, Object>> selectAttachmentImgs(Integer projectId,String userId) {
        List<Map<String,Object>> attachments = new ArrayList<>();
        Map<String, Object> map = appTabProjectMapper.selectByIdDetails(projectId,userId);
        if(null != map.get("projectImg") && !("").equals(map.get("projectImg"))){
            String photos = (String)map.get("projectImg");
            String[] ids = photos.split(",");
            attachments = appTabProjectMapper.selectForProjectId(ids);
            for (Map<String,Object> mapList:attachments) {
                mapList.put("Url",fileIp+ "/img/"+mapList.get("Url"));
            }
        }
        return attachments;
    }

    @Override
    public List<Map<String, Object>> selectAttachmentVideos(Integer projectId,String userId) {
        List<Map<String,Object>> attachments = new ArrayList<>();
        Map<String, Object> map = appTabProjectMapper.selectByIdDetails(projectId,userId);
        if(null != map.get("projectVideo") && !("").equals(map.get("projectVideo"))){
            String photos = (String)map.get("projectVideo");
            String[] ids = photos.split(",");
            attachments = appTabProjectMapper.selectForProjectId(ids);
            for (Map<String,Object> mapList:attachments) {
                mapList.put("Url",fileIp+"/video/"+mapList.get("Url"));
            }
        }
        return attachments;
    }
}
