package com.cqndt.disaster.device.common.service.impl;

import com.cqndt.disaster.device.domain.TabArea;
import com.cqndt.disaster.device.dao.TabAreaMapper;
import com.cqndt.disaster.device.common.service.TabAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/15  12:09
 * Description:
 */
@Service
public class TabAreaServiceImpl implements TabAreaService {
    @Autowired
    private TabAreaMapper tabAreaMapper;

    @Override
    public List<TabArea> selectByAreaCode(String areaCode){
        return tabAreaMapper.selectByAreaCode(areaCode);
    }
}
