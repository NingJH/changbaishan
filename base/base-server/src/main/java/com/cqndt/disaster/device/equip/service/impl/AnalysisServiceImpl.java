package com.cqndt.disaster.device.equip.service.impl;

import com.cqndt.disaster.device.common.shrio.ShiroUtils;
import com.cqndt.disaster.device.dao.*;
import com.cqndt.disaster.device.domain.TabWarnSetting;
import com.cqndt.disaster.device.equip.service.AnalysisService;
import com.cqndt.disaster.device.vo.DeviceInProjectVo;
import com.cqndt.disaster.device.vo.DeviceMonitorVo;
import com.cqndt.disaster.device.vo.SearchVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/4/10  17:52
 * Description:获取曲线数据
 */
//@Service
public class AnalysisServiceImpl{
    @Autowired
    private TbsLfwyMapper tbsLfwyMapper;
    @Autowired
    private TbsQjyWbxMapper tbsQjyWbxMapper;
    @Autowired
    private TbsDbbxHpMapper tbsDbbxHpMapper;
    @Autowired
    private TbsGnssMapper tbsGnssMapper;
    @Autowired
    private TbsSbwyQxyMapper tbsSbwyQxyMapper;
    @Autowired
    private TbsChenjiangMapper tbsChenjiangMapper;
    @Autowired
    private TbsKongxishuiyaMapper tbsKongxishuiyaMapper;
    @Autowired
    private TbsGangjinjiMapper tbsGangjinjiMapper;
    @Autowired
    private TbsTylMapper tbsTylMapper;
    @Autowired
    private TbsYlMapper tbsYlMapper;
    @Autowired
    private TbsShuiweijiMapper tbsShuiweijiMapper;
    @Autowired
    private TbsYingbianMapper tbsYingbianMapper;
    @Autowired
    private TbsWenshiduMapper tbsWenshiduMapper;
    @Autowired
    private TbsNiweijiMapper tbsNiweijiMapper;
    @Autowired
    private TbsZhendongMapper tbsZhendongMapper;
    @Autowired
    private TbsLxdbdMapper tbsLxdbdMapper;
    @Autowired
    private TbsDxswMapper tbsDxswMapper;
    @Autowired
    private TbsTotalStationMapper tbsTotalStationMapper;
    @Autowired
    private TbsRgszMapper tbsRgszMapper;
    @Autowired
    private TbsQxyMapper tbsQxyMapper;
    @Autowired
    private TbsSbwyLxMapper tbsSbwyLxMapper;
    @Autowired
    private TbsSbwyCxMapper tbsSbwyCxMapper;
    @Autowired
    private TbsSxcfjMapper tbsSxcfjMapper;
    @Autowired
    private TbsQjyMapper tbsQjyMapper;
    @Autowired
    private TbsZxsyljMapper tbsZxsyljMapper;
    @Autowired
    private TbsDishengMapper tbsDishengMapper;
    @Autowired
    private TbsCishengMapper tbsCishengMapper;
    @Autowired
    private TbsWwjMapper tbsWwjMapper;
    @Autowired
    private  TbsGdqxMapper tbsGdqxMapper;
    @Autowired
    private TbsBmwyMapper tbsBmwyMapper;
    @Autowired
    private TbsLsyMapper  tbsLsyMapper;
    @Autowired
    private TbsHslMapper tbsHslMapper;
    @Autowired
    private TabDeviceMapper tabDeviceMapper;
    @Autowired
    private TabWarnSettingMapper tabWarnSettingMapper;
    @Autowired
    private TbsDmQxyMapper tbsDmQxyMapper;
    @Autowired
    private TbsPtqcbxyMapper tbsPtqcbxyMapper;
    @Autowired
    private TbsLiefengLxMapper tbsLiefengLxMapper;
    @Autowired
    private TbsLiefengQjMapper tbsLiefengQjMapper;

    //@Override
    public Map<String, Object> selecteChart(SearchVo vo) {
        Map<String, Object> map = new HashMap<>();
        Integer type = Integer.valueOf(vo.getSensorType());
        switch (type) {
            //裂缝智能测报仪\裂缝计\裂缝位移计\滑动变位计 tbs_lfwy
            //滑动变位计 tbs_lfwy
            case 38:
            case 1:
                List<Map<String, Object>>  tbsLfwy = tbsLfwyMapper.tbsLfwy(vo);
                getParam(tbsLfwy,map,"value","temp");
                break;
            //微变形倾角测量仪tbs_qjy_wbx
            case 2:
                List<Map<String, Object>>  tbsQjyWbx = tbsQjyWbxMapper.tbsQjyWbx(vo);
                getParam(tbsQjyWbx,map,"qxjd","temp");
                break;
            //滑坡地表变形测量仪tbs_dbbx_hp
            case 3:
                List<Map<String, Object>>  tbsDbbxHp = tbsDbbxHpMapper.tbsDbbxHp(vo);
                getParam(tbsDbbxHp,map,"x","y","z","temp");
                break;
            //GNSS位移\地表位移(GPS)tbs_gnss
            case 4:
                List<Map<String, Object>>  tbsGnss = tbsGnssMapper.tbsGnss(vo);
                getParam(tbsGnss,map,"x1","y1","z1","swy","cwy");
                break;
            //深部位移\深部位移(倾斜仪)tbs_sbwy_qxy
            case 5:
                List<Map<String, Object>>  tbsSbwyQxy = tbsSbwyQxyMapper.tbsSbwyQxy(vo);
                getParam(tbsSbwyQxy,map,"x_jd","y_jd","z_jd","x","y","wy","jd");
                break;
            //沉降/土体沉降 tbs_chenjiang
            case 6:
            case 35:
                List<Map<String, Object>>  tbsChenjiang = tbsChenjiangMapper.tbsChenjiang(vo);
                getParam(tbsChenjiang,map,"x","y","z","temp");
                break;
            //孔隙水压 tbs_kongxishuiya
            case 7:
                List<Map<String, Object>>  tbsKongxishuiya = tbsKongxishuiyaMapper.tbsKongxishuiya(vo);
                getParam(tbsKongxishuiya,map,"value");
                break;
            //钢筋计 tbs_gangjinji
            case 8:
                List<Map<String, Object>>  tbsGangjinji = tbsGangjinjiMapper.tbsGangjinji(vo);
                getParam(tbsGangjinji,map,"value");
                break;
            //土压力tbs_tyl
            case 9:
                List<Map<String, Object>>  tbsTyl = tbsTylMapper.tbsTyl(vo);
                getParam(tbsTyl,map,"value");
                break;
            //雨量tbs_yl
            case 10:
                List<Map<String, Object>>  tbsYl = tbsYlMapper.tbsYl(vo);
                getParam(tbsYl,map,"value");
                break;
            //土壤湿度\土壤含水率tbs_hsl
            case 11:
            case 39:
                List<Map<String, Object>>  tbsHsl = tbsHslMapper.tbsHsl(vo);
                getParam(tbsHsl,map,"hsl","deep");
                break;
            //水位计tbs_shuiweiji
            case 12:
                List<Map<String, Object>>  tbsShuiweiji = tbsShuiweijiMapper.tbsShuiweiji(vo);
                getParam(tbsShuiweiji,map,"value");
                break;
            //应变 tbs_yingbian
            case 13:
                List<Map<String, Object>>  tbsYingbian = tbsYingbianMapper.tbsYingbian(vo);
                getParam(tbsYingbian,map,"value");
                break;
            //温湿度tbs_wenshidu
            case 14:
                List<Map<String, Object>>  tbsWenshidu = tbsWenshiduMapper.tbsWenshidu(vo);
                getParam(tbsWenshidu,map,"humidity","temp");
                break;
            //泥位tbs_niweiji
            case 15:
                List<Map<String, Object>>  tbsNiweiji = tbsNiweijiMapper.tbsNiweiji(vo);
                getParam(tbsNiweiji,map,"value");
                break;
            //振动tbs_zhendong
            case 16:
                List<Map<String, Object>>  tbsZhendong = tbsZhendongMapper.tbsZhendong(vo);
                getParam(tbsZhendong,map,"zf","pl");
                break;
            //地表位移(拉线式)tbs_lxdbd
            case 17:
                List<Map<String, Object>>  tbsLxdbd = tbsLxdbdMapper.tbsLxdbd(vo);
                getParam(tbsLxdbd,map,"value");
                break;
            //地下水(地下水位)tbs_dxsw
            case 18:
                List<Map<String, Object>>  tbsDxsw = tbsDxswMapper.tbsDxsw(vo);
                getParam(tbsDxsw,map,"value");
                break;
            //全站仪tbs_total_station
            case 19:
                List<Map<String, Object>>  tbsTotalStation = tbsTotalStationMapper.tbsTotalStation(vo);
                getParam(tbsTotalStation,map,"x","y","z");
                break;
            //人工水准仪tbs_rgsz
            case 20:
                List<Map<String, Object>>  tbsRgsz = tbsRgszMapper.tbsRgsz(vo);
                getParam(tbsRgsz,map,"value");
                break;
            //倾斜仪(WMT)tbs_qxy
            case 21:
                List<Map<String, Object>>  tbsQxy = tbsQxyMapper.tbsQxy(vo);
                getParam(tbsQxy,map,"one","two","three");
                break;
            //深部位移(拉线)tbs_sbwy_lx
            case 22:
                List<Map<String, Object>>  tbsSbwyLx = tbsSbwyLxMapper.tbsSbwyLx(vo);
                getParam(tbsSbwyLx,map,"value");
                break;
            // 深部位移(测斜)tbs_sbwy_cx
            case 23:
                List<Map<String, Object>>  tbsSbwyCx = tbsSbwyCxMapper.tbsSbwyCx(vo);
                getParam(tbsSbwyCx,map,"x","y");
                break;
            //三项测缝计tbs_sxcfj
            case 24:
                List<Map<String, Object>>  tbsSxcfj = tbsSxcfjMapper.tbsSxcfj(vo);
                getParam(tbsSxcfj,map,"value");
                break;
            //倾角仪tbs_qjy
            case 25:
                List<Map<String, Object>>  tbsQjy = tbsQjyMapper.tbsQjy(vo);
                getParam(tbsQjy,map,"x","y","z");
                break;
            // 振弦式应力计tbs_zxsylj
            case 26:
                List<Map<String, Object>>  tbsZxsylj = tbsZxsyljMapper.tbsZxsylj(vo);
                getParam(tbsZxsylj,map,"value");
                break;
            //地声tbs_disheng
            case 27:
                List<Map<String, Object>>  tbsDisheng = tbsDishengMapper.tbsDisheng(vo);
                getParamSpecial(tbsDisheng,map,"x","y","z");
                break;
            //次声tbs_cisheng
            case 28:
                List<Map<String, Object>>  tbsCisheng = tbsCishengMapper.tbsCisheng(vo);
                getParamSpecial(tbsCisheng, map,"value");
                break;
//            //地表位移（拉线式）tbs_lxdbd
//            case 29:
//                break;
            //物位计tbs_wwj
            case 30:
                List<Map<String, Object>>  tbsWwj = tbsWwjMapper.tbsWwj(vo);
                getParam(tbsWwj,map,"value");
                break;
            //固定测斜tbs_gdqx
            case 31:
                List<Map<String, Object>>  tbsGdqx = tbsGdqxMapper.tbsGdqx(vo);
                getParam(tbsGdqx,map,"value");
                break;
            //表面位移tbs_bmwy
            case 32:
                List<Map<String, Object>>  tbsBmwy = tbsBmwyMapper.tbsBmwy(vo);
                getParam(tbsBmwy,map,"value");
                break;
            //流速仪tbs_lsy
            case 33:
                List<Map<String, Object>>  tbsLsy = tbsLsyMapper.tbsLsy(vo);
                getParam(tbsLsy,map,"value");
                break;
            //自动激光测距仪
            case 34:
                break;
            //弦式表面应变计
            case 36:
                break;
            //测斜传感器
            case 37:
                break;
            //裂缝倾角(地表裂缝监测仪/墙裂缝监测仪——倾角) tbs_liefeng_qj
            case 40:
                List<Map<String,Object>> tbsLiefengQj = tbsLiefengQjMapper.tbsLiefengQj(vo);
                getParam(tbsLiefengQj,map,"x_jd","y_jd","z_jd");
                break;
            //地面倾斜监测仪tbs_dm_qxy
            case 41:
                List<Map<String,Object>> tbsDmQxy = tbsDmQxyMapper.tbsDmQxy(vo);
                getParam(tbsDmQxy,map,"x_jd","y_jd","z_jd");
                break;
            //坡体浅层变形仪-拉线 tbs_ptqcbxy
            case 42:
                List<Map<String,Object>> tbsPtqcbxy = tbsPtqcbxyMapper.tbsPtqcbxy(vo);
                getParam(tbsPtqcbxy,map,"x","y","z","x_jd","y_jd","z_jd");
                break;
            //坡体浅层变形仪-倾角 tbs_ptqcbxy
            case 44:
//                List<Map<String,Object>> tbsPtqcbxy = tbsPtqcbxyMapper.tbsPtqcbxy(vo);
//                getParam(tbsPtqcbxy,map,"x","y","z","x_jd","y_jd","z_jd");
                break;
            //裂缝拉线(地表裂缝监测仪/墙裂缝监测仪——拉线) tbs_liefeng_lx
            case 43:
                List<Map<String, Object>>  tbsLiefengLx = tbsLiefengLxMapper.tbsLiefengLx(vo);
                getParam(tbsLiefengLx,map,"value");
                break;
        }
        return map;
    }

    //@Override
    public List<DeviceInProjectVo> selectDeviceByProjectId(String projectId) {
        List<DeviceInProjectVo> list = tabDeviceMapper.selectByProjectId(projectId);
        if(list.size()>0){
            for (DeviceInProjectVo vo: list) {
                List<DeviceMonitorVo> listNo = tabDeviceMapper.selectByPIdAndDtype(projectId,vo.getDeviceType(),ShiroUtils.getUserId());
                vo.setListNo(listNo);
            }
        }
        return list;
    }

    //@Override
    public TabWarnSetting getSettingBySensorNo(String sensorNo) {
        return tabWarnSettingMapper.getSettingBySensorNo(sensorNo);
    }
    /**
     * 转换对应的结果集
     * @param results
     * @param map
     * @param name
     */
    private void getParam(List<Map<String, Object>> results, Map<String, Object> map,String ... name){
        if (results.size() <= 0) {
            return ;
        }
        BigDecimal maxData = null;
        BigDecimal minData = null;
        for(int i=0;i<name.length;i++){
            List<Map<String, Object>> mapList = new ArrayList<>();
            List<List<Object>> list = new ArrayList<>();
            for (Map<String, Object> mapResult:results) {
                Object time = mapResult.get("time");
                if (mapResult.get("param"+(i+1)) != null && mapResult.get("param"+(i+1)) !="") {
                    BigDecimal value = (BigDecimal)mapResult.get("param"+(i+1));
                    maxData = maxData == null? value: maxData.max(value);
                    minData = minData == null? value: minData.min(value);
                    Map<String, Object> map1 = new HashMap<>();
                    map1.put("name", time);
                    List<Object> list1 = new ArrayList<>();
                    list1.add(time);
                    list1.add(value);
                    list.add(list1);
                    map1.put("value", list1);
                    mapList.add(map1);
                }
            }
            if(mapList.size()>0){
                map.put("maxData", maxData);
                map.put("minData", minData);
                map.put(name[i],mapList);
            }
        }
    }
    /**
     * 地声、次声转换对应的结果集
     * @param results
     * @param map
     * @param name
     */
    private void getParamSpecial(List<Map<String, Object>> results, Map<String, Object> map,String ... name){
        if (results.size() <= 0) {
            return ;
        }
        BigDecimal maxData = null;
        BigDecimal minData = null;
        for(int i=0;i<name.length;i++){
            List<Map<String, Object>> mapList = new ArrayList<>();
            List<List<Object>> list = new ArrayList<>();
            for (Map<String, Object> mapResult:results) {
                Object time = mapResult.get("time");
                if (mapResult.get("param"+(i+1)) != null && mapResult.get("param"+(i+1)) !="") {
                    String  value = (String)mapResult.get("param"+(i+1));
                    String[] values = value.split(",");
                    for (String str:values) {
                        if(null==str||str.equals("")){
                            continue;
                        }
                        BigDecimal num = new BigDecimal(str);
                        maxData = maxData == null? num: maxData.max(num);
                        minData = minData == null? num: minData.min(num);
                    }
                    Map<String, Object> map1 = new HashMap<>();
                    map1.put("name", time);
                    List<Object> list1 = new ArrayList<>();
                    list1.add(time);
                    list1.add(value);
                    list.add(list1);
                    map1.put("value", list1);
                    mapList.add(map1);
                }
            }
            map.put("maxData", maxData);
            map.put("minData", minData);
            if(mapList.size()>0){
                map.put(name[i],mapList);
            }
        }
    }
}
