package com.cqndt.disaster.device.common.service;

import com.cqndt.disaster.device.domain.TabPhotography;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/12  15:39
 * Description:获取倾斜摄影
 */
public interface TabPhotographyService {
    /**
     * 根据相关编号获取倾斜摄影
     * @param relationNo
     * @return
     */
    List<TabPhotography> selectByRelationNo(String relationNo,String modelType);
}
