package com.cqndt.disaster.device.common.util;

import java.math.BigInteger;

/**
 * @Description: 进制、换位相关工具类
 * @Auther: lhl
 * @Date: 2019/4/29 19:43
 */
public class BinaryUtil {

    /**
     * 高低位换位
     * @param str
     * @return
     */
    public static String sortHighShort(String str){
        String[] src = new String[str.length()/2];
        for(int i=0;i<str.length()/2;i++){
            src[i] = str.substring(2*i, 2*i+2);
        }
        StringBuffer ok =new StringBuffer();
        for(int i=src.length-1;i>=0;i--){
            ok.append(src[i]);
        }
        return ok.toString();
    }

    /**
     * byte 转 16进制
     * @param bytes
     * @return
     */
    public static String bytesToHexFun3(byte[] bytes) {
        StringBuilder buf = new StringBuilder(bytes.length * 2);
        for(byte b : bytes) { // 使用String的format方法进行转换
            buf.append(String.format("%02x", new Integer(b & 0xff)));
        }
        return buf.toString();
    }


    /**
     * string->int ->0X ->float
     * @param str
     * @return
     */
    public static Float strToFloat(String str){
        BigInteger b = new BigInteger(str,16);
        Float value = Float.intBitsToFloat(b.intValue());
        return value;
    }

    /**
     * string ->int
     * @param strHex
     * @return
     */
    public static int HexToInt(String strHex){
        int nResult = 0;
        if ( !IsHex(strHex) )
            return nResult;
        String str = strHex.toUpperCase();
        if ( str.length() > 2 ){
            if ( str.charAt(0) == '0' && str.charAt(1) == 'X' ){
                str = str.substring(2);
            }
        }
        int nLen = str.length();
        for ( int i=0; i<nLen; ++i ){
            char ch = str.charAt(nLen-i-1);
            try {
                nResult += (GetHex(ch)*GetPower(16, i));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return nResult;
    }

    /**
     * 判断字符串能否转16进制
     * @param strHex
     * @return
     */
    public static boolean IsHex(String strHex){
        int i = 0;
        if ( strHex.length() > 2 ){
            if ( strHex.charAt(0) == '0' && (strHex.charAt(1) == 'X' || strHex.charAt(1) == 'x') ){
                i = 2;
            }
        }
        for ( ; i<strHex.length(); ++i ){
            char ch = strHex.charAt(i);
            if ( (ch>='0' && ch<='9') || (ch>='A' && ch<='F') || (ch>='a' && ch<='f') )
                continue;
            return false;
        }
        return true;
    }

    /**
     * 字符转16进制
     * @param ch
     * @return
     * @throws Exception
     */
    public static int GetHex(char ch) throws Exception{
        if ( ch>='0' && ch<='9' )
            return (int)(ch-'0');
        if ( ch>='a' && ch<='f' )
            return (int)(ch-'a'+10);
        if ( ch>='A' && ch<='F' )
            return (int)(ch-'A'+10);
        throw new Exception("error param");
    }

    public static int GetPower(int nValue, int nCount) throws Exception{
        if ( nCount <0 )
            throw new Exception("nCount can't small than 1!");
        if ( nCount == 0 )
            return 1;
        int nSum = 1;
        for ( int i=0; i<nCount; ++i ){
            nSum = nSum*nValue;
        }
        return nSum;
    }

    /**
     * 0x的string ->十进制string
     * @param s
     * @return
     */
    public static String hexStringToString(String s) {
        if (s == null || s.equals("")) {
            return null;
        }
        s = s.replace(" ", "");
        byte[] baKeyword = new byte[s.length() / 2];
        for (int i = 0; i < baKeyword.length; i++) {
            try {
                baKeyword[i] = (byte) (0xff & Integer.parseInt(
                        s.substring(i * 2, i * 2 + 2), 16));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            s = new String(baKeyword, "gbk");
            new String();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return s;
    }

}
