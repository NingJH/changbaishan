package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.WarnSetting;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface WarnSettingMapper {

    WarnSetting selectBySensorNo(@Param("sensorNo") String sensorNo);

}