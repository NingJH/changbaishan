package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TbsLdswj;
import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TbsLdswjMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbsLdswj record);

    int insertSelective(TbsLdswj record);

    TbsLdswj selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbsLdswj record);

    int updateByPrimaryKey(TbsLdswj record);

    List<Map<String,Object>> tbsLdswj(SearchVo vo);
}