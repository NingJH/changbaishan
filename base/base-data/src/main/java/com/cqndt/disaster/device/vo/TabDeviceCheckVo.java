package com.cqndt.disaster.device.vo;


import com.cqndt.disaster.device.domain.TabDeviceCheck;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class TabDeviceCheckVo extends TabDeviceCheck {

    /*
    当前页
     */
    private Integer page = 1;

    /*
    条数
     */
    private Integer limit = 10;


    //开始时间
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    //结束时间
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date endDate;
}
