package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TabHedgeCard;
import com.cqndt.disaster.device.domain.TabHedgeCardWithBLOBs;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TabHedgeCardMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_hedge_card
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_hedge_card
     *
     * @mbg.generated
     */
    int insert(TabHedgeCardWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_hedge_card
     *
     * @mbg.generated
     */
    int insertSelective(TabHedgeCardWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_hedge_card
     *
     * @mbg.generated
     */
    TabHedgeCardWithBLOBs selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_hedge_card
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TabHedgeCardWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_hedge_card
     *
     * @mbg.generated
     */
    int updateByPrimaryKeyWithBLOBs(TabHedgeCardWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_hedge_card
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TabHedgeCard record);

    /**
     * 根据灾害点编号查询避险明白卡
     * @param disNo
     * @return
     */
    List<TabHedgeCard> selectByDisNo(String disNo);
}