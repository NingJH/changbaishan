package com.cqndt.disaster.device.domain;

import lombok.Data;

import java.util.Date;

/**
 * Create Njh
 * @Time : 2019-11-16 15:28
 **/
@Data
public class Excel {
    //项目表tab_project
    private String projectNo;//项目编号
    private String projectName;//项目名称
    private String projectAddress;//项目地址
    private Date startTime;//项目开始时间
    private String monitorUnit;//监测单位
    private String projectLon;//项目经度（2000坐标）
    private String projectLat;//项目纬度（2000坐标）
    private String projectMan;//项目联系人
    private String projectManPhone;//联系方式

    //灾害点表tab_basic
    private String disNo;//灾害点编号
    private String disName;//灾害点名称
    private String disType;//灾害类型
    private String disasterName;//灾害类型对应的中文名称
    private String dangerLevel;//险情等级(A特大型B大型C中型D小型),

    //设备表tab_device
    private String areaName;//所属区县
    private String isOnline;//在线离线
    private String deviceName;//设备名称
    private String deviceNo;//设备编号
    private String deviceType;//设备类型
    private String deviceFactory;//设备厂商
    private String deviceLon;//设备经度
    private String deviceLat;//设备纬度
    private String simCard;//sim卡号
    private String transferType;//传输方式

    //监测点表tab_monitor
    private String monitorNo;//监测点编号
    private String position;//监测点位置
    private String monitorLon;//监测点经纬度
    private String monitorLat;//监测点经纬度

    //安装信息表tab_device_install
    private String anwz;//安装位置
    private String sgtj;//施工条件（1便利2较便利3不便利）
    private String gdfs;//供电方式（1.220v交流供电 2.太阳能供电）
    private String gdsm;//供电说明
    private char yxtj;//岩性条件（1.黄土2.砂卵石3.基岩4屋顶）
    private char cgtj;//采光条件（1.充足2.较充足3.不充足）
    private char ydxh;//移动信号（1强2中3弱）
    private char gprsXh;//gprs信号（1强2中3差4无）
    private char dx3g;//电信3g（1强2中3差4无）
    private char slqk;//水流情况（1强2中3弱）
    private Date sgrq;//施工日期
    private Date jgrq;//竣工日期
    private String sgry;//施工人员
    private String jcry;//检查人员
    private String ysry;//验收人员
    private char wgjc;//外观检查（1正常2损坏3水浸4污染）
    private String lgaz;//立杆安装（1成功2不成功）
    private String tyngd;//太阳能供电（1成功2不成功）
    private String sjcj;//数据采集（1成功2不成功）
    private String sjcs;//数据传输（1成功2不成功）
    private String azdw;//安装单位
    private String remark;//备注

    //传感器表tab_sensor
    private String sensorType;//传感器类型
    private String sensorName;//传感器名称
    private String sensorNo;//传感器编号
    private String sensorDeviceNo;//传感器表设备编号

    //用户绑定角色tab_user_device
    private String userId;//用户id
    private String deviceId;//用户绑定角色表角色id

    //刻度表tab_device_type
//    private Integer deviceType;//设备类型
//    private String monitorNo;//监测点编号
    private String type;//设备类型名称
    private String scale;//刻度
    private Date addTime;//添加时间
}
