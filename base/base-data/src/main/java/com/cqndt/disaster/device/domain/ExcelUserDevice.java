package com.cqndt.disaster.device.domain;

import lombok.Data;

/**
 * Create Njh
 * @Time : 2019-11-18 10:57
 **/
@Data
public class ExcelUserDevice {
    private Integer userId;
    private Integer deviceId;
}
