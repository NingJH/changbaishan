package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.vo.SysMenuVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * Description: 导航菜单
 * Date: 2019-02-28
 */

@Mapper
public interface SysMenuMapper {
    /**
     * 查询用户分配的菜单
     * @param userId
     * @param frontBack
     * @param parentId
     * @return
     */
    List<Map<String,Object>> queryMenuForUser(@Param("userId") String userId, @Param("frontBack") String frontBack, @Param("parentId") String parentId);

    /**
     * 查询超级管理员菜单
     * @param frontBack
     * @param parentId
     * @return
     */
    List<Map<String,Object>> queryMenuForAdmin(@Param("frontBack") String frontBack, @Param("parentId") String parentId);
    /**
     * 查询用户所分配权限
     * @param userId
     * @return
     */
    List<String> queryPermsForUser(@Param("userId") String userId);

    /**
     * 超级管理员所有权限
     * @return
     */
    List<String> queryPermsForAdmin();
    /**
     * 根据条件查询list
     * @param sysMenuVo 查询条件
     */
    List<SysMenuVo> list(SysMenuVo sysMenuVo);
    /**
     * 增加
     * @param sysMenuVo 条件
     */
    int save(SysMenuVo sysMenuVo);
    /**
     * 修改
     * @param sysMenuVo 条件
     */
    int update(SysMenuVo sysMenuVo);
    /**
     * 删除
     * @param id
     */
    int delete(@Param("id") String id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    SysMenuVo getTabMenuById(@Param("id") String id);
    /**
     * 根据id查询是否有下级menu
     * @param id
     */
    List<SysMenuVo> getTabMenuSub(@Param("id") String id);

    /**
     * 下拉框列表
     */
    List<Map<String,Object>> listTabMenu(SysMenuVo sysMenuVo);
    /**
     * 关联表删除
     */
    int deleteTabRoleMenu(String menuId);

    int deleteMenuButton(String parentId);

    /**
     * 中间表保存
     */
    int saveTabRoleMenu(@Param("menuId") String menuId, @Param("roleId") String roleId);

    /**
     * 子表信息
     */
    List<SysMenuVo> listTabRoleAll(SysMenuVo sysMenuVo);

    /**
     * 已选子表信息
     */
    List<SysMenuVo> listTabRoleSelect(Integer id);
    /**
     * 下拉框列表
     */
    List<Map<String,Object>> listTabStaticType(SysMenuVo sysMenuVo);

    int selectCount();


}
