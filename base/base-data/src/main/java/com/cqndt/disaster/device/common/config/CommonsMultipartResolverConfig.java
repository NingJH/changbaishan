package com.cqndt.disaster.device.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * 配置上传文件大小
 */
@Configuration
public class CommonsMultipartResolverConfig {

//    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver getCommonsMultipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(1048576000);
        multipartResolver.setMaxInMemorySize(1048576);
        return multipartResolver;
    }
}
