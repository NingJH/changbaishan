package com.cqndt.disaster.device.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther : meng
 * @Date : 2018/8/14 14:38
 **/

@Data
@NoArgsConstructor
public class Monitor {

	/**
	 * 监测点Id
	 */
	private Integer monitorId;
	
	/**
	 * 设备ID
	 */
	private Integer deviceId;
	
	/**
	 * 监测点名称
	 */
    private String monitorName;
    
    /**
     * 项目名称
     */
    private String projectName;
    
    /**
     * 监测点编号
     */
    private String monitorNo;
    
    /**
     * 监测方法名称
     */
    private String deviceTypeName;
    
    /**
     * 经度
     */
    private double lon;
    
    /**
     * 纬度
     */
    private double lat;
    
    /**
     * 设备实体类
     */
    private AppTabDevice device;

}
