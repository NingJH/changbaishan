package com.cqndt.disaster.device.dao.app;

import com.cqndt.disaster.device.domain.TabPhotography;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AppTabPhotographyMapper {

    Map<String,Object> selectByProjectId(@Param("projectId") Integer projectId, @Param("modelType") String modelType);

}