package com.cqndt.disaster.device.domain;

import java.math.BigDecimal;
import java.util.Date;

public class TbsTyl {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_tyl.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_tyl.sensor_no
     *
     * @mbg.generated
     */
    private String sensorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_tyl.monitor_no
     *
     * @mbg.generated
     */
    private String monitorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_tyl.value
     *
     * @mbg.generated
     */
    private BigDecimal value;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_tyl.time
     *
     * @mbg.generated
     */
    private Date time;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_tyl.id
     *
     * @return the value of tbs_tyl.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_tyl.id
     *
     * @param id the value for tbs_tyl.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    public String getSensorNo() {
        return sensorNo;
    }

    public void setSensorNo(String sensorNo) {
        this.sensorNo = sensorNo;
    }

    public String getMonitorNo() {
        return monitorNo;
    }

    public void setMonitorNo(String monitorNo) {
        this.monitorNo = monitorNo;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_tyl.time
     *
     * @return the value of tbs_tyl.time
     *
     * @mbg.generated
     */
    public Date getTime() {
        return time;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_tyl.time
     *
     * @param time the value for tbs_tyl.time
     *
     * @mbg.generated
     */
    public void setTime(Date time) {
        this.time = time;
    }
}