package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.TabDevice;
import lombok.Data;

import java.util.Date;

/**
 * Created By marc
 * Date: 2019/4/9  15:45
 * Description:
 */
@Data
public class TabDeviceVo extends TabDevice {

    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 区域等级
     */
    private String level;
    /**
     * 区域编码
     */
    private String areaCode;
    /**
     * 监测点编号
     */
    private String monitorNo;
    /**
     * 监测点id
     */
    private String monitorId;
    /**
     * 项目编号
     */
    private String projectNo;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 项目id
     */
    private Integer projectId;
    /**
     * 区域名称
     */
    private String areaName;
    /**
     * 项目监测单位
     */
    private String unitName;
    /**
     * 在线离线
     */
    private String stateName;
    /**
     * 判断设备在线离线时间(小时)
     */
    private Integer stateTime=24;
    /**
     * 离线时间(h)
     */
    private String offlineTime;
    /**
     * 设备在线离线(0离线 1在线)
     */
    private String state;

    private String monitorName;

    private String getAreaCodeSplit(){
        if(null != getAreaCode()){
            return getAreaCode().substring(0,2);
        }
        return null;
    }

}
