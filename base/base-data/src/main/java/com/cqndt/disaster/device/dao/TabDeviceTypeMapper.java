package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TabDeviceType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Time : 2019-09-15 21:47
 **/
@Mapper
public interface TabDeviceTypeMapper {

    int updateScale(@Param("typeId") Integer typeId,@Param("scale") String scale);

    int insertScale(TabDeviceType tabDeviceType);

    List<Map<String,Object>> getDeviceTypeName(@Param("projectId")String projectId,@Param("userId")Integer userId);

    List<Map<String,Object>> getMonitorName(@Param("projectId") String projectId,@Param("type") String type);

    List<Map<String,Object>> getType(@Param("deviceType") String deviceType);

    String selectMonitorNoBySensorNo(@Param("sensorNo") String sensorNo);

    List<TabDeviceType> getDeviceType(@Param("monitorNo") String monitorNo);

    Map<String,Object> getoriginalValue(@Param("sensorNo") String sensorNo);

    List<String> getSensorType();

    TabDeviceType selectDeviceType(@Param("deviceType") Integer deviceType, @Param("monitorNo") String monitorNo, @Param("deviceTypeName") String deviceTypeName);
    List<TabDeviceType> selectDeviceType1(@Param("deviceType") Integer deviceType, @Param("monitorNo") String monitorNo);

    String selectSensorTypeBySensorNo(String sensorNo);

}
