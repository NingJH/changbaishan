package com.cqndt.disaster.device.dao.app;

import com.cqndt.disaster.device.domain.TabAndroidVersion;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TabAndroidVersionMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_android_version
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_android_version
     *
     * @mbg.generated
     */
    int insert(TabAndroidVersion record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_android_version
     *
     * @mbg.generated
     */
    int insertSelective(TabAndroidVersion record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_android_version
     *
     * @mbg.generated
     */
    TabAndroidVersion selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_android_version
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TabAndroidVersion record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_android_version
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TabAndroidVersion record);

    List<Map<String, Object>> findNewVersion();
}