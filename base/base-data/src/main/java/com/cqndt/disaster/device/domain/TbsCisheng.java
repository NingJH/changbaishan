package com.cqndt.disaster.device.domain;

import java.math.BigDecimal;
import java.util.Date;

public class TbsCisheng {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_cisheng.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_cisheng.sensor_no
     *
     * @mbg.generated
     */
    private String sensorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_cisheng.monitor_no
     *
     * @mbg.generated
     */
    private String monitorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_cisheng.value
     *
     * @mbg.generated
     */
    private String value;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_cisheng.time
     *
     * @mbg.generated
     */
    private Date time;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_cisheng.id
     *
     * @return the value of tbs_cisheng.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_cisheng.id
     *
     * @param id the value for tbs_cisheng.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_cisheng.sensor_no
     *
     * @return the value of tbs_cisheng.sensor_no
     *
     * @mbg.generated
     */
    public String getSensorNo() {
        return sensorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_cisheng.sensor_no
     *
     * @param sensorNo the value for tbs_cisheng.sensor_no
     *
     * @mbg.generated
     */
    public void setSensorNo(String sensorNo) {
        this.sensorNo = sensorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_cisheng.monitor_no
     *
     * @return the value of tbs_cisheng.monitor_no
     *
     * @mbg.generated
     */
    public String getMonitorNo() {
        return monitorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_cisheng.monitor_no
     *
     * @param monitorNo the value for tbs_cisheng.monitor_no
     *
     * @mbg.generated
     */
    public void setMonitorNo(String monitorNo) {
        this.monitorNo = monitorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_cisheng.value
     *
     * @return the value of tbs_cisheng.value
     *
     * @mbg.generated
     */
    public String getValue() {
        return value;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_cisheng.value
     *
     * @param value the value for tbs_cisheng.value
     *
     * @mbg.generated
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_cisheng.time
     *
     * @return the value of tbs_cisheng.time
     *
     * @mbg.generated
     */
    public Date getTime() {
        return time;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_cisheng.time
     *
     * @param time the value for tbs_cisheng.time
     *
     * @mbg.generated
     */
    public void setTime(Date time) {
        this.time = time;
    }
}