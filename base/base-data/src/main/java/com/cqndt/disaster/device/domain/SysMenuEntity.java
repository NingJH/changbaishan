package com.cqndt.disaster.device.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * Created By marc
 * Date: 2019/3/6  10:02
 * Description:
 */
@Data
public class SysMenuEntity implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  id
     */
    private Integer id;
    /**
     *  菜单名称
     */
    private String name;
    /**
     *  菜单url地址
     */
    private String path;
    /**
     *  是否有子节点(0:是，1:否)
     */
    private String hassub;
    /**
     *  父级id
     */
    private Integer parentId;
    /**
     *  排序码
     */
    private Integer sortNum;
    /**
     *  图标
     */
    private String icons;
    /**
     *  菜单状态：0未删除，1删除
     */
    private String status;
    /**
     *  类型（目录1 菜单2 按钮3）
     */
    private String type;
    /**
     *  授权
     */
    private String perms;
    /**
     *  前端菜单0 后端菜单1
     */
    private String frontBack;
}
