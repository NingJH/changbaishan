package com.cqndt.disaster.device.domain;

public class TabBasicScope {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_basic_scope.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_basic_scope.photo_id
     *
     * @mbg.generated
     */
    private String photoId;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_basic_scope.id
     *
     * @return the value of tab_basic_scope.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_basic_scope.id
     *
     * @param id the value for tab_basic_scope.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_basic_scope.photo_id
     *
     * @return the value of tab_basic_scope.photo_id
     *
     * @mbg.generated
     */
    public String getPhotoId() {
        return photoId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_basic_scope.photo_id
     *
     * @param photoId the value for tab_basic_scope.photo_id
     *
     * @mbg.generated
     */
    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }
}