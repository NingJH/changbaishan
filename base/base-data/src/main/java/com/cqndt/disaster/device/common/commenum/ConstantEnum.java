package com.cqndt.disaster.device.common.commenum;

import lombok.Getter;

/**
 * @author lhl
 * @Description 常量类
 * @Date 2019/1/5 14:21
 */
@Getter
public enum  ConstantEnum {
    //命令头
    DEVICE_TYPE_F1("F1",""),
    DEVICE_TYPE_F2("F2",""),
    DEVICE_TYPE_F3("F3",""),
    DEVICE_TYPE_F4("F4",""),
    DEVICE_TYPE_F5("F5",""),
    DEVICE_TYPE_F6("F6",""),
    DEVICE_TYPE_NEW("0300010003",""),
    DEVICE_TYPE_NEW_DOWN("03010001",""),

    //数据长度
    DEVICE_HEIGHT_05("05",""),
    DEVICE_HEIGHT_03("03",""),
    DEVICE_HEIGHT_02("02",""),
    DEVICE_HEIGHT_0D("0D",""),
    DEVICE_HEIGHT_06("06",""),
    DEVICE_HEIGHT_0F("0F",""),
    ;

    private String type;
    private String message;

    ConstantEnum(String type, String message) {
        this.type = type;
        this.message = message;
    }
}
