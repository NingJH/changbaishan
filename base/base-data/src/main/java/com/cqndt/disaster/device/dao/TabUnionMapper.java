package com.cqndt.disaster.device.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 联合告警任务列表
 * @author liaohonglai
 * @date 2019/8/6
 */
@Mapper
public interface TabUnionMapper {
    /**
     * 插入数据联合告警任务表
     * @param identify 类型
     * @param vaule 值
     * @return
     */
    int insert(@Param("identify") String identify,@Param("vaule") Double vaule,@Param("sensorNo") String sensorNo);

    List<String> getDeviceNoList(@Param("id") Integer id);
}
