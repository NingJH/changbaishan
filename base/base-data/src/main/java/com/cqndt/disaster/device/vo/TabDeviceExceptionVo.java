package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.TabDeviceException;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/6/7  16:33
 * Description:
 */
@Data
public class TabDeviceExceptionVo extends TabDeviceException {
    /**
     * 页数
     */
    private Integer page = 1;
    /**
     * 条数
     */
    private Integer limit = 10;
    /**
     * 当前用户id
     */
    private String userId;
    /**
     * 设备名称
     */
    private String deviceName;
    /**
     * 设备类型
     */
    private String deviceType;
    /**
     * 设备类型名称
     */
    private String deviceTypeName;
    /**
     * 设备厂商
     */
    private String deviceFactory;
    /**
     * 设备厂商名称
     */
    private String deviceFactoryName;
    /**
     * 项目id
     */
    private String projectId;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 传输方式
     */
    private String transferType;
    /**
     * 电压值
     */
    private String dyz;
    /**
     *  电量
     */
    private String deviceElectric;
    /**
     * 异常类型名称
     */
    private String exceptionTypeName;
    /**
     * 状态名称
     */
    private String dealStateName;
    /**
     * 处理方式名称
     */
    private String dealTypeName;
    /**
     * 发送短信人员
     */
//    private List<TabHuman> listHuman;

    private List<Map<String,Object>> listHuman;

    /**
     * 发送短信内容
     */
    private String content;
    /**
     * 搜索开始时间
     */
    private String startFrom;
    /**
     * 搜索结束时间
     */
    private String startTo;

}
