package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Create Njh
 * @Time : 2019-11-17 15:41
 **/
@Mapper
public interface ExcelMapper {
    /**
     * 添加项目
     * @return
     */
    int insertProject(ExcelProject excelProject);

    /**
     * 添加灾害点
     * @return
     */
    int insertBasic(ExcelBasic excelBasic);

    /**
     * 添加设备
     * @return
     */
    void insertDevice(ExcelDevice excelDevice);

    /**
     * 添加监测点
     * @return
     */
    void insertMonitor(ExcelMonitor excelMonitor);

    /**
     * 添加传感器
     * @return
     */
    void insertSensor(ExcelSensor excelSensor);

    /**
     * 添加监测点
     * @return
     */
    void insertDeviceInstall(ExcelDeviceInstall excelDeviceInstall);

    void insertUserDevice(ExcelUserDevice excelUserDevice);

    void insertUserProject(ExcelUserProject excelUserProject);

    Integer selectDeviceNo(@Param("deviceNo") String deviceNo);

    Integer selectProjectNo(@Param("projetcNo") String projetcNo);

    void insertDeviceType(ExcelDeviceType excelDeviceType);

    List<Map<String,Object>> export(@Param("userId")int userId);

    int selectIdByUserName(@Param("userName") String userName);

    String selectSensorNoByDeviceNo(@Param("deviceNo") String deviceNo);

    void insertNiweiji(DataSupplement country);

    void insertYl(DataSupplement country);

    void insertKongxishuiya(DataSupplement country);

    void insertLfwy(DataSupplement country);

    void insertCisheng(DataSupplement country);

    void insertDxsw(DataSupplement country);

    void insertLlj(DataSupplement country);

    void insertSbwy(DataSupplement country);

    void insertQyjc(DataSupplement country);

    void insertKsw(DataSupplement country);

    void insertYsyl(DataSupplement country);

    void insertHsl(DataSupplement country);

    void insertQjy(DataSupplement country);

    void insertJlszy(DataSupplement country);

    int deleteNiweiji(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteYl(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteLfwy(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteKongxishuiya(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteCisheng(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteDxsw(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteLlj(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteTtcj(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteQyjc(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteKsw(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteYsyl(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteHsl(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteQjy(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int deleteJlszy(@Param("sensorNo")String sensorNo,@Param("startTime")String startTime,@Param("endTime")String endTime);

    int login(@Param("loginUsername")String loginUsername, @Param("loginPassword")String loginPassword);

    String selectNiweijiLostTime(String sensorNo);

    String selectYlLostTime(String sensorNo);

    String selectHslLostTime(String sensorNo);

    String selectLfwyLostTime(String sensorNo);

    String selectQjyLostTime(String sensorNo);

    String selectKongxishuiyaLostTime(String sensorNo);

    String selectCishengLostTime(String sensorNo);

    String selectDxswLostTime(String sensorNo);

    String selectLljLostTime(String sensorNo);

    String selectTtcjTime(String sensorNo);

    String selectQyjcLostTime(String sensorNo);

    String selectJlszyLostTime(String sensorNo);

    String selectKswLostTime(String sensorNo);

    String selectYsylLostTime(String sensorNo);

    List<Map<String, Object>> selectProjectNoByAreaName(String areaName);
}
