package com.cqndt.disaster.device.domain;

import lombok.Data;

import java.util.Date;

/**
 * Create Njh
 * @Time : 2019-11-18 19:37
 **/
@Data
public class ExcelDeviceType {
    private Integer deviceType;//设备类型
    private String monitorNo;//监测点编号
    private String type;//设备类型名称
    private String scale;//刻度
    private Date addTime;//添加时间
}
