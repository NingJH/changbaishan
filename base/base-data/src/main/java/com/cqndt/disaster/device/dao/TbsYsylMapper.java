package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TbsYsyl;
import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TbsYsylMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbsYsyl record);

    int insertSelective(TbsYsyl record);

    TbsYsyl selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbsYsyl record);

    int updateByPrimaryKey(TbsYsyl record);

    List<Map<String,Object>> tbsYsyl(SearchVo vo);
}