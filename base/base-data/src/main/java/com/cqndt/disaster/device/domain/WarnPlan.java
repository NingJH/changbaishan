package com.cqndt.disaster.device.domain;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 地裂缝任务表
 *
 * @Author yin_q
 * @Date 2019/9/4 14:53
 * @Email yin_qingqin@163.com
 **/
@Data
public class WarnPlan {

    private int id;

    // 标识
    private String sign;

    // 当次设备值
    private BigDecimal value;

    // 设备编号
    private String sensorNo;

    // 类型  0.地裂缝  1.坡倾  2.倾斜  3.gnss
    private Integer type;

    public WarnPlan() {
    }

    public WarnPlan(String sign, BigDecimal value, String sensorNo, Integer type) {
        this.sign = sign;
        this.value = value;
        this.sensorNo = sensorNo;
        this.type = type;
    }
}
