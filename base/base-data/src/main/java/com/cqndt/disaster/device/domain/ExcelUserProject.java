package com.cqndt.disaster.device.domain;

import lombok.Data;

/**
 * Create Njh
 * @Time : 2019-11-18 11:35
 **/
@Data
public class ExcelUserProject {
    private Integer userId;//用户id
    private Integer projectId;//项目id
}
