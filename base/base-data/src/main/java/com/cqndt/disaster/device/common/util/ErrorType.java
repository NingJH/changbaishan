package com.cqndt.disaster.device.common.util;

/**
 * user: zhaojianji
 * date: 2017/05/25
 * desc:  描述用途
 */
public enum ErrorType {

    SERVER_ERROR(500,"服务出错!"),
    DATA_BIND_ERROR(400,"数据绑定错误!"),

    NOT_FOUND(404,"不存在该资源!"),
    BAD_REQUEST(611,"参数错误"),
    USER_NAME_EXISTED(606,"用户名已经存在!"),
    LOGIN_CODE_ERROR(601,"验证码错误"),
    TOKEN_ERROR(401,"登陆过期,请重新登陆"),
    NICKNAME_ERROR(603,"昵称不符合规范"),
    NICKNAME_EXISTED(602,"昵称已存在"),
    PHONE_ERROR(604,"手机号格式不正确"),
    SIGN_ERROR(605,"签名格式不符合规范"),
    SERVICE_ERROR(606,"服务器连接失败"),
    EMCREATE_ERROR(607,"环信创建聊天室失败"),
    STOCK_ERROR(608,"余额不足"),
    IMUUID_ERROR(609,"IMuuid错误"),
    VIDEO_SAVE_ERROR(610,"视频上传失败"),
    TYPE_ERROR(612,"请使用新版本APP上传"),
    TEAM_LEAST_ERROR(613,"团队报名不能少于3人"),
    TEAM_MOST_ERROR(614,"团队报名5V5只能5人"),
    VOTE_ERROR(615,"每天只能投票3次"),
    VIDEO_DELETE(616,"视频已被删除"),
    USERNAME_NOTEXIST(617,"账号或密码错误"),
    PASSWORD_ERROR(618,"密码错误"),
    AUTH_NAME_EXISTED(2001,"权限名已经存在!");


    private int code;
    private String errorMsg;

    ErrorType(int code,String errorMsg) {
        this.code = code;
        this.errorMsg = errorMsg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }


   /* private static  final Map<String,ErrorType> ERROR_TYPE_MAP = new HashMap<>();

    static {
        for(ErrorType type : values()){
            ERROR_TYPE_MAP.put(type.name(),type);
        }
    }

    public static ErrorType fromString(String name){

        return ERROR_TYPE_MAP.get(name);
    }*/



}
