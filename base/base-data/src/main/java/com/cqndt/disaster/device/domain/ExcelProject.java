package com.cqndt.disaster.device.domain;

import lombok.Data;

import java.util.Date;

/**
 * Create Njh
 * @Time : 2019-11-17 11:56
 **/
@Data
public class ExcelProject {
    //项目表tab_project
    private String id;
    private String projectNo;//项目编号
    private String projectName;//项目名称
    private String projectAdd;//项目地址
    private String state;//状态(0 删除 1 正常)
    private Date startTime;//项目开始时间
    private Integer monitorUnit;//监测单位
    private String projectLon;//项目经度（2000坐标）
    private String projectLat;//项目纬度（2000坐标）
    private String projectMan;//项目联系人
    private String projectManPhone;//联系方式
    private String disNo;//灾害点编号

    private String userId;//用户id
}
