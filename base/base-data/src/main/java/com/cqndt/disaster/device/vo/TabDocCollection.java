package com.cqndt.disaster.device.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Auther : meng
 * @Date : 2018/8/17 9:12
 **/

@Data
@NoArgsConstructor
public class TabDocCollection {
    int totalCount;
    List<TabDoc> tabDocList;
}
