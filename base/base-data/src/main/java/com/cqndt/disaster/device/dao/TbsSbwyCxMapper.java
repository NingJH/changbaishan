package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TbsSbwyCx;
import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TbsSbwyCxMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_cx
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_cx
     *
     * @mbg.generated
     */
    int insert(TbsSbwyCx record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_cx
     *
     * @mbg.generated
     */
    int insertSelective(TbsSbwyCx record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_cx
     *
     * @mbg.generated
     */
    TbsSbwyCx selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_cx
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TbsSbwyCx record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_cx
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TbsSbwyCx record);

    List<Map<String,Object>> tbsSbwyCx(SearchVo vo);

    List<Map<String, Object>> tbsSbwyCxIntervals(String sensorNo);
}