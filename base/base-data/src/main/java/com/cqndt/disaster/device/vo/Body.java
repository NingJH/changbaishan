package com.cqndt.disaster.device.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Body {
  private String body;
}
