package com.cqndt.disaster.device.dao.app;

import com.cqndt.disaster.device.domain.TabTwoRound;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AppTabTwoRoundMapper {
    /**
     * 根据项目id获取相应的720全景
     * @param projectId
     * @param modelType
     * @return
     */
    Map<String,Object> selectByProjectId(@Param("projectId") Integer projectId, @Param("modelType") String modelType);
}