package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TbsQyjc;
import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;
@Mapper
public interface TbsQyjcMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbsQyjc record);

    int insertSelective(TbsQyjc record);

    TbsQyjc selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbsQyjc record);

    int updateByPrimaryKey(TbsQyjc record);

    List<Map<String,Object>> tbsQyjc(SearchVo vo);

    TbsQyjc getLastValue(@Param("sensorNo")String sensorNo, @Param("time") Date time);

    TbsQyjc getPreviousValue(@Param("sensorNo")String sensorNo, @Param("time") Date time);
}