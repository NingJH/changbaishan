package com.cqndt.disaster.device.common.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

public class JsonArrayToList {

    public static String parseJSON2List(String sql){
        if(null == sql){
            return null;
        }
        List<Map<String, String>> data = JSONArray.parseObject(sql, List.class);
        StringBuilder sb = new StringBuilder();
        String str = null;
        for (Map mapList : data) {
            sb.append(" and ");
            if(mapList.get("value") instanceof JSONArray){
                List<String> list = JSONObject.parseArray(mapList.get("value").toString(),String.class);
                for(int i=0;i<list.size();i++){
                    if(i==0){
                        sb.append("(");
                    }
                    sb.append(list.get(i));
                    if(i<list.size()-1){
                        sb.append(" or ");
                    }
                }
                sb.append(")");
            }else {
                sb.append(mapList.get("fieldName"));
                sb.append(mapList.get("value"));
            }
            str = sb.toString();
        }
        return str;
    }
}
