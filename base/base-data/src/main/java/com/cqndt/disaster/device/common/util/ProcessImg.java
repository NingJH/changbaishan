package com.cqndt.disaster.device.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.util.List;

/**
 * 视频截图
 *
 * @Author yin_q
 * @Date 2019/7/19 10:01
 * @Email yin_qingqin@163.com
 **/
@Slf4j
public class ProcessImg {

    // 视频路径
    //private static String videoPath = "C:\\Users\\yin_q\\Desktop\\J.Fla rerer - 副本.mp4";

    // 转换工具
   // @Value("${ffmpegPath}")
    //private static String ffmpegPath = "E:\\ffmpeg\\ffmpeg.exe";
    //private static String ffmpegPath;

    public static void getImg(String path,String ffmpegPath){
        File file = new File(path);
        if (!file.exists()) {
            log.error("视频截取失败：路径[" + path + "]对应的视频文件不存在!");
            return;
        }
        List<String> commands = new java.util.ArrayList<String>();
        commands.add(ffmpegPath);
        commands.add("-i");
        commands.add(path);
        commands.add("-y");
        commands.add("-f");
        commands.add("image2");
        commands.add("-ss");
        commands.add("1");// 这个参数是设置截取视频多少秒时的画面
        commands.add("-s");
        commands.add("700x525");
        commands.add(path.substring(0, path.lastIndexOf("."))
                .replaceFirst("vedio", "file") + ".jpg");
        try {
            ProcessBuilder builder = new ProcessBuilder();
            builder.command(commands);
            builder.start();
            log.info("截取成功");
        } catch (Exception e) {
            log.error("视频截取失败，原因：" + e.getMessage());
        }
    }

    /*public static void main(String[] args) {
        getImg(videoPath);
    }*/

}
