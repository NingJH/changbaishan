package com.cqndt.disaster.device.dao.app;

import com.cqndt.disaster.device.domain.TabReportingFile;
import com.cqndt.disaster.device.vo.TabDoc;
import com.cqndt.disaster.device.vo.TabReportFileVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AppTabReportingFileMapper {

    List<TabDoc> listReportingFile(TabReportFileVo vo);
}