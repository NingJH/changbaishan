package com.cqndt.disaster.device.domain;

import lombok.Data;

/**
 * Create Njh
 * @Time : 2019-11-17 11:57
 **/
@Data
public class ExcelDevice {
    //设备表tab_device
    private Integer id;
    private String areaCode;//所属区县
    private String isOnline;//在线离线
    private String deviceName;//设备名称
    private String deviceNo;//设备编号
    private String deviceType;//设备类型
    private String deviceFactory;//设备厂商
    private String deviceLon;//设备经度
    private String deviceLat;//设备纬度
    private String simCard;//sim卡号
    private String transferType;//传输方式
    private String userId;//用户id
    private String deviceTypeName;//设备类型名称
    private String address;//安装位置
}
