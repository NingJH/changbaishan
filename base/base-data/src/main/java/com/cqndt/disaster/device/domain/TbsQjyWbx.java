package com.cqndt.disaster.device.domain;

import java.math.BigDecimal;
import java.util.Date;

public class TbsQjyWbx {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_qjy_wbx.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_qjy_wbx.sensor_no
     *
     * @mbg.generated
     */
    private String sensorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_qjy_wbx.monitor_no
     *
     * @mbg.generated
     */
    private String monitorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_qjy_wbx.qxjd
     *
     * @mbg.generated
     */
    private BigDecimal qxjd;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_qjy_wbx.temp
     *
     * @mbg.generated
     */
    private BigDecimal temp;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_qjy_wbx.time
     *
     * @mbg.generated
     */
    private Date time;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_qjy_wbx.id
     *
     * @return the value of tbs_qjy_wbx.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_qjy_wbx.id
     *
     * @param id the value for tbs_qjy_wbx.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_qjy_wbx.sensor_no
     *
     * @return the value of tbs_qjy_wbx.sensor_no
     *
     * @mbg.generated
     */
    public String getSensorNo() {
        return sensorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_qjy_wbx.sensor_no
     *
     * @param sensorNo the value for tbs_qjy_wbx.sensor_no
     *
     * @mbg.generated
     */
    public void setSensorNo(String sensorNo) {
        this.sensorNo = sensorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_qjy_wbx.monitor_no
     *
     * @return the value of tbs_qjy_wbx.monitor_no
     *
     * @mbg.generated
     */
    public String getMonitorNo() {
        return monitorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_qjy_wbx.monitor_no
     *
     * @param monitorNo the value for tbs_qjy_wbx.monitor_no
     *
     * @mbg.generated
     */
    public void setMonitorNo(String monitorNo) {
        this.monitorNo = monitorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_qjy_wbx.qxjd
     *
     * @return the value of tbs_qjy_wbx.qxjd
     *
     * @mbg.generated
     */
    public BigDecimal getQxjd() {
        return qxjd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_qjy_wbx.qxjd
     *
     * @param qxjd the value for tbs_qjy_wbx.qxjd
     *
     * @mbg.generated
     */
    public void setQxjd(BigDecimal qxjd) {
        this.qxjd = qxjd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_qjy_wbx.temp
     *
     * @return the value of tbs_qjy_wbx.temp
     *
     * @mbg.generated
     */
    public BigDecimal getTemp() {
        return temp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_qjy_wbx.temp
     *
     * @param temp the value for tbs_qjy_wbx.temp
     *
     * @mbg.generated
     */
    public void setTemp(BigDecimal temp) {
        this.temp = temp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_qjy_wbx.time
     *
     * @return the value of tbs_qjy_wbx.time
     *
     * @mbg.generated
     */
    public Date getTime() {
        return time;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_qjy_wbx.time
     *
     * @param time the value for tbs_qjy_wbx.time
     *
     * @mbg.generated
     */
    public void setTime(Date time) {
        this.time = time;
    }
}