package com.cqndt.disaster.device.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @Description:
 * @Auther: lhl
 * @Date: 2019/4/23 13:19
 */
@Mapper
public interface DataCtccMapper {

    Integer saveAndList(Map map);

}
