package com.cqndt.disaster.device.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Created By marc
 * Date: 2019/4/11  9:35
 * Description: 查询条件
 */
@Data
public class SearchVo implements Serializable {
    /**
     * 设备名称
     */
     private String deviceName;
    /**
     * 传感器编号
     */
    private String sensorNo;
    /**
     * 安装布置点编号
     */
    private String monitorNo;
    /**
     * 传感器类型
     */
    private String sensorType;
    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 时间类型(今天:today, 三天：threeDay, 本周:week, 本月:month, 近三个月:3month)
     */
    private String dateType;

    private String orderDesc;

    @Override
    public String toString() {
        return "SearchVo{" +
                "sensorNo='" + sensorNo + '\'' +
                ", monitorNo='" + monitorNo + '\'' +
                ", sensorType='" + sensorType + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", dateType='" + dateType + '\'' +
                '}';
    }
}
