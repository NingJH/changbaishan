package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TbsKsw;
import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TbsKswMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbsKsw record);

    int insertSelective(TbsKsw record);

    TbsKsw selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbsKsw record);

    int updateByPrimaryKey(TbsKsw record);

    List<Map<String,Object>> tbsKsw(SearchVo vo);
}