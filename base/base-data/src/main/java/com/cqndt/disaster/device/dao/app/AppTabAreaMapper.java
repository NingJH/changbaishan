package com.cqndt.disaster.device.dao.app;

import com.cqndt.disaster.device.domain.TabArea;
import com.cqndt.disaster.device.vo.TabAreaVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AppTabAreaMapper {
    /**
     * 获取下一级
     * @param areaCode
     * @return
     */
    List<TabAreaVo> getNextArea(String areaCode);

    /**
     * 获取上一级
     * @param areaCode
     * @return
     */
    List<TabAreaVo> getPreArea(String areaCode);

    /**
     * 获取当前
     * @param areaCode
     * @return
     */
    List<TabAreaVo> getArea(String areaCode);

}