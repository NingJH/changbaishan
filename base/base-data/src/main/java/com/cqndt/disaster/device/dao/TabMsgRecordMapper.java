package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.vo.TabMsgRecord;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TabMsgRecordMapper {

    /**
     * 添加短信记录
     * @param tabMsgRecord
     * @return
     */
    int addTabMsgRecord(TabMsgRecord tabMsgRecord);
}
