package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.TabProject;
import lombok.Data;

/**
 * Created By marc
 * Date: 2019/4/12  10:21
 * Description:
 */
@Data
public class TabProjectVo extends TabProject {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 每页条数
     */
    private Integer limit=10;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 状态
     */
    private String stateName;
    /**
     * 是否结束
     */
    private String endName;
    /**
     * 人工、自动
     */
    private String autoName;
    /**
     * 区域名称
     */
    private String areaName;
    /**
     * 区域等级
     */
    private String level;
    /**
     * 区域编码
     */
    private String areaCode;
    /**
     * 灾害类型名称
     */
    private String disTypeName;
    /**
     * 灾害类型
     */
    private String disType;
    /**
     * 险情等级
     */
    private String dangerLevelName;

    private String search;

    private String getAreaCodeSplit(){
        if(null != getAreaCode()){
            return getAreaCode().substring(0,2);
        }
        return null;
    }
}
