package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TabSensorInit;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;
@Mapper
public interface TabSensorInitMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_sensor_init
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_sensor_init
     *
     * @mbg.generated
     */
    int insert(TabSensorInit record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_sensor_init
     *
     * @mbg.generated
     */
    int insertSelective(TabSensorInit record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_sensor_init
     *
     * @mbg.generated
     */
    TabSensorInit selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_sensor_init
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TabSensorInit record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_sensor_init
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TabSensorInit record);

    Map<String,Object> getDataByFiledName(@Param("sensorNo") String sensorNo, @Param("filedName") String filedName);
}