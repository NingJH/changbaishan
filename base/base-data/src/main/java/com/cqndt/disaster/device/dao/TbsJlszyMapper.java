package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TbsJlszy;
import com.cqndt.disaster.device.domain.TbsQyjc;
import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Mapper
public interface TbsJlszyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbsJlszy record);

    int insertSelective(TbsJlszy record);

    TbsJlszy selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbsJlszy record);

    int updateByPrimaryKey(TbsJlszy record);

    List<Map<String,Object>> tbsJlszy(SearchVo vo);

    List<Map<String,Object>> selectSensor();

    Set<String> selectTime();

    int updateSensor(@Param("id")String id,@Param("finalValue") double finalValue);

    int deleteSensor(@Param("id")String id);

    TbsJlszy getLastValue(@Param("sensorNo")String sensorNo, @Param("time") Date time);

    TbsJlszy getPreviousValue(@Param("sensorNo")String sensorNo, @Param("time") Date time);
}