package com.cqndt.disaster.device.dao.warn;

import com.cqndt.disaster.device.domain.WarnPlan;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author yin_q
 * @Date 2019/8/31 14:21
 * @Email yin_qingqin@163.com
 **/
@Mapper
public interface WarnPlanMapper {

    /**
     * 查询地裂缝告警任务表
     * @return
     */
    List<WarnPlan> queryWarnPlan(Integer type);

    /**
     * 根据任务编号删除任务
     * @return
     */
    int deleteWarnPlanById(Integer id);

    /**
     * 新增地裂缝预警任务
     * @return
     */
    int insertWarnPlan(WarnPlan warnPlan);
}
