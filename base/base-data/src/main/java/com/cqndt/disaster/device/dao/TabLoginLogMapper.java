package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TabLoginLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface TabLoginLogMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_login_log
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_login_log
     *
     * @mbg.generated
     */
    int insert(TabLoginLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_login_log
     *
     * @mbg.generated
     */
    int insertSelective(TabLoginLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_login_log
     *
     * @mbg.generated
     */
    TabLoginLog selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_login_log
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TabLoginLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_login_log
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TabLoginLog record);

    TabLoginLog getByUserNameAndPlat(@Param("userName") String userName, @Param("platType")Integer platType);
}