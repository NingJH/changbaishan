package com.cqndt.disaster.device.domain;

import lombok.Data;

@Data
public class WarnPeople {
    private Integer id;

    private String name;

    private String phone;

    private String sensorNo;

}