package com.cqndt.disaster.device.common.util;

public class CamelNamedUtil {

    public static final char UNDERLINE='_';

    /**
     * 驼峰风格转下划线
     * @param param
     * @return
     */
    public static String camel2Underline(String param){
        if (param==null||"".equals(param.trim())){
            return "";
        }
        int len=param.length();
        if(len <2){
            return param;
        }
        StringBuilder sb=new StringBuilder(len);
        //从第二个单词开始
        for (int i = 0; i < len; i++) {
            char c=param.charAt(i);
            if (Character.isUpperCase(c)){
                if(i>0){
                    sb.append(UNDERLINE);
                }
                sb.append(Character.toLowerCase(c));
            }else{
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * 下划线转驼峰风格
     * @param param
     * @return
     */
    public static String underline2Camel(String param){
        if (param==null||"".equals(param.trim())){
            return "";
        }
        int len=param.length();
        StringBuilder sb=new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c=param.charAt(i);
            if (c==UNDERLINE){
                if (++i<len){
                    sb.append(Character.toUpperCase(param.charAt(i)));
                }
            }else{
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(camel2Underline("TabString"));
    }
}
