package com.cqndt.disaster.device.common.util;

import java.util.zip.CRC32;

/**
 * @author lhl
 * @Description
 * @Date 2019/1/4 16:49
 */
public class CRC8 {

    public static int FindCRC(byte[] data) {

        int CRC = 0;

        int genPoly = 0x8C;

        for (int i = 0; i < data.length; i++) {

            CRC ^= data[i];

            CRC &= 0xff;//保证CRC余码输出为1字节。

            for (int j = 0; j < 8; j++) {

                if ((CRC & 0x01) != 0) {

                    CRC = (CRC >> 1) ^ genPoly;

                    CRC &= 0xff;//保证CRC余码输出为1字节。

                } else {

                    CRC >>= 1;

                }

            }

        }

        return CRC ^ 0xff;

    }

    public static long crc32(byte[] data){
        CRC32 crc32 = new CRC32();
        crc32.update(data);
        return crc32.getValue();
    }


    public static void main(String[] args) {
        byte[] bytes = {
                (byte) 0xF6,
                Byte.parseByte("05", 16),
                Byte.parseByte("17", 16),
                Byte.parseByte("00", 16),
                Byte.parseByte("00", 16),
                Byte.parseByte("00", 16),
                Byte.parseByte("40", 16),
                (byte) 0xb5
        };

        int testcrc = FindCRC(NumberChangeUtil.stringToByte("f60d0c000000000000000000000000"));
        System.out.println(Integer.toHexString(testcrc));

    }


}
