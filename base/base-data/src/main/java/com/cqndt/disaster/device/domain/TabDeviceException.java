package com.cqndt.disaster.device.domain;

import java.util.Date;

public class TabDeviceException {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.device_no
     *
     * @mbg.generated
     */
    private String deviceNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.data_exception
     *
     * @mbg.generated
     */
    private String dataException;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.exception_type
     *
     * @mbg.generated
     */
    private String exceptionType;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.deal_state
     *
     * @mbg.generated
     */
    private String dealState;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.deal_time_from
     *
     * @mbg.generated
     */
    private String dealTimeFrom;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.create_time
     *
     * @mbg.generated
     */
    private Date createTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.deal_type
     *
     * @mbg.generated
     */
    private String dealType;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.exception_remark
     *
     * @mbg.generated
     */
    private String exceptionRemark;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.defend_remark
     *
     * @mbg.generated
     */
    private String defendRemark;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.deal_person_name
     *
     * @mbg.generated
     */
    private String dealPersonName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.exception_time_from
     *
     * @mbg.generated
     */
    private String exceptionTimeFrom;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.exception_time_to
     *
     * @mbg.generated
     */
    private String exceptionTimeTo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.exception_data_current
     *
     * @mbg.generated
     */
    private String exceptionDataCurrent;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.exception_data_pre
     *
     * @mbg.generated
     */
    private String exceptionDataPre;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.rate_change
     *
     * @mbg.generated
     */
    private String rateChange;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.deal_person_phone
     *
     * @mbg.generated
     */
    private String dealPersonPhone;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.operate_person_name
     *
     * @mbg.generated
     */
    private String operatePersonName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.operate_time
     *
     * @mbg.generated
     */
    private Date operateTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.deal_time_to
     *
     * @mbg.generated
     */
    private String dealTimeTo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.defend_enter_time
     *
     * @mbg.generated
     */
    private Date defendEnterTime;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_device_exception.defend_imgs
     *
     * @mbg.generated
     */
    private String defendImgs;

    private String dealBasic;

    public String getDealBasic() {
        return dealBasic;
    }

    public void setDealBasic(String dealBasic) {
        this.dealBasic = dealBasic;
    }

    public String getDealTimeFrom() {
        return dealTimeFrom;
    }

    public void setDealTimeFrom(String dealTimeFrom) {
        this.dealTimeFrom = dealTimeFrom;
    }

    public String getDealTimeTo() {
        return dealTimeTo;
    }

    public void setDealTimeTo(String dealTimeTo) {
        this.dealTimeTo = dealTimeTo;
    }

    public Date getDefendEnterTime() {
        return defendEnterTime;
    }

    public void setDefendEnterTime(Date defendEnterTime) {
        this.defendEnterTime = defendEnterTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.id
     *
     * @return the value of tab_device_exception.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.id
     *
     * @param id the value for tab_device_exception.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.device_no
     *
     * @return the value of tab_device_exception.device_no
     *
     * @mbg.generated
     */
    public String getDeviceNo() {
        return deviceNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.device_no
     *
     * @param deviceNo the value for tab_device_exception.device_no
     *
     * @mbg.generated
     */
    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.data_exception
     *
     * @return the value of tab_device_exception.data_exception
     *
     * @mbg.generated
     */
    public String getDataException() {
        return dataException;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.data_exception
     *
     * @param dataException the value for tab_device_exception.data_exception
     *
     * @mbg.generated
     */
    public void setDataException(String dataException) {
        this.dataException = dataException;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.exception_type
     *
     * @return the value of tab_device_exception.exception_type
     *
     * @mbg.generated
     */
    public String getExceptionType() {
        return exceptionType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.exception_type
     *
     * @param exceptionType the value for tab_device_exception.exception_type
     *
     * @mbg.generated
     */
    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.deal_state
     *
     * @return the value of tab_device_exception.deal_state
     *
     * @mbg.generated
     */
    public String getDealState() {
        return dealState;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.deal_state
     *
     * @param dealState the value for tab_device_exception.deal_state
     *
     * @mbg.generated
     */
    public void setDealState(String dealState) {
        this.dealState = dealState;
    }



    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.create_time
     *
     * @return the value of tab_device_exception.create_time
     *
     * @mbg.generated
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.create_time
     *
     * @param createTime the value for tab_device_exception.create_time
     *
     * @mbg.generated
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.deal_type
     *
     * @return the value of tab_device_exception.deal_type
     *
     * @mbg.generated
     */
    public String getDealType() {
        return dealType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.deal_type
     *
     * @param dealType the value for tab_device_exception.deal_type
     *
     * @mbg.generated
     */
    public void setDealType(String dealType) {
        this.dealType = dealType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.exception_remark
     *
     * @return the value of tab_device_exception.exception_remark
     *
     * @mbg.generated
     */
    public String getExceptionRemark() {
        return exceptionRemark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.exception_remark
     *
     * @param exceptionRemark the value for tab_device_exception.exception_remark
     *
     * @mbg.generated
     */
    public void setExceptionRemark(String exceptionRemark) {
        this.exceptionRemark = exceptionRemark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.defend_remark
     *
     * @return the value of tab_device_exception.defend_remark
     *
     * @mbg.generated
     */
    public String getDefendRemark() {
        return defendRemark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.defend_remark
     *
     * @param defendRemark the value for tab_device_exception.defend_remark
     *
     * @mbg.generated
     */
    public void setDefendRemark(String defendRemark) {
        this.defendRemark = defendRemark;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.deal_person_name
     *
     * @return the value of tab_device_exception.deal_person_name
     *
     * @mbg.generated
     */
    public String getDealPersonName() {
        return dealPersonName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.deal_person_name
     *
     * @param dealPersonName the value for tab_device_exception.deal_person_name
     *
     * @mbg.generated
     */
    public void setDealPersonName(String dealPersonName) {
        this.dealPersonName = dealPersonName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.exception_time_from
     *
     * @return the value of tab_device_exception.exception_time_from
     *
     * @mbg.generated
     */
    public String getExceptionTimeFrom() {
        return exceptionTimeFrom;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.exception_time_from
     *
     * @param exceptionTimeFrom the value for tab_device_exception.exception_time_from
     *
     * @mbg.generated
     */
    public void setExceptionTimeFrom(String exceptionTimeFrom) {
        this.exceptionTimeFrom = exceptionTimeFrom;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.exception_time_to
     *
     * @return the value of tab_device_exception.exception_time_to
     *
     * @mbg.generated
     */
    public String getExceptionTimeTo() {
        return exceptionTimeTo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.exception_time_to
     *
     * @param exceptionTimeTo the value for tab_device_exception.exception_time_to
     *
     * @mbg.generated
     */
    public void setExceptionTimeTo(String exceptionTimeTo) {
        this.exceptionTimeTo = exceptionTimeTo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.exception_data_current
     *
     * @return the value of tab_device_exception.exception_data_current
     *
     * @mbg.generated
     */
    public String getExceptionDataCurrent() {
        return exceptionDataCurrent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.exception_data_current
     *
     * @param exceptionDataCurrent the value for tab_device_exception.exception_data_current
     *
     * @mbg.generated
     */
    public void setExceptionDataCurrent(String exceptionDataCurrent) {
        this.exceptionDataCurrent = exceptionDataCurrent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.exception_data_pre
     *
     * @return the value of tab_device_exception.exception_data_pre
     *
     * @mbg.generated
     */
    public String getExceptionDataPre() {
        return exceptionDataPre;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.exception_data_pre
     *
     * @param exceptionDataPre the value for tab_device_exception.exception_data_pre
     *
     * @mbg.generated
     */
    public void setExceptionDataPre(String exceptionDataPre) {
        this.exceptionDataPre = exceptionDataPre;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.rate_change
     *
     * @return the value of tab_device_exception.rate_change
     *
     * @mbg.generated
     */
    public String getRateChange() {
        return rateChange;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.rate_change
     *
     * @param rateChange the value for tab_device_exception.rate_change
     *
     * @mbg.generated
     */
    public void setRateChange(String rateChange) {
        this.rateChange = rateChange;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.deal_person_phone
     *
     * @return the value of tab_device_exception.deal_person_phone
     *
     * @mbg.generated
     */
    public String getDealPersonPhone() {
        return dealPersonPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.deal_person_phone
     *
     * @param dealPersonPhone the value for tab_device_exception.deal_person_phone
     *
     * @mbg.generated
     */
    public void setDealPersonPhone(String dealPersonPhone) {
        this.dealPersonPhone = dealPersonPhone;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.operate_person_name
     *
     * @return the value of tab_device_exception.operate_person_name
     *
     * @mbg.generated
     */
    public String getOperatePersonName() {
        return operatePersonName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.operate_person_name
     *
     * @param operatePersonName the value for tab_device_exception.operate_person_name
     *
     * @mbg.generated
     */
    public void setOperatePersonName(String operatePersonName) {
        this.operatePersonName = operatePersonName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.operate_time
     *
     * @return the value of tab_device_exception.operate_time
     *
     * @mbg.generated
     */
    public Date getOperateTime() {
        return operateTime;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.operate_time
     *
     * @param operateTime the value for tab_device_exception.operate_time
     *
     * @mbg.generated
     */
    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }


    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_device_exception.defend_imgs
     *
     * @return the value of tab_device_exception.defend_imgs
     *
     * @mbg.generated
     */
    public String getDefendImgs() {
        return defendImgs;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_device_exception.defend_imgs
     *
     * @param defendImgs the value for tab_device_exception.defend_imgs
     *
     * @mbg.generated
     */
    public void setDefendImgs(String defendImgs) {
        this.defendImgs = defendImgs;
    }
}