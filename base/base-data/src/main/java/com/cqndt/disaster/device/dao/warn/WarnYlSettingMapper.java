package com.cqndt.disaster.device.dao.warn;

import com.cqndt.disaster.device.domain.WarnYlSetting;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author yin_q
 * @Date 2019/8/31 14:21
 * @Email yin_qingqin@163.com
 **/
@Mapper
public interface WarnYlSettingMapper {

    /**
     * 根据设备编号获取雨量阈值信息
     * @param sensorNo
     * @return
     */
    List<WarnYlSetting> queryWarnYlSettingList(@Param("sensorNo") String sensorNo);

    /**
     * 根据设备编号删除 雨量阈值信息
     * @param sensorNo
     * @return
     */
    int deleteWarnYlSettingBySensorNo(@Param("sensorNo") String sensorNo);

    /**
     * 新增阈值信息
     * @param warnYlSetting
     * @return
     */
    int insertWarnYlSetting(WarnYlSetting warnYlSetting);
}
