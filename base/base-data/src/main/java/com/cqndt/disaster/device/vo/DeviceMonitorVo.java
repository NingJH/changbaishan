package com.cqndt.disaster.device.vo;

import lombok.Data;

/**
 * Created By marc
 * Date: 2019/4/17  15:32
 * Description:设备、监测点编号
 */
@Data
public class DeviceMonitorVo {
    /**
     * 设备编号
     */
    private String deviceNo;
    /**
     * 设备名称
     */
    private String deviceName;
    /**
     * 监测点编号
     */
    private String monitorNo;

    private String deviceId;

    private String monitorName;
}
