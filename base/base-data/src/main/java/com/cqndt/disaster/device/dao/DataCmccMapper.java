package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.vo.Data;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Auther: lhl
 * @Date: 2019/4/23 13:19
 */
@Mapper
public interface DataCmccMapper {
    Integer save(Data data);

    List<Map<String, String>> selectNo();

    int selectCountBySensorNo(@Param("sensorNo") String sensorNo, @Param("dsId") String dsId);

    List<Map<String, Object>> selectQingXieWX(@Param("sensorNo") String sensorNo, @Param("start") int start, @Param("limit") int limit);

    List<Map<String, Object>> selectQingXieWY(@Param("sensorNo") String sensorNo, @Param("start") int start, @Param("limit") int limit);

    List<Map<String, Object>> selectQingXieWZ(@Param("sensorNo") String sensorNo, @Param("start") int start, @Param("limit") int limit);

    List<Map<String, Object>> selectQingXieX(@Param("sensorNo") String sensorNo, @Param("start") int start, @Param("limit") int limit);

    List<Map<String, Object>> selectQingXieY(@Param("sensorNo") String sensorNo, @Param("start") int start, @Param("limit") int limit);

    List<Map<String, Object>> selectQingXieZ(@Param("sensorNo") String sensorNo, @Param("start") int start, @Param("limit") int limit);

    List<String> selectQingXieT(@Param("sensorNo") String sensorNo, @Param("start") int start, @Param("limit") int limit);

    List<Map<String, Object>> selectLX(@Param("sensorNo") String sensorNo, @Param("start") int start, @Param("limit") int limit);

    String selectLastRecordTime(@Param("sensorNo") String sensorNo, @Param("dsId") String dsId);

    List<Map<String, Object>> selectQingXieXByTime(@Param("sensorNo") String sensorNo, @Param("lastRecordTime") String lastRecordTime);

    List<Map<String, Object>> selectQingXieYByTime(@Param("sensorNo") String sensorNo, @Param("lastRecordTime") String lastRecordTime);

    List<Map<String, Object>> selectQingXieZByTime(@Param("sensorNo") String sensorNo, @Param("lastRecordTime") String lastRecordTime);

    List<Map<String, Object>> selectQingXieWXByTime(@Param("sensorNo") String sensorNo, @Param("lastRecordTime") String lastRecordTime);

    List<Map<String, Object>> selectQingXieWYByTime(@Param("sensorNo") String sensorNo, @Param("lastRecordTime") String lastRecordTime);

    List<Map<String, Object>> selectQingXieWZByTime(@Param("sensorNo") String sensorNo, @Param("lastRecordTime") String lastRecordTime);

    void insertLastRecordTime(@Param("sensorNo") String sensorNo,@Param("dsId") String dsId,@Param("lRTime") String lRTime);

    int selectIsLastRecordTime(@Param("sensorNo") String sensorNo,@Param("dsId") String dsId);

    void updateLastRecordTime(@Param("sensorNo") String sensorNo,@Param("dsId") String dsId,@Param("lRTime") String lRTime);
}
