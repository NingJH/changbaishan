package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TabBasicScope;
import com.cqndt.disaster.device.domain.TabBasicScopeWithBLOBs;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TabBasicScopeMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic_scope
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic_scope
     *
     * @mbg.generated
     */
    int insert(TabBasicScopeWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic_scope
     *
     * @mbg.generated
     */
    int insertSelective(TabBasicScopeWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic_scope
     *
     * @mbg.generated
     */
    TabBasicScopeWithBLOBs selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic_scope
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TabBasicScopeWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic_scope
     *
     * @mbg.generated
     */
    int updateByPrimaryKeyWithBLOBs(TabBasicScopeWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic_scope
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TabBasicScope record);

    List<TabBasicScope> selectByPhotoId(String photoId);
}