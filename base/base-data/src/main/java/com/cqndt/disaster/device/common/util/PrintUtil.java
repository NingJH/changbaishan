package com.cqndt.disaster.device.common.util;

import com.alibaba.fastjson.JSON;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * Created By marc
 * Date: 2019/3/18  10:17
 * Description:输出前端
 */
public class PrintUtil {

    public static void printJson(HttpServletRequest request, HttpServletResponse response, Integer code, String msg) {
        Result result = new Result(code,msg);
        String content = JSON.toJSONString(result);
        printContent(request,response, content);
    }
    private static void printContent(HttpServletRequest request, HttpServletResponse response, String content) {
        try {
            response.reset();
            response.setContentType("application/json");
            String origin = request.getHeader("Origin");
            response.setHeader("Access-Control-Allow-Origin", origin);
            response.setHeader("Access-Control-Allow-Methods", "*");
            response.setHeader("Access-Control-Allow-Headers","Origin,Content-Type,Accept,token,X-Requested-With");
            response.setHeader("Access-Control-Allow-Credentials", "true");
            response.setHeader("Cache-Control", "no-store");
            response.setCharacterEncoding("UTF-8");
            PrintWriter pw = response.getWriter();
            pw.write(content);
            pw.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
