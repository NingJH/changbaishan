package com.cqndt.disaster.device.domain;

import lombok.Data;

/**
 * Create Njh
 *
 * @Time : 2019-11-17 12:00
 **/
@Data
public class ExcelSensor {
    //传感器表tab_sensor
    private String sensorType;//传感器类型
    private String sensorName;//传感器名称
    private String sensorNo;//传感器编号
    private String deviceNo;//传感器表设备编号
}
