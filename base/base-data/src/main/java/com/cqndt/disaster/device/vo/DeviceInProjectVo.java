package com.cqndt.disaster.device.vo;

import lombok.Data;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/4/17  15:30
 * Description:项目下对应的设备
 */
@Data
public class DeviceInProjectVo {
    /**
     * 设备类型
     */
    private Integer deviceType;
    /**
     * 设备类型名称
     */
    private String deviceTypeName;
    /**
     * 同一项目下的设备(含设备编号、监测点编号)
     */
    private List<DeviceMonitorVo> listNo;
}
