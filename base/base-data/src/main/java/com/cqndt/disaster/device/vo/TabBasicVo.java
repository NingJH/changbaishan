package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.TabBasicWithBLOBs;
import lombok.Data;

/**
 * Created By marc
 * Date: 2019/4/15  10:04
 * Description:
 */
@Data
public class TabBasicVo extends TabBasicWithBLOBs {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;

    /**
     * 区域等级
     */
    private String level;
    /**
     * 区域编码
     */
    private String areaCode;
    /**
     * 区域名称
     */
    private String areaName;
    /**
     * 查询条件(前台json组装)
     */
    private String sqlStr;

    private String getAreaCodeSplit(){
        if(null != getAreaCode()){
            return getAreaCode().substring(0,2);
        }
        return null;
    }

}
