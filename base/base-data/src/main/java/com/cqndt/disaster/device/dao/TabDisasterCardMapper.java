package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TabDisasterCard;
import com.cqndt.disaster.device.domain.TabDisasterCardWithBLOBs;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TabDisasterCardMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_disaster_card
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_disaster_card
     *
     * @mbg.generated
     */
    int insert(TabDisasterCardWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_disaster_card
     *
     * @mbg.generated
     */
    int insertSelective(TabDisasterCardWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_disaster_card
     *
     * @mbg.generated
     */
    TabDisasterCardWithBLOBs selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_disaster_card
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TabDisasterCardWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_disaster_card
     *
     * @mbg.generated
     */
    int updateByPrimaryKeyWithBLOBs(TabDisasterCardWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_disaster_card
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TabDisasterCard record);
    /**
     * 根据灾害点编号查询防灾明白卡
     * @param disNo
     * @return
     */
    List<TabDisasterCard> selectByDisNo(String disNo);
}