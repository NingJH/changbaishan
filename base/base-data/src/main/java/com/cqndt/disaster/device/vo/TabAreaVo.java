package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.TabArea;
import lombok.Data;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/5/12  17:30
 * Description:
 */
@Data
public class TabAreaVo extends TabArea {
    private List<TabAreaVo> areaList;
}
