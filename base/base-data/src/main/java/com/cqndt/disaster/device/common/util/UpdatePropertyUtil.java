package com.cqndt.disaster.device.common.util;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public class UpdatePropertyUtil {


    /**
     *
     * @param path 文件路径
     * @param str key
     * @return void
     */
    public static Map<String,String> getValue(String path,String str) throws IOException {
        Properties properties = new Properties();
        InputStream inputStream = new BufferedInputStream(new FileInputStream(path));
        properties.load(inputStream);
        Map<String ,String> map = new HashMap<>();
        map.put(str,properties.getProperty(str));
        return map;
    }


    /**
     *
     * @param path 文件路径
     * @param value value
     * @param str key
     */
    public static void setValue(String path, String value,String str) throws IOException {
        Properties properties = new Properties();
        InputStream inputStream = new BufferedInputStream(new FileInputStream(path));
        properties.load(inputStream);
        properties.setProperty(str,value);
        OutputStream outputStream = new FileOutputStream(path);
        properties.store(outputStream,"当前偏移量");
    }
}
