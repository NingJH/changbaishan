package com.cqndt.disaster.device.domain;

import java.math.BigDecimal;
import java.util.Date;

public class TbsYingbian {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_yingbian.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_yingbian.sensor_no
     *
     * @mbg.generated
     */
    private String sensorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_yingbian.monitor_no
     *
     * @mbg.generated
     */
    private String monitorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_yingbian.value
     *
     * @mbg.generated
     */
    private BigDecimal value;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_yingbian.time
     *
     * @mbg.generated
     */
    private Date time;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_yingbian.id
     *
     * @return the value of tbs_yingbian.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_yingbian.id
     *
     * @param id the value for tbs_yingbian.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_yingbian.sensor_no
     *
     * @return the value of tbs_yingbian.sensor_no
     *
     * @mbg.generated
     */
    public String getSensorNo() {
        return sensorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_yingbian.sensor_no
     *
     * @param sensorNo the value for tbs_yingbian.sensor_no
     *
     * @mbg.generated
     */
    public void setSensorNo(String sensorNo) {
        this.sensorNo = sensorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_yingbian.monitor_no
     *
     * @return the value of tbs_yingbian.monitor_no
     *
     * @mbg.generated
     */
    public String getMonitorNo() {
        return monitorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_yingbian.monitor_no
     *
     * @param monitorNo the value for tbs_yingbian.monitor_no
     *
     * @mbg.generated
     */
    public void setMonitorNo(String monitorNo) {
        this.monitorNo = monitorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_yingbian.value
     *
     * @return the value of tbs_yingbian.value
     *
     * @mbg.generated
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_yingbian.value
     *
     * @param value the value for tbs_yingbian.value
     *
     * @mbg.generated
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_yingbian.time
     *
     * @return the value of tbs_yingbian.time
     *
     * @mbg.generated
     */
    public Date getTime() {
        return time;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_yingbian.time
     *
     * @param time the value for tbs_yingbian.time
     *
     * @mbg.generated
     */
    public void setTime(Date time) {
        this.time = time;
    }
}