package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.WarnPeople;
import com.cqndt.disaster.device.domain.WarnPeopleExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WarnPeopleMapper {
    int countByExample(WarnPeopleExample example);

    int deleteByExample(WarnPeopleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WarnPeople record);

    int insertSelective(WarnPeople record);

    List<WarnPeople> selectByExample(WarnPeopleExample example);
    List<WarnPeople> selectAll(@Param("sensorNo") String sensorNo);

    WarnPeople selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WarnPeople record, @Param("example") WarnPeopleExample example);

    int updateByExample(@Param("record") WarnPeople record, @Param("example") WarnPeopleExample example);

    int updateByPrimaryKeySelective(WarnPeople record);

    int updateByPrimaryKey(WarnPeople record);
}