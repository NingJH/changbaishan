package com.cqndt.disaster.device.common.util;

import com.iotplatform.client.NorthApiClient;
import com.iotplatform.client.NorthApiException;
import com.iotplatform.client.dto.ClientInfo;
import com.iotplatform.utils.PropertyUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AuthUtil {

    private static NorthApiClient northApiClient = null;

    public static NorthApiClient initApiClient() {
        if (northApiClient != null) {
            return northApiClient;
        }
        northApiClient = new NorthApiClient();
        Properties p = new Properties();
        InputStream keycert = AuthUtil.class.getClassLoader().getResourceAsStream("tianyi/tianyi.properties");
        try {
            p.load(keycert);
            ClientInfo clientInfo = new ClientInfo();
            clientInfo.setPlatformIp(p.getProperty("platformIp").trim());
            clientInfo.setPlatformPort(p.getProperty("platformPort").trim());
            clientInfo.setAppId(p.getProperty("appId").trim());
            clientInfo.setSecret(p.getProperty("secret").trim());
//        clientInfo.setSecret(getAesPropertyValue("secret"));
            northApiClient.setClientInfo(clientInfo);
            northApiClient.initSSLConfig();
        } catch (NorthApiException e) {
            System.out.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (keycert!=null){
                try {
                    keycert.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return northApiClient;
    }

    public static String getAesPropertyValue(String propertyName) {
        String aesPwd = "123987"; //this is a test AES password

//      String originalProperty = "gPnTWO52yrobtjyobykkf12P8f4a";
//      byte[] temp = AesUtil.encrypt(originalProperty, aesPwd);
//      String hexStrResult = HexParser.parseByte2HexStr(temp);
//      System.out.println("encrypted secret hex sting is ："  + hexStrResult);

//        PropertyUtil.init("./src/main/resources/application.properties");
//        System.out.println(PropertyUtil.getProperty(propertyName));
//        byte[] secret = HexParser.parseHexStr2Byte(PropertyUtil.getProperty(propertyName));
//        return new String(AesUtil.decrypt(secret, aesPwd));
        return null;
    }
}
