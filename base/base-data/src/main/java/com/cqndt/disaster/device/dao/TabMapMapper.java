package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TabMap;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TabMapMapper {

    List<TabMap> getAllMap();
}
