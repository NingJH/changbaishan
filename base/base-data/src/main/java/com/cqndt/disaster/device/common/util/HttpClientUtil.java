package com.cqndt.disaster.device.common.util;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.CharArrayBuffer;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/10  14:16
 * Description:http传输连接
 */
public class HttpClientUtil {

    /**
     * post请求
     *
     * @param str
     * @return
     */
    public static Map<String, Object> post(String clientUrl, String str, String token) {
        Map<String, Object> map = new HashMap<>(16);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost post = new HttpPost(clientUrl);
        CloseableHttpResponse response = null;
        try {
            post.setHeader("Content-Type", "application/json; charset=utf-8");
            if (!"".equals(token)) {
                post.setHeader("token", token);

            }
            post.setEntity(new ByteArrayEntity(str.getBytes("UTF-8")));
            response = httpClient.execute(post);
            if (response != null && response.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                map = changResult(entity);
            }
            return map;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    /**
     * post请求
     *
     * @param str
     * @return
     */
    public static Map<String, Object> post(String clientUrl, String str) {
        Map<String,Object> map = new HashMap<>(16);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost post = new HttpPost(clientUrl);
        CloseableHttpResponse response = null;
        try {
            post.setHeader("Content-Type", "application/json; charset=utf-8");
            post.setEntity(new ByteArrayEntity(str.getBytes("UTF-8")));
            response = httpClient.execute(post);
            if (response != null && response.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                map = changResult(entity);
            }
            return map;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    /**
     * post请求
     *
     * @return
     */
    public static Map<String, Object> post(String clientUrl) {
        Map<String,Object> map = new HashMap<>(16);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost post = new HttpPost(clientUrl);
        CloseableHttpResponse response = null;
        try {
            response = httpClient.execute(post);
            if (response != null && response.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                map = changResult(entity);
            }
            return map;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    public static Map<String, Object> get(String clientUrl,String accessToken) {
        Map<String,Object> map = new HashMap<>(16);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet get = new HttpGet(clientUrl);
        CloseableHttpResponse response = null;

        try {
            get.setHeader("Authorization", accessToken);
            get.setHeader("Content-Type", "application/json; charset=utf-8");
            response = httpClient.execute(get);
            if (response != null && response.getStatusLine().getStatusCode() == 200) {
                HttpEntity entity = response.getEntity();
                map = changResult(entity);
            }
            return map;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    private static Map<String, Object> changResult(HttpEntity entity) throws IOException {
        String result = null;
        if (entity != null) {
            long lenth = entity.getContentLength();
            if (lenth != -1 && lenth < 2048) {
                result = EntityUtils.toString(entity, "UTF-8");
            } else {
                InputStreamReader reader1 = new InputStreamReader(entity.getContent(), "UTF-8");
                CharArrayBuffer buffer = new CharArrayBuffer(2048);
                char[] tmp = new char[1024];
                int l;
                while ((l = reader1.read(tmp)) != -1) {
                    buffer.append(tmp, 0, l);
                }
                result = buffer.toString();
            }
            Map<String, Object> map = JSONObject.parseObject(result, Map.class);
            return map;
        }
        return null;
    }
}
