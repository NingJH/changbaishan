package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.TabOrderLog;
import lombok.Data;

/**
 * Created By marc
 * Date: 2019/5/8  10:42
 * Description:
 */
@Data
public class TabOrderLogVo extends TabOrderLog {

    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 每页条数
     */
    private Integer limit=10;
    /**
     * 指令播报师傅成功(0失败 1成功)
     */
    private String stateName;

    private String platTypeName;

}
