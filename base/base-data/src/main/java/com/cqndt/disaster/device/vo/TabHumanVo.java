package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.TabHuman;
import lombok.Data;

/**
 * Created By marc
 * Date: 2019/4/25  14:47
 * Description:
 */
@Data
public class TabHumanVo extends TabHuman {

    private String workTypeName;
}
