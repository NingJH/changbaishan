package com.cqndt.disaster.device.domain;

import java.math.BigDecimal;
import java.util.Date;

public class TbsChenjiang {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_chenjiang.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_chenjiang.sensor_no
     *
     * @mbg.generated
     */
    private String sensorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_chenjiang.monitor_no
     *
     * @mbg.generated
     */
    private String monitorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_chenjiang.x
     *
     * @mbg.generated
     */
    private BigDecimal x;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_chenjiang.y
     *
     * @mbg.generated
     */
    private BigDecimal y;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_chenjiang.z
     *
     * @mbg.generated
     */
    private BigDecimal z;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_chenjiang.temp
     *
     * @mbg.generated
     */
    private BigDecimal temp;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_chenjiang.time
     *
     * @mbg.generated
     */
    private Date time;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_chenjiang.id
     *
     * @return the value of tbs_chenjiang.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_chenjiang.id
     *
     * @param id the value for tbs_chenjiang.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_chenjiang.sensor_no
     *
     * @return the value of tbs_chenjiang.sensor_no
     *
     * @mbg.generated
     */
    public String getSensorNo() {
        return sensorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_chenjiang.sensor_no
     *
     * @param sensorNo the value for tbs_chenjiang.sensor_no
     *
     * @mbg.generated
     */
    public void setSensorNo(String sensorNo) {
        this.sensorNo = sensorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_chenjiang.monitor_no
     *
     * @return the value of tbs_chenjiang.monitor_no
     *
     * @mbg.generated
     */
    public String getMonitorNo() {
        return monitorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_chenjiang.monitor_no
     *
     * @param monitorNo the value for tbs_chenjiang.monitor_no
     *
     * @mbg.generated
     */
    public void setMonitorNo(String monitorNo) {
        this.monitorNo = monitorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_chenjiang.x
     *
     * @return the value of tbs_chenjiang.x
     *
     * @mbg.generated
     */
    public BigDecimal getX() {
        return x;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_chenjiang.x
     *
     * @param x the value for tbs_chenjiang.x
     *
     * @mbg.generated
     */
    public void setX(BigDecimal x) {
        this.x = x;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_chenjiang.y
     *
     * @return the value of tbs_chenjiang.y
     *
     * @mbg.generated
     */
    public BigDecimal getY() {
        return y;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_chenjiang.y
     *
     * @param y the value for tbs_chenjiang.y
     *
     * @mbg.generated
     */
    public void setY(BigDecimal y) {
        this.y = y;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_chenjiang.z
     *
     * @return the value of tbs_chenjiang.z
     *
     * @mbg.generated
     */
    public BigDecimal getZ() {
        return z;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_chenjiang.z
     *
     * @param z the value for tbs_chenjiang.z
     *
     * @mbg.generated
     */
    public void setZ(BigDecimal z) {
        this.z = z;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_chenjiang.temp
     *
     * @return the value of tbs_chenjiang.temp
     *
     * @mbg.generated
     */
    public BigDecimal getTemp() {
        return temp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_chenjiang.temp
     *
     * @param temp the value for tbs_chenjiang.temp
     *
     * @mbg.generated
     */
    public void setTemp(BigDecimal temp) {
        this.temp = temp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_chenjiang.time
     *
     * @return the value of tbs_chenjiang.time
     *
     * @mbg.generated
     */
    public Date getTime() {
        return time;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_chenjiang.time
     *
     * @param time the value for tbs_chenjiang.time
     *
     * @mbg.generated
     */
    public void setTime(Date time) {
        this.time = time;
    }
}