package com.cqndt.disaster.device.domain;

import java.math.BigDecimal;
import java.util.Date;

public class TbsDbbxHp {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_dbbx_hp.id
     *
     * @mbg.generated
     */
    private Integer id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_dbbx_hp.sensor_no
     *
     * @mbg.generated
     */
    private String sensorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_dbbx_hp.monitor_no
     *
     * @mbg.generated
     */
    private String monitorNo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_dbbx_hp.wyl
     *
     * @mbg.generated
     */
    private BigDecimal wyl;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_dbbx_hp.x
     *
     * @mbg.generated
     */
    private BigDecimal x;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_dbbx_hp.y
     *
     * @mbg.generated
     */
    private BigDecimal y;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_dbbx_hp.z
     *
     * @mbg.generated
     */
    private BigDecimal z;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_dbbx_hp.temp
     *
     * @mbg.generated
     */
    private BigDecimal temp;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tbs_dbbx_hp.time
     *
     * @mbg.generated
     */
    private Date time;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_dbbx_hp.id
     *
     * @return the value of tbs_dbbx_hp.id
     *
     * @mbg.generated
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_dbbx_hp.id
     *
     * @param id the value for tbs_dbbx_hp.id
     *
     * @mbg.generated
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_dbbx_hp.sensor_no
     *
     * @return the value of tbs_dbbx_hp.sensor_no
     *
     * @mbg.generated
     */
    public String getSensorNo() {
        return sensorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_dbbx_hp.sensor_no
     *
     * @param sensorNo the value for tbs_dbbx_hp.sensor_no
     *
     * @mbg.generated
     */
    public void setSensorNo(String sensorNo) {
        this.sensorNo = sensorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_dbbx_hp.monitor_no
     *
     * @return the value of tbs_dbbx_hp.monitor_no
     *
     * @mbg.generated
     */
    public String getMonitorNo() {
        return monitorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_dbbx_hp.monitor_no
     *
     * @param monitorNo the value for tbs_dbbx_hp.monitor_no
     *
     * @mbg.generated
     */
    public void setMonitorNo(String monitorNo) {
        this.monitorNo = monitorNo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_dbbx_hp.wyl
     *
     * @return the value of tbs_dbbx_hp.wyl
     *
     * @mbg.generated
     */
    public BigDecimal getWyl() {
        return wyl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_dbbx_hp.wyl
     *
     * @param wyl the value for tbs_dbbx_hp.wyl
     *
     * @mbg.generated
     */
    public void setWyl(BigDecimal wyl) {
        this.wyl = wyl;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_dbbx_hp.x
     *
     * @return the value of tbs_dbbx_hp.x
     *
     * @mbg.generated
     */
    public BigDecimal getX() {
        return x;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_dbbx_hp.x
     *
     * @param x the value for tbs_dbbx_hp.x
     *
     * @mbg.generated
     */
    public void setX(BigDecimal x) {
        this.x = x;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_dbbx_hp.y
     *
     * @return the value of tbs_dbbx_hp.y
     *
     * @mbg.generated
     */
    public BigDecimal getY() {
        return y;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_dbbx_hp.y
     *
     * @param y the value for tbs_dbbx_hp.y
     *
     * @mbg.generated
     */
    public void setY(BigDecimal y) {
        this.y = y;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_dbbx_hp.z
     *
     * @return the value of tbs_dbbx_hp.z
     *
     * @mbg.generated
     */
    public BigDecimal getZ() {
        return z;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_dbbx_hp.z
     *
     * @param z the value for tbs_dbbx_hp.z
     *
     * @mbg.generated
     */
    public void setZ(BigDecimal z) {
        this.z = z;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_dbbx_hp.temp
     *
     * @return the value of tbs_dbbx_hp.temp
     *
     * @mbg.generated
     */
    public BigDecimal getTemp() {
        return temp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_dbbx_hp.temp
     *
     * @param temp the value for tbs_dbbx_hp.temp
     *
     * @mbg.generated
     */
    public void setTemp(BigDecimal temp) {
        this.temp = temp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tbs_dbbx_hp.time
     *
     * @return the value of tbs_dbbx_hp.time
     *
     * @mbg.generated
     */
    public Date getTime() {
        return time;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tbs_dbbx_hp.time
     *
     * @param time the value for tbs_dbbx_hp.time
     *
     * @mbg.generated
     */
    public void setTime(Date time) {
        this.time = time;
    }
}