package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TbsSbwyLx;
import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TbsSbwyLxMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_lx
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_lx
     *
     * @mbg.generated
     */
    int insert(TbsSbwyLx record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_lx
     *
     * @mbg.generated
     */
    int insertSelective(TbsSbwyLx record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_lx
     *
     * @mbg.generated
     */
    TbsSbwyLx selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_lx
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TbsSbwyLx record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbs_sbwy_lx
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TbsSbwyLx record);

    List<Map<String,Object>> tbsSbwyLx(SearchVo vo);

    List<Map<String, Object>> tbsSbwyLxIntervals(String sensorNo);
}