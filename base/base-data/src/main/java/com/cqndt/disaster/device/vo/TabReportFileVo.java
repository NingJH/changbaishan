package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.TabReportingFile;
import lombok.Data;

/**
 * Created By marc
 * Date: 2019/5/11  10:08
 * Description:
 */
@Data
public class TabReportFileVo extends TabReportingFile {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 每页条数
     */
    private Integer limit=10;
    /**
     * 报表类型名称
     */
    private String fileTypeName;

    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 年份
     */
    private String uploadYear;
    /**
     * 月
     */
    private String uploadMonth;
    /**
     * 查询时间
     */
    private String searchTime;

    private String staticType;

    private String userId;
}
