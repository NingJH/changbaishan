package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TbsHwzd;
import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TbsHwzdMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbsHwzd record);

    int insertSelective(TbsHwzd record);

    TbsHwzd selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbsHwzd record);

    int updateByPrimaryKey(TbsHwzd record);

    List<Map<String,Object>> tbsHwzd(SearchVo vo);
}