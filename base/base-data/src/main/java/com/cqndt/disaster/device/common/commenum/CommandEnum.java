package com.cqndt.disaster.device.common.commenum;

import lombok.Getter;

/**
 * @author lhl
 * @Description 设备命令常量类
 * @Date 2019/1/5 14:30
 */
@Getter
public enum  CommandEnum {
    /*地表裂缝监测仪*/
    SET_UP_ANGLE_AB04("04AB","设置角度偏移告警阈值"),
    SET_UP_STAYWIRE_AB06("06AB","设置拉线偏移告警阈值 "),
    SET_UP_UPLOAD_AB0D("0DAB","设置数据上报周期"),
    SET_UP_SAMPLING_AB0F("0FAB","设置采样间隔"),
    /*地面倾斜监测仪*/
    SET_UP_ANGLE_AC04("04AC","设置角度偏移告警阈值"),
    SET_UP_UPLOAD_AC0D("0DAC","设置数据上报周期"),
    SET_UP_SAMPLING_AC0F("0FAC","设置采样间隔"),
    /*智能报警器*/
    SET_UP_SHOUDLIGHT_AD04("04AE","设置智能报警器"),
    /*雨量计*/
    SET_UP_UPLOAD_AE0D("0DAD","设置数据上报周期"),
    SET_UP_SAMPLING_AE0F("0FAD","设置采样间隔"),
    SET_UP_RAINFALL_AE06("06AD","设置雨量计阈值"),

    /*液位计*/
    SET_UP_MUD_AF0D("0DAF","设置数据上报周期"),
    SET_UP_MUD_AF0F("0FAF","设置采样间隔"),
    SET_UP_MUD_AF04("04AF","报警值设置"),

    /*获取版本*/
    SET_UP_GETVERSION_AB0E("0EAB","读取软件版本(地表地裂，墙裂)"),
    SET_UP_GETVERSION_AC0E("0EAC","读取软件版本(地面倾斜)"),
    SET_UP_GETVERSION_AD0E("0EAE","读取软件版本(智能告警器)"),
    SET_UP_GETVERSION_AE0E("0EAD","读取软件版本(雨量)"),
    SET_UP_GETVERSION_AF0E("0EAF","读取软件版本(液位)"),

    /*升级*/
    SET_UP_UPGRADE_F3("F3","服务器向设备发送升级数据"),
    SET_UP_VERSION_F1("F1","服务器向设备发送最新软件版本"),
    ;

    private String code;
    private String message;

    CommandEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
