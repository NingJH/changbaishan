package com.cqndt.disaster.device.domain;

import lombok.Data;

import java.util.Date;

/**
 * Create Njh
 * @Time : 2019-11-17 11:59
 **/
@Data
public class ExcelDeviceInstall {
    //安装信息表tab_device_install
    private String anwz;//安装位置
    private String sgtj;//施工条件（1便利2较便利3不便利）
    private String gdfs;//供电方式（1.220v交流供电 2.太阳能供电）
    private String gdsm;//供电说明
    private char yxtj;//岩性条件（1.黄土2.砂卵石3.基岩4屋顶）
    private char cgtj;//采光条件（1.充足2.较充足3.不充足）
    private char ydxh;//移动信号（1强2中3弱）
    private char gprsXh;//gprs信号（1强2中3差4无）
    private char dx3g;//电信3g（1强2中3差4无）
    private char slqk;//水流情况（1强2中3弱）
    private Date sgrq;//施工日期
    private Date jgrq;//竣工日期
    private String sgry;//施工人员
    private String jcry;//检查人员
    private String ysry;//验收人员
    private char wgjc;//外观检查（1正常2损坏3水浸4污染）
    private String lgaz;//立杆安装（1成功2不成功）
    private String tyngd;//太阳能供电（1成功2不成功）
    private String sjcj;//数据采集（1成功2不成功）
    private String sjcs;//数据传输（1成功2不成功）
    private String azdw;//安装单位
    private String remark;//备注
    private String deviceNo;//设备编号
}
