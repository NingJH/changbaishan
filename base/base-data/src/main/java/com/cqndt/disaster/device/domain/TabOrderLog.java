package com.cqndt.disaster.device.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TabOrderLog {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.id
     *
     * @mbg.generated
     */
    private Long id;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.username
     *
     * @mbg.generated
     */
    private String username;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.operation
     *
     * @mbg.generated
     */
    private String operation;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.method
     *
     * @mbg.generated
     */
    private String method;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.params
     *
     * @mbg.generated
     */
    private String params;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.time
     *
     * @mbg.generated
     */
    private Long time;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.ip
     *
     * @mbg.generated
     */
    private String ip;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.create_date
     *
     * @mbg.generated
     */
    private Date createDate;

    private Integer platType;

    private Integer voiceType;

    public Integer getVoiceType() {
        return voiceType;
    }

    public void setVoiceType(Integer voiceType) {
        this.voiceType = voiceType;
    }

    public Integer getPlatType() {
        return platType;
    }

    public void setPlatType(Integer platType) {
        this.platType = platType;
    }

    public String getCreateDateStr() {
        if(null == getCreateDate()){
            return null;
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(getCreateDate());
        return dateString;
    }
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.state
     *
     * @mbg.generated
     */
    private Integer state;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.sn
     *
     * @mbg.generated
     */
    private String sn;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.device_id
     *
     * @mbg.generated
     */
    private String deviceId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column tab_order_log.voice_type_name
     *
     * @mbg.generated
     */
    private String voiceTypeName;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.id
     *
     * @return the value of tab_order_log.id
     *
     * @mbg.generated
     */
    public Long getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.id
     *
     * @param id the value for tab_order_log.id
     *
     * @mbg.generated
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.username
     *
     * @return the value of tab_order_log.username
     *
     * @mbg.generated
     */
    public String getUsername() {
        return username;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.username
     *
     * @param username the value for tab_order_log.username
     *
     * @mbg.generated
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.operation
     *
     * @return the value of tab_order_log.operation
     *
     * @mbg.generated
     */
    public String getOperation() {
        return operation;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.operation
     *
     * @param operation the value for tab_order_log.operation
     *
     * @mbg.generated
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.method
     *
     * @return the value of tab_order_log.method
     *
     * @mbg.generated
     */
    public String getMethod() {
        return method;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.method
     *
     * @param method the value for tab_order_log.method
     *
     * @mbg.generated
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.params
     *
     * @return the value of tab_order_log.params
     *
     * @mbg.generated
     */
    public String getParams() {
        return params;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.params
     *
     * @param params the value for tab_order_log.params
     *
     * @mbg.generated
     */
    public void setParams(String params) {
        this.params = params;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.time
     *
     * @return the value of tab_order_log.time
     *
     * @mbg.generated
     */
    public Long getTime() {
        return time;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.time
     *
     * @param time the value for tab_order_log.time
     *
     * @mbg.generated
     */
    public void setTime(Long time) {
        this.time = time;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.ip
     *
     * @return the value of tab_order_log.ip
     *
     * @mbg.generated
     */
    public String getIp() {
        return ip;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.ip
     *
     * @param ip the value for tab_order_log.ip
     *
     * @mbg.generated
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.create_date
     *
     * @return the value of tab_order_log.create_date
     *
     * @mbg.generated
     */
    public Date getCreateDate() {
        return createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.create_date
     *
     * @param createDate the value for tab_order_log.create_date
     *
     * @mbg.generated
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.state
     *
     * @return the value of tab_order_log.state
     *
     * @mbg.generated
     */
    public Integer getState() {
        return state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.state
     *
     * @param state the value for tab_order_log.state
     *
     * @mbg.generated
     */
    public void setState(Integer state) {
        this.state = state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.sn
     *
     * @return the value of tab_order_log.sn
     *
     * @mbg.generated
     */
    public String getSn() {
        return sn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.sn
     *
     * @param sn the value for tab_order_log.sn
     *
     * @mbg.generated
     */
    public void setSn(String sn) {
        this.sn = sn;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.device_id
     *
     * @return the value of tab_order_log.device_id
     *
     * @mbg.generated
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.device_id
     *
     * @param deviceId the value for tab_order_log.device_id
     *
     * @mbg.generated
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column tab_order_log.voice_type_name
     *
     * @return the value of tab_order_log.voice_type_name
     *
     * @mbg.generated
     */
    public String getVoiceTypeName() {
        return voiceTypeName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column tab_order_log.voice_type_name
     *
     * @param voiceTypeName the value for tab_order_log.voice_type_name
     *
     * @mbg.generated
     */
    public void setVoiceTypeName(String voiceTypeName) {
        this.voiceTypeName = voiceTypeName;
    }
}