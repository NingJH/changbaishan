package com.cqndt.disaster.device.dao.app;

import com.cqndt.disaster.device.domain.TabAttachment;
import com.cqndt.disaster.device.domain.TabDevice;
import com.cqndt.disaster.device.domain.TabDeviceCheck;
import com.cqndt.disaster.device.vo.TabDeviceCheckVo;
import com.cqndt.disaster.device.vo.TabDeviceVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface AppTabDeviceCheckMapper {

    /**
     * 通过项目id查询设备下拉列表
     * @param projectNo
     * @return
     */
    List<Map<String,String>> listForDevice(@Param("projectNo")String projectNo, @Param("deviceName")String deviceName);

    /**
     * 新增
     * @param tabDeviceCheck
     * @return
     */
    int save(TabDeviceCheck tabDeviceCheck);

    /**
     * 新增图片和视频
     * @param record
     * @return
     */
    int insertTabAttachmentSelective(TabAttachment record);

    /**
     * 查询巡查记录详情
     * @param tabDeviceCheckVo
     * @return
     */
    List<TabDeviceCheckVo> listForDeviceCheck(TabDeviceCheckVo tabDeviceCheckVo);


}
