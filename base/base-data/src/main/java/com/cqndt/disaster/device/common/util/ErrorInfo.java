package com.cqndt.disaster.device.common.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * user: zhaojianji
 * date: 2017/05/28 15:21
 * desc:  描述用途
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorInfo {

    private String filedName;
    private String errMsg;
    
}
