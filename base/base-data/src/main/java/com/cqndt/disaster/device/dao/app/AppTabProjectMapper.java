package com.cqndt.disaster.device.dao.app;

import com.cqndt.disaster.device.vo.AppTabDevice;
import com.cqndt.disaster.device.vo.Monitor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AppTabProjectMapper {

    /**
     * 根据条件查询项目信息
     */
    List<Map<String,Object>> selectByCondition(@Param("userId") String userId, @Param("level") String level, @Param("areaCode") String areaCode,
                                               @Param("id") String id, @Param("search") String search,@Param("areaCodeSplit") String areaCodeSplit);
    Map<String, Object> selectByIdDetails(@Param("projectId")Integer projectId,@Param("userId") String userId);

    List<Map<String,Object>> selectAllPush(@Param("userId") String userId);

    List<Map<String,Object>> selectAttachmentIds(String[] ids);

    List<Map<String,Object>> getByProjectId(@Param("projectId")Integer projectId,@Param("search")String search,@Param("monitorType")Integer monitorType,@Param("offlineTime")Integer offlineTime);

    Integer countDeviceZxForProject(@Param("projectId")Integer projectId,@Param("offlineTime")Integer offlineTime);

    Monitor getMonitorDataById(@Param("monitorId")Integer monitorId);

    AppTabDevice selectDeviceById(@Param("deviceId")Integer deviceId,@Param("offlineTime")Integer offlineTime);

    List<Map<String, Object>> selectProjectDatum(@Param("projectId")Integer projectId);

    List<Map<String, Object>> selectForProjectId(String[] ids);
}