package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.TabAlarmInfo;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created By marc
 * Date: 2019/4/22  11:44
 * Description:
 */
@Data
public class TabAlarmInfoVo extends TabAlarmInfo {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;
    /**
     * 区域等级
     */
    private String areaLevel;
    /**
     * 区域编码
     */
    private String areaCode;
    /**
     * 区域名称
     */
    private String areaName;
    /**
     * 监测点编号
     */
    private String monitorNo;
    /**
     * 监测名称
     */
    private String monitorName;
    /**
     * 项目id
     */
    private String projectId;
    /**
     * 项目编号
     */
    private String projectNo;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 设备id
     */
    private String deviceId;
    /**
     * 设备编号
     */
    private String deviceNo;
    /**
     * 设备名称
     */
    private String deviceName;
    /**
     * 设备经度
     */
    private BigDecimal longitude;
    /**
     * 设备纬度
     */
    private BigDecimal latitude;
    /**
     * 设备类型
     */
    private String deviceType;
    /**
     * 告警等级名称
     */
    private String levelName;
    /**
     * 当前用户id
     */
    private Integer userId;
    /**
     * 监测点id
     */
    private String monitorId;

    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 告警类型名称
     */
    private String warnTypeName;
    /**
     * 告警等级
     */
    private String warmLevel;

    private String getAreaCodeSplit(){
        if(null != getAreaCode()){
            return getAreaCode().substring(0,2);
        }
        return null;
    }

}
