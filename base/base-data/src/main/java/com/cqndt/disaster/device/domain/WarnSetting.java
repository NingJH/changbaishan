package com.cqndt.disaster.device.domain;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class WarnSetting {
    private Integer id;

    private Integer hours;

    private Integer typeValue;

    private BigDecimal initValue;

    private BigDecimal red;

    private BigDecimal orange;

    private BigDecimal yellow;

    private BigDecimal avgValue;

    private BigDecimal blue;

    private String unit;

    private String deviceNo;

    private String sensorNo;
    private String msgModel;

}