package com.cqndt.disaster.device.dao.app;

import com.cqndt.disaster.device.vo.TabUserVo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AppTabUserMapper {
    TabUserVo getUserByUserName(TabUserVo record);
}