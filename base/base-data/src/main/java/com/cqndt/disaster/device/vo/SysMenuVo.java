package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.SysMenuEntity;
import lombok.Data;

/**
 * Created By marc
 * Date: 2019/3/6  10:03
 * Description:
 */
@Data
public class SysMenuVo extends SysMenuEntity {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;

    private String labelName1;

    private String id1;

    private String roleName1;

    private String name4;

    private String staticNum;
}
