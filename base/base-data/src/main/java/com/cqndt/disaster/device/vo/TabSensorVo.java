package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.TabSensor;
import lombok.Data;

import java.util.Date;

/**
 * Created By marc
 * Date: 2019/4/11  16:13
 * Description:
 */
@Data
public class TabSensorVo extends TabSensor {
    /**
     * 开始时间
     */
    private Date startTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 设备名称
     */
    private String deviceName;
}
