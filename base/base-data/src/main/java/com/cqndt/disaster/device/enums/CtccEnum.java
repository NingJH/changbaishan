package com.cqndt.disaster.device.enums;

import lombok.Getter;

/**
 * @Description:
 * @Auther: lhl
 * @Date: 2019/4/30 10:48
 */
@Getter
public enum  CtccEnum {
    //new
    SHOCK_TIME("6666_0_0001","震动持续时间"),
    SHOCK_NUM("6666_0_0002","震动次数"),
    SHOCK_DIRECTION("6666_0_0003","震动方向"),
    ELECTRIIC("6666_0_0004","电量"),
    HARDWARE("6666_0_0005","硬件版本"),

    //like
    DIPANGLE_X("3345_0_5702","倾角x"),
    DIPANGLE_Y("3345_0_5703","倾角y"),
    DIPANGLE_Z("3345_0_5704","倾角z"),

    PULLLINE("3330_0_5700","拉线值"),
    VOLTAGE("3316_0_5700","电压"),

    LONGITUDE("3336_0_5514","经度"),
    LATITUDE("3336_0_5513","纬度"),

    ANGLE_WARN("3330_0_5750","角度偏移告警阈值"),

    SIGNAL("4_0_2","信号强度"),

    SOFTWARE("3_0_19","软件版本"),

    REPORING_CYCLE("3340_0_5538","静止上报周期"),

    SAMPLING_CYCLE("3340_0_5521","采样时长"),

    DEVICE_TYPE("3_0_17","设备类型"),


    ;

    /**
     * 编码
     */
    private String code;

    /**
     * 解释
     */
    private String msg;

    CtccEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
