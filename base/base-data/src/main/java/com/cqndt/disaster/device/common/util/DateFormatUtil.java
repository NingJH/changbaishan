package com.cqndt.disaster.device.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/5/13  16:33
 * Description:
 */
public class DateFormatUtil {
    /**
     * 获取当前时间前n天的时间
     */
    public static Map<String,Object> getPreToday(int num){
        Map<String,Object> map = new HashMap<>();
        map.put("end_time",new SimpleDateFormat("yyyy-MM-dd").format( new Date())+" 23:59:59");
        Calendar calendar2 = Calendar.getInstance();
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        calendar2.add(Calendar.DATE, num);
        String numDays = sdf2.format(calendar2.getTime())+" 00:00:00";
        map.put("start_time",numDays);
        return map;
    }
    /**
     * 获取当前时间后n天的时间
     */
    public static Map<String,Object> getNextToday(int num){
        Map<String,Object> map = new HashMap<>();
        map.put("start_time",new SimpleDateFormat("yyyy-MM-dd").format( new Date())+" 00:00:00");
        Calendar calendar2 = Calendar.getInstance();
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        calendar2.add(Calendar.DATE, num);
        String numDays = sdf2.format(calendar2.getTime())+" 23:59:59";
        map.put("end_time",numDays);
        return map;
    }
    /**
     *  获取当天
     * @return
     */
    public static Map<String,Object> getToday(){
        Map<String,Object> map = new HashMap<>();
        map.put("start_time",new SimpleDateFormat("yyyy-MM-dd").format( new Date())+" 00:00:00");
        map.put("end_time",new SimpleDateFormat("yyyy-MM-dd").format( new Date())+" 23:59:59");
        return map;
    }

    /**
     *  获取当前时间的周一和周日的日期
     * @return
     */
    public static Map<String,Object> getMonSumTime(){
        Calendar c1=Calendar.getInstance();
        c1.setTime(new Date());
        c1.set( Calendar.DAY_OF_WEEK,Calendar.MONDAY );
        SimpleDateFormat sdf=new SimpleDateFormat( "yyyy-MM-dd" );
        Map<String,Object> map=new HashMap<String, Object>();
        map.put("mon",sdf.format( c1.getTime())+" 00:00:00");
        c1.roll( Calendar.DAY_OF_WEEK,7 );
        int day_of_week = c1.get(Calendar.DAY_OF_WEEK) - 1;
        if (day_of_week == 0)
            day_of_week = 7;
        c1.add(Calendar.DATE, -day_of_week + 7);
        map.put("sun",sdf.format( c1.getTime())+" 23:59:59");
        return map;
    }

    public static void main(String[] args) {
        System.out.println(getPreThreeMonth(-2));
    }

    /**
     * 获取当月的月初时间和月末时间
     * @return
     */
    public static Map<String,Object> getMonthFirstAndEndDay(){
        Calendar c1=Calendar.getInstance();
        c1.setTime(new Date());
        c1.set(Calendar.DAY_OF_MONTH,1);
        Map<String,Object> map=new HashMap<String,Object>();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        map.put( "first",sdf.format( c1.getTime() )+" 00:00:00");
        c1.roll( Calendar.DAY_OF_MONTH,-1 );
        map.put( "end",sdf.format( c1.getTime() )+" 23:59:59");
        return map;
    }
    /**
     * 获取当月至前3个月
     * @return
     */
    public static Map<String,Object> getPreThreeMonth(int num){
        Calendar c1=Calendar.getInstance();
        c1.setTime(new Date());
        //c1.set(Calendar.DAY_OF_MONTH,c1.getActualMaximum(Calendar.DAY_OF_MONTH));
        Map<String,Object> map=new HashMap<String,Object>();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        map.put( "end",sdf.format( c1.getTime() )+" 23:59:59");
        c1.roll( Calendar.MONTH,num);
        c1.set(Calendar.DAY_OF_MONTH,1);
        map.put( "first",sdf.format( c1.getTime() )+" 00:00:00");
        return map;
    }

    /**
     * 时间字符串转时间戳
     * @param s
     * @return
     * @throws ParseException
     */
    public static String dateToStamp(String s){
        try {
            String res;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = simpleDateFormat.parse(s);
            long ts = date.getTime();
            res = String.valueOf(ts);
            return res;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 时间戳转时间字符串
     * @param s
     * @return
     */
    public static String stampToDate(String s){
        try {
            String res;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long lt = new Long(s);
            Date date = new Date(lt);
            res = simpleDateFormat.format(date);
            return res;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
