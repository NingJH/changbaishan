package com.cqndt.disaster.device.vo;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @Description: 数据表实体类
 * @Auther: lhl
 * @Date: 2019/4/23 12:11
 */
@Getter
@Setter
@ToString
public class Data {
    private String id;
    private Date at;
    private String imei;
    private Integer type;
    private String dsId;
    private String value;
    private String devId;

}
