package com.cqndt.disaster.device.dao.app;

import com.cqndt.disaster.device.domain.TabStaticType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AppTabStaticTypeMapper {

    List<Map<String,Object>> selectByStaticNum(String staticNum);

    Map<String,Object> selectByStaticNumAndValue(@Param("staticNum") String staticNum,@Param("staticKeyVal")String staticKeyVal);
}