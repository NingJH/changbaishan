package com.cqndt.disaster.device.domain;

import lombok.Data;

/**
 * Create Njh
 * @Time : 2019-11-17 15:16
 **/
@Data
public class ExcelBasic {
    //灾害点表tab_basic
    private String disNo;//灾害点编号
    private String disName;//灾害点名称
    private String disType;//灾害类型
    private String disasterName;//灾害类型对应的中文名称
    private String disasterLevel;//险情等级(A特大型B大型C中型D小型),
}
