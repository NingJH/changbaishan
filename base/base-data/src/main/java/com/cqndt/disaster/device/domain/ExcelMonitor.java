package com.cqndt.disaster.device.domain;

import lombok.Data;

import java.util.Date;

/**
 * Create Njh
 * @Time : 2019-11-17 11:58
 **/
@Data
public class ExcelMonitor {
    //监测点表tab_monitor
    private String monitorNo;//监测点编号
    private String monitorName;//监测点名称
    private String position;//监测点位置
    private String monitorLon;//监测点经纬度
    private String monitorLat;//监测点经纬度
    private Date createTime;//添加时间

    private Integer projectId;//项目id
    private String projetcNo;//项目编号
    private Integer deviceId;//设备id
    private String deviceNo;//设备编号
}
