package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TabBasic;
import com.cqndt.disaster.device.domain.TabBasicWithBLOBs;
import com.cqndt.disaster.device.vo.TabAlarmInfoVo;
import com.cqndt.disaster.device.vo.TabBasicVo;
import com.cqndt.disaster.device.vo.TabDeviceVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface TabBasicMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic
     *
     * @mbg.generated
     */
    int insert(TabBasicWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic
     *
     * @mbg.generated
     */
    int insertSelective(TabBasicWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic
     *
     * @mbg.generated
     */
    TabBasicWithBLOBs selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TabBasicWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic
     *
     * @mbg.generated
     */
    int updateByPrimaryKeyWithBLOBs(TabBasicWithBLOBs record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_basic
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TabBasic record);

    /**
     * 根据区域统计灾害点数量
     * @param areaCode
     * @param level
     * @return
     */
    Integer countDisasterNumber(@Param("areaCode") String areaCode, @Param("level") String level,@Param("areaCodeSplit") String areaCodeSplit);

    /**
     * 根据项目关联统计灾害点数量
     * @param userId
     * @return
     */
    Integer countDisasterNumberByUserId(@Param("userId") String userId);
    /**
     * 根据区域统计灾害点类型数量
     * @return
     */
    List<Map<String,Object>> countDisasterTypeNumber(@Param("userId") Integer userId);

    /**
     * 灾害点列表
     * @param tabBasic
     * @return
     */
    List<TabBasicVo> selectTabBasic(TabBasicVo tabBasic);

    TabBasicVo selectTabBasicById(Integer id);

    List<Map<String,Object>> getBasicByNPerson(int id);

    List<Map<String, Object>> getBasicNperson(String disNo);
}