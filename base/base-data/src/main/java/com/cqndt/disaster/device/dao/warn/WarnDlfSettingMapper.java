package com.cqndt.disaster.device.dao.warn;

import com.cqndt.disaster.device.domain.WarnDlfSetting;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author yin_q
 * @Date 2019/8/31 14:21
 * @Email yin_qingqin@163.com
 **/
@Mapper
public interface WarnDlfSettingMapper {

    /**
     * 根据设备编号获取地裂缝阈值信息
     * @param sensorNo
     * @return
     */
    List<WarnDlfSetting> queryWarnDlfSettingList(@Param("sensorNo") String sensorNo);

    /**
     * 根据设备编号删除 地裂缝阈值信息
     * @param sensorNo
     * @return
     */
    int deleteWarnDlfSettingBySensorNo(@Param("sensorNo") String sensorNo);

    /**
     * 新增阈值信息
     * @param warnDlfSetting
     * @return
     */
    int insertWarnDlfSetting(WarnDlfSetting warnDlfSetting);
}
