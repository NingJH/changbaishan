package com.cqndt.disaster.device.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther : meng
 * @Date : 2018/8/15 17:42
 **/

@Data
@NoArgsConstructor
public class TabDoc {

    private int id;
    private String docName;
    private String docType;
    private String staticType;
    private String uploadTime;
    private String docUrl;
    private String previewUrl;

}
