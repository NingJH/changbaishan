package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TbsLlj;
import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TbsLljMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbsLlj record);

    int insertSelective(TbsLlj record);

    TbsLlj selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbsLlj record);

    int updateByPrimaryKey(TbsLlj record);

    List<Map<String,Object>> tbsLlj(SearchVo vo);
}