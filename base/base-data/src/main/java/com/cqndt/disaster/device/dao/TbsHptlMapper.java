package com.cqndt.disaster.device.dao;

import com.cqndt.disaster.device.domain.TbsHptl;
import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TbsHptlMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(TbsHptl record);

    int insertSelective(TbsHptl record);

    TbsHptl selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(TbsHptl record);

    int updateByPrimaryKey(TbsHptl record);

    List<Map<String,Object>> tbsHptl(SearchVo vo);
}