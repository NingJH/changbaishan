package com.cqndt.disaster.device.vo;
/**
 * <p>Title: TabDevice</p>
 * 
 * <p>Description: 监测设备实体类</p>
 * 
 * <p>Copyright:  Copyright   HBR(cc)2018</p>
 * 
 * <p>Company: 重庆南地科技有限公司</p>
 * 
 * @author lv
 *@version 1.0
 */
public class AppTabDevice {
	
	/**
	 * 设备Id
	 * 
	 */
	private Integer deviceId;
	
	/**
	 * 设备名称
	 */
	private String deviceName;
	
	/**
	 * 设备编号
	 */
	private String deviceNo;
	
	private Integer deviceType;
	
	/***
	 * 是否自动化设备
	 */
	private String automationEquipment;
	
	/**
	 * 设备厂商
	 */
	private String deviceManufacturer;

	/**
	 * 设备注册时间
	 */
	private String registerDate;
	
	/**
	 * 设备状态
	 */
	private Integer state;
	/**
	 * 平台设备id
	 */
	private String platDeviceId;
	/**
	 * 设备sn编号
	 */
	private String sn;

	public String getPlatDeviceId() {
		return platDeviceId;
	}

	public void setPlatDeviceId(String platDeviceId) {
		this.platDeviceId = platDeviceId;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceNo() {
		return deviceNo;
	}

	public void setDeviceNo(String deviceNo) {
		this.deviceNo = deviceNo;
	}

	public String getAutomationEquipment() {
		return automationEquipment;
	}

	public void setAutomationEquipment(String automationEquipment) {
		this.automationEquipment = automationEquipment;
	}

	public String getDeviceManufacturer() {
		return deviceManufacturer;
	}

	public void setDeviceManufacturer(String deviceManufacturer) {
		this.deviceManufacturer = deviceManufacturer;
	}

	public String getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}

	public Integer getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(Integer deviceType) {
		this.deviceType = deviceType;
	}

}
