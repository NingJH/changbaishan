package com.cqndt.disaster.device.dao.warn;

import com.cqndt.disaster.device.domain.WarnQxSetting;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author yin_q
 * @Date 2019/8/31 14:21
 * @Email yin_qingqin@163.com
 **/
@Mapper
public interface WarnQxSettingMapper {

    /**
     * 根据设备编号获取坡倾阈值信息
     * @param sensorNo
     * @return
     */
    List<WarnQxSetting> queryWarnQxSettingList(@Param("sensorNo") String sensorNo);

    /**
     * 根据设备编号删除 地裂缝阈值信息
     * @param sensorNo
     * @return
     */
    int deleteWarnQxSettingBySensorNo(@Param("sensorNo") String sensorNo);

    /**
     * 新增阈值信息
     * @param warnQxSetting
     * @return
     */
    int insertWarnQxSetting(WarnQxSetting warnQxSetting);
}
