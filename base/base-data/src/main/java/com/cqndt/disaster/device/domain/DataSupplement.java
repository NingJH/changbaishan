package com.cqndt.disaster.device.domain;

import lombok.Data;

import java.util.Date;

/**
 * Create Njh
 * 数据补录
 * @Time : 2020-02-10 11:33
 **/
@Data
public class DataSupplement {

    private String deviceNo = null;//设备编号

    private String sensorNo;//传感器编号

    private String monitorNo;//监测点编号

    private String value;//值

    private Date date;//时间

    private String hsl;//含水率
    private String deep;//深度

    private String xQjy;//x(倾角仪)
    private String yQjy;//y(倾角仪)
    private String zQjy;//z(倾角仪)

    private String y;//y(静力水准仪)
    private String yValue;//y_value(静力水准仪)
    private String x;//x(静力水准仪)
    private String valueJlszy;//value(静力水准仪)
    private String baseValue;//base_value(静力水准仪)
}
