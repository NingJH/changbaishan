package com.cqndt.disaster.device.dao.app;

import com.cqndt.disaster.device.vo.SearchVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface AppTabDeviceMapper {
    /**
     * 设备总数
     * @param userId
     * @return
     */
    Integer selectCountDevice(@Param("userId") String userId);
    /**
     * 该用户设备在线数量
     */
    Integer countDeviceStateZx(@Param("userId") String userId, @Param("offlineTime") Integer offlineTime);
    /**
     * 该用户设备离线数量
     */
    Integer countDeviceStateLx(@Param("userId") String userId, @Param("offlineTime") Integer offlineTime);

    /**
     * 项目设备在线情况
     */
    List<Map<String, Object>> selectOnlineRateByProject(@Param("userId") String userId, @Param("offlineTime") Integer offlineTime, @Param("seachText") String seachText);

    List<Map<String, Object>> selectOnlineDeviceByProjectID(@Param("projectId") Integer projectId, @Param("offlineTime") Integer offlineTime);

    List<Map<String, Object>> selectPerson(@Param("projectId") Integer projectId);

    Map<String, Object> selectDeviceById(@Param("deviceId") int deviceId, @Param("offlineTime") Integer offlineTime);

    List<Map<String, Object>> deviceCoordinateByProjectId(@Param("id") int id);

    List<Map<String, Object>> selectDeviceTypeStatistics(@Param("userId") String userId);

    Integer selectDeviceTypeStatisticsTotal(@Param("userId") String userId);

    List<Map<String, Object>> selectProjectByDeviceType(@Param("userId") String userId, @Param("deviceTypeId") String deviceTypeId, @Param("projectName") String projectName);

    Integer selectProjectByDeviceTypeCount(@Param("userId") String userId, @Param("deviceTypeId") String deviceTypeId, @Param("projectName") String projectName);

    List<Map<String, Object>> projectByDeviceTypeAndId(@Param("deviceTypeId") String deviceTypeId, @Param("projectId") String projectId);

    List<Map<String,Object>> listTabSensor(@Param("deviceId") String deviceId);

    List<Map<String,Object>> tbsBmwy(SearchVo vo);

    List<Map<String,Object>> tbsChenjiang(SearchVo vo);

    List<Map<String,Object>> tbsCisheng(SearchVo vo);

    List<Map<String,Object>> tbsDbbxHp(SearchVo vo);

    List<Map<String,Object>> tbsDisheng(SearchVo vo);

    List<Map<String,Object>> tbsDmQxy(SearchVo vo);

    List<Map<String,Object>> tbsDxsw(SearchVo vo);

    List<Map<String,Object>> tbsGangjinji(SearchVo vo);

    List<Map<String,Object>> tbsGdqx(SearchVo vo);

    List<Map<String,Object>> tbsGnss(SearchVo vo);

    List<Map<String,Object>> tbsHsl(SearchVo vo);

    List<Map<String,Object>> tbsKongxishuiya(SearchVo vo);

    List<Map<String,Object>> tbsLfwy(SearchVo vo);

    List<Map<String,Object>> tbsLiefengLx(SearchVo vo);

    List<Map<String,Object>> tbsLiefengQj(SearchVo vo);

    List<Map<String,Object>> tbsLsy(SearchVo vo);

    List<Map<String,Object>> tbsLxdbd(SearchVo vo);

    List<Map<String,Object>> tbsNiweiji(SearchVo vo);

    List<Map<String,Object>> tbsPtqcbxy(SearchVo vo);

    List<Map<String,Object>> tbsQjy(SearchVo vo);

    List<Map<String,Object>> tbsQjyWbx(SearchVo vo);

    List<Map<String,Object>> tbsQxy(SearchVo vo);

    List<Map<String,Object>> tbsRgsz(SearchVo vo);

    List<Map<String,Object>> tbsSbwyCx(SearchVo vo);

    List<Map<String,Object>> tbsSbwyLx(SearchVo vo);

    List<Map<String,Object>> tbsSbwyQxy(SearchVo vo);

    List<Map<String,Object>> tbsShuiweiji(SearchVo vo);

    List<Map<String,Object>> tbsSxcfj(SearchVo vo);

    List<Map<String,Object>> tbsTotalStation(SearchVo vo);

    List<Map<String,Object>> tbsTyl(SearchVo vo);

    List<Map<String,Object>> tbsWenshidu(SearchVo vo);

    List<Map<String,Object>> tbsWwj(SearchVo vo);

    List<Map<String,Object>> tbsYingbian(SearchVo vo);

    List<Map<String,Object>> tbsYl(SearchVo vo);

    List<Map<String,Object>> tbsZhendong(SearchVo vo);

    List<Map<String,Object>> tbsZxsylj(SearchVo vo);

    Map<String,Object> tbsBmwyOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsChenjiangOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsCishengOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsDbbxHpOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsDishengOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsDmQxyOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsDxswOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsGangjinjiOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsGdqxOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsGnssOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsHslOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsKongxishuiyaOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsLfwyOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsLiefengLxOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsLiefengQjOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsLsyOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsLxdbdOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsNiweijiOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsPtqcbxyOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsQjyOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsQjyWbxOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsQxyOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsRgszOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsSbwyCxOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsSbwyLxOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsSbwyQxyOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsShuiweijiOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsSxcfjOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsTotalStationOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsTylOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsWenshiduOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsWwjOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsYingbianOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsYlOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsZhendongOrigin(@Param("sensorNo")String sensorNo);

    Map<String,Object> tbsZxsyljOrigin(@Param("sensorNo")String sensorNo);

}