package com.cqndt.disaster.device.common.shrio;

import com.cqndt.disaster.device.vo.TabUserVo;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Component;

/**
 * Created By marc
 * Date: 2019/3/6  14:32
 * Description:shiro工具类
 */
@Component
public class ShiroUtils {
    /**  加密算法 */
    public final static String hashAlgorithmName = "SHA-256";
    /**  循环次数 */
    public final static int hashIterations = 16;

    public static String sha256(String password, String salt) {
        return new SimpleHash(hashAlgorithmName, password, salt, hashIterations).toString();
    }
    public static void main(String[] args) {
        //String salt = RandomStringUtils.randomAlphanumeric(20);
        System.out.println(sha256("didiao03","zb6nDRiHWHAmo8qUMgxw"));
    }


    public static Session getSession(){
        return SecurityUtils.getSubject().getSession();
    }

    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }
    public static TabUserVo getUserEntity() {
        return (TabUserVo)SecurityUtils.getSubject().getPrincipal();
    }
    public static Integer getUserId() {
        return getUserEntity().getId();
    }

    public static void setSessionAttribute(Object key, Object value) {
        getSession().setAttribute(key, value);
    }

    public static Object getSessionAttribute(Object key) {
        return getSession().getAttribute(key);
    }

    public static boolean isLogin() {
        return SecurityUtils.getSubject().getPrincipal() != null;
    }

    public static void logout() {
        SecurityUtils.getSubject().logout();
    }

    public static String getKaptcha(String key) throws Exception {
        Object kaptcha = getSessionAttribute(key);
        if(kaptcha == null){
            throw new Exception("验证码已失效");
        }
        //getSession().removeAttribute(key);
        return kaptcha.toString();
    }
}
