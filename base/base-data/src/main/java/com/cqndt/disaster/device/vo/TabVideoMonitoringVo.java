package com.cqndt.disaster.device.vo;

import com.cqndt.disaster.device.domain.TabVideoMonitoring;
import lombok.Data;

/**
 * Created By marc
 * Date: 2019/4/18  16:12
 * Description:
 */
@Data
public class TabVideoMonitoringVo extends TabVideoMonitoring {
    /**
     * 页数
     */
    private Integer page = 1;
    /**
     * 条数
     */
    private Integer limit = 10;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 区域等级
     */
    private String level;
    /**
     * 区域编码
     */
    private String areaCode;
    /**
     * 区域编码
     */
    private String areaCodeSplit;
    /**
     * 设备厂商名称
     */
    private String staticName;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 区域名称
     */
    private String areaName;

    /**
     * 图片路径
     */
    private String img;

    public String getAreaCodeSplit(){
        if(null != getAreaCode()){
            return getAreaCode().substring(0,2);
        }
        return null;
    }
}
