package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabWarnPeopleMapper;
import com.cqndt.disaster.device.entity.data.TabWarnPeople;
import com.cqndt.disaster.device.service.data.TabWarnPeopleService;
import com.cqndt.disaster.device.vo.data.TabBindWarnPeopleVo;
import com.cqndt.disaster.device.vo.data.TabWarnPeopleVo;
import com.cqndt.disaster.device.vo.data.TabWarnPersonVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cqndt.disaster.device.util.Result;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author : laihan
 * @date : 2019/10/22
 * description :
 */
@Service
public class TabWarnPeopleServiceImpl implements TabWarnPeopleService {

    @Autowired
    private TabWarnPeopleMapper tabWarnPeopleMapper;

    /**
     * 根据当前选择的乡镇地区获取所属的灾害点（项目）
     *
     * @param areaCode
     * @return
     */
    @Override
    public List<Map<String, Object>> getBasicByCode(Integer areaCode) {
        return tabWarnPeopleMapper.getBasicByCode(areaCode);
    }

    @Override
    public Result selectList(TabWarnPeopleVo vo) {
        Result result = new Result();
        PageHelper.startPage(vo.getPage(), vo.getLimit());
        List<TabWarnPersonVo> warnPeople = tabWarnPeopleMapper.findWarnPeople(vo);
        PageInfo<TabWarnPersonVo> pageInfo = new PageInfo<>(warnPeople);
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        result.setMsg("查询成功");
        return result;
    }

    @Override
    public Result addWarnPeople(TabWarnPeopleVo vo) {
        int i = 0;
        int areaCode = vo.getAreaCode();
        int basicId = vo.getBasicId();
        Map<String, Object> basicInfo = tabWarnPeopleMapper.selectBasicInfo(basicId);

        //当前时间
        Date date = new Date();
        for (Map<String, Object> vo1 : vo.getTableData()) {

            TabWarnPeopleVo peopleVo = new TabWarnPeopleVo();
            peopleVo.setAreaCode(areaCode);
            peopleVo.setBasicId(basicId);
            peopleVo.setUnitData(vo1.get("unitData").toString());
            peopleVo.setName(vo1.get("name").toString());
            peopleVo.setJob(vo1.get("job").toString());
            peopleVo.setPhone(vo1.get("phone").toString());
            peopleVo.setLevel1(vo1.get("level1").toString());
            peopleVo.setLevel2(vo1.get("level2").toString());
            peopleVo.setLevel3(vo1.get("level3").toString());
            peopleVo.setLevel4(vo1.get("level4").toString());
            peopleVo.setCountry((vo1.get("country") == null || vo1.get("country") == "") ? null : vo1.get("country").toString());
            peopleVo.setUpdateTime(date);
            peopleVo.setAddress((String) basicInfo.get("address"));
            peopleVo.setProjectName((String) basicInfo.get("name"));
            int i1 = tabWarnPeopleMapper.addWarnPeople(peopleVo);
            System.out.println(i1);
            i++;
        }

//        List<Map<String, Object>> list = tabWarnPeopleMapper.selectList(vo);
//
//        for (Map<String, Object> map : list) {
//            //map.replace("address", map.get("address"), map.get("cityName").toString() + map.get("countyName").toString() + map.get("codeName").toString());
//            String address = map.get("cityName").toString() + map.get("countyName").toString() + map.get("codeName").toString();
//            //map.put("areaCode", map.get("cityCode").toString() + "," + map.get("countyCode").toString() + "," + map.get("code").toString());
//
////            tabWarnPeopleMapper.updateAddress(address, (Integer) map.get("id"));
//            tabWarnPeopleMapper.updateProjectName(address, map.get("projectName").toString(), Integer.parseInt(map.get("id").toString()));
//        }
        return new Result().success("成功添加" + i + "条");
    }

    @Override
    public Result peopleList(Integer id) {
        List<Map<String, Object>> list = tabWarnPeopleMapper.peopleList(id);
        for (Map map : list) {
            if (!map.containsKey("job")) {
                map.put("job", "");
            }
        }
        Result result = new Result();
        result.setCode(0);
        result.setData(list);
        result.setMsg("查询成功");
        return result;
    }

    @Override
    public Result updatePeople(TabWarnPeopleVo vo) {
        tabWarnPeopleMapper.delByAreaCode(vo.getBasicId());
        Result result = addWarnPeople(vo);
        result.setMsg("修改成功");
        return result;
//        int ii = 0;
//        //当前时间
//        Date date = new Date();
//        //根据传回的灾害点编号查询出对应的人员id集合
//        List<Integer> selectPeopleList = tabWarnPeopleMapper.selectPeopleIdByBasicId(vo.getBasicId());
//
//
//        //传回来的人员id集合
//        List<Integer> chuanPeopleList = new ArrayList<>();
//        //传回来的人员id循环添加进集合
//        for (Map<String, Object> vo1 : vo.getTableData()) {
//            chuanPeopleList.add(Integer.parseInt(vo1.get("peopleId").toString()));
//        }
//
//        for (Map<String, Object> vo1 : vo.getTableData()) {
//            for (Integer list : selectPeopleList) {
//                if (vo1.get("peopleId").toString().equals(list.toString())) {
//                    TabWarnPeopleVo peopleVo = new TabWarnPeopleVo();
//                    peopleVo.setId((Integer) vo1.get("peopleId"));
//                    peopleVo.setUnitData(vo1.get("unitData").toString());
//                    peopleVo.setName(vo1.get("name").toString());
//                    peopleVo.setJob(vo1.get("job").toString());
//                    peopleVo.setPhone(vo1.get("phone").toString());
//                    peopleVo.setLevel1(vo1.get("level1").toString());
//                    peopleVo.setLevel2(vo1.get("level2").toString());
//                    peopleVo.setLevel3(vo1.get("level3").toString());
//                    peopleVo.setLevel4(vo1.get("level4").toString());
//                    peopleVo.setCountry(vo1.get("country").toString());
//                    peopleVo.setUpdateTime(date);
//                    ii = tabWarnPeopleMapper.updatePeople(peopleVo);
//                }
//            }
//        }
//
//        /*//把传回的人员id全部删除
//        for (Integer deletePeopleId : chuanPeopleList) {
//            tabWarnPeopleMapper.deletePeople(deletePeopleId);
//        }*/
//
//        /*//需要新增的人员id集合
//        List<Integer> addPeopleList = new ArrayList<>();*/
//
//        /*//传回的人员id和已有的人员id对比
//        for (Integer list : chuanPeopleList) {
//            int count = 0;
//            for (Integer list1 : selectPeopleList) {
//                if (list == list1) {
//                    break;
//                }
//                count++;
//                if (count == selectPeopleList.size()) {
//                    //把不重复的数据存入
//                    addPeopleList.add(list);
//                }
//            }
//        }*/
//
//        //需要删除的人员id集合
//        List<Integer> deletePeopleList = new ArrayList<>();
//
//        //循环已有的所有人员id
//        for (Integer list : selectPeopleList) {
//            int count = 0;
//            for (Integer list1 : chuanPeopleList) {
//                if (list == list1) {
//                    break;
//                }
//                count++;
//                if (count == chuanPeopleList.size()) {
//                    //把需要删除的数据存入
//                    deletePeopleList.add(list);
//                }
//            }
//        }
//        //循环删除掉想要删除的人员
//        for (Integer deletePeopleId : deletePeopleList) {
//            tabWarnPeopleMapper.deletePeople(deletePeopleId);
//        }
//
//        //循环添加想要新增的人员
//        for (Map map : vo.getTableData()) {
//            if ("0".equals(map.get("peopleId").toString())) {
////                if (map.get("peopleId")==0) {
//                TabWarnPeopleVo peopleVo = new TabWarnPeopleVo();
//                peopleVo.setAreaCode(vo.getAreaCode());
//                peopleVo.setBasicId(vo.getBasicId());
//                peopleVo.setUnitData(map.get("unitData").toString());
//                peopleVo.setName(map.get("name").toString());
//                peopleVo.setJob(map.get("job").toString());
//                peopleVo.setPhone(map.get("phone").toString());
//                peopleVo.setLevel1(map.get("level1").toString());
//                peopleVo.setLevel2(map.get("level2").toString());
//                peopleVo.setLevel3(map.get("level3").toString());
//                peopleVo.setLevel4(map.get("level4").toString());
//                peopleVo.setCountry(map.get("country").toString());
//                peopleVo.setUpdateTime(date);
//                tabWarnPeopleMapper.addWarnPeople(peopleVo);
//            }
//        }
//
//        Result result = new Result();
//        result.setCode(0);
//        result.setData(ii);
//        result.setMsg("修改成功");
//        return result;

    }

    @Override
    public Result delete(TabWarnPeopleVo vo) {
        if (null == vo.getIds()) {
            return new Result().failure(-1, "请选择至少一条数据进行操作");
        }
        String[] ids = vo.getIds().split(",");
        for (String idStr : ids) {
            tabWarnPeopleMapper.delete(Integer.parseInt(idStr));
        }
        return new Result().success("删除成功");
    }

    /**
     * 绑定时查询列表
     *
     * @return
     */
    @Override
    public List<TabBindWarnPeopleVo> selectList1(TabBindWarnPeopleVo tabBindWarnPeopleVo) {
        /*//灾害点绑定预警人员
        if (!StringUtils.isEmpty(tabBindWarnPeopleVo.getIds())){
            String[] arr = tabBindWarnPeopleVo.getIds().split(",");
            return tabWarnPeopleMapper.selectList2(arr);
        }*/
        //预警人员列表
        return tabWarnPeopleMapper.selectList1(tabBindWarnPeopleVo);
    }

    @Override
    public List<TabWarnPeople> allWarnPeople() {
        List<TabWarnPeople> warnPeoples = tabWarnPeopleMapper.allWarnPeople();
        return warnPeoples;
    }
}
