package com.cqndt.disaster.device.controller.data;


import com.cqndt.disaster.device.entity.data.TabNPerson;
import com.cqndt.disaster.device.service.data.TabNPersonService;
import com.cqndt.disaster.device.util.Result;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("tabNPerson")
@Api(value = "人员信息")
public class TabNPersonController {

    @Resource
    private TabNPersonService nPersonService;

    //function
    public Result success(Object date,String msg){
        Result result = new Result();
        result.setCode(0);
        result.setData(date);
        result.setMsg(msg);
        return result;
    }

    @ApiOperation("新增人员信息")
    @PostMapping("add")
    public Result add(@RequestBody TabNPerson nPerson){
        nPersonService.savePerson(nPerson);
        return success(null,"新增人员成功");
    }

    @ApiOperation("删除人员")
    @PostMapping("delete")
    public Result delete(@RequestBody TabNPerson nPerson){
        return new Result().success(nPersonService.deletePersonById(nPerson));
    }

    @ApiOperation("修改人员信息")
    @PostMapping("update")
    public Result update(@RequestBody TabNPerson nPerson){
        nPersonService.updatePerson(nPerson);
        return success(null,"修改人员信息成功");
    }

    @ApiOperation("查询人员信息byId")
    @PostMapping("findById")
    public Result findById(@RequestParam("personId") Integer personId){
        return success(nPersonService.findPersonById(personId),"查询人员信息成功");
    }

    @ApiOperation("查询人员信息byPage")
    @PostMapping("findAllByPage")
    public Result findAllByPage(@RequestBody TabNPerson tabNPerson){
        Result result = new Result();
        PageHelper.startPage(tabNPerson.getPage(),tabNPerson.getLimit());
        PageInfo<TabNPerson> pageInfo = new PageInfo<>(nPersonService.findPersonsByPage(tabNPerson));
        result.setMsg("操作成功");
        result.setCode(0);
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

}
