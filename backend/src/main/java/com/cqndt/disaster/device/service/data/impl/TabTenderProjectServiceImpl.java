package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.entity.data.TabTenderProject;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabTenderProjectVo;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.service.data.TabTenderProjectService;
import com.cqndt.disaster.device.dao.data.TabTenderProjectMapper;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TabTenderProjectServiceImpl implements TabTenderProjectService{
    @Autowired
    private TabTenderProjectMapper tabTenderProjectMapper;
    
    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(TabTenderProjectVo tabTenderProjectVo) throws Exception {
        tabTenderProjectMapper.save(tabTenderProjectVo);
    
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void update(TabTenderProjectVo tabTenderProjectVo) throws Exception {
        TabTenderProjectVo vo = tabTenderProjectMapper.getTabTenderProjectById(tabTenderProjectVo.getId());
        AttachmentVo attachmentVo = new AttachmentVo();
        tabTenderProjectMapper.update(tabTenderProjectVo);
     
   }
    @Override
    public TabTenderProjectVo getTabTenderProjectById(Long id){
        return tabTenderProjectMapper.getTabTenderProjectById(id);
    }
    @Override
    public void delete(String id){
         String[] ids = id.split(",");
         for (String idStr:ids) {
            tabTenderProjectMapper.delete(Integer.parseInt(idStr));
         }
    }
    @Override
    public List<TabTenderProjectVo> list(TabTenderProjectVo tabTenderProjectVo) {
        return  tabTenderProjectMapper.list(tabTenderProjectVo);
    }
    @Override
    public List<Map<String,Object>> listTabStaticType(TabTenderProjectVo tabTenderProjectVo){
        return  tabTenderProjectMapper.listTabStaticType(tabTenderProjectVo);
    }

    


}