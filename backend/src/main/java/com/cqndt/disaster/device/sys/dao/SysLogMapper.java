package com.cqndt.disaster.device.sys.dao;

import com.cqndt.disaster.device.sys.vo.SysLogVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created By marc
 * Date: 2019/3/18  15:14
 * Description:
 */
@Mapper
public interface SysLogMapper {
    void save(SysLogVo sysLogVo);
}
