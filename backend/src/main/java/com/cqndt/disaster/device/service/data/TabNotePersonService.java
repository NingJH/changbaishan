package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabNotePerson;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabNotePersonVo;

import java.util.List;

/**
 * @version 1.0
 * @Author:hh
 * @Date:2019/8/5
 * @Description:com.cqndt.disaster.device.service.data
 */
public interface TabNotePersonService {
    /**
     * 添加短信人员
     */
    Result saveTabNotePerson(TabNotePersonVo tabNotePersonVo);

    /**
     * 查询短信人员
     * @param tabNotePersonVo
     * @return
     */
    List<TabNotePersonVo> queryTabNotePerson(TabNotePersonVo tabNotePersonVo);

    /**
     * 删除短信人员
     * @param id
     * @return
     */
    int deleteTabNotePersonById(String id);

    /**
     * 修改短信人员
     * @param tabNotePersonVo
     * @return
     */
    Result updateTabNotePersonById(TabNotePersonVo tabNotePersonVo);

}
