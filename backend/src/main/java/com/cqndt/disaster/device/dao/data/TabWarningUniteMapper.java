package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabWarningUnite;
import com.cqndt.disaster.device.vo.data.TabWarningUniteVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TabWarningUniteMapper {
    /**
     * 联合预警列表
     */
    List<Map<String, Object>> selectList(TabWarningUniteVo vo);
    /**
     * 联合预警新增
     */
    int save(TabWarningUniteVo vo);
    /**
     * 联合预警删除
     */
    int delete(int id);
    /**
     * 修改
     */
    int update(TabWarningUniteVo vo);

    int getMaxId();

    int countId();

}
