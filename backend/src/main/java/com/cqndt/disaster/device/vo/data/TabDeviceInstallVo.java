package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabDeviceInstall;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
* Description: 
* Date: 2019-06-26
*/
@Data
public class TabDeviceInstallVo extends TabDeviceInstall {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;

    private String[] imgId;

    private String[] videoId;


    private List<Map<String, Object>> filesInfo;

    private List<Map<String, Object>> videoInfo;

}
