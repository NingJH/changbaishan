package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabDeviceMapper;
import com.cqndt.disaster.device.dao.data.TabDeviceTemplateMapper;
import com.cqndt.disaster.device.dao.data.TabProjectMapper;
import com.cqndt.disaster.device.entity.data.TabDeviceTemplate;
import com.cqndt.disaster.device.service.data.TabDeviceTemplateService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabDeviceTemplateVo;
import com.cqndt.disaster.device.vo.data.TabProjectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TabDeviceTemplateServiceImpl implements TabDeviceTemplateService {

    @Autowired
    TabDeviceTemplateMapper tabDeviceTemplateMapper;
    @Autowired
    private TabProjectMapper tabProjectMapper;
    @Autowired
    private TabDeviceMapper tabDeviceMapper;

    @Override
    public void saveTabDeviceTemplate(TabDeviceTemplate tabDeviceTemplate) {
        tabDeviceTemplateMapper.saveTabDeviceTemplate(tabDeviceTemplate);
    }

    @Override
    public void updateTabDeviceTemplate(TabDeviceTemplateVo tabDeviceTemplate) {
        tabDeviceTemplateMapper.updateTabDeviceTemplate(tabDeviceTemplate);
    }

    @Override
    public List<TabDeviceTemplateVo> listTabDeviceTemplate(TabDeviceTemplateVo tabDeviceTemplate) {
        return tabDeviceTemplateMapper.listTabDeviceTemplate(tabDeviceTemplate);
    }

    @Override
    public void deleteTabDeviceTemplate(Integer id) {
        tabDeviceTemplateMapper.deleteTabDeviceTemplate(id);
    }

    @Override
    public List<Map<String,Object>> getTabDeviceTemplate(List<TabProjectVo> tabProjectVo) {
        List<Map<String, Object>> list = new ArrayList<>();

        for(TabProjectVo vo:tabProjectVo){
            //获取选中项目里的设备
            List<Map<String, Object>> children = vo.getChildren();
            for (Map map : children){
                //获取选中设备里的设备类型标识模版
                List<TabDeviceTemplateVo> deviceType = tabDeviceTemplateMapper.getTabDeviceTemplateByDeviceValue(map.get("deviceType").toString());
                map.put("marks",deviceType);
                list.add(map);
            }
        }
       return  list;
    }
}
