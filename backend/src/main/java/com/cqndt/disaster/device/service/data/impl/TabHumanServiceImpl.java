package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabHumanMapper;
import com.cqndt.disaster.device.service.data.TabHumanService;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.vo.data.TabHumanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TabHumanServiceImpl implements TabHumanService {
    @Autowired
    private TabHumanMapper tabHumanMapper;

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(TabHumanVo tabHumanVo) throws Exception {
        tabHumanMapper.save(tabHumanVo);
    
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void update(TabHumanVo tabHumanVo) throws Exception {
        TabHumanVo vo = tabHumanMapper.getTabHumanById(tabHumanVo.getId());
        AttachmentVo attachmentVo = new AttachmentVo();
        tabHumanMapper.update(tabHumanVo);
     
   }
    @Override
    public TabHumanVo getTabHumanById(Long id){
        return tabHumanMapper.getTabHumanById(id);
    }
    @Override
    public void delete(String id){
         String[] ids = id.split(",");
         for (String idStr:ids) {
            tabHumanMapper.delete(Integer.parseInt(idStr));
         }
    }
    @Override
    public List<TabHumanVo> list(TabHumanVo tabHumanVo) {
        return  tabHumanMapper.list(tabHumanVo);
    }



}