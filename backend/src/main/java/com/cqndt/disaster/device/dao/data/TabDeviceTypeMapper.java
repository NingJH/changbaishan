package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabDeviceType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Time : 2019-09-15 21:47
 **/
@Mapper
public interface TabDeviceTypeMapper {

    int insertScale(TabDeviceType tabDeviceType);

    List<String> selectTypeName(String deviceType);

    String selectMonitorNo(String deviceNo);
}
