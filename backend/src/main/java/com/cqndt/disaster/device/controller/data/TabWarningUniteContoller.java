package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.dao.data.TabDeviceMapper;
import com.cqndt.disaster.device.dao.data.TabDeviceTemplateMapper;
import com.cqndt.disaster.device.dao.data.TabWarningDeviceMapper;
import com.cqndt.disaster.device.dao.data.TabWarningUniteMapper;
import com.cqndt.disaster.device.entity.data.TabWarningDevice;
import com.cqndt.disaster.device.service.data.TabWarningUniteService;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import com.cqndt.disaster.device.util.AbstractController;
import com.cqndt.disaster.device.util.JSONUtils;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.ibatis.annotations.Param;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;


@Api(description = "联合预警表")
@RestController
@RequestMapping("tabWarningUnite")
public class TabWarningUniteContoller extends AbstractController {
    @Autowired
    private TabWarningUniteService tabWarningUniteService;


    @ApiOperation(value = "联合预警列表", notes = "联合预警列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "name", value = "名称", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "每页条数", defaultValue = "", required = true, paramType = "query")
    })
    @RequiresPermissions("sys:tabWarningUnite:list")
    @PostMapping("selectList")
    public Result selectList(@RequestBody TabWarningUniteVo vo) {
        return tabWarningUniteService.selectList(vo);
    }

    @ApiOperation(value = "新增联合预警", notes = "新增联合预警")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "name", value = "名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "isAvailable", value = "是否启用", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "blueWarning", value = "蓝色告警（1级）", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "yellowWarning", value = "黄色告警（2级）", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "orangeWarning", value = "橙色告警（3级）", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "redWarning", value = "红色告警（4级）", defaultValue = "", required = true, paramType = "query")
    })
    @RequiresPermissions("sys:tabWarningUnite:save")
    @PostMapping("save")
    public Result save(@RequestBody TabjieshouVo vo) {
        if (vo.getThres().size() == 0){
            return new Result().failure(-1,"请至少选择一个设备填写阈值");
        }
        return tabWarningUniteService.save(vo);
    }

    @ApiOperation(value = "修改联合预警", notes = "修改联合预警")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "name", value = "名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "isAvailable", value = "是否启用", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "blueWarning", value = "蓝色告警短信模版（4级）", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "yellowWarning", value = "黄色告警短信模版（3级）", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "orangeWarning", value = "橙色告警短信模版（2级）", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "redWarning", value = "红色告警短信模版（1级）", defaultValue = "", required = true, paramType = "query")
    })
    @RequiresPermissions("sys:tabWarningUnite:update")
    @PostMapping("update")
    public Result update(@RequestBody TabjieshouVo vo) {
        if (vo.getThres().size() == 0){
            return new Result().failure(-1,"请至少选择一个设备填写阈值");
        }
        return tabWarningUniteService.update(vo);
    }

    @ApiOperation(value = "删除联合预警", notes = "删除联合预警")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "删除联合预警信息的id", defaultValue = "", required = true, paramType = "query")
    })
    @RequiresPermissions("sys:tabWarningUnite:delete")
    @PostMapping("delete")
    public Result delete(String id) {
        return tabWarningUniteService.delete(id);
    }

    @ApiOperation(value = "通过联合预警id获取所有选中项目及设备和阈值", notes = "通过联合预警id获取所有选中项目及设备和阈值")
    @PostMapping("getCheck")
    public Result getCheck(@RequestBody TabjieshouVo vo) {
        return tabWarningUniteService.getCheck(vo);
    }


    @ApiOperation(value = "选中设备进入阈值填写接口", notes = "选中设备进入阈值填写接口")
    @PostMapping("reciveDevice")
    public Result reciveDevice(@RequestBody TabJHouVo vo) throws Exception {
        if (vo.getAid()==null || "".equals(vo.getAid().toString())){
            return new Result().failure(-1,"操作失败。请选择一条数据");
        }

        return tabWarningUniteService.reciveDevice(vo);
    }
}
