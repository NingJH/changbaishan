package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.WarnPqSetting;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabWarnSettingVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


/**
 * Description: 
 * Date: 2019-06-11
 */

@Mapper
public interface TabWarnSettingMapper {
    /**
     * 根据条件查询list
     * @param tabWarnSettingVo 查询条件
     */
    List<TabWarnSettingVo> list(TabWarnSettingVo tabWarnSettingVo);
    /**
     * 增加
     * @param tabWarnSettingVo 条件
     */
    int save(TabWarnSettingVo tabWarnSettingVo);
    /**
     * 修改
     * @param tabWarnSettingVo 条件
     */
    int update(TabWarnSettingVo tabWarnSettingVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    TabWarnSettingVo getTabWarnSettingById(Long id);


    TabWarnSettingVo getTabDeviceInstallByDeviceNo(String sensorNo);

    /**
     * 根据传感器编号获取阈值对象
     * @param sensorNo
     * @return
     */
    List<TabWarnSettingVo> getTabDeviceInstallBySensorNoList(List<String> sensorNo);

    Map<String, Object> queryWarnSettingList(String sensorNo);

    Map<String, Object> warnCishengAmplitude(String sensorNo);

    Map<String, Object> warnCishengFrequency(String sensorNo);

    int updateWarnSetting(TabWarnSettingVo tabWarnSettingVo);
}
