package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabAttachmentMapper;
import com.cqndt.disaster.device.dao.data.TabDeviceCheckMapper;
import com.cqndt.disaster.device.dao.data.TabDeviceInstallMapper;
import com.cqndt.disaster.device.dao.data.TabStaticTypeMapper;
import com.cqndt.disaster.device.entity.data.TabAttachment;
import com.cqndt.disaster.device.entity.data.TabDeviceCheck;
import com.cqndt.disaster.device.entity.data.TabStaticType;
import com.cqndt.disaster.device.service.data.TabDeviceCheckService;
import com.cqndt.disaster.device.util.ExportExcelUtil;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabDeviceCheckVo;
import com.cqndt.disaster.device.vo.data.TabDeviceVo;
import com.cqndt.disaster.device.vo.data.TabStaticTypeVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TabDeviceCheckServiceImpl implements TabDeviceCheckService {

    @Autowired
    private TabDeviceCheckMapper tabDeviceCheckMapper;

    @Autowired
    private TabStaticTypeMapper tabStaticTypeMapper;

    @Autowired
    private TabDeviceInstallMapper tabDeviceInstallMapper;

    @Autowired
    private TabAttachmentMapper tabAttachmentMapper;

    @Value("${file-ip}")
    private String fileIp;
    @Value("attachmentUrlSet")
    private String attachmentUrlSet;
    @Value("tempAttachmentUrl")
    private String tempAttachmentUrl;
    @Value("${open-office-path}")
    private String openOfficePath;
    @Value("${open-office-host}")
    private String openOfficeHost;

    @Override
    public List<TabDeviceCheckVo> list(TabDeviceCheckVo tabDeviceCheckVo) {
        List<TabDeviceCheckVo> list = tabDeviceCheckMapper.list(tabDeviceCheckVo);
        if (null != list && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                List<Map<String, Object>> list1 = new ArrayList<>();
                String imgIds = list.get(i).getImgIds();
                if (StringUtils.isNotEmpty(imgIds)) {
                    String[] arr = imgIds.split(",");
                    for (int k = 0; k < arr.length; k++) {
                        if (StringUtils.isNotEmpty(arr[k])) {
                            Map<String, Object> map = tabDeviceInstallMapper.getUrlName(arr[k]);
                            //Map<String, Object> map = new HashMap<>();
                            map.put("viewUrl",fileIp + File.separator + "img" + File.separator + arr[k]);
                            //map.put("attachmentUrl",arr[k]);
                            list1.add(map);
                        } else {
                            break;
                        }
                    }
                }
                List<Map<String, Object>> list2 = new ArrayList<>();
                String videosIds = list.get(i).getVideoIds();
                if (StringUtils.isNotEmpty(videosIds)) {
                    String[] arr = videosIds.split(",");
                    for (int k = 0; k < arr.length; k++) {
                        if (StringUtils.isNotEmpty(arr[k])) {
                            Map<String, Object> map = tabDeviceInstallMapper.getUrlName(arr[k]);
                            //Map<String, Object> map = new HashMap<>();
                            map.put("viewUrl",fileIp + File.separator + "video" + File.separator + arr[k]);
                           //map.put("attachmentUrl",arr[k]);
                            //map.put("viewUrl",fileIp + File.separator + "video" + File.separator + map.get("attachmentUrl"));
                            list2.add(map);
                        } else {
                            break;
                        }
                    }
                }
                list.get(i).setDeviceCheckImgs(list1);
                list.get(i).setDeviceCheckVideos(list2);
            }
        }
        return list;
    }

    @Override
    public Result save(TabDeviceCheckVo tabDeviceCheckVo) {
        Result result = new Result();
        if( null == tabDeviceCheckVo.getDeviceNo() || (tabDeviceCheckVo.getDeviceNo()+"") == "" ){
            result.failure(-1,"设备编号不能为空");
            return result;
        }
        tabDeviceCheckMapper.save(tabDeviceCheckVo);
        result.setMsg("操作成功");
        return result;
    }

    @Override
    public Result update(TabDeviceCheckVo tabDeviceCheckVo) {
        Result result = new Result();
        if(null == tabDeviceCheckVo.getId()){
            result.failure(-1,"至少选择一条数据");
            return result;
        }
        tabDeviceCheckMapper.updateAll(tabDeviceCheckVo);
        result.setMsg("操作成功");
        return result;
    }

    @Override
    public Result delete(String id) {
        Result result = new Result();
        if(null == id){
            result.failure(-1,"至少选择一条数据");
            return result;
        }
        String[] str = id.split(",");
        tabDeviceCheckMapper.delete(str);
        result.setMsg("操作成功");

        return result;
    }


}
