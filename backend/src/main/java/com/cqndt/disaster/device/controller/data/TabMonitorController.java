package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.service.data.TabAreaService;
import com.cqndt.disaster.device.service.data.TabMonitorService;
import com.cqndt.disaster.device.util.AbstractController;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabMonitorVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Description:
 * Date: 2019-06-14
 */
@RestController
@RequestMapping("tabMonitor")
@Api(value = "安装布置点", description = "安装布置点")
public class TabMonitorController extends AbstractController {

    @Autowired
    private TabMonitorService tabMonitorService;

    @Autowired
    private TabAreaService tabAreaService;

    @SysLog("查询安装布置点")
    @PostMapping("list")
    @RequiresPermissions("sys:tabmonitor:list")
    @ApiOperation(value = "安装布置点列表", notes = "安装布置点列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "所属区域", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "monitorName", value = "布置点名称", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "monitorNo", value = "布置点编号", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "position", value = "地理位置", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "project", value = "项目id", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "每页条数", defaultValue = "", required = true, paramType = "query")
    })
    public Result listTabUser(@RequestBody TabMonitorVo tabMonitorVo) {
        tabMonitorVo.setUserId(getUserId().toString());
        PageHelper.startPage(tabMonitorVo.getPage(), tabMonitorVo.getLimit());
        PageInfo<TabMonitorVo> pageInfo = new PageInfo<TabMonitorVo>(tabMonitorService.list(tabMonitorVo));
        Result result = new Result();
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    /**
     * 信息
     */
    @PostMapping("getTabMonitorById")
    public Result getTabMonitorById(Long id) {
        if (null == id) {
            return new Result().failure(-1, "请选择一条数据进行操作");
        }
        TabMonitorVo tabMonitorVo = tabMonitorService.getTabMonitorById(id);
        return new Result().success(tabMonitorVo);
    }

    /**
     * 保存
     */
    @SysLog("增加")
    @PostMapping("save")
    //@RequiresPermissions("sys:tabmonitor:save")
    @ApiOperation(value = "增加安装布置点", notes = "增加安装布置点")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "monitorNo", value = "布置点编号", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "monitorName", value = "布置点名称", defaultValue = "",required = true,  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "latitude", value = "纬度", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "longitude", value = "经度", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "position", value = "地理位置", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Long", name = "deviceId", value = "灾害点id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Long", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query")
    })
    public Result save(@RequestBody  TabMonitorVo tabMonitorVo) {
        Result result = new Result();
        try {
            if(StringUtils.isEmpty(tabMonitorVo.getMonitorNo())){
                return result.failure(-1, "布置点编号不能为空");
            }else if(StringUtils.isEmpty(tabMonitorVo.getMonitorName())){
                return result.failure(-1, "布置点名称不能为空");
            }else if(tabMonitorVo.getDeviceId() == 0){
                return result.failure(-1, "灾害点id不能为空");
            }else if(tabMonitorVo.getProjectId() == 0){
                return result.failure(-1, "项目id不能为空");
            }
            String[] arr = tabMonitorVo.getLonLat().split(",");
            BigDecimal longitude =new BigDecimal(arr[0]);
            tabMonitorVo.setLongitude(longitude);
            BigDecimal lagitude =new BigDecimal(arr[1]);
            tabMonitorVo.setLatitude(lagitude);
            Date date = new Date();
            tabMonitorVo.setCreateTime(date);
            tabMonitorService.save(tabMonitorVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("update")
    //@RequiresPermissions("sys:tabmonitor:update")
    @ApiOperation(value = "修改安装布置点", notes = "修改安装布置点")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "布置点id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "monitorNo", value = "布置点编号", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "monitorName", value = "布置点名称", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "latitude", value = "纬度", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "longitude", value = "经度", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "position", value = "地理位置", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Long", name = "deviceId", value = "灾害点id", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Long", name = "projectId", value = "项目id", defaultValue = "",  paramType = "query")
    })
    public Result update(@RequestBody TabMonitorVo tabMonitorVo) {
        Result result = new Result();
        try {
            if (null == tabMonitorVo.getId()) {
                return result.failure(-1, "请选择至少一条数据进行操作");
            }
            String[] arr = tabMonitorVo.getLonLat().split(",");
            BigDecimal longitude =new BigDecimal(arr[0]);
            tabMonitorVo.setLongitude(longitude);
            BigDecimal lagitude =new BigDecimal(arr[1]);
            tabMonitorVo.setLatitude(lagitude);
            tabMonitorService.update(tabMonitorVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabmonitor:delete")
    @ApiOperation(value = "删除安装布置点",notes = "删除安装布置点")
    @ApiImplicitParam(name = "id", value="布置点id", dataType = "String", required = true, paramType="query")
    public Result delete(@RequestBody TabMonitorVo tabMonitorVo) {
        Result result = new Result();
        try {
            if (null == tabMonitorVo.getId()) {
                return result.failure(-1, "请选择至少一条数据进行操作");
            }
            tabMonitorService.delete(tabMonitorVo.getId());
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 获取项目列表
     */
    @PostMapping("getUserProjectInfo")
    public Result getUserProjectInfo() {
        Result result = new Result();
        List<Map<String, Object>> list = tabMonitorService.getUserProjectInfo(getUserId());
        return result.success(list);
    }

    @GetMapping("listTabAreaInfo")
    public Result listTabAreaInfo(String areaId) {
        Result result = new Result();
        try {
            List<Map<String, Object>> list = tabMonitorService.listTabAreaInfo(areaId);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    @PostMapping("getDeviceNameList")
    @ApiOperation(value = "通过项目ID获取下面的设备名称",notes = "通过项目ID获取下面的设备名称")
    @ApiImplicitParam(name = "deviceId", value="项目id", dataType = "String", required = true, paramType="query")
    public Result getDeviceNameList(@RequestBody TabMonitorVo vo){
        Result result = new Result();
        if(String.valueOf(vo.getDeviceId()) == null){
            return result.failure(-1, "id不能为空");
        }
        List<TabArea> listChild = tabAreaService.TabAreaList(getUser());
        List<Map<String,String>> list = tabMonitorService.getDeviceNameList(String.valueOf(vo.getDeviceId()),listChild);
        result.setData(list);
        result.setMsg("操作成功");
        return result;
    }


}