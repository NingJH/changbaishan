package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.service.data.TabAreaService;
import com.cqndt.disaster.device.service.data.TabBasicService;
import com.cqndt.disaster.device.service.data.TabUnitService;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import com.cqndt.disaster.device.util.AbstractController;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabUnitVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("tabUnit")
@Api(value = "单位管理", description = "单位管理")
public class TabUnitController extends AbstractController {
    @Autowired
    private TabUnitService tabUnitService;
    @Autowired
    private TabBasicService tabBasicService;
    @Autowired
    private TabAreaService tabAreaService;


    @ApiOperation(value = "单位类别、厂商类别列表", notes = "单位类别、厂商类别列表")
    @PostMapping("getVendorType")
    public Result getVendorType() {
        return tabUnitService.getVendorType();
    }

    @ApiOperation(value = "单位信息列表", notes = "单位信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "unitName", value = "单位名称", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "unitPerson", value = "负责人", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "phone", value = "电话号码", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "unitAddress", value = "单位所属地区", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "vendorId", value = "厂商、单位id", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "每页条数", defaultValue = "", required = true, paramType = "query")
    })
    @PostMapping("queryList")
    @RequiresPermissions("sys:tabUnit:list")
    public Result queryList(@RequestBody TabUnitVo vo) {
        SysUserVo sysUserVo = getUser();
        List<TabArea> list = tabAreaService.TabAreaList(sysUserVo);
        vo.setList(list);
        return tabUnitService.queryList(vo);
    }

    @ApiOperation(value = "新增单位信息", notes = "新增单位信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "unitName", value = "单位名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "unitPerson", value = "负责人", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "phone", value = "电话号码", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "areaCode", value = "乡镇或者街道Code", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "vendorId", value = "厂商、单位id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "每页条数", defaultValue = "", required = true, paramType = "query")
    })
    @PostMapping("addUnit")
    @RequiresPermissions("sys:tabUnit:save")
    public Result addUnit(@RequestBody TabUnitVo vo) {
        return tabUnitService.addUnit(vo);
    }

    @ApiOperation(value = "删除单位信息", notes = "删除单位信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "删除单位信息的id", defaultValue = "", required = true, paramType = "query")
    })
    @PostMapping("deleteUnitById")
    @RequiresPermissions("sys:tabUnit:delete")
    public Result deleteUnitById(@RequestBody TabUnitVo vo) {
        return tabUnitService.deleteUnitById(vo);
    }

    @ApiOperation(value = "根据id查询（查看详情、修改）单位信息", notes = "根据id查询（查看详情、修改）单位信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "查询（查看详情、修改）单位信息的id", defaultValue = "", required = true, paramType = "query")
    })
    @PostMapping("getTabUnitById")
    public Result getTabUnitById(Long id) {
        return tabUnitService.getTabUnitById(id);
    }

    @GetMapping("listTabAreaInfo")
    public Result listTabAreaInfo(String areaId) {
        Result result = new Result();
        try {
            List<Map<String, Object>> list = tabBasicService.listTabAreaInfo(areaId);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    @ApiOperation(value = "修改单位信息", notes = "修改单位信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "unitName", value = "单位名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "unitPerson", value = "负责人", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "phone", value = "电话号码", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "areaCode", value = "乡镇或者街道Code", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "vendorId", value = "厂商、单位id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "每页条数", defaultValue = "", required = true, paramType = "query")
    })
    @PostMapping("updateUnit")
    @RequiresPermissions("sys:tabUnit:update")
    public Result updateUnitById(@RequestBody TabUnitVo vo){
        return tabUnitService.updateUnitById(vo);
    }

    @ApiOperation(value = "根据单位类别id查询单位类别名称", notes = "根据单位类别id查询单位类别名称")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "vendorId", value = "厂商、单位id", defaultValue = "", paramType = "query")
    })
    @PostMapping("getTabUnitByVendorId")
    public Result getTabUnitByVendorId(int vendorId){
        return tabUnitService.getTabUnitByVendorId(vendorId);
    }

}
