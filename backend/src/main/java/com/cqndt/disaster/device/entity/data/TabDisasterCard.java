package com.cqndt.disaster.device.entity.data;

import java.io.Serializable;

/**
* Description: 
* Date: 2019-06-11
*/

public class TabDisasterCard implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  
     */
    private Long id;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getdPosition() {
        return dPosition;
    }

    public void setdPosition(String dPosition) {
        this.dPosition = dPosition;
    }

    public String getdType() {
        return dType;
    }

    public void setdType(String dType) {
        this.dType = dType;
    }

    public String getdInduceFactor() {
        return dInduceFactor;
    }

    public void setdInduceFactor(String dInduceFactor) {
        this.dInduceFactor = dInduceFactor;
    }

    public String getdThreat() {
        return dThreat;
    }

    public void setdThreat(String dThreat) {
        this.dThreat = dThreat;
    }

    public String getdMonitorMan() {
        return dMonitorMan;
    }

    public void setdMonitorMan(String dMonitorMan) {
        this.dMonitorMan = dMonitorMan;
    }

    public String getdMonitorPhone() {
        return dMonitorPhone;
    }

    public void setdMonitorPhone(String dMonitorPhone) {
        this.dMonitorPhone = dMonitorPhone;
    }

    public String getdMonitorSign() {
        return dMonitorSign;
    }

    public void setdMonitorSign(String dMonitorSign) {
        this.dMonitorSign = dMonitorSign;
    }

    public String getdAlarmType() {
        return dAlarmType;
    }

    public void setdAlarmType(String dAlarmType) {
        this.dAlarmType = dAlarmType;
    }

    public String getdMonitorJudge() {
        return dMonitorJudge;
    }

    public void setdMonitorJudge(String dMonitorJudge) {
        this.dMonitorJudge = dMonitorJudge;
    }

    public String getdEPlace() {
        return dEPlace;
    }

    public void setdEPlace(String dEPlace) {
        this.dEPlace = dEPlace;
    }

    public String getdELine() {
        return dELine;
    }

    public void setdELine(String dELine) {
        this.dELine = dELine;
    }

    public String getdESignal() {
        return dESignal;
    }

    public void setdESignal(String dESignal) {
        this.dESignal = dESignal;
    }

    public String getdEvacuateMan() {
        return dEvacuateMan;
    }

    public void setdEvacuateMan(String dEvacuateMan) {
        this.dEvacuateMan = dEvacuateMan;
    }

    public String getdEvacuatePhone() {
        return dEvacuatePhone;
    }

    public void setdEvacuatePhone(String dEvacuatePhone) {
        this.dEvacuatePhone = dEvacuatePhone;
    }

    public String getdExcludeMan() {
        return dExcludeMan;
    }

    public void setdExcludeMan(String dExcludeMan) {
        this.dExcludeMan = dExcludeMan;
    }

    public String getdExcludePhone() {
        return dExcludePhone;
    }

    public void setdExcludePhone(String dExcludePhone) {
        this.dExcludePhone = dExcludePhone;
    }

    public String getdSecurityMan() {
        return dSecurityMan;
    }

    public void setdSecurityMan(String dSecurityMan) {
        this.dSecurityMan = dSecurityMan;
    }

    public String getdSecurityPhone() {
        return dSecurityPhone;
    }

    public void setdSecurityPhone(String dSecurityPhone) {
        this.dSecurityPhone = dSecurityPhone;
    }

    public String getdDocMan() {
        return dDocMan;
    }

    public void setdDocMan(String dDocMan) {
        this.dDocMan = dDocMan;
    }

    public String getdDocPhone() {
        return dDocPhone;
    }

    public void setdDocPhone(String dDocPhone) {
        this.dDocPhone = dDocPhone;
    }

    public String getdGrantUnit() {
        return dGrantUnit;
    }

    public void setdGrantUnit(String dGrantUnit) {
        this.dGrantUnit = dGrantUnit;
    }

    public String getdGrantPhone() {
        return dGrantPhone;
    }

    public void setdGrantPhone(String dGrantPhone) {
        this.dGrantPhone = dGrantPhone;
    }

    public String getdGrantDate() {
        return dGrantDate;
    }

    public void setdGrantDate(String dGrantDate) {
        this.dGrantDate = dGrantDate;
    }

    public String getdHoldUnit() {
        return dHoldUnit;
    }

    public void setdHoldUnit(String dHoldUnit) {
        this.dHoldUnit = dHoldUnit;
    }

    public String getdHoldPhone() {
        return dHoldPhone;
    }

    public void setdHoldPhone(String dHoldPhone) {
        this.dHoldPhone = dHoldPhone;
    }

    public String getdHoldDate() {
        return dHoldDate;
    }

    public void setdHoldDate(String dHoldDate) {
        this.dHoldDate = dHoldDate;
    }

    public String getdNo() {
        return dNo;
    }

    public void setdNo(String dNo) {
        this.dNo = dNo;
    }

    /**
     *  灾害位置
     */
    private String dPosition;
    /**
     *  类型及规模
     */
    private String dType;
    /**
     *  诱发因素
     */
    private String dInduceFactor;
    /**
     *  威胁对象
     */
    private String dThreat;
    /**
     *  监测负责人
     */
    private String dMonitorMan;
    /**
     *  联系电话
     */
    private String dMonitorPhone;
    /**
     *  监测的主要迹象
     */
    private String dMonitorSign;
    /**
     *  预警的主要手段
     */
    private String dAlarmType;
    /**
     *  临灾预报的判断
     */
    private String dMonitorJudge;
    /**
     *  预定避灾地点
     */
    private String dEPlace;
    /**
     *  预定疏散路线
     */
    private String dELine;
    /**
     *  预定报警信号
     */
    private String dESignal;
    /**
     *  疏散命令发布人
     */
    private String dEvacuateMan;
    /**
     *  值班电话
     */
    private String dEvacuatePhone;
    /**
     *  抢、排险单位负责人
     */
    private String dExcludeMan;
    /**
     *  值班电话
     */
    private String dExcludePhone;
    /**
     *  治安保卫单位负责人
     */
    private String dSecurityMan;
    /**
     *  值班电话
     */
    private String dSecurityPhone;
    /**
     *  医疗救护单位为负责人
     */
    private String dDocMan;
    /**
     *  值班电话
     */
    private String dDocPhone;
    /**
     *  本卡发放单位（签章）
     */
    private String dGrantUnit;
    /**
     *  联系电话
     */
    private String dGrantPhone;
    /**
     *  日期
     */
    private String dGrantDate;
    /**
     *  持卡单位或个人
     */
    private String dHoldUnit;
    /**
     *  联系电话
     */
    private String dHoldPhone;
    /**
     *  日期
     */
    private String dHoldDate;
    /**
     *  灾害点编号(与tab_basic表dis_no关联）
     */
    private String dNo;
    
}
