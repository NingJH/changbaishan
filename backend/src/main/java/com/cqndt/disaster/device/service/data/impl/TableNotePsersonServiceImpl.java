package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabNotePersonMapper;
import com.cqndt.disaster.device.service.data.TabNotePersonService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabNotePersonVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @Author:hh
 * @Date:2019/8/5
 * @Description:com.cqndt.disaster.device.service.data.impl
 */
@Service
public class TableNotePsersonServiceImpl implements TabNotePersonService{

    @Autowired
    private TabNotePersonMapper tabNotePersonMapper;

    @Override
    public Result saveTabNotePerson(TabNotePersonVo tabNotePersonVo){

        String regx="^[1](([3|5|8][\\d])|([4][4,5,6,7,8,9])|([6][2,5,6,7])|([7][^9])|([9][1,8,9]))[\\d]{8}$";

        if("".equals(tabNotePersonVo.getLevel())){
            return new Result().failure(-1, "level不能为空");
        }
        if(StringUtils.isEmpty(tabNotePersonVo.getName())){
            return new Result().failure(-1, "人员姓名不能为空");
        }
        if("".equals(tabNotePersonVo.getType())){
            return new Result().failure(-1, "告警类型不能为空");
        }
        if(StringUtils.isEmpty(tabNotePersonVo.getPhone()) || tabNotePersonVo.getPhone().length()!=11){
            return new Result().failure(-1, "手机号不能为空或位数不正确");
        }

        if(!(tabNotePersonVo.getPhone().matches(regx))){
            return new Result().failure(-1, "手机号格式不正确");
        }

        Integer row=tabNotePersonMapper.saveTabNotePerson(tabNotePersonVo);

        if(row == null){
            return new Result().failure(-1, "人员信息未添加成功");
        }
        return new Result().success("添加成功");
    }

    @Override
    public List<TabNotePersonVo> queryTabNotePerson(TabNotePersonVo tabNotePersonVo){
        return tabNotePersonMapper.queryTabNotePerson(tabNotePersonVo);
    }

    @Override
    public int deleteTabNotePersonById(String id){
        return tabNotePersonMapper.deleteTabNotePersonById(id.split(","));
    }

    @Override
    public Result updateTabNotePersonById(TabNotePersonVo tabNotePersonVo){
        Integer row=tabNotePersonMapper.updateTabNotePersonById(tabNotePersonVo);
        if(row == null){
            return new Result().failure(-1, "人员信息未修改成功");
        }
        return new Result().success("修改成功");
    }

}
