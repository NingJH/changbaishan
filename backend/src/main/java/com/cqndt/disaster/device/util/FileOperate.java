package com.cqndt.disaster.device.util;

import java.io.*;

/**
 * Created By marc
 * Date: 2019/1/2  15:27
 * Description:文件操作
 */
public class FileOperate {
    /**
     * 复制文件内容
     * @param f1 原始文件
     * @param f2 目标文件
     * @throws IOException
     */
    public static void copy(File f1, File f2) throws IOException {
        // 检测是否存在目录
        if (!f2.getParentFile().exists()) {
            f2.getParentFile().mkdirs();
        }
        if(f2.exists()){
            return;
        }
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(f1));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(f2));
        int len = 0;
        int i = bis.available();
        byte[] b = new byte[i];
        while ((len = bis.read(b)) != -1) {
            bos.write(b, 0, len);
        }
        bis.close();
        bos.close();
    }
    /**
     * 删除文件
     * @param f
     */
    public static void del(File f) {
        f.delete();
    }

    public static void main(String[] args) throws Exception {
        // 读取文件和创建文件夹
        // 1、判断该文件下是否存在文件
        File f = new File("E:\\test\\img.jpg");// "OldPath"指旧的文件路径
        File f2 = new File("E:\\test\\file\\tabWorker\\img.jpg");// "NewPath"指新的文件路径
//        copy(f, f2);
//        System.out.println("复制完成");
        del(f2);
        System.out.println("删除完成");
    }
}
