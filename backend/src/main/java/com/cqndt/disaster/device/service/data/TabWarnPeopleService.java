package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabWarnPeople;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabBindWarnPeopleVo;
import com.cqndt.disaster.device.vo.data.TabWarnPeopleVo;

import java.util.List;
import java.util.Map;

/**
 * @author : laihan
 * @date : 2019/10/22
 * description :
 */
public interface TabWarnPeopleService {
    /**
     * 获取
     */

    /**
     * 根据当前选择的乡镇地区获取所属的灾害点（项目）
     */
    List<Map<String,Object>> getBasicByCode(Integer areaCode);

    /**
     * 外层大列表展示
     * @param vo
     * @return
     */
    Result selectList(TabWarnPeopleVo vo);

    Result addWarnPeople(TabWarnPeopleVo vo);

    Result peopleList(Integer id);

    Result updatePeople(TabWarnPeopleVo vo);

    Result delete(TabWarnPeopleVo vo);

    List<TabBindWarnPeopleVo> selectList1(TabBindWarnPeopleVo tabBindWarnPeopleVo);

    List<TabWarnPeople> allWarnPeople();

}
