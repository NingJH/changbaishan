package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabStaticTypeMapper;
import com.cqndt.disaster.device.entity.data.TabStaticType;
import com.cqndt.disaster.device.service.data.TabStaticTypeService;
import com.cqndt.disaster.device.vo.data.TabStaticTypeVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TabStaticTypeServiceImpl implements TabStaticTypeService {
    private static final Logger logger = LoggerFactory.getLogger(TabStaticTypeServiceImpl.class);

    @Autowired
    private TabStaticTypeMapper tabStaticTypeMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void save(TabStaticTypeVo tabStaticTypeVo) throws Exception {
        if (null != tabStaticTypeVo) {
            tabStaticTypeMapper.save(tabStaticTypeVo);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(TabStaticTypeVo tabStaticTypeVo) throws Exception {
        tabStaticTypeMapper.update(tabStaticTypeVo);

    }

    @Override
    public TabStaticTypeVo getTabStaticTypeById(String id) {
        return tabStaticTypeMapper.getTabStaticTypeById(id);
    }

    @Override
    public void delete(String id) {
        String[] ids = id.split(",");
        for (String idStr : ids) {
            TabStaticTypeVo vo = tabStaticTypeMapper.getTabStaticTypeById(idStr);
            if (null != vo) {
                tabStaticTypeMapper.delete(idStr);
            }
        }
    }

    @Override
    public List<TabStaticTypeVo> list(TabStaticTypeVo tabStaticTypeVo) {
        return tabStaticTypeMapper.list(tabStaticTypeVo);
    }

    @Override
    public List<TabStaticType> getStaticByNum(Integer staticNum) {
        return tabStaticTypeMapper.getStaticByNum(staticNum);
    }

    @Override
    public List<TabStaticTypeVo> getStaticTypeList() {
        return tabStaticTypeMapper.getStaticTypeList();
    }

    @Override
    public List<TabStaticType> getByStaticType(Long staticNum) {
        return tabStaticTypeMapper.getByStaticType(staticNum);
    }


}