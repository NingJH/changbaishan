package com.cqndt.disaster.device.service.data.impl;

import com.alibaba.fastjson.JSONObject;
import com.cqndt.disaster.device.dao.data.TabPushMsgMapper;
import com.cqndt.disaster.device.entity.data.SysPushmsg;
import com.cqndt.disaster.device.entity.data.TabProject;
import com.cqndt.disaster.device.entity.data.TabPushMsg;
import com.cqndt.disaster.device.influxdb.InfluxDBConnect;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 数据同步服务类
 *
 * @author liaohonglai
 * @date 2019/7/29
 */
@Service
@Slf4j
public class TabPushMsgServiceImpl {

    /**
     * 注入restTemplate Bean
     */
    @Autowired
    public RestTemplate restConfig;
    /**
     * token
     */
    private String token;
    /**
     * 状态 0 成功
     */
    private String status = "1";

    @Autowired
    private InfluxDBConnect influxDBConnect;

    @Autowired
    private TabPushMsgMapper tpmMapper;

    private final SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    //地裂缝监测点编号
    private Map<String,String> maps = new HashMap<>();

    /**
     * 定时刷新token,token有效时间20分钟，定时17分钟执行一次
     */
//    @Scheduled(fixedRate = 1000 * 60 * 17)
    private void getToken() {
        //token地址  183.224.17.135:1080
        String url = "http://192.168.2.127:80/api/api/security/token?username=datasync&password=datasync123456";
        //发送请求
        token(url);
        //不为成功状态-重试
        if (!"0".equals(status)) {
            //请求重试3次
            for (int retry = 3; retry > 0; retry--) {
                token(url);
                if ("0".equals(status)) {
                    break;
                }
            }
        }
    }

    /**
     * 获取token请求发送
     *
     * @param url 地址
     */
    private void token(String url) {
        //返回json数据
        JSONObject jsonObject = restConfig.postForObject(url, null, JSONObject.class);
        //判断jsonObject是否为null
        if (jsonObject == null) {
            log.info("【TabPushMsgServiceImpl】类getToken方法获取token返回值为null");
            return;
        }
        //获取状态码-0成功
        String st = jsonObject.getString("status");
        //判断状态码，赋值token
        if ("0".equals(st)) {
            status = st;
            token = jsonObject.getString("token");
        }
        status = st;
    }

    /**
     * 发送同步数据-雨量
     */
//    @Scheduled(initialDelay = 5000, fixedRate = 60000)
    private void pushMsgYl() {
        List<TabPushMsg> list = new ArrayList<>();
        SysPushmsg myConfig = tpmMapper.getYLConfig("YL");
        //时间
        String ylTime = spf.format(myConfig.getTime());
        //获取雨量数据
        List<TabPushMsg> yl = tpmMapper.getYl(ylTime, myConfig.getMaxid());
        if (yl != null && yl.size() > 0) {
            TabPushMsg tb = new TabPushMsg();
            //存储同一时间不同的结果
            Set<String> set = new HashSet<>();
            //循环数据
            for (int i = 0; i < yl.size(); i++) {
                //第一条数据
                if (i == 0) {
                    set.add(yl.get(i).getV1());
                    BeanUtils.copyProperties(yl.get(i), tb);
                    continue;
                }
                //同一时间,同一设备的数据
                if (tb.getTime().equals(yl.get(i).getTime()) && tb.getSenserNo().equals(yl.get(i).getSenserNo())) {
                    set.add(yl.get(i).getV1());
                    if (i != yl.size() - 1)
                        continue;
                }
                //如果同一个时间只对应一个监测值，只需要填v1，余下v2~v6留空或者不写即可
                for (Iterator it2 = set.iterator(); it2.hasNext(); ) {
                    if ("".equals(tb.getV1())) {
                        tb.setV1(it2.next().toString());
                        continue;
                    }
                    if ("".equals(tb.getV2())) {
                        tb.setV2(it2.next().toString());
                        continue;
                    }
                    if ("".equals(tb.getV3())) {
                        tb.setV3(it2.next().toString());
                        continue;
                    }
                    if ("".equals(tb.getV4())) {
                        tb.setV4(it2.next().toString());
                        continue;
                    }
                    if ("".equals(tb.getV5())) {
                        tb.setV5(it2.next().toString());
                        continue;
                    }
                    if ("".equals(tb.getV6())) {
                        tb.setV6(it2.next().toString());
                        continue;
                    }

                }
                list.add(tb);
                //清空结果
                set.clear();
                //如果时间不同，重新复制对象
                BeanUtils.copyProperties(yl.get(i), tb);
                //获取最后一条id
                if (i == yl.size() - 1) {
                    myConfig.setMaxid(yl.get(i).getId());
                }
            }
        }
        //存储时间
        try {
            Date parse = spf.parse(ylTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(parse);
            calendar.add(Calendar.HOUR_OF_DAY, 24);
            if (System.currentTimeMillis() > calendar.getTime().getTime()) ;
            {
                myConfig.setTime(calendar.getTime());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //更新时间标记配置
        int i = tpmMapper.updateSysPushMsg(myConfig);
        if (i<0){
            log.error("【tabPushMsgServiceImpl】雨量更新时间失败");
            return;
        }
        //调用发送请求方法
        JSONObject body = sendMsg(list);
        if (body.getInteger("status")==0){
            log.info("【tabPushMsgServiceImpl】雨量更新数据{}条",body.getInteger("count"));
        }else {
            log.error("【tabPushMsgServiceImpl】雨量更新数据失败：{}",body.getString("msg"));
        }
    }

    /**
     * 发送同步数据-裂缝
     */
//    @Scheduled(initialDelay = 15000, fixedRate = 60000)
    private void pushMsgLf() throws ParseException {
        SysPushmsg lfX = tpmMapper.getYLConfig("LF_WY");
        String X = "select time,sensor_no,value  from tbs_liefeng_lx where time >'"+spf.format(lfX.getTime())+"' limit 100";
        List<Map<String, Object>> lf_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(X));
        List<TabPushMsg> list = new ArrayList<>();
        String time = "";
        for (Map map : lf_x){
            TabPushMsg tabPushMsg = new TabPushMsg();
            tabPushMsg.setSensorType("LF");
            String sensorNo = String.valueOf(map.get("sensor_no"));
            time = String.valueOf(map.get("time")).replace("T", " ").replace("Z", "");
            if (!maps.containsKey(sensorNo)){
                String moniterNo = tpmMapper.getMoniterNo(sensorNo);
                if (StringUtils.isEmpty(moniterNo)){
                    continue;
                }
                maps.put(sensorNo, moniterNo);
            }
            tabPushMsg.setSensorId(maps.get(sensorNo));
            tabPushMsg.setTime(time);
            tabPushMsg.setV1(String.valueOf(map.get("value")));
            list.add(tabPushMsg);
        }
        //更新时间标记配置
        if (!time.isEmpty()){
            lfX.setTime(spf.parse(time));
        }
        int i = tpmMapper.updateSysPushMsg(lfX);
        if (i<0){
            log.error("【tabPushMsgServiceImpl】裂缝更新时间失败");
            return;
        }
        //调用发送请求方法
        JSONObject body = sendMsg(list);
        if (body.getInteger("status")==0){
            log.info("【tabPushMsgServiceImpl】裂缝更新数据{}条",body.getInteger("count"));
        }else {
            log.error("【tabPushMsgServiceImpl】裂缝更新数据失败：{}",body.getString("msg"));
        }
    }


    /**
     * 发送同步数据-gnss
     */
//    @Scheduled(initialDelay = 25000, fixedRate = 60000)
    public void pushMsgGnss(){
        List<TabPushMsg> list = new ArrayList<>();
        SysPushmsg myConfig = tpmMapper.getYLConfig("GP");
        String time = spf.format(myConfig.getTime());
        List<TabPushMsg> gnss = tpmMapper.getGnss(time, myConfig.getMaxid());
        for (TabPushMsg tabPushMsg : gnss) {
            double sqrt = Math.sqrt(Math.pow(Double.valueOf(tabPushMsg.getV1()), 2) + Math.pow(Double.valueOf(tabPushMsg.getV2()), 2) + Math.pow(Double.valueOf(tabPushMsg.getV3()), 2));
            tabPushMsg.setV4(String.format("%.2f", sqrt));
            list.add(tabPushMsg);
            myConfig.setMaxid(tabPushMsg.getId());
        }
        //存储时间
        try {
            Date parse = spf.parse(time);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(parse);
            calendar.add(Calendar.HOUR_OF_DAY, 24);
            if (System.currentTimeMillis() > calendar.getTime().getTime()) ;
            {
                myConfig.setTime(calendar.getTime());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //更新时间标记配置
        int i = tpmMapper.updateSysPushMsg(myConfig);
        if (i<0){
            log.error("【tabPushMsgServiceImpl】gnss更新时间失败");
            return;
        }
        //调用发送请求方法
        JSONObject body = sendMsg(list);
        if (body.getInteger("status")==0){
            log.info("【tabPushMsgServiceImpl】gnss更新数据{}条",body.getInteger("count"));
        }else {
            log.error("【tabPushMsgServiceImpl】gnss更新数据失败：{}",body.getString("msg"));
        }
    }

    /**
     * 发送同步数据-高精度倾斜 QJ/坡浅 QP
     */
//    @Scheduled(initialDelay = 35000, fixedRate = 60000)
    public void pushMsgQj() throws ParseException {
        SysPushmsg gxX = tpmMapper.getYLConfig("QJ_X");
        SysPushmsg gxY = tpmMapper.getYLConfig("QJ_Y");
        SysPushmsg gxZ = tpmMapper.getYLConfig("QJ_Z");

        String gxx = "select time,sensor_no,value from tbs_dm_qxy_x where time >'"+spf.format(gxX.getTime())+"' limit 100";
        String gxy = "select time,sensor_no,value from tbs_dm_qxy_y where time >'"+spf.format(gxY.getTime())+"' limit 100";
        String gxz = "select time,sensor_no,value from tbs_dm_qxy_z where time >'"+spf.format(gxZ.getTime())+"' limit 100";

        List<Map<String, Object>> gx_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(gxx));
        List<Map<String, Object>> gx_y = influxDBConnect.queryResultToListMap(influxDBConnect.query(gxy));
        List<Map<String, Object>> gx_z = influxDBConnect.queryResultToListMap(influxDBConnect.query(gxz));
        List<TabPushMsg> list = new ArrayList<>();
        String time1="";
        for (Map map : gx_x){
            TabPushMsg tabPushMsg = new TabPushMsg();
            tabPushMsg.setSensorType("QJ");
            String sensorNo = String.valueOf(map.get("sensor_no"));
            time1 = String.valueOf(map.get("time")).replace("T", " ").replace("Z", "");
            if (!maps.containsKey(sensorNo)){
                String moniterNo = tpmMapper.getMoniterNo(sensorNo);
                if (StringUtils.isEmpty(moniterNo)){
                    continue;
                }
                maps.put(sensorNo, moniterNo);
            }
            tabPushMsg.setSensorId(maps.get(sensorNo));

            tabPushMsg.setTime(time1);
            tabPushMsg.setV1(String.valueOf(map.get("value")));
            list.add(tabPushMsg);
        }

        String time2="";
        for (Map map : gx_y){
            TabPushMsg tabPushMsg = new TabPushMsg();
            tabPushMsg.setSensorType("QJ");
            String sensorNo = String.valueOf(map.get("sensor_no"));
            time2 = String.valueOf(map.get("time")).replace("T", " ").replace("Z", "");
            if (!maps.containsKey(sensorNo)){
                String moniterNo = tpmMapper.getMoniterNo(sensorNo);
                if (StringUtils.isEmpty(moniterNo)){
                    continue;
                }
                maps.put(sensorNo, moniterNo);
            }
            String sensor = maps.get(sensorNo);
            tabPushMsg.setSensorId(sensor.substring(0, 2)+(Integer.valueOf(sensor.substring(2))+1));
            tabPushMsg.setTime(time2);
            tabPushMsg.setV1(String.valueOf(map.get("value")));
            list.add(tabPushMsg);
        }

        String time3="";
        for (Map map : gx_z){
            TabPushMsg tabPushMsg = new TabPushMsg();
            tabPushMsg.setSensorType("QJ");
            String sensorNo = String.valueOf(map.get("sensor_no"));
            time3 = String.valueOf(map.get("time")).replace("T", " ").replace("Z", "");
            if (!maps.containsKey(sensorNo)){
                String moniterNo = tpmMapper.getMoniterNo(sensorNo);
                if (StringUtils.isEmpty(moniterNo)){
                    continue;
                }
                maps.put(sensorNo, moniterNo);
            }
            String sensor = maps.get(sensorNo);
            tabPushMsg.setSensorId(sensor.substring(0, 2)+(Integer.valueOf(sensor.substring(2))+2));
            tabPushMsg.setTime(time3);
            tabPushMsg.setV1(String.valueOf(map.get("value")));
            list.add(tabPushMsg);
        }

        //更新时间标记配置
        if (!time1.isEmpty()) {
            gxX.setTime(spf.parse(time1));
        }
        if (!time2.isEmpty()) {
            gxY.setTime(spf.parse(time2));
        }
        if (!time3.isEmpty()) {
            gxZ.setTime(spf.parse(time3));
        }
        int x = tpmMapper.updateSysPushMsg(gxX);
        int y = tpmMapper.updateSysPushMsg(gxY);
        int z = tpmMapper.updateSysPushMsg(gxZ);
        if (x<0 || y<0 || z<0){
            log.error("【tabPushMsgServiceImpl】高精度倾斜更新时间失败");
            return;
        }
        //调用发送请求方法
        JSONObject body = sendMsg(list);
        if (body.getInteger("status")==0){
            log.info("【tabPushMsgServiceImpl】高精度倾斜更新数据{}条",body.getInteger("count"));
        }else {
            log.error("【tabPushMsgServiceImpl】高精度倾斜更新数据失败：{}",body.getString("msg"));
        }
    }

    /**
     * 发送同步数据-坡浅 QP
     */
//    @Scheduled(initialDelay = 45000, fixedRate = 60000)
    public void pushMsgQp() throws ParseException {
        SysPushmsg qpX = tpmMapper.getYLConfig("QP_X");
        SysPushmsg qpY = tpmMapper.getYLConfig("QP_Y");
        SysPushmsg qpZ = tpmMapper.getYLConfig("QP_Z");
        SysPushmsg qpA = tpmMapper.getYLConfig("QP_A");
        SysPushmsg qpB = tpmMapper.getYLConfig("QP_B");
        SysPushmsg qpC = tpmMapper.getYLConfig("QP_C");

        String qpx = "select time,sensor_no,value from tbs_ptqcbxy_lx_x where time >'"+spf.format(qpX.getTime())+"' limit 100";
        String qpy = "select time,sensor_no,value from tbs_ptqcbxy_lx_y where time >'"+spf.format(qpY.getTime())+"' limit 100";
        String qpz = "select time,sensor_no,value from tbs_ptqcbxy_lx_z where time >'"+spf.format(qpZ.getTime())+"' limit 100";
        String qpa = "select time,sensor_no,value from tbs_ptqcbxy_wy_x where time >'"+spf.format(qpA.getTime())+"' limit 100";
        String qpb = "select time,sensor_no,value from tbs_ptqcbxy_wy_y where time >'"+spf.format(qpB.getTime())+"' limit 100";
        String qpc = "select time,sensor_no,value from tbs_ptqcbxy_wy_z where time >'"+spf.format(qpC.getTime())+"' limit 100";

        List<Map<String, Object>> qp_x = influxDBConnect.queryResultToListMap(influxDBConnect.query(qpx));
        List<Map<String, Object>> qp_y = influxDBConnect.queryResultToListMap(influxDBConnect.query(qpy));
        List<Map<String, Object>> qp_z = influxDBConnect.queryResultToListMap(influxDBConnect.query(qpz));
        List<Map<String, Object>> qp_a = influxDBConnect.queryResultToListMap(influxDBConnect.query(qpa));
        List<Map<String, Object>> qp_b = influxDBConnect.queryResultToListMap(influxDBConnect.query(qpb));
        List<Map<String, Object>> qp_c = influxDBConnect.queryResultToListMap(influxDBConnect.query(qpc));

        List<TabPushMsg> list = new ArrayList<>();
        String time1="";
        for (Map map : qp_x){
            TabPushMsg tabPushMsg = new TabPushMsg();
            tabPushMsg.setSensorType("QP");
            String sensorNo = String.valueOf(map.get("sensor_no"));
            time1 = String.valueOf(map.get("time")).replace("T", " ").replace("Z", "");
            if (!maps.containsKey(sensorNo)){
                String moniterNo = tpmMapper.getMoniterNo(sensorNo);
                if (StringUtils.isEmpty(moniterNo)){
                    continue;
                }
                maps.put(sensorNo, moniterNo);
            }
            tabPushMsg.setSensorId(maps.get(sensorNo));
            tabPushMsg.setTime(time1);
            tabPushMsg.setV1(String.valueOf(map.get("value")));
            list.add(tabPushMsg);
        }

        String time2="";
        for (Map map : qp_y){
            TabPushMsg tabPushMsg = new TabPushMsg();
            tabPushMsg.setSensorType("QP");
            String sensorNo = String.valueOf(map.get("sensor_no"));
            time2 = String.valueOf(map.get("time")).replace("T", " ").replace("Z", "");
            if (!maps.containsKey(sensorNo)){
                String moniterNo = tpmMapper.getMoniterNo(sensorNo);
                if (StringUtils.isEmpty(moniterNo)){
                    continue;
                }
                maps.put(sensorNo, moniterNo);
            }
            String sensor = maps.get(sensorNo);
            tabPushMsg.setSensorId(sensor.substring(0, 2)+(Integer.valueOf(sensor.substring(2))+1));

            tabPushMsg.setTime(time2);
            tabPushMsg.setV1(String.valueOf(map.get("value")));
            list.add(tabPushMsg);
        }

        String time3="";
        for (Map map : qp_z){
            TabPushMsg tabPushMsg = new TabPushMsg();
            tabPushMsg.setSensorType("QP");
            String sensorNo = String.valueOf(map.get("sensor_no"));
            time3 = String.valueOf(map.get("time")).replace("T", " ").replace("Z", "");
            if (!maps.containsKey(sensorNo)){
                String moniterNo = tpmMapper.getMoniterNo(sensorNo);
                if (StringUtils.isEmpty(moniterNo)){
                    continue;
                }
                maps.put(sensorNo, moniterNo);
            }
            String sensor = maps.get(sensorNo);
            tabPushMsg.setSensorId(sensor.substring(0, 2)+(Integer.valueOf(sensor.substring(2))+2));

            tabPushMsg.setTime(time3);
            tabPushMsg.setV1(String.valueOf(map.get("value")));
            list.add(tabPushMsg);
        }

        String time4 = "";
        for (Map map : qp_a){
            TabPushMsg tabPushMsg = new TabPushMsg();
            tabPushMsg.setSensorType("QP");
            String sensorNo = String.valueOf(map.get("sensor_no"));
            time4 = String.valueOf(map.get("time")).replace("T", " ").replace("Z", "");
            if (!maps.containsKey(sensorNo)){
                String moniterNo = tpmMapper.getMoniterNo(sensorNo);
                if (StringUtils.isEmpty(moniterNo)){
                    continue;
                }
                maps.put(sensorNo, moniterNo);
            }
            String sensor = maps.get(sensorNo);
            tabPushMsg.setSensorId(sensor.substring(0, 2)+(Integer.valueOf(sensor.substring(2))+3));
            tabPushMsg.setTime(time4);
            tabPushMsg.setV1(String.valueOf(map.get("value")));
            list.add(tabPushMsg);
        }

        String time5 = "";
        for (Map map : qp_b){
            TabPushMsg tabPushMsg = new TabPushMsg();
            tabPushMsg.setSensorType("QP");
            String sensorNo = String.valueOf(map.get("sensor_no"));
            time5 = String.valueOf(map.get("time")).replace("T", " ").replace("Z", "");
            if (!maps.containsKey(sensorNo)){
                String moniterNo = tpmMapper.getMoniterNo(sensorNo);
                if (StringUtils.isEmpty(moniterNo)){
                    continue;
                }
                maps.put(sensorNo, moniterNo);
            }
            String sensor = maps.get(sensorNo);
            tabPushMsg.setSensorId(sensor.substring(0, 2)+(Integer.valueOf(sensor.substring(2))+4));
            tabPushMsg.setTime(time5);
            tabPushMsg.setV1(String.valueOf(map.get("value")));
            list.add(tabPushMsg);
        }

        String time6 = "";
        for (Map map : qp_c){
            TabPushMsg tabPushMsg = new TabPushMsg();
            tabPushMsg.setSensorType("QP");
            String sensorNo = String.valueOf(map.get("sensor_no"));
            time6 = String.valueOf(map.get("time")).replace("T", " ").replace("Z", "");
            if (!maps.containsKey(sensorNo)){
                String moniterNo = tpmMapper.getMoniterNo(sensorNo);
                if (StringUtils.isEmpty(moniterNo)){
                    continue;
                }
                maps.put(sensorNo, moniterNo);
            }
            String sensor = maps.get(sensorNo);
            tabPushMsg.setSensorId(sensor.substring(0, 2)+(Integer.valueOf(sensor.substring(2))+5));
            tabPushMsg.setTime(time6);
            tabPushMsg.setV1(String.valueOf(map.get("value")));
            list.add(tabPushMsg);
        }

        //更新时间标记配置
        if (!time1.isEmpty()) {
            qpX.setTime(spf.parse(time1));
        }
        if (!time2.isEmpty()) {
            qpY.setTime(spf.parse(time2));
        }
        if (!time3.isEmpty()) {
            qpZ.setTime(spf.parse(time3));
        }
        if (!time4.isEmpty()) {
            qpA.setTime(spf.parse(time4));
        }
        if (!time5.isEmpty()) {
            qpB.setTime(spf.parse(time5));
        }
        if (!time6.isEmpty()) {
            qpC.setTime(spf.parse(time6));
        }
        int x = tpmMapper.updateSysPushMsg(qpX);
        int y = tpmMapper.updateSysPushMsg(qpY);
        int z = tpmMapper.updateSysPushMsg(qpZ);
        int a = tpmMapper.updateSysPushMsg(qpA);
        int b = tpmMapper.updateSysPushMsg(qpB);
        int c = tpmMapper.updateSysPushMsg(qpC);
        if (x<0||y<0||z<0||a<0||b<0||c<0){
            log.error("【tabPushMsgServiceImpl】坡浅更新时间失败");
            return;
        }
        //调用发送请求方法
        JSONObject body = sendMsg(list);
        if (body.getInteger("status")==0){
            log.info("【tabPushMsgServiceImpl】坡浅更新数据{}条",body.getInteger("count"));
        }else {
            log.error("【tabPushMsgServiceImpl】坡浅更新数据失败：{}",body.getString("msg"));
        }
    }

    /**
     * 发送请求
     * @param list 数据
     */
    public JSONObject sendMsg(List<TabPushMsg> list){
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        requestHeaders.add("token", token);
        //body
        Map<String, Object> requestBody = new HashMap<>();
        requestBody.put("dataSet", list);
        //HttpEntity
        HttpEntity<Map> requestEntity = new HttpEntity<>(requestBody, requestHeaders);
        //post
        ResponseEntity<JSONObject> responseEntity = restConfig.postForEntity("http://192.168.2.127:80/api/api/datasync/push", requestEntity, JSONObject.class);
        JSONObject body = responseEntity.getBody();
        return body;
    }

//
//    public static void main(String[] args) throws ParseException {
//         SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date parse = spf.parse("2019-08-08 17:20:00");
//        Calendar calendar = Calendar.getInstance();
//        calendar.setTime(parse);
//        calendar.add(Calendar.HOUR_OF_DAY, 24);
//        System.out.println(System.currentTimeMillis());
//        System.out.println(calendar.getTime().getTime());
//        if (System.currentTimeMillis() > calendar.getTime().getTime())
//        {
//            System.out.println("da");
//        }else{
//            System.out.println("xiao");
//        }
//    }

}
