package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabStaticType;
import lombok.Data;

/**
* Description: 
* Date: 2019-06-04
*/
@Data
public class TabStaticTypeVo extends TabStaticType {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;


}
