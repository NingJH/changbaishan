package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabAttachment;
import lombok.Data;

@Data
public class TabAttachmentVo extends TabAttachment {

    /*
    绝对路径
     */
    private String viewUrl;
}
