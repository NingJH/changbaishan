package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabBasic;
import com.cqndt.disaster.device.vo.data.TabBasicVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * Description:
 * Date: 2019-06-04
 */

@Mapper
public interface TabBasicMapper {
    /**
     * 根据条件查询list
     *
     * @param tabBasicVo 查询条件
     */
    List<Map<String, Object>> list(TabBasicVo tabBasicVo);

    /**
     * 增加
     *
     * @param tabBasicVo 条件
     */
    int save(TabBasicVo tabBasicVo);

    /**
     * 修改
     *
     * @param tabBasicVo 条件
     */
    int update(TabBasicVo tabBasicVo);

    /**
     * 删除
     *
     * @param id
     */
    int delete(Integer id);

    /**
     * 根据id查询单个记录
     *
     * @param id
     */
    TabBasicVo getTabBasicById(String id);

    /**
     * 关联表删除
     */
    int delete1(String param1);

    /**
     * 关联表删除
     */
    int deleteTabBasicHuman(String diasaterNo);

    /**
     * 中间表保存
     */
    int save1(@Param("param1") String param1, @Param("param2") String param2);

    /**
     * 中间表保存
     */
    int saveTabBasicHuman(@Param("diasaterNo") String diasaterNo, @Param("humanId") String humanId);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabDisasterCardAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabHedgeCardAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabPlanInfoAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabAreaAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabHumanAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabPhotographyAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabAttachmentAll(TabBasicVo tabBasicVo);

    /**
     * 已选子表信息
     */
    List<TabBasicVo> listTabDisasterCardSelect(Integer id);

    /**
     * 已选子表信息
     */
    List<TabBasicVo> listTabHumanSelect(Integer id);

    Map<String, Object> listTabAreaInfo(String areaId);

    List<Map<String, Object>> listTabAreaInfoByAreaParent(String id);

    List<Map<String, Object>> listTabAreaInfoByAreaId(String id);

    /**
     * 项目下拉框选择灾害点
     *
     * @return
     */
    List<Map<String, Object>> getBasicForProject(String id);

    List<Map<String, Object>> listTabPerson(Map<String, Object> personId);

    /**
     * 根据灾害点编号查询
     * @param disNo
     * @return
     */
    List<Map<String,Object>> getBaseForDisNo(String disNo);

    /**
     * 根据地区编号查询所有下级地区编号
     * @return
     */
    String getAreaById(@Param("areaParent") String areaParent);

    List<TabBasicVo> getBasicDisNoList();

    List<TabBasic> findAllBasic();
}
