package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabStaticType;
import com.cqndt.disaster.device.vo.data.TabStaticTypeVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface TabStaticTypeService {
    /**
     * 增加
     * @param tabStaticTypeVo
     */
    void save(TabStaticTypeVo tabStaticTypeVo) throws Exception;
     /**
     * 修改
     * @param tabStaticTypeVo
     */
     void update(TabStaticTypeVo tabStaticTypeVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabStaticTypeVo
     */
    TabStaticTypeVo getTabStaticTypeById(String id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabStaticTypeVo 查询条件
     * @return 数据列表
     */
    List<TabStaticTypeVo> list(TabStaticTypeVo tabStaticTypeVo);

    List<TabStaticType> getStaticByNum(Integer staticNum);

    /**
     * 获取静态值类型列表
     * @return
     */
    List<TabStaticTypeVo> getStaticTypeList();

    /**
     * 根据静态值类型值查询
     */
    List<TabStaticType> getByStaticType(@Param("staticNum")Long staticNum);
}