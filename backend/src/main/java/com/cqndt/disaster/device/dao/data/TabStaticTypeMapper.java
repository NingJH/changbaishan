package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabStaticType;
import com.cqndt.disaster.device.vo.data.TabStaticTypeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * Description: 
 * Date: 2019-06-04
 */

@Mapper
public interface TabStaticTypeMapper {
    /**
     * 根据条件查询list
     * @param tabStaticTypeVo 查询条件
     */
    List<TabStaticTypeVo> list(TabStaticTypeVo tabStaticTypeVo);
    /**
     * 增加
     * @param tabStaticTypeVo 条件
     */
    int save(TabStaticTypeVo tabStaticTypeVo);
    /**
     * 修改
     * @param tabStaticTypeVo 条件
     */
    int update(TabStaticTypeVo tabStaticTypeVo);
    /**
     * 删除
     * @param id
     */
    int delete(@Param("id")String id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    TabStaticTypeVo getTabStaticTypeById(@Param("id")String id);
     /**
      * 根据id查询是否有下级menu
      * @param id
      */
     TabStaticTypeVo getTabStaticTypeSub(@Param("id")String id);

    List<TabStaticType> getStaticByNum(@Param("staticNum")Integer staticNum);

    /**
     * 获取静态值类型列表
     * @return
     */
    List<TabStaticTypeVo> getStaticTypeList();

    String getDeviceTypeName(String deviceType);

    /**
     * 根据静态值类型值查询
     */
    List<TabStaticType> getByStaticType(@Param("staticNum")Long staticNum);
}
