package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabPerson;
import lombok.Data;

@Data
public class TabPersonVo extends TabPerson {

    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;
    /**
     *人员类型
     */
    private String personType;
    /**
     * 公司名称
     */
    private String unitName;
    /**
     * 公司类型
     */
    private String unitType;
}
