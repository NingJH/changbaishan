package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabNotePerson;

/**
 * @version 1.0
 * @Author:hh
 * @Date:2019/8/6
 * @Description:com.cqndt.disaster.device.vo.data
 */
public class TabNotePersonVo extends TabNotePerson {
    /**
     * 显示页数
     */
    private Integer page=1;
    /**
     * 每页显示条数
     */
    private  Integer limit=10;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
