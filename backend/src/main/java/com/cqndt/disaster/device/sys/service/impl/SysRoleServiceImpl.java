package com.cqndt.disaster.device.sys.service.impl;

import com.cqndt.disaster.device.sys.dao.SysRoleMapper;
import com.cqndt.disaster.device.sys.service.SysRoleService;
import com.cqndt.disaster.device.sys.vo.SysRoleVo;
import com.cqndt.disaster.device.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class SysRoleServiceImpl implements SysRoleService {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(SysRoleVo sysRoleVo) throws Exception {
        sysRoleVo.setCreateTime(new Date());
        sysRoleMapper.save(sysRoleVo);
        if(null != sysRoleVo.getLabelName1() && !"".equals(sysRoleVo.getLabelName1())){
            String[] labelName1s = sysRoleVo.getLabelName1().split(",");
            for (String labelName1:labelName1s) {
                sysRoleMapper.saveTabRoleMenu(sysRoleVo.getId().toString(),labelName1);
            }
        }
    
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void update(SysRoleVo sysRoleVo) throws Exception {
        sysRoleMapper.update(sysRoleVo);
        sysRoleMapper.deleteTabRoleMenu(sysRoleVo.getId().toString());
        if(null != sysRoleVo.getLabelName1() && !"".equals(sysRoleVo.getLabelName1())){
             String[] labelName1s = sysRoleVo.getLabelName1().split(",");
             for (String labelName1:labelName1s) {
                 sysRoleMapper.saveTabRoleMenu(sysRoleVo.getId().toString(),labelName1);
             }
        }
   
   }
    @Override
    public Result checkValidate(SysRoleVo sysRoleVo,Integer flag){
        Result result = new Result();
        boolean isUpdate = false;
        if(flag == 1){
            SysRoleVo vo = sysRoleMapper.getTabRoleById(sysRoleVo.getId());
            isUpdate = vo.getRoleName().equals(sysRoleVo.getRoleName());
        }
        if(!isUpdate){
        int num = sysRoleMapper.checkValidate1(sysRoleVo);
            if(num>0){
                return result.failure(-2,"角色名称不能重复");
            }
        }
        return result;
    }
    @Override
    public SysRoleVo getTabRoleById(Long id){
        SysRoleVo vo = sysRoleMapper.getTabRoleById(id);
        return vo;
    }

    @Override
    public List<String> getCheckedMenuById(Long id,String frontBack) {
        //获取当前角色已被分配的权限、角色
        List<String> list = sysRoleMapper.getMenuByRoleId(id.toString(),frontBack);
        return list;
    }

    @Override
    public void delete(String id){
         String[] ids = id.split(",");
         for (String idStr:ids) {
             sysRoleMapper.delete(Integer.parseInt(idStr));
             //删除角色分配的菜单、权限
             sysRoleMapper.deleteTabRoleMenu(idStr);
         }
    }
    @Override
    public List<SysRoleVo> list(SysRoleVo sysRoleVo) {
        if(null != sysRoleVo.getStartCondition1()){
            sysRoleVo.setStartCondition1(sysRoleVo.getStartCondition1()+" 00:00:00");
        }
        if(null != sysRoleVo.getEndCondition1()){
            sysRoleVo.setEndCondition1(sysRoleVo.getEndCondition1()+" 23:59:59");
        }
        return  sysRoleMapper.list(sysRoleVo);
    }
    @Override
    public List<Map<String,Object>> listTabDept(SysRoleVo sysRoleVo){
        return  sysRoleMapper.listTabDept(sysRoleVo);
    }

}