package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.vo.data.TabPlanInfoVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


/**
 * Description: 表备注
 * Date: 2019-06-11
 */

@Mapper
public interface TabPlanInfoMapper {
    /**
     * 根据条件查询list
     * @param tabPlanInfoVo 查询条件
     */
    List<TabPlanInfoVo> list(TabPlanInfoVo tabPlanInfoVo);
    /**
     * 增加
     * @param tabPlanInfoVo 条件
     */
    int save(TabPlanInfoVo tabPlanInfoVo);
    /**
     * 修改
     * @param tabPlanInfoVo 条件
     */
    int update(TabPlanInfoVo tabPlanInfoVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    Map<String,Object> getTabPlanInfoById(Long id);


}
