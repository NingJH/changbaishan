package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabUnit;
import lombok.Data;

import java.util.List;

@Data
public class TabUnitVo extends TabUnit {

    /**
     * 页数
     */
    private Integer page = 1;
    /**
     * 条数
     */
    private Integer limit = 10;

    /**
     * 单位类型
     */
    private String staticName;

    /**
     * 厂商、单位类型
     */
    private Integer vendorId;

    /**
     * 单位所属地区
     */
    private String unitAddress;

    /**
     * 项目id
     */
    private Integer projectId;

    private List<TabArea> list;
}
