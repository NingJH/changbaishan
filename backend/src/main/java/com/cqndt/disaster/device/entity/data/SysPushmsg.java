package com.cqndt.disaster.device.entity.data;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 理工大数据标记配置表
 * @author liaohonglai
 * @date 2019/7/31
 */
@Getter
@Setter
public class SysPushmsg {
    /**
     * GP gps监测，YL 雨量监测 ,QX 表面倾斜监测,LF 地表裂缝监测
     */
    private String type;

    /**
     * 最新取值时间
     */
    private Date time;

    /**
     * 最大id
     */
    private Integer maxid;
}
