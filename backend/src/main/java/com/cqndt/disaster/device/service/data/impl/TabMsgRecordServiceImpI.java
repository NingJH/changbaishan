package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabMsgRecordMapper;
import com.cqndt.disaster.device.entity.data.TabMsgRecord;
import com.cqndt.disaster.device.service.data.TabMsgRecordService;
import com.cqndt.disaster.device.sys.dao.SysMenuMapper;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabMsgRecordVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TabMsgRecordServiceImpI implements TabMsgRecordService {
    @Autowired
    private TabMsgRecordMapper tabMsgRecordMapper;

    /*
     *根据参数动态查询
     * @parm tabMsgRecord,page,limit
     * @return
     * */
    @Override
    public Result showCondition(TabMsgRecordVo tabMsgRecordVo) {
        PageHelper.startPage(tabMsgRecordVo.getPage(), tabMsgRecordVo.getLimit());
        PageInfo<TabMsgRecord>p= new PageInfo<>(tabMsgRecordMapper.showCondition(tabMsgRecordVo));
        return new Result().success(p);
    }

    /**
     * 添加短信记录
     * @param tabMsgRecord
     * @return
     */
    @Override
    public int addTabMsgRecord(TabMsgRecord tabMsgRecord) {
        return tabMsgRecordMapper.addTabMsgRecord(tabMsgRecord);
    }

}
