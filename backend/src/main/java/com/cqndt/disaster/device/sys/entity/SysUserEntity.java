package com.cqndt.disaster.device.sys.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * Created By marc
 * Date: 2019/3/4  14:32
 * Description:系统用户
 */
@Data
@TableName("tab_user")
public class SysUserEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId
    private Long id;

    private String userName;

    private String password;

    //private String email;

    private String phone;

    private Integer status;

    private String nickName;

    private String salt;

    private Integer aduitType;

    private String remark;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

}

