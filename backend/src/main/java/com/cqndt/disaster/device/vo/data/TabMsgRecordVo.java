package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabMsgRecord;
import lombok.Data;

@Data
public class TabMsgRecordVo extends TabMsgRecord {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;
    private String startCondition1;
    private String endCondition1;
}
