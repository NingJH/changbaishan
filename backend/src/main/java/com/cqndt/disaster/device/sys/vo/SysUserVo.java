package com.cqndt.disaster.device.sys.vo;

import com.cqndt.disaster.device.sys.entity.SysUserEntity;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created By marc
 * Date: 2019/3/11  14:34
 * Description:
 */
@Data
public class SysUserVo extends SysUserEntity {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;

    private String labelName1;

    private String id1;

    private String roleName1;

    private String labelName2;

    private String id2;

    private String areaName2;

    private String staticName5;

    private String staticNum;

    private String oldPass;

    private String newPass;

    private String areaId;

    private String areaName;

    private String areaCode;

    private String level;

    private BigDecimal longitude;

    private BigDecimal latitude;

    private String getAreaCodeSplit(){
        if(null != getAreaCode()){
            return getAreaCode().substring(0,2);
        }
        return null;
    }
}
