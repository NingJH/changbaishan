package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabDeviceCheck;
import com.cqndt.disaster.device.vo.data.TabDeviceCheckVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TabDeviceCheckMapper {

    List<TabDeviceCheckVo> list(TabDeviceCheckVo tabDeviceCheckVo);

    int save(TabDeviceCheckVo tabDeviceCheckVo);

    int updateAll(TabDeviceCheckVo tabDeviceCheckVo);

    int delete(String[] array);

}
