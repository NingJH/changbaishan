package com.cqndt.disaster.device.entity.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
* Description: 
* Date: 2019-06-14
*/

@Data
public class TabMonitor implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  
     */
    private String id;
    /**
     *  布置点编号
     */
    private String monitorNo;
    /**
     *  布置点名称
     */
    private String monitorName;
    /**
     *  创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
    private Date createTime;
    /**
     *  纬度
     */
    private BigDecimal latitude;
    /**
     *  经度
     */
    private BigDecimal longitude;
    /**
     *  高程
     */
    private BigDecimal elevation;
    /**
     *  地理位置
     */
    private String position;
    /**
     *  交通条件
     */
    private String traffic;
    /**
     *  材料供给
     */
    private String material;
    /**
     *  防护条件
     */
    private String protect;
    /**
     *  雇车条件
     */
    private String hireVehicle;
    /**
     *  施工人姓名
     */
    private String workerName;
    /**
     *  施工人电话
     */
    private String workerPhone;
    /**
     *  地形地貌
     */
    private String topographicFeatures;
    /**
     *  关联tab_attachment
     */
    private String imgIds;
    /**
     *  关联tab_device
     */
    private Long deviceId;
    /**
     *  关联tab_project
     */
    private Long projectId;
    
}
