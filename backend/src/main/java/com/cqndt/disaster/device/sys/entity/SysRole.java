package com.cqndt.disaster.device.sys.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
* Description: 角色
* Date: 2019-03-14
*/

@Data
public class SysRole implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  id
     */
    private Long id;
    /**
     *  角色名称
     */
    private String roleName;
    /**
     *  备注
     */
    private String remark;
    /**
     *  部门ID
     */
    private Long deptId;
    /**
     *  创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
    
}
