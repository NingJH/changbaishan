package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.WarnGnssSetting;
import com.cqndt.disaster.device.entity.data.WarnPqSetting;
import lombok.Data;

import java.util.List;

/**
 * @Author yin_q
 * @Date 2019/8/31 16:12
 * @Email yin_qingqin@163.com
 **/
@Data
public class WarnGnssSettingVo {

    private List<WarnGnssSetting> warnGnssSettingList;

    private String sensorNo;

}
