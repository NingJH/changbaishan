package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.WarnGnssSetting;
import com.cqndt.disaster.device.entity.data.WarnPqSetting;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author yin_q
 * @Date 2019/8/31 14:21
 * @Email yin_qingqin@163.com
 **/
@Mapper
public interface WarnGnssSettingMapper {

    /**
     * 根据设备编号获取GNSS阈值信息
     * @param sensorNo
     * @return
     */
    List<WarnGnssSetting> queryWarnGnssSettingList(@Param("sensorNo") String sensorNo);

    /**
     * 根据设备编号删除 地裂缝阈值信息
     * @param sensorNo
     * @return
     */
    int deleteWarnGnssSettingBySensorNo(@Param("sensorNo") String sensorNo);

    /**
     * 新增阈值信息
     * @param warnGnssSetting
     * @return
     */
    int insertWarnGnssSetting(WarnGnssSetting warnGnssSetting);
}
