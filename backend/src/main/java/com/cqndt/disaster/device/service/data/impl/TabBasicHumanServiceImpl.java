package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabBasicHumanMapper;
import com.cqndt.disaster.device.service.data.TabBasicHumanService;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.vo.data.TabBasicHumanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TabBasicHumanServiceImpl implements TabBasicHumanService {
    @Autowired
    private TabBasicHumanMapper tabBasicHumanMapper;

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(TabBasicHumanVo tabBasicHumanVo) throws Exception {
        tabBasicHumanMapper.save(tabBasicHumanVo);
    
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void update(TabBasicHumanVo tabBasicHumanVo) throws Exception {
        TabBasicHumanVo vo = tabBasicHumanMapper.getTabBasicHumanById(tabBasicHumanVo.getId());
        AttachmentVo attachmentVo = new AttachmentVo();
        tabBasicHumanMapper.update(tabBasicHumanVo);
     
   }
    @Override
    public TabBasicHumanVo getTabBasicHumanById(Long id){
        return tabBasicHumanMapper.getTabBasicHumanById(id);
    }
    @Override
    public void delete(String id){
         String[] ids = id.split(",");
         for (String idStr:ids) {
            tabBasicHumanMapper.delete(Integer.parseInt(idStr));
         }
    }
    @Override
    public List<TabBasicHumanVo> list(TabBasicHumanVo tabBasicHumanVo) {
        return  tabBasicHumanMapper.list(tabBasicHumanVo);
    }



}