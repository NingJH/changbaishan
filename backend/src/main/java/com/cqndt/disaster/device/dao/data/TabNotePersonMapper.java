package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabNotePerson;
import com.cqndt.disaster.device.vo.data.TabNotePersonVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @version 1.0
 * @Author:hh
 * @Date:2019/8/5
 * @Description:com.cqndt.disaster.device.dao.data
 */
@Mapper
public interface TabNotePersonMapper {
    int saveTabNotePerson(TabNotePersonVo tabNotePersonVo);
    List<TabNotePersonVo> queryTabNotePerson(TabNotePersonVo tabNotePersonVo);
    int deleteTabNotePersonById(String[] id);
    int updateTabNotePersonById(TabNotePersonVo tabNotePersonVo);

}
