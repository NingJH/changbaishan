package com.cqndt.disaster.device.entity.data;

import java.io.Serializable;

/**
* Description: 
* Date: 2019-06-04
*/


public class TabStaticType implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  
     */
    private Long id;
    /**
     *  静态值名称
     */
    private String staticName;
    /**
     *  静态值类型值
     */
    private Long staticNum;
    /**
     *  静态值类型名称
     */
    private String staticType;
    /**
     *  静态值
     */
    private Long staticKeyVal;
    /**
     *  说明
     */
    private String remark;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStaticName() {
        return staticName;
    }

    public void setStaticName(String staticName) {
        this.staticName = staticName;
    }

    public Long getStaticNum() {
        return staticNum;
    }

    public void setStaticNum(Long staticNum) {
        this.staticNum = staticNum;
    }

    public String getStaticType() {
        return staticType;
    }

    public void setStaticType(String staticType) {
        this.staticType = staticType;
    }

    public Long getStaticKeyVal() {
        return staticKeyVal;
    }

    public void setStaticKeyVal(Long staticKeyVal) {
        this.staticKeyVal = staticKeyVal;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
