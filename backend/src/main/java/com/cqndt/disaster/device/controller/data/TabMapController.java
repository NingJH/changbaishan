package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.service.data.TabMapService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabMapVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 * Date: 2019-08-02
 */
@RestController
@RequestMapping("/tabMap")
public class TabMapController {
    @Autowired
    private TabMapService tabMapService;

    /**
     * 获取全部底图
     */
    @SysLog("获取全部底图")
    @PostMapping("list")
//    @RequiresPermissions("sys:tabmap:list")
    @ApiOperation(value = "分页查询", notes = "分页条件查询")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "10", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "底图id", paramType = "query", required = false),
            @ApiImplicitParam(dataType = "String", name = "name", value = "底图名称", paramType = "query", required = false),
            @ApiImplicitParam(dataType = "String", name = "map", value = "MAP", paramType = "query", required = false),
            @ApiImplicitParam(dataType = "String", name = "url", value = "底图URL", paramType = "query", required = false),
            @ApiImplicitParam(dataType = "Integer", name = "type", value = "底图类型", paramType = "query", required = false),
            @ApiImplicitParam(dataType = "String", name = "mapType", value = "底图/高程类型", paramType = "query", required = false),
            @ApiImplicitParam(dataType = "String", name = "mapUrl", value = "底图/高程地址", paramType = "query", required = false),
            @ApiImplicitParam(dataType = "String", name = "imgs", value = "底图ids", paramType = "query", required = false)
    })
    public Result ListTabMap(@RequestBody TabMapVo tabMapVo){
        PageHelper.startPage(tabMapVo.getPage(),tabMapVo.getLimit());
        PageInfo<TabMapVo> pageInfo = new PageInfo<TabMapVo>(tabMapService.list(tabMapVo));
        Result result = new Result();
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    /**
     * 根据ID获取信息
     */
    @SysLog("根据ID获取信息")
    @PostMapping("getTabMapById")
//    @RequiresPermissions("sys:tabmap:gettabmapbyid")
    @ApiOperation(value = "根据ID查询", notes = "根据ID查询")
    @ApiImplicitParam(dataType = "Integer", name = "id", value = "底图id", required = true, paramType = "query")
    public Result getTabMapById(Integer id){
        if (null == id){
            return new Result().failure(-1, "请选择一条数据进行操作");
        }
        TabMapVo tabMapVo = tabMapService.getTabMapById(id);
        return new Result().success(tabMapVo);
    }

    /**
     * 保存
     */
    @SysLog("增加")
    @PostMapping("save")
//    @RequiresPermissions("sys:tabmap:save")
    @ApiOperation(value = "底图保存", notes = "地图保存")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "name", value = "底图名称", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "map", value = "MAP", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "url", value = "底图URL", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "type", value = "底图类型", defaultValue = "-1", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "mapType", value = "底图/高程类型", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "mapUrl", value = "底图/高程地址", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "imgs", value = "图片ids", paramType = "query")
    })
    public Result save(@RequestBody TabMapVo tabMapVo){
        Result result = new Result();
        try {
            if (tabMapVo.getName().isEmpty()){
                return new Result().failure(-1, "底图/高程名称不能为空");
            }
            if (tabMapVo.getType() == -1){
                return new Result().failure(-1, "类型错误");
            }
            if (tabMapVo.getMapUrl().isEmpty()){
                return new Result().failure(-1, "底图/高程地址不能为空");
            }
            if (tabMapVo.getMapType().isEmpty()){
                return new Result().failure(-1, "底图/高程类型错误");
            }
            tabMapService.save(tabMapVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 更新
     */
    @SysLog("更新")
    @PostMapping("update")
//    @RequiresPermissions("sys:tabmap:update")
    @ApiOperation(value = "地图更新", notes = "地图更新")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "底图ID", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "name", value = "底图名称", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "map", value = "MAP", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "url", value = "底图URL称", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "type", value = "底图类型", defaultValue = "-1", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "mapType", value = "底图/高程类型", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "mapUrl", value = "底图/高程地址", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "imgs", value = "图片ids", paramType = "query")
    })
    public Result update(@RequestBody TabMapVo tabMapVo){
        Result result = new Result();
        try {
            if (tabMapVo.getName().isEmpty()){
                return new Result().failure(-1, "底图/高程名称不能为空");
            }
            if (tabMapVo.getType() == -1){
                return new Result().failure(-1, "类型错误");
            }
            if (tabMapVo.getMapUrl().isEmpty()){
                return new Result().failure(-1, "底图/高程地址不能为空");
            }
            if (tabMapVo.getMapType().isEmpty()){
                return new Result().failure(-1, "底图/高程类型错误");
            }
            if(null == tabMapVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabMapService.update(tabMapVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
//    @RequiresPermissions("sys:tabmap:delete")
    @ApiOperation(value = "删除底图", notes = "删除底图")
    @ApiImplicitParam(dataType = "String", name = "id", value = "底图ID", defaultValue = "1", required = true, paramType = "query")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabMapService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }
}
