package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.vo.data.TabBasicHumanVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * Description: 
 * Date: 2019-06-06
 */

@Mapper
public interface TabBasicHumanMapper {
    /**
     * 根据条件查询list
     * @param tabBasicHumanVo 查询条件
     */
    List<TabBasicHumanVo> list(TabBasicHumanVo tabBasicHumanVo);
    /**
     * 增加
     * @param tabBasicHumanVo 条件
     */
    int save(TabBasicHumanVo tabBasicHumanVo);
    /**
     * 修改
     * @param tabBasicHumanVo 条件
     */
    int update(TabBasicHumanVo tabBasicHumanVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    TabBasicHumanVo getTabBasicHumanById(Long id);


    int getBasicHumanInfoByDisNo(@Param("disNo") String disNo);

    void updateByDisNo(@Param("disNo") String disNo, @Param("preventId") Long preventId);

    void deleteByDisNo(String disNo);

}
