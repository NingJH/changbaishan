package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.MapManagerMapper;
import com.cqndt.disaster.device.dao.data.TabDeviceInstallMapper;
import com.cqndt.disaster.device.service.data.TabMapService;
import com.cqndt.disaster.device.vo.data.TabMapVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public class TabMapServiceImpl implements TabMapService {
    @Autowired
    private MapManagerMapper mapManagerMapper;
    @Autowired
    private TabDeviceInstallMapper tabDeviceInstallMapper;

    @Value("${file-ip}")
    private String fileIp;

    @Override
    public List<TabMapVo> list(TabMapVo tabMapVo) {
        List<TabMapVo> list = mapManagerMapper.list(tabMapVo);
        if(!list.isEmpty()){
            for(TabMapVo tabMapVo1 : list){
                String url = tabMapVo1.getUrl();
                if(StringUtils.isEmpty(url)){
                    continue;
                }
                Map<String, Object> map = tabDeviceInstallMapper.getUrlName(url);
                if(map != null){
                    map.put("viewUrl",fileIp + File.separator + "img" + File.separator + url);
                    tabMapVo1.setMapImgs(map);
                }
            }
        }
        return list;
    }

    @Override
    public TabMapVo getTabMapById(Integer id) {
        return mapManagerMapper.getTabMapById(id);
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRED)
    public void save(TabMapVo tabMapVo) throws Exception {
        mapManagerMapper.save(tabMapVo);
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void update(TabMapVo tabMapVo) throws Exception {
        mapManagerMapper.update(tabMapVo);
    }

    @Override
    public void delete(String id) {
        String[] ids = id.split(",");
        for (String idsStr : ids){
            mapManagerMapper.delete(Integer.parseInt(idsStr));
        }
    }
}
