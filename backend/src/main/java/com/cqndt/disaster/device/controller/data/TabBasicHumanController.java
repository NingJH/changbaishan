package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.service.data.TabBasicHumanService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabBasicHumanVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Description: 
* Date: 2019-06-06
*/
@RestController
@RequestMapping("tabBasicHuman")
public class TabBasicHumanController {
    @Autowired
    private TabBasicHumanService tabBasicHumanService;

    @PostMapping("list")
    public Result listTabUser(@RequestBody TabBasicHumanVo tabBasicHumanVo) {
        PageHelper.startPage(tabBasicHumanVo.getPage(),tabBasicHumanVo.getLimit());
        PageInfo<TabBasicHumanVo> pageInfo = new PageInfo<TabBasicHumanVo>(tabBasicHumanService.list(tabBasicHumanVo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabBasicHumanById")
    public Result getTabBasicHumanById(Long id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        TabBasicHumanVo  tabBasicHumanVo = tabBasicHumanService.getTabBasicHumanById(id);
        return new Result().success(tabBasicHumanVo);
    }

    /**
     * 保存
     */
    @SysLog("增加")
    @PostMapping("save")
    @RequiresPermissions("sys:tabbasichuman:save")
    public Result save(@RequestBody TabBasicHumanVo tabBasicHumanVo){
        Result result = new Result();
        try {
            tabBasicHumanService.save(tabBasicHumanVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("update")
    @RequiresPermissions("sys:tabbasichuman:update")
    public Result update(@RequestBody TabBasicHumanVo tabBasicHumanVo){
        Result result = new Result();
        try {
            if(null == tabBasicHumanVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabBasicHumanService.update(tabBasicHumanVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabbasichuman:delete")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabBasicHumanService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
       return result;
    }


}