package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.*;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabProjectVo;
import com.cqndt.disaster.device.vo.data.TabReportingFileVo;
import com.cqndt.disaster.device.vo.data.TabUnitVo;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/6/5  11:31
 * Description:
 */
public interface TabProjectService {
    /**
     * 条件分页查询
     * @param vo
     * @return
     */
    List<TabProjectVo> listProject(TabProjectVo vo);

    /**
     * 根据项目id获取详情
     * @param id
     * @return
     */
    TabProjectVo getTabProjectById(Integer id);

    /**
     * 保存项目
     * @param tabProjectVo
     * @return
     */
    Result save(TabProjectVo tabProjectVo);

    /**
     * 根据父id获取下级区域信息
     * @param id
     * @return
     */
    List<TabArea> getNextById(String id);
    /**
     * 招标项目选择
     * @return
     */
    List<TabTenderProject> getTenderProject();
    /**
     * 根据区域id获取灾害点选择
     * @param id
     * @return
     */
    List<Map<String,Object>> getBasic(String id);
    /**
     * 修改项目
     * @param tabProjectVo
     * @return
     */
    Result update(TabProjectVo tabProjectVo);
    /**
     * 删除项目
     * @param id
     * @return
     */
    Result delete(String id);
    /**
     * 项目参见单位
     * @param tabProjectVo
     * @return
     */
    Result updateProjectUnit(TabProjectVo tabProjectVo);
    /**
     * 获取项目参见单位
     * @param vo
     * @return
     */
    Result listProjectUnit(TabUnitVo vo);

    /**
     * 删除项目参见单位
     * @param id
     * @return
     */
    Result deleteProjectCjdw(String id);
    /**
     * 获取项目文档
     * @param projectId
     * @return
     */
    Result selectProjectDatum(Integer projectId);
    /**
     * 获取项目班子
     * @param id
     * @return
     */
    Result listProjectPerson(Integer projectId, Integer unitId);
    /**
     * 修改项目班子
     * @param tabProjectVo
     * @return
     */
    Result updateProjectPerson(Integer projectId, Integer unitId, String personIds);

    /**
     * 删除项目班子
     * @param id
     * @return
     */
    Result deleteProjectXmbz(String id);

    /**
     * 新增项目文档
     * @param datum
     * @return
     */
    Result saveTabProjectDatum(TabProjectDatum datum);
    /**
     * 修改项目文档
     * @param datum
     * @return
     */
    Result updateTabProjectDatum(TabProjectDatum datum);
    /**
     * 删除项目文档
     * @param id
     * @return
     */
    Result delTabProjectDatum(Integer id);

    /**
     * 根据项目id和报表类型获取对应的报表
     * @param file
     * @return
     */
    Result listTabReportingFile(TabReportingFile file);
    /**
     * 修改项目报表
     * @param file
     * @return
     */
    Result updateTabReportingFile(TabReportingFile file);
    /**
     * 新增项目报表
     * @param file
     * @return
     */
    Result saveTabReportingFile(TabReportingFile file);
    /**
     * 删除项目报表
     * @param id
     * @return
     */
    Result deleteTabReportingFile(String id);

    void exportExcelFile(TabReportingFile file, HttpServletResponse response);

    /**
     * 获取所有项目及其所拥有设备
     * @return
     */
    Result getAllProject();
}
