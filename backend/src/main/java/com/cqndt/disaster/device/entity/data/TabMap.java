package com.cqndt.disaster.device.entity.data;

import lombok.Data;

@Data
public class TabMap{
    private Integer id = -1;
    private String name; //底图名称
    private String map;
    private String url; //底图地址
    private Integer type = -1; // 底图类型 0、底图 1、高程
    private String mapType; // 底图类型： wmts arcgis google 高程类型：dem
    private String mapUrl;
    private String imgs;
}
