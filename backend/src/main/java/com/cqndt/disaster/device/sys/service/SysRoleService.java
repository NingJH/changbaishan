package com.cqndt.disaster.device.sys.service;

import com.cqndt.disaster.device.sys.vo.SysRoleVo;
import com.cqndt.disaster.device.util.Result;

import java.util.List;
import java.util.Map;

public interface SysRoleService {
    /**
     * 增加
     * @param sysRoleVo
     */
    void save(SysRoleVo sysRoleVo) throws Exception;
     /**
     * 修改
     * @param sysRoleVo
     */
     void update(SysRoleVo sysRoleVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return SysRoleVo
     */
    SysRoleVo getTabRoleById(Long id);
    /**
     * 根据角色id获取已分配的菜单,前台、后台
     */
    List<String> getCheckedMenuById(Long id,String frontBack);
    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param sysRoleVo 查询条件
     * @return 数据列表
     */
    List<SysRoleVo> list(SysRoleVo sysRoleVo);
    /**
     * 信息验证.
     * @param sysRoleVo 信息
     * @param flag (1修改 0增加)
     * @return Result
     */
     Result checkValidate(SysRoleVo sysRoleVo, Integer flag);
    /**
     * 下拉框list
     */
    List<Map<String,Object>> listTabDept(SysRoleVo sysRoleVo);

}