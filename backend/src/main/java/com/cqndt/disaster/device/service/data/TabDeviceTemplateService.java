package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabDeviceTemplate;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabDeviceTemplateVo;
import com.cqndt.disaster.device.vo.data.TabProjectVo;

import java.util.List;
import java.util.Map;

public interface TabDeviceTemplateService {

    void saveTabDeviceTemplate(TabDeviceTemplate tabDeviceTemplate);

    void updateTabDeviceTemplate(TabDeviceTemplateVo tabDeviceTemplate);

    List<TabDeviceTemplateVo> listTabDeviceTemplate(TabDeviceTemplateVo tabDeviceTemplate);

    void deleteTabDeviceTemplate(Integer id);

    //根据选中的项目里--选中的设备id 获取对应的设备模版
    List<Map<String, Object>> getTabDeviceTemplate(List<TabProjectVo> tabProjectVo);
}
