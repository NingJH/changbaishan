package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabWarningDevice;
import com.cqndt.disaster.device.vo.data.TabjsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TabWarningDeviceMapper {
    //新增预警设备表
    int addWarningDevice(TabWarningDevice tabWarningDevice);
    //修改预警设备表
    int updateWarningDevice(TabWarningDevice tabWarningDevice);
    //删除预警设备表
    int deleteWarningDevice(int id);

    int deleteDeviceByDeviceNo(@Param("deviceNo")int deviceNo ,@Param("wid") int wid);
    //根据联合预警 id 获取其下所属 选中的项目id、设备 id
    List<TabjsVo> getCheck(int id);
    //获取标识信息（根据设备id和联合预警表id）
    List<TabWarningDevice> getSerson(@Param("deviceNo") String deviceNo,@Param("id") int id);
    //根据联合预警表id获取每一个设备id （分组展示）
    List<TabWarningDevice> getDeviceNoGroup(int id);

    List<TabWarningDevice> getDeviceByWarn(@Param("id") int id);
    //根据设备类型获取设备模版
    List<TabWarningDevice> getTabDeviceTemplateByDeviceValue1(@Param("deviceType")int  deviceType );

}
