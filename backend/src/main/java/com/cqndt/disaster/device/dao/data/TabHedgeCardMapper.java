package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.vo.data.TabHedgeCardVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * Description: 
 * Date: 2019-06-11
 */

@Mapper
public interface TabHedgeCardMapper {
    /**
     * 根据条件查询list
     * @param tabHedgeCardVo 查询条件
     */
    List<TabHedgeCardVo> list(TabHedgeCardVo tabHedgeCardVo);
    /**
     * 增加
     * @param tabHedgeCardVo 条件
     */
    int save(TabHedgeCardVo tabHedgeCardVo);
    /**
     * 修改
     * @param tabHedgeCardVo 条件
     */
    int update(TabHedgeCardVo tabHedgeCardVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    TabHedgeCardVo getTabHedgeCardById(Long id);


}
