package com.cqndt.disaster.device.entity.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 数据同步传输包装类
 * @author liaohonglai
 * @date 2019/7/30
 */
@Getter
@Setter
@ToString
public class TabPushMsg {


    /**
     * 数据id
     */
    private int id;

    /**
     * 监测单位简称，两位字母
     */
    private String dataSource="zy";

    /**
     * 第三方数据库中监测点编号，唯一；
     */
    private String sensorId;

    /**
     * 监测单位简称，两位字母；GPS监测:GP  地表裂缝监测:LF  雨量监测:YL
     */
    private String sensorType;

    /**
     * 取值为01-15，深部位移监测设备专用，其他设备可以不填；
     */
    private String dataType;

    /**
     * 时间
     */
    private String time;

    /**
     * 传感器编号
     */
    private String senserNo;

    /**
     * 如果同一个时间只对应一个监测值，只需要填v1，余下v2~v6留空或者不写即可。GPS需要v1~v4，即x,y,z,合位移（均为变化量）。
     */
    private String v1;
    private String v2;
    private String v3;
    private String v4;
    private String v5;
    private String v6;
}
