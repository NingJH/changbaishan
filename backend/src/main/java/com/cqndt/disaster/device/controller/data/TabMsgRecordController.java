package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.entity.data.TabMsgRecord;
import com.cqndt.disaster.device.service.data.TabMsgRecordService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabMsgRecordVo;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("tabMsgRecord")
public class TabMsgRecordController {
    @Autowired
    private TabMsgRecordService tabMsgRecordService;
    @ApiImplicitParam(value = "根据参数动态查询短信表")
    @RequestMapping("showCnd")
    public Result showCnd(@RequestBody TabMsgRecordVo tabMsgRecordVo) {
        return tabMsgRecordService.showCondition(tabMsgRecordVo);
    }
}
