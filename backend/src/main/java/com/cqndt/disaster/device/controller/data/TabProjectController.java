package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.dao.data.TabReportingFileMapper;
import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabProjectDatum;
import com.cqndt.disaster.device.entity.data.TabReportingFile;
import com.cqndt.disaster.device.service.data.TabAreaService;
import com.cqndt.disaster.device.service.data.TabProjectService;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import com.cqndt.disaster.device.util.AbstractController;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabProjectVo;
import com.cqndt.disaster.device.vo.data.TabReportingFileVo;
import com.cqndt.disaster.device.vo.data.TabUnitVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
* Description: 项目管理
* Date: 2019-05-30
*/
@Api(description = "项目管理")
@RestController
@RequestMapping("tabProject")
public class TabProjectController extends AbstractController {
    @Autowired
    private TabProjectService tabProjectService;

    @Autowired
    TabAreaService tabAreaService;

    @SysLog("查询项目")
    @PostMapping("list")
    @RequiresPermissions("sys:tabproject:list")
    @ApiOperation(value = "分页查询", notes = "分页条件查询")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "10", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "disType", value = "灾害点类型", defaultValue = "1", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "dangerLevel", value = "险情等级(A特大型B大型C中型D小型)", defaultValue = "A", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectName", value = "项目名称", defaultValue = "1", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "proIsEnd", value = "是否结束(1是,2否)", defaultValue = "2", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "1", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域级别", defaultValue = "1", paramType = "query")
    })
    public Result list(@RequestBody TabProjectVo vo) {
        SysUserVo sysUserVo = getUser();
        List<TabArea> list = tabAreaService.TabAreaList(sysUserVo);
        vo.setList(list);
        vo.setUserId(getUserId().toString());
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<TabProjectVo> pageInfo = new PageInfo<TabProjectVo>(tabProjectService.listProject(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabProjectById")
    @ApiOperation(value = "根据项目id查询项目详情", notes = "根据项目id查询项目详情")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "项目id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabProjectById(Integer id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        TabProjectVo vo = tabProjectService.getTabProjectById(id);
        return new Result().success(vo);
    }
    /**
     * 保存
     */
    @SysLog("增加项目管理")
    @PostMapping("save")
    @RequiresPermissions("sys:tabproject:save")
    @ApiOperation(value = "增加项目管理", notes = "增加项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "projectNo", value = "灾害点编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectName", value = "项目名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Date", name = "startDate", value = "项目开始时间", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "BigDecimal", name = "longitude", value = "经度", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "BigDecimal", name = "latitude", value = "纬度", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectAdd", value = "项目位置", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "remark", value = "备注", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "state", value = "状态(0:删除,1:正常)", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "proType", value = "项目类别", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "proTypeName", value = "项目类别名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "proIsEnd", value = "是否结束(0:已结束,1:未结束)", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "manualAuto", value = "人工/自动（1人工 2自动）", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Date", name = "startTime", value = "监测开始时间", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Date", name = "endTime", value = "监测结束时间", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "monitorUnit", value = "监测单位", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "monitorUnitName", value = "监测单位名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectMan", value = "项目负责人", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectManPhone", value = "项目负责电话", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "areaId", value = "所属区域id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "tenderId", value = "招标项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "tenderName", value = "招标项目名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "taken", value = "正摄", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "model", value = "模型", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "photographyType", value = "倾斜摄影类型", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "twoRoundUrl", value = "720图Url（多个逗号隔开）", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "twoRoundName ", value = "720图原zip名（多个逗号隔开,与twoRoundUrl一一对应）", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "List<Attachment>", name = "imgs ", value = "图片(数组一条包含：id、attachmentUrl、attachmentType、attachmentName)", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "List<Attachment>", name = "videos ", value = "视频(数组一条包含：id、attachmentUrl、attachmentType、attachmentName)", defaultValue = "", required = false, paramType = "query")
    })
    public Result save(@RequestBody TabProjectVo tabProjectVo){
        tabProjectVo.setUserId(getUserId().toString());
        return tabProjectService.save(tabProjectVo);
    }

    /**
     * 区域级联查询
     */
    @PostMapping("getNextAreaById")
    @ApiOperation(value = "根据区域id查询下级区域", notes = "根据区域id查询下级区域")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "区域id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getTabArea(String id){
        if(null == id){
            id = getUser().getAreaId();
        }
        return new Result().success(tabProjectService.getNextById(id));
    }
    /**
     * 获取招标项目
     */
    @PostMapping("getTenderProject")
    @ApiOperation(value = "获取招标项目", notes = "获取招标项目")
    public Result getTenderProject(){
        return new Result().success(tabProjectService.getTenderProject());
    }

    /**
     * 根据区县id获取灾害点编号
     */
    @PostMapping("getBasic")
    @ApiOperation(value = "根据区县id获取灾害点编号", notes = "根据区县id获取灾害点编号")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "区域id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result getBasic(String id){
        return new Result().success(tabProjectService.getBasic(id));
    }

    /**
     * 修改项目
     */
    @SysLog("修改项目")
    @PostMapping("update")
    @RequiresPermissions("sys:tabproject:update")
    @ApiOperation(value = "修改项目管理", notes = "修改项目管理")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectNo", value = "灾害点编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectName", value = "项目名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Date", name = "startDate", value = "项目开始时间", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "BigDecimal", name = "longitude", value = "经度", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "BigDecimal", name = "latitude", value = "纬度", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectAdd", value = "项目位置", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "remark", value = "备注", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "state", value = "状态(0:删除,1:正常)", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "proType", value = "项目类别", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "proTypeName", value = "项目类别名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "proIsEnd", value = "是否结束(0:已结束,1:未结束)", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "manualAuto", value = "人工/自动（1人工 2自动）", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Date", name = "startTime", value = "监测开始时间", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Date", name = "endTime", value = "监测结束时间", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "monitorUnit", value = "监测单位", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "monitorUnitName", value = "监测单位名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectMan", value = "项目负责人", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectManPhone", value = "项目负责电话", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "areaId", value = "所属区域id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "tenderId", value = "招标项目id", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "tenderName", value = "招标项目名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "taken", value = "正摄", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "model", value = "模型", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "photographyType", value = "倾斜摄影类型", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "twoRoundUrl", value = "720图Url（多个逗号隔开）", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "twoRoundName ", value = "720图原zip名（多个逗号隔开,与twoRoundUrl一一对应）", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "List<Attachment>", name = "imgs ", value = "图片(数组一条包含：id、attachmentUrl、attachmentType、attachmentName)", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "List<Attachment>", name = "videos ", value = "视频(数组一条包含：id、attachmentUrl、attachmentType、attachmentName)", defaultValue = "", required = false, paramType = "query")
    })
    public Result update(@RequestBody TabProjectVo tabProjectVo){
        return tabProjectService.update(tabProjectVo);
    }

    /**
     * 删除项目
     */
    @SysLog("删除项目")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabproject:delete")
    @ApiOperation(value = "删除项目", notes = "删除项目")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "项目id(多条逗号隔开)", defaultValue = "", required = true, paramType = "query")
    })
    public Result delete(String id){
        return tabProjectService.delete(id);
    }

    /**
     * 获取项目参建单位
     */
    @PostMapping("listProjectUnit")
    @ApiOperation(value = "获取项目参建单位", notes = "获取项目参建单位")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "unitName", value = "单位名称", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "unitPerson", value = "单位负责人", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "vendorId", value = "厂商、单位类型id", defaultValue = "",  paramType = "query"),
    })
    public Result listProjectUnit(@RequestBody TabUnitVo vo){
        return tabProjectService.listProjectUnit(vo);
    }
    /**
     * 增加/修改项目参建单位
     */
    @SysLog("增加/修改项目参建单位")
    @PostMapping("updateProjectUnit")
    @RequiresPermissions("sys:tabproject:update")
    @ApiOperation(value = "增加/修改项目参建单位", notes = "增加/修改项目参建单位")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "List<TabProjectCjdw>", name = "cjdws", value = "(id(增加为空，修改必填)、参建单位类型-cjType（静态值获取static_num=3）、参建单位类型名称-cjTypeName、单位名称-cjName、资质-cjZj、法人-cjFr、联系人-cjLxr)", defaultValue = "", required = false, paramType = "query")
    })
    public Result updateProjectUnit(TabProjectVo tabProjectVo){
        return tabProjectService.updateProjectUnit(tabProjectVo);
    }

    /**
     * 删除项目参建单位
     */
    /*@SysLog("删除项目参建单位")
    @PostMapping("deleteProjectCjdw")
    @RequiresPermissions("sys:tabproject:delete")
    @ApiOperation(value = "删除项目参建单位", notes = "删除项目参建单位")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "参见单位id（多条逗号隔开）", defaultValue = "", required = true, paramType = "query")
    })
    public Result deleteProjectCjdw(String id){
        return tabProjectService.deleteProjectCjdw(id);
    }*/

    /**
     * 获取项目班子
     */
    @PostMapping("listProjectPerson")
    @ApiOperation(value = "获取项目班子", notes = "获取项目班子")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "项目id", defaultValue = "", required = true, paramType = "query")
    })
    public Result listProjectPerson(Integer projectId, Integer unitId){
        return tabProjectService.listProjectPerson(projectId, unitId);
    }
    /**
     * 增加/修改项目班子
     */
    @SysLog("增加/修改项目班子")
    @PostMapping("updateProjectPerson")
    @RequiresPermissions("sys:tabproject:update")
    @ApiOperation(value = "增加/修改项目班子", notes = "增加/修改项目班子")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "unitId", value = "单位ID", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "personIds", value = "人员ID", defaultValue = "", required = false, paramType = "query")
    })
    public Result updateProjectPerson(Integer projectId, Integer unitId, String personIds){
        return tabProjectService.updateProjectPerson(projectId, unitId, personIds);
    }

    /**
     * 删除项目班子
     */
    /*@SysLog("删除项目班子")
    @PostMapping("deleteProjectXmbz")
    @RequiresPermissions("sys:tabproject:delete")
    @ApiOperation(value = "删除项目班子", notes = "删除项目班子")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "项目班子id(多条逗号隔开)", defaultValue = "", required = true, paramType = "query")
    })
    public Result deleteProjectXmbz(String id){
        return tabProjectService.deleteProjectXmbz(id);
    }*/

    @PostMapping("/listTabProjectDatum")
    @ApiOperation(value = "根据项目id查询项目文档", notes = "根据项目id查询项目文档")
    @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query")
    public Result listTabProjectDatum(Integer projectId){
        return tabProjectService.selectProjectDatum(projectId);
    }

    @SysLog("新增项目文档")
    @PostMapping("saveTabProjectDatum")
    @RequiresPermissions("sys:tabproject:save")
    @ApiOperation(value = "新增项目文档", notes = "新增项目文档")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "title", value = "标题", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "content", value = "内容", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "fileType", value = "文件类型（1文件夹 2 文件）", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "fileTypeName", value = "类型名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "parentId", value = "父级id", defaultValue = "", required = false, paramType = "query")
    })
    public Result saveTabProjectDatum(@RequestBody TabProjectDatum datum){
        return tabProjectService.saveTabProjectDatum(datum);
    }

    @SysLog("修改项目文档")
    @PostMapping("updateTabProjectDatum")
    @RequiresPermissions("sys:tabproject:update")
    @ApiOperation(value = "修改项目文档", notes = "修改项目文档")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "文档id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "title", value = "标题", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "content", value = "内容", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "fileType", value = "文件类型（1文件夹 2 文件）", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "fileTypeName", value = "类型名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "parentId", value = "父级id", defaultValue = "", required = false, paramType = "query")
    })
    public Result updateTabProjectDatum(@RequestBody TabProjectDatum datum){
        return tabProjectService.updateTabProjectDatum(datum);
    }

    @SysLog("删除项目文档")
    @PostMapping("/deleteTabProjectDatum")
    @RequiresPermissions("sys:tabproject:delete")
    @ApiOperation(value = "删除项目文档", notes = "删除项目文档")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "文档id", defaultValue = "", required = true, paramType = "query")
    })
    public Result deleteTabProjectDatum(Integer id){
        return tabProjectService.delTabProjectDatum(id);
    }

    @PostMapping("/listTabReportingFile")
    @ApiOperation(value = "获取项目报表", notes = "获取项目报表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "fileType", value = "报表类型", defaultValue = "", required = true, paramType = "query")
    })
    public Result listTabReportingFile(@RequestBody TabReportingFile file){
        return tabProjectService.listTabReportingFile(file);
    }

    @PostMapping("/saveTabReportingFile")
    @SysLog("新增项目报表")
    @RequiresPermissions("sys:tabproject:save")
    @ApiOperation(value = "新增项目报表", notes = "新增项目报表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "fileType", value = "报表类型", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "fileName", value = "报表名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "uploadUrl", value = "报表路径", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query")
    })
    public Result saveTabReportingFile(@RequestBody TabReportingFile file){
        return tabProjectService.saveTabReportingFile(file);
    }

    @PostMapping("/updateTabReportingFile")
    @SysLog("修改项目报表")
    @RequiresPermissions("sys:tabproject:update")
    @ApiOperation(value = "修改项目报表", notes = "修改项目报表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "报表id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "fileType", value = "报表类型", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "fileName", value = "报表名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "uploadUrl", value = "报表路径", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query")
    })
    public Result updateTabReportingFile(TabReportingFile file){
        return tabProjectService.updateTabReportingFile(file);
    }

    @PostMapping("/deleteTabReportingFile")
    @SysLog("删除项目报表")
    @RequiresPermissions("sys:tabproject:delete")
    @ApiOperation(value = "删除项目报表", notes = "删除项目报表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "报表id(多条逗号隔开)", defaultValue = "", required = true, paramType = "query")
    })
    public Result deleteTabReportingFile(String id){
        return tabProjectService.deleteTabReportingFile(id);
    }

    /**
     * 项目报表导出 excel
     */
    @ApiOperation(value = "导出项目报表", notes = "导出项目报表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "fileType", value = "报表类型", defaultValue = "", required = true, paramType = "query")
    })
    @GetMapping("exportExcelFile")
    public void exportExcelFile( TabReportingFile file, HttpServletResponse response){
        tabProjectService.exportExcelFile(file,response);
    }

    /**
     * 获取所有项目
     * @return
     */
    @ApiOperation(value = "获取所有项目及其下所属设备", notes = "获取所有项目及其下所属设备")
    @PostMapping("getAllProject")
    public Result getAllProject(){
        return tabProjectService.getAllProject();
    }
}
