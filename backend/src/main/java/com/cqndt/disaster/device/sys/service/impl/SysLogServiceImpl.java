package com.cqndt.disaster.device.sys.service.impl;

import com.cqndt.disaster.device.sys.dao.SysLogMapper;
import com.cqndt.disaster.device.sys.service.SysLogService;
import com.cqndt.disaster.device.sys.vo.SysLogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created By marc
 * Date: 2019/3/18  15:13
 * Description:日志管理
 */
@Service
public class SysLogServiceImpl implements SysLogService {
    @Autowired
    private SysLogMapper sysLogMapper;

    @Override
    public void save(SysLogVo sysLogVo) {
        sysLogMapper.save(sysLogVo);
    }
}
