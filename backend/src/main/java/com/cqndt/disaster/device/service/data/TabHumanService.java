package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.vo.data.TabHumanVo;

import java.util.List;

public interface TabHumanService {
    /**
     * 增加
     * @param tabHumanVo
     */
    void save(TabHumanVo tabHumanVo) throws Exception;
     /**
     * 修改
     * @param tabHumanVo
     */
     void update(TabHumanVo tabHumanVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabHumanVo
     */
    TabHumanVo getTabHumanById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabHumanVo 查询条件
     * @return 数据列表
     */
    List<TabHumanVo> list(TabHumanVo tabHumanVo);



}