package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.vo.data.TabMapVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Description:
 * Date: 2019-08-02
 */
@Mapper
public interface MapManagerMapper {
    /**
     * 根据条件查询list
     * @param tabMapVo 查询条件
     */
    List<TabMapVo> list(TabMapVo tabMapVo);

    /**
     * 根据id查询单条数据
     * @param id 查询条件
     */
    TabMapVo getTabMapById(Integer id);

    /**
     * 删除
     * @param id 条件
     */
    void delete(Integer id);

    /**
     * 更新
     * @param tabMapVo 条件
     */
    void update(TabMapVo tabMapVo);

    /**
     * 保存
     * @param tabMapVo 条件
     */
    void save(TabMapVo tabMapVo);
}
