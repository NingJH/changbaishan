package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabPhotography;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface TabPhotographyMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_photography
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_photography
     *
     * @mbg.generated
     */
    int insert(TabPhotography record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_photography
     *
     * @mbg.generated
     */
    int insertSelective(TabPhotography record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_photography
     *
     * @mbg.generated
     */
    TabPhotography selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_photography
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TabPhotography record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_photography
     *
     * @mbg.generated
     */
    int updateByPrimaryKeyWithBLOBs(TabPhotography record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_photography
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TabPhotography record);

    void deleteByRelationAndType(@Param("relationNo") String relationNo,@Param("modelType") String modelType);

    TabPhotography getByRelationAndType(@Param("relationNo") String relationNo, @Param("modelType") String modelType);

    int getInfomationByDisNo(String disNo);

}