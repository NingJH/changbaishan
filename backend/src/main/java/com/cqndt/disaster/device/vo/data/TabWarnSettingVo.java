package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabWarnSetting;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Description:
 * Date: 2019-06-11
 */
@Data
public class TabWarnSettingVo extends TabWarnSetting {
    /**
     * 页数
     */
    private Integer page = 1;
    /**
     * 条数
     */
    private Integer limit = 10;

    private Long yuzhiId;

    private BigDecimal red = null;

    private BigDecimal orange = null;

    private BigDecimal yellow = null;

    private BigDecimal blue = null;

    private BigDecimal frequencyRed = null;

    private BigDecimal frequencyOrange = null;

    private BigDecimal frequencyYellow = null;

    private BigDecimal frequencyBlue = null;

    private BigDecimal amplitudeRed = null;

    private BigDecimal amplitudeOrange = null;

    private BigDecimal amplitudeYellow = null;

    private BigDecimal amplitudeBlue = null;

    private List<TabWarnSettingVo> tabWarnSettingVoList;
}
