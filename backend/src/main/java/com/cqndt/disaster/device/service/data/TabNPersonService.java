package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabNPerson;

import java.util.List;

public interface TabNPersonService {

    /**
     * 新增人员
     */
    void savePerson(TabNPerson nPerson);

    /**
     * 删除人员byId
     */
    int deletePersonById(TabNPerson personId);

    /**
     * 修改人员信息
     */
    void updatePerson(TabNPerson nPerson);

    /**
     * 查询人员byId
     */
    TabNPerson findPersonById(Integer personId);

    /**
     * 查询所有人员byPage
     */
    List<TabNPerson> findPersonsByPage(TabNPerson tabNPerson);


}
