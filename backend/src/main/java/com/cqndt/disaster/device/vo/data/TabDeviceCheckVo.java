package com.cqndt.disaster.device.vo.data;


import com.cqndt.disaster.device.entity.data.TabAttachment;
import com.cqndt.disaster.device.entity.data.TabDeviceCheck;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class TabDeviceCheckVo extends TabDeviceCheck {

    /**
     * 页数
     */
    private Integer page = 1;

    /**
     * 条数
     */
    private Integer limit = 10;

    /**
     * 图片
     */
    private List<TabAttachmentVo> imgs;
    /**
     * 视频
     */
    private List<TabAttachmentVo> videos;

    /**
     * 图片
     */
    private List<Map<String,Object>> DeviceCheckImgs;
    /**
     * 视频
     */
    private List<Map<String,Object>> DeviceCheckVideos;
}
