package com.cqndt.disaster.device.entity.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Gnss阈值
 *
 * @Author yin_q
 * @Date 2019/8/31 12:05
 * @Email yin_qingqin@163.com
 **/
@Data
public class WarnGnssSetting implements Serializable {

    private int id;

    // 标识
    private String sign;

    // 单位
    private String unit;

    // 标识名称
    private String signName;

    // 初始值
    private BigDecimal initialValue;

    // 相邻告警阈值(红色告警)
    private BigDecimal xlAlarmRed;

    // 相邻告警阈值(橙色告警)
    private BigDecimal xlAlarmOrange;

    // 相邻告警阈值(黄色告警)
    private BigDecimal xlAlarmYellow;

    // 相邻告警阈值(蓝色告警)
    private BigDecimal xlAlarmBlue;

    // 累积告警阈值(红色告警)
    private BigDecimal ljAlarmRed;

    // 累积告警阈值(橙色告警)
    private BigDecimal ljAlarmOrange;

    // 累积告警阈值(黄色告警)
    private BigDecimal ljAlarmYellow;

    // 累积告警阈值(蓝色告警)
    private BigDecimal ljAlarmBlue;

    // 监测点
    private int monitorId;

    // 设备编号
    private String sensorNo;

}
