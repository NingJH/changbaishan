package com.cqndt.disaster.device.entity.data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
* Description: 表备注
* Date: 2019-06-11
*/

public class TabPlanInfo implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  
     */
    private Long id;
    /**
     *  灾害点名称
     */
    private String pDisName;
    /**
     *  省
     */
    private String pEProvince;
    /**
     *  市
     */
    private String pECity;
    /**
     *  区
     */
    private String pEDistrict;
    /**
     *  乡
     */
    private String pETown;
    /**
     *  村
     */
    private String pEVillage;
    /**
     *  组
     */
    private String pEGroup;
    /**
     *  监测方式
     */
    private String pSurveyWay;
    /**
     *  监测周期
     */
    private String pSurveyCycle;
    /**
     *  监测手段
     */
    private String pSurveyMeans;
    /**
     *  监测部位
     */
    private String pWallPart;
    /**
     *  预警方式
     */
    private String pType;
    /**
     *  应急处理措施
     */
    private String pEMeasure;
    /**
     *  撤离路线
     */
    private String pOutLine;
    /**
     *  撤离顺序
     */
    private String pEOrder;
    /**
     *  监测人员
     */
    private String pMonitorMan;
    /**
     *  电话
     */
    private String pMonitorPhone;
    /**
     *  村长
     */
    private String pVillageHead;
    /**
     *  电话
     */
    private String pVillagePhone;
    /**
     *  组长
     */
    private String pGroupHead;
    /**
     *  电话
     */
    private String pGroupPhone;
    /**
     *  报警电话
     */
    private String pAlarmCall;
    /**
     *  危险区范围及撤离路线图
     */
    private String pLinePic;
    /**
     *  预案编制单位
     */
    private String pWeaveUnit;
    /**
     *  预案批准单位
     */
    private String pApproveUnit;
    /**
     *  灾害点编号(与tab_basic表dis_no关联）
     */
    private String pNo;
    /**
     *  经度
     */
    private BigDecimal longitude;
    /**
     *  纬度
     */
    private BigDecimal latitude;
    /**
     *  隐患点类型
     */
    private String pDisType;
    /**
     *  规模(m3)
     */
    private String pScale;
    /**
     *  规模等级
     */
    private String pScaleLevel;
    /**
     *  威胁人口
     */
    private String pThreatenPeople;
    /**
     *  威胁财产
     */
    private String pThreatenAssets;
    /**
     *  险情等级
     */
    private String pDangerLevel;
    /**
     *  曾发生灾害时间
     */
    private String pThereHappen;
    /**
     *  地质环境条件
     */
    private String pGeologicalEnvironmentCondition;
    /**
     *  变形特征历史活动
     */
    private String pDeformationCharacteristicsHistoricalActivities;
    /**
     *  稳定性分析
     */
    private String pStability;
    /**
     *  引发因素
     */
    private String pTriggeringFactors;
    /**
     *  潜在危害
     */
    private String pPotentialHazards;
    /**
     *  临灾状态预测
     */
    private String pDisasterPrediction;
    /**
     *  监测方法
     */
    private String pMonitoringMethod;
    /**
     *  监测仪器
     */
    private String pMonitoringInstrument;
    /**
     *  监测周期
     */
    private String pMonitoringCycle;
    /**
     *  监测责任人
     */
    private String pMonitoringPersonLiable;
    /**
     *  监测责任人电话
     */
    private String pMonitoringPersonLiablePhone;
    /**
     *  专职监测人员
     */
    private String pFullTimeMonitorsPerson;
    /**
     *  专职监测人员电话
     */
    private String pFullTimeMonitorsPersonPhone;
    /**
     *  报警方法
     */
    private String pAlarmMethod;
    /**
     *  报警信号
     */
    private String pAlarmSignal;
    /**
     *  报警人
     */
    private String pAlarmPerson;
    /**
     *  报警人电话
     */
    private String pAlarmPersonPhone;
    /**
     *  预定避灾点
     */
    private String pPredeterminedDisasterAvoidancePoint;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getpDisName() {
        return pDisName;
    }

    public void setpDisName(String pDisName) {
        this.pDisName = pDisName;
    }

    public String getpEProvince() {
        return pEProvince;
    }

    public void setpEProvince(String pEProvince) {
        this.pEProvince = pEProvince;
    }

    public String getpECity() {
        return pECity;
    }

    public void setpECity(String pECity) {
        this.pECity = pECity;
    }

    public String getpEDistrict() {
        return pEDistrict;
    }

    public void setpEDistrict(String pEDistrict) {
        this.pEDistrict = pEDistrict;
    }

    public String getpETown() {
        return pETown;
    }

    public void setpETown(String pETown) {
        this.pETown = pETown;
    }

    public String getpEVillage() {
        return pEVillage;
    }

    public void setpEVillage(String pEVillage) {
        this.pEVillage = pEVillage;
    }

    public String getpEGroup() {
        return pEGroup;
    }

    public void setpEGroup(String pEGroup) {
        this.pEGroup = pEGroup;
    }

    public String getpSurveyWay() {
        return pSurveyWay;
    }

    public void setpSurveyWay(String pSurveyWay) {
        this.pSurveyWay = pSurveyWay;
    }

    public String getpSurveyCycle() {
        return pSurveyCycle;
    }

    public void setpSurveyCycle(String pSurveyCycle) {
        this.pSurveyCycle = pSurveyCycle;
    }

    public String getpSurveyMeans() {
        return pSurveyMeans;
    }

    public void setpSurveyMeans(String pSurveyMeans) {
        this.pSurveyMeans = pSurveyMeans;
    }

    public String getpWallPart() {
        return pWallPart;
    }

    public void setpWallPart(String pWallPart) {
        this.pWallPart = pWallPart;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public String getpEMeasure() {
        return pEMeasure;
    }

    public void setpEMeasure(String pEMeasure) {
        this.pEMeasure = pEMeasure;
    }

    public String getpOutLine() {
        return pOutLine;
    }

    public void setpOutLine(String pOutLine) {
        this.pOutLine = pOutLine;
    }

    public String getpEOrder() {
        return pEOrder;
    }

    public void setpEOrder(String pEOrder) {
        this.pEOrder = pEOrder;
    }

    public String getpMonitorMan() {
        return pMonitorMan;
    }

    public void setpMonitorMan(String pMonitorMan) {
        this.pMonitorMan = pMonitorMan;
    }

    public String getpMonitorPhone() {
        return pMonitorPhone;
    }

    public void setpMonitorPhone(String pMonitorPhone) {
        this.pMonitorPhone = pMonitorPhone;
    }

    public String getpVillageHead() {
        return pVillageHead;
    }

    public void setpVillageHead(String pVillageHead) {
        this.pVillageHead = pVillageHead;
    }

    public String getpVillagePhone() {
        return pVillagePhone;
    }

    public void setpVillagePhone(String pVillagePhone) {
        this.pVillagePhone = pVillagePhone;
    }

    public String getpGroupHead() {
        return pGroupHead;
    }

    public void setpGroupHead(String pGroupHead) {
        this.pGroupHead = pGroupHead;
    }

    public String getpGroupPhone() {
        return pGroupPhone;
    }

    public void setpGroupPhone(String pGroupPhone) {
        this.pGroupPhone = pGroupPhone;
    }

    public String getpAlarmCall() {
        return pAlarmCall;
    }

    public void setpAlarmCall(String pAlarmCall) {
        this.pAlarmCall = pAlarmCall;
    }

    public String getpLinePic() {
        return pLinePic;
    }

    public void setpLinePic(String pLinePic) {
        this.pLinePic = pLinePic;
    }

    public String getpWeaveUnit() {
        return pWeaveUnit;
    }

    public void setpWeaveUnit(String pWeaveUnit) {
        this.pWeaveUnit = pWeaveUnit;
    }

    public String getpApproveUnit() {
        return pApproveUnit;
    }

    public void setpApproveUnit(String pApproveUnit) {
        this.pApproveUnit = pApproveUnit;
    }

    public String getpNo() {
        return pNo;
    }

    public void setpNo(String pNo) {
        this.pNo = pNo;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public String getpDisType() {
        return pDisType;
    }

    public void setpDisType(String pDisType) {
        this.pDisType = pDisType;
    }

    public String getpScale() {
        return pScale;
    }

    public void setpScale(String pScale) {
        this.pScale = pScale;
    }

    public String getpScaleLevel() {
        return pScaleLevel;
    }

    public void setpScaleLevel(String pScaleLevel) {
        this.pScaleLevel = pScaleLevel;
    }

    public String getpThreatenPeople() {
        return pThreatenPeople;
    }

    public void setpThreatenPeople(String pThreatenPeople) {
        this.pThreatenPeople = pThreatenPeople;
    }

    public String getpThreatenAssets() {
        return pThreatenAssets;
    }

    public void setpThreatenAssets(String pThreatenAssets) {
        this.pThreatenAssets = pThreatenAssets;
    }

    public String getpDangerLevel() {
        return pDangerLevel;
    }

    public void setpDangerLevel(String pDangerLevel) {
        this.pDangerLevel = pDangerLevel;
    }

    public String getpThereHappen() {
        return pThereHappen;
    }

    public void setpThereHappen(String pThereHappen) {
        this.pThereHappen = pThereHappen;
    }

    public String getpGeologicalEnvironmentCondition() {
        return pGeologicalEnvironmentCondition;
    }

    public void setpGeologicalEnvironmentCondition(String pGeologicalEnvironmentCondition) {
        this.pGeologicalEnvironmentCondition = pGeologicalEnvironmentCondition;
    }

    public String getpDeformationCharacteristicsHistoricalActivities() {
        return pDeformationCharacteristicsHistoricalActivities;
    }

    public void setpDeformationCharacteristicsHistoricalActivities(String pDeformationCharacteristicsHistoricalActivities) {
        this.pDeformationCharacteristicsHistoricalActivities = pDeformationCharacteristicsHistoricalActivities;
    }

    public String getpStability() {
        return pStability;
    }

    public void setpStability(String pStability) {
        this.pStability = pStability;
    }

    public String getpTriggeringFactors() {
        return pTriggeringFactors;
    }

    public void setpTriggeringFactors(String pTriggeringFactors) {
        this.pTriggeringFactors = pTriggeringFactors;
    }

    public String getpPotentialHazards() {
        return pPotentialHazards;
    }

    public void setpPotentialHazards(String pPotentialHazards) {
        this.pPotentialHazards = pPotentialHazards;
    }

    public String getpDisasterPrediction() {
        return pDisasterPrediction;
    }

    public void setpDisasterPrediction(String pDisasterPrediction) {
        this.pDisasterPrediction = pDisasterPrediction;
    }

    public String getpMonitoringMethod() {
        return pMonitoringMethod;
    }

    public void setpMonitoringMethod(String pMonitoringMethod) {
        this.pMonitoringMethod = pMonitoringMethod;
    }

    public String getpMonitoringInstrument() {
        return pMonitoringInstrument;
    }

    public void setpMonitoringInstrument(String pMonitoringInstrument) {
        this.pMonitoringInstrument = pMonitoringInstrument;
    }

    public String getpMonitoringCycle() {
        return pMonitoringCycle;
    }

    public void setpMonitoringCycle(String pMonitoringCycle) {
        this.pMonitoringCycle = pMonitoringCycle;
    }

    public String getpMonitoringPersonLiable() {
        return pMonitoringPersonLiable;
    }

    public void setpMonitoringPersonLiable(String pMonitoringPersonLiable) {
        this.pMonitoringPersonLiable = pMonitoringPersonLiable;
    }

    public String getpMonitoringPersonLiablePhone() {
        return pMonitoringPersonLiablePhone;
    }

    public void setpMonitoringPersonLiablePhone(String pMonitoringPersonLiablePhone) {
        this.pMonitoringPersonLiablePhone = pMonitoringPersonLiablePhone;
    }

    public String getpFullTimeMonitorsPerson() {
        return pFullTimeMonitorsPerson;
    }

    public void setpFullTimeMonitorsPerson(String pFullTimeMonitorsPerson) {
        this.pFullTimeMonitorsPerson = pFullTimeMonitorsPerson;
    }

    public String getpFullTimeMonitorsPersonPhone() {
        return pFullTimeMonitorsPersonPhone;
    }

    public void setpFullTimeMonitorsPersonPhone(String pFullTimeMonitorsPersonPhone) {
        this.pFullTimeMonitorsPersonPhone = pFullTimeMonitorsPersonPhone;
    }

    public String getpAlarmMethod() {
        return pAlarmMethod;
    }

    public void setpAlarmMethod(String pAlarmMethod) {
        this.pAlarmMethod = pAlarmMethod;
    }

    public String getpAlarmSignal() {
        return pAlarmSignal;
    }

    public void setpAlarmSignal(String pAlarmSignal) {
        this.pAlarmSignal = pAlarmSignal;
    }

    public String getpAlarmPerson() {
        return pAlarmPerson;
    }

    public void setpAlarmPerson(String pAlarmPerson) {
        this.pAlarmPerson = pAlarmPerson;
    }

    public String getpAlarmPersonPhone() {
        return pAlarmPersonPhone;
    }

    public void setpAlarmPersonPhone(String pAlarmPersonPhone) {
        this.pAlarmPersonPhone = pAlarmPersonPhone;
    }

    public String getpPredeterminedDisasterAvoidancePoint() {
        return pPredeterminedDisasterAvoidancePoint;
    }

    public void setpPredeterminedDisasterAvoidancePoint(String pPredeterminedDisasterAvoidancePoint) {
        this.pPredeterminedDisasterAvoidancePoint = pPredeterminedDisasterAvoidancePoint;
    }
}
