package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabUnit;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabUnitVo;

import java.util.List;
import java.util.Map;

public interface TabUnitService {

    /**
     * 查询单位列表以及对应id
     *
     * @return
     */
    List<TabUnit> getTabUnitName();

    /**
     * 查询列表
     *
     * @return
     */
    Result queryList(TabUnitVo vo);


    /**
     * 查询单位类别、厂商类别
     */
    Result getVendorType();

    /**
     * 新增单位
     */
    Result addUnit(TabUnitVo vo);

    /**
     * 删除单位
     */
    Result deleteUnitById(TabUnitVo vo);

    /**
     * 修改单位
     */
    Result updateUnitById(TabUnitVo vo);

    /**
     * 根据id获取详情
     */
    Result getTabUnitById(Long id);

    /**
     * 根据单位类别id查询所属单位
     */
    Result getTabUnitByVendorId(int vendorId);

}
