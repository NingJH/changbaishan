package com.cqndt.disaster.device.entity.data;

import lombok.Data;

/**
 * @version 1.0
 * @Author:hh
 * @Date:2019/8/5
 * @Description:com.cqndt.disaster.device.entity.data
 */
@Data
public class TabNotePerson {
    private int id;
    private String phone;
    private String  name;
    private int level;
    private int type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "TabNotePerson{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", name='" + name + '\'' +
                ", level=" + level +
                ", type=" + type +
                '}';
    }
}
