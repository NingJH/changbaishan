package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabAreaMapper;
import com.cqndt.disaster.device.dao.data.TabNPersonMapper;
import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabNPerson;
import com.cqndt.disaster.device.service.data.TabAreaService;
import com.cqndt.disaster.device.service.data.TabNPersonService;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.beans.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class TabNPersonServiceImpl implements TabNPersonService {

    @Resource
    private TabNPersonMapper nPersonMapper;


    @Override
    @Transactional
    public void savePerson(TabNPerson nPerson) {
        nPerson.setCreateTime(new Date());
        nPerson.setUpdateTime(new Date());
        nPersonMapper.addPerson(nPerson);
        nPerson.getTabNPersonBasic().forEach(t->{
            nPersonMapper.addPersonAndBasic(nPerson.getId(),t);
        });
    }

    @Override
    @Transactional
    public int deletePersonById(TabNPerson nPerson) {
        if(nPerson.getPersonId().contains(",")){
            String[] id = nPerson.getPersonId().split(",");
            for (int i = 0; i < id.length; i++) {
                nPersonMapper.deletePersonById(id[i]+"");
                nPersonMapper.deletePersonBasicById(id[i]+"");
            }
        }else {
            nPersonMapper.deletePersonBasicById(nPerson.getPersonId());
            return nPersonMapper.deletePersonById(nPerson.getPersonId());
        }
        return 1;
    }

    @Override
    @Transactional
    public void updatePerson(TabNPerson nPerson) {
        nPersonMapper.updatePerson(nPerson);
        nPersonMapper.deletePersonBasicById(nPerson.getId().toString());
        for (String tabNPersonBasic : nPerson.getTabNPersonBasic()) {
            nPersonMapper.addPersonAndBasic(nPerson.getId(),tabNPersonBasic);
        }
    }

    @Override
    public TabNPerson findPersonById(Integer personId) {
        return nPersonMapper.findPersonById(personId);
    }

    @Override
    public List<TabNPerson> findPersonsByPage(TabNPerson tabNPerson) {
        List<TabNPerson> tabNPersonList = nPersonMapper.findAllByPage(tabNPerson);
        tabNPersonList.forEach(t->{
            switch (t.getPType()){
                case 1:
                    t.setTypeName("群测群防");
                    break;
                case 2:
                    t.setTypeName("片区负责人");
                    break;
                case 3:
                    t.setTypeName("驻守人员");
                    break;
                case 4:
                    t.setTypeName("地环站人员");
                    break;
            }
            t.setTabNPersonBasic(nPersonMapper.findAllByPerson(t.getId()));
        });
        return tabNPersonList;
    }
}
