package com.cqndt.disaster.device.service.data;


import com.cqndt.disaster.device.entity.data.TabMsgRecord;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabMsgRecordVo;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface TabMsgRecordService {
    /*
    *根据参数动态查询
    * @parm tabMsgRecordVo
    * @return
    * */
    Result showCondition(TabMsgRecordVo tabMsgRecordVo);
    /**
     * 添加短信记录
     * @param tabMsgRecord
     * @return
     */
    int addTabMsgRecord(TabMsgRecord tabMsgRecord);
}
