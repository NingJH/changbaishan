package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabPerson;
import com.cqndt.disaster.device.entity.data.TabUnit;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabPersonVo;
import com.cqndt.disaster.device.vo.data.TabStaticTypeVo;

import java.util.List;
import java.util.Map;

public interface TabPersonService {

    /**
     * 根据单位id或者姓名查询单位所属人员
     * @return
     */
    List<Map<String,String>> queryTabPersonById(Integer unitId, String username);

    /**
     * 新增人员
     * @param TabPersonVo
     * @throws Exception
     */
    Result save(TabPersonVo TabPersonVo);

    /**
     * 查询人员信息列表
     * @param tabPersonVo 参数
     * @return 返回集合
     */
    List<TabPersonVo> list(TabPersonVo tabPersonVo);

    /**
     * 修改人员信息
     * @param tabPersonVo 参数
     * @return 返回受影响行数
     */
    Result update(TabPersonVo tabPersonVo) throws Exception;

    /**
     * 删除
     * @param tabPersonVo 对象
     */
    Result delete(TabPersonVo tabPersonVo) throws Exception;

    /**
     * 查询公司列表
     * @return
     */
    List<TabUnit> findUnitList();

    /**
     * 查询人员类型列表
     * @return
     */
    List<TabStaticTypeVo> findPersonTypeList();

    /**
     * 根据类型获取人员
     * @param type
     * @return
     */
    List<TabPerson> getPersonListByType(Integer type);
}
