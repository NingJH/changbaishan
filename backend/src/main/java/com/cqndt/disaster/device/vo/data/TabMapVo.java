package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabMap;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Description:
 * Date: 2019-08-02
 */
@Data
public class TabMapVo extends TabMap {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;
    /**
     * 图片
     */
    private Map<String,Object> MapImgs;

}
