package com.cqndt.disaster.device.sys.vo;

import com.cqndt.disaster.device.sys.entity.SysRole;
import lombok.Data;

/**
* Description: 表备注
* Date: 2019-03-14
*/
@Data
public class SysRoleVo extends SysRole {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;

    private String labelName1;

    private String id1;

    private String name1;

    private String deptName3;

    private String startCondition1;

    private String endCondition1;

}
