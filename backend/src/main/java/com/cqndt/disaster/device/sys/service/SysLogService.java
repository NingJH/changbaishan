package com.cqndt.disaster.device.sys.service;


import com.cqndt.disaster.device.sys.vo.SysLogVo;

/**
 * Created By marc
 * Date: 2019/3/18  15:11
 * Description:日志管理
 */
public interface SysLogService {
    /**
     * 增加
     * @param sysLogVo
     */
    void save(SysLogVo sysLogVo);
}
