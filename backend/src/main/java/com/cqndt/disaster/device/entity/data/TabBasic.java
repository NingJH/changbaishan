package com.cqndt.disaster.device.entity.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
* Description: 
* Date: 2019-06-04
*/

public class TabBasic implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  
     */
    private String id;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisNo() {
        return disNo;
    }

    public void setDisNo(String disNo) {
        this.disNo = disNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDisType() {
        return disType;
    }

    public void setDisType(String disType) {
        this.disType = disType;
    }

    public String getDisasterName() {
        return disasterName;
    }

    public void setDisasterName(String disasterName) {
        this.disasterName = disasterName;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getElevation() {
        return elevation;
    }

    public void setElevation(BigDecimal elevation) {
        this.elevation = elevation;
    }

    public String getDangerLevel() {
        return dangerLevel;
    }

    public void setDangerLevel(String dangerLevel) {
        this.dangerLevel = dangerLevel;
    }

    public String getDisasterLevel() {
        return disasterLevel;
    }

    public void setDisasterLevel(String disasterLevel) {
        this.disasterLevel = disasterLevel;
    }

    public String getStability() {
        return stability;
    }

    public void setStability(String stability) {
        this.stability = stability;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public Date getHappenTime() {
        return happenTime;
    }

    public void setHappenTime(Date happenTime) {
        this.happenTime = happenTime;
    }

    public BigDecimal getThreatenPeople() {
        return threatenPeople;
    }

    public void setThreatenPeople(BigDecimal threatenPeople) {
        this.threatenPeople = threatenPeople;
    }

    public BigDecimal getThreatenAssets() {
        return threatenAssets;
    }

    public void setThreatenAssets(BigDecimal threatenAssets) {
        this.threatenAssets = threatenAssets;
    }

    public BigDecimal getDeadPeople() {
        return deadPeople;
    }

    public void setDeadPeople(BigDecimal deadPeople) {
        this.deadPeople = deadPeople;
    }

    public String getDirectLoss() {
        return directLoss;
    }

    public void setDirectLoss(String directLoss) {
        this.directLoss = directLoss;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getThreatenHousehold() {
        return threatenHousehold;
    }

    public void setThreatenHousehold(Long threatenHousehold) {
        this.threatenHousehold = threatenHousehold;
    }

    public BigDecimal getThreatenArea() {
        return threatenArea;
    }

    public void setThreatenArea(BigDecimal threatenArea) {
        this.threatenArea = threatenArea;
    }

    public String getThreatenOther() {
        return threatenOther;
    }

    public void setThreatenOther(String threatenOther) {
        this.threatenOther = threatenOther;
    }

    public BigDecimal getBackElevation() {
        return backElevation;
    }

    public void setBackElevation(BigDecimal backElevation) {
        this.backElevation = backElevation;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public void setVolume(BigDecimal volume) {
        this.volume = volume;
    }

    public String getImgIds() {
        return imgIds;
    }

    public void setImgIds(String imgIds) {
        this.imgIds = imgIds;
    }

    public String getVideoIds() {
        return videoIds;
    }

    public void setVideoIds(String videoIds) {
        this.videoIds = videoIds;
    }

    public String getDangerLevelName() {
        return dangerLevelName;
    }

    public void setDangerLevelName(String dangerLevelName) {
        this.dangerLevelName = dangerLevelName;
    }

    public String getDisasterLevelName() {
        return disasterLevelName;
    }

    public void setDisasterLevelName(String disasterLevelName) {
        this.disasterLevelName = disasterLevelName;
    }

    public String getStabilityName() {
        return stabilityName;
    }

    public void setStabilityName(String stabilityName) {
        this.stabilityName = stabilityName;
    }

    public String getScaleName() {
        return scaleName;
    }

    public void setScaleName(String scaleName) {
        this.scaleName = scaleName;
    }

    /**
     *  灾害点编号
     */
    private String disNo;
    /**
     *  灾害点名称
     */
    private String name;
    /**
     *  乡镇编号
     */
    private String areaCode;
    /**
     *  地址
     */
    private String address;
    /**
     *  灾害点类型(1滑坡2泥石流3地面沉降4不稳定斜坡5地面塌陷6地裂缝7崩塌)
     */
    private String disType;
    /**
     *  灾害类型对应的中文名称
     */
    private String disasterName;
    /**
     *  经度
     */
    private BigDecimal longitude;
    /**
     *  纬度
     */
    private BigDecimal latitude;
    /**
     *  前缘高程
     */
    private BigDecimal elevation;
    /**
     *  险情等级(A特大型B大型C中型D小型)
     */
    private String dangerLevel;
    /**
     *  灾情等级(A:特大型,B:大型,C:中型,D:小型)
     */
    private String disasterLevel;
    /**
     *  稳定性(A稳定B基本稳定C不稳定D潜在不稳定)
     */
    private String stability;
    /**
     *  规模（1巨型,2大型,3中型,4小型）
     */
    private String scale;
    /**
     *  发生时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date happenTime;
    /**
     *  威胁人口/人
     */
    private BigDecimal threatenPeople;
    /**
     *  威胁财产/万元
     */
    private BigDecimal threatenAssets;
    /**
     *  死亡人数/人
     */
    private BigDecimal deadPeople;
    /**
     *  直接损失
     */
    private String directLoss;
    /**
     *  灾情介绍
     */
    private String remark;
    /**
     *  威胁户数/户
     */
    private Long threatenHousehold;
    /**
     *  威胁房屋面积/㎡
     */
    private BigDecimal threatenArea;
    /**
     *  威胁其他
     */
    private String threatenOther;
    /**
     *  后缘高程/m
     */
    private BigDecimal backElevation;
    /**
     *  面积
     */
    private BigDecimal area;
    /**
     *  体积
     */
    private BigDecimal volume;
    /**
     *  关联tab_attachment表的id
     */
    private String imgIds;
    /**
     *  关联tab_attachment表的id
     */
    private String videoIds;
    /**
     *  险情等级名称
     */
    private String dangerLevelName;
    /**
     *  灾情等级名称
     */
    private String disasterLevelName;
    /**
     *  稳定性名称
     */
    private String stabilityName;
    /**
     *  规模名称
     */
    private String scaleName;
    
}
