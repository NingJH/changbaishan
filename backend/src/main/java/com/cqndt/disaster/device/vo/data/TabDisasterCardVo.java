package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabDisasterCard;

/**
* Description: 
* Date: 2019-06-11
*/

public class TabDisasterCardVo extends TabDisasterCard {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
