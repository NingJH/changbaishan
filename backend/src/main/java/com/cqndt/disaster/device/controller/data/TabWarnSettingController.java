package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.service.data.TabWarnSettingService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabWarnSettingVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Description: 
* Date: 2019-06-11
*/
@RestController
@RequestMapping("tabWarnSetting")
@Api(description = "阈值" ,value = "阈值")
public class TabWarnSettingController {
    @Autowired
    private TabWarnSettingService tabWarnSettingService;

    @PostMapping("list")
    public Result listTabUser(@RequestBody TabWarnSettingVo tabWarnSettingVo) {
        PageHelper.startPage(tabWarnSettingVo.getPage(),tabWarnSettingVo.getLimit());
        PageInfo<TabWarnSettingVo> pageInfo = new PageInfo<TabWarnSettingVo>(tabWarnSettingService.list(tabWarnSettingVo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabWarnSettingById")
    public Result getTabWarnSettingById(Long id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        TabWarnSettingVo  tabWarnSettingVo = tabWarnSettingService.getTabWarnSettingById(id);
        return new Result().success(tabWarnSettingVo);
    }

    /**
     * 保存
     */
    @SysLog("增加")
    @PostMapping("save")
    @RequiresPermissions("sys:tabwarnsetting:save")
    public Result save(@RequestBody TabWarnSettingVo tabWarnSettingVo){
        Result result = new Result();
        try {
            tabWarnSettingService.save(tabWarnSettingVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("update")
    @RequiresPermissions("sys:tabwarnsetting:update")
    public Result update(@RequestBody TabWarnSettingVo tabWarnSettingVo){
        Result result = new Result();
        try {
            if(null == tabWarnSettingVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabWarnSettingService.update(tabWarnSettingVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabwarnsetting:delete")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabWarnSettingService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
       return result;
    }


}