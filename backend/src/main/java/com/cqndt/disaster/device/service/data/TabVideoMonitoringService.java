package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.vo.data.TabVideoMonitoringVo;

import java.util.List;
import java.util.Map;

public interface TabVideoMonitoringService {
    /**
     * 增加
     * @param tabVideoMonitoringVo
     */
    void save(TabVideoMonitoringVo tabVideoMonitoringVo) throws Exception;
     /**
     * 修改
     * @param tabVideoMonitoringVo
     */
     void update(TabVideoMonitoringVo tabVideoMonitoringVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabVideoMonitoringVo
     */
    TabVideoMonitoringVo getTabVideoMonitoringById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabVideoMonitoringVo 查询条件
     * @return 数据列表
     */
    List<TabVideoMonitoringVo> list(TabVideoMonitoringVo tabVideoMonitoringVo);

    /**
     * 下拉框list
     */
    List<Map<String,Object>> listTabArea(TabVideoMonitoringVo tabVideoMonitoringVo);
        /**
     * 级联上级信息查询
     */
    Map<String,Object> listTabAreaUp(TabVideoMonitoringVo tabVideoMonitoringVo);
        /**
     * 下拉框list
     */
    List<Map<String,Object>> listTabProject(TabVideoMonitoringVo tabVideoMonitoringVo);
        /**
     * 下拉框list
     */
    List<Map<String,Object>> listTabStaticType(TabVideoMonitoringVo tabVideoMonitoringVo);
    
    /**
      * 附件删除
      */
    void deleteTabVideoMonitoringAttachment(TabVideoMonitoringVo tabVideoMonitoringVo);
    /**
      * 获取附件
      */
    Map<String,Object> listTabVideoMonitoringAttachment(Long id);

    void saveVideo(TabVideoMonitoringVo tabVideoMonitoringVo);

    List<TabVideoMonitoringVo> selectVideo(TabVideoMonitoringVo tabVideoMonitoringVo);

    void updateVideo(TabVideoMonitoringVo tabVideoMonitoringVo);

    void deleteVideo(TabVideoMonitoringVo tabVideoMonitoringVo);

    void updateVideoMark(TabVideoMonitoringVo tabVideoMonitoringVo);
}