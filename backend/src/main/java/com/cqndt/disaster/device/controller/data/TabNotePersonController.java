package com.cqndt.disaster.device.controller.data;
import com.cqndt.disaster.device.service.data.TabNotePersonService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabNotePersonVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @version 1.0
 * @Author:hh
 * @Date:2019/8/5
 * @Description:com.cqndt.disaster.device.controller.data
 */
@RestController
@RequestMapping("/TabNotePerson")
@Api(value = "短信推送人员信息",description = "短信推送人员信息" )
public class TabNotePersonController {
    @Autowired
    private TabNotePersonService tabNotePersonService;

    //添加短信推送人员
    @ApiOperation(value = "添加人员信息", notes = "添加人员信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "name", value = "人员姓名", required = true,defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "phone", value = "电话", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "type", value = "告警类型(1.累计告警,2.相邻告警,3. 速率告警,4.任一告警都推送)", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "level", value = "告警等级", defaultValue = "", paramType = "query"),

    })
    @PostMapping("/saveTabNotePerson")
    //@RequiresPermissions("sys:noteperson:save")
    public Result saveTabNotePerson(@RequestBody TabNotePersonVo tabNotePersonVo){
        try{
            return tabNotePersonService.saveTabNotePerson(tabNotePersonVo);
        }catch (Exception e){
            e.printStackTrace();
            return new Result().failure(-1,e.getMessage());
        }
    }
    //查询全部人员
    @ApiOperation(value = "查询发送短信人员信息列表", notes = "查询发送短信人员信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "发送短信人员id", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "phone", value = "人员电话", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "name", value = "人员姓名", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "level", value = "告警等级", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "type", value = "告警类型(1.累计告警,2.相邻告警,3. 速率告警,4.任一告警都推送)", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "每页条数", defaultValue = "", required = true, paramType = "query")
    })
    @PostMapping("/queryTabNotePerson")
    //@RequiresPermissions("sys:noteperson:query")
    public Result queryTabNotePerson(@RequestBody TabNotePersonVo tabNotePersonVo){
        PageHelper.startPage(tabNotePersonVo.getPage(),tabNotePersonVo.getLimit());
        PageInfo<TabNotePersonVo> pageInfo=new PageInfo<>(tabNotePersonService.queryTabNotePerson(tabNotePersonVo));
        Result result=new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        result.setMsg("查询成功");
        return result;
    }
    //删除短信推送人员
    @ApiOperation(value = "删除人员信息", notes = "删除人员信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "人员id", defaultValue = "", required = true,paramType = "query"),
    })
    @PostMapping("/deleteTabNotePersonById")
    //@RequiresPermissions("sys:noteperson:delete")
    public Result deleteTabNotePersonById(String id) {
        Result result=new Result();

            int i= tabNotePersonService.deleteTabNotePersonById(id);
            if(i>0) {
                return result.success("删除成功");
            }
            return result.failure(-1,"删除失败");

    }
    //修改短信推送人员
    @ApiOperation(value = "修改人员信息", notes = "修改人员信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "人员id", required = true,defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "name", value = "人员姓名", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "phone", value = "电话", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "type", value = "告警类型(1.累计告警,2.相邻告警,3. 速率告警,4.任一告警都推送)", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "level", value = "告警等级", defaultValue = "", paramType = "query")
    })
    @PostMapping("/updateTabNotePersonById")
    //@RequiresPermissions("sys:noteperson:update")
    public Result updateTabNotePersonById(@RequestBody  TabNotePersonVo tabNotePersonVo){
        try{
            return tabNotePersonService.updateTabNotePersonById(tabNotePersonVo);
        }catch (Exception e){
            e.printStackTrace();
            return new Result().failure(-1,e.getMessage());
        }
    }

}
