package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabJHouVo;
import com.cqndt.disaster.device.vo.data.TabWarningUniteVo;
import com.cqndt.disaster.device.vo.data.TabjieshouVo;
import com.cqndt.disaster.device.vo.data.TabjsVo;

import java.util.List;

public interface TabWarningUniteService {
    /**
     * 联合预警列表
     */
    Result selectList(TabWarningUniteVo vo);
    /**
     * 新增联合预警
     */
    Result save(TabjieshouVo vo);
    /**
     * 修改联合预警
     */
    Result update(TabjieshouVo vo);
    /**
     * 删除联合预警
     */
    Result delete(String id);

    /**
     * 根据联合预警 id 获取其下所属 选中的项目、设备
     * 回显至选中设备填写阈值
     */
    Result getCheck(TabjieshouVo vo);

    /**
     * 选中设备进入阈值填写
     * @param vo
     * @return
     * @throws Exception
     */
    Result reciveDevice(TabJHouVo vo) throws Exception;
}
