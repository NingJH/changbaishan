package com.cqndt.disaster.device.entity.data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
* Description: 
* Date: 2019-06-11
*/

public class TabWarnSetting implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  
     */
    private Long id;
    /**
     *  初始值
     */
    private BigDecimal initialValue;
    /**
     *  相邻告警阈值1
     */
    private BigDecimal adAlarm1;
    /**
     *  相邻告警阈值2
     */
    private BigDecimal adAlarm2;
    /**
     *  相邻告警阈值3
     */
    private BigDecimal adAlarm3;
    /**
     *  相邻告警阈值4
     */
    private BigDecimal adAlarm4;
    /**
     *  累计告警1
     */
    private BigDecimal cuAlarm1;
    /**
     *  累计告警2
     */
    private BigDecimal cuAlarm2;
    /**
     *  累计告警3
     */
    private BigDecimal cuAlarm3;
    /**
     *  累计告警4
     */
    private BigDecimal cuAlarm4;
    /**
     *  
     */
    private Long monitorId;
    /**
     *  0 不发送,1发送 默认0
     */
    private Long isSend;
    /**
     *  多个号码用,隔开
     */
    private String alarmMobiles;
    /**
     *  传感器编号
     */
    private String sensorNo;
    /**
     *  速率告警1
     */
    private BigDecimal spAlarm1;
    /**
     *  速率告警2
     */
    private BigDecimal spAlarm2;
    /**
     *  速率告警3
     */
    private BigDecimal spAlarm3;
    /**
     *  速率告警4
     */
    private BigDecimal spAlarm4;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(BigDecimal initialValue) {
        this.initialValue = initialValue;
    }

    public BigDecimal getAdAlarm1() {
        return adAlarm1;
    }

    public void setAdAlarm1(BigDecimal adAlarm1) {
        this.adAlarm1 = adAlarm1;
    }

    public BigDecimal getAdAlarm2() {
        return adAlarm2;
    }

    public void setAdAlarm2(BigDecimal adAlarm2) {
        this.adAlarm2 = adAlarm2;
    }

    public BigDecimal getAdAlarm3() {
        return adAlarm3;
    }

    public void setAdAlarm3(BigDecimal adAlarm3) {
        this.adAlarm3 = adAlarm3;
    }

    public BigDecimal getAdAlarm4() {
        return adAlarm4;
    }

    public void setAdAlarm4(BigDecimal adAlarm4) {
        this.adAlarm4 = adAlarm4;
    }

    public BigDecimal getCuAlarm1() {
        return cuAlarm1;
    }

    public void setCuAlarm1(BigDecimal cuAlarm1) {
        this.cuAlarm1 = cuAlarm1;
    }

    public BigDecimal getCuAlarm2() {
        return cuAlarm2;
    }

    public void setCuAlarm2(BigDecimal cuAlarm2) {
        this.cuAlarm2 = cuAlarm2;
    }

    public BigDecimal getCuAlarm3() {
        return cuAlarm3;
    }

    public void setCuAlarm3(BigDecimal cuAlarm3) {
        this.cuAlarm3 = cuAlarm3;
    }

    public BigDecimal getCuAlarm4() {
        return cuAlarm4;
    }

    public void setCuAlarm4(BigDecimal cuAlarm4) {
        this.cuAlarm4 = cuAlarm4;
    }

    public Long getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Long monitorId) {
        this.monitorId = monitorId;
    }

    public Long getIsSend() {
        return isSend;
    }

    public void setIsSend(Long isSend) {
        this.isSend = isSend;
    }

    public String getAlarmMobiles() {
        return alarmMobiles;
    }

    public void setAlarmMobiles(String alarmMobiles) {
        this.alarmMobiles = alarmMobiles;
    }

    public String getSensorNo() {
        return sensorNo;
    }

    public void setSensorNo(String sensorNo) {
        this.sensorNo = sensorNo;
    }

    public BigDecimal getSpAlarm1() {
        return spAlarm1;
    }

    public void setSpAlarm1(BigDecimal spAlarm1) {
        this.spAlarm1 = spAlarm1;
    }

    public BigDecimal getSpAlarm2() {
        return spAlarm2;
    }

    public void setSpAlarm2(BigDecimal spAlarm2) {
        this.spAlarm2 = spAlarm2;
    }

    public BigDecimal getSpAlarm3() {
        return spAlarm3;
    }

    public void setSpAlarm3(BigDecimal spAlarm3) {
        this.spAlarm3 = spAlarm3;
    }

    public BigDecimal getSpAlarm4() {
        return spAlarm4;
    }

    public void setSpAlarm4(BigDecimal spAlarm4) {
        this.spAlarm4 = spAlarm4;
    }
}
