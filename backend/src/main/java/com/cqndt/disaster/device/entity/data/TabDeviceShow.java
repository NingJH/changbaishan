package com.cqndt.disaster.device.entity.data;

import lombok.Data;


@Data
public class TabDeviceShow {

    private Integer id;
    private String deviceNo;
    private Integer showType;
    private Double originalValue;

}
