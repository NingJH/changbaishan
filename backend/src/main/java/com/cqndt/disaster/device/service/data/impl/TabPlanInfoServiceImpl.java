package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabPlanInfoMapper;
import com.cqndt.disaster.device.service.data.TabPlanInfoService;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.vo.data.TabPlanInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class TabPlanInfoServiceImpl implements TabPlanInfoService {
    @Autowired
    private TabPlanInfoMapper tabPlanInfoMapper;

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(TabPlanInfoVo tabPlanInfoVo) throws Exception {
        tabPlanInfoMapper.save(tabPlanInfoVo);
    
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void update(TabPlanInfoVo tabPlanInfoVo) throws Exception {
        TabPlanInfoVo vo = (TabPlanInfoVo) tabPlanInfoMapper.getTabPlanInfoById(tabPlanInfoVo.getId());
        AttachmentVo attachmentVo = new AttachmentVo();
        tabPlanInfoMapper.update(tabPlanInfoVo);
     
   }
    @Override
    public Map<String,Object> getTabPlanInfoById(Long id){
        return tabPlanInfoMapper.getTabPlanInfoById(id);
    }
    @Override
    public void delete(String id){
         String[] ids = id.split(",");
         for (String idStr:ids) {
            tabPlanInfoMapper.delete(Integer.parseInt(idStr));
         }
    }
    @Override
    public List<TabPlanInfoVo> list(TabPlanInfoVo tabPlanInfoVo) {
        return  tabPlanInfoMapper.list(tabPlanInfoVo);
    }



}