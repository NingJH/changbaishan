package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.entity.data.TabDeviceTemplate;
import com.cqndt.disaster.device.service.data.TabDeviceTemplateService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabDeviceTemplateVo;
import com.cqndt.disaster.device.vo.data.TabMonitorVo;
import com.cqndt.disaster.device.vo.data.TabProjectVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @User : ningjunhao+
 * @Mail : 15823526611@139.com
 * @Time : 2019-08-06 11:17
 **/
@RestController
@RequestMapping("TabDeviceTemplate")
public class TabDeviceTemplateController {

    @Autowired
    TabDeviceTemplateService tabDeviceTemplateService;

    @SysLog("保存设备模板")
    @PostMapping("saveTabDeviceTemplate")
    @ApiOperation(value = "增加设备模板", notes = "增加设备模板")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "int", name = "typeValue", value = "设备类型值", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "typeName", value = "设备类型名称", defaultValue = "",required = true,  paramType = "query"),
            @ApiImplicitParam(dataType = "int", name = "mark", value = "标识", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "markName", value = "标识名称", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "int", name = "sequence", value = "序列", defaultValue = "",  paramType = "query")
    })
    public Result saveTabDeviceTemplate(@RequestBody List<TabDeviceTemplateVo> tabDeviceTemplateList){
        try {
            if(tabDeviceTemplateList.isEmpty()){
                return new Result().failure(-1, "标识信息不能为空");
            }
            for(TabDeviceTemplateVo tabDeviceTemplate : tabDeviceTemplateList){
                if(StringUtils.isEmpty(tabDeviceTemplate.getTypeValue().toString())){
                    return new Result().failure(-1, "设备类型值不能为空");
                }else if(StringUtils.isEmpty(tabDeviceTemplate.getTypeName())){
                    return new Result().failure(-1, "设备类型名称不能为空");
                }else if(StringUtils.isEmpty(tabDeviceTemplate.getMark())){
                    return new Result().failure(-1, "标识不能为空");
                }else if(StringUtils.isEmpty(tabDeviceTemplate.getMarkName())){
                    return new Result().failure(-1, "标识名称不能为空");
                }
                tabDeviceTemplate.setAddDate(new Date());
                tabDeviceTemplate.setAlterDate(new Date());
                tabDeviceTemplateService.saveTabDeviceTemplate(tabDeviceTemplate);
            }
            Result result = new Result();
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result().failure(-1, "操作失败");
        }
        return new Result().success();
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("updateTabDeviceTemplate")
    //@RequiresPermissions("sys:tabmonitor:update")
    @ApiOperation(value = "修改设备模板", notes = "修改设备模板")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "int", name = "typeValue", value = "设备类型值", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "typeName", value = "设备类型名称", defaultValue = "",required = true,  paramType = "query"),
            @ApiImplicitParam(dataType = "int", name = "mark", value = "标识", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "markName", value = "标识名称", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "int", name = "sequence", value = "序列", defaultValue = "",  paramType = "query")
    })
    public Result updateTabDeviceTemplate(@RequestBody TabDeviceTemplateVo TabDeviceTemplate) {
        Result result = new Result();
        try {
            if (null == TabDeviceTemplate.getId()) {
                return result.failure(-1, "请选择至少一条数据进行操作");
            }
            TabDeviceTemplate.setAlterDate(new Date());
            tabDeviceTemplateService.updateTabDeviceTemplate(TabDeviceTemplate);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }


    @SysLog("查询安装布置点")
    @PostMapping("listTabDeviceTemplate")
//    @RequiresPermissions("sys:tabmonitor:list")
    @ApiOperation(value = "设备模板列表", notes = "设备模板列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "typeName", value = "设备类型名称", defaultValue = "",  paramType = "query"),
    })
    public Result listTabDeviceTemplate(@RequestBody TabDeviceTemplateVo tabDeviceTemplate) {
        PageHelper.startPage(tabDeviceTemplate.getPage(), tabDeviceTemplate.getLimit());
        PageInfo<TabDeviceTemplateVo> pageInfo = new PageInfo<TabDeviceTemplateVo>(tabDeviceTemplateService.listTabDeviceTemplate(tabDeviceTemplate));
        Result result = new Result();
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    @SysLog("删除")
    @PostMapping("deleteTabDeviceTemplate")
    @RequiresPermissions("sys:tabmonitor:delete")
    @ApiOperation(value = "删除设备模板",notes = "删除设备模板")
    @ApiImplicitParam(name = "id", value="布置点id", dataType = "String", required = true, paramType="query")
    public Result deleteTabDeviceTemplate(@RequestBody TabDeviceTemplateVo tabDeviceTemplate) {
        Result result = new Result();
        try {
            if (null == tabDeviceTemplate.getId()) {
                return result.failure(-1, "请选择至少一条数据进行操作");
            }
            tabDeviceTemplateService.deleteTabDeviceTemplate(tabDeviceTemplate.getId());
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 根据项目获取设备模板
     * @param tabProjectVo
     * @return
     */
    @PostMapping("getTabDeviceTemplate")
    @ApiOperation(value = "根据项目获取设备模板",notes = "根据项目获取设备模板")
    public Result getTabDeviceTemplate(@RequestBody  List<TabProjectVo> tabProjectVo){
        Result result = new Result();
        result.setData(tabDeviceTemplateService.getTabDeviceTemplate(tabProjectVo));
        return result;
    }
}
