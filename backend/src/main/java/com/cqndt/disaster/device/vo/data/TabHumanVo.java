package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabHuman;
import lombok.Data;

/**
* Description: 
* Date: 2019-06-05
*/
@Data
public class TabHumanVo extends TabHuman {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;


}
