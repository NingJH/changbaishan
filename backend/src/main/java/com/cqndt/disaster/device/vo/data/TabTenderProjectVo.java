package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabTenderProject;
import lombok.Data;

/**
* Description: 招标项目管理
* Date: 2019-06-04
*/
@Data
public class TabTenderProjectVo extends TabTenderProject{
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;

    private String staticName;


    private String staticNum;
}
