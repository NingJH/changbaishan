package com.cqndt.disaster.device.sys.controller;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.sys.service.SysUserService;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import com.cqndt.disaster.device.util.AbstractController;
import com.cqndt.disaster.device.util.Result;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/3/11  15:13
 * Description:用户管理
 */
@RestController
@RequestMapping("sysUser")
public class SysUserController extends AbstractController {
    @Autowired
    private SysUserService sysUserService;

    @PostMapping("list")
    public Result listTabUser(@RequestBody SysUserVo sysUserVo) {
        if(null == sysUserVo.getLevel()){
            SysUserVo userVo = getUser();
            sysUserVo.setLevel(userVo.getLevel());
            sysUserVo.setAreaCode(userVo.getAreaCode());
        }
        PageHelper.startPage(sysUserVo.getPage(),sysUserVo.getLimit());
        PageInfo<SysUserVo> pageInfo = new PageInfo<SysUserVo>(sysUserService.list(sysUserVo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabUserById")
    public Result getTabUserById(Long id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        SysUserVo  sysUserVo = sysUserService.getTabUserById(id);
        return new Result().success(sysUserVo);
    }

    /**
     * 保存
     */
    @SysLog("增加用户")
    @PostMapping("save")
    @RequiresPermissions("sys:tabuser:save")
    public Result save(@RequestBody SysUserVo sysUserVo){
        Result result = new Result();
        try {
            result = sysUserService.checkValidate(sysUserVo,0);
            if(result.getCode()!=0) return result;
            sysUserService.save(sysUserVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改用户")
    @PostMapping("update")
    @RequiresPermissions("sys:tabuser:update")
    public Result update(@RequestBody SysUserVo sysUserVo){
        Result result = new Result();
        try {
            if(null == sysUserVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            result = sysUserService.checkValidate(sysUserVo,1);
            if(result.getCode()!=0) return result;
            sysUserService.update(sysUserVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除用户")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabuser:delete")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            sysUserService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }


    /**
     * 下拉框list
     */
    @PostMapping("listTabStaticType")
    public Result listTabStaticType(SysUserVo sysUserVo){
        Result result = new Result();
        try {
            List<Map<String,Object>> list = sysUserService.listTabStaticType(sysUserVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }



    /**
     * 子表信息
     */
    @PostMapping("listTabRoleAll")
    public Result listTabRoleAll(SysUserVo sysUserVo){
        Result result = new Result();
        try {
            List<SysUserVo> list = sysUserService.listTabRoleAll(sysUserVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 子表信息
     */
    @PostMapping("listTabAreaAll")
    public Result listTabAreaAll(SysUserVo sysUserVo){
        Result result = new Result();
        try {
            List<SysUserVo> list = sysUserService.listTabAreaAll(sysUserVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 子表已选择信息
     */
    @PostMapping("listTabRoleSelect")
    public Result listTabRoleSelect(Integer id){
        Result result = new Result();
        try {
            List<SysUserVo> list = sysUserService.listTabRoleSelect(id);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 子表已选择信息
     */
    @PostMapping("listTabAreaSelect")
    public Result listTabAreaSelect(Integer id){
        Result result = new Result();
        try {
            List<SysUserVo> list = sysUserService.listTabAreaSelect(id);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }
    /**
     * 修改密码
     */
    @SysLog("修改密码")
    @PostMapping("upPassword")
    public Result upPassword(@RequestBody SysUserVo sysUserVo){
        Result result = new Result();
        try {
            Long id = getUserId();
            result = sysUserService.upPassword(id,sysUserVo.getOldPass(),sysUserVo.getNewPass());
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }
    /**
     * 重置密码
     */
    @SysLog("重置密码")
    @PostMapping("resetPassword")
    public Result resetPassword(@RequestBody SysUserVo sysUserVo){
        Result result = new Result();
        try {
            result = sysUserService.resetPass(sysUserVo.getId(),sysUserVo.getNewPass());
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }
    /**
     * 项目分配列表
     */
    @PostMapping("assignProjectList")
    @ApiOperation(value = "根据用户id分配项目", notes = "根据用户id分配项目")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "用户id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result assignProjectList(SysUserVo vo){
        Result result = new Result();
        try {
           result = sysUserService.assignProjectList(vo);
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }
    /**
     * 用户已分配项目
     */
    @PostMapping("assignedProjectList")
    @ApiOperation(value = "用户已分配项目", notes = "用户已分配项目")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "用户id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result assignedProjectList(Long id){
        Result result = new Result();
        try {
            result = sysUserService.assignedProjectList(id);
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"查询失败");
        }
        return result;
    }
    /**
     * 分配项目
     */
    @PostMapping("assignProject")
    @ApiOperation(value = "用户所分配项目", notes = "用户所分配项目")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Long", name = "id", value = "用户id", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectIds", value = "项目id(多个逗号隔开)", defaultValue = "1", required = true, paramType = "query")
    })
    public Result assignProject(Long id,String projectIds){
        Result result = new Result();
        try {
            result = sysUserService.assignProject(id,projectIds);
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 区域级联查询
     */
    @PostMapping("cascadeSelectArea")
    @ApiOperation(value = "区域级联查询", notes = "区域级联查询")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaParent", value = "父级id（默认为0）", defaultValue = "0", required = true, paramType = "query")
    })
    public Result cascadeSelectArea(String areaParent){
        Result result = new Result();
        try {
            result = sysUserService.cascadeSelectArea(areaParent);
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }
    /**
     * 区域回写
     */
    @PostMapping("cascadeSelectedArea")
    @ApiOperation(value = "区域回写", notes = "区域回写")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "用户id", defaultValue = "0", required = true, paramType = "query")
    })
    public Result cascadeSelectedArea(Integer id){
        Result result = new Result();
        try {
            result = sysUserService.cascadeSelectedArea(id);
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }



}
