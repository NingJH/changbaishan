package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabBasic;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import com.cqndt.disaster.device.vo.data.TabBasicVo;

import java.util.List;
import java.util.Map;

public interface TabBasicService {
    /**
     * 增加
     *
     * @param tabBasicVo
     * @param
     */
    void save(TabBasicVo tabBasicVo) throws Exception;

    /**
     * 修改
     *
     * @param tabBasicVo
     */
    void update(TabBasicVo tabBasicVo) throws Exception;

    /**
     * 根据id获取信息
     *
     * @param id
     * @return tabBasicVo
     */
    TabBasicVo getTabBasicById(String id);

    /**
     * 删除
     *
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     *
     * @param tabBasicVo 查询条件
     * @return 数据列表
     */
    List<Map<String,Object>> list(TabBasicVo tabBasicVo,String areaCode,List<TabArea> listMenu);


    /**
     * 子表信息
     */
    List<TabBasicVo> listTabDisasterCardAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabHedgeCardAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabPlanInfoAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabAreaAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabHumanAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabPhotographyAll(TabBasicVo tabBasicVo);

    /**
     * 子表信息
     */
    List<TabBasicVo> listTabAttachmentAll(TabBasicVo tabBasicVo);

    /**
     * 已选择子表信息
     */
    List<TabBasicVo> listTabDisasterCardSelect(Integer id);

    /**
     * 已选择子表信息
     */
    List<TabBasicVo> listTabHumanSelect(Integer id);

    List<Map<String, Object>> listTabAreaInfo(String areaId);

    List<Map<String,Object>> listTabPerson(String personId);

    /**
     * 根据灾害点编号查询
     * @param disNo
     * @return
     */
    List<Map<String,Object>> getBaseForDisNo(String disNo);


    List<TabBasic> findAllBasic();
}