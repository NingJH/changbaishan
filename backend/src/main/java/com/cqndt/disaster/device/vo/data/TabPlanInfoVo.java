package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabPlanInfo;
import lombok.Data;

/**
* Description: 表备注
* Date: 2019-06-11
*/
@Data
public class TabPlanInfoVo extends TabPlanInfo {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;


}
