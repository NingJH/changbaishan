package com.cqndt.disaster.device.entity.data;

import java.io.Serializable;

/**
* Description: 
* Date: 2019-06-11
*/

public class TabHedgeCard implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  
     */
    private String id;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String gethFamilyName() {
        return hFamilyName;
    }

    public void sethFamilyName(String hFamilyName) {
        this.hFamilyName = hFamilyName;
    }

    public String gethFamilyNum() {
        return hFamilyNum;
    }

    public void sethFamilyNum(String hFamilyNum) {
        this.hFamilyNum = hFamilyNum;
    }

    public String gethHouseType() {
        return hHouseType;
    }

    public void sethHouseType(String hHouseType) {
        this.hHouseType = hHouseType;
    }

    public String gethAddress() {
        return hAddress;
    }

    public void sethAddress(String hAddress) {
        this.hAddress = hAddress;
    }

    public String gethDisType() {
        return hDisType;
    }

    public void sethDisType(String hDisType) {
        this.hDisType = hDisType;
    }

    public String gethDisScale() {
        return hDisScale;
    }

    public void sethDisScale(String hDisScale) {
        this.hDisScale = hDisScale;
    }

    public String gethDisRelationship() {
        return hDisRelationship;
    }

    public void sethDisRelationship(String hDisRelationship) {
        this.hDisRelationship = hDisRelationship;
    }

    public String gethDisFactor() {
        return hDisFactor;
    }

    public void sethDisFactor(String hDisFactor) {
        this.hDisFactor = hDisFactor;
    }

    public String gethDisMatters() {
        return hDisMatters;
    }

    public void sethDisMatters(String hDisMatters) {
        this.hDisMatters = hDisMatters;
    }

    public String gethPreMan() {
        return hPreMan;
    }

    public void sethPreMan(String hPreMan) {
        this.hPreMan = hPreMan;
    }

    public String gethPrePhone() {
        return hPrePhone;
    }

    public void sethPrePhone(String hPrePhone) {
        this.hPrePhone = hPrePhone;
    }

    public String gethPreSignal() {
        return hPreSignal;
    }

    public void sethPreSignal(String hPreSignal) {
        this.hPreSignal = hPreSignal;
    }

    public String gethSignalMan() {
        return hSignalMan;
    }

    public void sethSignalMan(String hSignalMan) {
        this.hSignalMan = hSignalMan;
    }

    public String gethSignalPhone() {
        return hSignalPhone;
    }

    public void sethSignalPhone(String hSignalPhone) {
        this.hSignalPhone = hSignalPhone;
    }

    public String gethEvaLine() {
        return hEvaLine;
    }

    public void sethEvaLine(String hEvaLine) {
        this.hEvaLine = hEvaLine;
    }

    public String gethEvaPlacement() {
        return hEvaPlacement;
    }

    public void sethEvaPlacement(String hEvaPlacement) {
        this.hEvaPlacement = hEvaPlacement;
    }

    public String gethPlacementMan() {
        return hPlacementMan;
    }

    public void sethPlacementMan(String hPlacementMan) {
        this.hPlacementMan = hPlacementMan;
    }

    public String gethPlacementPhone() {
        return hPlacementPhone;
    }

    public void sethPlacementPhone(String hPlacementPhone) {
        this.hPlacementPhone = hPlacementPhone;
    }

    public String gethAmbulanceUnit() {
        return hAmbulanceUnit;
    }

    public void sethAmbulanceUnit(String hAmbulanceUnit) {
        this.hAmbulanceUnit = hAmbulanceUnit;
    }

    public String gethAmbulanceMan() {
        return hAmbulanceMan;
    }

    public void sethAmbulanceMan(String hAmbulanceMan) {
        this.hAmbulanceMan = hAmbulanceMan;
    }

    public String gethAmbulancePhone() {
        return hAmbulancePhone;
    }

    public void sethAmbulancePhone(String hAmbulancePhone) {
        this.hAmbulancePhone = hAmbulancePhone;
    }

    public String gethGrantUnit() {
        return hGrantUnit;
    }

    public void sethGrantUnit(String hGrantUnit) {
        this.hGrantUnit = hGrantUnit;
    }

    public String gethGrantPhone() {
        return hGrantPhone;
    }

    public void sethGrantPhone(String hGrantPhone) {
        this.hGrantPhone = hGrantPhone;
    }

    public String gethGrantDate() {
        return hGrantDate;
    }

    public void sethGrantDate(String hGrantDate) {
        this.hGrantDate = hGrantDate;
    }

    public String gethHolder() {
        return hHolder;
    }

    public void sethHolder(String hHolder) {
        this.hHolder = hHolder;
    }

    public String gethHolderPhone() {
        return hHolderPhone;
    }

    public void sethHolderPhone(String hHolderPhone) {
        this.hHolderPhone = hHolderPhone;
    }

    public String gethHolderDate() {
        return hHolderDate;
    }

    public void sethHolderDate(String hHolderDate) {
        this.hHolderDate = hHolderDate;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getName3() {
        return name3;
    }

    public void setName3(String name3) {
        this.name3 = name3;
    }

    public String getName4() {
        return name4;
    }

    public void setName4(String name4) {
        this.name4 = name4;
    }

    public String getName5() {
        return name5;
    }

    public void setName5(String name5) {
        this.name5 = name5;
    }

    public String getName6() {
        return name6;
    }

    public void setName6(String name6) {
        this.name6 = name6;
    }

    public String getName7() {
        return name7;
    }

    public void setName7(String name7) {
        this.name7 = name7;
    }

    public String getSex1() {
        return sex1;
    }

    public void setSex1(String sex1) {
        this.sex1 = sex1;
    }

    public String getSex2() {
        return sex2;
    }

    public void setSex2(String sex2) {
        this.sex2 = sex2;
    }

    public String getSex3() {
        return sex3;
    }

    public void setSex3(String sex3) {
        this.sex3 = sex3;
    }

    public String getSex4() {
        return sex4;
    }

    public void setSex4(String sex4) {
        this.sex4 = sex4;
    }

    public String getSex5() {
        return sex5;
    }

    public void setSex5(String sex5) {
        this.sex5 = sex5;
    }

    public String getSex6() {
        return sex6;
    }

    public void setSex6(String sex6) {
        this.sex6 = sex6;
    }

    public String getSex7() {
        return sex7;
    }

    public void setSex7(String sex7) {
        this.sex7 = sex7;
    }

    public String getName8() {
        return name8;
    }

    public void setName8(String name8) {
        this.name8 = name8;
    }

    public String getSex8() {
        return sex8;
    }

    public void setSex8(String sex8) {
        this.sex8 = sex8;
    }

    public Long getAge1() {
        return age1;
    }

    public void setAge1(Long age1) {
        this.age1 = age1;
    }

    public Long getAge2() {
        return age2;
    }

    public void setAge2(Long age2) {
        this.age2 = age2;
    }

    public Long getAge3() {
        return age3;
    }

    public void setAge3(Long age3) {
        this.age3 = age3;
    }

    public Long getAge4() {
        return age4;
    }

    public void setAge4(Long age4) {
        this.age4 = age4;
    }

    public Long getAge5() {
        return age5;
    }

    public void setAge5(Long age5) {
        this.age5 = age5;
    }

    public Long getAge6() {
        return age6;
    }

    public void setAge6(Long age6) {
        this.age6 = age6;
    }

    public Long getAge7() {
        return age7;
    }

    public void setAge7(Long age7) {
        this.age7 = age7;
    }

    public Long getAge8() {
        return age8;
    }

    public void setAge8(Long age8) {
        this.age8 = age8;
    }

    public String gethNo() {
        return hNo;
    }

    public void sethNo(String hNo) {
        this.hNo = hNo;
    }

    /**

     *  户主姓名
     */
    private String hFamilyName;
    /**
     *  家庭人数
     */
    private String hFamilyNum;
    /**
     *  房屋类型
     */
    private String hHouseType;
    /**
     *  家庭住址
     */
    private String hAddress;
    /**
     *  灾害类型
     */
    private String hDisType;
    /**
     *  灾害规模
     */
    private String hDisScale;
    /**
     *  灾害体与本住户的位置关系
     */
    private String hDisRelationship;
    /**
     *  灾害诱发因素
     */
    private String hDisFactor;
    /**
     *  本住户注意事项
     */
    private String hDisMatters;
    /**
     *  监测人
     */
    private String hPreMan;
    /**
     *  联系电话
     */
    private String hPrePhone;
    /**
     *  预警信号
     */
    private String hPreSignal;
    /**
     *  预警信号发布人
     */
    private String hSignalMan;
    /**
     *  联系电话
     */
    private String hSignalPhone;
    /**
     *  撤离路线
     */
    private String hEvaLine;
    /**
     *  安置单位地点
     */
    private String hEvaPlacement;
    /**
     *  负责人
     */
    private String hPlacementMan;
    /**
     *  联系电话
     */
    private String hPlacementPhone;
    /**
     *  救护单位
     */
    private String hAmbulanceUnit;
    /**
     *  负责人
     */
    private String hAmbulanceMan;
    /**
     *  联系电话
     */
    private String hAmbulancePhone;
    /**
     *  本卡发放单位
     */
    private String hGrantUnit;
    /**
     *  联系电话
     */
    private String hGrantPhone;
    /**
     *  日期
     */
    private String hGrantDate;
    /**
     *  户主签名
     */
    private String hHolder;
    /**
     *  联系电话
     */
    private String hHolderPhone;
    /**
     *  日期
     */
    private String hHolderDate;
    /**
     *  
     */
    private String name1;
    /**
     *  
     */
    private String name2;
    /**
     *  
     */
    private String name3;
    /**
     *  
     */
    private String name4;
    /**
     *  
     */
    private String name5;
    /**
     *  
     */
    private String name6;
    /**
     *  
     */
    private String name7;
    /**
     *  
     */
    private String sex1;
    /**
     *  
     */
    private String sex2;
    /**
     *  
     */
    private String sex3;
    /**
     *  
     */
    private String sex4;
    /**
     *  
     */
    private String sex5;
    /**
     *  
     */
    private String sex6;
    /**
     *  
     */
    private String sex7;
    /**
     *  
     */
    private String name8;
    /**
     *  
     */
    private String sex8;
    /**
     *  
     */
    private Long age1;
    /**
     *  
     */
    private Long age2;
    /**
     *  
     */
    private Long age3;
    /**
     *  
     */
    private Long age4;
    /**
     *  
     */
    private Long age5;
    /**
     *  
     */
    private Long age6;
    /**
     *  
     */
    private Long age7;
    /**
     *  
     */
    private Long age8;
    /**
     *  灾害点编号(与tab_basic表dis_no关联）
     */
    private String hNo;
    
}
