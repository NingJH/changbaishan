package com.cqndt.disaster.device.sys.vo;

import com.cqndt.disaster.device.sys.entity.SysLogEntity;
import lombok.Data;

/**
 * Created By marc
 * Date: 2019/3/18  15:12
 * Description:
 */
@Data
public class SysLogVo extends SysLogEntity {
}
