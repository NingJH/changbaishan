//package com.cqndt.disaster.device.util;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.servlet.http.HttpServletResponse;
//
///**
// * Created By marc
// * Date: 2019/2/14  9:51
// * Description:文件上传、文件预览
// */
//@Controller
//public class FileController {
//    private static Logger logger = LoggerFactory.getLogger(FileController.class);
//    @Value("attachmentUrlSet")
//    private String attachmentUrlSet;
//    @Value("tempAttachmentUrl")
//    private String tempAttachmentUrl;
//    @Value("open-office-path")
//    private String openOfficePath;
//    @Value("open-office-host")
//    private String openOfficeHost;
//    /**
//     * 文件上传
//     * @param file 上传的文件
//     * @param attachmentType 文件类型（1.图片 2.视频 3.文件）
//     * @return
//     */
//    @ResponseBody
//    @RequestMapping(value = "fileUpload", method = RequestMethod.POST)
//    public Result fileUpload(MultipartFile file, String attachmentType) {
//        Result result = FileUpload.fileUpload(file,attachmentType,tempAttachmentUrl);
//        return result;
//    }
//
//    /**
//     * 预览文件
//     * @param attachmentUrl 文件路径
//     * @param attachmentType 文件类型
//     * @param response
//     */
//    @RequestMapping(value = "readByIo", method = RequestMethod.GET)
//    public void readByIo(String attachmentUrl,String attachmentType,HttpServletResponse response) {
//        try {
//            FileView.readByIo(attachmentUrl, attachmentType, response,attachmentUrlSet,openOfficePath,openOfficeHost,0);
//        }catch (Exception e){
//            logger.error("文件预览失败");
//            e.printStackTrace();
//        }
//    }
//    /**
//     * 文件下载
//     * @param attachmentUrl 文件路径
//     * @param attachmentType 文件类型
//     * @param response
//     */
//    @RequestMapping(value = "fileDownLoad", method = RequestMethod.GET)
//    public void fileDownLoad(String attachmentUrl,String attachmentType,HttpServletResponse response) {
//        try {
//            FileView.readByIo(attachmentUrl, attachmentType, response,attachmentUrlSet,openOfficePath,openOfficeHost,1);
//        }catch (Exception e){
//            logger.error("文件下载失败");
//            e.printStackTrace();
//        }
//    }
//}
