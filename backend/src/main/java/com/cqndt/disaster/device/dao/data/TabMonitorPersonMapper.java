package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabMonitorPerson;
import com.cqndt.disaster.device.vo.data.TabMonitorPersonVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface TabMonitorPersonMapper {
    /**
     * 查询所有监测人
     * @return
     */
    List<TabMonitorPersonVo> getMonitorPerson(TabMonitorPersonVo tabMonitorPersonVo);

    List<Map<String, Object>> getMonitorPersonList(TabMonitorPersonVo tabMonitorPersonVo);

    int addMonitorPerson(TabMonitorPersonVo vo);

    int updateMonitorPerson(TabMonitorPersonVo vo);

    int deleteMonitorPerson(int id);
    /**
     * 给单位地址赋值
     */
    int updateAddress(@Param("Address") String address, @Param("id") String id);



}
