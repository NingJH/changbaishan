package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.service.data.TabDisasterCardService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabDisasterCardVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 * Date: 2019-06-11
 */
@RestController
@RequestMapping("tabDisasterCard")
@Api(description = "防灾明白卡", value = "防灾明白卡")
public class TabDisasterCardController {
    @Autowired
    private TabDisasterCardService tabDisasterCardService;

    @PostMapping("list")
    public Result listTabUser(@RequestBody TabDisasterCardVo tabDisasterCardVo) {
        PageHelper.startPage(tabDisasterCardVo.getPage(), tabDisasterCardVo.getLimit());
        PageInfo<TabDisasterCardVo> pageInfo = new PageInfo<TabDisasterCardVo>(tabDisasterCardService.list(tabDisasterCardVo));
        Result result = new Result();
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    /**
     * 信息
     */
    @PostMapping("getTabDisasterCardById")
    public Result getTabDisasterCardById(@RequestBody TabDisasterCardVo disasterCardVo) {
        if (null == disasterCardVo.getdNo()) {
            return new Result().failure(-1, "请选择一条数据进行操作");
        }
        return new Result().success(tabDisasterCardService.getTabDisasterCardById(disasterCardVo.getdNo()));
    }

    /**
     * 保存
     */
    @SysLog("增加")
    @PostMapping("save")
    @RequiresPermissions("sys:tabdisastercard:save")
    public Result save(@RequestBody TabDisasterCardVo tabDisasterCardVo) {
        Result result = new Result();
        try {
            tabDisasterCardService.save(tabDisasterCardVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("update")
    @RequiresPermissions("sys:tabdisastercard:update")
    public Result update(@RequestBody TabDisasterCardVo tabDisasterCardVo) {
        Result result = new Result();
        try {
            if (null == tabDisasterCardVo.getId()) {
                tabDisasterCardService.save(tabDisasterCardVo);
            }else {
                tabDisasterCardService.update(tabDisasterCardVo);
            }
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabdisastercard:delete")
    public Result delete(String id) {
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1, "请选择至少一条数据进行操作");
            }
            tabDisasterCardService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }


}