package com.cqndt.disaster.device.entity.data;

import java.io.Serializable;

/**
* Description: 
* Date: 2019-06-11
*/

public class TabSensor implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  
     */
    private Long id;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSensorNo() {
        return sensorNo;
    }

    public void setSensorNo(String sensorNo) {
        this.sensorNo = sensorNo;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    /**
     *  传感器编号
     */
    private String sensorNo;
    /**
     *  传感器名称
     */
    private String sensorName;
    /**
     *  传感器类型
     */
    private String sensorType;
    /**
     *  设备编号
     */
    private String deviceNo;
    
}
