package com.cqndt.disaster.device.influxdb;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Frog
 */
@Configuration
public class InfluxdbConfig {

    @Value("${spring.influx.username}")
    private String username;
    @Value("${spring.influx.password}")
    private String password;
    @Value("${spring.influx.url}")
    private String url;
    @Value("${spring.influx.database}")
    private String database;

    @Bean
    public InfluxDBConnect getInfluxDBConnect() {
        InfluxDBConnect influxDBConnect = new InfluxDBConnect(username, password, url, database);
        influxDBConnect.influxDbBuild();
        return influxDBConnect;
    }

}
