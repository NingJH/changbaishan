package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.vo.data.TabSensorVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * Description: 
 * Date: 2019-06-11
 */

@Mapper
public interface TabSensorMapper {
    /**
     * 根据条件查询list
     * @param tabSensorVo 查询条件
     */
    List<TabSensorVo> list(TabSensorVo tabSensorVo);
    /**
     * 增加
     * @param tabSensorVo 条件
     */
    int save(TabSensorVo tabSensorVo);
    /**
     * 修改
     * @param tabSensorVo 条件
     */
    int update(TabSensorVo tabSensorVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);

    /**
     * 根据id查询单个记录
     * @param id
     */
    TabSensorVo getTabSensorById(Long id);


    List<TabSensorVo> getTabDeviceInstallByDeviceNo(String deviceNo);

    /**
     * 根据设备集合获取传感器
     * @param deviceNoList
     * @return
     */
    List<TabSensorVo> getTabDeviceInstallByDeviceNoList(List<String> deviceNoList);

    /**
     * 根据设备删除传感器
     * @param deviceNo
     */
    void deleteBySensorNo(String deviceNo);

}
