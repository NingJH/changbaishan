package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.vo.data.TabBasicHumanVo;

import java.util.List;

public interface TabBasicHumanService {
    /**
     * 增加
     * @param tabBasicHumanVo
     */
    void save(TabBasicHumanVo tabBasicHumanVo) throws Exception;
     /**
     * 修改
     * @param tabBasicHumanVo
     */
     void update(TabBasicHumanVo tabBasicHumanVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabBasicHumanVo
     */
    TabBasicHumanVo getTabBasicHumanById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabBasicHumanVo 查询条件
     * @return 数据列表
     */
    List<TabBasicHumanVo> list(TabBasicHumanVo tabBasicHumanVo);



}