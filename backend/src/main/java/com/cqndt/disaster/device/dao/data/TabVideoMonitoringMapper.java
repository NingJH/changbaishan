package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.vo.data.TabVideoMonitoringVo;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;
import java.util.List;


/**
 * Description: 视频监控管理
 * Date: 2019-05-31
 */

@Mapper
public interface TabVideoMonitoringMapper {
    /**
     * 根据条件查询list
     * @param tabVideoMonitoringVo 查询条件
     */
    List<TabVideoMonitoringVo> list(TabVideoMonitoringVo tabVideoMonitoringVo);
    /**
     * 增加
     * @param tabVideoMonitoringVo 条件
     */
    int save(TabVideoMonitoringVo tabVideoMonitoringVo);
    /**
     * 修改
     * @param tabVideoMonitoringVo 条件
     */
    int update(TabVideoMonitoringVo tabVideoMonitoringVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    TabVideoMonitoringVo getTabVideoMonitoringById(Long id);
     /**
      * 下拉框列表
      */
     List<Map<String,Object>> listTabArea(TabVideoMonitoringVo tabVideoMonitoringVo);
        /**
      * 级联上级信息查询
      */
     Map<String,Object> listTabAreaUp(TabVideoMonitoringVo tabVideoMonitoringVo);

     /**
      * 下拉框列表
      */
     List<Map<String,Object>> listTabProject(TabVideoMonitoringVo tabVideoMonitoringVo);
         /**
      * 下拉框列表
      */
     List<Map<String,Object>> listTabStaticType(TabVideoMonitoringVo tabVideoMonitoringVo);
        /**
      * 附件保存
      */
    int saveTabVideoMonitoringAttachment(AttachmentVo attachmentVo);
    /**
      * 附件删除
      */
    int deleteTabVideoMonitoringAttachment(AttachmentVo attachmentVo);
    /**
      * 获取附件
      */
    List<AttachmentVo> listTabVideoMonitoringAttachment(AttachmentVo attachmentVo);


    void saveVideo(TabVideoMonitoringVo tabVideoMonitoringVo);

    List<TabVideoMonitoringVo> selectVideo(TabVideoMonitoringVo tabVideoMonitoringVo);

    void updateVideo(TabVideoMonitoringVo tabVideoMonitoringVo);

    void updateVideoMark(@Param("serial") String serial,@Param("mark")String mark);

    void deleteVideo(String id);

    void deleteImg(String id);
}
