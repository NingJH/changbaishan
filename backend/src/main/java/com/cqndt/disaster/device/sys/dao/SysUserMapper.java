package com.cqndt.disaster.device.sys.dao;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/3/6  17:27
 * Description:
 */
@Mapper
public interface SysUserMapper{
    /**
     * 根据条件查询list
     * @param sysUserVo 查询条件
     */
    List<SysUserVo> list(SysUserVo sysUserVo);
    /**
     * 增加
     * @param sysUserVo 条件
     */
    int save(SysUserVo sysUserVo);
    /**
     * 修改
     * @param sysUserVo 条件
     */
    int update(SysUserVo sysUserVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    SysUserVo getTabUserById(Long id);
    /**
     * 验证是否重复
     * @param sysUserVo 条件
     */
    int checkValidate1(SysUserVo sysUserVo);

    /**
     * 下拉框列表
     */
    List<Map<String,Object>> listTabStaticType(SysUserVo sysUserVo);
    /**
     * 关联表删除
     */
    int deleteTabUserRole(String userId);

    /**
     * 关联表删除
     */
    int deleteTabUserArea(String userId);

    /**
     * 行政区划图删除
     */
    int deleteTabMapservice(String userId);

    /**
     * 中间表保存
     */
    int saveTabUserRole(@Param("userId") String userId, @Param("roleId") String roleId);

    /**
     * 中间表保存
     */
    int saveTabUserArea(@Param("userId") String userId, @Param("areaId") String areaId);

    /**
     * 行政区划图保存
     */
    int saveTabMapservice(@Param("userId") String userId, @Param("xzqhurl") String xzqhurl);

    /**
     * 子表信息
     */
    List<SysUserVo> listTabRoleAll(SysUserVo sysUserVo);

    /**
     * 子表信息
     */
    List<SysUserVo> listTabAreaAll(SysUserVo sysUserVo);

    /**
     * 已选子表信息
     */
    List<SysUserVo> listTabRoleSelect(Integer id);

    /**
     * 已选子表信息
     */
    List<SysUserVo> listTabAreaSelect(Integer id);
    /**
     * 根据用户名和密码查询用户信息
     */
    SysUserVo getByUserNameAndPass(SysUserVo sysUserVo);

    /**
     * 根据用户id查询对应的角色id
     * @param userId
     * @return
     */
    String getRoleIdByUserId(Long userId);

    String getAreaIdByUserId(Long userId);

    List<Map<String,Object>> assignProjectList(SysUserVo sysUserVo);

    List<Map<String,Object>> assignedProjectList(String id);
    /**
     *  用户分配项目删除
     */
    int deleteTabUserProject(String userId);
    /**
     * 增加用户分配的项目
     */
    int insertTabUserProject(@Param("userId")String userId,@Param("projectId")String projectId);

    String cascadeSelectedArea(@Param("level")String level, @Param("areaId")String areaId);
}
