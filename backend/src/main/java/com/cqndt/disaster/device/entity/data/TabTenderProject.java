package com.cqndt.disaster.device.entity.data;

import java.io.Serializable;

/**
* Description: 招标项目管理
* Date: 2019-06-04
*/


public class TabTenderProject implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    /**
     *  
     */

    private Long id;
    /**
     *  项目名称
     */
    private String name;
    /**
     *  所属单位
     */
    private Long unitId;
    
}
