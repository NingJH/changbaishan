package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.service.data.TabSensorService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabSensorVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Description: 
* Date: 2019-06-11
*/
@RestController
@RequestMapping("tabSensor")
@Api(description = "传感器" ,value = "传感器")
public class TabSensorController {
    @Autowired
    private TabSensorService tabSensorService;

    @SysLog("查询传感器")
    @RequiresPermissions("sys:tabsensor:list")
    @PostMapping("list")
    public Result listTabUser(@RequestBody TabSensorVo tabSensorVo) {
        PageHelper.startPage(tabSensorVo.getPage(),tabSensorVo.getLimit());
        PageInfo<TabSensorVo> pageInfo = new PageInfo<TabSensorVo>(tabSensorService.list(tabSensorVo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabSensorById")
    public Result getTabSensorById(Long id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        TabSensorVo  tabSensorVo = tabSensorService.getTabSensorById(id);
        return new Result().success(tabSensorVo);
    }

    /**
     * 保存
     */
    @SysLog("增加")
    @PostMapping("save")
    @RequiresPermissions("sys:tabsensor:save")
    public Result save(@RequestBody TabSensorVo tabSensorVo){
        Result result = new Result();
        try {
            tabSensorService.save(tabSensorVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("update")
    @RequiresPermissions("sys:tabsensor:update")
    public Result update(@RequestBody TabSensorVo tabSensorVo){
        Result result = new Result();
        try {
            if(null == tabSensorVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabSensorService.update(tabSensorVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabsensor:delete")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabSensorService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
       return result;
    }


}