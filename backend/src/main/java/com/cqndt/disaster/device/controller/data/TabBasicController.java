package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.service.data.TabAreaService;
import com.cqndt.disaster.device.service.data.TabBasicService;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import com.cqndt.disaster.device.util.AbstractController;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabBasicVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Description:
 * Date: 2019-06-04
 */
@RestController
@RequestMapping("tabBasic")
@Api(description = "隐患点" ,value = "隐患点")
public class TabBasicController extends AbstractController{

    @Autowired
    private TabBasicService tabBasicService;

    @Autowired
    private TabAreaService tabAreaService;

    @SysLog("查询隐患点")
    @PostMapping("list")
    //@RequiresPermissions("sys:tabbasic:list")
    public Result listTabUser(@RequestBody TabBasicVo tabBasicVo) {
        SysUserVo sysUserVo = getUser();
        List<TabArea> listMenu = tabAreaService.TabAreaList(sysUserVo);
        PageHelper.startPage(tabBasicVo.getPage(), tabBasicVo.getLimit());
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(tabBasicService.list(tabBasicVo,sysUserVo.getAreaCode(),listMenu));
        Result result = new Result();
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    /**
     * 信息
     */
    @PostMapping("getTabBasicById")
    public Result getTabBasicById(String id) {
        if (null == id) {
            return new Result().failure(-1, "请选择一条数据进行操作");
        }
        TabBasicVo tabBasicVo = tabBasicService.getTabBasicById(id);
        return new Result().success(tabBasicVo);
    }

    /**
     * 保存
     */
    @SysLog("增加")
    @PostMapping("save")
    //@RequiresPermissions("sys:tabbasic:save")
    public Result save(@RequestBody TabBasicVo tabBasicVo) {
        Result result = new Result();
        List<Map<String,Object>> list = tabBasicService.getBaseForDisNo(tabBasicVo.getDisNo());
        if(list.size() != 0){
            return result.failure(-1, "灾害点编号不能重复");
        }
        try {
            String[] arr = tabBasicVo.getLonLat().split(",");
            BigDecimal longitude =new BigDecimal(arr[0]);
            tabBasicVo.setLongitude(longitude);
            BigDecimal lagitude =new BigDecimal(arr[1]);
            tabBasicVo.setLatitude(lagitude);
            tabBasicService.save(tabBasicVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("update")
    //@RequiresPermissions("sys:tabbasic:update")
    public Result update(@RequestBody TabBasicVo tabBasicVo) {
        Result result = new Result();
        try {
            if (null == tabBasicVo.getId()) {
                return result.failure(-1, "请选择至少一条数据进行操作");
            }
            String[] arr = tabBasicVo.getLonLat().split(",");
            BigDecimal longitude =new BigDecimal(arr[0]);
            tabBasicVo.setLongitude(longitude);
            BigDecimal lagitude =new BigDecimal(arr[1]);
            tabBasicVo.setLatitude(lagitude);
            tabBasicService.update(tabBasicVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
    //@RequiresPermissions("sys:tabbasic:delete")
    public Result delete(@RequestBody TabBasicVo tabBasicVo) {
        Result result = new Result();
        try {
            if (null == tabBasicVo.getId()) {
                return result.failure(-1, "请选择至少一条数据进行操作");
            }
            tabBasicService.delete(tabBasicVo.getId());
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }


    /**
     * 子表信息
     */
    @PostMapping("listTabDisasterCardAll")
    public Result listTabDisasterCardAll(TabBasicVo tabBasicVo) {
        Result result = new Result();
        try {
            List<TabBasicVo> list = tabBasicService.listTabDisasterCardAll(tabBasicVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 子表信息
     */
    @PostMapping("listTabHedgeCardAll")
    public Result listTabHedgeCardAll(TabBasicVo tabBasicVo) {
        Result result = new Result();
        try {
            List<TabBasicVo> list = tabBasicService.listTabHedgeCardAll(tabBasicVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 子表信息
     */
    @PostMapping("listTabPlanInfoAll")
    public Result listTabPlanInfoAll(TabBasicVo tabBasicVo) {
        Result result = new Result();
        try {
            List<TabBasicVo> list = tabBasicService.listTabPlanInfoAll(tabBasicVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 子表信息
     */
    @PostMapping("listTabAreaAll")
    public Result listTabAreaAll(TabBasicVo tabBasicVo) {
        Result result = new Result();
        try {
            List<TabBasicVo> list = tabBasicService.listTabAreaAll(tabBasicVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 子表信息
     */
    @PostMapping("listTabHumanAll")
    public Result listTabHumanAll(TabBasicVo tabBasicVo) {
        Result result = new Result();
        try {
            List<TabBasicVo> list = tabBasicService.listTabHumanAll(tabBasicVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 子表信息
     */
    @PostMapping("listTabPhotographyAll")
    public Result listTabPhotographyAll(TabBasicVo tabBasicVo) {
        Result result = new Result();
        try {
            List<TabBasicVo> list = tabBasicService.listTabPhotographyAll(tabBasicVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 子表信息
     */
    @PostMapping("listTabAttachmentAll")
    public Result listTabAttachmentAll(TabBasicVo tabBasicVo) {
        Result result = new Result();
        try {
            List<TabBasicVo> list = tabBasicService.listTabAttachmentAll(tabBasicVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 子表已选择信息
     */
    @PostMapping("listTabDisasterCardSelect")
    public Result listTabDisasterCardSelect(Integer id) {
        Result result = new Result();
        try {
            List<TabBasicVo> list = tabBasicService.listTabDisasterCardSelect(id);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 子表已选择信息
     */
    @PostMapping("listTabHumanSelect")
    public Result listTabHumanSelect(Integer id) {
        Result result = new Result();
        try {
            List<TabBasicVo> list = tabBasicService.listTabHumanSelect(id);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    @GetMapping("listTabAreaInfo")
    public Result listTabAreaInfo(String areaId) {
        Result result = new Result();
        try {
            List<Map<String, Object>> list = tabBasicService.listTabAreaInfo(areaId);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    @GetMapping("listTabPerson")
    public Result listTabPerson(String personId) {
        Result result = new Result();
        try {
            List<Map<String, Object>> list = tabBasicService.listTabPerson(personId);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }


    @ApiOperation("查询灾害点信息")
    @GetMapping("findAllBasic")
    public Result findAllBasic(){
        return new Result().success(tabBasicService.findAllBasic());
    }
}