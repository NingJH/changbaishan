package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.vo.data.TabMonitorVo;
import org.apache.ibatis.annotations.Param;

import java.awt.geom.Area;
import java.util.List;
import java.util.Map;

public interface TabMonitorService {
    /**
     * 增加
     * @param tabMonitorVo
     */
    void save(TabMonitorVo tabMonitorVo) throws Exception;
     /**
     * 修改
     * @param tabMonitorVo
     */
     void update(TabMonitorVo tabMonitorVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabMonitorVo
     */
    TabMonitorVo getTabMonitorById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabMonitorVo 查询条件
     * @return 数据列表
     */
    List<TabMonitorVo> list(TabMonitorVo tabMonitorVo);


    List<Map<String, Object>>  getUserProjectInfo(Long userId);

    List<Map<String,Object>> listTabAreaInfo(String areaId);

    /**
     * 根据项目id查询设备
     * @param deviceId
     * @return
     */
    List<Map<String,String>> getDeviceNameList(String deviceId, List<TabArea> list);
}