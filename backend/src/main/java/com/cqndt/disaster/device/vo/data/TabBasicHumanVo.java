package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabBasicHuman;
import lombok.Data;

/**
* Description: 
* Date: 2019-06-06
*/
@Data
public class TabBasicHumanVo extends TabBasicHuman {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;


}
