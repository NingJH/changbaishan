package com.cqndt.disaster.device.sys.service;

import com.cqndt.disaster.device.sys.vo.SysMenuVo;
import com.cqndt.disaster.device.util.Result;

import java.util.List;
import java.util.Map;

public interface SysMenuService {

    /**
     * 导航菜单
     */
    List<Map<String,Object>> navigationList(String userId, String frontBack);
    /**
     * 增加
     * @param sysMenuVo
     */
    void save(SysMenuVo sysMenuVo) throws Exception;
    /**
     * 修改
     * @param sysMenuVo
     */
    void update(SysMenuVo sysMenuVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return sysMenuVo
     */
    SysMenuVo getTabMenuById(String id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    Result delete(String id);

    /**
     * 校验
     */
    Result checkData(SysMenuVo sysMenuVo);

    /**
     * 列表
     * @param sysMenuVo 查询条件
     * @return 数据列表
     */
    List<SysMenuVo> list(SysMenuVo sysMenuVo);

    /**
     * 下拉框list
     */
    List<Map<String,Object>> listTabMenu(SysMenuVo sysMenuVo);

    /**
     * 子表信息
     */
    List<SysMenuVo> listTabRoleAll(SysMenuVo sysMenuVo);
    /**
     * 已选择子表信息
     */
    List<SysMenuVo> listTabRoleSelect(Integer id);

    /**
     * 下拉框list
     */
    List<Map<String,Object>> listTabStaticType(SysMenuVo sysMenuVo);
}