package com.cqndt.disaster.device.vo.data;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class TabProDevVo {

    private String id;

    private String label;
    /**
     * 项目下所属设备
     */
    private List<Map<String, Object>> children;
}
