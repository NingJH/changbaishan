package com.cqndt.disaster.device.controller.data;


import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.entity.data.TabDeviceCheck;
import com.cqndt.disaster.device.service.data.TabDeviceCheckService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabDeviceCheckVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("tabDerviceCheck")
@Api(description = "巡查记录" ,value = "巡查记录")
public class TabDeviceCheckController {

    @Autowired
    private TabDeviceCheckService tabDeviceCheckService;

    @SysLog("查询巡查记录")
    @PostMapping("list")
    @ApiOperation(value = "查询巡查记录",notes = "查询巡查记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceNo", value="设备编号", dataType = "String",required = true,paramType="query"),
    })
    //@RequiresPermissions("sys:tabDerviceCheck:list")
    public Result list(@RequestBody TabDeviceCheckVo tabDeviceCheckVo){
        Result result = new Result();
        PageHelper.startPage(tabDeviceCheckVo.getPage(),tabDeviceCheckVo.getLimit());
        PageInfo<TabDeviceCheckVo> pageInfo = new PageInfo<>(tabDeviceCheckService.list(tabDeviceCheckVo));
        result.setMsg("操作成功");
        result.setCode(0);
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    @SysLog("新增巡查记录")
    @PostMapping("save")
    @ApiOperation(value = "新增巡查记录",notes = "新增巡查记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceNo", value="设备编号", dataType = "String",required = true,paramType="query"),
    })
    //@RequiresPermissions("sys:tabDerviceCheck:save")
    public Result save(@RequestBody TabDeviceCheckVo tabDeviceCheckVo){
        return tabDeviceCheckService.save(tabDeviceCheckVo);
    }

    @SysLog("修改巡查记录")
    @PostMapping("update")
    @ApiOperation(value = "修改巡查记录",notes = "修改巡查记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value="巡查id", dataType = "String",required = true,paramType="query"),
    })
    //@RequiresPermissions("sys:tabDerviceCheck:update")
    public Result update(@RequestBody TabDeviceCheckVo tabDeviceCheckVo){
        return tabDeviceCheckService.update(tabDeviceCheckVo);
    }

    @SysLog("删除巡查记录")
    @PostMapping("delete")
    @ApiOperation(value = "删除巡查记录",notes = "删除巡查记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value="巡查id", dataType = "String",required = true,paramType="query"),
    })
    //@RequiresPermissions("sys:tabDerviceCheck:delete")
    public Result delete(String id){
        return tabDeviceCheckService.delete(id);
    }

}
