package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabDeviceTemplate;
import com.cqndt.disaster.device.entity.data.TabWarningDevice;
import com.cqndt.disaster.device.vo.data.TabDeviceTemplateVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

@Mapper
public interface TabDeviceTemplateMapper {

    void saveTabDeviceTemplate(TabDeviceTemplate tabDeviceTemplate);

    void updateTabDeviceTemplate(TabDeviceTemplateVo tabDeviceTemplate);

    List<TabDeviceTemplateVo> listTabDeviceTemplate(TabDeviceTemplateVo tabDeviceTemplate);

    void deleteTabDeviceTemplate(Integer id);

    List<TabDeviceTemplateVo> getTabDeviceTemplateByDeviceValue(@Param("deviceType")String deviceType );

    List<TabWarningDevice> getTabDeviceTemplateByDeviceValue1(String deviceType);

    List<Integer> getAllTabDeviceTemplate();
}
