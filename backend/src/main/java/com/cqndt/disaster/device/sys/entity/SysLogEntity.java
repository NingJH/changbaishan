package com.cqndt.disaster.device.sys.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created By marc
 * Date: 2019/3/18  15:08
 * Description:日志管理
 */
@Data
public class SysLogEntity implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;
    private Long id;
    //用户名
    private String username;
    //用户操作
    private String operation;
    //请求方法
    private String method;
    //请求参数
    private String params;
    //执行时长(毫秒)
    private Long time;
    //IP地址
    private String ip;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createDate;

    @Override
    public int hashCode() {
        return id.hashCode();
    }
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SysLogEntity) {
            Long targetId = ((SysLogEntity) obj).getId();
            return id.equals(targetId);
        }
        return super.equals(obj);
    }
}
