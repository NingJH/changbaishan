package com.cqndt.disaster.device.sys.controller;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.sys.service.SysRoleService;
import com.cqndt.disaster.device.sys.vo.SysRoleVo;
import com.cqndt.disaster.device.util.Result;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* Description: 角色管理
* Date: 2019-03-14
*/
@RestController
@RequestMapping("sysRole")
public class SysRoleController {
    @Autowired
    private SysRoleService sysRoleService;

    @PostMapping("list")
    public Result listTabUser(@RequestBody SysRoleVo sysRoleVo) {
        PageHelper.startPage(sysRoleVo.getPage(),sysRoleVo.getLimit());
        PageInfo<SysRoleVo> pageInfo = new PageInfo<SysRoleVo>(sysRoleService.list(sysRoleVo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabRoleById")
    public Result getTabRoleById(Long id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        SysRoleVo  sysRoleVo = sysRoleService.getTabRoleById(id);
        return new Result().success(sysRoleVo);
    }
    /**
     * 信息
     */
    @PostMapping("getSelectedMenuById")
    public Result getSelectedMenuById(Long id){
        if(null == id){
            return new Result().failure(-1,"id不能为空");
        }
        List<String> listFront = sysRoleService.getCheckedMenuById(id,"2");
        List<String> listBack = sysRoleService.getCheckedMenuById(id,"1");
        Map<String,Object> map = new HashMap<>();
        map.put("listFront",listFront);
        map.put("listBack",listBack);
        return new Result().success(map);
    }

    /**
     * 保存
     */
    @SysLog("保存角色")
    @PostMapping("save")
    @RequiresPermissions("sys:tabrole:save")
    public Result save(@RequestBody SysRoleVo sysRoleVo){
        Result result = new Result();
        try {
            result = sysRoleService.checkValidate(sysRoleVo,0);
            if(result.getCode()!=0) return result;
            sysRoleService.save(sysRoleVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改角色")
    @PostMapping("update")
    @RequiresPermissions("sys:tabrole:update")
    public Result update(@RequestBody SysRoleVo sysRoleVo){
        Result result = new Result();
        try {
            if(null == sysRoleVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            result = sysRoleService.checkValidate(sysRoleVo,1);
            if(result.getCode()!=0) return result;
            sysRoleService.update(sysRoleVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除角色")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabrole:delete")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            sysRoleService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
       return result;
    }


    /**
     * 下拉框list
     */
    @PostMapping("listTabDept")
    public Result listTabDept(SysRoleVo sysRoleVo){
        Result result = new Result();
        try {
            List<Map<String,Object>> list = sysRoleService.listTabDept(sysRoleVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }
}