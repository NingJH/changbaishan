package com.cqndt.disaster.device.entity.data;

import java.io.Serializable;

/**
* Description: 
* Date: 2019-06-06
*/

public class TabBasicHuman implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  
     */
    private Long id;
    /**
     *  人员id
     */
    private Long humanId;
    /**
     *  隐患点编号
     */
    private String diasaterNo;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHumanId() {
        return humanId;
    }

    public void setHumanId(Long humanId) {
        this.humanId = humanId;
    }

    public String getDiasaterNo() {
        return diasaterNo;
    }

    public void setDiasaterNo(String diasaterNo) {
        this.diasaterNo = diasaterNo;
    }
}
