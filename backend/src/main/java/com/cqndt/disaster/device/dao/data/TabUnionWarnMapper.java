package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabUnion;
import com.cqndt.disaster.device.entity.data.TabWarningDevice;
import com.cqndt.disaster.device.entity.data.TabWarningUnite;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 联合告警dao层
 * @author liaohonglai
 * @date 2019/8/6
 */
@Mapper
public interface TabUnionWarnMapper {

    /**
     * 获取任务表数据列表
     * @return
     */
    List<TabUnion> getUnionList();

    /**
     * 删除以获取得任务数据列表
     * @param unionList
     * @return
     */
    int deleteUnion(List<TabUnion> unionList);

    /**
     * 获取类型所在联合设备阈值列表
     * @param identify
     * @return
     */
    List<TabWarningUnite> getIdentifyInDevice(@Param("identify") String identify,@Param("sensorNo") String sensorNo);

    /**
     * 以联合表id查询所有设备数据
     * @param warningUniteId
     * @return
     */
    List<TabWarningDevice> getUniteIdInDevcie(Integer warningUniteId);

    /**
     * 获取gnss 传感器24小时最大值
     * @param identify
     * @param date
     * @param sensorType
     * @return
     */
    Double getGnss24HourMax(@Param("identify") String identify, @Param("sensorType") String sensorType, @Param("date") Date date);

    /**
     * 获取yl 传感器24小时最大值
     * @param identify
     * @param date
     * @return
     */
    Double getYl24HourMax(@Param("identify") String identify,@Param("date") Date date);

    /**
     * 其他(地裂缝等等)数据24小时历史最大值
     * @param identify
     * @param deviceNo
     * @param date
     * @return
     */
    Double getOther24HourMax(@Param("identify") String identify,@Param("deviceNo") String deviceNo,@Param("date") Date date);

    /**
     * 获取短信发送人电话字符串，逗号拼接
     * @param i
     * @return
     */
    String getMsgPerson(int i);

    /**
     * 联合告警存储
     * @param max 告警等级
     * @param date 发生时间
     * @param id 联合告警编号
     * @param message 告警短信内容
     * @return
     */
    int saveAlarmInfo(@Param("max") Integer max,@Param("date") Date date,@Param("id") Integer id,@Param("message") String message);

    /**
     * 记录短信发送记录到列表tab_msg_record
     * @param phoneArray
     * @param message
     * @return
     */
    Integer saveMsgRecord(@Param("phoneArray") String[] phoneArray,@Param("message") String message,@Param("date") Date date);

    Integer updateWarning(@Param("warningUniteId") Integer warningUniteId,@Param("date") Date date);
}
