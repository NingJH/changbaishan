package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.vo.data.TabDisasterCardVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


/**
 * Description: 
 * Date: 2019-06-11
 */

@Mapper
public interface TabDisasterCardMapper {
    /**
     * 根据条件查询list
     * @param tabDisasterCardVo 查询条件
     */
    List<TabDisasterCardVo> list(TabDisasterCardVo tabDisasterCardVo);
    /**
     * 增加
     * @param tabDisasterCardVo 条件
     */
    int save(TabDisasterCardVo tabDisasterCardVo);
    /**
     * 修改
     * @param tabDisasterCardVo 条件
     */
    int update(TabDisasterCardVo tabDisasterCardVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param disNo
     */
    Map<String,Object> getTabDisasterCardById(String disNo);


}
