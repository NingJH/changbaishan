package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabSensor;
import lombok.Data;

/**
* Description: 
* Date: 2019-06-11
*/
@Data
public class TabSensorVo extends TabSensor {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;


}
