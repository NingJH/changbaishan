package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabVideoMonitoring;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
* Description: 视频监控管理
* Date: 2019-05-31
*/
@Data
public class TabVideoMonitoringVo extends TabVideoMonitoring{
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;

    private List<AttachmentVo> attachmentList;

    private String attachmentId;

    private String cascaderLevel3;
    private String areaName2;

    private String projectName12;

    private String staticName13;


    private String areaParent;
    private String staticNum;

    private List<Map<String, Object>> filesInfo;

    private String deviceInstallInfo;

}
