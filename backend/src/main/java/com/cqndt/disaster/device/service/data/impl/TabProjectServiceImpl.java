package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.*;
import com.cqndt.disaster.device.entity.data.*;
import com.cqndt.disaster.device.service.data.TabAttachmentService;
import com.cqndt.disaster.device.service.data.TabProjectService;
import com.cqndt.disaster.device.util.ExportExcelUtil;
import com.cqndt.disaster.device.util.FileConverter;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabProDevVo;
import com.cqndt.disaster.device.vo.data.TabProjectVo;
import com.cqndt.disaster.device.vo.data.TabUnitVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created By marc
 * Date: 2019/6/5  11:33
 * Description:
 */
@Service
@Slf4j
public class TabProjectServiceImpl implements TabProjectService {
    @Autowired
    private TabProjectMapper tabProjectMapper;
    @Autowired
    private TabAttachmentMapper tabAttachmentMapper;
    @Autowired
    private TabDeviceInstallMapper tabDeviceInstallMapper;
    @Autowired
    private TabDeviceMapper tabDeviceMapper;
    @Autowired
    private TabTwoRoundMapper tabTwoRoundMapper;
    @Autowired
    private TabPhotographyMapper tabPhotographyMapper;
    @Autowired
    private TabAreaMapper tabAreaMapper;
    @Autowired
    private TabTenderProjectMapper tabTenderProjectMapper;
    @Autowired
    private TabBasicMapper tabBasicMapper;
    @Autowired
    private TabUserProjectMapper tabUserProjectMapper;
    @Autowired
    private TabProjectCjdwMapper tabProjectCjdwMapper;
    @Autowired
    private TabProjectXmbzMapper tabProjectXmbzMapper;
    @Autowired
    private TabProjectDatumMapper tabProjectDatumMapper;
    @Autowired
    private TabReportingFileMapper tabReportingFileMapper;
    @Autowired
    private TabAttachmentService tabAttachmentService;
    @Autowired
    private TabDeviceTemplateMapper tabDeviceTemplateMapper;

    @Autowired
    TabUnitMapper tabUnitMapper;

    @Value("${file-ip}")
    private String fileIp;
    @Value("attachmentUrlSet")
    private String attachmentUrlSet;
    @Value("tempAttachmentUrl")
    private String tempAttachmentUrl;

    @Value("${open-office-path}")
    private String openOfficePath;
    @Value("${open-office-host}")
    private String openOfficeHost;


    @Override
    public List<TabProjectVo> listProject(TabProjectVo vo) {
        List<TabProjectVo> list = tabProjectMapper.listProject(vo);
        if (null != list && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                List<Map<String, Object>> list1 = new ArrayList<>();
                String imgIds = list.get(i).getProjectImg();
                if (StringUtils.isNotEmpty(imgIds)) {
                    String[] arr = imgIds.split(",");
                    for (int k = 0; k < arr.length; k++) {
                        if (StringUtils.isNotEmpty(arr[k])) {
                            Map<String, Object> map = tabDeviceInstallMapper.getImgUrlAndById(arr[k]);
                            if (map != null) {
                                map.put("viewUrl", fileIp + File.separator + "img" + File.separator + map.get("attachmentUrl"));
                                list1.add(map);

                            }
                        } else {
                            break;
                        }
                    }
                }
                List<Map<String, Object>> list2 = new ArrayList<>();
                String videosIds = list.get(i).getProjectVideo();
                if (StringUtils.isNotEmpty(videosIds)) {
                    String[] arr = videosIds.split(",");
                    for (int k = 0; k < arr.length; k++) {
                        if (StringUtils.isNotEmpty(arr[k])) {
                            Map<String, Object> map = tabDeviceInstallMapper.getImgUrlAndById(arr[k]);
                            if (map != null) {
                                map.put("viewUrl", fileIp + File.separator + "video" + File.separator + map.get("attachmentUrl"));
                                list2.add(map);
                            }
                        } else {
                            break;
                        }
                    }
                }
                list.get(i).setProjectImgs(list1);
                list.get(i).setProjectVideos(list2);
                list.get(i).setTwoRounds(tabTwoRoundMapper.getByRelationAndType(list.get(i).getProjectNo(), "2"));
                list.get(i).setPhotography(tabPhotographyMapper.getByRelationAndType(list.get(i).getProjectNo(), "2"));

            }
        }
        return list;
    }

    @Override
    public TabProjectVo getTabProjectById(Integer id) {
        TabProjectVo vo = tabProjectMapper.getTabProjectById(id);
        //获取图片
        if (null != vo.getProjectImg()) {
            List<TabAttachment> imgs = tabAttachmentMapper.getTabAttachmentByIds(vo.getProjectImg().split(","));
            for (TabAttachment tabAttachment : imgs) {
                tabAttachment.setAttachmentUrl(fileIp + File.separator + "img" + File.separator + tabAttachment.getAttachmentUrl());
            }
            vo.setImgs(imgs);
        }
        //获取视频
        if (null != vo.getProjectVideo()) {
            List<TabAttachment> videos = tabAttachmentMapper.getTabAttachmentByIds(vo.getProjectVideo().split(","));
            for (TabAttachment tabAttachment : videos) {
                tabAttachment.setAttachmentUrl(fileIp + File.separator + "video" + File.separator + tabAttachment.getAttachmentUrl());
            }
            vo.setVideos(videos);
        }
        //获取全景图
        List<TabTwoRound> tabTwoRounds = tabTwoRoundMapper.getByRelationAndType(vo.getProjectNo(), "2");
        if (tabTwoRounds.size() > 0) {
            for (TabTwoRound round : tabTwoRounds) {
                round.setUrl(fileIp + File.separator + round.getUrl());
            }
            vo.setTwoRounds(tabTwoRounds);
        }
        return vo;
    }

    @Override
    public Result save(TabProjectVo tabProjectVo) {
        Result result = new Result();
        if (validateDataProjectNo(tabProjectVo.getProjectNo())) {
            return result.failure(-5, "项目编号重复");
        }
        TabProjectWithBLOBs tabProject = new TabProjectWithBLOBs();
        //基础信息保存
        tabProjectMapper.insertSelective(setData(tabProject, tabProjectVo));
        //全景图保存
        String twoRoundUrl = tabProjectVo.getTwoRoundUrl();
        if (null != twoRoundUrl && twoRoundUrl != "") {
            insertTabTwoRound(twoRoundUrl, tabProjectVo.getTwoRoundName(), tabProjectVo.getProjectNo());
        }
        //倾斜摄影保存
        TabPhotography tabPhotography = new TabPhotography();
        tabPhotography.setRelationNo(tabProjectVo.getProjectNo());
        tabPhotography.setModelType("2");
        tabPhotography.setModel(tabProjectVo.getModel());
        tabPhotography.setTaken(tabProjectVo.getTaken());
        tabPhotography.setPhotographyTime(new Date());
        tabPhotography.setPhotographyType(tabProjectVo.getPhotographyType());
        tabPhotographyMapper.insertSelective(tabPhotography);
        //当前用户分配该项目
        TabUserProject userProject = new TabUserProject();
        userProject.setUserId(Integer.valueOf(tabProjectVo.getUserId()));
        userProject.setProjectId(tabProject.getId());
        tabUserProjectMapper.insert(userProject);
        //保存附件(图片、视频)
        /*try {
            List<TabAttachment> list = tabProjectVo.getImgs();
            System.out.println(list);
            tabAttachmentService.saveTabAttachments(tabProjectVo.getImgs());
            tabAttachmentService.saveTabAttachments(tabProjectVo.getVideos());
        } catch (Exception e) {
            return result.failure(-5, "附件上传失败");
        }*/
        return result.success("操作成功");
    }

    @Override
    public List<TabArea> getNextById(String id) {
        return tabAreaMapper.getNextById(id);
    }

    @Override
    public List<TabTenderProject> getTenderProject() {
        return tabTenderProjectMapper.getTenderProject();
    }

    @Override
    public List<Map<String, Object>> getBasic(String id) {
        return tabBasicMapper.getBasicForProject(id);
    }

    @Override
    @Transactional
    public Result update(TabProjectVo tabProjectVo) {
        Result result = new Result();
        TabProjectWithBLOBs tabProject = tabProjectMapper.selectByPrimaryKey(tabProjectVo.getId());
        Boolean flag = validateDataProjectNo(tabProjectVo.getProjectNo());
        if (flag && !tabProjectVo.getProjectNo().equals(tabProject.getProjectNo())) {
            return new Result().failure(-5, "项目编号重复");
        }
        tabProject = setData(tabProject, tabProjectVo);
        tabProjectMapper.updateByPrimaryKeySelective(tabProject);
        //删除全景图
        tabTwoRoundMapper.deleteByRelationAndType(tabProject.getProjectNo(), "2");
        //修改倾斜摄影
        TabPhotography tabPhotography = tabPhotographyMapper.getByRelationAndType(tabProject.getProjectNo(), "2");
        if (null != tabPhotography) {
            tabPhotography.setRelationNo(tabProjectVo.getProjectNo());
            tabPhotography.setModel(tabProjectVo.getModel());
            tabPhotography.setTaken(tabProjectVo.getTaken());
            tabPhotography.setPhotographyType(tabProjectVo.getPhotographyType());
            tabPhotographyMapper.updateByPrimaryKeySelective(tabPhotography);
        }
        //全景图保存
        String twoRoundUrl = tabProjectVo.getTwoRoundUrl();
        if (null != twoRoundUrl && twoRoundUrl != "") {
            insertTabTwoRound(twoRoundUrl, tabProjectVo.getTwoRoundName(), tabProjectVo.getProjectNo());
        }
        /*//删除附件
        if (null != tabProject.getProjectImg() && tabProject.getProjectImg() != "") {
            tabAttachmentMapper.delTabAttachmentByIds(tabProject.getProjectImg().split(","));
        }
        if (null != tabProject.getProjectVideo() && tabProject.getProjectVideo() != "") {
            tabAttachmentMapper.delTabAttachmentByIds(tabProject.getProjectVideo().split(","));
        }*/
        //保存附件(图片、视频)
        /*try {
            tabAttachmentService.saveTabAttachments(tabProjectVo.getImgs());
            tabAttachmentService.saveTabAttachments(tabProjectVo.getVideos());
        } catch (Exception e) {
            return result.failure(-5, "附件上传失败");
        }*/

        return result.success("操作成功");
    }

    @Override
    public Result delete(String id) {
        if (null != id) {
            String[] ids = id.split(",");
            for (String str : ids) {
                //修改项目状态
                TabProjectWithBLOBs tabProject = tabProjectMapper.selectByPrimaryKey(Integer.parseInt(str));
                tabProject.setState("0");
                tabProjectMapper.updateByPrimaryKeySelective(tabProject);
                //删除附件(图片\视频)
                if (null != tabProject.getProjectImg()) {
                    tabAttachmentMapper.delTabAttachmentByIds(tabProject.getProjectImg().split(","));
                }
                if (null != tabProject.getProjectVideo()) {
                    tabAttachmentMapper.delTabAttachmentByIds(tabProject.getProjectVideo().split(","));
                }
                //删除全景图
                tabTwoRoundMapper.deleteByRelationAndType(tabProject.getProjectNo(), "2");
                //删除倾斜摄影
                tabPhotographyMapper.deleteByRelationAndType(tabProject.getProjectNo(), "2");
                //删除参见单位
                tabProjectCjdwMapper.deleteByProjectId(tabProject.getId());
                //TODO 删除项目班子
                //TODO 删除项目文档
                //TODO 删除项目报表
                //删除项目
                tabProjectMapper.deleteByPrimaryKey(Integer.parseInt(str));
            }
        }
        return new Result().success("操作成功");
    }

    @Override
    public Result updateProjectUnit(TabProjectVo tabProjectVo) {
        //删除对应的参见项目
        tabProjectMapper.deleteProjectUnitByProjectId(tabProjectVo.getId());

        List<Integer> units = tabProjectVo.getUnitList();
        for (Integer unitCode : units) {
            TabProjectUnit unit = new TabProjectUnit();
            unit.setProjectId(tabProjectVo.getId());
            unit.setUnitId(unitCode);
            tabProjectMapper.insertProjectUnit(unit);
        }
        return new Result().success("操作成功");
    }

    @Override
    public Result listProjectUnit(TabUnitVo vo) {
        List<TabUnitVo> cjdws = tabUnitMapper.getTabUnitByProject(vo);
        return new Result().success(cjdws);
    }

    @Override
    public Result deleteProjectCjdw(String id) {
        if (null != id) {
            String[] ids = id.split(",");
            for (String str : ids) {
                tabProjectCjdwMapper.deleteByPrimaryKey(Integer.parseInt(str));
            }
        }
        return new Result().success("操作成功");
    }

    @Override
    public Result selectProjectDatum(Integer projectId) {
//        List<Map<String, Object>> list = getAllDatumList(projectId);
//        return new Result().success(list);
        List<Map<String, Object>> list = getAllDatumList(projectId);
//        for (Map<String, Object> map:list) {
//            for (Map.Entry<String, Object> entry:map.entrySet()) {
//                System.err.println("!!!!"+entry);
//            }
//        }
        if (list.size() > 0) {
            for (Map<String, Object> map : list) {
                map.put("pdf", fileIp + File.separator + "file" + File.separator + map.get("pdf"));
                //System.err.println(map.get("pdf"));
            }
        }
        return new Result().success(list);
    }

    @Override
    public Result listProjectPerson(Integer projectId, Integer unitId) {
        List<TabPerson> persons = tabProjectMapper.getTabPersonByProjectIdAndUnit(projectId, unitId);
        return new Result().success(persons);
    }

    @Override
    public Result updateProjectPerson(Integer projectId, Integer unitId, String personIds) {
        //删除项目班子
        tabProjectMapper.deleteProjectPerson(projectId, unitId);
        if (personIds == null || "".equals(personIds)) {
            return new Result().success("操作成功");
        }
        //增加项目班子
        List<String> ids = Arrays.asList(personIds.split(","));
        for (String personId : ids) {
            TabProjectPerson person = new TabProjectPerson();
            person.setProjectId(projectId);
            person.setPersonId(Integer.parseInt(personId));
            person.setUnitId(unitId);
            tabProjectMapper.insertProjectPerson(person);
        }
        return new Result().success("操作成功");
    }

    @Override
    public Result deleteProjectXmbz(String id) {
        if (null != id) {
            String[] ids = id.split(",");
            for (String str : ids) {
                tabProjectXmbzMapper.deleteByPrimaryKey(Integer.parseInt(str));
            }
        }
        return new Result().success("操作成功");
    }


    @Override
    public Result saveTabProjectDatum(TabProjectDatum datum) {
        datum.setTime(new Date());
        datum.setParentId(null == datum.getParentId() ? 0 : datum.getParentId());
        String content = datum.getContent();
        String[] type = content.split("\\.");
        String pdf = type[0] + ".pdf";
        datum.setPdf(pdf);
        FileConverter.office2PDF(content, pdf, openOfficePath, openOfficeHost);
        tabProjectDatumMapper.insertSelective(datum);
        return new Result().success("操作成功");
    }

    @Override
    public Result updateTabProjectDatum(TabProjectDatum datum) {
        tabProjectDatumMapper.updateByPrimaryKeySelective(datum);
        return new Result().success("操作成功");
    }

    @Override
    public Result delTabProjectDatum(Integer id) {
        //如果存在下级则不能进行删除
        List<TabProjectDatum> list = tabProjectDatumMapper.selectDatumByParentId(id);
        if (list.size() > 0) {
            return new Result().failure(-5, "存在下级文档不能进行删除");
        }
        tabProjectDatumMapper.deleteByPrimaryKey(id);
        return new Result().success("操作成功");
    }

    @Override
    public Result listTabReportingFile(TabReportingFile file) {
        PageHelper.startPage(file.getPage(), file.getRows());
        List<TabReportingFile> list = tabReportingFileMapper.listTabReportingFile(file);
        for (TabReportingFile tabReportingFile : list) {
            tabReportingFile.setUploadUrl(fileIp + File.separator + "file" + File.separator + tabReportingFile.getPdf());
        }
        PageInfo<TabReportingFile> pageInfo = new PageInfo<TabReportingFile>(list);
        Result result = new Result();
        result.setCount((int) pageInfo.getTotal());
        return result.success(pageInfo.getList());
    }

    @Override
    public Result updateTabReportingFile(TabReportingFile file) {
        file.setState("1");
        if (null == file.getUploadTime()) {
            file.setUploadTime(new Date());
        }
        tabReportingFileMapper.updateByPrimaryKeySelective(file);
        return new Result().success("操作成功");
    }

    @Override
    public Result saveTabReportingFile(TabReportingFile file) {
        file.setState("1");
        file.setUploadTime(new Date());
        String[] type;
        String pdf;
        if (file.getFileName() != null && file.getUploadUrl() != null) {
            String[] fileName = file.getFileName().split(",");
            String[] url = file.getUploadUrl().split(",");
            for (int i = 0; i < fileName.length; i++) {
                type = url[i].split("\\.");
                pdf = type[0] + ".pdf";
                file.setPdf(pdf);
                file.setFileName(fileName[i]);
                file.setUploadUrl(url[i]);
                tabReportingFileMapper.insertSelective(file);
            }
        }
        return new Result().success("操作成功");
    }


    @Override
    public Result deleteTabReportingFile(String id) {
        if (null != id) {
            String[] ids = id.split(",");
            for (String str : ids) {
                TabReportingFile file = tabReportingFileMapper.selectByPrimaryKey(Integer.parseInt(str));
                file.setState("0");
                tabReportingFileMapper.updateByPrimaryKeySelective(file);
            }
        }
        return new Result().success("操作成功");
    }

    /**
     * 获取所有文档
     */
    private List<Map<String, Object>> getAllDatumList(Integer projectId) {
        //查询根文档
        List<Map<String, Object>> datumList = tabProjectDatumMapper.selectDatumForProject(0, projectId);
        //递归获取子菜单
        getDatumTreeList(datumList, projectId);
        return datumList;
    }

    /**
     * 递归
     */
    private List<Map<String, Object>> getDatumTreeList(List<Map<String, Object>> datumList, Integer projectId) {
        List<Map<String, Object>> subMenuList = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> map : datumList) {
            //目录
            if ("1".equals(map.get("file_type"))) {
                List<Map<String, Object>> list = tabProjectDatumMapper.selectDatumForProject((Integer) map.get("id"), projectId);
                for (Map<String, Object> map1 : list) {
                    map1.put("pdf", fileIp + File.separator + "file" + File.separator + map1.get("pdf"));
                }
                map.put("children", getDatumTreeList(list, projectId));
            } else {
                map.put("children", new ArrayList<Map<String, Object>>());
            }
            subMenuList.add(map);
        }
        return subMenuList;
    }

    private void insertTabTwoRound(String twoRoundUrl, String twoRoundName, String projectNo) {
        String[] urls = twoRoundUrl.split(",");
        String[] zipNames = twoRoundName.split(",");
        for (int i = 0; i < urls.length; i++) {
            TabTwoRound round = new TabTwoRound();
            round.setRelationNo(projectNo);
            round.setUrl(urls[i]);
            round.setModelType("2");
            round.setRoundName(zipNames[i]);
            round.setPicType(urls[i].endsWith("html") ? "1" : "0");
            round.setRoundTime(new Date());
            tabTwoRoundMapper.insertSelective(round);
        }
    }

    private TabProjectWithBLOBs setData(TabProjectWithBLOBs tabProject, TabProjectVo tabProjectVo) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //if(null != tabProjectVo.getTenderName()){
        tabProject.setTenderName(tabProjectVo.getTenderName());
        //}
        // if(null != tabProjectVo.getTenderId()){
        tabProject.setTenderId(tabProjectVo.getTenderId());
        // }
        if(null != tabProjectVo.getStartTime()){
            String startTime = sdf.format(tabProjectVo.getStartTime());
            try {
                tabProject.setStartTime(sdf.parse(startTime));
            } catch (ParseException e) {
                log.info("结束时间转化错误！");
                e.printStackTrace();
            }
        }
        if(null != tabProjectVo.getStartDate()){
            String startDate = sdf.format(tabProjectVo.getStartDate());
            try {
                tabProject.setStartDate(sdf.parse(startDate));
            } catch (ParseException e) {
                log.info("开始时间转化错误！");
                e.printStackTrace();
            }
        }
//        if(null != tabProjectVo.getProTypeName()){
        tabProject.setProTypeName(tabProjectVo.getProTypeName());
//        }
//        if(null != tabProjectVo.getProType()){
        tabProject.setProType(tabProjectVo.getProType());
//        }
//        if(null != tabProjectVo.getProjectNo()){
        tabProject.setProjectNo(tabProjectVo.getProjectNo());
//        }
//        if(null != tabProjectVo.getProjectName()){
        tabProject.setProjectName(tabProjectVo.getProjectName());
//        }
//        if(null != tabProjectVo.getProjectManPhone()){
        tabProject.setProjectManPhone(tabProjectVo.getProjectManPhone());
//        }
//        if(null != tabProjectVo.getProjectMan()){
        tabProject.setProjectMan(tabProjectVo.getProjectMan());
//        }
//        if(null != tabProjectVo.getProjectAdd()){
        tabProject.setProjectAdd(tabProjectVo.getProjectAdd());
        // }
        tabProject.setState(tabProjectVo.getState());
//        if(null != tabProjectVo.getProIsEnd()){
        tabProject.setProIsEnd(tabProjectVo.getProIsEnd());
//        }
//        if(null != tabProjectVo.getMonitorUnitName()){
        tabProject.setMonitorUnitName(tabProjectVo.getMonitorUnitName());
//        }
//        if(null != tabProjectVo.getMonitorUnit()){
        tabProject.setMonitorUnit(tabProjectVo.getMonitorUnit());
//        }
//        if(null != tabProjectVo.getManualAuto()){
        tabProject.setManualAuto(tabProjectVo.getManualAuto());
//        }
//        if(null != tabProjectVo.getLongitude()){
        tabProject.setLongitude(tabProjectVo.getLongitude());
//        }
//        if(null != tabProjectVo.getLatitude()){
        tabProject.setLatitude(tabProjectVo.getLatitude());
//        }
        if(null != tabProjectVo.getEndTime()){
            String endTime = sdf.format(tabProjectVo.getEndTime());
            try {
                tabProject.setEndTime(sdf.parse(endTime));
            } catch (ParseException e) {
                log.info("结束时间转化错误！");
                e.printStackTrace();
            }
        }
//        if(null != tabProjectVo.getDisasterNo()){
        tabProject.setDisasterNo(tabProjectVo.getDisasterNo());
//        }
//        if(null != tabProjectVo.getAreaId()){
        tabProject.setAreaId(tabProjectVo.getAreaId());
//        }
//        if(null != tabProjectVo.getProjectImg()){
        tabProject.setProjectImg(tabProjectVo.getProjectImg());
//        }
//        if(null != tabProjectVo.getProjectVideo()){
        tabProject.setProjectVideo(tabProjectVo.getProjectVideo());
//        }
//        if(null != tabProjectVo.getRemark()){
        tabProject.setRemark(tabProjectVo.getRemark());
//        }
        return tabProject;
    }

    //检查项目编号是否重复
    private Boolean validateDataProjectNo(String projectNo) {
        Integer num = tabProjectMapper.countNumByCondition(projectNo);
        return num > 0;
    }

    /**
     * 导出项目报表excel
     *
     * @param file
     */
    @Override
    public void exportExcelFile(TabReportingFile file, HttpServletResponse response) {
        String file1 = file.getFileType();
        String title = tabReportingFileMapper.getFileTypeName(file1);
        String[] rowName = {"编号", "报表名称", "上传时间", "报表地址"};
        List<TabReportingFile> tabReportingFiles = tabReportingFileMapper.listTabReportingFile(file);
        List<Object[]> list = new ArrayList<>();
        for (int i = 0; i < tabReportingFiles.size(); i++) {
            Object[] o = new Object[4];
            o[1] = tabReportingFiles.get(i).getFileName();
            o[2] = tabReportingFiles.get(i).getTime();
            o[3] = tabReportingFiles.get(i).getUploadUrl();
            list.add(o);
        }
        String filePath = System.getProperty("user.dir") + File.separator + title + "报表";
        ExportExcelUtil ex = new ExportExcelUtil(title, rowName, list, filePath, response);
        try {
            ex.export();
           /* response.setHeader("Content-Disposition", "attachment;fileName=" + new String(filePath.getBytes("UTF-8"), "ISO8859-1"));
            response.setHeader("Connection", "close");
            response.setHeader("Content-Type", "application/vnd.ms-excel;charset=UTF-8");*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取所有项目及其下所属设备
     *
     * @return
     */
    @Override
    public Result getAllProject() {
        //获取所有项目
        List<TabProDevVo> allProject = tabProjectMapper.getAllProject();
        //存储所有项目id的集合
        List<Integer> projectIds = new ArrayList<>();
        //遍历所有项目，获取所有项目id 并在id前面加上"p"
        for (TabProDevVo vo : allProject) {
            projectIds.add(Integer.parseInt(vo.getId()));
            vo.setId("p" + vo.getId());
        }
        //获取所有项目下所拥有的设备
        List<Map<String, Object>> allDevice = tabDeviceMapper.getAllDevice(projectIds);
        //给所有设备id 前加上"d"
        for (Map map : allDevice) {
            map.replace("id", "d" + map.get("id"));
        }
        //获取有标识模版的设备类型
        List<Integer> typeValueList = tabDeviceTemplateMapper.getAllTabDeviceTemplate();

        for (TabProDevVo tabProDevVo : allProject) {
            //每个项目拥有的设备 的集合
            List<Map<String, Object>> list = new ArrayList<>();
            //遍历所有设备，将对应的设备加入对应的项目中
            for (Map map : allDevice) {
                //截取掉项目标识符"p" ,取得项目id
                String substring = tabProDevVo.getId().substring(1, tabProDevVo.getId().length());
                for (Integer typeValue : typeValueList) {
                    if (typeValue == Integer.parseInt(map.get("deviceType").toString())) {
                        if (substring.equals(map.get("pid").toString())) {
                            map.replace("pid", "p" + map.get("pid").toString());
                            //加入拥有的设备集合
                            list.add(map);
                            //将拥有的设备集合存入所属项目中
                            tabProDevVo.setChildren(list);
                        }
                    }
                }
            }
        }
        Result result = new Result();
        result.setData(allProject);
        result.setCode(0);
        result.setMsg("查询成功");
        return result;
    }
}
