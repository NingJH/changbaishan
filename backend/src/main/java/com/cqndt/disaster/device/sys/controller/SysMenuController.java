package com.cqndt.disaster.device.sys.controller;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.sys.service.SysMenuService;
import com.cqndt.disaster.device.sys.vo.SysMenuVo;
import com.cqndt.disaster.device.util.AbstractController;
import com.cqndt.disaster.device.util.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/2/27  16:07
 * Description:菜单管理
 */
@RestController
public class SysMenuController extends AbstractController {
    private static Logger logger = LoggerFactory.getLogger(SysMenuController.class);
    @Autowired
    private SysMenuService menuService;
    /**
     * 导航菜单
     * @return
     */
   // @RequestMapping(value = "navigationList", method = RequestMethod.POST)
    @PostMapping("navigationList")
    public Result navigationList(HttpServletRequest request) {
        String frontBack = request.getParameter("frontBack");
        String userId = getUserId().toString();
        List<Map<String,Object>> list = menuService.navigationList(userId,frontBack);
        return new Result().success(list);
    }

    /**
     * 树列表
     * @param menuVo
     * @return
     */
    @PostMapping("list")
    public Result listTabUser(@RequestBody SysMenuVo menuVo) {
        Result result = new Result();
        List<SysMenuVo> list = menuService.list(menuVo);
        result.setData(list);
        result.setCount(list.size());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabMenuById")
    public Result getTabMenuById(String id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        SysMenuVo  menuVo = menuService.getTabMenuById(id);
        return new Result().success(menuVo);
    }

    /**
     * 保存
     */
    @SysLog("保存菜单")
    @PostMapping("save")
    @RequiresPermissions("sys:tabmenu:save")
    public Result save(@RequestBody SysMenuVo menuVo){
        Result result = new Result();
        try {
            result = menuService.checkData(menuVo);
            if(result.getCode()==0){
                menuService.save(menuVo);
                result.setMsg("操作成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改菜单")
    @PostMapping("update")
    @RequiresPermissions("sys:tabmenu:update")
    public Result update(@RequestBody SysMenuVo menuVo){
        Result result = new Result();
        try {
            if(null == menuVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            result = menuService.checkData(menuVo);
            if(result.getCode()==0){
                menuService.update(menuVo);
                result.setMsg("操作成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除菜单")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabmenu:delete")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            result = menuService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }


    /**
     * 下拉框list
     */
    @PostMapping("listTabMenu")
    public Result listTabMenu(SysMenuVo menuVo){
        Result result = new Result();
        try {
            List<Map<String,Object>> list = menuService.listTabMenu(menuVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }



    /**
     * 子表信息
     */
    @PostMapping("listTabRoleAll")
    public Result listTabRoleAll(SysMenuVo menuVo){
        Result result = new Result();
        try {
            List<SysMenuVo> list = menuService.listTabRoleAll(menuVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 子表已选择信息
     */
    @PostMapping("listTabRoleSelect")
    public Result listTabRoleSelect(Integer id){
        Result result = new Result();
        try {
            List<SysMenuVo> list = menuService.listTabRoleSelect(id);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }
    /**
     * 下拉框list
     */
    @PostMapping("listTabStaticType")
    public Result listTabStaticType(SysMenuVo sysMenuVo){
        Result result = new Result();
        try {
            List<Map<String,Object>> list = menuService.listTabStaticType(sysMenuVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }
}
