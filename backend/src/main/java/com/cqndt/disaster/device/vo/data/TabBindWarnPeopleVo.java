package com.cqndt.disaster.device.vo.data;

import lombok.Data;

@Data
public class TabBindWarnPeopleVo {
    /**
     * 页数
     */
    private Integer page = 1;
    /**
     * 条数
     */
    private Integer limit = 10;

    //主键
    private Integer id;
    //人员姓名
    private String name;
    private String phone;
    private String job;

    //单位名称
    private String unitData;

    private String ids;

    private Integer basicId;

}
