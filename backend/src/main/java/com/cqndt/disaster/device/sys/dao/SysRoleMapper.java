package com.cqndt.disaster.device.sys.dao;

import com.cqndt.disaster.device.sys.vo.SysRoleVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * Description: 表备注
 * Date: 2019-03-14
 */

@Mapper
public interface SysRoleMapper {
    /**
     * 根据条件查询list
     * @param sysRoleVo 查询条件
     */
    List<SysRoleVo> list(SysRoleVo sysRoleVo);
    /**
     * 增加
     * @param sysRoleVo 条件
     */
    int save(SysRoleVo sysRoleVo);
    /**
     * 修改
     * @param sysRoleVo 条件
     */
    int update(SysRoleVo sysRoleVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    SysRoleVo getTabRoleById(Long id);
     /**
      * 验证是否重复
      * @param sysRoleVo 条件
      */
     int checkValidate1(SysRoleVo sysRoleVo);

     /**
      * 下拉框列表
      */
     List<Map<String,Object>> listTabDept(SysRoleVo sysRoleVo);
         /**
      * 关联表删除
      */
     int deleteTabRoleMenu(String roleId);

     /**
      * 中间表保存
      */
     int saveTabRoleMenu(@Param("roleId") String roleId, @Param("menuId") String menuId);

    /**
     * 根据角色id获取已分配的菜单及权限
     */
    List<String> getMenuByRoleId(@Param("roleId")String roleId,@Param("frontBack")String frontBack);
}
