package com.cqndt.disaster.device.dao.data;


import com.cqndt.disaster.device.entity.data.TabPerson;
import com.cqndt.disaster.device.entity.data.TabUnit;
import com.cqndt.disaster.device.vo.data.TabPersonVo;
import com.cqndt.disaster.device.vo.data.TabStaticTypeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface TabPersonMapper {

    /**
     * 根据单位id或者姓名查询单位所属人员
     * @param unitId
     * @param username
     * @return
     */
    List<Map<String,String>> queryTabPersonById(@Param("unitId") Integer unitId, @Param("username") String username);

    /**
     * 新增人员
     * @param TabPersonVo 新增内容
     * @return 返回受影响的行数
     */
    int save(TabPersonVo TabPersonVo);

    /**
     * 根据条件查询人员列表
     * @param tabPersonVo 查询条件
     * @return 返回TabPersonVo对象
     */
    List<TabPersonVo> list(TabPersonVo tabPersonVo);

    /**
     * 修改人员信息
     * @param tabPersonVo 参数
     * @return 返回受影响行数
     */
    int update(TabPersonVo tabPersonVo);

    /**
     * 根据id查询人员信息
     * @param id 人员id
     * @return 返回对象
     */
    TabPersonVo getById(Integer id);

    /**
     * 根据id删除信息
     * @param id 人员id
     * @return 受影响的行数
     */
    int delete(Integer id);

    /**
     * 查询公司列表
     * @return 返回公司列表
     */
    List<TabUnit> unitList();

    /**
     * 查询人员类型列表
     * @return 返回人员类型列表
     */
    List<TabStaticTypeVo> personTypeList();

    /**
     * 根据人员类型获取人员
     * @param type
     * @return
     */
    List<TabPerson> getPersonByType(@Param("type") Integer type);
}
