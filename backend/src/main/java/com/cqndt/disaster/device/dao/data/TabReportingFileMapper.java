package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabReportingFile;
import com.cqndt.disaster.device.vo.data.TabReportingFileVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TabReportingFileMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_reporting_file
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_reporting_file
     *
     * @mbg.generated
     */
    int insert(TabReportingFile record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_reporting_file
     *
     * @mbg.generated
     */
    int insertSelective(TabReportingFile record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_reporting_file
     *
     * @mbg.generated
     */
    TabReportingFile selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_reporting_file
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(TabReportingFile record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tab_reporting_file
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(TabReportingFile record);

    List<TabReportingFile> listTabReportingFile(TabReportingFile file);

    /**
     * 获取对应标题title名称
     * @param fileType
     * @return
     */
    String getFileTypeName(String fileType);
}