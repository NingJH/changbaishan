package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabWarningUnite;
import lombok.Data;

@Data
public class TabWarningUniteVo extends TabWarningUnite {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;
}
