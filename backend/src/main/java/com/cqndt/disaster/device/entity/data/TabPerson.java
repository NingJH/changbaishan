package com.cqndt.disaster.device.entity.data;

import lombok.Data;

@Data
public class TabPerson {

    private String id;

    private String username;

    private String tel;

    private String type;

    private String job;

    private Integer unitId;
}
