package com.cqndt.disaster.device.entity.data;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TabUnion {
    private Integer id;

    private String identify;

    private Double result;

    private String sensorNo;
}
