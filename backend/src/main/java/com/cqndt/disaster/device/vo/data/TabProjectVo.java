package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/6/5  10:59
 * Description:
 */
@Data
public class TabProjectVo extends TabProjectWithBLOBs {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;
    /**
     * 当前用户id
     */
    private String userId;

    /**
     * 灾害类型
     */
    private String disType;
    /**
     * 灾害类型名称
     */
    private String disasterName;
    /**
     * 险情等级
     */
    private String dangerLevel;
    /**
     * 险情等级名称
     */
    private String dangerLevelName;
    /**
     * 区域编码
     */
    private String areaCode;
    /**
     * 区域等级
     */
    private String level;
    /**
     * 图片
     */
    private List<TabAttachment> imgs;
    /**
     * 视频
     */
    private List<TabAttachment> videos;

    /**
     * 图片
     */
    private List<Map<String,Object>> projectImgs;
    /**
     * 视频
     */
    private List<Map<String,Object>> projectVideos;
    /**
     * 全景图
     */
    private List<TabTwoRound> twoRounds;

    /**
     * 倾斜摄影
     */
    private TabPhotography photography;
    /**
     * 全景图url
     */
    private String twoRoundUrl;
    /**
     * 全景图名称
     */
    private String twoRoundName;
    /**
     * 全景图类型(0:图片通过工具转化，1:无人机中心提供)
     */
    private String picType;
    /**
     * 正摄地址
     */
    private String taken;
    /**
     * 模型地址
     */
    private String model;
    /**
     * 倾斜摄影类型
     */
    private String photographyType;
    /**
     * 参见单位
     */
    private List<TabProjectCjdw> cjdws;
    /**
     * 参见单位
     */
    private List<TabProjectXmbz> xmbzs;

    /**
     * 参见单位
     */
    private List<Integer> unitList;

    /**
     * 地区ID
     */
    private List<TabArea> list;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date startTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date startDate;

    private String getAreaCodeSplit(){
        if(null != getAreaCode() && !"".equals(getAreaCode())){
            return getAreaCode().substring(0,2);
        }
        return null;
    }

    /**
     * 父级id
     */
    private Integer pid=0;

    private String label;
    /**
     * 项目下所属设备
     */
    private List<Map<String,Object>> children;


}
