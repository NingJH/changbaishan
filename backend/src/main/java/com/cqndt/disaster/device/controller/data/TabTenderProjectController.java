package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.service.data.TabTenderProjectService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabTenderProjectVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
* Description: 招标项目管理
* Date: 2019-06-04
*/
@RestController
@RequestMapping("tabTenderProject")
public class TabTenderProjectController {
    @Autowired
    private TabTenderProjectService tabTenderProjectService;

    @PostMapping("list")
    public Result listTabUser(@RequestBody TabTenderProjectVo tabTenderProjectVo) {
        PageHelper.startPage(tabTenderProjectVo.getPage(),tabTenderProjectVo.getLimit());
        PageInfo<TabTenderProjectVo> pageInfo = new PageInfo<TabTenderProjectVo>(tabTenderProjectService.list(tabTenderProjectVo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabTenderProjectById")
    public Result getTabTenderProjectById(Long id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        TabTenderProjectVo  tabTenderProjectVo = tabTenderProjectService.getTabTenderProjectById(id);
        return new Result().success(tabTenderProjectVo);
    }

    /**
     * 保存
     */
    @SysLog("增加招标项目管理")
    @PostMapping("save")
    @RequiresPermissions("sys:tabtenderproject:save")
    public Result save(@RequestBody TabTenderProjectVo tabTenderProjectVo){
        Result result = new Result();
        try {
            tabTenderProjectService.save(tabTenderProjectVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改招标项目管理")
    @PostMapping("update")
    @RequiresPermissions("sys:tabtenderproject:update")
    public Result update(@RequestBody TabTenderProjectVo tabTenderProjectVo){
        Result result = new Result();
        try {
            if(null == tabTenderProjectVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabTenderProjectService.update(tabTenderProjectVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除招标项目管理")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabtenderproject:delete")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabTenderProjectService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
       return result;
    }


    /**
     * 下拉框list
     */
    @PostMapping("listTabStaticType")
    public Result listTabStaticType(TabTenderProjectVo tabTenderProjectVo){
        Result result = new Result();
        try {
            List<Map<String,Object>> list = tabTenderProjectService.listTabStaticType(tabTenderProjectVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

       
}