package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabDeviceInstallMapper;
import com.cqndt.disaster.device.service.data.TabDeviceInstallService;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.vo.data.TabDeviceInstallVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TabDeviceInstallServiceImpl implements TabDeviceInstallService {
    @Autowired
    private TabDeviceInstallMapper tabDeviceInstallMapper;

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(TabDeviceInstallVo tabDeviceInstallVo) throws Exception {
        tabDeviceInstallMapper.save(tabDeviceInstallVo);
    
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void update(TabDeviceInstallVo tabDeviceInstallVo) throws Exception {
        TabDeviceInstallVo vo = tabDeviceInstallMapper.getTabDeviceInstallById(tabDeviceInstallVo.getId());
        AttachmentVo attachmentVo = new AttachmentVo();
        tabDeviceInstallMapper.update(tabDeviceInstallVo);
     
   }
    @Override
    public TabDeviceInstallVo getTabDeviceInstallById(Long id){
        return tabDeviceInstallMapper.getTabDeviceInstallById(id);
    }
    @Override
    public void delete(String id){
         String[] ids = id.split(",");
         for (String idStr:ids) {
            tabDeviceInstallMapper.delete(Integer.parseInt(idStr));
         }
    }
    @Override
    public List<TabDeviceInstallVo> list(TabDeviceInstallVo tabDeviceInstallVo) {
        return  tabDeviceInstallMapper.list(tabDeviceInstallVo);
    }



}