package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.vo.data.TabDisasterCardVo;

import java.util.List;
import java.util.Map;

public interface TabDisasterCardService {
    /**
     * 增加
     * @param tabDisasterCardVo
     */
    void save(TabDisasterCardVo tabDisasterCardVo) throws Exception;
     /**
     * 修改
     * @param tabDisasterCardVo
     */
     void update(TabDisasterCardVo tabDisasterCardVo) throws Exception;

    /**
     * 根据id获取信息
     * @param disNo
     * @return tabDisasterCardVo
     */
    Map<String,Object> getTabDisasterCardById(String disNo);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabDisasterCardVo 查询条件
     * @return 数据列表
     */
    List<TabDisasterCardVo> list(TabDisasterCardVo tabDisasterCardVo);



}