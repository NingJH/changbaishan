package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.*;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabBasicHumanVo;
import com.cqndt.disaster.device.vo.data.TabDeviceVo;
import com.cqndt.disaster.device.vo.data.TabMonitorPersonVo;
import com.cqndt.disaster.device.vo.data.TabWarnSettingVo;

import java.util.Date;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface TabDeviceService {
    /**
     * 增加
     * @param tabDeviceVo
     */
    Result save(TabDeviceVo tabDeviceVo) throws Exception;
     /**
     * 修改
     * @param tabDeviceVo
     */
     Result update(TabDeviceVo tabDeviceVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabDeviceVo
     */
    TabDeviceVo getTabDeviceById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabDeviceVo 查询条件
     * @return 数据列表
     */
    List<TabDeviceVo> list(TabDeviceVo tabDeviceVo);

    /**
     *
     * @param deviceNo 传感器编号
     * @param deviceType 设备类型
     * @param fileName 上传文件的名称
     * @param path 上传文件的绝对路劲
     * @param now 日期
     * @return result对象
     * @throws Exception
     *
     * 将上传的excel文件的数据 存入数据库中相对应的表
     */
    Result saveTabProjectDatum(String deviceNo,  String deviceType, String fileName, String path, Date now) throws Exception;
    /**
     * 导出设备报表
     */
    void exportDeviceExcel(TabDeviceVo tabDeviceVo, HttpServletResponse response);

    Result getAllDeviceName(TabMonitorPersonVo tabMonitorPersonVo);

    /**
     * 获取监测设备对应类型的阈值
     * @return
     */
    Result getWarnSettingList(String sensorNo, int type);

    /**
     * 添加地裂缝设备阈值设置
     * @return
     */
    Result insertWarnSetting(List<WarnDlfSetting> warnDlfSettingList, String sensorNo);

    /**
     * 添加坡倾仪设备阈值设置
     * @param warnPqSettingList
     * @param sensorNo
     * @return
     */
    Result insertPqWarnSetting(List<WarnPqSetting> warnPqSettingList, String sensorNo);

    /**
     * 添加倾斜设备阈值设置
     * @param warnQxSettingList
     * @param sensorNo
     * @return
     */
    Result insertQxWarnSetting(List<WarnQxSetting> warnQxSettingList, String sensorNo);

    /**
     * 添加gnss设备阈值设置
     * @param warnGnssSettingList
     * @param sensorNo
     * @return
     */
    Result insertGnssWarnSetting(List<WarnGnssSetting> warnGnssSettingList, String sensorNo);

    /**
     * 添加雨量设备阈值设置
     * @param warnYlSettingList
     * @param sensorNo
     * @return
     */
    Result insertYlWarnSetting(List<WarnYlSetting> warnYlSettingList, String sensorNo);

    Result updateWarnSetting(List<TabWarnSettingVo> tabWarnSettingVo);
}