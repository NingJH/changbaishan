package com.cqndt.disaster.device.service.data;


import com.cqndt.disaster.device.vo.data.TabMapVo;

import java.util.List;

public interface TabMapService {
    /**
     * 列表
     * @param tabMapVo
     * @return 数据列表
     */
    List<TabMapVo> list(TabMapVo tabMapVo);

    /**
     * 根据id获取信息
     * @param id
     * @return TabMapVo
     */
    TabMapVo getTabMapById(Integer id);

    /**
     * 保存
     * @param tabMapVo
     */
    void save(TabMapVo tabMapVo) throws Exception;

    /**
     * 更新
     * @param tabMapVo
     */
    void update(TabMapVo tabMapVo) throws Exception;

    /**
     * 删除
     * @param id
     */
    void delete(String id);
}
