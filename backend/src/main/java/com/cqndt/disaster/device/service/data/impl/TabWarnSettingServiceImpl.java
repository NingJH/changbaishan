package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabWarnSettingMapper;
import com.cqndt.disaster.device.service.data.TabWarnSettingService;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.vo.data.TabWarnSettingVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TabWarnSettingServiceImpl implements TabWarnSettingService {
    @Autowired
    private TabWarnSettingMapper tabWarnSettingMapper;

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(TabWarnSettingVo tabWarnSettingVo) throws Exception {
        tabWarnSettingMapper.save(tabWarnSettingVo);
    
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void update(TabWarnSettingVo tabWarnSettingVo) throws Exception {
        TabWarnSettingVo vo = tabWarnSettingMapper.getTabWarnSettingById(tabWarnSettingVo.getId());
        AttachmentVo attachmentVo = new AttachmentVo();
        tabWarnSettingMapper.update(tabWarnSettingVo);
     
   }
    @Override
    public TabWarnSettingVo getTabWarnSettingById(Long id){
        return tabWarnSettingMapper.getTabWarnSettingById(id);
    }
    @Override
    public void delete(String id){
         String[] ids = id.split(",");
         for (String idStr:ids) {
            tabWarnSettingMapper.delete(Integer.parseInt(idStr));
         }
    }
    @Override
    public List<TabWarnSettingVo> list(TabWarnSettingVo tabWarnSettingVo) {
        return  tabWarnSettingMapper.list(tabWarnSettingVo);
    }



}