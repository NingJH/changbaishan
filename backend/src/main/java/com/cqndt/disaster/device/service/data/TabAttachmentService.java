package com.cqndt.disaster.device.service.data;



import com.cqndt.disaster.device.entity.data.TabAttachment;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/6/19  15:03
 * Description:
 */
public interface TabAttachmentService {

    void saveTabAttachments(List<TabAttachment> attachments);

    void saveTabAttachment(TabAttachment attachment);
}
