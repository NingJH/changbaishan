package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabWarnPeople;
import com.cqndt.disaster.device.service.data.TabAreaService;
import com.cqndt.disaster.device.service.data.TabWarnPeopleService;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import com.cqndt.disaster.device.util.AbstractController;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabBindWarnPeopleVo;
import com.cqndt.disaster.device.vo.data.TabWarnPeopleVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author : laishihao
 * @date : 2019/10/22
 * description :
 */
@Api("预警人员表")
@RestController
@RequestMapping("tabWarnPeople")
@Slf4j
public class TabWarnPeopleController extends AbstractController {

    @Autowired
    private TabAreaService tabAreaService;

    @Autowired
    private TabWarnPeopleService tabWarnPeopleService;


    @ApiOperation(value = "根据当前选择的乡镇地区获取所属的灾害点", notes = "根据当前选择的乡镇地区获取所属的灾害点")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "areaCode", value = "乡镇code",  required = true, paramType = "query")
    })
    @GetMapping("getBasicByCode")
    public Result getBasicByCode(Integer areaCode) {
        Result result=new Result();
        List<Map<String, Object>> basicByCode = tabWarnPeopleService.getBasicByCode(areaCode);
        result.setData(basicByCode);
        return result;
    }

    @ApiOperation(value = "外层大列表展示", notes = "外层大列表展示")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "每页条数", defaultValue = "", required = true, paramType = "query")
    })
    @PostMapping("selectList")
    public Result selectList(@RequestBody TabWarnPeopleVo vo){
        SysUserVo sysUserVo = getUser();
        List<TabArea> list = tabAreaService.TabAreaList(sysUserVo);
        vo.setList(list);

        return tabWarnPeopleService.selectList(vo);
    }

    /**
     * 项目绑定告警人员
     * @return
     */
    @PostMapping("selectList1")
    public Result selectList1(@RequestBody TabBindWarnPeopleVo tabBindWarnPeopleVo ){
//        System.out.println(tabBindWarnPeopleVo.toString());
        PageHelper.startPage(tabBindWarnPeopleVo.getPage(), tabBindWarnPeopleVo.getLimit());
        PageInfo<TabBindWarnPeopleVo> pageInfo = new PageInfo<TabBindWarnPeopleVo>(tabWarnPeopleService.selectList1( tabBindWarnPeopleVo));
        Result result = new Result();
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        log.info("====================list{}",pageInfo.getList());
        return result;
    }

    @ApiOperation(value = "新增人员", notes = "新增人员")
    @PostMapping("addWarnPeople")
    public Result addWarnPeople(@RequestBody TabWarnPeopleVo vo){
        return  tabWarnPeopleService.addWarnPeople(vo);
    }


    @ApiOperation(value = "人员列表展示", notes = "人员列表展示")
    @GetMapping("peopleList")
    public Result peopleList(Integer id){
        return tabWarnPeopleService.peopleList(id);
    }
    @ApiOperation(value = "人员列表展示", notes = "人员列表展示")
    @PostMapping("allWarnPeople")
    public Result peopleList(){
        List<TabWarnPeople> warnPeople = tabWarnPeopleService.allWarnPeople();
        Result result = new Result();
        result.setData(warnPeople);
        return result;
    }

    @ApiOperation(value = "修改人员", notes = "修改人员")
    @PostMapping("updatePeople")
    public Result updatePeople(@RequestBody TabWarnPeopleVo vo){

        return tabWarnPeopleService.updatePeople(vo);
    }

    @ApiOperation(value = "删除人员", notes = "删除人员")
    @PostMapping("delete")
    public Result delete(@RequestBody TabWarnPeopleVo vo){

        return tabWarnPeopleService.delete(vo);
    }
}











