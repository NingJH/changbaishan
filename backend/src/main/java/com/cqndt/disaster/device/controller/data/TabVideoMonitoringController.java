package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.service.data.TabVideoMonitoringService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabVideoMonitoringVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
* Description: 视频监控管理
* Date: 2019-05-31
*/
@RestController
@RequestMapping("tabVideoMonitoring")
public class TabVideoMonitoringController {
    @Autowired
    private TabVideoMonitoringService tabVideoMonitoringService;

    @PostMapping("list")
    public Result listTabUser(@RequestBody TabVideoMonitoringVo tabVideoMonitoringVo) {
        PageHelper.startPage(tabVideoMonitoringVo.getPage(),tabVideoMonitoringVo.getLimit());
        PageInfo<TabVideoMonitoringVo> pageInfo = new PageInfo<TabVideoMonitoringVo>(tabVideoMonitoringService.list(tabVideoMonitoringVo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabVideoMonitoringById")
    public Result getTabVideoMonitoringById(Long id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        TabVideoMonitoringVo  tabVideoMonitoringVo = tabVideoMonitoringService.getTabVideoMonitoringById(id);
        return new Result().success(tabVideoMonitoringVo);
    }

    /**
     * 保存
     */
    @SysLog("增加视频监控管理")
    @PostMapping("save")
    @RequiresPermissions("sys:tabvideomonitoring:save")
    public Result save(@RequestBody TabVideoMonitoringVo tabVideoMonitoringVo){
        Result result = new Result();
        try {
            tabVideoMonitoringService.save(tabVideoMonitoringVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改视频监控管理")
    @PostMapping("update")
    @RequiresPermissions("sys:tabvideomonitoring:update")
    public Result update(@RequestBody TabVideoMonitoringVo tabVideoMonitoringVo){
        Result result = new Result();
        try {
            if(null == tabVideoMonitoringVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabVideoMonitoringService.update(tabVideoMonitoringVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除视频监控管理")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabvideomonitoring:delete")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabVideoMonitoringService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
       return result;
    }


    /**
     * 下拉框list
     */
    @PostMapping("listTabArea")
    public Result listTabArea(TabVideoMonitoringVo tabVideoMonitoringVo){
        Result result = new Result();
        try {
            List<Map<String,Object>> list = tabVideoMonitoringService.listTabArea(tabVideoMonitoringVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    
    /**
     * 级联关系查询相应的上级
     */
    @PostMapping("listTabAreaUp")
    public Result listTabAreaUp(TabVideoMonitoringVo tabVideoMonitoringVo){
        Result result = new Result();
        try {
            Map<String,Object> list = tabVideoMonitoringService.listTabAreaUp(tabVideoMonitoringVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }
       
    /**
     * 下拉框list
     */
    @PostMapping("listTabProject")
    public Result listTabProject(TabVideoMonitoringVo tabVideoMonitoringVo){
        Result result = new Result();
        try {
            List<Map<String,Object>> list = tabVideoMonitoringService.listTabProject(tabVideoMonitoringVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

       
    /**
     * 下拉框list
     */
    @PostMapping("listTabStaticType")
    public Result listTabStaticType(TabVideoMonitoringVo tabVideoMonitoringVo){
        Result result = new Result();
        try {
            List<Map<String,Object>> list = tabVideoMonitoringService.listTabStaticType(tabVideoMonitoringVo);
            result.setData(list);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

       

    /**
     * 获取图片
     */
    @PostMapping("listTabVideoMonitoringAttachment")
    public Result listTabVideoMonitoringAttachment(Long id){
         Result result = new Result();
         try {
             Map<String,Object> map = tabVideoMonitoringService.listTabVideoMonitoringAttachment(id);
             result.setData(map);
             result.setMsg("操作成功");
         } catch (Exception e) {
             e.printStackTrace();
             return result.failure(-1,"操作失败");
         }
         return result;
    }
    /**
     * 删除图片
     */
    @SysLog("删除视频监控管理的相关图片")
    @PostMapping("deleteTabVideoMonitoringAttachment")
    @RequiresPermissions("sys:tabvideomonitoring:delete")
    public Result deleteTabVideoMonitoringAttachment(@RequestBody TabVideoMonitoringVo tabVideoMonitoringVo){
         Result result = new Result();
         try {
             tabVideoMonitoringService.deleteTabVideoMonitoringAttachment(tabVideoMonitoringVo);
             result.setMsg("操作成功");
         } catch (Exception e) {
             e.printStackTrace();
             return result.failure(-1,"操作失败");
         }
         return result;
    }

//    @SysLog("增加视频监控管理")
    @PostMapping("saveVideo")
    public Result saveVideo(@RequestBody TabVideoMonitoringVo tabVideoMonitoringVo){
        Result result = new Result();
        try {
            tabVideoMonitoringService.saveVideo(tabVideoMonitoringVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    @PostMapping("selectVideo")
    public Result selectVideo(@RequestBody TabVideoMonitoringVo tabVideoMonitoringVo){
        Result result = new Result();
        try {
            PageHelper.startPage(tabVideoMonitoringVo.getPage(), tabVideoMonitoringVo.getLimit());
            PageInfo<TabVideoMonitoringVo> pageInfo = new PageInfo<>(tabVideoMonitoringService.selectVideo(tabVideoMonitoringVo));
            result.setCount((int) pageInfo.getTotal());
            result.setData(pageInfo.getList());
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    @PostMapping("updateVideo")
    public Result updateVideo(@RequestBody TabVideoMonitoringVo tabVideoMonitoringVo){
        Result result = new Result();
        try {
            tabVideoMonitoringService.updateVideo(tabVideoMonitoringVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    @PostMapping("updateVideoMark")
    public Result updateVideoMark(@RequestBody TabVideoMonitoringVo tabVideoMonitoringVo){
        Result result = new Result();
        try {
            tabVideoMonitoringService.updateVideoMark(tabVideoMonitoringVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    @PostMapping("deleteVideo")
    public Result deleteVideo(@RequestBody TabVideoMonitoringVo tabVideoMonitoringVo){
        Result result = new Result();
        try {
            tabVideoMonitoringService.deleteVideo(tabVideoMonitoringVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }
}