package com.cqndt.disaster.device.controller.data;


import com.cqndt.disaster.device.entity.data.TabUnit;
import com.cqndt.disaster.device.service.data.TabPersonService;
import com.cqndt.disaster.device.service.data.TabUnitService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabPersonVo;
import com.cqndt.disaster.device.vo.data.TabStaticTypeVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("TabPerson")
@Api(value = "人员单位信息",description = "人员单位信息" )
public class TabPersonController {

    @Autowired
    private TabUnitService tabUnitService;

    @Autowired
    private TabPersonService tabPersonService;

    @ApiOperation(value = "单位信息列表", notes = "单位信息列表")
    @PostMapping("getTabUnitName")
    public Result getTabUnitName(){
        Result result = new Result();
        List<TabUnit> list = tabUnitService.getTabUnitName();
        result.setCode(0);
        result.setData(list);
        result.setMsg("查询成功");
        return result;
    }

    @ApiOperation(value = "人员信息列表", notes = "人员信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "unitId", value = "单位id", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "username", value = "单位人员姓名", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "每页条数", defaultValue = "", required = true, paramType = "query")
    })
    @PostMapping("queryTabPersonById")
    public Result queryTabPersonById(@RequestBody  TabPersonVo vo){
        System.out.println(vo.getUnitId());
        Result result = new Result();
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        List<Map<String,String>> list = tabPersonService.queryTabPersonById(vo.getUnitId(),vo.getUsername());
        PageInfo<Map<String,String>> pageInfo = new PageInfo<>(list);
        result.setCode(0);
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        result.setMsg("查询成功");
        return result;
    }

    @ApiOperation(value = "添加人员信息", notes = "添加人员信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "username", value = "人员姓名", required = true,defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "tel", value = "电话", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "type", value = "人员类型", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "job", value = "职务", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "unitId", value = "单位id", defaultValue = "", paramType = "query")
    })
    @PostMapping("save")
    @RequiresPermissions("sys:person:save")
    public Result save(@RequestBody TabPersonVo tabPersonVo){
        try{
            return tabPersonService.save(tabPersonVo);
        }catch (Exception e){
            e.printStackTrace();
            return new Result().failure(-1,e.getMessage());
        }

    }

    @ApiOperation(value = "人员信息列表", notes = "人员信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "unitId", value = "单位id", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "personType", value = "人员类型", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "username", value = "人员姓名", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "每页条数", defaultValue = "", required = true, paramType = "query")
    })
    @PostMapping("list")
//    @RequiresPermissions("sys:person:list")
    public Result list(@RequestBody TabPersonVo tabPersonVo){
        PageHelper.startPage(tabPersonVo.getPage(),tabPersonVo.getLimit());
        PageInfo<TabPersonVo> pageInfo=new PageInfo<>(tabPersonService.list(tabPersonVo));
        Result result=new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    @ApiOperation(value = "修改人员信息", notes = "修改人员信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "人员id", required = true,defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "username", value = "人员姓名", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "tel", value = "电话", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "type", value = "人员类型", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "job", value = "职务", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "unitId", value = "单位id", defaultValue = "", paramType = "query")
    })
    @PostMapping("update")
    @RequiresPermissions("sys:person:update")
    public Result update(@RequestBody TabPersonVo tabPersonVo){
        try {
            return tabPersonService.update(tabPersonVo);
        }catch (Exception e){
            e.printStackTrace();
            return new Result().failure(-1, e.getMessage());
        }
    }

    @ApiOperation(value = "删除人员信息", notes = "删除人员信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "人员id", defaultValue = "", required = true,paramType = "query"),
           })
    @PostMapping("delete")
    @RequiresPermissions("sys:person:delete")
    public Result delete(@RequestBody TabPersonVo tabPersonVo){
        try {
            return tabPersonService.delete(tabPersonVo);
        }catch (Exception e) {
            e.printStackTrace();
            return new Result().failure(-1,"操作失败");
        }
    }

    @ApiOperation(value = "查询公司列表", notes = "查询公司列表")
    @PostMapping("unitList")
    public Result unitList(){
        Result result=new Result();
        PageInfo<TabUnit> pageInfo=new PageInfo<>(tabPersonService.findUnitList());
        result.setData(pageInfo.getList());
        result.setMsg("操作成功");
        return  result;
    }

    @ApiOperation(value = "查询人员类型列表", notes = "查询人员类型列表")
    @PostMapping("personTypeList")
    public Result personTypeList(){
        Result result=new Result();
        PageInfo<TabStaticTypeVo> pageInfo=new PageInfo<>(tabPersonService.findPersonTypeList());
        result.setData(pageInfo.getList());
        result.setMsg("操作成功");
        return  result;
    }

    @ApiOperation(value = "查询人员类型列表", notes = "查询人员类型列表")
    @PostMapping("getPersonByType")
    public Result getPersonByType(Integer type){
        return  new Result().success(tabPersonService.getPersonListByType(type));
    }
}
