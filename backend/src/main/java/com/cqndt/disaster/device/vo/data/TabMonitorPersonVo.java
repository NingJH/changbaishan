package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabMonitorPerson;
import lombok.Data;

import java.util.List;

@Data
public class TabMonitorPersonVo extends TabMonitorPerson {
    /**
     * 页数
     */
    private Integer page = 1;
    /**
     * 条数
     */
    private Integer limit = 10;

    private String deviceName;

    private String projectName;

    private List<TabArea> list;

    private String Address;


}
