package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabWarningDevice;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class TabjsVo {

    private String id;

    private String pid;

    private String deviceNo;

    private String projectNo;

    private String label;

    private Integer deviceType;

    private List<TabjsVo> children;

    private List<TabWarningDevice> Marks;

}
