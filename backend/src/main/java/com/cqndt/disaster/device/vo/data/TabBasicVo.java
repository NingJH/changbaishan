package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabBasic;
import com.cqndt.disaster.device.entity.data.TabTwoRound;

import java.sql.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Description:
 * Date: 2019-06-04
 */
public class TabBasicVo extends TabBasic {
    /**
     * 页数
     */
    private Integer page = 1;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getLabelName1() {
        return labelName1;
    }

    public void setLabelName1(String labelName1) {
        this.labelName1 = labelName1;
    }

    public String getdNo1() {
        return dNo1;
    }

    public void setdNo1(String dNo1) {
        this.dNo1 = dNo1;
    }

    public String getLabelName2() {
        return labelName2;
    }

    public void setLabelName2(String labelName2) {
        this.labelName2 = labelName2;
    }

    public String gethNo2() {
        return hNo2;
    }

    public void sethNo2(String hNo2) {
        this.hNo2 = hNo2;
    }

    public String getLabelName3() {
        return labelName3;
    }

    public void setLabelName3(String labelName3) {
        this.labelName3 = labelName3;
    }

    public String getpNo3() {
        return pNo3;
    }

    public void setpNo3(String pNo3) {
        this.pNo3 = pNo3;
    }

    public String getLabelName4() {
        return labelName4;
    }

    public void setLabelName4(String labelName4) {
        this.labelName4 = labelName4;
    }

    public String getAreaCode4() {
        return areaCode4;
    }

    public void setAreaCode4(String areaCode4) {
        this.areaCode4 = areaCode4;
    }

    public String getLabelName5() {
        return labelName5;
    }

    public void setLabelName5(String labelName5) {
        this.labelName5 = labelName5;
    }

    public String getId5() {
        return id5;
    }

    public void setId5(String id5) {
        this.id5 = id5;
    }

    public String getName5() {
        return name5;
    }

    public void setName5(String name5) {
        this.name5 = name5;
    }

    public String getLabelName6() {
        return labelName6;
    }

    public void setLabelName6(String labelName6) {
        this.labelName6 = labelName6;
    }

    public String getRelationNo6() {
        return relationNo6;
    }

    public void setRelationNo6(String relationNo6) {
        this.relationNo6 = relationNo6;
    }

    public String getLabelName7() {
        return labelName7;
    }

    public void setLabelName7(String labelName7) {
        this.labelName7 = labelName7;
    }

    public String getId7() {
        return id7;
    }

    public void setId7(String id7) {
        this.id7 = id7;
    }

    public String getAttachmentName7() {
        return attachmentName7;
    }

    public void setAttachmentName7(String attachmentName7) {
        this.attachmentName7 = attachmentName7;
    }

    public String getLabelName8() {
        return labelName8;
    }

    public void setLabelName8(String labelName8) {
        this.labelName8 = labelName8;
    }

    public String getId8() {
        return id8;
    }

    public void setId8(String id8) {
        this.id8 = id8;
    }

    public String getAttachmentName8() {
        return attachmentName8;
    }

    public void setAttachmentName8(String attachmentName8) {
        this.attachmentName8 = attachmentName8;
    }

    public String getTownName() {
        return townName;
    }

    public void setTownName(String townName) {
        this.townName = townName;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getMatching() {
        return matching;
    }

    public void setMatching(String matching) {
        this.matching = matching;
    }

    public Long getPreventId() {
        return preventId;
    }

    public void setPreventId(Long preventId) {
        this.preventId = preventId;
    }

    public Long getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Long monitorId) {
        this.monitorId = monitorId;
    }

    public Long getMonitorHuman() {
        return monitorHuman;
    }

    public void setMonitorHuman(Long monitorHuman) {
        this.monitorHuman = monitorHuman;
    }

    public String getTaken() {
        return taken;
    }

    public void setTaken(String taken) {
        this.taken = taken;
    }

    public String getRelationNo() {
        return relationNo;
    }

    public void setRelationNo(String relationNo) {
        this.relationNo = relationNo;
    }

    public Date getPhotographyTime() {
        return photographyTime;
    }

    public void setPhotographyTime(Date photographyTime) {
        this.photographyTime = photographyTime;
    }

    public String getPhotographyType() {
        return photographyType;
    }

    public void setPhotographyType(String photographyType) {
        this.photographyType = photographyType;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public List<TabTwoRound> getTwoRounds() {
        return twoRounds;
    }

    public void setTwoRounds(List<TabTwoRound> twoRounds) {
        this.twoRounds = twoRounds;
    }

    public String getTwoRoundUrl() {
        return twoRoundUrl;
    }

    public void setTwoRoundUrl(String twoRoundUrl) {
        this.twoRoundUrl = twoRoundUrl;
    }

    public String getTwoRoundName() {
        return twoRoundName;
    }

    public void setTwoRoundName(String twoRoundName) {
        this.twoRoundName = twoRoundName;
    }

    public String getPicType() {
        return picType;
    }

    public void setPicType(String picType) {
        this.picType = picType;
    }

    /**
     * 条数
     */
    private Integer limit = 10;

    private String labelName1;

    private String dNo1;

    private String labelName2;

    private String hNo2;

    private String labelName3;

    private String pNo3;

    private String labelName4;

    private String areaCode4;

    private String labelName5;

    private String id5;

    private String name5;

    private String labelName6;

    private String relationNo6;

    private String labelName7;

    private String id7;

    private String attachmentName7;

    private String labelName8;

    private String id8;

    private String attachmentName8;

    private String townName;

    private String countyName;

    private String matching;

    /**
     * 图片
     */
    private List<TabAttachmentVo> imgs;
    /**
     * 视频
     */
    private List<TabAttachmentVo> videos;

    /**
     * 图片
     */
    private List<Map<String,Object>> BasicImgs;
    /**
     * 视频
     */
    private List<Map<String,Object>> BasicVideos;

    /**
     * 全景图
     */
    private List<TabTwoRound> twoRounds;
    /**
     * 全景图url
     */
    private String twoRoundUrl;
    /**
     * 全景图名称
     */
    private String twoRoundName;
    /**
     * 全景图类型(0:图片通过工具转化，1:无人机中心提供)
     */
    private String picType;

    /**
     * 防灾责任人
     */
    private Long preventId;

    /**
     * 监测责任人
     */
    private Long monitorId;

    /**
     * 监测人
     */
    private Long monitorHuman;

    private String taken;

    private String relationNo;

    private Date photographyTime;

    private String photographyType;

    private String modelType;

    private String model;

    private String top;

    private String personId;

    private String lonLat;

    public String getLonLat() {
        return lonLat;
    }

    public void setLonLat(String lonLat) {
        this.lonLat = lonLat;
    }


    private String level;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    private String getAreaCodeSplit() {
        if (null != getAreaCode()) {
            return getAreaCode().substring(0, 2);
        }
        return null;
    }

    private String areaCode;

    @Override
    public String getAreaCode() {
        return areaCode;
    }

    @Override
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    private List<TabArea> set;

    public List<TabArea> getSet() {
        return set;
    }

    public void setSet(List<TabArea> set) {
        this.set = set;
    }

    public List<TabAttachmentVo> getImgs() {
        return imgs;
    }

    public void setImgs(List<TabAttachmentVo> imgs) {
        this.imgs = imgs;
    }

    public List<TabAttachmentVo> getVideos() {
        return videos;
    }

    public void setVideos(List<TabAttachmentVo> videos) {
        this.videos = videos;
    }

    public List<Map<String, Object>> getBasicImgs() {
        return BasicImgs;
    }

    public void setBasicImgs(List<Map<String, Object>> basicImgs) {
        BasicImgs = basicImgs;
    }

    public List<Map<String, Object>> getBasicVideos() {
        return BasicVideos;
    }

    public void setBasicVideos(List<Map<String, Object>> basicVideos) {
        BasicVideos = basicVideos;
    }
}
