package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabWarnPeople;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author : laishihao
 * @date : 2019/10/23
 * description :
 */
@Data
public class TabWarnPeopleVo extends TabWarnPeople {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;

    private List<TabArea> list;

    private List<Map<String,Object>> tableData;

    private List<String> levelList;

    private String ids;
}
