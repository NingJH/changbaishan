package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabAttachmentMapper;
import com.cqndt.disaster.device.entity.data.TabAttachment;
import com.cqndt.disaster.device.service.data.TabAttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created By marc
 * Date: 2019/6/19  15:04
 * Description:
 */
@Service
public class TabAttachmentServiceImpl implements TabAttachmentService {
    @Autowired
    private TabAttachmentMapper tabAttachmentMapper;
    @Override
    public void saveTabAttachments(List<TabAttachment> attachments) {
        if(null != attachments){
            for (TabAttachment vo:attachments) {
                tabAttachmentMapper.save(vo);
            }
        }
    }

    @Override
    public void saveTabAttachment(TabAttachment attachment) {
        tabAttachmentMapper.save(attachment);
    }
}
