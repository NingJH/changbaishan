package com.cqndt.disaster.device.entity.data;

import lombok.Data;

@Data
public class TabUnit {

        private String id;

        private String unitName;

        private String unitPerson;

        private String phone;

        private String unitType;

        private Integer areaCode;

        private String unitAddress;
}
