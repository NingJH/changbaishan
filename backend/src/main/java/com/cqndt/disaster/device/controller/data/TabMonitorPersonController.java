package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.service.data.TabAreaService;
import com.cqndt.disaster.device.service.data.TabMonitorPersonService;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import com.cqndt.disaster.device.util.AbstractController;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabMonitorPersonVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("tabMonitorPerson")
@Api(value = "监测人管理", description = "监测人管理")
public class TabMonitorPersonController extends AbstractController {

    @Autowired
    private TabMonitorPersonService tabMonitorPersonService;
    @Autowired
    private TabAreaService tabAreaService;


    @ApiOperation(value = "监测人下拉列表（所属地区下）", notes = "监测人下拉列表（所属地区下）")
    @PostMapping("getMonitorPerson")
    public Result getMonitorPerson(TabMonitorPersonVo vo){
        SysUserVo sysUserVo = getUser();
        List<TabArea> list = tabAreaService.TabAreaList(sysUserVo);
        vo.setList(list);
        return tabMonitorPersonService.getMonitorPerson(vo);
    }


    @ApiOperation(value = "查询监测人大列表（所属地区下）", notes = "查询监测人大列表（所属地区下）")
    //@RequiresPermissions("sys:tabMonitorPerson:list")
    @PostMapping("listMonitorPerson")
    public Result listMonitorPerson(@RequestBody TabMonitorPersonVo vo){
        SysUserVo sysUserVo = getUser();
        List<TabArea> list = tabAreaService.TabAreaList(sysUserVo);
        vo.setList(list);
        return  tabMonitorPersonService.getMonitorPersonList(vo);
    }


    @ApiOperation(value = "新增监测人", notes = "新增监测人")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "name", value = "监测人名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "phone", value = "监测人手机号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "地区code", defaultValue = "", required = true, paramType = "query")
    })
    //@RequiresPermissions("sys:tabMonitorPerson:list")
    @PostMapping("addMonitorPerson")
    public Result addMonitorPerson(@RequestBody TabMonitorPersonVo vo){
        return tabMonitorPersonService.addMonitorPerson(vo);
    }

    @ApiOperation(value = "修改监测人", notes = "修改监测人")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "name", value = "监测人名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "phone", value = "监测人手机号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "地区code", defaultValue = "", required = true, paramType = "query")
    })
    //@RequiresPermissions("sys:tabMonitorPerson:update")
    @PostMapping("updateMonitorPerson")
    public Result updateMonitorPerson(@RequestBody TabMonitorPersonVo vo){
        return tabMonitorPersonService.updateMonitorPerson(vo);
    }

    @ApiOperation(value = "删除监测人信息", notes = "删除监测人信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "需要删除的id", defaultValue = "", required = true, paramType = "query")
    })
    @PostMapping("deleteMonitorPerson")
    //@RequiresPermissions("sys:tabUnit:delete")
    public Result deleteMonitorPerson(@RequestBody TabMonitorPersonVo vo) {
        return tabMonitorPersonService.deleteMonitorPerson(vo);
    }
}
