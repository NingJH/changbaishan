package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.service.data.TabDeviceInstallService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabDeviceInstallVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Description: 
* Date: 2019-06-26
*/
@RestController
@RequestMapping("tabDeviceInstall")
public class TabDeviceInstallController {
    @Autowired
    private TabDeviceInstallService tabDeviceInstallService;

    @SysLog("查询设备安装信息")
    @PostMapping("list")
    @RequiresPermissions("sys:tabdeviceinstall:list")
    public Result listTabUser(@RequestBody TabDeviceInstallVo tabDeviceInstallVo) {
        PageHelper.startPage(tabDeviceInstallVo.getPage(),tabDeviceInstallVo.getLimit());
        PageInfo<TabDeviceInstallVo> pageInfo = new PageInfo<TabDeviceInstallVo>(tabDeviceInstallService.list(tabDeviceInstallVo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabDeviceInstallById")
    public Result getTabDeviceInstallById(Long id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        TabDeviceInstallVo  tabDeviceInstallVo = tabDeviceInstallService.getTabDeviceInstallById(id);
        return new Result().success(tabDeviceInstallVo);
    }

    /**
     * 保存
     */
    @SysLog("增加")
    @PostMapping("save")
    @RequiresPermissions("sys:tabdeviceinstall:save")
    public Result save(@RequestBody TabDeviceInstallVo tabDeviceInstallVo){
        Result result = new Result();
        try {
            tabDeviceInstallService.save(tabDeviceInstallVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("update")
    @RequiresPermissions("sys:tabdeviceinstall:update")
    public Result update(@RequestBody TabDeviceInstallVo tabDeviceInstallVo){
        Result result = new Result();
        try {
            if(null == tabDeviceInstallVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabDeviceInstallService.update(tabDeviceInstallVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabdeviceinstall:delete")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabDeviceInstallService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
       return result;
    }


}