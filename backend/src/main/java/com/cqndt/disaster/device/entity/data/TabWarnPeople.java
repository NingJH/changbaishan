package com.cqndt.disaster.device.entity.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @author : laihan
 * @date : 2019/10/22
 * description :
 */
@Data
public class TabWarnPeople {
    //主键
    private Integer id;
    //乡镇或者街道Code
    private Integer areaCode;
    //灾害点id（获取灾害点名称（等于项目名称））
    private Integer basicId;
    //单位名称
    private String unitData;
    //人员姓名
    private String name;
    //职务
    private String job;
    //电话号码
    private String phone;
    //1.红色预警
    private String level1;
    //2.橙色预警
    private String level2;
    //3.黄色预警
    private String level3;
    //4.蓝色预警
    private String level4;
    //地址
    private String address;
    //村委会名称
    private String country;
    //项目名称
    private String projectName;
    //修改时间
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    private Date updateTime;
}
