package com.cqndt.disaster.device.vo.data;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class TabjieshouVo {
    private Integer id;
    //基础数据
    private Map<String ,String> base;

    //设备阈值信息
    private List<Map<String ,Object>> thres;
}
