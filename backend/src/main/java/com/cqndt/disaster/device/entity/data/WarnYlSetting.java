package com.cqndt.disaster.device.entity.data;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 雨量阈值
 *
 * @Author yin_q
 * @Date 2019/9/10 15:57
 * @Email yin_qingqin@163.com
 **/
@Data
public class WarnYlSetting implements Serializable {

    private int id;

    private BigDecimal value;

    private Integer hours;

    private Integer level;

    private Integer type;

    private String sensorNo;

    private String unit;

}
