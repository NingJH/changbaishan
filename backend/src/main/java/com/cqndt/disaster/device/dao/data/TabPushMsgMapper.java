package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.SysPushmsg;
import com.cqndt.disaster.device.entity.data.TabPushMsg;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author liaohonglai
 * @date 2019/7/30
 */
@Mapper
public interface TabPushMsgMapper {

     List<TabPushMsg> getYl(@Param("time") String time,@Param("maxId") int maxId);

    SysPushmsg getYLConfig(String type);

    int updateSysPushMsg(SysPushmsg myConfig);

    String getMoniterNo(String sensorNo);

    List<TabPushMsg> getGnss(@Param("time") String time,@Param("maxId") int maxId);
}
