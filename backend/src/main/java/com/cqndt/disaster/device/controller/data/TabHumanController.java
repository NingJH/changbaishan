package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.service.data.TabHumanService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabHumanVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Description: 
* Date: 2019-06-05
*/
@RestController
@RequestMapping("tabHuman")
public class TabHumanController {
    @Autowired
    private TabHumanService tabHumanService;

    @PostMapping("list")
    public Result listTabUser(@RequestBody TabHumanVo tabHumanVo) {
        PageHelper.startPage(tabHumanVo.getPage(),tabHumanVo.getLimit());
        PageInfo<TabHumanVo> pageInfo = new PageInfo<TabHumanVo>(tabHumanService.list(tabHumanVo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabHumanById")
    public Result getTabHumanById(Long id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        TabHumanVo  tabHumanVo = tabHumanService.getTabHumanById(id);
        return new Result().success(tabHumanVo);
    }

    /**
     * 保存
     */
    @SysLog("增加")
    @PostMapping("save")
    @RequiresPermissions("sys:tabhuman:save")
    public Result save(@RequestBody TabHumanVo tabHumanVo){
        Result result = new Result();
        try {
            tabHumanService.save(tabHumanVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("update")
    @RequiresPermissions("sys:tabhuman:update")
    public Result update(@RequestBody TabHumanVo tabHumanVo){
        Result result = new Result();
        try {
            if(null == tabHumanVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabHumanService.update(tabHumanVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabhuman:delete")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabHumanService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
       return result;
    }


}