package com.cqndt.disaster.device.entity.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.*;
import java.math.BigDecimal;
import lombok.Data;

/**
* Description: 视频监控管理
* Date: 2019-05-31
*/

@Data
public class TabVideoMonitoring implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  视频监控id
     */
    private Long id;
    /**
     *  设备名称
     */
    private String deviceName;

    private String mark;//标识，1.前端展示

    private String appkey;

    private String appSecret;

    private String rtmpUrl;

    private String httpUrl;

    /**
     *  经度
     */
    private BigDecimal longitude;

    /**
     *  纬度
     */
    private BigDecimal latitude;

    /**
     *  安装时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date installTime;



    /**
     *  区域编号
     */
    private Long areaId;
    /**
     *  描述
     */
    private String explains;
    /**
     *  安装图片
     */
    private String installPic;
    /**
     *  视频图片
     */
    private String resultPic;


    /**
     *  IP地址
     */
    private String ipAddress;
    /**
     *  端口号
     */
    private String port;
    /**
     *  用户名
     */
    private String userName;
    /**
     *   密码
     */
    private String password;
    /**
     *  项目id
     */
    private Long projectId;
    /**
     *  设备厂商类型（与tab_static_type的static_keyVal关联 1.海康威视 2.大华 3.华测）
     */
    private Long staticKey;
    /**
     *  地址
     */
    private String address;
    /**
     *  摄像头通道id
     */
    private String figure;


    private String channelNo = "1";//通道号

    private String serial;//编号

    private String deleteId;

    private String img;//图片地址,关联tab_attachment的id
}
