package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabTenderProject;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.vo.data.TabTenderProjectVo;

import java.util.List;
import java.util.Map;

public interface TabTenderProjectService {
    /**
     * 增加
     * @param tabTenderProjectVo
     */
    void save(TabTenderProjectVo tabTenderProjectVo) throws Exception;
     /**
     * 修改
     * @param tabTenderProjectVo
     */
     void update(TabTenderProjectVo tabTenderProjectVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabTenderProjectVo
     */
    TabTenderProjectVo getTabTenderProjectById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabTenderProjectVo 查询条件
     * @return 数据列表
     */
    List<TabTenderProjectVo> list(TabTenderProjectVo tabTenderProjectVo);

    /**
     * 下拉框list
     */
    List<Map<String,Object>> listTabStaticType(TabTenderProjectVo tabTenderProjectVo);
    

}