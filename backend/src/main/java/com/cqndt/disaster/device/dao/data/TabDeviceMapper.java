package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabDeviceShow;
import com.cqndt.disaster.device.vo.data.TabDeviceVo;
import com.cqndt.disaster.device.vo.data.TabMonitorPersonVo;
import com.cqndt.disaster.device.vo.data.TabjsVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Description: 
 * Date: 2019-06-11
 */

@Mapper
public interface TabDeviceMapper {
    /**
     * 根据条件查询list
     * @param tabDeviceVo 查询条件
     */
    List<TabDeviceVo> list(TabDeviceVo tabDeviceVo);

    /**
     *获取所有设备
     */
    List<Map<String,Object>> getAllDevice(@Param("list") List list);

    /**
     * 根据设备id获取设备
     */
    List<TabjsVo> getDeviceByDeviceNo(@Param("projectNo") int projectNo, @Param("id") int id);

    /**
     * 根据设备id获取设备类型
     */
    String getDeviceTypeByDeviceNo(@Param("id") int id);
    /**
     * 根据设备id获取设备名称、项目id等
     */
    List<Map<String ,Object>> getDeviceTypeByDeviceName(@Param("id") int id);
    /**
     * 根据设备id获取且通过名称分组
     */
    Map<String ,Object> getDeviceTypeByDeviceNameGroup(@Param("id") int id);

    /**
     * 增加
     * @param tabDeviceVo 条件
     */
    int save(TabDeviceVo tabDeviceVo);

    /**
     * 增加
     * @param tabDeviceShow 条件
     */
    int saveShowType(TabDeviceShow tabDeviceShow);
    /**
     * 修改
     * @param tabDeviceVo 条件
     */
    int update(TabDeviceVo tabDeviceVo);

    int updateDeviceShow(TabDeviceShow tabDeviceShow);


    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    TabDeviceVo getTabDeviceById(Long id);

    List<String> getAllDeviceName();

    List<TabMonitorPersonVo> getMonitorPersonN();

    List<String> getMonitorPerson();
    //查询所有关联了设备的监测人
    List<String> getAllMonitorPersonGroup();

    /**
     * author afnhai
     * description 查询当前用户所有设备信息
     */
    List<TabDeviceVo> listAll(Long id);

    List<Map<String,Object>> getDeviceName(@Param("list") List<String> monitorPerson);

    List<Map<String,Object>> getProjectName(@Param("list") List<String> listDevice);

    int save2(@Param("deviceNo") String deviceNo,
              @Param("now") Date now, @Param("qxjd") Object qxjd, @Param("temp") Object temp);

    int save3(@Param("deviceNo") String deviceNo,
              @Param("now") Date now, @Param("wyl") Object wyl,@Param("x") Object x,
              @Param("y") Object y,@Param("z") Object z,@Param("temp") Object temp);

    int save4(@Param("deviceNo") String deviceNo,
              @Param("now") Date now, @Param("x") Object x, @Param("y") Object y,@Param("z") Object z,
              @Param("x1") Object x1, @Param("y1") Object y1,@Param("z1") Object z1,
              @Param("angel") Object angel, @Param("swy") Object swy,@Param("cwy") Object cwy,
              @Param("swysd") Object swysd, @Param("cwysd") Object cwysd,@Param("swyjs") Object swyjs,
              @Param("cwyjs") Object cwyjs);

    int save6(@Param("deviceNo") String deviceNo,
              @Param("now") Date now, @Param("x") Object x, @Param("y") Object y,@Param("z") Object z,
              @Param("temp") Object temp);

    int save7(@Param("deviceNo") String deviceNo,
              @Param("now") Date now, @Param("value") Object value);

    int save8(@Param("deviceNo") String deviceNo,
              @Param("now") Date now, @Param("value") Object value);
    int save9(@Param("deviceNo") String deviceNo,
              @Param("now") Date now, @Param("value") Object value);
    int save10(@Param("deviceNo") String deviceNo,
              @Param("now") Date now, @Param("value") Object value);
    int save11(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("hsl") Object hsl,@Param("deep") Object deep);
    int save12(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("value") Object value);
    int save13(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("value") Object value);
    int save14(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("humidity") Object humidity,@Param("temp") Object temp);
    int save15(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("value") Object value);
    int save16(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("zf") Object zf,@Param("pl") Object pl);
    int save18(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("value") Object value);
    int save19(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("x") Object x, @Param("y") Object y,@Param("h") Object h,
               @Param("x1") Object x1, @Param("y1") Object y1,@Param("h1") Object h1);
    int save20(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("czgc") Object czgc);
    int save21(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("one") Object one,@Param("two") Object two,@Param("three") Object three);
    int save22(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("value") Object value);
    int save23(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("x") Object x,@Param("y") Object y);
    int save24(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("value") Object value);
    int save25(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("x") Object x,@Param("y") Object y,@Param("z") Object z);
    int save26(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("value") Object value);
    int save27(@Param("deviceNo") String deviceNo,
           @Param("now") Date now, @Param("x") Object x,@Param("y") Object y,@Param("z") Object z);
    int save28(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("value") Object value);
    int save30(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("value") Object value);
    int save31(@Param("deviceNo") String deviceNo,
               @Param("now") Date now, @Param("value") Object value);
    int save32(@Param("deviceNo") String deviceNo, @Param("now") Date now,
               @Param("value") Object value);
    int save33(@Param("deviceNo") String deviceNo, @Param("now") Date now,
               @Param("value") Object value);

}
