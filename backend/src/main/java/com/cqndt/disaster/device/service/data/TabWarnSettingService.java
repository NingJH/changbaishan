package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.vo.data.TabWarnSettingVo;

import java.util.List;

public interface TabWarnSettingService {
    /**
     * 增加
     * @param tabWarnSettingVo
     */
    void save(TabWarnSettingVo tabWarnSettingVo) throws Exception;
     /**
     * 修改
     * @param tabWarnSettingVo
     */
     void update(TabWarnSettingVo tabWarnSettingVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabWarnSettingVo
     */
    TabWarnSettingVo getTabWarnSettingById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabWarnSettingVo 查询条件
     * @return 数据列表
     */
    List<TabWarnSettingVo> list(TabWarnSettingVo tabWarnSettingVo);



}