package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.sys.vo.SysUserVo;

import java.util.List;

public interface TabAreaService {

    /**
     * 查询地区所有子节点id
     * @return
     */
    List<TabArea> TabAreaList(SysUserVo sysUserVo);
}
