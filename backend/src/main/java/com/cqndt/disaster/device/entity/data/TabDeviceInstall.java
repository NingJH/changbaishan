package com.cqndt.disaster.device.entity.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * Description:
 * Date: 2019-06-26
 */

@Data
public class TabDeviceInstall implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private Long id;
    /**
     * 基础规格
     */
    private String jcgg;
    /**
     * 安装位置
     */
    private String anwz;
    /**
     * 施工条件（1便利2较便利3不便利）
     */
    private String sgtj;
    /**
     * 供电方式（1.220v交流供电 2.太阳能供电）
     */
    private String gdfs;
    /**
     * 供电说明
     */
    private String gdsm;
    /**
     * 岩性条件（1.黄土2.砂卵石3.基岩4屋顶）
     */
    private String yxtj;
    /**
     * 采光条件（1.充足2.较充足3.不充足）
     */
    private String cgtj;
    /**
     * 移动信号（1强2中3弱）
     */
    private String ydxh;
    /**
     * gprs信号（1强2中3差4无）
     */
    private String gprsXh;
    /**
     * 电信3g（1强2中3差4无）
     */
    private String dxg;
    /**
     * 水流情况（1强2中3弱）
     */
    private String slqk;
    /**
     * 施工日期
     */
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date sgrq;
    /**
     * 竣工日期
     */
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date jgrq;
    /**
     * 施工人员
     */
    private String sgry;
    /**
     * 检查人员
     */
    private String jcry;
    /**
     * 验收人员
     */
    private String ysry;
    /**
     * 外观检查（1正常2损坏3水浸4污染）
     */
    private String wgjc;
    /**
     * 立杆安装（1成功2不成功）
     */
    private String lgaz;
    /**
     * 太阳能供电（1成功2不成功）
     */
    private String tyngd;
    /**
     * 数据采集（1成功2不成功）
     */
    private String sjcj;
    /**
     * 数据传输（1成功2不成功）
     */
    private String sjcs;
    /**
     * 安装日期
     */
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date azrq;
    /**
     * 完成日期
     */
//    @DateTimeFormat(pattern = "yyyy-MM-dd")
//    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date wcrq;
    /**
     * 安装人员
     */
    private String azry;
    /**
     * 安装检查人员
     */
    private String azjcry;
    /**
     * 安装验收人员
     */
    private String azysry;
    /**
     * 主机编号
     */
    private String zjbh;
    /**
     * 安装单位
     */
    private String azdw;
    /**
     * 备注
     */
    private String remark;
    /**
     * 设备图片
     */
    private String imgIds;
    /**
     * 设备图片
     */
    private String videoIds;
    /**
     * 安装图片
     */
    private String installImg;
    /**
     * 设备编号
     */
    private String deviceNo;
    /**
     * 温度
     */
    private Double temperature;

    private int deviceInstallInfoId;
}
