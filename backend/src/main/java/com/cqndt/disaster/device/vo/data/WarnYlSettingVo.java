package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.WarnGnssSetting;
import com.cqndt.disaster.device.entity.data.WarnYlSetting;
import lombok.Data;

import java.util.List;

/**
 * @Author yin_q
 * @Date 2019/8/31 16:12
 * @Email yin_qingqin@163.com
 **/
@Data
public class WarnYlSettingVo {

    private List<WarnYlSetting> warnYlSettingList;

    private String sensorNo;

}
