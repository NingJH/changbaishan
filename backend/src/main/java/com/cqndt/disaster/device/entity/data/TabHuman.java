package com.cqndt.disaster.device.entity.data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
* Description: 
* Date: 2019-06-05
*/

public class TabHuman implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getRealMobile() {
        return realMobile;
    }

    public void setRealMobile(String realMobile) {
        this.realMobile = realMobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    /**
     *

     */
    private Long id;
    /**
     *  姓名
     */
    private String name;
    /**
     *  手机IMEI
     */
    private String imsi;
    /**
     *  联系电话
     */
    private String realMobile;
    /**
     *  人员地址
     */
    private String address;
    /**
     *  区域id
     */
    private Long areaId;
    /**
     *  人员头像
     */
    private String headUrl;
    /**
     *  监测负责人、防灾责任人等
     */
    private String workType;
    /**
     *  经度
     */
    private BigDecimal longitude;
    /**
     *  纬度
     */
    private BigDecimal latitude;
    
}
