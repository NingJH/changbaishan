package com.cqndt.disaster.device.entity.data;

import lombok.Data;

@Data
public class TabMonitorPerson {
    //主键编号
    private String id;
    //监测人姓名
    private String name;
    //监测人电话
    private String phone;
    //监测人areaCode
    private String areaCode;
}
