package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabWarnPeople;
import com.cqndt.disaster.device.vo.data.TabBindWarnPeopleVo;
import com.cqndt.disaster.device.vo.data.TabWarnPeopleVo;
import com.cqndt.disaster.device.vo.data.TabWarnPersonVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author : laihan
 * @date : 2019/10/22
 * description :
 */
@Mapper
public interface TabWarnPeopleMapper {

    /**
     * 根据当前选择的乡镇地区获取所属的灾害点（项目）
     * @param areaCode
     * @return
     */
    List<Map<String,Object>> getBasicByCode(@Param("areaCode") Integer areaCode);

    /**
     * 外层大列表展示
     * @param vo
     * @return
     */
    List<Map<String, Object>> selectList(TabWarnPeopleVo vo);

    //void updateAddress(TabWarnPeopleVo tabWarnPeopleVo);
    void updateAddress(@Param("address") String address,@Param("basicId") Integer basicId);

    void updateProjectName(@Param("address") String address,@Param("projectName") String projectName,@Param("id") Integer id);

    /**
     * 获取 县人民政府 级的人员姓名
     * @param id
     * @return
     */
    List<String> getPeopleName(@Param("id")Integer id);


    int addWarnPeople(TabWarnPeopleVo peopleVo);

    List<Map<String, Object>> peopleList(@Param("id")Integer id);

    List<Integer> selectPeopleIdByBasicId(@Param("basicId")Integer basicId);

    int deletePeople(@Param("deletePeopleId")Integer deletePeopleId);
    int delByAreaCode(@Param("basicId")Integer basicId);

    int updatePeople(TabWarnPeopleVo peopleVo);

    int delete(@Param("id")int parseInt);

    List<Map<String,Object>> selectGroupBy(TabWarnPeopleVo vo);
    List<Map<String, Object>> getDataBy(@Param("address")String address,@Param("projectName")String projectName);

    List<TabBindWarnPeopleVo> selectList1(@Param("vo") TabBindWarnPeopleVo vo);

    List<Map<String,Object>> getAllData();

    List<TabWarnPersonVo> findWarnPeople(TabWarnPeopleVo vo);

//    List<TabBindWarnPeopleVo> selectList2(@Param("arr") String[] arr);
    Map<String,Object> selectBasicInfo(Integer id);

    List<TabWarnPeople> allWarnPeople();

}
