package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabBasicMapper;
import com.cqndt.disaster.device.dao.data.TabMonitorMapper;
import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.service.data.TabMonitorService;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.vo.data.TabMonitorVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class TabMonitorServiceImpl implements TabMonitorService {

    @Autowired
    private TabBasicMapper tabBasicMapper;

    @Autowired
    private TabMonitorMapper tabMonitorMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void save(TabMonitorVo tabMonitorVo) throws Exception {
        tabMonitorMapper.save(tabMonitorVo);

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(TabMonitorVo tabMonitorVo) throws Exception {
        //TabMonitorVo vo = tabMonitorMapper.getTabMonitorById(Long.valueOf(tabMonitorVo.getId()));
        //AttachmentVo attachmentVo = new AttachmentVo();
        tabMonitorMapper.update(tabMonitorVo);
    }

    @Override
    public TabMonitorVo getTabMonitorById(Long id) {
        return tabMonitorMapper.getTabMonitorById(id);
    }

    @Override
    public void delete(String id) {
        String[] ids = id.split(",");
        for (String idStr : ids) {
            tabMonitorMapper.delete(Integer.parseInt(idStr));
        }
    }

    @Override
    public List<TabMonitorVo> list(TabMonitorVo tabMonitorVo) {
        return tabMonitorMapper.list(tabMonitorVo);
    }

    @Override
    public List<Map<String, Object>> getUserProjectInfo(Long userId) {
        return tabMonitorMapper.getUserProjectInfo(userId);
    }

    @Override
    public List<Map<String, Object>> listTabAreaInfo(String areaId) {
        Map<String, Object> map = tabBasicMapper.listTabAreaInfo(areaId);
        List<Map<String, Object>> list = null;
        if (!StringUtils.isEmpty(map.toString()) && "1".equals(map.get("level").toString())) {
            list = tabBasicMapper.listTabAreaInfoByAreaParent(map.get("id").toString());
            for (Map<String, Object> map1 : list) {
                List<Map<String, Object>> list1 = tabBasicMapper.listTabAreaInfoByAreaParent(map1.get("id").toString());
                map1.put("children", list1);
            }
        }
        if (!StringUtils.isEmpty(map.toString()) && "2".equals(map.get("level").toString())) {
            list = tabBasicMapper.listTabAreaInfoByAreaParent(map.get("id").toString());
        }
        if(null==list){
            list=tabBasicMapper.listTabAreaInfoByAreaId(areaId);
        }
        return list;
    }

    @Override
    public List<Map<String, String>> getDeviceNameList(String deviceId, List<TabArea> list) {
        return tabMonitorMapper.getDeviceNameList(deviceId,list);
    }


}