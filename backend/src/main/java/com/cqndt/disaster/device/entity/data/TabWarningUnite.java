package com.cqndt.disaster.device.entity.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class TabWarningUnite extends TabWarningDevice{
    //主键
    private Integer id;
    //名称
    private String name;
    //是否启用（0.不启用，1.启用）
    private Integer isAvailable;
    //最后告警时间
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    private Date lastWarningTime;
    //蓝色告警（4级）
    private String blueWarning;
    //黄色告警（3级）
    private String yellowWarning;
    //橙色告警（2级）
    private String orangeWarning;
    //红色告警（1级）
    private String redWarning;
    //添加时间
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    private Date createTime;
    //修改时间
    @JsonFormat(
            pattern = "yyyy-MM-dd HH:mm:ss",
            timezone = "GMT+8"
    )
    private Date updateTime;
    //创建人
    private String createPerson;
    //修改人
    private String updatePerson;


}
