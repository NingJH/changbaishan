package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.*;
import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabBasic;
import com.cqndt.disaster.device.entity.data.TabPhotography;
import com.cqndt.disaster.device.entity.data.TabTwoRound;
import com.cqndt.disaster.device.service.data.TabBasicService;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import com.cqndt.disaster.device.vo.data.TabBasicHumanVo;
import com.cqndt.disaster.device.vo.data.TabBasicVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@Service
public class TabBasicServiceImpl implements TabBasicService {

    @Autowired
    private TabBasicMapper tabBasicMapper;

    @Autowired
    private TabAreaMapper tabAreaMapper;

    @Autowired
    private TabTwoRoundMapper tabTwoRoundMapper;

    @Autowired
    private TabBasicHumanMapper humanMapper;

    @Autowired
    private TabPhotographyMapper photographyMapper;

    @Autowired
    TabDeviceInstallMapper tabDeviceInstallMapper;

    @Value("${file-ip}")
    private String fileIp;
    @Value("attachmentUrlSet")
    private String attachmentUrlSet;
    @Value("tempAttachmentUrl")
    private String tempAttachmentUrl;

    private static Set<TabArea> listChild = new HashSet<>();

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void save(TabBasicVo tabBasicVo) throws Exception {

        if (!org.springframework.util.StringUtils.isEmpty(tabBasicVo)) {
            /*if (null != tabBasicVo.getPreventId() && !"".equals(tabBasicVo.getPreventId())) {
                TabBasicHumanVo humanVo = new TabBasicHumanVo();
                humanVo.setDiasaterNo(tabBasicVo.getDisNo());
                humanVo.setHumanId(tabBasicVo.getPreventId());
                humanMapper.save(humanVo);
            }
            if (null != tabBasicVo.getMonitorId() && !"".equals(tabBasicVo.getMonitorId())) {
                TabBasicHumanVo humanVo = new TabBasicHumanVo();
                humanVo.setDiasaterNo(tabBasicVo.getDisNo());
                humanVo.setHumanId(tabBasicVo.getMonitorId());
                humanMapper.save(humanVo);
            }
            if (null != tabBasicVo.getMonitorHuman() && !"".equals(tabBasicVo.getMonitorHuman())) {
                TabBasicHumanVo humanVo = new TabBasicHumanVo();
                humanVo.setDiasaterNo(tabBasicVo.getDisNo());
                humanVo.setHumanId(tabBasicVo.getMonitorHuman());
                humanMapper.save(humanVo);
            }*/
            //灾害点全景图保存
            String twoRoundUrl = tabBasicVo.getTwoRoundUrl();
            if (null != twoRoundUrl && !"".equals(twoRoundUrl)) {
                insertTabTwoRound(twoRoundUrl, tabBasicVo.getTwoRoundName(), tabBasicVo.getDisNo());
            }

            if (null != tabBasicVo.getDisNo() && "" != tabBasicVo.getDisNo()) {
                humanMapper.deleteByDisNo(tabBasicVo.getDisNo());
            }
            if (StringUtils.isNotEmpty(tabBasicVo.getPersonId())) {
                String[] str = tabBasicVo.getPersonId().split(",");
                for (String string : str) {
                    TabBasicHumanVo humanVo = new TabBasicHumanVo();
                    humanVo.setDiasaterNo(tabBasicVo.getDisNo());
                    humanVo.setHumanId(Long.valueOf(string));
                    humanMapper.save(humanVo);
                }
            }
            if (StringUtils.isNotEmpty(tabBasicVo.getPhotographyType())) {
                TabPhotography tabPhotography = new TabPhotography();
                tabPhotography.setRelationNo(tabBasicVo.getDisNo());
                tabPhotography.setModel(tabBasicVo.getModel());
                tabPhotography.setTaken(tabBasicVo.getTaken());
                tabPhotography.setPhotographyType(tabBasicVo.getPhotographyType());
                tabPhotography.setPhotographyTime(new Date());
                tabPhotography.setModelType("1");
                photographyMapper.insertSelective(tabPhotography);
            }
            tabBasicMapper.save(tabBasicVo);
        }

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(TabBasicVo tabBasicVo) throws Exception {
        if (!org.springframework.util.StringUtils.isEmpty(tabBasicVo)) {
            tabBasicMapper.update(tabBasicVo);
            if (null != tabBasicVo.getDisNo() && "" != tabBasicVo.getDisNo()) {
                humanMapper.deleteByDisNo(tabBasicVo.getDisNo());
            }
            if (StringUtils.isNotEmpty(tabBasicVo.getPersonId())) {
                String[] str = tabBasicVo.getPersonId().split(",");
                for (String string : str) {
                    TabBasicHumanVo humanVo = new TabBasicHumanVo();
                    humanVo.setDiasaterNo(tabBasicVo.getDisNo());
                    humanVo.setHumanId(Long.valueOf(string));
                    humanMapper.save(humanVo);
                }
            }
            //删除全景图
            tabTwoRoundMapper.deleteByRelationAndType(tabBasicVo.getDisNo(), "1");

            //灾害点全景图保存
            String twoRoundUrl = tabBasicVo.getTwoRoundUrl();
            if (null != twoRoundUrl && !"".equals(twoRoundUrl)) {
                insertTabTwoRound(twoRoundUrl, tabBasicVo.getTwoRoundName(), tabBasicVo.getDisNo());
            }

            if (StringUtils.isNotEmpty(tabBasicVo.getPhotographyType())) {
                TabPhotography tabPhotography = new TabPhotography();
                tabPhotography.setRelationNo(tabBasicVo.getDisNo());
                tabPhotography.setModel(tabBasicVo.getModel());
                tabPhotography.setTaken(tabBasicVo.getTaken());
                tabPhotography.setPhotographyType(tabBasicVo.getPhotographyType());
                tabPhotography.setPhotographyTime(new Date());
                tabPhotography.setModelType("1");
                int i = photographyMapper.getInfomationByDisNo(tabBasicVo.getDisNo());
                if (i > 0) {
                    photographyMapper.updateByPrimaryKeySelective(tabPhotography);
                } else {
                    photographyMapper.insertSelective(tabPhotography);
                }
            }
        }
    }


    @Override
    public TabBasicVo getTabBasicById(String id) {
        TabBasicVo vo = tabBasicMapper.getTabBasicById(id);
        //获取全景图
        List<TabTwoRound> tabTwoRounds = tabTwoRoundMapper.getByRelationAndType(vo.getDisNo(), "1");
        if (tabTwoRounds.size() > 0) {
            for (TabTwoRound round : tabTwoRounds) {
                round.setUrl(fileIp + File.separator + round.getUrl());
            }
            vo.setTwoRounds(tabTwoRounds);
        }
        return tabBasicMapper.getTabBasicById(id);
    }

    @Override
    public void delete(String id) {
        String[] ids = id.split(",");
        for (String idStr : ids) {
            TabBasicVo tabBasicVo = tabBasicMapper.getTabBasicById(idStr);
            tabTwoRoundMapper.deleteByRelationAndType(tabBasicVo.getDisNo(), "1");
            tabBasicMapper.delete(Integer.parseInt(idStr));
        }
    }

    private void insertTabTwoRound(String twoRoundUrl, String twoRoundName, String DisNo) {
        String[] urls = twoRoundUrl.split(",");
        String[] zipNames = twoRoundName.split(",");
        for (int i = 0; i < urls.length; i++) {
            TabTwoRound round = new TabTwoRound();
            round.setRelationNo(DisNo);
            round.setUrl(urls[i]);
            round.setModelType("1");
            round.setRoundName(zipNames[i]);
            round.setPicType(urls[i].endsWith("html") ? "1" : "0");
            round.setRoundTime(new Date());
            tabTwoRoundMapper.insertSelective(round);
        }
    }

    @Override
    public List<Map<String, Object>> list(TabBasicVo tabBasicVo, String areaCode, List<TabArea> listMenu) {
//        CopyOnWriteArrayList<TabArea> listMenu2 = new CopyOnWriteArrayList<TabArea>(listMenu);
//         listChild== new
//        Set<TabArea> listChild = treeAreaList(listMenu2,Integer.parseInt(areaCode));
//
//        /*StringBuffer areaCodeNew = new StringBuffer();
//        for(TabArea area:listChild){
//            areaCodeNew.append(area.getId()).append(",");
//        }
//        areaCodeNew.substring(0,areaCodeNew.lastIndexOf(","));*/
//        tabBasicVo.setSet(listChild);
//        String s = getAreaCode(areaCode);
//        System.out.println("11111111111111111111111111111111111111111:"+s);
        tabBasicVo.setSet(listMenu);
        List<Map<String, Object>> list = tabBasicMapper.list(tabBasicVo);
        if (null != list && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                List<Map<String, Object>> list1 = new ArrayList<>();
                String imgIds = (String) list.get(i).get("imgIds");
                if (StringUtils.isNotEmpty(imgIds)) {
                    String[] arr = imgIds.split(",");
                    for (int k = 0; k < arr.length; k++) {
                        if (StringUtils.isNotEmpty(arr[k])) {
                            Map<String, Object> map = tabDeviceInstallMapper.getImgUrlAndById(arr[k]);
                            if (map != null) {
                                map.put("viewUrl", fileIp + File.separator + "img" + File.separator + map.get("attachmentUrl"));
                                list1.add(map);

                            }
                        } else {
                            break;
                        }
                    }
                }
                List<Map<String, Object>> list2 = new ArrayList<>();
                String videosIds = (String) list.get(i).get("videoIds");
                if (StringUtils.isNotEmpty(videosIds)) {
                    String[] arr = videosIds.split(",");
                    for (int k = 0; k < arr.length; k++) {
                        if (StringUtils.isNotEmpty(arr[k])) {
                            Map<String, Object> map = tabDeviceInstallMapper.getImgUrlAndById(arr[k]);
                            if (map != null) {
                                map.put("viewUrl", fileIp + File.separator + "video" + File.separator + map.get("attachmentUrl"));
                                list2.add(map);
                            }
                        } else {
                            break;
                        }
                    }
                }
                list.get(i).put("BasicImgs", list1);
                list.get(i).put("BasicVideos", list2);
            }
        }
        if (!org.springframework.util.StringUtils.isEmpty(list)) {
            for (Map<String, Object> map : list) {
                String areaCodes = map.get("cityCode").toString() + "," + map.get("countyCode").toString() + "," + map.get("code").toString();
                map.put("areaCode", areaCodes);
                //获取全景图并显示
                List<TabTwoRound> tabTwoRounds = tabTwoRoundMapper.getByRelationAndType(map.get("disNo").toString(), "1");
                map.put("twoRounds", tabTwoRounds);
            }
        }

        return list;
    }

   /* public Set<TabArea> treeAreaList(List<TabArea> listMenu,int areaCode){
        for (TabArea area : listMenu) {
            if(null != area.getAreaParent()){
                //遍历出父id等于参数的id，add进子节点集合
                if (area.getAreaParent() == areaCode) {
                    //递归遍历下一级
                    listMenu.remove(area);
                    treeAreaList(listMenu, area.getId());
                    listChild.add(area);
                }
            }
        }
        return listChild;
    }*/

    public String getAreaCode(String areaParent) {
        String areaId = tabBasicMapper.getAreaById(areaParent);
        if (null == areaId || "0".equals(areaId)) {
            return areaId;
        } else {
            return getAreaCode(areaId) + "," + areaParent;
        }
    }

    @Override
    public List<TabBasicVo> listTabDisasterCardAll(TabBasicVo tabBasicVo) {
        return tabBasicMapper.listTabDisasterCardAll(tabBasicVo);
    }

    @Override
    public List<TabBasicVo> listTabHedgeCardAll(TabBasicVo tabBasicVo) {
        return tabBasicMapper.listTabHedgeCardAll(tabBasicVo);
    }

    @Override
    public List<TabBasicVo> listTabPlanInfoAll(TabBasicVo tabBasicVo) {
        return tabBasicMapper.listTabPlanInfoAll(tabBasicVo);
    }

    @Override
    public List<TabBasicVo> listTabAreaAll(TabBasicVo tabBasicVo) {
        return tabBasicMapper.listTabAreaAll(tabBasicVo);
    }

    @Override
    public List<TabBasicVo> listTabHumanAll(TabBasicVo tabBasicVo) {
        return tabBasicMapper.listTabHumanAll(tabBasicVo);
    }

    @Override
    public List<TabBasicVo> listTabPhotographyAll(TabBasicVo tabBasicVo) {
        return tabBasicMapper.listTabPhotographyAll(tabBasicVo);
    }

    @Override
    public List<TabBasicVo> listTabAttachmentAll(TabBasicVo tabBasicVo) {
        return tabBasicMapper.listTabAttachmentAll(tabBasicVo);
    }

    @Override
    public List<TabBasicVo> listTabDisasterCardSelect(Integer id) {
        return tabBasicMapper.listTabDisasterCardSelect(id);
    }

    @Override
    public List<TabBasicVo> listTabHumanSelect(Integer id) {
        return tabBasicMapper.listTabHumanSelect(id);
    }

    @Override
    public List<Map<String, Object>> listTabAreaInfo(String areaId) {
        Map<String, Object> map = tabBasicMapper.listTabAreaInfo(areaId);
        List<Map<String, Object>> list = null;
        if (!StringUtils.isEmpty(map.toString()) && "1".equals(map.get("level").toString())) {
            list = tabBasicMapper.listTabAreaInfoByAreaParent(map.get("id").toString());
            for (Map<String, Object> map1 : list) {
                List<Map<String, Object>> list1 = tabBasicMapper.listTabAreaInfoByAreaParent(map1.get("id").toString());
                map1.put("children", list1);
                for (Map<String, Object> map2 : list1) {
                    map2.put("children", tabBasicMapper.listTabAreaInfoByAreaParent(map2.get("id").toString()));
                }
            }
        }
        if (!StringUtils.isEmpty(map.toString()) && "2".equals(map.get("level").toString())) {
            //查询子地名
            list = tabBasicMapper.listTabAreaInfoByAreaParent(map.get("id").toString());
            for (Map<String, Object> map1 : list) {
                map1.put("children", tabBasicMapper.listTabAreaInfoByAreaParent(map1.get("id").toString()));
            }
        }
        if (!StringUtils.isEmpty(map.toString()) && "3".equals(map.get("level").toString())) {
            list = tabBasicMapper.listTabAreaInfoByAreaParent(map.get("id").toString());
        }
        if (null == list) {
            list = tabBasicMapper.listTabAreaInfoByAreaId(areaId);
        }
        return list;
    }

    @Override
    public List<Map<String, Object>> listTabPerson(String personId) {
        Map<String, Object> map = new HashMap();
        map.put("personId", personId.split(","));
        return tabBasicMapper.listTabPerson(map);
    }

    @Override
    public List<Map<String, Object>> getBaseForDisNo(String disNo) {
        return tabBasicMapper.getBaseForDisNo(disNo);
    }

    @Override
    public List<TabBasic> findAllBasic() {
        return tabBasicMapper.findAllBasic();
    }


}