package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabBasicMapper;
import com.cqndt.disaster.device.dao.data.TabPersonMapper;
import com.cqndt.disaster.device.dao.data.TabUnitMapper;
import com.cqndt.disaster.device.entity.data.TabPerson;
import com.cqndt.disaster.device.entity.data.TabUnit;
import com.cqndt.disaster.device.service.data.TabUnitService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabUnitVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service
public class TabUnitServiceImpl implements TabUnitService {

    @Autowired
    private TabUnitMapper tabUnitMapper;

    @Autowired
    private TabBasicMapper tabBasicMapper;

    @Override
    public List<TabUnit> getTabUnitName() {
        return tabUnitMapper.getTabUnitName();
    }

    @Override
    public Result queryList(TabUnitVo vo) {
        Result result = new Result();
        List<Map<String, Object>> list = tabUnitMapper.queryList(vo);
        for (Map<String, Object> map : list) {
            map.replace("unitAddress", map.get("unitAddress"), map.get("cityName").toString() + map.get("countyName").toString() + map.get("codeName").toString());
            String address=map.get("cityName").toString() + map.get("countyName").toString() + map.get("codeName").toString();
            map.put("areaCode", map.get("cityCode").toString() + "," + map.get("countyCode").toString() + "," + map.get("code").toString());
            tabUnitMapper.updateAddress(address,map.get("id").toString());
        }
        PageHelper.startPage(vo.getPage(), vo.getLimit());
        PageHelper.orderBy("id desc");
        List<Map<String, Object>> list1 = tabUnitMapper.queryList(vo);

        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(list1);
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        result.setMsg("查询成功");
        return result;
    }


    @Override
    public Result getVendorType() {
        List<Map<String, String>> list = tabUnitMapper.getVendorType();
        Result result = new Result();
        result.setCode(0);
        result.setData(list);
        result.setMsg("查询成功");
        return result;
    }

    @Override

    public Result addUnit(TabUnitVo vo) {
        Result result = new Result();
        if (vo.getUnitName().equals("") || vo.getUnitName() == null) {
            return result.failure(-1, "单位名称不能为空");
        }
        if (vo.getUnitPerson().equals("") || vo.getUnitPerson() == null) {
            return result.failure(-1, "单位负责人不能为空");
        }
        if (vo.getPhone().equals("") || vo.getPhone() == null) {
            return result.failure(-1, "电话号码不能为空");
        }
        if (vo.getAreaCode().equals("") || vo.getAreaCode() == null) {
            return result.failure(-1, "乡镇或者街道Code不能为空");
        }
        if (vo.getVendorId().equals("") || vo.getVendorId() == null) {
            return result.failure(-1, "厂商、单位id不能为空");
        }

        int i = tabUnitMapper.addUnit(vo);
        result.setCode(0);
        result.setData(i);
        result.setMsg("添加成功");
        return result;
    }

    @Override
    public Result deleteUnitById(TabUnitVo vo) {
        if (null == vo.getId()) {
            return new Result().failure(-1, "请选择一条数据进行操作");
        }
        String[] ids = vo.getId().split(",");
        for (String idStr : ids) {
            Integer row = tabUnitMapper.deleteUnitById(Integer.parseInt(idStr));
        }
        return new Result().success("删除成功");
    }

    @Override
    public Result updateUnitById(TabUnitVo vo) {
        Result result = new Result();
        if (vo.getUnitName().equals("") || vo.getUnitName() == null) {
            return result.failure(-1, "单位名称不能为空");
        }
        if (vo.getUnitPerson().equals("") || vo.getUnitPerson() == null) {
            return result.failure(-1, "单位负责人不能为空");
        }
        if (vo.getPhone().equals("") || vo.getPhone() == null) {
            return result.failure(-1, "电话号码不能为空");
        }
        if (vo.getAreaCode().equals("") || vo.getAreaCode() == null) {
            return result.failure(-1, "乡镇或者街道Code不能为空");
        }
        if (vo.getVendorId().equals("") || vo.getVendorId() == null) {
            return result.failure(-1, "厂商、单位id不能为空");
        }

        int i = tabUnitMapper.updateUnitById(vo);
        result.setCode(0);
        result.setData(i);
        result.setMsg("修改成功");
        return result;
    }

    @Override
    public Result getTabUnitById(Long id) {
        if (null == id) {
            return new Result().failure(-1, "请选择一条数据进行操作");
        }
        List<TabUnitVo> tabUnitById = tabUnitMapper.getTabUnitById(id);
        Result result = new Result();
        result.setCode(0);
        result.setData(tabUnitById);
        result.setMsg("查看成功");
        return result;
    }

    @Override
    public Result getTabUnitByVendorId(int vendorId) {
        Result result = new Result();
        List<TabUnitVo> listByVendorId = tabUnitMapper.getTabUnitByVendorId(vendorId);
        result.setData(listByVendorId);
        result.setMsg("查看成功");
        return result;
    }

    /**
     * 所属地区列表展示
     *
     * @param areaId
     * @return
     */
    public List<Map<String, Object>> listTabAreaInfo(String areaId) {
        Map<String, Object> map = tabBasicMapper.listTabAreaInfo(areaId);
        List<Map<String, Object>> list = null;
        if (!StringUtils.isEmpty(map.toString()) && "1".equals(map.get("level").toString())) {
            list = tabBasicMapper.listTabAreaInfoByAreaParent(map.get("id").toString());
            for (Map<String, Object> map1 : list) {
                List<Map<String, Object>> list1 = tabBasicMapper.listTabAreaInfoByAreaParent(map1.get("id").toString());
                map1.put("children", list1);
                for (Map<String, Object> map2 : list1) {
                    map2.put("children", tabBasicMapper.listTabAreaInfoByAreaParent(map2.get("id").toString()));
                }
            }
        }
        if (!StringUtils.isEmpty(map.toString()) && "2".equals(map.get("level").toString())) {
            list = tabBasicMapper.listTabAreaInfoByAreaParent(map.get("id").toString());
            for (Map<String, Object> map1 : list) {
                map1.put("children", tabBasicMapper.listTabAreaInfoByAreaParent(map1.get("id").toString()));
            }
        }
        if (!StringUtils.isEmpty(map.toString()) && "3".equals(map.get("level").toString())) {
            list = tabBasicMapper.listTabAreaInfoByAreaParent(map.get("id").toString());
        }
        if(null==list){
            list=tabBasicMapper.listTabAreaInfoByAreaId(areaId);
        }
        return list;
    }
}
