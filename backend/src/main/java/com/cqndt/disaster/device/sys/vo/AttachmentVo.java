package com.cqndt.disaster.device.sys.vo;

/**
 * Created By marc
 * Date: 2019/1/2  9:30
 * Description:附件信息
 */
public class AttachmentVo {
    /**
     * 附件id
     */
    private String attachmentId;
    /**
     * 附件路径(临时存储)
     */
    private String url;
    /**
     * 附件存储路径
     */
    private String attachmentUrl;

    /**
     * 附件类型
     */
    private String attachmentType;
    /**
     * 附件名称
     */
    private String name;
    /**
     * 附件上传时间
     */
    private String attachmentDate;
    /**
     * 附件描述
     */
    private String attachmentDesc;

    private String[] attachmentIds;

    public String[] getAttachmentIds() {
        return attachmentIds;
    }

    public void setAttachmentIds(String[] attachmentIds) {
        this.attachmentIds = attachmentIds;
    }

    public String getAttachmentDesc() {
        return attachmentDesc;
    }

    public void setAttachmentDesc(String attachmentDesc) {
        this.attachmentDesc = attachmentDesc;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttachmentType() {
        return attachmentType;
    }

    public void setAttachmentType(String attachmentType) {
        this.attachmentType = attachmentType;
    }


    public String getAttachmentDate() {
        return attachmentDate;
    }

    public void setAttachmentDate(String attachmentDate) {
        this.attachmentDate = attachmentDate;
    }

    public String getAttachmentUrl() {
        return attachmentUrl;
    }

    public void setAttachmentUrl(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
    }
}
