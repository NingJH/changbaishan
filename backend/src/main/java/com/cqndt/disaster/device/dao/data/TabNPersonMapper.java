package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabBasic;
import com.cqndt.disaster.device.entity.data.TabNPerson;
import com.cqndt.disaster.device.vo.data.TabMapVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author : afnhia
 * Description:
 * Date: 2020年7月24日
 */
@Mapper
public interface TabNPersonMapper {
    //添加人员
    void addPerson(TabNPerson nPerson);
    //添加人员与灾害点关系
    void addPersonAndBasic(@Param("personId") Integer personId,@Param("basicId") String basicId);
    //删除人员
    int deletePersonById(String personId);
    //删除与人员有关的灾害点的关系
    void deletePersonAndBasicByP(Integer personId);
    //删除人员与灾害点的关系
    void deletePersonAndBasicByPB(Integer personId,Integer basicId);

    //修改人员
    void updatePerson(TabNPerson nPerson);

    //查询人员
    TabNPerson findPersonById(Integer personId);
    //分页查询所有人员
    List<TabNPerson> findAllByPage(TabNPerson tabNPerson);

    List<String> findAllByPerson(Integer personId);

    void deletePersonBasicById(String personId);
}
