package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.vo.data.TabHedgeCardVo;

import java.util.List;

public interface TabHedgeCardService {
    /**
     * 增加
     * @param tabHedgeCardVo
     */
    void save(TabHedgeCardVo tabHedgeCardVo) throws Exception;
     /**
     * 修改
     * @param tabHedgeCardVo
     */
     void update(TabHedgeCardVo tabHedgeCardVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabHedgeCardVo
     */
    TabHedgeCardVo getTabHedgeCardById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabHedgeCardVo 查询条件
     * @return 数据列表
     */
    List<TabHedgeCardVo> list(TabHedgeCardVo tabHedgeCardVo);



}