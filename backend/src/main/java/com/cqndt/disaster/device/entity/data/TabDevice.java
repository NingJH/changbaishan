package com.cqndt.disaster.device.entity.data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
* Description: 
* Date: 2019-06-11
*/

public class TabDevice implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    /**
     *  
     */

    private Long id;
    /**
     *  设备编号
     */
    private String deviceNo;
    /**
     *  设备名称
     */
    private String deviceName;
    /**
     *  经度
     */
    private BigDecimal longitude;
    /**
     *  纬度
     */
    private BigDecimal latitude;
    /**
     *  监测类型
     */
    private String deviceType;
    /**
     *  sim卡
     */
    private String simCard;
    /**
     *  监测地址
     */
    private String address;
    /**
     *  野外编号
     */
    private String outsideNo;
    /**
     *  传输方式（1.GPRS协议2.北斗3电信3G）
     */
    private String transferType;
    /**
     *  北斗卡号
     */
    private String bdkh;
    /**
     *  下限值
     */
    private BigDecimal lowerValue;
    /**
     *  上限值
     */
    private BigDecimal upValue;
    /**
     *  标准电压值/伏
     */
    private BigDecimal dyz;
    /**
     *  采集字段
     */
    private String cjzd;
    /**
     *  沟口位置（1.近沟口2.远沟口）
     */
    private String gkwz;
    /**
     *  标准水位置
     */
    private BigDecimal bzswz;
    /**
     *  埋深
     */
    private BigDecimal buryingDepth;
    /**
     *  设备类型名称
     */
    private String deviceTypeName;
    /**
     *  高程
     */
    private BigDecimal height;
    /**
     *  平台(电信、移动、联通)设备id
     */
    private String devicePlatId;
    /**
     *  平台（1电信 2移动 3联通）
     */
    private String platType;
    /**
     *  设备sn
     */
    private String deviceSn;
    /**
     *  设备电量
     */
    private String deviceElectric;
    /**
     *  设备厂商
     */
    private String deviceFactory;

    /**
     * 设备精度
     */
    private String deviceScale;

    /**
     * 展示类型
     */
    private Integer showType;

    /**
     * 初始值
     */
    private Double originalValue;

    public Double getOriginalValue() {
        return originalValue;
    }

    public void setOriginalValue(Double originalValue) {
        this.originalValue = originalValue;
    }

    public Integer getShowType() {
        return showType;
    }

    public void setShowType(Integer showType) {
        this.showType = showType;
    }


    public String getDeviceScale() {
        return deviceScale;
    }

    public void setDeviceScale(String deviceScale) {
        this.deviceScale = deviceScale;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getSimCard() {
        return simCard;
    }

    public void setSimCard(String simCard) {
        this.simCard = simCard;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOutsideNo() {
        return outsideNo;
    }

    public void setOutsideNo(String outsideNo) {
        this.outsideNo = outsideNo;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getBdkh() {
        return bdkh;
    }

    public void setBdkh(String bdkh) {
        this.bdkh = bdkh;
    }

    public BigDecimal getLowerValue() {
        return lowerValue;
    }

    public void setLowerValue(BigDecimal lowerValue) {
        this.lowerValue = lowerValue;
    }

    public BigDecimal getUpValue() {
        return upValue;
    }

    public void setUpValue(BigDecimal upValue) {
        this.upValue = upValue;
    }

    public BigDecimal getDyz() {
        return dyz;
    }

    public void setDyz(BigDecimal dyz) {
        this.dyz = dyz;
    }

    public String getCjzd() {
        return cjzd;
    }

    public void setCjzd(String cjzd) {
        this.cjzd = cjzd;
    }

    public String getGkwz() {
        return gkwz;
    }

    public void setGkwz(String gkwz) {
        this.gkwz = gkwz;
    }

    public BigDecimal getBzswz() {
        return bzswz;
    }

    public void setBzswz(BigDecimal bzswz) {
        this.bzswz = bzswz;
    }

    public BigDecimal getBuryingDepth() {
        return buryingDepth;
    }

    public void setBuryingDepth(BigDecimal buryingDepth) {
        this.buryingDepth = buryingDepth;
    }

    public String getDeviceTypeName() {
        return deviceTypeName;
    }

    public void setDeviceTypeName(String deviceTypeName) {
        this.deviceTypeName = deviceTypeName;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }

    public String getDevicePlatId() {
        return devicePlatId;
    }

    public void setDevicePlatId(String devicePlatId) {
        this.devicePlatId = devicePlatId;
    }

    public String getPlatType() {
        return platType;
    }

    public void setPlatType(String platType) {
        this.platType = platType;
    }

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public String getDeviceElectric() {
        return deviceElectric;
    }

    public void setDeviceElectric(String deviceElectric) {
        this.deviceElectric = deviceElectric;
    }

    public String getDeviceFactory() {
        return deviceFactory;
    }

    public void setDeviceFactory(String deviceFactory) {
        this.deviceFactory = deviceFactory;
    }
}
