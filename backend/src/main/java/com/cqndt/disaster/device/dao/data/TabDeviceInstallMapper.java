package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.vo.data.TabDeviceInstallVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Description:
 * Date: 2019-06-26
 */

@Mapper
public interface TabDeviceInstallMapper {
    /**
     * 根据条件查询list
     *
     * @param tabDeviceInstallVo 查询条件
     */
    List<TabDeviceInstallVo> list(TabDeviceInstallVo tabDeviceInstallVo);

    /**
     * 增加
     *
     * @param tabDeviceInstallVo 条件
     */
    int save(TabDeviceInstallVo tabDeviceInstallVo);

    /**
     * 修改
     *
     * @param tabDeviceInstallVo 条件
     */
    int update(TabDeviceInstallVo tabDeviceInstallVo);

    /**
     * 删除
     *
     * @param id
     */
    int delete(Integer id);

    /**
     * 根据id查询单个记录
     *
     * @param id
     */
    TabDeviceInstallVo getTabDeviceInstallById(Long id);


    TabDeviceInstallVo getDeviceInstallInfo(String deviceNo);

    TabDeviceInstallVo getTabDeviceInstallByDeviceNo(String deviceNo);

    /**
     * 根据设备编号集合获取设备安装信息
     * @param deviceNoList
     * @return
     */
    List<TabDeviceInstallVo> getTabDeviceInstallByDeviceNoList(List<String> deviceNoList);


    Map<String, Object> getImgUrlAndById(String id);

    /**
     * 根据附件id集合获取附件对象
     * @param ids
     * @return
     */
    List<Map<String, Object>> getImgUrlAndByIdList(List<String> ids);

    /**
     * 设备视频集合
     * @param videoids
     * @return
     */
    List<Map<String, Object>> getvideoUrlAndByIdList(List<String> videoids);

    Map<String, Object> getUrlAndById(String id);

    void deleteByDeviceNo(String deviceNo);

    Map<String,Object> getUrlName(String url);
}
