package com.cqndt.disaster.device.entity.data;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class TabDeviceCheck {

    private Integer id;

    /*
     '设备状态(1.正常/0.异常)'
     */
    private String deviceState;

    /*
     '设备外观(1.完好 2.损毁 3.局部损毁)'
     */
    private String deviceAppearance;

    //设备现状原因
    private String reason;

    //蓄电池状态
    private String batteryState;

    //故障原因
    private String faultReason;

    //'后期能否恢复(0、否 1、是)'
    private String laterIsRecover;

    //恢复建议
    private String recoverAdvise;

    //建议阈值
    private BigDecimal adviseValue;

    //看管人姓名
    private String guardName;

    //看管人电话
    private String guardPhone;

    //主管人姓名
    private String managerName;

    //主管人电话
    private String managerPhone;

    //设备质量预评
    private String sbzlypg;

    //建设年度
    private String buildTime;

    //建设厂家
    private String buildUnit;

    //核实人
    private String checkPerson;

    //核实日期
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd",timezone="GMT+8")
    private Date checkDate;

    //备注
    private String remark;

    //设备编号
    private String deviceNo;

    //现场图片
    private String imgIds;

    //视频
    private String videoIds;
}
