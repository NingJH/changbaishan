package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabWarnSetting;
import com.cqndt.disaster.device.entity.data.WarnDlfSetting;
import com.cqndt.disaster.device.entity.data.WarnYlSetting;
import com.cqndt.disaster.device.service.data.TabAreaService;
import com.cqndt.disaster.device.service.data.TabDeviceService;
import com.cqndt.disaster.device.sys.vo.SysUserVo;
import com.cqndt.disaster.device.util.AbstractController;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Description:
 * Date: 2019-06-11
 */
@RestController
@RequestMapping("tabDevice")
@Api(description = "监测设备", value = "监测设备")
public class TabDeviceController extends AbstractController {

    @Autowired
    private TabDeviceService tabDeviceService;
    @Autowired
    private TabAreaService tabAreaService;


    @PostMapping("getAllDeviceName")
    public Result getAllDeviceName(TabMonitorPersonVo tabMonitorPersonVo) {
        return tabDeviceService.getAllDeviceName(tabMonitorPersonVo);
    }
    @PostMapping("list")
    @RequiresPermissions("sys:tabdevice:list")
    public Result listTabDevice(@RequestBody TabDeviceVo tabDeviceVo) {
        SysUserVo sysUserVo = getUser();
        List<TabArea> list = tabAreaService.TabAreaList(sysUserVo);
        tabDeviceVo.setList(list);
        PageHelper.startPage(tabDeviceVo.getPage(), tabDeviceVo.getLimit());
        PageInfo<TabDeviceVo> pageInfo = new PageInfo<TabDeviceVo>(tabDeviceService.list(tabDeviceVo));
        Result result = new Result();
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    /**
     * 信息
     */
    @PostMapping("getTabDeviceById")
    public Result getTabDeviceById(Long id) {
        if (null == id) {
            return new Result().failure(-1, "请选择一条数据进行操作");
        }
        TabDeviceVo tabDeviceVo = tabDeviceService.getTabDeviceById(id);
        return new Result().success(tabDeviceVo);
    }

    /**
     * 保存
     */
    @SysLog("增加")
    @PostMapping("save")
    @RequiresPermissions("sys:tabdevice:save")
    public Result save(@RequestBody TabDeviceVo tabDeviceVo) {
        Result result = new Result();
        try {
            tabDeviceVo.setAreaCode(Integer.parseInt(getUser().getAreaCode()));
            tabDeviceService.save(tabDeviceVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("update")
    @RequiresPermissions("sys:tabdevice:update")
    public Result update(@RequestBody TabDeviceVo tabDeviceVo) {
        Result result = new Result();
        try {
            if (null == tabDeviceVo.getId()) {
                return result.failure(-1, "请选择至少一条数据进行操作");
            }
            tabDeviceService.update(tabDeviceVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabdevice:delete")
    public Result delete(String id) {
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1, "请选择至少一条数据进行操作");
            }
            tabDeviceService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }
    /**
     * 导出设备报表
     */
    @ApiOperation(value = "导出设备报表", notes = "导出设备报表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "DeviceName", value = "设备名称", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "DeviceTypeName", value = "设备类型名称", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "State", value = "设备状态", defaultValue = "", paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "DeviceNo", value = "设备编号", defaultValue = "",  paramType = "query")
    })
    @GetMapping("exportDeviceExcel")
    public void exportDeviceExcel(TabDeviceVo tabDeviceVo, HttpServletResponse response, HttpServletRequest request){
        SysUserVo sysUserVo = getUser();
        List<TabArea> list = tabAreaService.TabAreaList(sysUserVo);
        tabDeviceVo.setList(list);
        tabDeviceService.exportDeviceExcel(tabDeviceVo,response);
    }


    @SysLog("新增监测数据文档")
    @PostMapping("saveMonitorDataDatum")
    @RequiresPermissions("sys:tabdevice:upload")
    @ApiOperation(value = "新增监测数据文档", notes = "新增监测数据文档")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "sensor_no", value = "设备编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceType", value = "设备类型", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "fileName", value = "文件名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "path", value = "文件绝对路径", defaultValue = "", required = true, paramType = "query")
    })
    public Result saveTabProjectDatum(@RequestParam("deviceNo") String deviceNo,
                                      @RequestParam("deviceType") String deviceType,
                                      @RequestParam("fileName") String fileName,
                                      @RequestParam("path") String path) throws Exception {
        String[] name=fileName.split("\\.");
        String[] type=name[0].split("-");
        int length=type.length;
        if(!(deviceType.equals(type[length-1]))){
            return new Result().failure(-1,"请选择第"+deviceType+"号模板");
        }
        Date now=new Date();
        return tabDeviceService.saveTabProjectDatum(deviceNo,deviceType,fileName, path,now);
    }

    @GetMapping("getWarnSettingList")
    @ApiOperation(value = "根据设备获取当前设备监测阈值", notes = "根据设备获取当前设备监测阈值")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "sensorNo", value = "设备编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "type", value = "设备类型", defaultValue = "", required = true, paramType = "query")
    })
    public Result getWarnSettingList(String sensorNo, int type) {
        return tabDeviceService.getWarnSettingList(sensorNo, type);
    }

    @PostMapping("updateWarnSetting")
    @ApiOperation(value = "修改当前设备监测阈值", notes = "修改当前设备监测阈值")
    public Result updateWarnSetting(@RequestBody List<TabWarnSettingVo> tabWarnSettingVo) {
        return tabDeviceService.updateWarnSetting(tabWarnSettingVo);
    }

    @PostMapping("insertWarnSetting")
    @ApiOperation(value = "根据设备设置地裂缝监测阈值", notes = "根据设备设置地裂缝监测阈值")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "sensorNo", value = "设备编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Array", name = "warnDlfSettingList", value = "阈值集合", defaultValue = "", required = true, paramType = "query")
    })
    public Result insertWarnSetting(@RequestBody WarnDlfSettingVo warnDlfSettingVo) {
        return tabDeviceService.insertWarnSetting(warnDlfSettingVo.getWarnDlfSettingList(), warnDlfSettingVo.getSensorNo());
    }

    @PostMapping("insertPqWarnSetting")
    @ApiOperation(value = "根据设备设置坡倾监测阈值", notes = "根据设备设置坡倾监测阈值")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "sensorNo", value = "设备编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Array", name = "warnPqSettingList", value = "阈值集合", defaultValue = "", required = true, paramType = "query")
    })
    public Result insertPqWarnSetting(@RequestBody WarnPqSettingVo warnPqSettingVo) {
        return tabDeviceService.insertPqWarnSetting(warnPqSettingVo.getWarnPqSettingList(), warnPqSettingVo.getSensorNo());
    }

    @PostMapping("insertQxWarnSetting")
    @ApiOperation(value = "根据设备设置倾斜监测阈值", notes = "根据设备设置坡倾斜测阈值")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "sensorNo", value = "设备编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Array", name = "warnQxSettingList", value = "阈值集合", defaultValue = "", required = true, paramType = "query")
    })
    public Result insertQxWarnSetting(@RequestBody WarnQxSettingVo warnQxSettingVo) {
        return tabDeviceService.insertQxWarnSetting(warnQxSettingVo.getWarnQxSettingList(), warnQxSettingVo.getSensorNo());
    }

    @PostMapping("insertGnssWarnSetting")
    @ApiOperation(value = "根据设备设置Gnss监测阈值", notes = "根据设备设置Gnss监测阈值")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "sensorNo", value = "设备编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Array", name = "warnGnssSettingList", value = "阈值集合", defaultValue = "", required = true, paramType = "query")
    })
    public Result insertGnssWarnSetting(@RequestBody WarnGnssSettingVo warnGnssSettingVo) {
        return tabDeviceService.insertGnssWarnSetting(warnGnssSettingVo.getWarnGnssSettingList(), warnGnssSettingVo.getSensorNo());
    }

    @PostMapping("insertYlWarnSetting")
    @ApiOperation(value = "根据设备设置雨量监测阈值", notes = "根据设备设置雨量监测阈值")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "sensorNo", value = "设备编号", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Array", name = "warnYlSettingList", value = "阈值集合", defaultValue = "", required = true, paramType = "query")
    })
    public Result insertYlWarnSetting(@RequestBody WarnYlSettingVo warnYlSettingVo) {
        return tabDeviceService.insertYlWarnSetting(warnYlSettingVo.getWarnYlSettingList(), warnYlSettingVo.getSensorNo());
    }

}