package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.entity.data.TabStaticType;
import com.cqndt.disaster.device.service.data.TabStaticTypeService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabStaticTypeVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
* Description: 
* Date: 2019-06-04
*/
@RestController
@RequestMapping("tabStaticType")
@Api(description = "字典管理" ,value = "字典管理")
public class TabStaticTypeController {
    @Autowired
    private TabStaticTypeService tabStaticTypeService;

    @SysLog("查询数据字典")
//    @RequiresPermissions("sys:tabstatictype:list")
    @PostMapping("list")
    @ApiOperation(value = "静态值列表", notes = "静态值列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Long", name = "staticNum", value = "静态值类型值", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "staticName", value = "静态值名称", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "staticType", value = "静态值类型名称", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "每页条数", defaultValue = "", required = true, paramType = "query")
    })
    public Result listTabUser(@RequestBody TabStaticTypeVo tabStaticTypeVo) {
        Result result = new Result();
        PageHelper.startPage(tabStaticTypeVo.getPage(),tabStaticTypeVo.getLimit());
        List<TabStaticTypeVo> list = tabStaticTypeService.list(tabStaticTypeVo);
        PageInfo<TabStaticTypeVo> pageInfo = new PageInfo<>(list);
        result.setMsg("操作成功");
        result.setData(pageInfo.getList());
        result.setCount((int)pageInfo.getTotal());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabStaticTypeById")
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Long", name = "id", value = "id", defaultValue = "",  paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "每页条数", defaultValue = "", required = true, paramType = "query")
    })
    public Result getTabStaticTypeById(String id){
        if(null == id){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        TabStaticTypeVo  tabStaticTypeVo = tabStaticTypeService.getTabStaticTypeById(id);
        return new Result().success(tabStaticTypeVo);
    }

    /**
     * 保存
     */
    @SysLog("保存")
    @PostMapping("save")
    @ApiOperation(value = "新增", notes = "新增")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "staticName", value = "静态值名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Long", name = "staticNum", value = "静态值类型值", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "staticType", value = "静态值类型名称", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "remark", value = "说明", defaultValue = "", required = false, paramType = "query")
    })
    //@RequiresPermissions("sys:tabstatictype:save")
    public Result save(@RequestBody TabStaticTypeVo tabStaticTypeVo){
        Result result = new Result();
        try {
            if(StringUtils.isEmpty(tabStaticTypeVo.getStaticName())){
                return result.failure(-1,"静态值名称不能为空");
            }else if(StringUtils.isEmpty(tabStaticTypeVo.getStaticType())){
                return result.failure(-1,"静态值类型名称不能为空");
            }else if(null == tabStaticTypeVo.getStaticNum()){
                return result.failure(-1,"静态值类型值不能为空");
            }
            List<TabStaticType> list = tabStaticTypeService.getByStaticType(tabStaticTypeVo.getStaticNum());
            if( list.size() != 0 ){
                //静态值类型值不需新增，将静态值类型值名称替换为数据库中名称
                tabStaticTypeVo.setStaticType(list.get(0).getStaticType());
                tabStaticTypeVo.setStaticKeyVal(list.get(0).getStaticKeyVal()+1);
            }else {
                //静态值类型值需要新增
                tabStaticTypeVo.setStaticKeyVal(1L);
            }
            tabStaticTypeService.save(tabStaticTypeVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("update")
    @ApiOperation(value = "修改", notes = "修改")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Long", name = "id", value = "静态值id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "staticName", value = "静态值名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "staticType", value = "静态值类型名称", defaultValue = "", required = false, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "remark", value = "说明", defaultValue = "", required = false, paramType = "query")
    })
    //@RequiresPermissions("sys:tabstatictype:update")
    public Result update(@RequestBody TabStaticTypeVo tabStaticTypeVo){
        Result result = new Result();
        try {
            if(null == tabStaticTypeVo.getId()){
                return result.failure(-1,"请选择至少一条数据进行操作");
            }else if( StringUtils.isEmpty(tabStaticTypeVo.getStaticName()) || StringUtils.isEmpty(tabStaticTypeVo.getStaticType())){
                return result.failure(-1,"请修改数据");
            }
            List<TabStaticType> list = tabStaticTypeService.getByStaticType(tabStaticTypeVo.getStaticNum());
            tabStaticTypeVo.setStaticKeyVal(list.get(0).getStaticKeyVal());
            tabStaticTypeService.update(tabStaticTypeVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
    @ApiOperation(value = "根据id删除数据", notes = "根据id删除数据")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "id", value = "静态值id", defaultValue = "", required = true, paramType = "query")
    })
    @RequiresPermissions("sys:tabstatictype:delete")
    public Result delete(String id){
        Result result = new Result();
        try {
            if (null == id) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabStaticTypeService.delete(id);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
       return result;
    }

    @PostMapping("getStaticByNum")
    @ApiOperation(value = "根据静态值类型值获取相应的数据", notes = "根据静态值类型值获取相应的数据")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "staticNum", value = "静态值类型值", defaultValue = "", required = true, paramType = "query")
    })
    public Result getStaticByNum(Integer staticNum){
        Result result = new Result();
        try {
            List<TabStaticType> lists = tabStaticTypeService.getStaticByNum(staticNum);
            result.setData(lists);
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"获取失败");
        }
        return result;
    }

}