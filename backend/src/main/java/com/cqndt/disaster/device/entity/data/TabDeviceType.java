package com.cqndt.disaster.device.entity.data;

import lombok.Data;

import java.util.Date;

/**
 * @Time : 2019-09-14 17:49
 **/
@Data
public class TabDeviceType {
    /**
     * 主键自增长
     */
    private Integer id;

    /**
     * 设备类型
     */
    private Integer deviceType;

    /**
     * 监测点编号ID
     */
    private String monitorNo;

    /**
     * 设备类型
     */
    private String type;

    /**
     * 刻度
     */
    private String scale;

    /**
     * 添加时间
     */
    private Date addTime;
}
