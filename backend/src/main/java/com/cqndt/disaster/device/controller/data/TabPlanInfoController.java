package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.service.data.TabPlanInfoService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabPlanInfoVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* Description: 表备注
* Date: 2019-06-11
*/
@RestController
@RequestMapping("tabPlanInfo")
@Api(description = "防灾预案",value = "防灾预案")
public class TabPlanInfoController {
    @Autowired
    private TabPlanInfoService tabPlanInfoService;

    @PostMapping("list")
    public Result listTabUser(@RequestBody TabPlanInfoVo tabPlanInfoVo) {
        PageHelper.startPage(tabPlanInfoVo.getPage(),tabPlanInfoVo.getLimit());
        PageInfo<TabPlanInfoVo> pageInfo = new PageInfo<TabPlanInfoVo>(tabPlanInfoService.list(tabPlanInfoVo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
    /**
     * 信息
     */
    @PostMapping("getTabPlanInfoById")
    public Result getTabPlanInfoById(@RequestBody TabPlanInfoVo tabPlanInfoVo){
        if(null == tabPlanInfoVo.getpNo()){
            return new Result().failure(-1,"请选择一条数据进行操作");
        }
        return new Result().success(tabPlanInfoService.getTabPlanInfoById(Long.parseLong(tabPlanInfoVo.getpNo())));
    }

    /**
     * 保存
     */
    @SysLog("增加表备注")
    @PostMapping("save")
    @RequiresPermissions("sys:tabplaninfo:save")
    public Result save(@RequestBody TabPlanInfoVo tabPlanInfoVo){
        Result result = new Result();
        try {
            tabPlanInfoService.save(tabPlanInfoVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改表备注")
    @PostMapping("update")
    @RequiresPermissions("sys:tabplaninfo:update")
    public Result update(@RequestBody TabPlanInfoVo tabPlanInfoVo){
        Result result = new Result();
        try {
            if(null == tabPlanInfoVo.getId()){
                tabPlanInfoService.save(tabPlanInfoVo);
            }
            tabPlanInfoService.update(tabPlanInfoVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除表备注")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabplaninfo:delete")
    public Result delete(@RequestBody TabPlanInfoVo tabPlanInfoVo){
        Result result = new Result();
        try {
            if (null == tabPlanInfoVo.getId()) {
                return result.failure(-1,"请选择至少一条数据进行操作");
            }
            tabPlanInfoService.delete(String.valueOf(tabPlanInfoVo.getId()));
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1,"操作失败");
        }
       return result;
    }


}