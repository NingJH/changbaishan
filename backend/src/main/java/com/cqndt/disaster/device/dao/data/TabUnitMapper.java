package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabUnit;
import com.cqndt.disaster.device.vo.data.TabUnitVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface TabUnitMapper {
    /**
     * 得到单位名称
     *
     * @return
     */
    List<TabUnit> getTabUnitName();

    /**
     * 查询列表
     */
    List<Map<String, Object>> queryList(TabUnitVo vo);

    /**
     * 查询单位类别、厂商类别
     */
    List<Map<String, String>> getVendorType();

    /**
     * 新增单位
     */
    int addUnit(TabUnitVo vo);

    /**
     * 删除单位
     */
    int deleteUnitById(Integer id);

    /**
     * 修改单位
     */
    int updateUnitById(TabUnitVo vo);
    /**
     * 根据id获取详情
     */
    List<TabUnitVo> getTabUnitById(Long id);

    /**
     * 给单位地址赋值
     */
    int updateAddress(@Param("unitAddress") String address,@Param("id") String id);


    /**
     * 根据单位类别id查询所属单位
     */
    List<TabUnitVo> getTabUnitByVendorId(int vendorId);

    /**
     * 根据项目id获取单位
     * @param vo
     * @return
     */
    List<TabUnitVo> getTabUnitByProject(TabUnitVo vo);
}
