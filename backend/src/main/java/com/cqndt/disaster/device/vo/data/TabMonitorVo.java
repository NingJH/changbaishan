package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabMonitor;
import lombok.Data;

/**
* Description: 
* Date: 2019-06-14
*/
@Data
public class TabMonitorVo extends TabMonitor {
    /**
     * 页数
     */
    private Integer page=1;
    /**
     * 条数
     */
    private Integer limit=10;

    private String projectName;

    private Integer project;

    private String areaCode;

    private String lonLat;

    private String userId;
}
