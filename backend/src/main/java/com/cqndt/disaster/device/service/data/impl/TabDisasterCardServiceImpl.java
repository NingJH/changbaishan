package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabDisasterCardMapper;
import com.cqndt.disaster.device.service.data.TabDisasterCardService;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.vo.data.TabDisasterCardVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
public class TabDisasterCardServiceImpl implements TabDisasterCardService {
    @Autowired
    private TabDisasterCardMapper tabDisasterCardMapper;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void save(TabDisasterCardVo tabDisasterCardVo) throws Exception {
        tabDisasterCardMapper.save(tabDisasterCardVo);

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void update(TabDisasterCardVo tabDisasterCardVo) throws Exception {
        TabDisasterCardVo vo = (TabDisasterCardVo) tabDisasterCardMapper.getTabDisasterCardById(String.valueOf(tabDisasterCardVo.getId()));
        AttachmentVo attachmentVo = new AttachmentVo();
        tabDisasterCardMapper.update(tabDisasterCardVo);

    }

    @Override
    public Map<String,Object> getTabDisasterCardById(String disNo) {
        return tabDisasterCardMapper.getTabDisasterCardById(disNo);
    }

    @Override
    public void delete(String id) {
        String[] ids = id.split(",");
        for (String idStr : ids) {
            tabDisasterCardMapper.delete(Integer.parseInt(idStr));
        }
    }

    @Override
    public List<TabDisasterCardVo> list(TabDisasterCardVo tabDisasterCardVo) {
        return tabDisasterCardMapper.list(tabDisasterCardVo);
    }


}