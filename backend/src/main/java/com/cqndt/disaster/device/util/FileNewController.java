package com.cqndt.disaster.device.util;

import com.cqndt.disaster.device.entity.data.TabAttachment;
import com.cqndt.disaster.device.service.data.TabAttachmentService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/2/14  9:51
 * Description:附件上传、附件预览
 */
@Controller
public class FileNewController {
    private static Logger logger = LoggerFactory.getLogger(FileNewController.class);
    @Autowired
    private TabAttachmentService tabAttachmentService;
    //文件路径
    @Value("${attachmentUrlSet}")
    private String attachmentUrlSet;
    //openOffice安装路径
    @Value("${open-office-path}")
    private String openOfficePath;
    //openOffice host
    @Value("${open-office-host}")
    private String openOfficeHost;
    @Value("${file-ip}")
    private String fileIp;

    /**
     * 文件上传
     * @param file 上传的文件
     * @param attachmentType 文件类型（1.图片 2.视频 3.文件）
     * @param modelName 模块名称
     * @param modelName 文件别名
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "fileUpload", method = RequestMethod.POST)
    @ApiOperation(value = "附件上传", notes = "附件上传")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "MultipartFile", name = "file", value = "上传附件", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "attachmentType", value = "附件类型（1.图片 2.视频 3.文件 4.）", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "modelName", value = "模块名称", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "newName", value = "附件别名", defaultValue = "1", required = true, paramType = "query")
    })
    public Result fileUpload(MultipartFile file, String attachmentType,String modelName,String newName) {
        Result result = FileUpload.fileUpload(file,attachmentType,modelName,attachmentUrlSet,openOfficePath,openOfficeHost,fileIp,newName);
        Map<String,Object> map = ( Map<String,Object>)result.getData();
        TabAttachment tabAttachment = new TabAttachment();
        tabAttachment.setId(map.get("id").toString());
        tabAttachment.setAttachmentUrl(map.get("attachmentUrl").toString());
        tabAttachment.setAttachmentName(map.get("attachmentName").toString());
        tabAttachment.setAttachmentType(attachmentType);
        tabAttachmentService.saveTabAttachment(tabAttachment);
        return result;
    }

    /**
     * 全景图上传
     * @param file 上传的文件
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "fileUploadTwoRound", method = RequestMethod.POST)
    public Result fileUploadTwoRound(MultipartFile file,String newName) {
        Result result = FileUpload.fileUploadTwoRound(file,attachmentUrlSet,fileIp,newName);
        return result;
    }


    /**
     * 预览文件
     * @param attachmentUrl 文件路径
     * @param response
     */
    @RequestMapping(value = "readByIo", method = RequestMethod.GET)
    public void readByIo(String attachmentType,String attachmentUrl,HttpServletResponse response) {
        try {
            FileView.readByIo(attachmentType,attachmentUrl,response,0,attachmentUrlSet,openOfficePath,openOfficeHost);
        }catch (Exception e){
            logger.error("文件预览失败");
            e.printStackTrace();
        }
    }
    /**
     * 文件下载
     * @param attachmentUrl 文件路径
     * @param response
     */
    @RequestMapping(value = "fileDownLoad", method = RequestMethod.GET)
    public void fileDownLoad(String attachmentType,String attachmentUrl,HttpServletResponse response) {
        try {
            FileView.readByIo(attachmentType,attachmentUrl,response,1,attachmentUrlSet,openOfficePath,openOfficeHost);
        }catch (Exception e){
            logger.error("文件下载失败");
            e.printStackTrace();
        }
    }

    @ResponseBody
    @PostMapping("uploadFile")
    @ApiOperation(value = "附件上传", notes = "附件上传")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "MultipartFile", name = "file", value = "上传附件", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "attachmentType", value = "附件类型（1.图片 2.视频 3.文件 4.）", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "modelName", value = "模块名称", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "newName", value = "附件别名", defaultValue = "1", required = true, paramType = "query")
    })
    public Result uploadFile(MultipartFile file, String attachmentType,String modelName,String newName) {
        Result result = UploadFile.uploadFile(file,attachmentType,modelName,attachmentUrlSet,openOfficePath,openOfficeHost,fileIp,newName);
        Map<String,Object> map = ( Map<String,Object>)result.getData();
        TabAttachment tabAttachment = new TabAttachment();
        tabAttachment.setId(map.get("id").toString());
        tabAttachment.setAttachmentUrl(map.get("attachmentUrl").toString());
        tabAttachment.setAttachmentName(map.get("attachmentName").toString());
        tabAttachment.setAttachmentType(attachmentType);
        tabAttachmentService.saveTabAttachment(tabAttachment);
        return result;
    }
}
