package com.cqndt.disaster.device.service.data.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.cqndt.disaster.device.dao.data.*;
import com.cqndt.disaster.device.entity.data.*;
import com.cqndt.disaster.device.service.data.TabDeviceService;
import com.cqndt.disaster.device.sys.shiro.ShiroUtils;
import com.cqndt.disaster.device.util.ImportExcelUtil;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.util.ExportExcelUtil;
import com.cqndt.disaster.device.vo.data.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import java.util.*;

@Service
public class TabDeviceServiceImpl implements TabDeviceService {

    @Resource
    private TabDeviceMapper tabDeviceMapper;

    @Resource
    private TabSensorMapper sensorMapper;

    @Resource
    private TabWarnSettingMapper warnSettingMapper;

    @Resource
    private TabDeviceInstallMapper tabDeviceInstallMapper;

    @Resource
    private TabStaticTypeMapper tabStaticTypeMapper;

    @Resource
    private WarnDlfSettingMapper warnDlfSettingMapper;

    @Resource
    private WarnPqSettingMapper warnPqSettingMapper;

    @Resource
    private WarnGnssSettingMapper warnGnssSettingMapper;

//    @Resource
//    private TabWarnSettingMapper warnSettingMapper;

    @Resource
    private WarnQxSettingMapper warnQxSettingMapper;

    @Resource
    private WarnYlSettingMapper warnYlSettingMapper;

    @Resource
    private TabDeviceTypeMapper tabDeviceTypeMapper;

    @Value("${file-ip}")
    private String fileIp;

    @Value("${attachmentUrlSet}")
    private String attachmentUrlSet;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Result save(TabDeviceVo tabDeviceVo) throws Exception {
        if(tabDeviceVo == null){
            return new Result().failure(-1, "未接收到对象");
        }

        String deviceTypeName = tabStaticTypeMapper.getDeviceTypeName(tabDeviceVo.getDeviceType());
        tabDeviceVo.setDeviceTypeName(deviceTypeName);
        if("15".equals(tabDeviceVo.getDeviceType())){
            TabDeviceShow tabDeviceShow=new TabDeviceShow();
            tabDeviceShow.setDeviceNo(tabDeviceVo.getDeviceNo());
            tabDeviceShow.setShowType(tabDeviceVo.getShowType());
            tabDeviceShow.setOriginalValue(tabDeviceVo.getOriginalValue());
            tabDeviceMapper.saveShowType(tabDeviceShow);
        }
        tabDeviceMapper.save(tabDeviceVo);

        // 添加传感器
        if (StringUtils.isNotEmpty(tabDeviceVo.getSensor())) {
            JSONArray sensorArry = JSONArray.fromObject(tabDeviceVo.getSensor());
            if (null != sensorArry && sensorArry.size() > 0) {
                for (int i = 0; i < sensorArry.size(); i++) {
                    JSONObject sensorObj = sensorArry.getJSONObject(i);
                    if (null != sensorObj.get("sensorNo")) {
                        TabSensorVo sensorVo = new TabSensorVo();
                        sensorVo.setSensorNo(sensorObj.getString("sensorNo"));
                        sensorVo.setSensorName(sensorObj.getString("sensorName"));
                        sensorVo.setSensorType(sensorObj.getString("sensorType"));
                        sensorVo.setDeviceNo(sensorObj.getString("deviceNo"));
                        sensorMapper.save(sensorVo);
                    }
                    // 最初设置阈值的代码，后面阈值设置变更，不在这里设置  2019-08-31
//                    if (null != sensorObj.getJSONObject("yuzhi")) {
//                        JSONObject warnSetting = sensorObj.getJSONObject("yuzhi");
//                        if (StringUtils.isNotEmpty(sensorObj.get("sensorNo").toString())) {
//                            TabWarnSettingVo warnSettingVo = (TabWarnSettingVo) JSONObject.toBean(warnSetting, TabWarnSettingVo.class);
//                            String sensorNo = sensorObj.getString("sensorNo");
//                            warnSettingVo.setSensorNo(sensorNo);
//                            warnSettingMapper.save(warnSettingVo);
//                        }
//                    }
                }
            }
        }
        // 添加安装信息
        if (StringUtils.isNotEmpty(tabDeviceVo.getDeviceInstallInfo())) {
            JSONObject deviceInstallInfo = JSONObject.fromObject(tabDeviceVo.getDeviceInstallInfo());
            TabDeviceInstallVo tabDeviceInstallVo = (TabDeviceInstallVo) JSONObject.toBean(deviceInstallInfo, TabDeviceInstallVo.class);
            if (StringUtils.isNotEmpty(tabDeviceVo.getDeviceNo())) {
                tabDeviceInstallVo.setDeviceNo(tabDeviceVo.getDeviceNo());
            }
            if (null != tabDeviceInstallVo.getImgId() && tabDeviceInstallVo.getImgId().length > 0) {
                tabDeviceInstallVo.setImgIds(tabDeviceInstallVo.getImgId()[1]);
            }
            if(null != tabDeviceInstallVo.getVideoId() && tabDeviceInstallVo.getVideoId().length > 0){
                tabDeviceInstallVo.setVideoIds(tabDeviceInstallVo.getVideoId()[1]);
            }
            tabDeviceInstallMapper.save(tabDeviceInstallVo);
        }
        //添加默认刻度值
        List<String> deviceTypeNameList = tabDeviceTypeMapper.selectTypeName(tabDeviceVo.getDeviceType());
        String monitorNo = tabDeviceTypeMapper.selectMonitorNo(tabDeviceVo.getDeviceNo());
        for(String typeName : deviceTypeNameList){
            TabDeviceType tabDeviceType = new TabDeviceType();
            tabDeviceType.setDeviceType(Integer.parseInt(tabDeviceVo.getDeviceType()));
            tabDeviceType.setType(typeName);
            tabDeviceType.setAddTime(new Date());
            tabDeviceType.setMonitorNo(monitorNo);
            tabDeviceType.setScale(tabDeviceVo.getDeviceScale());
            tabDeviceTypeMapper.insertScale(tabDeviceType);
        }
        return new Result().success("添加成功！");
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Result update(TabDeviceVo tabDeviceVo) throws Exception {
        if(tabDeviceVo == null){
            return new Result().failure(-1, "未接收到对象");
        }
        String deviceTypeName = tabStaticTypeMapper.getDeviceTypeName(tabDeviceVo.getDeviceType());
        tabDeviceVo.setDeviceTypeName(deviceTypeName);
        tabDeviceMapper.update(tabDeviceVo);

        TabDeviceShow tabDeviceShow=new TabDeviceShow();
        tabDeviceShow.setShowType(tabDeviceVo.getShowType());
        tabDeviceShow.setOriginalValue(tabDeviceVo.getOriginalValue());
        tabDeviceShow.setDeviceNo(tabDeviceVo.getDeviceNo());
//        int j = tabDeviceMapper.updateDeviceShow(tabDeviceShow);
//        System.out.println(j);

        // 删除所有传感器，重新添加传感器
        sensorMapper.deleteBySensorNo(tabDeviceVo.getDeviceNo());
        if (StringUtils.isNotEmpty(tabDeviceVo.getSensor())) {
            JSONArray sensorArry = JSONArray.fromObject(tabDeviceVo.getSensor());
            if (null != sensorArry && sensorArry.size() > 0) {
                for (int i = 0; i < sensorArry.size(); i++) {
                    JSONObject sensorObj = sensorArry.getJSONObject(i);
                    if (null != sensorObj.get("sensorNo")) {
                        TabSensorVo sensorVo = new TabSensorVo();
                        sensorVo.setSensorNo(sensorObj.get("sensorNo").toString());
                        sensorVo.setSensorName(sensorObj.get("sensorName").toString());
                        sensorVo.setSensorType(sensorObj.get("sensorType").toString());
                        sensorVo.setDeviceNo(sensorObj.get("deviceNo").toString());
                        sensorMapper.save(sensorVo);
                    }
                    // 最初设置阈值的代码，后面阈值设置变更，不在这里设置  2019-08-31
//                    if (0 != sensorObj.getJSONObject("yuzhi").size()) {
//                        JSONObject warnSetting = sensorObj.getJSONObject("yuzhi");
//                        if (StringUtils.isNotEmpty(sensorObj.get("sensorNo").toString())) {
//                            TabWarnSettingVo warnSettingVo = (TabWarnSettingVo) JSONObject.toBean(warnSetting, TabWarnSettingVo.class);
//                            warnSettingVo.setSensorNo(sensorObj.get("sensorNo").toString());
//                            if (null != warnSetting.getString("yuzhiId") && !"".equals(warnSetting.getString("yuzhiId"))) {
//                                warnSettingVo.setId(warnSetting.getLong("yuzhiId"));
//                                warnSettingMapper.update(warnSettingVo);
//                            } else {
//                                warnSettingMapper.save(warnSettingVo);
//                            }
//                        }
//                    }
                }
            }
        }
        if (StringUtils.isNotEmpty(tabDeviceVo.getDeviceInstallInfo())) {
            System.out.println("传参"+tabDeviceVo.getDeviceInstallInfo());
            JSONObject deviceInstallInfo = JSONObject.fromObject(tabDeviceVo.getDeviceInstallInfo());
//            TabDeviceInstallVo tabDeviceInstallVo = (TabDeviceInstallVo) JSONObject.toBean(deviceInstallInfo, TabDeviceInstallVo.class);
            ObjectMapper mapper = new ObjectMapper();
            TabDeviceInstallVo tabDeviceInstallVo = mapper.readValue(deviceInstallInfo.toString(),TabDeviceInstallVo.class);
            if (StringUtils.isNotEmpty(tabDeviceVo.getDeviceNo())) {
                tabDeviceInstallVo.setDeviceNo(tabDeviceVo.getDeviceNo());
            }
            if (null != tabDeviceInstallVo.getImgId() && tabDeviceInstallVo.getImgId().length > 0) {
                tabDeviceInstallVo.setImgIds(tabDeviceInstallVo.getImgId()[1]);
            }
            if (null != tabDeviceInstallVo.getVideoId() && tabDeviceInstallVo.getVideoId().length > 0) {
                tabDeviceInstallVo.setVideoIds(tabDeviceInstallVo.getVideoId()[1]);
            }
            if (null != deviceInstallInfo.getString("deviceInstallInfoId") && !"".equals(deviceInstallInfo.getString("deviceInstallInfoId"))) {
                tabDeviceInstallVo.setId(deviceInstallInfo.getLong("deviceInstallInfoId"));
                tabDeviceInstallMapper.update(tabDeviceInstallVo);
            } else {
                tabDeviceInstallMapper.save(tabDeviceInstallVo);
            }
        }
        return new Result().success("修改成功");
    }

    @Override
    public TabDeviceVo getTabDeviceById(Long id) {
        return tabDeviceMapper.getTabDeviceById(id);
    }

    @Override
    public void delete(String id) {
        String[] ids = id.split(",");
        for (String idStr : ids) {
            TabDeviceVo tabDeviceVo = tabDeviceMapper.getTabDeviceById(Long.valueOf(idStr));
            tabDeviceInstallMapper.deleteByDeviceNo(tabDeviceVo.getDeviceNo());
            sensorMapper.deleteBySensorNo(tabDeviceVo.getDeviceNo());
            tabDeviceMapper.delete(Integer.parseInt(idStr));
        }
    }

    @Override
    public List<TabDeviceVo> list(TabDeviceVo tabDeviceVo) {
//        List<TabDeviceVo> list=null;
//        list = tabDeviceMapper.listAll(ShiroUtils.getUserId());
//        return list;
        List<TabDeviceVo> list = tabDeviceMapper.list(tabDeviceVo);

        if(list.isEmpty()){
            return list;
        }

        // 设备编号集合
        List<String> deviceNoList = new ArrayList<>();

        for(TabDeviceVo tabDeviceVo1 : list){
            deviceNoList.add(tabDeviceVo1.getDeviceNo());
        }

        List<TabDeviceInstallVo> tabDeviceInstallVoList = tabDeviceInstallMapper.getTabDeviceInstallByDeviceNoList(deviceNoList);

        // 附件编号集合
        List<String> imgIds = new ArrayList<>();

        //设备视频编号集合
        List<String> videoIds = new ArrayList<>();

        // 遍历获取附件编号
        for(TabDeviceInstallVo tabDeviceInstallVo : tabDeviceInstallVoList){
            if(StringUtils.isEmpty(tabDeviceInstallVo.getImgIds())){
                continue;
            }
            String[] arr = tabDeviceInstallVo.getImgIds().split(",");
            imgIds.addAll(Arrays.asList(arr));
        }
        // 遍历获取设备视频编号
        for (TabDeviceInstallVo tabDeviceInstallVo : tabDeviceInstallVoList) {
            if (StringUtils.isEmpty(tabDeviceInstallVo.getVideoIds())) {
                continue;
            }
            String[] arr1 =tabDeviceInstallVo.getVideoIds().split(",");
            videoIds.addAll(Arrays.asList(arr1));
        }

        if(imgIds.isEmpty()){
            imgIds.add("");
        }

        if (videoIds.isEmpty()) {
            videoIds.add("");
        }

        // 附件信息集合
        List<Map<String, Object>> imgObjList = tabDeviceInstallMapper.getImgUrlAndByIdList(imgIds);

        List<Map<String, Object>> videoObjList = tabDeviceInstallMapper.getvideoUrlAndByIdList(videoIds);

        for(TabDeviceInstallVo tabDeviceInstallVo : tabDeviceInstallVoList){
            if(StringUtils.isEmpty(tabDeviceInstallVo.getImgIds())){
                continue;
            }
            String[] arr = tabDeviceInstallVo.getImgIds().split(",");

            // 当前安装信息临时图片集合
            List<Map<String, Object>> tempImgList = new ArrayList<>();
            for(String imgId : arr){
                for(Map<String, Object> imgMap : imgObjList){
                    if(!imgId.equals(imgMap.get("id").toString())){
                        continue;
                    }
                    imgMap.put("viewUrl", fileIp + File.separator+"img"+File.separator +imgMap.get("attachmentUrl").toString());
                    tempImgList.add(imgMap);
                }
            }
            if(!tempImgList.isEmpty()){
                tabDeviceInstallVo.setFilesInfo(tempImgList);
            }
        }

        for (TabDeviceInstallVo tabDeviceInstallVo : tabDeviceInstallVoList) {
            if (StringUtils.isEmpty(tabDeviceInstallVo.getVideoIds())) {
                continue;
            }

            String [] arr1 =tabDeviceInstallVo.getVideoIds().split(",");

            // 当前安装信息临时视频集合
            List<Map<String, Object>> tempvideoList = new ArrayList<>();

            for (String videoId : arr1) {
                for (Map<String, Object> videoMap : videoObjList) {
                    if (!videoId.equals(videoMap.get("id").toString())) {
                        continue;
                    }
                    videoMap.put("videoUrl", fileIp + File.separator + "video" + File.separator + videoMap.get("attachmentUrl").toString());
                    tempvideoList.add(videoMap);
                }
            }

            if (!tempvideoList.isEmpty()) {
                tabDeviceInstallVo.setVideoInfo(tempvideoList);
            }
        }

        // 获取设备传感器信息
        List<TabSensorVo> sensorVos = sensorMapper.getTabDeviceInstallByDeviceNoList(deviceNoList);

        List<String> sensorNos = new ArrayList<>();
        for(TabSensorVo tabSensorVo : sensorVos){
            sensorNos.add(tabSensorVo.getSensorNo());
        }

        if(sensorNos.isEmpty()){
            sensorNos.add("");
        }

        // 传感器阈值对象
        List<TabWarnSettingVo> tabWarnSettingVos = warnSettingMapper.getTabDeviceInstallBySensorNoList(sensorNos);

        // 组装返回格式
        for(TabDeviceVo tabDeviceVo1 : list){
            // 安装信息的组装
            for(TabDeviceInstallVo tabDeviceInstallVo : tabDeviceInstallVoList){
                if(!tabDeviceVo1.getDeviceNo().equals(tabDeviceInstallVo.getDeviceNo())){
                    continue;
                }
                tabDeviceVo1.setDeviceInstallInfo(JSON.toJSONStringWithDateFormat(tabDeviceInstallVo, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
                break;
            }

            // 传感器信息的组装
            List<TabSensorVo> tempSensorVoList = new ArrayList<>();
            // 阈值信息组装
            List<TabWarnSettingVo> warnSettingList = new ArrayList<>();
            for(TabSensorVo tabSensorVo : sensorVos){
                if(!tabDeviceVo1.getDeviceNo().equals(tabSensorVo.getDeviceNo())){
                    continue;
                }
                tempSensorVoList.add(tabSensorVo);

                for(TabWarnSettingVo settingVo : tabWarnSettingVos){
                    if(!settingVo.getSensorNo().equals(tabSensorVo.getSensorNo())){
                        continue;
                    }
                    warnSettingList.add(settingVo);
                }
            }
            tabDeviceVo1.setSensorVos(tempSensorVoList);
            tabDeviceVo1.setYuzhi(warnSettingList);
        }
        return list;
    }

    @Override
    public Result saveTabProjectDatum(String deviceNo,  String deviceType, String fileName,String path,Date now) throws Exception {
        String excelPath=attachmentUrlSet+File.separator+"file"+File.separator+ path;
        InputStream in=new FileInputStream(excelPath);
        List<List<Object>> list= ImportExcelUtil.getBankListByExcel(in,fileName);
        Integer row;
        if("2".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object qxjd=innerList.get(0);
                Object temp=innerList.get(1);
                row=tabDeviceMapper.save2(deviceNo,now,qxjd,temp);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("3".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object wyl=innerList.get(0);
                Object x=innerList.get(1);
                Object y=innerList.get(2);
                Object z=innerList.get(3);
                Object temp=innerList.get(4);
                row=tabDeviceMapper.save3(deviceNo,now,wyl,x,y,z,temp);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("4".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object x=innerList.get(0);      Object y=innerList.get(1);
                Object z=innerList.get(2);      Object x1=innerList.get(3);
                Object y1=innerList.get(4);     Object z1=innerList.get(5);
                Object angel=innerList.get(6);  Object swy=innerList.get(7);
                Object cwy=innerList.get(8);    Object swysd=innerList.get(9);
                Object cwysd=innerList.get(10); Object swyjs=innerList.get(11);
                Object cwyjs=innerList.get(12);
                row=tabDeviceMapper.save4(deviceNo,now,x,y,z,x1,y1,z1,angel,swy,cwy,swysd,cwysd,swyjs,cwyjs);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("6".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object x=innerList.get(0);Object y=innerList.get(1);
                Object z=innerList.get(2);Object temp=innerList.get(3);
                row=tabDeviceMapper.save6(deviceNo,now,x,y,z,temp);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("7".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save7(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("8".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save8(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("9".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save9(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("10".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save10(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("11".equals(deviceType)||"39".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object hsl=innerList.get(0);
                Object deep=innerList.get(1);
                row=tabDeviceMapper.save11(deviceNo,now,hsl,deep);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("12".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save12(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("13".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save13(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("14".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object humidity=innerList.get(0);
                Object temp=innerList.get(1);
                row=tabDeviceMapper.save14(deviceNo,now,humidity,temp);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("15".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save15(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("16".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object zf=innerList.get(0);
                Object pl=innerList.get(1);
                row=tabDeviceMapper.save16(deviceNo,now,zf,pl);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("18".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save18(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("19".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object x=innerList.get(0);Object y=innerList.get(1);
                Object h=innerList.get(2);Object x1=innerList.get(3);
                Object y1=innerList.get(4);Object h1=innerList.get(5);
                row=tabDeviceMapper.save19(deviceNo,now,x,y,h,x1,y1,h1);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("20".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object czgc=innerList.get(0);
                row=tabDeviceMapper.save20(deviceNo,now,czgc);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("21".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object one=innerList.get(0);
                Object two=innerList.get(1);
                Object three=innerList.get(2);
                row=tabDeviceMapper.save21(deviceNo,now,one,two,three);

                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("22".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save22(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("23".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object x=innerList.get(0);
                Object y=innerList.get(1);
                row=tabDeviceMapper.save23(deviceNo,now,x,y);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("24".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save24(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("25".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object x=innerList.get(0);
                Object y=innerList.get(1);
                Object z=innerList.get(2);
                row=tabDeviceMapper.save25(deviceNo,now,x,y,z);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("26".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save26(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("27".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object x=innerList.get(0);
                Object y=innerList.get(1);
                Object z=innerList.get(2);
                row=tabDeviceMapper.save27(deviceNo,now,x,y,z);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("28".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save28(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("30".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save30(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("31".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save31(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("32".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save32(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        if("33".equals(deviceType)){
            for (List<Object> innerList:list) {
                isNull(innerList);
                Object value=innerList.get(0);
                row=tabDeviceMapper.save33(deviceNo,now,value);
                if (row==null) {
                    return new Result().failure(-1,"添加失败");
                }
            }
        }
        return new Result().success("添加成功");
    }

    private void isNull(List<Object> innerList){
        for (int i=0;i<innerList.size();i++){
            if(null==innerList.get(i)||"".equals(innerList.get(i))){
                innerList.set(i,"0");
            }
        }
    }

    /**
     * 导出设备报表
     * @param tabDeviceVo
     * @param response
     */
    @Override
    public void exportDeviceExcel(TabDeviceVo tabDeviceVo, HttpServletResponse response) {
        String title="监测设备报表";
        String [] rowName={"编号","监测地址","设备名称","设备编号","设备类型","所属布置点","所属项目","状态"};
        List<TabDeviceVo> list = tabDeviceMapper.list(tabDeviceVo);
        List<Object[]> list1=new ArrayList<>();
        for (int i=0;i<list.size();i++){
            Object[] o=new Object[8];
            o[1]=list.get(i).getAddress();
            o[2]=list.get(i).getDeviceName();
            o[3]=list.get(i).getDeviceNo();
            o[4]=list.get(i).getDeviceTypeName();
            o[5]=list.get(i).getPosition();
            o[6]=list.get(i).getProjectName();
            if(list.get(i).getState()==null){
                o[7]="离线";
            }else if(list.get(i).getState()==1){
                o[7]="在线";
            }else if(list.get(i).getState()==2){
                o[7]="离线";
            }
            list1.add(o);
        }
        String filePath=System.getProperty("user.dir") + File.separator +title;
        ExportExcelUtil ex= new ExportExcelUtil(title,rowName,list1,filePath,response);
        try {
            ex.export();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public Result getAllDeviceName(TabMonitorPersonVo tabMonitorPersonVo) {
        return new Result().success();
    }

    @Override
    public Result getWarnSettingList(String sensorNo, int type) {

        if(StringUtils.isEmpty(sensorNo)){
            return new Result().failure(-1, "未传入对应的设备编号！");
        }

//        switch (type){
//            case 8:
//                // 坡倾仪阈值设置
//                List<WarnPqSetting> warnPqSettingList = warnPqSettingMapper.queryWarnPqSettingList(sensorNo);
//                return new Result().success(warnPqSettingList);
//            case 41:
//                // 倾斜
//                List<WarnQxSetting> warnQxSettingList = warnQxSettingMapper.queryWarnQxSettingList(sensorNo);
//                return new Result().success(warnQxSettingList);
//            case 4:
//                // gnss
//                List<WarnGnssSetting> warnGnssSettingList = warnGnssSettingMapper.queryWarnGnssSettingList(sensorNo);
//                return new Result().success(warnGnssSettingList);
//            case 10:
//                // 雨量
//                List<WarnYlSetting> warnYlSettingList = warnYlSettingMapper.queryWarnYlSettingList(sensorNo);
//                return new Result().success(warnYlSettingList);
//            default:
//                return new Result().failure(-1, "当前设备类型暂无阈值设置");
//        }
        List<Map<String,Object>> list = new ArrayList<>();
        switch (type){
            case 8:
                // 次声
                Map<String,Object> warnCishengAmplitude = warnSettingMapper.warnCishengAmplitude(sensorNo);
                warnCishengAmplitude.put("tag","振幅");
                Map<String,Object> warnCishengFrequency = warnSettingMapper.warnCishengFrequency(sensorNo);
                warnCishengFrequency.put("tag","频率");
                list.add(warnCishengAmplitude);
                list.add(warnCishengFrequency);
                return new Result().success(list);
            case 4:
                // gnss
                List<WarnGnssSetting> warnGnssSettingList = warnGnssSettingMapper.queryWarnGnssSettingList(sensorNo);
                return new Result().success(warnGnssSettingList);
            default:
                Map<String,Object> warnSettings = warnSettingMapper.queryWarnSettingList(sensorNo);
                list.add(warnSettings);
                return new Result().success(list);
        }
    }

    @Override
    public Result insertWarnSetting(List<WarnDlfSetting> warnDlfSettingList, String sensorNo) {

        if(StringUtils.isEmpty(sensorNo)){
            return new Result().failure(-1, "未传入设备编号");
        }

        if(warnDlfSettingList.isEmpty()){
            return new Result().failure(-1, "未传入待设置的阈值");
        }

        warnDlfSettingMapper.deleteWarnDlfSettingBySensorNo(sensorNo);

        for(WarnDlfSetting warnDlfSetting : warnDlfSettingList){
            warnDlfSettingMapper.insertWarnDlfSetting(warnDlfSetting);
        }

        return new Result().success();
    }

    @Override
    public Result insertPqWarnSetting(List<WarnPqSetting> warnPqSettingList, String sensorNo) {
        if(StringUtils.isEmpty(sensorNo)){
            return new Result().failure(-1, "未传入设备编号");
        }

        if(warnPqSettingList.isEmpty()){
            return new Result().failure(-1, "未传入待设置的阈值");
        }

        warnPqSettingMapper.deleteWarnPqSettingBySensorNo(sensorNo);

        for(WarnPqSetting warnPqSetting : warnPqSettingList){
            warnPqSettingMapper.insertWarnPqSetting(warnPqSetting);
        }

        return new Result().success();
    }

    @Override
    public Result insertQxWarnSetting(List<WarnQxSetting> warnQxSettingList, String sensorNo) {
        if(StringUtils.isEmpty(sensorNo)){
            return new Result().failure(-1, "未传入设备编号");
        }

        if(warnQxSettingList.isEmpty()){
            return new Result().failure(-1, "未传入待设置的阈值");
        }

        warnQxSettingMapper.deleteWarnQxSettingBySensorNo(sensorNo);

        for(WarnQxSetting warnQxSetting : warnQxSettingList){
            warnQxSettingMapper.insertWarnQxSetting(warnQxSetting);
        }

        return new Result().success();
    }

    @Override
    public Result insertGnssWarnSetting(List<WarnGnssSetting> warnGnssSettingList, String sensorNo) {
        if(StringUtils.isEmpty(sensorNo)){
            return new Result().failure(-1, "未传入设备编号");
        }

        if(warnGnssSettingList.isEmpty()){
            return new Result().failure(-1, "未传入待设置的阈值");
        }

        warnGnssSettingMapper.deleteWarnGnssSettingBySensorNo(sensorNo);

        for(WarnGnssSetting warnGnssSetting : warnGnssSettingList){
            warnGnssSettingMapper.insertWarnGnssSetting(warnGnssSetting);
        }

        return new Result().success();
    }

    @Override
    public Result insertYlWarnSetting(List<WarnYlSetting> warnYlSettingList, String sensorNo) {
        if(StringUtils.isEmpty(sensorNo)){
            return new Result().failure(-1, "未传入设备编号");
        }

        if(warnYlSettingList.isEmpty()){
            return new Result().failure(-1, "未传入待设置的阈值");
        }

        warnYlSettingMapper.deleteWarnYlSettingBySensorNo(sensorNo);

        for(WarnYlSetting warnYlSetting : warnYlSettingList){
            warnYlSettingMapper.insertWarnYlSetting(warnYlSetting);
        }

        return new Result().success();
    }

    @Override
    public Result updateWarnSetting(List<TabWarnSettingVo> tabWarnSettingVo) {
        tabWarnSettingVo.forEach(t->{
            warnSettingMapper.updateWarnSetting(t);
        });
        return new Result().success(1);
    }
}