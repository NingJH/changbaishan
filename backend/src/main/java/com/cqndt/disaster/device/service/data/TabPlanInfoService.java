package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.vo.data.TabPlanInfoVo;

import java.util.List;
import java.util.Map;

public interface TabPlanInfoService {
    /**
     * 增加
     * @param tabPlanInfoVo
     */
    void save(TabPlanInfoVo tabPlanInfoVo) throws Exception;
     /**
     * 修改
     * @param tabPlanInfoVo
     */
     void update(TabPlanInfoVo tabPlanInfoVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabPlanInfoVo
     */
    Map<String,Object> getTabPlanInfoById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabPlanInfoVo 查询条件
     * @return 数据列表
     */
    List<TabPlanInfoVo> list(TabPlanInfoVo tabPlanInfoVo);



}