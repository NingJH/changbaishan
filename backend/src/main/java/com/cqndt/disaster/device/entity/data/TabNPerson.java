package com.cqndt.disaster.device.entity.data;

import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author : afnhia
 * @date : 2020年7月24日
 * description : 对应人员表
 */
@Data
@ApiModel("tab_n_person")
@TableName("tab_n_person")
public class TabNPerson {

    @Id
    @ApiModelProperty("id")
    private Integer id;

    @ApiModelProperty("人员姓名")
    private String name;

    @ApiModelProperty("人员头像")
    private String headImgUrl;

    @ApiModelProperty("城市")
    private String city;

    @ApiModelProperty("区县")
    private String county;

    @ApiModelProperty("个人地址")
    private String address;

    @ApiModelProperty("村社")
    private String village;

    @ApiModelProperty("个人手机")
    private String pPhone;

    @ApiModelProperty("工作手机")
    private String wPhone;

    @ApiModelProperty("工作岗位")
    private String job;

    @ApiModelProperty("工作职务")
    private String position;

    @ApiModelProperty("坐标（经度，纬度）")
    private String coordinates;

    @ApiModelProperty("人员类型")
    private Integer pType;

    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    @ApiModelProperty("最后修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;

    private int page;

    private int limit;

    private String typeName;

//    private List<Integer> findAllData;

    private List<String> tabNPersonBasic;

    private String personId;
}
