package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.vo.data.TabDeviceInstallVo;

import java.util.List;

public interface TabDeviceInstallService {
    /**
     * 增加
     * @param tabDeviceInstallVo
     */
    void save(TabDeviceInstallVo tabDeviceInstallVo) throws Exception;
     /**
     * 修改
     * @param tabDeviceInstallVo
     */
     void update(TabDeviceInstallVo tabDeviceInstallVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabDeviceInstallVo
     */
    TabDeviceInstallVo getTabDeviceInstallById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabDeviceInstallVo 查询条件
     * @return 数据列表
     */
    List<TabDeviceInstallVo> list(TabDeviceInstallVo tabDeviceInstallVo);



}