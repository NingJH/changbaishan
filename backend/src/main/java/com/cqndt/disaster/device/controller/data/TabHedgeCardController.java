package com.cqndt.disaster.device.controller.data;

import com.cqndt.disaster.device.common.annotation.SysLog;
import com.cqndt.disaster.device.service.data.TabHedgeCardService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabHedgeCardVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 * Date: 2019-06-11
 */
@RestController
@RequestMapping("tabHedgeCard")
@Api(description = "避险明白卡", value = "避险明白卡")
public class TabHedgeCardController {
    @Autowired
    private TabHedgeCardService tabHedgeCardService;

    @PostMapping("list")
    public Result listTabUser(@RequestBody TabHedgeCardVo tabHedgeCardVo) {
        PageHelper.startPage(tabHedgeCardVo.getPage(), tabHedgeCardVo.getLimit());
        PageInfo<TabHedgeCardVo> pageInfo = new PageInfo<TabHedgeCardVo>(tabHedgeCardService.list(tabHedgeCardVo));
        Result result = new Result();
        result.setCount((int) pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

    /**
     * 信息
     */
    @PostMapping("getTabHedgeCardById")
    public Result getTabHedgeCardById(@RequestBody TabHedgeCardVo hedgeCardVo) {
        if (null == hedgeCardVo.getId()) {
            return new Result().failure(-1, "请选择一条数据进行操作");
        }
        TabHedgeCardVo tabHedgeCardVo = tabHedgeCardService.getTabHedgeCardById(Long.valueOf(hedgeCardVo.getId()));
        return new Result().success(tabHedgeCardVo);
    }

    /**
     * 保存
     */
    @SysLog("增加")
    @PostMapping("save")
    @RequiresPermissions("sys:tabhedgecard:save")
    public Result save(@RequestBody TabHedgeCardVo tabHedgeCardVo) {
        Result result = new Result();
        try {
            tabHedgeCardService.save(tabHedgeCardVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 修改
     */
    @SysLog("修改")
    @PostMapping("update")
    @RequiresPermissions("sys:tabhedgecard:update")
    public Result update(@RequestBody TabHedgeCardVo tabHedgeCardVo) {
        Result result = new Result();
        try {
            if (null == tabHedgeCardVo.getId()) {
                return result.failure(-1, "请选择至少一条数据进行操作");
            }
            tabHedgeCardService.update(tabHedgeCardVo);
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }

    /**
     * 删除
     */
    @SysLog("删除")
    @PostMapping("delete")
    @RequiresPermissions("sys:tabhedgecard:delete")
    public Result delete(@RequestBody TabHedgeCardVo tabHedgeCardVo) {
        Result result = new Result();
        try {
            if (null == tabHedgeCardVo.getId()) {
                return result.failure(-1, "请选择至少一条数据进行操作");
            }
            tabHedgeCardService.delete(String.valueOf(tabHedgeCardVo.getId()));
            result.setMsg("操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            return result.failure(-1, "操作失败");
        }
        return result;
    }


}