package com.cqndt.disaster.device.config;

import com.cqndt.disaster.device.util.PrintUtil;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created By marc
 * Date: 2019/3/13  15:54
 * Description:shiro未登录反回状态码
 */
public class LoginAuthorizationFilter extends FormAuthenticationFilter {
    /**
     * 这个方法是未登录需要执行的方法
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request,
                                     ServletResponse response) throws Exception {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        Subject subject = getSubject(request, response);
        if (subject.getPrincipal() == null) {
            PrintUtil.printJson(httpRequest,httpResponse, -3,"登录超时,请重新登录");
        }
        return false;
    }
}
