package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.entity.data.TabDeviceCheck;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabDeviceCheckVo;

import java.util.List;
import java.util.Map;

public interface TabDeviceCheckService {

    /**
     * 查询
     * @param tabDeviceCheckVo
     * @return
     */
    List<TabDeviceCheckVo> list(TabDeviceCheckVo tabDeviceCheckVo);

    /**
     * 新增
     * @param tabDeviceCheckVo
     * @return
     */
    Result save(TabDeviceCheckVo tabDeviceCheckVo);

    /**
     * 更新
     * @param tabDeviceCheckVo
     * @return
     */
    Result update(TabDeviceCheckVo tabDeviceCheckVo);

    /**
     * 删除
     * @param id
     * @return
     */
    Result delete(String id);

}
