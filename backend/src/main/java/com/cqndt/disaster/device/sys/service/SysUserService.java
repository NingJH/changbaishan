package com.cqndt.disaster.device.sys.service;

import com.cqndt.disaster.device.sys.vo.SysUserVo;
import com.cqndt.disaster.device.util.Result;

import java.util.List;
import java.util.Map;

/**
 * Created By marc
 * Date: 2019/3/8  9:45
 * Description:用户管理
 */
public interface SysUserService {
    /**
     * 增加
     * @param sysUserVo
     */
    void save(SysUserVo sysUserVo) throws Exception;
    /**
     * 修改
     * @param sysUserVo
     */
    void update(SysUserVo sysUserVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return SysUserVo
     */
    SysUserVo getTabUserById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param sysUserVo 查询条件
     * @return 数据列表
     */
    List<SysUserVo> list(SysUserVo sysUserVo);
    /**
     * 信息验证.
     * @param sysUserVo 信息
     * @param flag (1修改 0增加)
     * @return Result
     */
    Result checkValidate(SysUserVo sysUserVo, Integer flag);
    /**
     * 下拉框list
     */
    List<Map<String,Object>> listTabStaticType(SysUserVo sysUserVo);

    /**
     * 子表信息
     */
    List<SysUserVo> listTabRoleAll(SysUserVo sysUserVo);
    /**
     * 子表信息
     */
    List<SysUserVo> listTabAreaAll(SysUserVo sysUserVo);
    /**s
     * 已选择子表信息
     */
    List<SysUserVo> listTabRoleSelect(Integer id);
    /**
     * 已选择子表信息
     */
    List<SysUserVo> listTabAreaSelect(Integer id);

    List<String> queryPermsForUser(Long userId);

    /**
     * 修改密码
     * @param userId 用户id
     * @param oldPass 原始密码
     * @param newPass 新密码
     * @return
     */
    Result upPassword(Long userId, String oldPass, String newPass);

    /**
     * 重置密码
     * @param userId 用户id
     * @param newPass 重置密码
     * @return
     */
    Result resetPass(Long userId, String newPass);

    String getAreaIdByUserId(Long userId);
    /**
     * 用户分配项目列表
     * @param vo
     * @return
     */
    Result assignProjectList(SysUserVo vo);

    /**
     * 用户已分配项目
     * @param id
     * @return
     */
    Result assignedProjectList(Long id);

    /**
     * 用户分配项目
     * @param id 用户id
     * @param projectIds 项目id
     * @return
     */
    Result assignProject(Long id,String projectIds);

    /**
     * 区域级联查询
     * @param areaParent 父级id
     * @return
     */
    Result cascadeSelectArea(String areaParent);

    /**
     * 查询用户区域
     * @param id
     * @return
     */
    Result cascadeSelectedArea(Integer id);

}
