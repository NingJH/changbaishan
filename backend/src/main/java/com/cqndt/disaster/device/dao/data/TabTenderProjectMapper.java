package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabTenderProject;
import com.cqndt.disaster.device.vo.data.TabTenderProjectVo;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Mapper;
import java.util.Map;
import java.util.List;


/**
 * Description: 招标项目管理
 * Date: 2019-06-04
 */

@Mapper
public interface TabTenderProjectMapper {
    /**
     * 根据条件查询list
     * @param tabTenderProjectVo 查询条件
     */
    List<TabTenderProjectVo> list(TabTenderProjectVo tabTenderProjectVo);
    /**
     * 增加
     * @param tabTenderProjectVo 条件
     */
    int save(TabTenderProjectVo tabTenderProjectVo);
    /**
     * 修改
     * @param tabTenderProjectVo 条件
     */
    int update(TabTenderProjectVo tabTenderProjectVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    TabTenderProjectVo getTabTenderProjectById(Long id);
     /**
      * 下拉框列表
      */
     List<Map<String,Object>> listTabStaticType(TabTenderProjectVo tabTenderProjectVo);

     List<TabTenderProject> getTenderProject();

}
