package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.vo.data.TabHumanVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * Description: 
 * Date: 2019-06-05
 */

@Mapper
public interface TabHumanMapper {
    /**
     * 根据条件查询list
     * @param tabHumanVo 查询条件
     */
    List<TabHumanVo> list(TabHumanVo tabHumanVo);
    /**
     * 增加
     * @param tabHumanVo 条件
     */
    int save(TabHumanVo tabHumanVo);
    /**
     * 修改
     * @param tabHumanVo 条件
     */
    int update(TabHumanVo tabHumanVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    TabHumanVo getTabHumanById(Long id);


}
