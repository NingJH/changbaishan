package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabSensorMapper;
import com.cqndt.disaster.device.service.data.TabSensorService;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.vo.data.TabSensorVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TabSensorServiceImpl implements TabSensorService {
    @Autowired
    private TabSensorMapper tabSensorMapper;

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(TabSensorVo tabSensorVo) throws Exception {
        tabSensorMapper.save(tabSensorVo);
    
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void update(TabSensorVo tabSensorVo) throws Exception {
        TabSensorVo vo = tabSensorMapper.getTabSensorById(tabSensorVo.getId());
        AttachmentVo attachmentVo = new AttachmentVo();
        tabSensorMapper.update(tabSensorVo);
     
   }
    @Override
    public TabSensorVo getTabSensorById(Long id){
        return tabSensorMapper.getTabSensorById(id);
    }
    @Override
    public void delete(String id){
         String[] ids = id.split(",");
         for (String idStr:ids) {
            tabSensorMapper.delete(Integer.parseInt(idStr));
         }
    }
    @Override
    public List<TabSensorVo> list(TabSensorVo tabSensorVo) {
        return  tabSensorMapper.list(tabSensorVo);
    }



}