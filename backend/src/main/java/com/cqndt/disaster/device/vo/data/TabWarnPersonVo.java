package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabWarnPeople;
import lombok.Data;

import java.util.List;

@Data
public class TabWarnPersonVo {
    private Integer id;
    private String cityCode;
    private String countyCode;
    private String code;
    private String address;
    private String projectName;
    private List<TabWarnPeople> jihe;
}
