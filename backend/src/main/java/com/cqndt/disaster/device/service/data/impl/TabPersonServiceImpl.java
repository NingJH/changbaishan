package com.cqndt.disaster.device.service.data.impl;


import com.cqndt.disaster.device.dao.data.TabPersonMapper;
import com.cqndt.disaster.device.entity.data.TabPerson;
import com.cqndt.disaster.device.entity.data.TabUnit;
import com.cqndt.disaster.device.service.data.TabPersonService;
import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabPersonVo;
import com.cqndt.disaster.device.vo.data.TabStaticTypeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class TabPersonServiceImpl implements TabPersonService {

    @Autowired
    private TabPersonMapper tabPersonMapper;

    @Override
    public List<Map<String,String>> queryTabPersonById(Integer unitId, String username) {
        return tabPersonMapper.queryTabPersonById(unitId,username);
    }

    @Override
    public Result save(TabPersonVo tabPersonVo){

        String regx="^[1](([3|5|8][\\d])|([4][4,5,6,7,8,9])|([6][2,5,6,7])|([7][^9])|([9][1,8,9]))[\\d]{8}$";
        if(tabPersonVo.getUsername()==null || "".equals(tabPersonVo.getUsername())){
            return new Result().failure(-1, "人员姓名不能为空");
        }
        if(tabPersonVo.getUnitId()==null || "".equals(tabPersonVo.getUnitId())){
            return new Result().failure(-1, "公司名称不能为空");
        }
        if(tabPersonVo.getType()==null || "".equals(tabPersonVo.getType())){
            return new Result().failure(-1, "人员类型不能为空");
        }
        if(tabPersonVo.getTel().length()!=11||tabPersonVo.getTel()==null||"".equals(tabPersonVo.getTel())){
            return new Result().failure(-1, "手机号位数不正确");
        }

        if(!(tabPersonVo.getTel().matches(regx))){
            return new Result().failure(-1, "手机号格式不正确");
        }

        Integer row=tabPersonMapper.save(tabPersonVo);

        if(row == null){
            return new Result().failure(-1, "人员信息未添加成功");
        }
        return new Result().success("添加成功");
    }

    @Override
    public List<TabPersonVo> list(TabPersonVo tabPersonVo) {
        return tabPersonMapper.list(tabPersonVo);
    }

    @Override
    public Result update(TabPersonVo tabPersonVo) throws Exception{
        if(null==tabPersonVo.getId()){
            return new Result().failure(-1, "请选择至少一条数据进行操作");
        }
        TabPersonVo vo=tabPersonMapper.getById(Integer.parseInt(tabPersonVo.getId()));
        if(vo==null){
            return new Result().failure(-1, "没有相应用户");
        }
        if(tabPersonVo.getUsername()==null||tabPersonVo.getUsername()==""){
            tabPersonVo.setUsername(vo.getUsername());
        }
        if(tabPersonVo.getTel()==null||tabPersonVo.getTel()==""){
            tabPersonVo.setTel(vo.getTel());
        }
        if(tabPersonVo.getType()==null||tabPersonVo.getType()==""){
            tabPersonVo.setType(vo.getType());
        }
        if(tabPersonVo.getJob()==null||tabPersonVo.getJob()==""){
            tabPersonVo.setJob(vo.getJob());
        }
        if(tabPersonVo.getUnitId()==null||tabPersonVo.getUnitId().equals("")){
            tabPersonVo.setUnitId(vo.getUnitId());
        }
        Integer row=tabPersonMapper.update(tabPersonVo);
        if(row==null){
            return new Result().failure(-1, "人员信息未修改成功");
        }
        return new Result().success("人员信息修改成功");
    }

    @Override
    public Result delete(TabPersonVo tabPersonVo) throws Exception{
        if (null == tabPersonVo.getId()) {
            return new Result().failure(-1,"请选择至少一条数据进行操作");
        }
        String[] ids = tabPersonVo.getId().split(",");
        for (String idStr : ids) {
            Integer row=tabPersonMapper.delete(Integer.parseInt(idStr));
            if(row==null){
                return new Result().failure(-1, "人员信息未删除成功");
            }
        }
        return new Result().success("人员信息删除成功");
    }

    @Override
    public List<TabUnit> findUnitList() {
        return tabPersonMapper.unitList();
    }

    @Override
    public List<TabStaticTypeVo> findPersonTypeList() {
        return tabPersonMapper.personTypeList();
    }

    @Override
    public List<TabPerson> getPersonListByType(Integer type) {
        return tabPersonMapper.getPersonByType(type);
    }


}
