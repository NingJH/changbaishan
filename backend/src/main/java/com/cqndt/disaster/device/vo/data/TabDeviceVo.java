package com.cqndt.disaster.device.vo.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.entity.data.TabDevice;
import lombok.Data;

import java.util.List;

/**
 * Description:
 * Date: 2019-06-11
 */
@Data
public class TabDeviceVo extends TabDevice {
    /**
     * 页数
     */
    private Integer page = 1;
    /**
     * 条数
     */
    private Integer limit = 10;

    private String monitorName;

    private String position;

    private Integer projectId;

    private String projectName;

    private Integer state;

    private String sensor;

    private String deviceInstallInfo;

    private Long deviceInstallId;

    private Long yuzhiId;

    private Long sensorId;

    private TabDeviceInstallVo deviceInstall;

    private List<TabWarnSettingVo> yuzhi;

    private List<TabSensorVo> SensorVos;

    private String userId;

    private Integer areaCode;

    private List<TabArea> list;
    //初始方位
    private String bearing;
    //x方位
    private String xaDirection;
    //-x方位
    private String x_Direction;

    //y方位
    private String yaDirection;
    //-y方位
    private String y_Direction;

    //z方位
    private String zaDirection;
    //-z方位
    private String z_Direction;

    private int monitorPerson;
    //设备名称
    private String deviceName;

    private String deviceNames;
    /**
     * 父级id
     */
    private Integer pid;

    private String label;

}
