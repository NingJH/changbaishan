package com.cqndt.disaster.device.dao.data;

import com.cqndt.disaster.device.entity.data.TabArea;
import com.cqndt.disaster.device.vo.data.TabMonitorVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * Description: 
 * Date: 2019-06-14
 */

@Mapper
public interface TabMonitorMapper {
    /**
     * 根据条件查询list
     * @param tabMonitorVo 查询条件
     */
    List<TabMonitorVo> list(TabMonitorVo tabMonitorVo);
    /**
     * 增加
     * @param tabMonitorVo 条件
     */
    int save(TabMonitorVo tabMonitorVo);
    /**
     * 修改
     * @param tabMonitorVo 条件
     */
    int update(TabMonitorVo tabMonitorVo);
    /**
     * 删除
     * @param id
     */
    int delete(Integer id);
    /**
     * 根据id查询单个记录
     * @param id
     */
    TabMonitorVo getTabMonitorById(Long id);


    List<Map<String,Object>> getUserProjectInfo(@Param("userId")Long userId);

    /**
     * 根据项目id查询设备
     * @param deviceId
     * @return
     */
    List<Map<String,String>> getDeviceNameList(@Param("deviceId") String deviceId, @Param("list")List<TabArea> list);

}
