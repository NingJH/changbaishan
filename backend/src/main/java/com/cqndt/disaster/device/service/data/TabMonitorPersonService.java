package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.util.Result;
import com.cqndt.disaster.device.vo.data.TabMonitorPersonVo;

public interface TabMonitorPersonService {
    /**
     * 监测人下拉列表（所属地区下）
     */
    Result getMonitorPerson(TabMonitorPersonVo vo);


    /**
     * 查询监测人大列表（所属地区下）
     */
    Result getMonitorPersonList(TabMonitorPersonVo tabMonitorPersonVo);

    /**
     * 新增监测人
     */
    Result addMonitorPerson(TabMonitorPersonVo tabMonitorPersonVo);

    /**
     * 修改监测人
     */
    Result updateMonitorPerson(TabMonitorPersonVo tabMonitorPersonVo);

    Result deleteMonitorPerson(TabMonitorPersonVo vo);
}
