package com.cqndt.disaster.device.entity.data;

import java.io.Serializable;

/**
* Description: 
* Date: 2019-06-11
*/

public class TabProjectPerson implements Serializable {
    /**
     * 序列化.
     */
    private static final long serialVersionUID = 1L;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     *  
     */
    private Long id;
    /**
     * 项目id
     */
    private Integer projectId;
    /**
     * 单位ID
     */
    private Integer unitId;

    /**
     * 人员ID
     */
    private Integer personId;

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }
}
