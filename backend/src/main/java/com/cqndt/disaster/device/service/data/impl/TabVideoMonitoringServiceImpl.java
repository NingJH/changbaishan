package com.cqndt.disaster.device.service.data.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.cqndt.disaster.device.dao.data.TabDeviceInstallMapper;
import com.cqndt.disaster.device.vo.data.TabDeviceInstallVo;
import com.cqndt.disaster.device.vo.data.TabVideoMonitoringVo;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.service.data.TabVideoMonitoringService;
import com.cqndt.disaster.device.dao.data.TabVideoMonitoringMapper;
import com.cqndt.disaster.device.util.FileOperate;
import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service
public class TabVideoMonitoringServiceImpl implements TabVideoMonitoringService{
    @Autowired
    private TabVideoMonitoringMapper tabVideoMonitoringMapper;

    @Autowired
    private TabDeviceInstallMapper tabDeviceInstallMapper;

    @Value("${file-ip}")
    private String fileIp;

    @Value("attachmentUrlSet")
    private String attachmentUrlSet;
    @Value("tempAttachmentUrl")
    private String tempAttachmentUrl;

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(TabVideoMonitoringVo tabVideoMonitoringVo) throws Exception {
        tabVideoMonitoringMapper.save(tabVideoMonitoringVo);
        List<AttachmentVo> attachments = tabVideoMonitoringVo.getAttachmentList();
        saveAttachment(attachments);
     
    }

    private void saveAttachment(List<AttachmentVo> attachments)throws Exception{
        if(null != attachments){
            for (AttachmentVo vo:attachments) {
                StringBuffer newUrl = new StringBuffer(attachmentUrlSet+File.separator);
                StringBuffer url = new StringBuffer(tempAttachmentUrl+File.separator);
                String attachmentType = vo.getAttachmentType();
                if("1".equals(attachmentType)){
                    newUrl.append("img");
                }else if("2".equals(attachmentType)){
                    newUrl.append("video");
                }else if("3".equals(attachmentType)){
                    newUrl.append("file");
                }
                Boolean flag = vo.getUrl().indexOf("tabVideoMonitoring")>=0;
                if(flag){
                    newUrl.append(File.separator+vo.getUrl());
                }else{
                    newUrl.append(File.separator+"tabVideoMonitoring"+File.separator+vo.getUrl());
                }
                url.append(vo.getUrl());
                FileOperate.copy(new File(url.toString()),new File(newUrl.toString()));
                if(flag){
                    vo.setAttachmentUrl(vo.getUrl());
                }else{
                    vo.setAttachmentUrl("tabVideoMonitoring"+File.separator+vo.getUrl());
                }
                tabVideoMonitoringMapper.saveTabVideoMonitoringAttachment(vo);
                if(!flag){
                    FileOperate.del(new File(url.toString()));
                }
            }
        }
    }
    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void update(TabVideoMonitoringVo tabVideoMonitoringVo) throws Exception {
        TabVideoMonitoringVo vo = tabVideoMonitoringMapper.getTabVideoMonitoringById(tabVideoMonitoringVo.getId());
        AttachmentVo attachmentVo = new AttachmentVo();
        String installPic = vo.getInstallPic()+"";
        String[] installPics = installPic.split(",");
        attachmentVo.setAttachmentIds(installPics);
        tabVideoMonitoringMapper.deleteTabVideoMonitoringAttachment(attachmentVo);
        String resultPic = vo.getResultPic()+"";
        String[] resultPics = resultPic.split(",");
        attachmentVo.setAttachmentIds(resultPics);
        tabVideoMonitoringMapper.deleteTabVideoMonitoringAttachment(attachmentVo);
        tabVideoMonitoringMapper.update(tabVideoMonitoringVo);
        List<AttachmentVo> attachments = tabVideoMonitoringVo.getAttachmentList();
        saveAttachment(attachments);
        
   }
    @Override
    public TabVideoMonitoringVo getTabVideoMonitoringById(Long id){
        return tabVideoMonitoringMapper.getTabVideoMonitoringById(id);
    }
    @Override
    public void delete(String id){
         String[] ids = id.split(",");
         for (String idStr:ids) {
            tabVideoMonitoringMapper.delete(Integer.parseInt(idStr));
         }
    }
    @Override
    public List<TabVideoMonitoringVo> list(TabVideoMonitoringVo tabVideoMonitoringVo) {
        return  tabVideoMonitoringMapper.list(tabVideoMonitoringVo);
    }
    @Override
    public List<Map<String,Object>> listTabArea(TabVideoMonitoringVo tabVideoMonitoringVo){
        return  tabVideoMonitoringMapper.listTabArea(tabVideoMonitoringVo);
    }

        @Override
    public Map<String,Object> listTabAreaUp(TabVideoMonitoringVo tabVideoMonitoringVo){
        return  tabVideoMonitoringMapper.listTabAreaUp(tabVideoMonitoringVo);
    }
    @Override
    public List<Map<String,Object>> listTabProject(TabVideoMonitoringVo tabVideoMonitoringVo){
        return  tabVideoMonitoringMapper.listTabProject(tabVideoMonitoringVo);
    }

        @Override
    public List<Map<String,Object>> listTabStaticType(TabVideoMonitoringVo tabVideoMonitoringVo){
        return  tabVideoMonitoringMapper.listTabStaticType(tabVideoMonitoringVo);
    }

    

    @Override
    public void deleteTabVideoMonitoringAttachment(TabVideoMonitoringVo tabVideoMonitoringVo){
        String attachmentId = tabVideoMonitoringVo.getAttachmentId();
        String[] ids = attachmentId.split(",");
        AttachmentVo attachmentVo = new AttachmentVo();
        attachmentVo.setAttachmentIds(ids);
        tabVideoMonitoringMapper.deleteTabVideoMonitoringAttachment(attachmentVo);
        TabVideoMonitoringVo vo = tabVideoMonitoringMapper.getTabVideoMonitoringById(tabVideoMonitoringVo.getId());
        //删除主表对应的附件id
        for (String id:ids) {
            if(null != vo.getInstallPic()){
                vo.setInstallPic(vo.getInstallPic().replace(id,""));
            }
            if(null != vo.getResultPic()){
                vo.setResultPic(vo.getResultPic().replace(id,""));
            }
        }
        //处理空字符串
        String installPic = vo.getInstallPic()+"";
        if(null != installPic){
            installPic = installPic.replaceAll(", ", ",");
            if(installPic.startsWith(",")){
                installPic = installPic.substring(1);
            }
            if(installPic.endsWith(",")){
                installPic = installPic.substring(0,installPic.length()-1);
            }
            vo.setInstallPic(installPic);
        }
        String resultPic = vo.getResultPic()+"";
        if(null != resultPic){
            resultPic = resultPic.replaceAll(", ", ",");
            if(resultPic.startsWith(",")){
                resultPic = resultPic.substring(1);
            }
            if(resultPic.endsWith(",")){
                resultPic = resultPic.substring(0,resultPic.length()-1);
            }
            vo.setResultPic(resultPic);
        }
    tabVideoMonitoringMapper.update(vo);

    }

    @Override
    public Map<String,Object> listTabVideoMonitoringAttachment(Long id){
        Map<String,Object> map = new HashMap<String,Object>();
        TabVideoMonitoringVo tabVideoMonitoringVo = tabVideoMonitoringMapper.getTabVideoMonitoringById(id);
        if(null == tabVideoMonitoringVo){
            return map;
        }
        AttachmentVo attachmentVo = new AttachmentVo();
        String installPic = tabVideoMonitoringVo.getInstallPic()+"";
        if(null != installPic&& !("").equals(installPic)){
            String[] installPics = installPic.split(",");
            attachmentVo.setAttachmentIds(installPics);
            List<AttachmentVo> installPic1 = tabVideoMonitoringMapper.listTabVideoMonitoringAttachment(attachmentVo);
            map.put("installPicData",installPic1);
        }
        String resultPic = tabVideoMonitoringVo.getResultPic()+"";
        if(null != resultPic&& !("").equals(resultPic)){
            String[] resultPics = resultPic.split(",");
            attachmentVo.setAttachmentIds(resultPics);
            List<AttachmentVo> resultPic2 = tabVideoMonitoringMapper.listTabVideoMonitoringAttachment(attachmentVo);
            map.put("resultPicData",resultPic2);
        }
        return map;
    }

    @Override
    public void saveVideo(TabVideoMonitoringVo tabVideoMonitoringVo) {
        tabVideoMonitoringVo.setChannelNo("1");
        tabVideoMonitoringVo.setInstallTime(new Date());
        tabVideoMonitoringMapper.saveVideo(tabVideoMonitoringVo);
    }

    @Override
    public List<TabVideoMonitoringVo> selectVideo(TabVideoMonitoringVo tabVideoMonitoringVo) {
        List<TabVideoMonitoringVo> videoList = tabVideoMonitoringMapper.selectVideo(tabVideoMonitoringVo);
        // 附件编号集合
        List<String> imgIds = new ArrayList<>();
        // 遍历获取附件编号
        for(TabVideoMonitoringVo tabVideoMonitoring : videoList){
            if(StringUtils.isEmpty(tabVideoMonitoring.getImg())){
                continue;
            }
            String[] arr = tabVideoMonitoring.getImg().split(",");
            imgIds.addAll(Arrays.asList(arr));
        }

        if(imgIds.isEmpty()){
            imgIds.add("");
        }

        // 附件信息集合
        List<Map<String, Object>> imgObjList = tabDeviceInstallMapper.getImgUrlAndByIdList(imgIds);

        for(TabVideoMonitoringVo tabVideoMonitoring : videoList){
            if(StringUtils.isEmpty(tabVideoMonitoring.getImg())){
                continue;
            }
            String[] arr = tabVideoMonitoring.getImg().split(",");

            // 当前安装信息临时图片集合
            List<Map<String, Object>> tempImgList = new ArrayList<>();
            for(String imgId : arr){
                for(Map<String, Object> imgMap : imgObjList){
                    if(!imgId.equals(imgMap.get("id").toString())){
                        continue;
                    }
                    imgMap.put("viewUrl", fileIp + File.separator+"img"+File.separator +imgMap.get("attachmentUrl").toString());
                    tempImgList.add(imgMap);
                }
            }
            if(!tempImgList.isEmpty()){
                tabVideoMonitoring.setFilesInfo(tempImgList);
            }
        }
        // 安装信息的组装
        for(TabVideoMonitoringVo tabVideoMonitoring : videoList){
            tabVideoMonitoring.setDeviceInstallInfo(JSON.toJSONStringWithDateFormat(tabVideoMonitoring, "yyyy-MM-dd", SerializerFeature.WriteDateUseDateFormat));
        }

        return videoList;
    }

    @Override
    public void updateVideo(TabVideoMonitoringVo tabVideoMonitoringVo) {
        tabVideoMonitoringMapper.updateVideo(tabVideoMonitoringVo);
    }

    @Override
    @Transactional
    public void deleteVideo(TabVideoMonitoringVo tabVideoMonitoringVo) {
        //删除摄像头信息
        if(tabVideoMonitoringVo.getDeleteId().contains(",")){
            String[] ids = tabVideoMonitoringVo.getDeleteId().split(",");
            for (int i = 0; i < ids.length; i++) {
                tabVideoMonitoringMapper.deleteVideo(ids[i]);
            }
        }else {
            tabVideoMonitoringMapper.deleteVideo(tabVideoMonitoringVo.getDeleteId());
        }
        //删除图片信息
        if(tabVideoMonitoringVo.getImg().contains(",")){
            String[] ids = tabVideoMonitoringVo.getImg().split(",");
            for (int i = 0; i < ids.length; i++) {
                tabVideoMonitoringMapper.deleteImg(ids[i]);
            }
        }else {
            tabVideoMonitoringMapper.deleteImg(tabVideoMonitoringVo.getImg());
        }
    }

    @Override
    public void updateVideoMark(TabVideoMonitoringVo tabVideoMonitoringVo) {
        tabVideoMonitoringMapper.updateVideoMark("","");
        tabVideoMonitoringMapper.updateVideoMark(tabVideoMonitoringVo.getSerial(),"1");
    }

}