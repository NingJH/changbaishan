package com.cqndt.disaster.device.service.data.impl;

import com.cqndt.disaster.device.dao.data.TabHedgeCardMapper;
import com.cqndt.disaster.device.service.data.TabHedgeCardService;
import com.cqndt.disaster.device.sys.vo.AttachmentVo;
import com.cqndt.disaster.device.vo.data.TabHedgeCardVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TabHedgeCardServiceImpl implements TabHedgeCardService {

    @Autowired
    private TabHedgeCardMapper tabHedgeCardMapper;

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void save(TabHedgeCardVo tabHedgeCardVo) throws Exception {
        tabHedgeCardMapper.save(tabHedgeCardVo);
    
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED)
    public void update(TabHedgeCardVo tabHedgeCardVo) throws Exception {
        TabHedgeCardVo vo = tabHedgeCardMapper.getTabHedgeCardById(Long.valueOf(tabHedgeCardVo.getId()));
        AttachmentVo attachmentVo = new AttachmentVo();
        tabHedgeCardMapper.update(tabHedgeCardVo);
     
   }
    @Override
    public TabHedgeCardVo getTabHedgeCardById(Long id){
        return tabHedgeCardMapper.getTabHedgeCardById(id);
    }
    @Override
    public void delete(String id){
         String[] ids = id.split(",");
         for (String idStr:ids) {
            tabHedgeCardMapper.delete(Integer.parseInt(idStr));
         }
    }
    @Override
    public List<TabHedgeCardVo> list(TabHedgeCardVo tabHedgeCardVo) {
        return  tabHedgeCardMapper.list(tabHedgeCardVo);
    }



}