package com.cqndt.disaster.device.entity.data;

import com.cqndt.disaster.device.vo.data.TabDeviceVo;
import com.cqndt.disaster.device.vo.data.TabjsVo;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class TabWarningDevice {
    //主键
    private Integer id;
    //设备编号
    private String deviceNo;
    //项目编号
    private String projectNo;
    //设备类型
    private Integer deviceType;
    //标识
    private String identify;
    //标识名称
    private String identifyName;
    //联合预警表id
    private Integer warningUniteId;
    //添加时间
    private Date createTime;
    //修改时间
    private Date updateTime;
    //序列
    private Integer sequence;
    //等级1
    private Double level1 =0d;
    //等级2
    private Double level2 =0d;
    //等级3
    private Double level3 =0d;
    //等级4
    private Double level4 =0d;


}
