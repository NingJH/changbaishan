package com.cqndt.disaster.device.service.data;

import com.cqndt.disaster.device.vo.data.TabSensorVo;

import java.util.List;

public interface TabSensorService {
    /**
     * 增加
     * @param tabSensorVo
     */
    void save(TabSensorVo tabSensorVo) throws Exception;
     /**
     * 修改
     * @param tabSensorVo
     */
     void update(TabSensorVo tabSensorVo) throws Exception;

    /**
     * 根据id获取信息
     * @param id
     * @return tabSensorVo
     */
    TabSensorVo getTabSensorById(Long id);

    /**
     * 删除
     * @param id id(多条逗号隔开)
     */
    void delete(String id);

    /**
     * 列表
     * @param tabSensorVo 查询条件
     * @return 数据列表
     */
    List<TabSensorVo> list(TabSensorVo tabSensorVo);



}