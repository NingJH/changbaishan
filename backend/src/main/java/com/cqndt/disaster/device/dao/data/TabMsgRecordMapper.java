package com.cqndt.disaster.device.dao.data;
import com.cqndt.disaster.device.entity.data.TabMsgRecord;
import com.cqndt.disaster.device.vo.data.TabMsgRecordVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TabMsgRecordMapper {
    /**
     * 根据参数动态查询
     * @param tabMsgRecordVo
     * @return
     */
    List<TabMsgRecord>showCondition(TabMsgRecordVo tabMsgRecordVo);
    /**
     * 添加短信记录
     * @param tabMsgRecord
     * @return
     */
    int addTabMsgRecord(TabMsgRecord tabMsgRecord);
}
