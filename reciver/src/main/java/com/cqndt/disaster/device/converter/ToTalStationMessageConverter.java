package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TbsTotalStationMapper;
import com.cqndt.disaster.device.domain.TbsTotalStation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-09 14:39
 **/
@Service
public class ToTalStationMessageConverter implements DeviceMessageConverter{

    @Autowired
    TbsTotalStationMapper tbsTotalStationMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 19;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TbsTotalStation tbsTotalStation = new TbsTotalStation();
        tbsTotalStation.setTime(acqTime);
        tbsTotalStation.setSensorNo(map.get("number"));
        tbsTotalStation.setX(BigDecimal.valueOf(Double.valueOf(map.get("x").equals(" ")?"0":map.get("x"))));
        tbsTotalStation.setY(BigDecimal.valueOf(Double.valueOf(map.get("y").equals(" ")?"0":map.get("y"))));
        tbsTotalStation.setH(BigDecimal.valueOf(Double.valueOf(map.get("h").equals(" ")?"0":map.get("h"))));
        tbsTotalStation.setX1(BigDecimal.valueOf(Double.valueOf(map.get("x1").equals(" ")?"0":map.get("x1"))));
        tbsTotalStation.setY1(BigDecimal.valueOf(Double.valueOf(map.get("y1").equals(" ")?"0":map.get("y1"))));
        tbsTotalStation.setZ1(BigDecimal.valueOf(Double.valueOf(map.get("z1").equals(" ")?"0":map.get("z1"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsTotalStation;
    }
}
