package com.cqndt.disaster.device.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.google.gson.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.HashOperations;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description: gson注册适配器以将日期类型作为长值进行管理
 * @Auther: lhl
 * @Date: 2019/4/23 15:07
 */
@Configuration
public class GsonConfig {

    @Bean
    public Gson adapterGson(){
        GsonBuilder builder = new GsonBuilder();
        builder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);

        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong());
            }
        });

        return  builder.create();
    }

    public static void main(String[] args) throws Exception{
            /*Double x = new Double("2585051.75120");
            Double x1 =  new Double("-8.7");
            Double y =  new Double("544438.27760");
            Double y1 =  new Double("1.30000");
            Long time2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-08-06 12:45:00").getTime();
            Long time1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2019-08-06 12:40:00").getTime();;


            Double time = (time2 - time1) / (1000.0 * 60 * 60);
            Double vx = ((x - x1) * 1000) / time;
            Double vy = ((y - y1) * 1000) / time;
            Double vs = (Math.sqrt(vx * vx + vy * vy));
            if (vs.toString().equals("Infinity")
                    || vs.toString().equals("-Infinity")
                    || vs.toString().equals("NaN")) {
                vs = 0.0;
            }
        System.out.println();*/
        String json = "{\"\":\"34.2\",\"sensor_no\":\"YNLH02_GNSS\",\"ax\":\"2585051.75120\",\"ah\":\"805.35980\",\"X\":\"-8.70000\",\"H\":\"9.40000\",\"ay\":\"544438.27760\",\"Y\":\"1.30000\",\"time\":\"2019-08-06 12:40:00\",\"type\":\"4\"}";
        GsonConfig cc = new GsonConfig();
        cc.analysisGnss(json);
    }

    public Map<String, String> analysisGnss(String json) throws ParseException {

        // 当前点坐标值
        Double xx = 0d;// 坐标值（m）
        Double yy = 0d;// 坐标值（m）
        Double hh = 0d; // 坐标值（m）
        // 上一个点坐标值
        Double xx1 = 0d;// 坐标值（m）
        Double yy1 = 0d;// 坐标值（m）
        Double hh1 = 0d; // 坐标值（m）
        // 上两个点坐标值
        Double xx2 = 0d;// 坐标值（m）
        Double yy2 = 0d;// 坐标值（m）
        Double hh2 = 0d; // 坐标值（m）

        Double x1 = 0d; // 偏移量（mm）
        Double y1 = 0d; // 偏移量（mm）
        Double h1 = 0d; // 偏移量（mm）

        String angel = "0"; // 偏移角度
        String degree = "";// 角度度数
        Double swy = 0d; // 水平位移（mm）
        Double cwy = 0d; // 垂直位移（mm）
        String swysd = "0.0"; // 水平位移速度（mm/h）
        String cwysd = "0.0"; // 垂直位移速度（mm/h）
        String swyjs = "0.0"; // 水平位移加速度（mm/h2）
        String cwyjs = "0.0"; // 垂直位移加速度（mm/h2）
        String aDatetime = "";// 时间
        Map<String, Object> map1 = new HashMap<String, Object>();
        Map<String, Object> map2 = new HashMap<String, Object>();
        Map<String, Object> map3 = new HashMap<String, Object>();

        Map<String, Object> p = (Map<String, Object>) JSONArray.parse(json);

//        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
//        String sensorNoRecored = p.get("sensor_no").toString() + "_LAST";
//        Map<Object, Object> entries = vo.entries(sensorNoRecored);

        List<Map<String, Object>> listDay = new ArrayList<>(16);

//        if (entries.size() == 0) {
//            vo.put(sensorNoRecored, "lastOne", json);
//            listDay.add(p);
//        } else {
        String one = "{\"\":\"7.2\",\"sensor_no\":\"YNLH03_GNSS\",\"ax\":\"2585047.7865\",\"ah\":\"806.6295\",\"X\":\"2.1\",\"H\":\"-3.3\",\"ay\":\"544482.6974\",\"Y\":\"-6.1\",\"time\":\"2019-08-06 16:55:00\",\"type\":\"4\"}";
        String two = "{\"\":\"7.2\",\"sensor_no\":\"YNLH03_GNSS\",\"ax\":\"2585047.78660\",\"ah\":\"806.62960\",\"X\":\"2.10000\",\"H\":\"-3.20000\",\"ay\":\"544482.69740\",\"Y\":\"-6.10000\",\"time\":\"2019-08-06 16:50:00\",\"type\":\"4\"}";
        Map<String, Object> mapOne = (Map<String, Object>) JSON.parse(one);
        Map<String, Object> mapTow = (Map<String, Object>) JSON.parse(two);
        listDay.add(mapOne);
        listDay.add(mapTow);
//        }
        if (listDay.size() == 2) {
            if (true) {
                listDay.add(p);
                for (int i = 2; i < listDay.size(); i++) {
                    map1 = listDay.get(i);// 取出集合里第一条数据
                    aDatetime = map1.get("time").toString();
                    xx = Double.valueOf(map1.get("ax").toString());// 坐标值（m）
                    yy = Double.valueOf(map1.get("ay").toString());// 坐标值（m）
                    hh = Double.valueOf(map1.get("ah").toString()); // 坐标值（m）
                    x1 = Double.valueOf(map1.get("X").toString()); // 偏移量（mm）
                    y1 = Double.valueOf(map1.get("Y").toString()); // 偏移量（mm）
                    h1 = Double.valueOf(map1.get("H").toString()); // 偏移量（mm）

                    angel = getJD(y1, x1); // 偏移角度
                    // degree = getDegree(yy, xx);
                    degree = getDegree(y1, x1);
                    swy = getWY(x1, y1); // 水平位移（mm）
                    cwy = h1; // 垂直位移（mm）
                    map2 = listDay.get(i - 1);// 上一个点
                    map3 = listDay.get(i - 2);// 上两个点
                    Long time1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 1).get("time") + "").getTime();
                    Long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i).get("time") + "").getTime();
                    Long time3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 2).get("time") + "").getTime();
                    // 上一个点数据
                    if (map2.get("ah") != null && !"".equals(map2.get("ah"))) {
                        hh1 = Double.valueOf(map2.get("ah").toString());
                    }
                    if (map2.get("ax") != null && !"".equals(map2.get("ax"))) {
                        xx1 = Double.valueOf(map2.get("ax").toString());
                    }
                    if (map2.get("ay") != null && !"".equals(map2.get("ay"))) {
                        yy1 = Double.valueOf(map2.get("ay").toString());
                    }
                    // 上两个点数据
                    if (map3.get("ah") != null && !"".equals(map3.get("ah"))) {
                        hh2 = Double.valueOf(map3.get("ah").toString());
                    }
                    if (map3.get("ax") != null && !"".equals(map3.get("ax"))) {
                        xx2 = Double.valueOf(map3.get("ax").toString());
                    }
                    if (map3.get("ay") != null && !"".equals(map3.get("ay"))) {
                        yy2 = Double.valueOf(map3.get("ay").toString());
                    }
                    Double lspeed = getSuLvL(xx, xx1, yy, yy1, time1, time);
                    Double hspeed = getSuLvH(hh, hh1, time1, time);
                    Double lspeed1 = getSuLvL(xx1, xx2, yy1, yy2, time3, time1);
                    Double hspeed1 = getSuLvH(hh1, hh2, time3, time1);
                    swysd = String.format("%.2f", lspeed);// 水平速率
                    cwysd = String.format("%.2f", hspeed);// 垂直速率
                    swyjs = String.format("%.2f", (lspeed - lspeed1)
                            / ((time - time1) / (1000.0 * 60 * 60)));// 水平加速度
                    cwyjs = String.format("%.2f", (hspeed - hspeed1)
                            / ((time - time1) / (1000.0 * 60 * 60)));// 垂直加速度
                    if (cwyjs.toString().equals("Infinity")
                            || cwyjs.toString().equals("-Infinity")
                            || cwyjs.toString().equals("NaN")) {
                        cwyjs = "0.0";
                    }
                    if (swyjs.toString().equals("Infinity")
                            || swyjs.toString().equals("-Infinity")
                            || swyjs.toString().equals("NaN")) {
                        swyjs = "0.0";
                    }
                    System.out.println("swysd:" + swysd);
                    System.out.println("cwysd:" + cwysd);
                    System.out.println("swyjs:" + swyjs);
                    System.out.println("cwyjs:" + cwyjs);
                }
            }
        } else if (listDay.size() == 1) {
            if (true) {
                listDay.add(p);
                for (int i = 1; i < listDay.size(); i++) {
                    map1 = listDay.get(i);
                    aDatetime = map1.get("time").toString();
                    xx = Double.valueOf(map1.get("ax").toString());// 坐标值（m）
                    yy = Double.valueOf(map1.get("ay").toString());// 坐标值（m）
                    hh = Double.valueOf(map1.get("ah").toString()); // 坐标值（m）
                    x1 = Double.valueOf(map1.get("X").toString()); // 偏移量（mm）
                    y1 = Double.valueOf(map1.get("Y").toString()); // 偏移量（mm）
                    h1 = Double.valueOf(map1.get("H").toString()); // 偏移量（mm）
                    angel = getJD(y1, x1); // 偏移角度
                    // degree = getDegree(yy, xx);
                    degree = getDegree(y1, x1);
                    swy = getWY(x1, y1); // 水平位移（mm）
                    cwy = getWY(h1, 0.0); // 垂直位移（mm）
                    if (i == 1) {
                        Long time1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 1).get("time") + "").getTime();
                        Long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i).get("time") + "").getTime();
                        map2 = listDay.get(i - 1);
                        // 上一个点数据
                        if (map2.get("ah") != null
                                && !"".equals(map2.get("ah"))) {
                            hh1 = Double.valueOf(map2.get("ah").toString());
                        }
                        if (map2.get("ax") != null
                                && !"".equals(map2.get("ax"))) {
                            xx1 = Double.valueOf(map2.get("ax").toString());
                        }
                        if (map2.get("ay") != null
                                && !"".equals(map2.get("ay"))) {
                            yy1 = Double.valueOf(map2.get("ay").toString());
                        }
                        Double lspeed = getSuLvL(xx, xx1, yy, yy1, time1, time);
                        Double hspeed = getSuLvH(hh, hh1, time1, time);
                        swysd = String.format("%.2f", lspeed);// 水平速率
                        cwysd = String.format("%.2f", hspeed);// 垂直速率

                        swyjs = String.format("%.2f", lspeed
                                / ((time - time1) / (1000.0 * 60 * 60)));// 水平加速度
                        cwyjs = String.format("%.2f", hspeed
                                / ((time - time1) / (1000.0 * 60 * 60)));// 垂直加速度

                        if (cwyjs.toString().equals("Infinity")
                                || cwyjs.toString().equals("-Infinity")
                                || cwyjs.toString().equals("NaN")) {
                            cwyjs = "0.0";
                        }
                        if (swyjs.toString().equals("Infinity")
                                || swyjs.toString().equals("-Infinity")
                                || swyjs.toString().equals("NaN")) {
                            swyjs = "0.0";
                        }
                        System.out.println("swysd:" + swysd);
                        System.out.println("cwysd:" + cwysd);
                        System.out.println("swyjs:" + swyjs);
                        System.out.println("cwyjs:" + cwyjs);
                    } else {
                        map2 = listDay.get(i - 1);// 上一个点
                        map3 = listDay.get(i - 2);// 上两个点
                        Long time1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 1).get("time") + "").getTime();
                        Long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i).get("time") + "").getTime();
                        Long time3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 2).get("time") + "").getTime();
                        // 上一个点数据
                        if (map2.get("ah") != null
                                && !"".equals(map2.get("ah"))) {
                            hh1 = Double.valueOf(map2.get("ah").toString());
                        }
                        if (map2.get("ax") != null
                                && !"".equals(map2.get("ax"))) {
                            xx1 = Double.valueOf(map2.get("ax").toString());
                        }
                        if (map2.get("ay") != null
                                && !"".equals(map2.get("ay"))) {
                            yy1 = Double.valueOf(map2.get("ay").toString());
                        }
                        // 上两个点数据
                        if (map3.get("ah") != null
                                && !"".equals(map3.get("ah"))) {
                            hh2 = Double.valueOf(map3.get("ah").toString());
                        }
                        if (map3.get("ax") != null
                                && !"".equals(map3.get("ax"))) {
                            xx2 = Double.valueOf(map3.get("ax").toString());
                        }
                        if (map3.get("ay") != null
                                && !"".equals(map3.get("ay"))) {
                            yy2 = Double.valueOf(map3.get("ay").toString());
                        }
                        Double lspeed = getSuLvL(xx, xx1, yy, yy1, time1, time);
                        Double hspeed = getSuLvH(hh, hh1, time1, time);
                        Double lspeed1 = getSuLvL(xx1, xx2, yy1, yy2, time3,
                                time1);
                        Double hspeed1 = getSuLvH(hh1, hh2, time3, time1);
                        swysd = String.format("%.5f", lspeed);// 水平速率
                        cwysd = String.format("%.5f", hspeed);// 垂直速率
                        swyjs = String.format("%.5f", (lspeed - lspeed1)
                                / ((time - time1) / (1000.0 * 60 * 60)));// 水平加速度
                        cwyjs = String.format("%.5f", (hspeed - hspeed1)
                                / ((time - time1) / (1000.0 * 60 * 60)));// 垂直加速度
                        if (cwyjs.toString().equals("Infinity")
                                || cwyjs.toString().equals("-Infinity")
                                || cwyjs.toString().equals("NaN")) {
                            cwyjs = "0.0";
                        }
                        if (swyjs.toString().equals("Infinity")
                                || swyjs.toString().equals("-Infinity")
                                || swyjs.toString().equals("NaN")) {
                            swyjs = "0.0";
                        }
                        System.out.println("swysd:" + swysd);
                        System.out.println("cwysd:" + cwysd);
                        System.out.println("swyjs:" + swyjs);
                        System.out.println("cwyjs:" + cwyjs);
                    }
                }
            }
        } else {
            if (true) {
                listDay.add(p);
                for (int i = 0; i < listDay.size(); i++) {
                    map1 = listDay.get(i);
                    aDatetime = map1.get("time").toString();
                    xx = Double.valueOf(map1.get("ax").toString());// 坐标值（m）
                    yy = Double.valueOf(map1.get("ay").toString());// 坐标值（m）
                    hh = Double.valueOf(map1.get("ah").toString()); // 坐标值（m）
                    x1 = Double.valueOf(map1.get("X").toString()); // 偏移量（mm）
                    y1 = Double.valueOf(map1.get("Y").toString()); // 偏移量（mm）
                    h1 = Double.valueOf(map1.get("H").toString()); // 偏移量（mm）
                    angel = getJD(y1, x1); // 偏移角度
                    // degree = getDegree(yy, xx);
                    degree = getDegree(y1, x1);
                    swy = getWY(x1, y1); // 水平位移（mm）
                    cwy = getWY(h1, 0.0); // 垂直位移（mm）
                    if (i == 0) {
                        swysd = "0.0"; // 水平位移速度（mm/h）
                        cwysd = "0.0"; // 垂直位移速度（mm/h）
                        swyjs = "0.0"; // 水平位移加速度（mm/h2）
                        cwyjs = "0.0"; // 垂直位移加速度（mm/h2）
                    } else if (i == 1) {
                        Long time1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 1).get("time") + "").getTime();
                        Long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i).get("time") + "").getTime();
                        map2 = listDay.get(i - 1);
                        // 上一个点数据
                        if (map2.get("ah") != null
                                && !"".equals(map2.get("ah"))) {
                            hh1 = (Double) map2.get("ah");
                        }
                        if (map2.get("ax") != null
                                && !"".equals(map2.get("ax"))) {
                            xx1 = (Double) map2.get("ax");
                        }
                        if (map2.get("ay") != null
                                && !"".equals(map2.get("ay"))) {
                            yy1 = (Double) map2.get("ay");
                        }
                        Double lspeed = getSuLvL(xx, xx1, yy, yy1, time1, time);
                        Double hspeed = getSuLvH(hh, hh1, time1, time);
                        swysd = String.format("%.5f", lspeed);// 水平速率
                        cwysd = String.format("%.5f", hspeed);// 垂直速率
                        swyjs = String.format("%.5f", lspeed
                                / ((time - time1) / (1000.0 * 60 * 60)));// 水平加速度
                        cwyjs = String.format("%.5f", hspeed
                                / ((time - time1) / (1000.0 * 60 * 60)));// 垂直加速度
                        if (cwyjs.toString().equals("Infinity")
                                || cwyjs.toString().equals("-Infinity")
                                || cwyjs.toString().equals("NaN")) {
                            cwyjs = "0.0";
                        }
                        if (swyjs.toString().equals("Infinity")
                                || swyjs.toString().equals("-Infinity")
                                || swyjs.toString().equals("NaN")) {
                            swyjs = "0.0";
                        }
                        System.out.println("swysd:" + swysd);
                        System.out.println("cwysd:" + cwysd);
                        System.out.println("swyjs:" + swyjs);
                        System.out.println("cwyjs:" + cwyjs);
                    } else {
                        map2 = listDay.get(i - 1);// 上一个点
                        map3 = listDay.get(i - 2);// 上两个点
                        Long time1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 1).get("time") + "").getTime();
                        Long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i).get("time") + "").getTime();
                        Long time3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 2).get("time") + "").getTime();
                        // 上一个点数据
                        if (map2.get("ah") != null
                                && !"".equals(map2.get("ah"))) {
                            hh1 = (Double) map2.get("ah");
                        }
                        if (map2.get("ax") != null
                                && !"".equals(map2.get("ax"))) {
                            xx1 = (Double) map2.get("ax");
                        }
                        if (map2.get("ay") != null
                                && !"".equals(map2.get("ay"))) {
                            yy1 = (Double) map2.get("ay");
                        }
                        // 上两个点数据
                        if (map3.get("ah") != null
                                && !"".equals(map3.get("ah"))) {
                            hh2 = (Double) map3.get("ah");
                        }
                        if (map3.get("ax") != null
                                && !"".equals(map3.get("ax"))) {
                            xx2 = (Double) map3.get("ax");
                        }
                        if (map3.get("ay") != null
                                && !"".equals(map3.get("ay"))) {
                            yy2 = (Double) map3.get("ay");
                        }
                        Double lspeed = getSuLvL(xx, xx1, yy, yy1, time1, time);
                        Double hspeed = getSuLvH(hh, hh1, time1, time);
                        Double lspeed1 = getSuLvL(xx1, xx2, yy1, yy2, time3,
                                time1);
                        Double hspeed1 = getSuLvH(hh1, hh2, time3, time1);
                        swysd = String.format("%.5f", lspeed);// 水平速率
                        cwysd = String.format("%.5f", hspeed);// 垂直速率
                        swyjs = String.format("%.5f", (lspeed - lspeed1)
                                / ((time - time1) / (1000.0 * 60 * 60)));// 水平加速度
                        cwyjs = String.format("%.5f", (hspeed - hspeed1)
                                / ((time - time1) / (1000.0 * 60 * 60)));// 垂直加速度
                        if (cwyjs.toString().equals("Infinity")
                                || cwyjs.toString().equals("-Infinity")
                                || cwyjs.toString().equals("NaN")) {
                            cwyjs = "0.0";
                        }
                        if (swyjs.toString().equals("Infinity")
                                || swyjs.toString().equals("-Infinity")
                                || swyjs.toString().equals("NaN")) {
                            swyjs = "0.0";
                        }

                        System.out.println("swysd:" + swysd);
                        System.out.println("cwysd:" + cwysd);
                        System.out.println("swyjs:" + swyjs);
                        System.out.println("cwyjs:" + cwyjs);
                    }
                }
            }
        }
        return null;
    }

    /**
     * 获取偏移方向
     *
     * @return
     */
    private String getJD(Double ay, Double ax) {
        ay = new BigDecimal(ay.toString()).doubleValue();
        ax = new BigDecimal(ax.toString()).doubleValue();
        double rotateAngel = Math.atan2(ay, ax) * 180 / (Math.PI);
        Double angle = Double.parseDouble(String.format("%.3f", rotateAngel));
        String directionStr = "";
        if (rotateAngel > 0 && rotateAngel < 90) {
            directionStr = "北偏东" + angle + "度";
        } else if (rotateAngel > -90 && rotateAngel < 0) {
            angle = Double.parseDouble(String.format("%.3f", (0 - angle)));
            directionStr = "北偏西" + angle + "度";
        } else if (rotateAngel > 90 && rotateAngel < 180) {
            angle = Double.parseDouble(String.format("%.3f", (180 - angle)));
            directionStr = "南偏东" + angle + "度";
        } else if (rotateAngel > -180 && rotateAngel < -90) {
            angle = Double.parseDouble(String.format("%.3f", (180 + angle)));
            directionStr = "南偏西" + angle + "度";
        } else if (rotateAngel == 0) {
            directionStr = "正北方向";
        } else if (rotateAngel == 90) {
            directionStr = "正东方向";
        } else if (rotateAngel == -90) {
            directionStr = "正西方向";
        } else if (rotateAngel == 180 || rotateAngel == -180) {
            directionStr = "正南方向";
        }
        return directionStr;
    }

    /**
     * 偏移角度
     *
     * @param ay
     * @param ax
     * @return
     */
    private String getDegree(Double ay, Double ax) {
        ay = new BigDecimal(ay.toString()).doubleValue();
        ax = new BigDecimal(ax.toString()).doubleValue();
        double rotateAngel = Math.atan2(ay, ax) * 180 / (Math.PI);
        Double angle = Double.parseDouble(String.format("%.3f", rotateAngel));
        return angle + "";
    }

    /**
     * 位移算法
     */
    private Double getWY(Double x, Double y) {
        return Math.sqrt(x * x + y * y);
    }

    /**
     * 水平位移速度(mm/h)
     *
     * @param x     当前x数值
     * @param x1    上一点x数值
     * @param y     当前y数值
     * @param y1    上一点y数值
     * @param time1 上一点时间 (ms)
     * @param time2 当前点时间 (ms)
     * @return
     */
    private Double getSuLvL(Double x, Double x1, Double y, Double y1,
                            Long time1, Long time2) {

        Double time = (time2 - time1) / (1000.0 * 60 * 60);
        Double vx = ((x - x1) * 1000) / time;
        Double vy = ((y - y1) * 1000) / time;
        Double vs = (Math.sqrt(vx * vx + vy * vy));
        if (vs.toString().equals("Infinity")
                || vs.toString().equals("-Infinity")
                || vs.toString().equals("NaN")) {
            vs = 0.0;
        }
        return vs;
    }

    /**
     * 垂直方向速度(mm/h)
     *
     * @param h     当前h数值
     * @param h1    上一点h数值
     * @param time1 上一点时间
     * @param time2 当前点时间
     * @return
     */
    private Double getSuLvH(Double h, Double h1, Long time1, Long time2) {
        Double time = (time2 - time1) / (1000.0 * 60 * 60);
        Double num = ((h - h1) * 1000) / time;
        if (num.toString().equals("Infinity")
                || num.toString().equals("-Infinity")
                || num.toString().equals("NaN")) {
            num = 0.0;
        }
        return num;
    }
}
