package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSON;
import com.cqndt.disaster.device.dao.TabAlarmInfoMapper;
import com.cqndt.disaster.device.dao.TabMonitorMapper;
import com.cqndt.disaster.device.dao.TabWarnSettingMapper;
import com.cqndt.disaster.device.dao.TbsGnssMapper;
import com.cqndt.disaster.device.dao.warn.WarnPlanMapper;
import com.cqndt.disaster.device.domain.TbsGnss;
import com.cqndt.disaster.device.domain.WarnPlan;
import com.cqndt.disaster.device.utils.JSONUtils;
import com.cqndt.disaster.device.websocket.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author Frog
 */
@Service
public class GnssMessageConverter implements DeviceMessageConverter {

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WebSocketServer webSocketServer;
    @Autowired
    private TabWarnSettingMapper tabWarnSettingMapper;
    @Autowired
    private TabMonitorMapper tabMonitorMapper;
    @Autowired
    private TabAlarmInfoMapper tabAlarmInfoMapper;
    @Autowired
    private TbsGnssMapper tbsGnssMapper;

    @Autowired
    private WarnPlanMapper warnPlanMapper;

    @Override
    public Integer getDeviceType() {
        return 4;
    }

    public static void replaceKey(Map map, String key, String replaceKey){
        map.put(replaceKey, map.get(key));
        map.remove(key);
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = analysisGnss(json);

        if (null != map) {
            String sensorNo = map.get("sensor_no");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date acqTime = sdf.parse(map.get("time"));
            String MonitorNo = tabMonitorMapper.selectMNoBySeonsorNo(sensorNo);

            TbsGnss tbsGnss = new TbsGnss();
            tbsGnss.setMonitorNo(MonitorNo);
            tbsGnss.setTime(acqTime);
            tbsGnss.setSensorNo(sensorNo);
            tbsGnss.setX(BigDecimal.valueOf(Double.valueOf(map.get("x"))));
            tbsGnss.setY(BigDecimal.valueOf(Double.valueOf(map.get("y"))));
            tbsGnss.setZ(BigDecimal.valueOf(Double.valueOf(map.get("z"))));
            tbsGnss.setX1(BigDecimal.valueOf(Double.valueOf(map.get("x1"))));
            tbsGnss.setY1(BigDecimal.valueOf(Double.valueOf(map.get("y1"))));
            tbsGnss.setZ1(BigDecimal.valueOf(Double.valueOf(map.get("z1"))));
//            tbsGnss.setAngel(BigDecimal.valueOf(Double.valueOf(map.get("angel"))));
            tbsGnss.setSwy(BigDecimal.valueOf(Double.valueOf(map.get("swy"))));
            tbsGnss.setCwy(BigDecimal.valueOf(Double.valueOf(map.get("cwy"))));
            tbsGnss.setSwysd(BigDecimal.valueOf(Double.valueOf(map.get("swysd"))));
            tbsGnss.setCwysd(BigDecimal.valueOf(Double.valueOf(map.get("cwysd"))));
            tbsGnss.setSwyjs(BigDecimal.valueOf(Double.valueOf(map.get("swyjs"))));
            tbsGnss.setCwyjs(BigDecimal.valueOf(Double.valueOf(map.get("cwyjs"))));

//            HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
//            vo.putAll(sensorNo, map);
            HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
            vo.put(map.get("sensor_no"), "time", map.get("time"));

            //告警推送判断
            WarnPlan swyWarnPlan = new WarnPlan("swy", tbsGnss.getSwy(), sensorNo, 3);
            warnPlanMapper.insertWarnPlan(swyWarnPlan);

            WarnPlan cwyWarnPlan = new WarnPlan("cwy", tbsGnss.getCwy(), sensorNo, 3);
            warnPlanMapper.insertWarnPlan(cwyWarnPlan);

            return tbsGnss;
        }
        return new TbsGnss();
    }

    public Map<String, String> analysisGnss(String json) throws Exception {
        Map<String, Object> p = JSONUtils.json2map(json);
        if(!p.containsKey("sensor_no")){
            Map<String, String> keyMap = new HashMap<>();
            keyMap.put("number","sensor_no");
            keyMap.put("acqTime","time");
            keyMap.put("x1","X");
            keyMap.put("y1","Y");
            keyMap.put("z1","H");
            keyMap.put("x","ax");
            keyMap.put("y","ay");
            keyMap.put("z","ah");

            for (Map.Entry<String, String> map : keyMap.entrySet()){
                replaceKey(p, map.getKey(), map.getValue());
            }
        }
        //        Map<String, Object> p = (Map<String, Object>) JSONArray.parse(json);

        // 当前点坐标值
        Double xx = 0d;// 坐标值（m）
        Double yy = 0d;// 坐标值（m）
        Double hh = 0d; // 坐标值（m）
        // 上一个点坐标值
        Double xx1 = 0d;// 坐标值（m）
        Double yy1 = 0d;// 坐标值（m）
        Double hh1 = 0d; // 坐标值（m）
        // 上两个点坐标值
        Double xx2 = 0d;// 坐标值（m）
        Double yy2 = 0d;// 坐标值（m）
        Double hh2 = 0d; // 坐标值（m）

        Double x1 = 0d; // 偏移量（mm）
        Double y1 = 0d; // 偏移量（mm）
        Double h1 = 0d; // 偏移量（mm）

        String angel = "0"; // 偏移角度
        String degree = "";// 角度度数
        Double swy = 0d; // 水平位移（mm）
        Double cwy = 0d; // 垂直位移（mm）
        String swysd = "0.0"; // 水平位移速度（mm/h）
        String cwysd = "0.0"; // 垂直位移速度（mm/h）
        String swyjs = "0.0"; // 水平位移加速度（mm/h2）
        String cwyjs = "0.0"; // 垂直位移加速度（mm/h2）
        String aDatetime = "";// 时间
        Map<String, Object> map1 = new HashMap<String, Object>();
        Map<String, Object> map2 = new HashMap<String, Object>();
        Map<String, Object> map3 = new HashMap<String, Object>();


        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        String sensorNoRecored = p.get("sensor_no").toString() + "_LAST";
        Map<Object, Object> entries = vo.entries(sensorNoRecored);

        List<Map<String, Object>> listDay = new ArrayList<>(16);

        if (entries.size() == 0) {
            vo.put(sensorNoRecored, "lastOne", json);
            listDay.add(p);
        } else {
            Map<String, Object> mapOne = (Map<String, Object>) JSON.parse(entries.get("lastOne").toString());
            if (entries.size() > 1) {
                Map<String, Object> mapTow = (Map<String, Object>) JSON.parse(entries.get("lastTow").toString());
                listDay.add(mapOne);
                listDay.add(mapTow);
            } else {
                listDay.add(mapOne);
            }
            String mapOneJson = JSON.toJSONString(mapOne);
            vo.put(sensorNoRecored, "lastTow", mapOneJson);
            vo.put(sensorNoRecored, "lastOne", json);
        }

        if (listDay.size() == 2) {
            if (true) {
                listDay.add(p);
                for (int i = 2; i < listDay.size(); i++) {
                    map1 = listDay.get(i);// 取出集合里第一条数据
                    aDatetime = map1.get("time").toString();
                    xx = Double.valueOf(map1.get("ax").toString());// 坐标值（m）
                    yy = Double.valueOf(map1.get("ay").toString());// 坐标值（m）
                    hh = Double.valueOf(map1.get("ah").toString()); // 坐标值（m）
                    x1 = Double.valueOf(map1.get("X").toString()); // 偏移量（mm）
                    y1 = Double.valueOf(map1.get("Y").toString()); // 偏移量（mm）
                    h1 = Double.valueOf(map1.get("H").toString()); // 偏移量（mm）

                    angel = getJD(y1, x1); // 偏移角度
                    // degree = getDegree(yy, xx);
                    degree = getDegree(y1, x1);
                    swy = getWY(x1, y1); // 水平位移（mm）
                    cwy = h1; // 垂直位移（mm）
                    map2 = listDay.get(i - 1);// 上一个点
                    map3 = listDay.get(i - 2);// 上两个点
                    Long time1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 1).get("time") + "").getTime();
                    Long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i).get("time") + "").getTime();
                    Long time3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 2).get("time") + "").getTime();
                    // 上一个点数据
                    if (map2.get("ah") != null && !"".equals(map2.get("ah"))) {
                        hh1 = Double.valueOf(map2.get("ah").toString());
                    }
                    if (map2.get("ax") != null && !"".equals(map2.get("ax"))) {
                        xx1 = Double.valueOf(map2.get("ax").toString());
                    }
                    if (map2.get("ay") != null && !"".equals(map2.get("ay"))) {
                        yy1 = Double.valueOf(map2.get("ay").toString());
                    }
                    // 上两个点数据
                    if (map3.get("ah") != null && !"".equals(map3.get("ah"))) {
                        hh2 = Double.valueOf(map3.get("ah").toString());
                    }
                    if (map3.get("ax") != null && !"".equals(map3.get("ax"))) {
                        xx2 = Double.valueOf(map3.get("ax").toString());
                    }
                    if (map3.get("ay") != null && !"".equals(map3.get("ay"))) {
                        yy2 = Double.valueOf(map3.get("ay").toString());
                    }
                    Double lspeed = getSuLvL(xx, xx1, yy, yy1, time1, time);
                    Double hspeed = getSuLvH(hh, hh1, time1, time);
                    Double lspeed1 = getSuLvL(xx1, xx2, yy1, yy2, time3, time1);
                    Double hspeed1 = getSuLvH(hh1, hh2, time3, time1);
                    swysd = String.format("%.2f", lspeed);// 水平速率
                    cwysd = String.format("%.2f", hspeed);// 垂直速率
                    swyjs = String.format("%.2f", (lspeed - lspeed1)
                            / ((time - time1) / (1000.0 * 60 * 60)));// 水平加速度
                    cwyjs = String.format("%.2f", (hspeed - hspeed1)
                            / ((time - time1) / (1000.0 * 60 * 60)));// 垂直加速度
                    if (cwyjs.toString().equals("Infinity")
                            || cwyjs.toString().equals("-Infinity")
                            || cwyjs.toString().equals("NaN")) {
                        cwyjs = "0.0";
                    }
                    if (swyjs.toString().equals("Infinity")
                            || swyjs.toString().equals("-Infinity")
                            || swyjs.toString().equals("NaN")) {
                        swyjs = "0.0";
                    }
                    System.out.println(swysd + "," + cwysd + "," + swyjs + ","
                            + cwyjs);

                    return putData(xx, yy, hh, x1, y1, h1, angel, swy, cwy, swysd,
                            cwysd, swyjs, cwyjs, aDatetime, p.get("sensor_no").toString(), degree);


                }
            }
        } else if (listDay.size() == 1) {
            if (true) {
                listDay.add(p);
                for (int i = 1; i < listDay.size(); i++) {
                    map1 = listDay.get(i);
                    aDatetime = map1.get("time").toString();
                    xx = Double.valueOf(map1.get("ax").toString());// 坐标值（m）
                    yy = Double.valueOf(map1.get("ay").toString());// 坐标值（m）
                    hh = Double.valueOf(map1.get("ah").toString()); // 坐标值（m）
                    x1 = Double.valueOf(map1.get("X").toString()); // 偏移量（mm）
                    y1 = Double.valueOf(map1.get("Y").toString()); // 偏移量（mm）
                    h1 = Double.valueOf(map1.get("H").toString()); // 偏移量（mm）
                    angel = getJD(y1, x1); // 偏移角度
                    // degree = getDegree(yy, xx);
                    degree = getDegree(y1, x1);
                    swy = getWY(x1, y1); // 水平位移（mm）
                    cwy = getWY(h1, 0.0); // 垂直位移（mm）
                    if (i == 1) {
                        Long time1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 1).get("time") + "").getTime();
                        Long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i).get("time") + "").getTime();
                        map2 = listDay.get(i - 1);
                        // 上一个点数据
                        if (map2.get("ah") != null
                                && !"".equals(map2.get("ah"))) {
                            hh1 = Double.valueOf(map2.get("ah").toString());
                        }
                        if (map2.get("ax") != null
                                && !"".equals(map2.get("ax"))) {
                            xx1 = Double.valueOf(map2.get("ax").toString());
                        }
                        if (map2.get("ay") != null
                                && !"".equals(map2.get("ay"))) {
                            yy1 = Double.valueOf(map2.get("ay").toString());
                        }
                        Double lspeed = getSuLvL(xx, xx1, yy, yy1, time1, time);
                        Double hspeed = getSuLvH(hh, hh1, time1, time);
                        swysd = String.format("%.2f", lspeed);// 水平速率
                        cwysd = String.format("%.2f", hspeed);// 垂直速率

                        swyjs = String.format("%.2f", lspeed
                                / ((time - time1) / (1000.0 * 60 * 60)));// 水平加速度
                        cwyjs = String.format("%.2f", hspeed
                                / ((time - time1) / (1000.0 * 60 * 60)));// 垂直加速度

                        if (cwyjs.toString().equals("Infinity")
                                || cwyjs.toString().equals("-Infinity")
                                || cwyjs.toString().equals("NaN")) {
                            cwyjs = "0.0";
                        }
                        if (swyjs.toString().equals("Infinity")
                                || swyjs.toString().equals("-Infinity")
                                || swyjs.toString().equals("NaN")) {
                            swyjs = "0.0";
                        }
                        System.out.println(swysd + "," + cwysd + "," + swyjs
                                + "," + cwyjs);
                    } else {
                        map2 = listDay.get(i - 1);// 上一个点
                        map3 = listDay.get(i - 2);// 上两个点
                        Long time1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 1).get("time") + "").getTime();
                        Long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i).get("time") + "").getTime();
                        Long time3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 2).get("time") + "").getTime();
                        // 上一个点数据
                        if (map2.get("ah") != null
                                && !"".equals(map2.get("ah"))) {
                            hh1 = Double.valueOf(map2.get("ah").toString());
                        }
                        if (map2.get("ax") != null
                                && !"".equals(map2.get("ax"))) {
                            xx1 = Double.valueOf(map2.get("ax").toString());
                        }
                        if (map2.get("ay") != null
                                && !"".equals(map2.get("ay"))) {
                            yy1 = Double.valueOf(map2.get("ay").toString());
                        }
                        // 上两个点数据
                        if (map3.get("ah") != null
                                && !"".equals(map3.get("ah"))) {
                            hh2 = Double.valueOf(map3.get("ah").toString());
                        }
                        if (map3.get("ax") != null
                                && !"".equals(map3.get("ax"))) {
                            xx2 = Double.valueOf(map3.get("ax").toString());
                        }
                        if (map3.get("ay") != null
                                && !"".equals(map3.get("ay"))) {
                            yy2 = Double.valueOf(map3.get("ay").toString());
                        }
                        Double lspeed = getSuLvL(xx, xx1, yy, yy1, time1, time);
                        Double hspeed = getSuLvH(hh, hh1, time1, time);
                        Double lspeed1 = getSuLvL(xx1, xx2, yy1, yy2, time3,
                                time1);
                        Double hspeed1 = getSuLvH(hh1, hh2, time3, time1);
                        swysd = String.format("%.5f", lspeed);// 水平速率
                        cwysd = String.format("%.5f", hspeed);// 垂直速率
                        swyjs = String.format("%.5f", (lspeed - lspeed1)
                                / ((time - time1) / (1000.0 * 60 * 60)));// 水平加速度
                        cwyjs = String.format("%.5f", (hspeed - hspeed1)
                                / ((time - time1) / (1000.0 * 60 * 60)));// 垂直加速度
                        if (cwyjs.toString().equals("Infinity")
                                || cwyjs.toString().equals("-Infinity")
                                || cwyjs.toString().equals("NaN")) {
                            cwyjs = "0.0";
                        }
                        if (swyjs.toString().equals("Infinity")
                                || swyjs.toString().equals("-Infinity")
                                || swyjs.toString().equals("NaN")) {
                            swyjs = "0.0";
                        }
                        System.out.println(swysd + "," + cwysd + "," + swyjs
                                + "," + cwyjs);
                    }

                    return putData(xx, yy, hh, x1, y1, h1, angel, swy, cwy, swysd,
                            cwysd, swyjs, cwyjs, aDatetime, p.get("sensor_no").toString(), degree);

                }
            }
        } else {
            if (true) {
                listDay.add(p);
                for (int i = 0; i < listDay.size(); i++) {
                    map1 = listDay.get(i);
                    aDatetime = map1.get("time").toString();
                    xx = Double.valueOf(map1.get("ax").toString());// 坐标值（m）
                    yy = Double.valueOf(map1.get("ay").toString());// 坐标值（m）
                    hh = Double.valueOf(map1.get("ah").toString()); // 坐标值（m）
                    x1 = Double.valueOf(map1.get("X").toString()); // 偏移量（mm）
                    y1 = Double.valueOf(map1.get("Y").toString()); // 偏移量（mm）
                    h1 = Double.valueOf(map1.get("H").toString()); // 偏移量（mm）
                    angel = getJD(y1, x1); // 偏移角度
                    // degree = getDegree(yy, xx);
                    degree = getDegree(y1, x1);
                    swy = getWY(x1, y1); // 水平位移（mm）
                    cwy = getWY(h1, 0.0); // 垂直位移（mm）
                    if (i == 0) {
                        swysd = "0.0"; // 水平位移速度（mm/h）
                        cwysd = "0.0"; // 垂直位移速度（mm/h）
                        swyjs = "0.0"; // 水平位移加速度（mm/h2）
                        cwyjs = "0.0"; // 垂直位移加速度（mm/h2）
                    } else if (i == 1) {
                        Long time1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 1).get("time") + "").getTime();
                        Long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i).get("time") + "").getTime();
                        map2 = listDay.get(i - 1);
                        // 上一个点数据
                        if (map2.get("ah") != null
                                && !"".equals(map2.get("ah"))) {
                            hh1 = (Double) map2.get("ah");
                        }
                        if (map2.get("ax") != null
                                && !"".equals(map2.get("ax"))) {
                            xx1 = (Double) map2.get("ax");
                        }
                        if (map2.get("ay") != null
                                && !"".equals(map2.get("ay"))) {
                            yy1 = (Double) map2.get("ay");
                        }
                        Double lspeed = getSuLvL(xx, xx1, yy, yy1, time1, time);
                        Double hspeed = getSuLvH(hh, hh1, time1, time);
                        swysd = String.format("%.5f", lspeed);// 水平速率
                        cwysd = String.format("%.5f", hspeed);// 垂直速率
                        swyjs = String.format("%.5f", lspeed
                                / ((time - time1) / (1000.0 * 60 * 60)));// 水平加速度
                        cwyjs = String.format("%.5f", hspeed
                                / ((time - time1) / (1000.0 * 60 * 60)));// 垂直加速度
                        if (cwyjs.toString().equals("Infinity")
                                || cwyjs.toString().equals("-Infinity")
                                || cwyjs.toString().equals("NaN")) {
                            cwyjs = "0.0";
                        }
                        if (swyjs.toString().equals("Infinity")
                                || swyjs.toString().equals("-Infinity")
                                || swyjs.toString().equals("NaN")) {
                            swyjs = "0.0";
                        }
                        System.out.println(swysd + "," + cwysd + "," + swyjs
                                + "," + cwyjs);
                    } else {
                        map2 = listDay.get(i - 1);// 上一个点
                        map3 = listDay.get(i - 2);// 上两个点
                        Long time1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 1).get("time") + "").getTime();
                        Long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i).get("time") + "").getTime();
                        Long time3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(listDay.get(i - 2).get("time") + "").getTime();
                        // 上一个点数据
                        if (map2.get("ah") != null
                                && !"".equals(map2.get("ah"))) {
                            hh1 = (Double) map2.get("ah");
                        }
                        if (map2.get("ax") != null
                                && !"".equals(map2.get("ax"))) {
                            xx1 = (Double) map2.get("ax");
                        }
                        if (map2.get("ay") != null
                                && !"".equals(map2.get("ay"))) {
                            yy1 = (Double) map2.get("ay");
                        }
                        // 上两个点数据
                        if (map3.get("ah") != null
                                && !"".equals(map3.get("ah"))) {
                            hh2 = (Double) map3.get("ah");
                        }
                        if (map3.get("ax") != null
                                && !"".equals(map3.get("ax"))) {
                            xx2 = (Double) map3.get("ax");
                        }
                        if (map3.get("ay") != null
                                && !"".equals(map3.get("ay"))) {
                            yy2 = (Double) map3.get("ay");
                        }
                        Double lspeed = getSuLvL(xx, xx1, yy, yy1, time1, time);
                        Double hspeed = getSuLvH(hh, hh1, time1, time);
                        Double lspeed1 = getSuLvL(xx1, xx2, yy1, yy2, time3,
                                time1);
                        Double hspeed1 = getSuLvH(hh1, hh2, time3, time1);
                        swysd = String.format("%.5f", lspeed);// 水平速率
                        cwysd = String.format("%.5f", hspeed);// 垂直速率
                        swyjs = String.format("%.5f", (lspeed - lspeed1)
                                / ((time - time1) / (1000.0 * 60 * 60)));// 水平加速度
                        cwyjs = String.format("%.5f", (hspeed - hspeed1)
                                / ((time - time1) / (1000.0 * 60 * 60)));// 垂直加速度
                        if (cwyjs.toString().equals("Infinity")
                                || cwyjs.toString().equals("-Infinity")
                                || cwyjs.toString().equals("NaN")) {
                            cwyjs = "0.0";
                        }
                        if (swyjs.toString().equals("Infinity")
                                || swyjs.toString().equals("-Infinity")
                                || swyjs.toString().equals("NaN")) {
                            swyjs = "0.0";
                        }
                        System.out.println(swysd + "," + cwysd + "," + swyjs
                                + "," + cwyjs);
                    }
                    return putData(xx, yy, hh, x1, y1, h1, angel, swy, cwy, swysd,
                            cwysd, swyjs, cwyjs, aDatetime, p.get("sensor_no").toString(), degree);

                }
            }
        }
        return null;
    }

    /**
     * 获取偏移方向
     *
     * @return
     */
    private String getJD(Double ay, Double ax) {
        ay = new BigDecimal(ay.toString()).doubleValue();
        ax = new BigDecimal(ax.toString()).doubleValue();
        double rotateAngel = Math.atan2(ay, ax) * 180 / (Math.PI);
        Double angle = Double.parseDouble(String.format("%.3f", rotateAngel));
        String directionStr = "";
        if (rotateAngel > 0 && rotateAngel < 90) {
            directionStr = "北偏东" + angle + "度";
        } else if (rotateAngel > -90 && rotateAngel < 0) {
            angle = Double.parseDouble(String.format("%.3f", (0 - angle)));
            directionStr = "北偏西" + angle + "度";
        } else if (rotateAngel > 90 && rotateAngel < 180) {
            angle = Double.parseDouble(String.format("%.3f", (180 - angle)));
            directionStr = "南偏东" + angle + "度";
        } else if (rotateAngel > -180 && rotateAngel < -90) {
            angle = Double.parseDouble(String.format("%.3f", (180 + angle)));
            directionStr = "南偏西" + angle + "度";
        } else if (rotateAngel == 0) {
            directionStr = "正北方向";
        } else if (rotateAngel == 90) {
            directionStr = "正东方向";
        } else if (rotateAngel == -90) {
            directionStr = "正西方向";
        } else if (rotateAngel == 180 || rotateAngel == -180) {
            directionStr = "正南方向";
        }
        return directionStr;
    }

    /**
     * 偏移角度
     *
     * @param ay
     * @param ax
     * @return
     */
    private String getDegree(Double ay, Double ax) {
        ay = new BigDecimal(ay.toString()).doubleValue();
        ax = new BigDecimal(ax.toString()).doubleValue();
        double rotateAngel = Math.atan2(ay, ax) * 180 / (Math.PI);
        Double angle = Double.parseDouble(String.format("%.3f", rotateAngel));
        return angle + "";
    }

    /**
     * 位移算法
     */
    private Double getWY(Double x, Double y) {
        return Math.sqrt(x * x + y * y);
    }

    /**
     * 水平位移速度(mm/h)
     *
     * @param x     当前x数值
     * @param x1    上一点x数值
     * @param y     当前y数值
     * @param y1    上一点y数值
     * @param time1 上一点时间 (ms)
     * @param time2 当前点时间 (ms)
     * @return
     */
    private Double getSuLvL(Double x, Double x1, Double y, Double y1,
                            Long time1, Long time2) {

        Double time = (time2 - time1) / (1000.0 * 60 * 60);
        Double vx = ((x - x1) * 1000) / time;
        Double vy = ((y - y1) * 1000) / time;
        Double vs = (Math.sqrt(vx * vx + vy * vy));
        if (vs.toString().equals("Infinity")
                || vs.toString().equals("-Infinity")
                || vs.toString().equals("NaN")) {
            vs = 0.0;
        }
        return vs;
    }

    /**
     * 垂直方向速度(mm/h)
     *
     * @param h     当前h数值
     * @param h1    上一点h数值
     * @param time1 上一点时间
     * @param time2 当前点时间
     * @return
     */
    private Double getSuLvH(Double h, Double h1, Long time1, Long time2) {
        Double time = (time2 - time1) / (1000.0 * 60 * 60);
        Double num = ((h - h1) * 1000) / time;
        if (num.toString().equals("Infinity")
                || num.toString().equals("-Infinity")
                || num.toString().equals("NaN")) {
            num = 0.0;
        }
        return num;
    }

    private Map<String, String> putData(Double xx, Double yy, Double hh, Double x1, Double y1,
                                        Double h1, String angel, Double swy, Double cwy, String swysd,
                                        String cwysd, String swyjs, String cwyjs, String aDatetime,
                                        String tablename, String degree) {

        Map<String, String> datamap = new HashMap<>(16);
        datamap.put("x", xx.toString());
        datamap.put("y", yy.toString());
        datamap.put("z", hh.toString());
        datamap.put("x1", x1.toString());
        datamap.put("y1", y1.toString());
        datamap.put("z1", h1.toString());
        datamap.put("angel", angel);
        datamap.put("swy", swy.toString());
        datamap.put("cwy", cwy.toString());
        datamap.put("swysd", swysd);
        datamap.put("cwysd", cwysd);
        datamap.put("swyjs", swyjs);
        datamap.put("cwyjs", cwyjs);
        datamap.put("time", aDatetime);
        datamap.put("degree", degree);
        datamap.put("sensor_no", tablename);

        return datamap;
    }

}
