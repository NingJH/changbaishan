package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TbsLxdbdMapper;
import com.cqndt.disaster.device.domain.TbsLxdbd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-08 17:10
 **/
@Service
public class DbwyLxsMessageConvert implements DeviceMessageConverter{
    @Autowired
    TbsLxdbdMapper tbsLxdbdMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 17;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TbsLxdbd tbsLxdbd = new TbsLxdbd();
        tbsLxdbd.setTime(acqTime);
        tbsLxdbd.setSensorNo(map.get("number"));
        tbsLxdbd.setLjwy(BigDecimal.valueOf(Double.valueOf(map.get("ljwy").equals(" ")?"0":map.get("ljwy"))));
        tbsLxdbd.setXlwy(BigDecimal.valueOf(Double.valueOf(map.get("xlwy").equals(" ")?"0":map.get("xlwy"))));
        tbsLxdbd.setSd(BigDecimal.valueOf(Double.valueOf(map.get("sd").equals(" ")?"0":map.get("sd"))));
        tbsLxdbd.setJsd(BigDecimal.valueOf(Double.valueOf(map.get("jsd").equals(" ")?"0":map.get("jsd"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsLxdbd;
    }
}
