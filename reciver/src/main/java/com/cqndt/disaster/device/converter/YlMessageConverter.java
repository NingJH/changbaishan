package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.*;
import com.cqndt.disaster.device.dao.warn.WarnPlanMapper;
import com.cqndt.disaster.device.domain.TabAlarmInfo;
import com.cqndt.disaster.device.domain.TbsYl;
import com.cqndt.disaster.device.domain.WarnPlan;
import com.cqndt.disaster.device.vo.Data;
import com.cqndt.disaster.device.websocket.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @author Frog
 */
@Service
public class YlMessageConverter implements DeviceMessageConverter {

    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private WebSocketServer webSocketServer;
    @Autowired
    private TabWarnSettingMapper tabWarnSettingMapper;
    @Autowired
    private TabMonitorMapper tabMonitorMapper;
    @Autowired
    private TabAlarmInfoMapper tabAlarmInfoMapper;
    @Autowired
    private TabUnionMapper tuMapper;
    @Autowired
    private TbsYlMapper tbsYlMapper;

    @Autowired
    WarnPlanMapper warnPlanMapper;

    @Override
    public Integer getDeviceType() {
        return 10;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        String sensorNo = map.get("number");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = sdf.parse(map.get("acqTime"));
        String MonitorNo = tabMonitorMapper.selectMNoBySeonsorNo(sensorNo);

        TbsYl tbsYl = new TbsYl();
        tbsYl.setSensorNo(sensorNo);
        tbsYl.setMonitorNo(MonitorNo);
        tbsYl.setTime(acqTime);
        tbsYl.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value").equals(" ")?"0":map.get("value"))));
//        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
//        vo.putAll(sensorNo, map);

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));

        //告警推送判断
        WarnPlan ylWarnPlan = new WarnPlan("yl", tbsYl.getValue(), tbsYl.getSensorNo(), 4);
        warnPlanMapper.insertWarnPlan(ylWarnPlan);
//        warnPush(map);
        //联合告警转存
        tuMapper.insert(sensorNo, Double.valueOf(map.get("value")),sensorNo);

        return tbsYl;
    }

    public void warnPush(Map<String, String> map) {
        String sensorNo = map.get("sensor_no");
        Map<String, BigDecimal> alarmMap = tabWarnSettingMapper.selectAlarm(sensorNo);
        String time = map.get("time");
        Date warnTime = null;
        try {
            warnTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Double value = Double.valueOf(map.get("value"));
        int level = getAlarmLevel(alarmMap, value);

        if (level > 0) {
            int monitorId = tabMonitorMapper.selectIdBySeonsorNo(sensorNo);
            map.put("level", String.valueOf(level));
            map.put("monitorId", String.valueOf(monitorId));
            String s = JSON.toJSONString(map);
            try {
                webSocketServer.sendMsgToAll(s, "10");
                insertAlarm(sensorNo, warnTime, level, value, monitorId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private int getAlarmLevel(Map<String, BigDecimal> alarmMap, Double value) {
        Double cuAlarm1;
        Double cuAlarm2;
        Double cuAlarm3;
        Double cuAlarm4;
        try {
            cuAlarm1 = Double.valueOf(alarmMap.get("cu_alarm1").toString());
            cuAlarm2 = Double.valueOf(alarmMap.get("cu_alarm2").toString());
            cuAlarm3 = Double.valueOf(alarmMap.get("cu_alarm3").toString());
            cuAlarm4 = Double.valueOf(alarmMap.get("cu_alarm4").toString());
        } catch (Exception e) {
            return 0;
        }

        return value > cuAlarm1 ? 1 : (value > cuAlarm2 ? 2 : (value > cuAlarm3 ? 3 : (value > cuAlarm4 ? 4 : 0)));
    }

    private void insertAlarm(String sensorNo, Date warnTime, int level, Double value, int monitorId) {
        TabAlarmInfo tabAlarmInfo = new TabAlarmInfo();
        tabAlarmInfo.setSensorNo(sensorNo);
        tabAlarmInfo.setMonitoringId(monitorId);
        tabAlarmInfo.setTime(warnTime);
        tabAlarmInfo.setValue(value);
        tabAlarmInfo.setLevel(level);
        tabAlarmInfo.setAlarmType(1);
        tabAlarmInfoMapper.insertSelective(tabAlarmInfo);
    }
}
