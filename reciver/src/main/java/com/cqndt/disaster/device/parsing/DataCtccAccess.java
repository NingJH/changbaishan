package com.cqndt.disaster.device.parsing;

import com.cqndt.disaster.device.common.util.GetRequestJsonUtils;
import com.cqndt.disaster.device.service.DataCtccService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


/**
 * @Description: 中国电信数据接收
 * @Auther: lhl
 * @Date: 2019/4/29 15:59
 */
@Controller
public class DataCtccAccess {

    @Autowired
    private DataCtccService dataCtccService;

    @PostMapping(value = "ctcc")
    @ResponseStatus(HttpStatus.OK)
    public void receive(HttpServletRequest request) {
        String requestJsonString = null;
        try {
            requestJsonString = GetRequestJsonUtils.getRequestJsonString(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("ctcc："+requestJsonString);
        dataCtccService.dataProcessing(requestJsonString);
    }


}
