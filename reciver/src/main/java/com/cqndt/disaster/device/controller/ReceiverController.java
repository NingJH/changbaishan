package com.cqndt.disaster.device.controller;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.*;
import com.cqndt.disaster.device.domain.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author qingqinchao
 */
@Slf4j
@RestController
public class ReceiverController {
    @Autowired
    private TbsLfwyMapper tbsLfwyMapper;
    @Autowired
    private TbsQjyWbxMapper tbsQjyWbxMapper;
    @Autowired
    private TbsDbbxHpMapper tbsDbbxHpMapper;
    @Autowired
    private TbsGnssMapper tbsGnssMapper;
    @Autowired
    private TbsSbwyQxyMapper tbsSbwyQxyMapper;
    @Autowired
    private TbsChenjiangMapper tbsChenjiangMapper;
    @Autowired
    private TbsKongxishuiyaMapper tbsKongxishuiyaMapper;
    @Autowired
    private TbsGangjinjiMapper tbsGangjinjiMapper;
    @Autowired
    private TbsTylMapper tbsTylMapper;
    @Autowired
    private TbsYlMapper tbsYlMapper;
    @Autowired
    private TbsHslMapper tbsHslMapper;
    @Autowired
    private TbsShuiweijiMapper tbsShuiweijiMapper;
    @Autowired
    private TbsYingbianMapper tbsYingbianMapper;
    @Autowired
    private TbsWenshiduMapper tbsWenshiduMapper;
    @Autowired
    private TbsNiweijiMapper tbsNiweijiMapper;
    @Autowired
    private TbsZhendongMapper tbsZhendongMapper;
    @Autowired
    private TabDeviceMapper tabDeviceMapper;
    @Autowired
    private TabSensorMapper tabSensorMapper;
    @Autowired
    private TabOnlineMapper tabOnlineMapper;
    @Autowired
    private TabMonitorMapper tabMonitorMapper;
    @Autowired
    private TabProjectMapper tabProjectMapper;
    @Autowired
    private TabAlarmInfoMapper tabAlarmInfoMapper;
    @Autowired
    private TabMonitorDeviceMapper tabMonitorDeviceMapper;

    @PostMapping(value = "receiverMonitoringData")
    public void receiverMonitoringData(String data) {
        List<Map<String, String>> mapList = (List<Map<String, String>>) JSONArray.parse(data);
        for (Map<String, String> map : mapList) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date acqTime = null;
            Integer id = Integer.valueOf(map.get("id"));
            String monitoringId = map.get("monitoringId");
            try {
                acqTime = sdf.parse(map.get("acqTime"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int type = Integer.valueOf(map.get("type"));
            switch (type) {
                case 1:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsLfwy tbsLfwy = new TbsLfwy();
                    tbsLfwy.setId(id);
                    tbsLfwy.setMonitorNo(monitoringId);
                    tbsLfwy.setTime(acqTime);
                    tbsLfwy.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value"))));
                    tbsLfwy.setTemp(BigDecimal.valueOf(Double.valueOf(map.get("temperature"))));
                    tbsLfwyMapper.insertSelective(tbsLfwy);
                    break;
                case 2:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsQjyWbx tbsQjyWbx = new TbsQjyWbx();
                    tbsQjyWbx.setId(id);
                    tbsQjyWbx.setMonitorNo(monitoringId);
                    tbsQjyWbx.setTime(acqTime);
                    tbsQjyWbx.setQxjd(BigDecimal.valueOf(Double.valueOf(map.get("value"))));
                    tbsQjyWbx.setTemp(BigDecimal.valueOf(Double.valueOf(map.get("temperature"))));
                    tbsQjyWbxMapper.insertSelective(tbsQjyWbx);
                    break;
                case 3:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsDbbxHp tbsDbbxHp = new TbsDbbxHp();
                    tbsDbbxHp.setId(id);
                    tbsDbbxHp.setMonitorNo(monitoringId);
                    tbsDbbxHp.setTime(acqTime);
                    tbsDbbxHp.setWyl(BigDecimal.valueOf(Double.valueOf(map.get("value"))));
                    tbsDbbxHp.setX(BigDecimal.valueOf(Double.valueOf(map.get("x"))));
                    tbsDbbxHp.setY(BigDecimal.valueOf(Double.valueOf(map.get("y"))));
                    tbsDbbxHp.setZ(BigDecimal.valueOf(Double.valueOf(map.get("z"))));
                    tbsDbbxHp.setTemp(BigDecimal.valueOf(Double.valueOf(map.get("temperature"))));
                    tbsDbbxHpMapper.insertSelective(tbsDbbxHp);
                    break;
                case 4:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsGnss tbsGnss = new TbsGnss();
                    tbsGnss.setId(id);
                    tbsGnss.setMonitorNo(monitoringId);
                    tbsGnss.setTime(acqTime);
                    tbsGnss.setX(BigDecimal.valueOf(Double.valueOf(map.get("x"))));
                    tbsGnss.setY(BigDecimal.valueOf(Double.valueOf(map.get("y"))));
                    tbsGnss.setZ(BigDecimal.valueOf(Double.valueOf(map.get("h"))));
                    tbsGnssMapper.insertSelective(tbsGnss);
                    break;
                case 5:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsSbwyQxy tbsSbwyQxy = new TbsSbwyQxy();
                    tbsSbwyQxy.setId(id);
                    tbsSbwyQxy.setMonitorNo(monitoringId);
                    tbsSbwyQxy.setTime(acqTime);
                    tbsSbwyQxy.setxJd(BigDecimal.valueOf(Double.valueOf(map.get("x"))));
                    tbsSbwyQxy.setyJd(BigDecimal.valueOf(Double.valueOf(map.get("y"))));
                    tbsSbwyQxy.setzJd(BigDecimal.valueOf(Double.valueOf(map.get("z"))));
//                    tbsSbwyQxy.setTemp(BigDecimal.valueOf(Double.valueOf(map.get("temperature"))));
                    tbsSbwyQxyMapper.insertSelective(tbsSbwyQxy);
                    break;
                case 6:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsChenjiang tbsChenjiang = new TbsChenjiang();
                    tbsChenjiang.setId(id);
                    tbsChenjiang.setMonitorNo(monitoringId);
                    tbsChenjiang.setTime(acqTime);
                    tbsChenjiang.setX(BigDecimal.valueOf(Double.valueOf(map.get("x"))));
                    tbsChenjiang.setY(BigDecimal.valueOf(Double.valueOf(map.get("y"))));
                    tbsChenjiang.setZ(BigDecimal.valueOf(Double.valueOf(map.get("z"))));
                    tbsChenjiang.setTemp(BigDecimal.valueOf(Double.valueOf(map.get("temperature"))));
                    tbsChenjiangMapper.insertSelective(tbsChenjiang);
                    break;
                case 7:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsKongxishuiya tbsKongxishuiya = new TbsKongxishuiya();
                    tbsKongxishuiya.setId(id);
                    tbsKongxishuiya.setMonitorNo(monitoringId);
                    tbsKongxishuiya.setTime(acqTime);
                    tbsKongxishuiya.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value"))));
                    tbsKongxishuiyaMapper.insertSelective(tbsKongxishuiya);
                    break;
                case 8:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsGangjinji tbsGangjinji = new TbsGangjinji();
                    tbsGangjinji.setId(id);
                    tbsGangjinji.setMonitorNo(monitoringId);
                    tbsGangjinji.setTime(acqTime);
                    tbsGangjinji.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value"))));
                    tbsGangjinjiMapper.insertSelective(tbsGangjinji);
                    break;
                case 9:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsTyl tbsTyl = new TbsTyl();
                    tbsTyl.setId(id);
                    tbsTyl.setMonitorNo(monitoringId);
                    tbsTyl.setTime(acqTime);
                    tbsTyl.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value"))));
                    tbsTylMapper.insertSelective(tbsTyl);
                    break;
                case 10:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsYl tbsYl = new TbsYl();
                    tbsYl.setId(id);
                    tbsYl.setMonitorNo(monitoringId);
                    tbsYl.setTime(acqTime);
                    tbsYl.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value"))));
                    tbsYlMapper.insertSelective(tbsYl);
                    break;
                case 11:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsHsl tbsHsl = new TbsHsl();
                    tbsHsl.setId(id);
                    tbsHsl.setMonitorNo(monitoringId);
                    tbsHsl.setTime(acqTime);
                    tbsHsl.setHsl(BigDecimal.valueOf(Double.valueOf(map.get("value"))));
                    tbsHslMapper.insertSelective(tbsHsl);
                    break;
                case 12:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsShuiweiji tbsShuiweiji = new TbsShuiweiji();
                    tbsShuiweiji.setId(id);
                    tbsShuiweiji.setMonitorNo(monitoringId);
                    tbsShuiweiji.setTime(acqTime);
                    tbsShuiweiji.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value"))));
                    tbsShuiweijiMapper.insertSelective(tbsShuiweiji);
                    break;
                case 13:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsYingbian tbsYingbian = new TbsYingbian();
                    tbsYingbian.setId(id);
                    tbsYingbian.setMonitorNo(monitoringId);
                    tbsYingbian.setTime(acqTime);
                    tbsYingbian.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value"))));
                    tbsYingbianMapper.insertSelective(tbsYingbian);
                    break;
                case 14:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsWenshidu tbsWenshidu = new TbsWenshidu();
                    tbsWenshidu.setId(id);
                    tbsWenshidu.setMonitorNo(monitoringId);
                    tbsWenshidu.setTime(acqTime);
                    tbsWenshidu.setHumidity(BigDecimal.valueOf(Double.valueOf(map.get("humidity"))));
                    tbsWenshidu.setTemp(BigDecimal.valueOf(Double.valueOf(map.get("temperature"))));
                    tbsWenshiduMapper.insertSelective(tbsWenshidu);
                    break;
                case 15:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsNiweiji tbsNiweiji = new TbsNiweiji();
                    tbsNiweiji.setId(id);
                    tbsNiweiji.setMonitorNo(monitoringId);
                    tbsNiweiji.setTime(acqTime);
                    tbsNiweiji.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value"))));
                    tbsNiweijiMapper.insertSelective(tbsNiweiji);
                    break;
                case 16:
                    insertSDMInfo(id, monitoringId, type, acqTime);
                    TbsZhendong tbsZhendong = new TbsZhendong();
                    tbsZhendong.setId(id);
                    tbsZhendong.setMonitorNo(monitoringId);
                    tbsZhendong.setTime(acqTime);
                    tbsZhendong.setZf(BigDecimal.valueOf(Double.valueOf(map.get("amplitude"))));
                    tbsZhendong.setPl(BigDecimal.valueOf(Double.valueOf(map.get("frequency"))));
                    tbsZhendongMapper.insertSelective(tbsZhendong);
                    break;
                default:
                    break;
            }

        }
    }

    private void insertSDMInfo(Integer id, String monitoringId, int type, Date acqTime) {
        int i;
        int j;
        int k;
        int m;
        String deviceNo;
        String sensorNo;

        TabDevice tabDevice = new TabDevice();
        TabSensor tabSensor = new TabSensor();
        TabOnline tabOnline = new TabOnline();
        TabMonitorDevice tabMonitorDevice = new TabMonitorDevice();
        //创建设备
        deviceNo = String.valueOf(Math.abs(("设备" + monitoringId).hashCode()));
        i = tabDeviceMapper.selectDeviceNo(deviceNo);
        if (i < 1) {
            tabDevice.setDeviceNo(deviceNo);
            tabDeviceMapper.insert(tabDevice);
        }
        //创建设备传感器关联
        sensorNo = String.valueOf(Math.abs(("传感器" + monitoringId + id).hashCode()));
        j = tabSensorMapper.selectDSNo(deviceNo, sensorNo);
        if (j < 1) {
            tabSensor.setDeviceNo(deviceNo);
            tabSensor.setSensorNo(sensorNo);
            tabSensor.setSensorType(String.valueOf(type));
            tabSensorMapper.insert(tabSensor);
        }
        //创建设备最后在线时间
        long l = acqTime.getTime() - System.currentTimeMillis();
        long h = l / 3600000;
        k = tabOnlineMapper.selectDeviceNo(deviceNo);
        if (k < 1) {
            tabOnline.setDeviceNo(deviceNo);
            tabOnline.setOfflineTime(BigDecimal.valueOf(h));
            tabOnlineMapper.insert(tabOnline);
        } else {
            tabOnlineMapper.updateByDeviceNo(deviceNo, BigDecimal.valueOf(h));
        }
        //新增监测点设备表关联关系
        String monitoringNo = tabMonitorMapper.selectNoById(Integer.valueOf(monitoringId)).toString();
        m = tabMonitorDeviceMapper.selectMDNo(monitoringNo, deviceNo);
        if (m < 0) {
            tabMonitorDevice.setMonitorNo(monitoringNo);
            tabMonitorDevice.setDeviceNo(deviceNo);
            tabMonitorDevice.setStartTime(acqTime);
            tabMonitorDeviceMapper.insertSelective(tabMonitorDevice);
        }
    }

    public void receiverMoitoringInfo(String data) {
        List<Map<String, String>> mapList = (List<Map<String, String>>) JSONArray.parse(data);
        for (Map<String, String> map : mapList) {
            TabMonitor tabMonitor = new TabMonitor();
            tabMonitor.setId(Integer.valueOf(map.get("id")));
            tabMonitor.setProjectId(Integer.valueOf(map.get("projectId")));
            tabMonitor.setMonitorName(map.get("name"));
            tabMonitor.setMonitorNo(map.get("number"));
            tabMonitor.setDeviceId(Integer.valueOf(map.get("device")));
            tabMonitor.setLongitude(BigDecimal.valueOf(Long.valueOf(map.get("lon"))));
            tabMonitor.setLatitude(BigDecimal.valueOf(Long.valueOf(map.get("lat"))));
            tabMonitorMapper.insertSelective(tabMonitor);
        }
    }

    public void receiverProjectInfo(String data) {
        List<Map<String, String>> mapList = (List<Map<String, String>>) JSONArray.parse(data);
        for (Map<String, String> map : mapList) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date saveTime = null;
            try {
                saveTime = sdf.parse(map.get("saveTime"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            TabProject tabProject = new TabProject();
            tabProject.setId(Integer.valueOf(map.get("id")));
            tabProject.setProjectNo(map.get("projectNo"));
            tabProject.setProjectName(map.get("projectName"));
            tabProject.setLongitude(BigDecimal.valueOf(Long.valueOf(map.get("lon"))));
            tabProject.setLatitude(BigDecimal.valueOf(Long.valueOf(map.get("lat"))));
            tabProject.setProjectAdd(map.get("address"));
            tabProject.setStartDate(saveTime);
            tabProject.setAreaId(Integer.valueOf(map.get("areaId")));
            tabProjectMapper.insertSelective(tabProject);
        }
    }

    public void receiverAlarmInfo(String data) {
        List<Map<String, String>> mapList = (List<Map<String, String>>) JSONArray.parse(data);
        for (Map<String, String> map : mapList) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date time = null;
            try {
                time = sdf.parse(map.get("saveTime"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            TabAlarmInfo tabAlarmInfo = new TabAlarmInfo();
            tabAlarmInfo.setId(Integer.valueOf(map.get("id")));
            tabAlarmInfo.setMonitoringId(Integer.valueOf(map.get("monitoringId")));
            tabAlarmInfo.setAlarmType(Integer.valueOf(map.get("alarmType")));
            tabAlarmInfo.setValue(Double.valueOf(map.get("value")));
            tabAlarmInfo.setLevel(Integer.valueOf(map.get("level")));
            tabAlarmInfo.setTime(time);
            tabAlarmInfoMapper.insertSelective(tabAlarmInfo);
        }
    }

}
