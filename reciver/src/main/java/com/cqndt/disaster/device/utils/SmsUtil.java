package com.cqndt.disaster.device.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * 华唐短信发送方法
 *
 * @Author yin_q
 * @Date 2019/5/10 17:16
 * @Email yin_qingqin@163.com
 **/
public class SmsUtil {

    public static void main(String[] args) {
        String time = String.valueOf(System.currentTimeMillis());
        sendSms(time, "18623118113", time, "设备编号：1111，告警阀值：11，本次监测值：111，发送一级红色告警，请注意");
    }

    public static void sendSms(String batch, String mobile, String sendTime,
                               String contentx) {
        contentx = "【沙区住建委】" + contentx;
        StringBuffer parameters = new StringBuffer();
        parameters.append("corpID="+batch);
        parameters.append("&Pwd=test");
        parameters.append("&Mobile=" + mobile.toString());
        parameters.append("&Content=" + contentx);
        if (sendTime != null) {
            parameters.append("&SendTime=" + sendTime);
        }
        URL msgUrl = null;
        try {
            msgUrl = new URL("http://183.230.108.112:8089/gdsms/IssuedMeteorMessage.do");// 服务地址
        } catch (MalformedURLException e) {
            //LoggerUtils.error(TestMessage.class, "URL地址不合法", e);
        }
        BufferedReader in = null;
        try {
            HttpURLConnection connection = (HttpURLConnection) msgUrl.openConnection();
            connection.setDoOutput(true);// 设置连接可输出
            connection.setDoInput(true);// 设置连接可输入
            connection.setRequestMethod("POST");// 设置请求为POST
            connection.setUseCaches(false);// 忽略缓存
            connection.setInstanceFollowRedirects(false);// 否要遵循HTTP重定向。
            connection.setConnectTimeout(10000);// 设置连接超时
            connection.setReadTimeout(10000);// 设置读超时到指定的超时
            connection.connect();// 方法被调用时,该连接已经打开了
            OutputStream out = connection.getOutputStream();// 返回一个输出流,写入此连接。
            out.write(parameters.toString().getBytes("utf-8"));// 命名字符集,将结果储存到一个新的字节数组。
            out.flush();
            out.close();
            int code = connection.getResponseCode();// 它将返回分别在200和401返回-1HTTP/1.0 200 OK,HTTP/1.0 401
            System.out.println("手机号码：" + mobile + "状态码" + code);
            if (code != 200) {
                throw new RuntimeException("手机号码：" + mobile + " 远程短信服务端出现异常,状态码" + code);
            }
            in=new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));// connection.getInputStream()返回一个输入流读取从这个打开的连接。
            String inputline = in.readLine();
        } catch (IOException e) {
            //LoggerUtils.error(SendUtils.class, "网络异常", e);
        } finally {
            if(in!=null){
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
