package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TabMonitorMapper;
import com.cqndt.disaster.device.dao.TbsNiweijiMapper;
import com.cqndt.disaster.device.domain.TbsNiweiji;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Service
public class NiweijiMessageConverter implements DeviceMessageConverter{
    @Autowired
    TbsNiweijiMapper tbsNiweijiMapper;
    @Autowired
    private TabMonitorMapper tabMonitorMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 15;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String sensorNo = map.get("number");
        String MonitorNo = tabMonitorMapper.selectMNoBySeonsorNo(sensorNo);

        TbsNiweiji tbsNiweiji = new TbsNiweiji();
        tbsNiweiji.setSensorNo(sensorNo);
        tbsNiweiji.setTime(acqTime);
        tbsNiweiji.setMonitorNo(MonitorNo);
        tbsNiweiji.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value").equals(" ")?"0":map.get("value"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsNiweiji;
    }
}
