package com.cqndt.disaster.device.service;

/**
 * @Description: cmcc业务层
 * @Auther: lhl
 * @Date: 2019/4/30 09:15
 */
public interface DataCtccService {
    void dataProcessing(String string);
}
