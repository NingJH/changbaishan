package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TabMonitorMapper;
import com.cqndt.disaster.device.dao.TbsDxswMapper;
import com.cqndt.disaster.device.domain.TbsDxsw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Create by Intellij IDEA
 * 地下水位tbs_dxsw
 * @Time : 2019-08-09 09:28
 **/
@Service
public class DxswMessageConverter implements DeviceMessageConverter{

    @Autowired
    TbsDxswMapper tbsDxswMapper;
    @Autowired
    private TabMonitorMapper tabMonitorMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 18;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String sensorNo = map.get("number");
        String MonitorNo = tabMonitorMapper.selectMNoBySeonsorNo(sensorNo);

        TbsDxsw tbsDxsw = new TbsDxsw();
        tbsDxsw.setTime(acqTime);
        tbsDxsw.setSensorNo(sensorNo);
        tbsDxsw.setMonitorNo(MonitorNo);
        tbsDxsw.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value").equals(" ")?"0":map.get("value"))));
        if(tbsDxsw.getValue().doubleValue()>=0){
            BigDecimal value = new BigDecimal(tbsDxsw.getValue().doubleValue());
            double a = 0.0;//孔口高程-水位安装深度+水位计测量值 //现在用的：水位安装深度-水位计测量值
            switch (sensorNo){
                case "570038670214_1":
//                    a = 493.8693 - 140;
                    a = 140;
                    break;
                case "565353034214_1":
//                    a = 521.5827 - 110;
                    a = 110;
                    break;
                case "565043279214_1":
//                    a = 505.7187 - 90;
                    a = 90;
                    break;
                case "569837193214_1":
//                    a = 489.2646 - 90;
                    a = 90;
                    break;
                case "570209630214_1":
//                    a = 503.8366 - 120;
                    a = 120;
                    break;
                case "570191535214_1":
//                    a = 512.0418 - 87;
                    a = 87;
                    break;
                case "569353428214_1":
//                    a = 510.7169 - 90;
                    a = 90;
                    break;
                case "553688863214_1":
//                    a = 495.9687 - 100;
                    a = 100;
                    break;
                case "552767144214_1":
//                    a = 497.5756 - 110;
                    a = 110;
                    break;
                case "573188328214_1":
//                    a = 496.1258 - 120;
                    a = 120;
                    break;
                case "574111078214_1":
//                    a = 492.7095 - 77;
                    a = 77;
                    break;
                case "574014648214_1":
//                    a = 507.9238 - 85;
                    a = 85;
                    break;
            }
//            value = value.subtract(new BigDecimal(a));//+水位测量值
            value = new BigDecimal(a).subtract(value);//-水位测量值
            tbsDxsw.setValue(value);
        }

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsDxsw;
    }
}
