package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TbsWenshiduMapper;
import com.cqndt.disaster.device.domain.TbsWenshidu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Service
public class WenshiduMessageConverter implements DeviceMessageConverter{
    @Autowired
    TbsWenshiduMapper tbsWenshiduMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 14;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TbsWenshidu tbsWenshidu = new TbsWenshidu();
        tbsWenshidu.setTime(acqTime);
        tbsWenshidu.setSensorNo(map.get("number"));
        tbsWenshidu.setHumidity(BigDecimal.valueOf(Double.valueOf(map.get("humidity").equals(" ")?"0":map.get("humidity"))));
        tbsWenshidu.setTemp(BigDecimal.valueOf(Double.valueOf(map.get("temperature").equals(" ")?"0":map.get("temperature"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsWenshidu;
    }
}
