package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TbsZhendongMapper;
import com.cqndt.disaster.device.domain.TbsZhendong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Service
public class ZhendongMessageConverter implements DeviceMessageConverter {
    @Autowired
    TbsZhendongMapper tbsZhendongMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 16;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TbsZhendong tbsZhendong = new TbsZhendong();
        tbsZhendong.setTime(acqTime);
        tbsZhendong.setSensorNo(map.get("number"));
        tbsZhendong.setZf(BigDecimal.valueOf(Double.valueOf(map.get("amplitude").equals(" ")?"0":map.get("amplitude"))));
        tbsZhendong.setPl(BigDecimal.valueOf(Double.valueOf(map.get("frequency").equals(" ")?"0":map.get("frequency"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsZhendong;
    }
}
