package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TbsSbwyLxMapper;
import com.cqndt.disaster.device.domain.TbsSbwyLx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-09 14:58
 **/
@Service
public class SbwyLxMessageConverter implements DeviceMessageConverter{

    @Autowired
    TbsSbwyLxMapper tbsSbwyLxMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public Integer getDeviceType() {
        return 22;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TbsSbwyLx tbsSbwyLx = new TbsSbwyLx();
        tbsSbwyLx.setTime(acqTime);
        tbsSbwyLx.setSensorNo(map.get("number"));
        tbsSbwyLx.setCzgc(BigDecimal.valueOf(Double.valueOf(map.get("czgc").equals(" ")?"0":map.get("czgc"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsSbwyLx;
    }
}
