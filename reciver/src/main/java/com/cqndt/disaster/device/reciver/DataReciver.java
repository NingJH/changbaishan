package com.cqndt.disaster.device.reciver;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cqndt.disaster.device.converter.ConverterRegister;
import com.cqndt.disaster.device.converter.DeviceMessageConverter;
import com.cqndt.disaster.device.data.serivce.TbsDataReciverService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qingqinchao
 */
@Component
@Slf4j
public class DataReciver {
    //单次入库数据最大条数
    private final static int MAX_NUM = 2000;

    @Autowired
    private ConverterRegister converterRegister;

    @Autowired
    private TbsDataReciverService tbsDataReciverService;

    @RabbitListener(queuesToDeclare = {@Queue("yunnan.done.data")})
    public void getMessage(Message message) {
        System.out.println(message);

        byte[] body = message.getBody();
        String bod1y = String.valueOf(SerializationUtils.deserialize(body));
        String msg = new String(bod1y);
        System.out.println(msg);
        JSONArray array = JSONObject.parseArray(msg);
        List tbsData = new ArrayList();
        for(int i=0;i<array.size();i++ ){
            JSONObject object = array.getJSONObject(i);
            Integer type = object.getInteger("type");
            DeviceMessageConverter converter = converterRegister.getConverterByType(type);

            if(converter!=null){

                try {
                    Object entity = converter.messageConvert(object.toJSONString());
                    tbsData.add(entity);

                } catch (Exception e) {
                    //抛出异常是防止单个JSON转换引起的错误导致整个流程被强行结束
                    e.printStackTrace();
                }
            }
        }

        prepareSave(tbsData);
    }

    /**
     * 判断数据源是否超过2000条，如果超出分批入库
     * @param tbsData
     */
    private void prepareSave(List tbsData){

        int[][] c = getTemp(tbsData.size(), MAX_NUM);
        for(int i=0;i<c.length;i++){
            int temp[] = c[i];
           List subList = tbsData.subList(temp[0], temp[1]);
           tbsDataReciverService.saveBatch(subList);
        }

    }


    /**
     * 如果需要写入的数据库大于1000条，则分批量写入，比如1024条就要分为11次写入，前10次一次写入100条，最后一次写入24条
     * @param size 总共要写入的数据大小
     * @param maxNum 单次允许写入数据的大小
     * @return
     */
    private int[][] getTemp(int size, int maxNum){
        //计算循环次数
        int c = size/maxNum;
        // 余下的加起
        long d = size%maxNum;

        if(d>0){
            c+=1;
        }

        int start = 0;

        int tem[][] = new int[c][2];

        for(int i=0;i<c;i++){
            tem[i][0] = start;
            if(i==(c-1)){
                start+=d;
            }else{
                start+=maxNum;
            }
            tem[i][1] = start;
        }
        return tem;
    }

    public static void main(String[] args) {

         long aLong = 10 / 60L;
        System.out.println(aLong);


    }

}
