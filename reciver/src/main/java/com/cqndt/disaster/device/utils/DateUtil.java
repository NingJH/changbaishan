package com.cqndt.disaster.device.utils;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
	
	/**
	 * 将时间转化为   yyyy-MM-dd HH:mm:ss 字符串
	 * @param date
	 * @return
	 */
	public static String dateToString(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return(sdf.format(date));
	}

	/**
	 * 将时间字符串转化为  yyyy-MM-dd HH:mm:ss 时间
	 * @param pstrString
	 * @return
	 */
	public static Date stringToDate(String pstrString){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		Date toDate = null;
		try {
			toDate = sdf.parse(pstrString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return toDate;
	}
	
	/**
	 * 将时间字符串转化为  yyyy-MM-dd 时间
	 * @param pstrString
	 * @return
	 */
	public static Date shortStringToDate(String pstrString){
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Date toDate = null;
		try {
			toDate = sdf.parse(pstrString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return toDate;
	}

	/**
	 * 日期转时间--yyyy-MM
	 * @param pstrString
	 * @return
	 */
	public static Date shortStringToDateByYear(String pstrString){

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");

		Date toDate = null;
		try {
			toDate = sdf.parse(pstrString);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return toDate;
	}

	/**
	 * Date转LocalDate
	 * @param date
	 * @return
	 */
	public static LocalDate dateToLocalDate(Date date){
		Instant instant = date.toInstant();
		ZoneId zoneId = ZoneId.systemDefault();
		LocalDate toLocalDate = instant.atZone(zoneId).toLocalDate();
		return toLocalDate;
	}
	
	/**
	 * LocalDate转Date
	 * @param localDate
	 * @return
	 */
	public static Date localDateToDate(LocalDate localDate){
		ZoneId zoneid = ZoneId.systemDefault();
		ZonedDateTime zdt = localDate.atStartOfDay(zoneid);
		Date date = Date.from(zdt.toInstant());
		return date;
	}
	
	 /**
     * Date 转 LocalDateTime
     *
     * @param date
     * @return LocalDateTime
     */
    public static LocalDateTime dateToLocalDateTime(Date date) {
        long nanoOfSecond = (date.getTime() % 1000) * 1000000;
        LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(date.getTime() / 1000, (int) nanoOfSecond, ZoneOffset.of("+8"));
 
        return localDateTime;
    }
	
    /**
     * 得到N天后的日期
     *
     * @param theDate 某日期
     *                格式 yyyy-MM-dd
     * @param nDayNum N天
     * @return String N天后的日期
     */
    public static String afterNDaysDate(String theDate, Integer nDayNum) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        return LocalDateTime.parse(theDate,dateTimeFormatter)
                .plusDays(nDayNum)
                .format(dateTimeFormatter);
    }
    
    /**
     * 得到N天后的日期
     * @param localDate
     * @param nDayNum
     * @return
     */
    public static Date afterNDaysDate(LocalDate localDate, Integer nDayNum){
    	return localDateToDate(localDate.plusDays(nDayNum));
    }
    
    /**
     * 得到N天后的日期
     * @param date
     * @param nDayNum
     * @return
     */
    public static Date afterNDaysDate(Date date, Integer nDayNum){
    	return localDateToDate(dateToLocalDate(date).plusDays(nDayNum));
    }
    
    /**
     * 得到N天前的日期
     * @param date
     * @param nDayNum
     * @return
     */
    public static Date beforeNDaysDate(Date date, Integer nDayNum){
    	SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
    	Date d = null;
    	try {
    		d = formatter.parse(dateToString(date));
    		long t1 = d.getTime();
    		long t3=nDayNum-1;
    		long t2=t3 * 24 * 60 * 60 * 1000;
    		d.setTime(t1-t2);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
		return stringToDate(formatter.format(d));
    }

    /**
     * 计算两个时间LocalDateTime相差的月数，不考虑日期前后，返回结果>=0
     *
     * @param before
     * @param after
     * @return
     */
    public static int getAbsDiffMonth(LocalDate before, LocalDate after) {
        return Math.abs(Period.between(before, after).getMonths());
    }

    /**
     * 根据时间获取当月有多少天数
     *
     * @param date
     * @return
     */
    public static int getActualMaximum(Date date) {
        return dateToLocalDateTime(date).getMonth().length(dateToLocalDate(date).isLeapYear());
    }

    /**
     * 判断指定时间是否在时间范围内
     * @param time
     * @param startTime
     * @param endTime
     * @return
     */
    public static boolean isTimeInRange(Date time, Date startTime, Date endTime) {
        LocalDateTime now = dateToLocalDateTime(time);
        LocalDateTime start = dateToLocalDateTime(startTime);
        LocalDateTime end = dateToLocalDateTime(endTime);
        return (start.isBefore(now) && end.isAfter(now)) || start.isEqual(now) || end.isEqual(now);
    }

    /**
     * 计算两个时间LocalDate相差的天数，不考虑日期前后，返回结果>=0
     *
     * @param before
     * @param after
     * @return
     */
    public static int getAbsTimeDiffDay(LocalDate before, LocalDate after) {
        return Math.abs(Period.between(before, after).getDays());
    }
    
    /**
     * 计算两个时间LocalDate相差的年数，不考虑日期前后，返回结果>=0
     * @param before
     * @param after
     * @return
     */
    public static int getAbsDiffYear(LocalDate before, LocalDate after) {
        return Math.abs(Period.between(before, after).getYears());
    }

    /**
     * 获取指定时间年份
     * @param date
     * @return
     */
    public static int getYear(Date date){
    	LocalDate localDate = dateToLocalDate(date);
    	return localDate.getYear();
    }
    
    /**
     * 特定指定年第一天
     *
     * @param date
     * @return
     */
    public static Date getYearToStart(Date date) {
        LocalDate localDate = dateToLocalDate(date);
        LocalDate newLocalDate = LocalDate.of(localDate.getYear(), 1, 1);
        return localDateToDate(newLocalDate);
    }
    
    /**
     * 得到指定年最后一天
     * @return
     */
    public static Date getYearToEnd(Date date){
    	LocalDate localDate = dateToLocalDate(date);
    	LocalDate newLocalDate = LocalDate.of(localDate.getYear(), 1, 1);
    	localDate = newLocalDate.minusDays(1);
    	return localDateToDate(localDate);
    }
    
    /**
     * 获取指定时间当月的开始时间
     * @param date
     * @return
     */
    public static Date getMonthToStart(Date date){
    	LocalDate localdate = dateToLocalDate(date);
    	LocalDate month = LocalDate.of(localdate.getYear(), localdate.getMonth(), 1);
    	return localDateToDate(month);
    }
    
    /**
     * 获取指定时间当月的结束时间
     * @param date
     * @return
     */
    public static Date getMonthToEnd(Date date){
    	int lastDay = getActualMaximum(date);
        LocalDate localDate = dateToLocalDate(date);
        LocalDate month = LocalDate.of(localDate.getYear(), localDate.getMonth(), lastDay);
        return localDateToDate(month);
    }
    
    /**
     * 获取指定时间当天的开始时间
     * @param date
     * @return
     */
    public static Date getDateToStart(Date date){
    	LocalDateTime dateTime = LocalDateTime.of(dateToLocalDate(date), LocalTime.MIN);
    	ZoneId zone = ZoneId.systemDefault();
    	Instant instant = dateTime.atZone(zone).toInstant();
    	return Date.from(instant);
    }
    
    /**
     * 获取指定时间当天的结束时间
     * @param date
     * @return
     */
    public static Date getDateToEnd(Date date){
    	LocalDateTime dateTime = LocalDateTime.of(dateToLocalDate(date), LocalTime.MAX);
    	ZoneId zone = ZoneId.systemDefault();
    	Instant instant = dateTime.atZone(zone).toInstant();
    	return Date.from(instant);
    }


	// 获取本月是哪一月
	 int getNowMonth() {
		Date date = new Date();
		GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
		gc.setTime(date);
		return gc.get(2) + 1;
	}

	//获取指定时间的 本周一0点的时间
	public static Date getWeekBegin(Calendar cal) {
		cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		Date date=cal.getTime();
		return date;
	}

	/**
	 * 得到当前时间的前N小时
	 * 
	 * @param ihour 小时
	 * @return
	 */
	public static String getBeforeByHourTime(int ihour){
		String returnstr = "";
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - ihour);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		returnstr = df.format(calendar.getTime());
		return returnstr;
	}
}
