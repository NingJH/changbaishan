package com.cqndt.disaster.device.converter;

public interface DeviceMessageConverter {

    Integer getDeviceType();


    Object messageConvert(String json) throws Exception;


}
