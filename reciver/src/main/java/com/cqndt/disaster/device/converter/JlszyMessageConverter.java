package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TabMonitorMapper;
import com.cqndt.disaster.device.dao.TbsJlszyMapper;
import com.cqndt.disaster.device.domain.TbsJlszy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 静力水准仪tbs_jlszy
 * @Time : 2019-10-04 15:13
 **/
@Service
public class JlszyMessageConverter implements DeviceMessageConverter{

    @Autowired
    TbsJlszyMapper tbsJlszyMapper;
    @Autowired
    private TabMonitorMapper tabMonitorMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 50;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String sensorNo = map.get("number");
        String MonitorNo = tabMonitorMapper.selectMNoBySeonsorNo(sensorNo);

        TbsJlszy tbsJlszy = new TbsJlszy();
        tbsJlszy.setTime(acqTime);
        tbsJlszy.setSensorNo(sensorNo);
        tbsJlszy.setMonitorNo(MonitorNo);
        tbsJlszy.setY(map.get("y").equals(" ")?"0":map.get("y"));
        tbsJlszy.setyValue(map.get("yvalue").equals(" ")?"0":map.get("yvalue"));
        tbsJlszy.setX(map.get("x").equals(" ")?"0":map.get("x"));
        tbsJlszy.setBaseValue(map.get("baseValue").equals(" ")?"0":map.get("baseValue"));
        //最终值 =（监测点初始值-基准点初始值）-（监测点测量值-基准点测量值）
        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        double formulaOne = 0.0;
        switch (sensorNo){
            case "571532527225_2":
                formulaOne = subtract(128.14,106.84);//（监测点初始值-基准点初始值）
                tbsJlszy.setSensorNo("571532527225_1");
                tbsJlszy.setValue(tbsJlszy.getBaseValue());
                tbsJlszyMapper.insert(tbsJlszy);
                vo.put(tbsJlszy.getSensorNo(), "time", map.get("acqTime"));
                tbsJlszy.setSensorNo("571532527225_2");
                break;
            case "562253512225_2":
                formulaOne = subtract(128.14,106.84);//（监测点初始值-基准点初始值）
                tbsJlszy.setSensorNo("562253512225_1");
                tbsJlszy.setValue(tbsJlszy.getBaseValue());
                tbsJlszyMapper.insert(tbsJlszy);
                vo.put(tbsJlszy.getSensorNo(), "time", map.get("acqTime"));
                tbsJlszy.setSensorNo("562253512225_2");
                break;
            case "561694511225_2":
                formulaOne = subtract(128.62,92.73);//（监测点初始值-基准点初始值）
                tbsJlszy.setSensorNo("561694511225_1");
                tbsJlszy.setValue(tbsJlszy.getBaseValue());
                tbsJlszyMapper.insert(tbsJlszy);
                vo.put(tbsJlszy.getSensorNo(), "time", map.get("acqTime"));
                tbsJlszy.setSensorNo("561694511225_2");
                break;
            case "567342070225_2":
                formulaOne = subtract(123.3,118.07);//（监测点初始值-基准点初始值）
                tbsJlszy.setSensorNo("567342070225_1");
                tbsJlszy.setValue(tbsJlszy.getBaseValue());
                tbsJlszyMapper.insert(tbsJlszy);
                vo.put(tbsJlszy.getSensorNo(), "time", map.get("acqTime"));
                tbsJlszy.setSensorNo("567342070225_2");
                break;
            case "552735876225_2":
                formulaOne = subtract(84.48,87.5);//（监测点初始值-基准点初始值）
                tbsJlszy.setSensorNo("552735876225_1");
                tbsJlszy.setValue(tbsJlszy.getBaseValue());
                tbsJlszyMapper.insert(tbsJlszy);
                vo.put(tbsJlszy.getSensorNo(), "time", map.get("acqTime"));
                tbsJlszy.setSensorNo("552735876225_2");
                break;
            case "567478805225_2":
                formulaOne = subtract(148.17,164.73);//（监测点初始值-基准点初始值）
                tbsJlszy.setSensorNo("567478805225_1");
                tbsJlszy.setValue(tbsJlszy.getBaseValue());
                tbsJlszyMapper.insert(tbsJlszy);
                vo.put(tbsJlszy.getSensorNo(), "time", map.get("acqTime"));
                tbsJlszy.setSensorNo("567478805225_2");
                break;
        }

        tbsJlszy.setValue(map.get("value").equals(" ")?"0":map.get("value"));
        double formulaTwo = subtract(Double.parseDouble(tbsJlszy.getValue()),Double.parseDouble(tbsJlszy.getBaseValue()));//（监测点测量值-基准点测量值）
        double value = subtract(formulaOne,formulaTwo);
        tbsJlszy.setValue(value+"");


//        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsJlszy;
    }


    private static double subtract(double v1, double v2) {
        BigDecimal b1 = new BigDecimal(Double.toString(v1));
        BigDecimal b2 = new BigDecimal(Double.toString(v2));
        return b1.subtract(b2).doubleValue();
    }

}
