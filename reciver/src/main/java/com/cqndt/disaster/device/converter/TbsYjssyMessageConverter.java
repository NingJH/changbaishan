package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TbsYjssyMapper;
import com.cqndt.disaster.device.domain.TbsYjssy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Create by Intellij IDEA
 *
 * @Time : 2019-08-09 18:44
 **/
@Service
public class TbsYjssyMessageConverter implements DeviceMessageConverter {
    @Autowired
    TbsYjssyMapper tbsYjssyMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 45;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TbsYjssy tbsYjssy = new TbsYjssy();
        tbsYjssy.setTime(acqTime);
        tbsYjssy.setSensorNo(map.get("numeber"));
        tbsYjssy.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value").equals(" ")?"0":map.get("value"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsYjssy;
    }
}
