package com.cqndt.disaster.device.reciver;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cqndt.disaster.device.dao.*;
import com.cqndt.disaster.device.domain.*;
import com.cqndt.disaster.device.utils.DateUtil;
import com.cqndt.disaster.device.utils.SmsUtil;
import com.cqndt.disaster.device.websocket.WebSocketData;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
@Async
public class Warn {
    @Autowired
    private WarnSettingMapper warnSettingMapper;
    @Autowired
    private TabDeviceMapper tabDeviceMapper;
    @Autowired
    private WarnPeopleMapper warnPeopleMapper;
    @Autowired
    private WebSocketData webSocketData;
    @Autowired
    private TbsQyjcMapper tbsQyjcMapper;
    @Autowired
    private TbsKongxishuiyaMapper kongxishuiyaMapper;
    @Autowired
    private TbsDxswMapper dxswMapper;
    @Autowired
    private TbsJlszyMapper jlszyMapper;
    @Autowired
    private TbsTtcjMapper SbwyMapper;
    @Autowired
    private TbsQjyMapper qjyMapper;
    @Autowired
    private TbsYlMapper ylMapper;

    /**
     * 从rabbitMq拿出数据进行报警处理
     *
     * @param message
     */
    @RabbitListener(queuesToDeclare = {@Queue("warn.old.device")})
    public void getMessage(Message message) {
        byte[] body = message.getBody();
        String msg = String.valueOf(SerializationUtils.deserialize(body));
        JSONArray array = JSONObject.parseArray(msg);
        for (int i = 0; i < array.size(); i++) {
            JSONObject object = array.getJSONObject(i);
            BjWarnSetting(object);
        }
    }


    /**
     * 进行判断告警
     *
     * @param object 从mq拿到的数据
     */
    public void BjWarnSetting(JSONObject object) {
        String sensorNo = object.getString("number");
        //先查阀值设置，没有则不进行下一步告警判断
        WarnSetting warnSetting = warnSettingMapper.selectBySensorNo(sensorNo);
        if (warnSetting == null) {
            log.info("######设备传感器编号{}，暂无告警阀值设置，不进行告警判断！", sensorNo);
            return;
        }
        String deviceNo = warnSetting.getDeviceNo();
        //566930499224_1
        if (deviceNo == null) {
            log.info("######设备编号为空，不进行告警判断！");
            return;
        }
        if (warnSetting.getRed().compareTo(new BigDecimal("0")) == 0 || warnSetting.getYellow().compareTo(new BigDecimal("0")) == 0) {
            log.info("######设备传感器编号{}，阀值设置为0，不进行告警判断！", sensorNo);
            return;
        }
        //根据设备类型判断用什么方式告警
        int type = warnSetting.getTypeValue();
        BigDecimal redValue;
        BigDecimal yellowValue;
        BigDecimal realValue;
        BigDecimal orange;
        BigDecimal blue;
        int warnLevel = 0;
        //从mq获取数据中取时间
        String jcDate = object.getString("acqTime");
        String msgModel = warnSetting.getMsgModel();
        Date nowDate = DateUtil.stringToDate(jcDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        switch (type) {
            case 51://气压
//
//                //红色告警值
//                redValue = warnSetting.getRed();
                //黄色告警值
                yellowValue = warnSetting.getYellow();
                //橙色告警值
                orange = warnSetting.getOrange();
                //从mq获取本次数据采集真实值
                realValue = new BigDecimal(object.getString("value")).abs();
                //去数据库查询上一条值
                Date qyTime1 = getWarnTime(12, jcDate);
                Date qyTime2 = getWarnTime(24, jcDate);
                //去数据库获取之前数据

                //获取上一条数据
                TbsQyjc previousValue = tbsQyjcMapper.getPreviousValue(sensorNo, nowDate);
                if (previousValue == null) {
                    //如果说取不出来数据，说明数据库没有，直接跳出告警
                    return;
                }
                //获取12小时前数据
                TbsQyjc tbsQyjc12 = tbsQyjcMapper.getLastValue(sensorNo, qyTime1);
                //获取24小时前一条数据
                TbsQyjc tbsQyjc24 = tbsQyjcMapper.getLastValue(sensorNo, qyTime2);
                //当前值减去之前数据,取绝对值
                BigDecimal valueOne = realValue.subtract(new BigDecimal(previousValue.getValue()).abs());
                BigDecimal value12 = realValue.subtract(new BigDecimal(tbsQyjc12.getValue())).abs();
                BigDecimal value24 = realValue.subtract(new BigDecimal(tbsQyjc24.getValue())).abs();
                //与阈值进行比较
                if (valueOne.compareTo(orange) > -1) {
                    //与上一条数据相比大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(previousValue.getTime()), "相邻");

                }
                if (value12.compareTo(yellowValue) > -1) {
                    //12小时的大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(tbsQyjc12.getTime()), "12小时");

                }
                if (value24.compareTo(yellowValue) > -1) {
                    //24小时的大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(tbsQyjc24.getTime()), "24小时");

                }
//
                break;
            case 7://水压
                //红色告警值
//                redValue = warnSetting.getRed();
                //黄色告警值
                yellowValue = warnSetting.getYellow();
                //橙色告警值
                orange = warnSetting.getOrange();
                //从mq获取本次数据采集真实值
                realValue = new BigDecimal(object.getString("value")).abs();
                //去数据库查询上一条值
                Date syTime1 = getWarnTime(12, jcDate);
                Date syTime2 = getWarnTime(24, jcDate);
                //去数据库获取之前数据

                //获取上一条数据
                TbsKongxishuiya previousValue1 = kongxishuiyaMapper.getPreviousValue(sensorNo, nowDate);
                if (previousValue1 == null) {
                    return;
                }
                //获取12小时前数据
                TbsKongxishuiya tbsKxsy12 = kongxishuiyaMapper.getLastValue(sensorNo, syTime1);
                //获取24小时前一条数据
                TbsKongxishuiya tbsKxsy24 = kongxishuiyaMapper.getLastValue(sensorNo, syTime2);
                //当前值减去之前数据,取绝对值
                BigDecimal KxsyOne = realValue.subtract(previousValue1.getValue()).abs();
                BigDecimal Kxsy12 = realValue.subtract(tbsKxsy12.getValue()).abs();
                BigDecimal Kxsy24 = realValue.subtract(tbsKxsy24.getValue()).abs();
                //与阈值进行比较
                if (KxsyOne.compareTo(orange) > -1) {
                    //与上一条数据相比大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(previousValue1.getTime()), "相邻");

                }
                if (Kxsy12.compareTo(yellowValue) > -1) {
                    //12小时的大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(tbsKxsy12.getTime()), "12小时");

                }
                if (Kxsy24.compareTo(yellowValue) > -1) {
                    //24小时的大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(tbsKxsy24.getTime()), "24小时");

                }
//                //进行告警判断
//                warnLevel = realValue.compareTo(yellowValue) > -1 ? 2 : 0;//二级黄色
//                warnLevel = realValue.compareTo(redValue) > -1 ? 1 : warnLevel;//一级红色告警
//                if (warnLevel != 0) {
//                    //发生告警，进行告警信息推送
//                    setWarnMsg2Socket(sensorNo, deviceNo, warnLevel, realValue, redValue, yellowValue, jcDate, msgModel);
//                }
                break;
            case 18://地下水位监测
                //红色告警值
//                redValue = warnSetting.getRed();
                //黄色告警值
                yellowValue = warnSetting.getYellow();
                //橙色告警值
                orange = warnSetting.getOrange();
                //从mq获取本次数据采集真实值
                realValue = new BigDecimal(object.getString("value")).abs();
                //去数据库查询上一条值
                Date dxswTime1 = getWarnTime(12, jcDate);
                Date dxswTime2 = getWarnTime(24, jcDate);
                //去数据库获取之前数据

                //获取上一条数据
                TbsDxsw previousValue2 = dxswMapper.getPreviousValue(sensorNo, nowDate);
                if (previousValue2 == null) {
                    return;
                }
                //获取12小时前数据
                TbsDxsw tbsDxsw12 = dxswMapper.getLastValue(sensorNo, dxswTime1);
                //获取24小时前一条数据
                TbsDxsw tbsDxsw24 = dxswMapper.getLastValue(sensorNo, dxswTime2);
                //当前值减去之前数据,取绝对值
                BigDecimal DxswOne = realValue.subtract(previousValue2.getValue()).abs();
                BigDecimal Dxsw12 = realValue.subtract(tbsDxsw12.getValue()).abs();
                BigDecimal Dxsw24 = realValue.subtract(tbsDxsw24.getValue()).abs();
                //与阈值进行比较
                if (DxswOne.compareTo(orange) > -1) {
                    //与上一条数据相比大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(previousValue2.getTime()), "相邻");

                }
                if (Dxsw12.compareTo(yellowValue) > -1) {
                    //12小时的大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(tbsDxsw12.getTime()), "12小时");

                }
                if (Dxsw24.compareTo(yellowValue) > -1) {
                    //24小时的大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(tbsDxsw24.getTime()), "24小时");

                }
                break;
            case 50://静力水准仪
                //红色告警值
//                redValue = warnSetting.getRed();
                //黄色告警值
                yellowValue = warnSetting.getYellow();
                //橙色告警值
                orange = warnSetting.getOrange();
                //从mq获取本次数据采集真实值
                realValue = new BigDecimal(object.getString("value")).abs();
                //去数据库查询上一条值
                Date jlszTime1 = getWarnTime(12, jcDate);
                Date jlszTime2 = getWarnTime(24, jcDate);
                //去数据库获取之前数据

                //获取上一条数据
                TbsJlszy previousValue3 = jlszyMapper.getPreviousValue(sensorNo, nowDate);
                if (previousValue3 == null) {
                    return;
                }
                //获取12小时前数据
                TbsJlszy tbsJlszy12 = jlszyMapper.getLastValue(sensorNo, jlszTime1);
                //获取24小时前一条数据
                TbsJlszy tbsJlszy24 = jlszyMapper.getLastValue(sensorNo, jlszTime2);
                //当前值减去之前数据,取绝对值
                BigDecimal JlszyOne = realValue.subtract(new BigDecimal(previousValue3.getValue())).abs();
                BigDecimal Jlszy12 = realValue.subtract(new BigDecimal(tbsJlszy12.getValue())).abs();
                BigDecimal Jlszy24 = realValue.subtract(new BigDecimal(tbsJlszy24.getValue())).abs();
                //与阈值进行比较
                if (JlszyOne.compareTo(orange) > -1) {
                    //与上一条数据相比大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(previousValue3.getTime()), "相邻");

                }
                if (Jlszy12.compareTo(yellowValue) > -1) {
                    //12小时的大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(tbsJlszy12.getTime()), "12小时");

                }
                if (Jlszy24.compareTo(yellowValue) > -1) {
                    //24小时的大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(tbsJlszy24.getTime()), "24小时");

                }
                break;
            case 35://深部位移（光纤光栅）
                //红色告警值
//                redValue = warnSetting.getRed();
                //黄色告警值
                yellowValue = warnSetting.getYellow();
                //橙色告警值
                orange = warnSetting.getOrange();
                //从mq获取本次数据采集真实值
                realValue = new BigDecimal(object.getString("value")).abs();
                //去数据库查询上一条值
                Date SbwyTime1 = getWarnTime(12, jcDate);
                Date SbwyTime2 = getWarnTime(24, jcDate);
                //去数据库获取之前数据

                //获取上一条数据
                TbsTtcj previousValue4 = SbwyMapper.getPreviousValue(sensorNo, nowDate);
                if (previousValue4 == null) {
                    return;
                }
                //获取12小时前数据
                TbsTtcj tbsTtcj12 = SbwyMapper.getLastValue(sensorNo, SbwyTime1);
                //获取24小时前一条数据
                TbsTtcj tbsTtcj24 = SbwyMapper.getLastValue(sensorNo, SbwyTime2);
                //当前值减去之前数据,取绝对值
                BigDecimal TtcjOne = realValue.subtract(previousValue4.getValue().abs());
                BigDecimal Ttcj12 = realValue.subtract(tbsTtcj12.getValue()).abs();
                BigDecimal Ttcj24 = realValue.subtract(tbsTtcj24.getValue()).abs();
                //与阈值进行比较
                if (TtcjOne.compareTo(orange) > -1) {
                    //与上一条数据相比大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(previousValue4.getTime()), "相邻");

                }
                if (Ttcj12.compareTo(yellowValue) > -1) {
                    //12小时的大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(tbsTtcj12.getTime()), "12小时");

                }
                if (Ttcj24.compareTo(yellowValue) > -1) {
                    //24小时的大于阈值，进行告警
                    setWarnMsg2Socket2(sensorNo, deviceNo, realValue, yellowValue, jcDate, msgModel, dateFormat.format(tbsTtcj24.getTime()), "24小时");

                }
            case 10://雨量

                redValue = warnSetting.getRed();
                yellowValue = warnSetting.getYellow();
                orange = warnSetting.getOrange();
                blue = warnSetting.getBlue();

                realValue = new BigDecimal(object.getString("value")).abs();
                String h1 = dateFormat.format(getWarnTime(1, jcDate));
                String h3 = dateFormat.format(getWarnTime(3, jcDate));
                String h5 = dateFormat.format(getWarnTime(5, jcDate));
                String h24 = dateFormat.format(getWarnTime(24, jcDate));
                BigDecimal h1Value = new BigDecimal(String.valueOf((ylMapper.getSumValueByDate(h1, jcDate, sensorNo)) == null ? new BigDecimal("0") : new BigDecimal(ylMapper.getSumValueByDate(h1, jcDate, sensorNo))));
                BigDecimal h3Value = new BigDecimal(String.valueOf((ylMapper.getSumValueByDate(h3, jcDate, sensorNo)) == null ? new BigDecimal("0") : new BigDecimal(ylMapper.getSumValueByDate(h3, jcDate, sensorNo))));
                BigDecimal h5Value = new BigDecimal(String.valueOf((ylMapper.getSumValueByDate(h5, jcDate, sensorNo)) == null ? new BigDecimal("0") : new BigDecimal(ylMapper.getSumValueByDate(h5, jcDate, sensorNo))));
                BigDecimal h24Value = new BigDecimal(String.valueOf((ylMapper.getSumValueByDate(h24, jcDate, sensorNo)) == null ? new BigDecimal("0") : new BigDecimal(ylMapper.getSumValueByDate(h24, jcDate, sensorNo))));
                h1Value = realValue.add(h1Value);
                h3Value = realValue.add(h3Value);
                h5Value = realValue.add(h5Value);
                h24Value = realValue.add(h24Value);
                //和阈值进行比较
                if (h1Value.compareTo(redValue) > -1) {
                    //1小时告警
                    setWarnMsg2Socket3(sensorNo, deviceNo, h1Value, redValue, jcDate, msgModel);
                }
                if (h3Value.compareTo(yellowValue) > -1) {
                    //1小时告警
                    setWarnMsg2Socket3(sensorNo, deviceNo, h3Value, yellowValue, jcDate, msgModel);
                }
                if (h5Value.compareTo(orange) > -1) {
                    //1小时告警
                    setWarnMsg2Socket3(sensorNo, deviceNo, h5Value, orange, jcDate, msgModel);
                }
                if (h24Value.compareTo(blue) > -1) {
                    //1小时告警
                    setWarnMsg2Socket3(sensorNo, deviceNo, h24Value, blue, jcDate, msgModel);
                }

                break;
            case 25:
                //倾斜监测告警
                //mq获取的真实值
                BigDecimal realValuex = new BigDecimal(object.getString("x")).abs();
                BigDecimal realValuey = new BigDecimal(object.getString("y")).abs();
//                BigDecimal realValuez = new BigDecimal(object.getString("z")).abs();
                redValue = warnSetting.getRed();
                yellowValue = warnSetting.getYellow();
                orange = warnSetting.getOrange();
                qxWarn(sensorNo, deviceNo, realValuex, realValuey, redValue, yellowValue, orange, jcDate, msgModel);
                break;

        }


    }

    /**
     * @param sensorNo  传感器编号
     * @param deviceNo  设备编号
     * @param sumValue  和值
     * @param warnValue 阈值
     * @param jcDate    监测时间
     * @param msgModel  短信模板
     */
    private void setWarnMsg2Socket3(String sensorNo, String deviceNo, BigDecimal sumValue, BigDecimal warnValue, String jcDate, String msgModel) {
        //获取设备项目信息
        Map<String, Object> deviceInfo = tabDeviceMapper.getDeviceAndProjectInfo(deviceNo);
        deviceInfo.put("sensorNo", sensorNo);
        deviceInfo.put("jcDate", jcDate);
        deviceInfo.put("lastTime", "");
        deviceInfo.put("jcValue", sumValue);
        deviceInfo.put("warnSetting", warnValue);
        deviceInfo.put("warn", "红色告警");

        msgModel = msgModel.replace("[告警阈值]", warnValue + "");

        deviceInfo.put("sign", "");
        List<WarnPeople> warnPeopleList = warnPeopleMapper.selectAll(sensorNo);
        try {
            //发送短信
            StringBuilder phone = new StringBuilder();
            for (int i = 1; i <= warnPeopleList.size(); i++) {
                if (i == 1) {
                    phone.append(warnPeopleList.get(i - 1).getPhone());
                } else {
                    phone.append(",").append(warnPeopleList.get(i - 1).getPhone());
                }
            }
            String phone2people = phone.toString();
            String time1 = String.valueOf(System.currentTimeMillis());
            msgModel = msgModel.replace("[告警时间]", deviceInfo.get("time")==null?"":deviceInfo.get("time")+"")
                    .replace("[告警等级]", "黄色告警")
                    .replace("[设备地址]", deviceInfo.get("address")==null ?"":deviceInfo.get("address")+"")
                    .replace("[监测值]", deviceInfo.get("jcValue")==null ?"":deviceInfo.get("jcValue")+"")
                    .replace("[设备类型]", deviceInfo.get("deviceType")==null ?"":deviceInfo.get("deviceType")+"")
                    .replace("[设备编号]", deviceInfo.get("deviceNo")==null ?"":deviceInfo.get("deviceNo")+"")
                    .replace("[传感器编号]", sensorNo)
                    .replace("[工程名称]", deviceInfo.get("projectName")==null ?"":deviceInfo.get("projectName")+"")
                    .replace("[设备名称]", deviceInfo.get("deviceName")==null ?"":deviceInfo.get("deviceName")+"");
            SmsUtil.sendSms(time1, phone2people, time1, msgModel);
            log.info("告警短信发送人员{}，短信内容{}", warnPeopleList, msgModel);
            webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo), "10");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param sensorNo    传感器编号
     * @param deviceNo    设备编号
     * @param realValue   真实值
     * @param yellowValue 黄色阈值
     * @param jcDate      监测时间
     * @param msgModel    短信模板
     * @param lastTime    上一次时间
     * @param itemTime    相邻多少时间
     */
    private void setWarnMsg2Socket2(String sensorNo, String deviceNo,
                                    BigDecimal realValue, BigDecimal yellowValue,
                                    String jcDate, String msgModel, String lastTime,
                                    String itemTime) {
        //获取设备项目信息，这个是要通过websocket返给real3D客户端
        Map<String, Object> deviceInfo = tabDeviceMapper.getDeviceAndProjectInfo(deviceNo);
        deviceInfo.put("sensorNo", sensorNo);
        deviceInfo.put("jcTime", jcDate);
        deviceInfo.put("lastTime", lastTime);
        deviceInfo.put("jcValue", realValue);
        deviceInfo.put("warnSetting", yellowValue);
        deviceInfo.put("warn", "黄色告警");
        deviceInfo.put("itemTime", itemTime);
        msgModel = msgModel.replace("[告警阈值]", realValue + "");
        deviceInfo.put("sign", "");
        List<WarnPeople> warnPeopleList = warnPeopleMapper.selectAll(sensorNo);
        try {
            //发送短信
            StringBuilder phone = new StringBuilder();
            for (int i = 1; i <= warnPeopleList.size(); i++) {
                if (i == 1) {
                    phone.append(warnPeopleList.get(i - 1).getPhone());
                } else {
                    phone.append(",").append(warnPeopleList.get(i - 1).getPhone());
                }
            }
            String phone2people = phone.toString();
            String time1 = String.valueOf(System.currentTimeMillis());
            msgModel = msgModel.replace("[告警时间]", deviceInfo.get("time")==null?"":deviceInfo.get("time")+"")
                    .replace("[告警等级]", "黄色告警")
                    .replace("[设备地址]", deviceInfo.get("address")==null ?"":deviceInfo.get("address")+"")
                    .replace("[监测值]", deviceInfo.get("jcValue")==null ?"":deviceInfo.get("jcValue")+"")
                    .replace("[设备类型]", deviceInfo.get("deviceType")==null ?"":deviceInfo.get("deviceType")+"")
                    .replace("[设备编号]", deviceInfo.get("deviceNo")==null ?"":deviceInfo.get("deviceNo")+"")
                    .replace("[传感器编号]", sensorNo)
                    .replace("[工程名称]", deviceInfo.get("projectName")==null ?"":deviceInfo.get("projectName")+"")
                    .replace("[设备名称]", deviceInfo.get("deviceName")==null ?"":deviceInfo.get("deviceName")+"");
            //发送短信前看是否是一天内发过

            SmsUtil.sendSms(time1, phone2people, time1, msgModel);
            //短信存储新表，里面把短信
            log.info("告警短信发送人员{}，短信内容{}", warnPeopleList, msgModel);
            webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo), "10");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * 倾斜监测告警
     *
     * @param realValuex  x监测值
     * @param realValuey  y监测值
     * @param redValue    红色告警值
     * @param yellowValue 黄色告警值
     */
    private void qxWarn(String sensorNo, String deviceNo, BigDecimal realValuex,
                        BigDecimal realValuey, BigDecimal redValue,
                        BigDecimal yellowValue, BigDecimal orange, String time,
                        String msgModel) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //去数据库查询上一条值
        Date QxTime1 = getWarnTime(12, time);
        Date QxTime2 = getWarnTime(24, time);
        //去数据库获取之前数据

        //获取上一条数据
        TbsQjy previousValue5 = qjyMapper.getPreviousValue(sensorNo, DateUtil.stringToDate(time));
        if (previousValue5 == null) {
            return;
        }
        //获取12小时前数据
        TbsQjy tbsQjy12 = qjyMapper.getLastValue(sensorNo, QxTime1);
        //获取24小时前一条数据
        TbsQjy tbsQjy24 = qjyMapper.getLastValue(sensorNo, QxTime2);

        //当前X值减去之前数据,取绝对值
        BigDecimal QjyXOne = realValuex.subtract(previousValue5.getX().abs());
        BigDecimal QjyX12 = realValuex.subtract(tbsQjy12.getX()).abs();
        BigDecimal QjyX24 = realValuex.subtract(tbsQjy24.getX()).abs();
        //当前Y值减去之前数据,取绝对值
        BigDecimal QjyYOne = realValuex.subtract(previousValue5.getY().abs());
        BigDecimal QjyY12 = realValuex.subtract(tbsQjy12.getY()).abs();
        BigDecimal QjyY24 = realValuex.subtract(tbsQjy24.getY()).abs();

        //与阈值进行比较
        if (QjyXOne.compareTo(orange) > -1) {
            //与上一条数据相比大于阈值，进行告警
            setWarnMsg2Socket2(sensorNo, deviceNo, realValuex, yellowValue, time, msgModel, dateFormat.format(previousValue5.getTime()), "相邻");
        }
        if (QjyX12.compareTo(yellowValue) > -1) {
            //12小时的大于阈值，进行告警
            setWarnMsg2Socket2(sensorNo, deviceNo, realValuex, yellowValue, time, msgModel, dateFormat.format(tbsQjy12.getTime()), "12小时");
        }
        if (QjyX24.compareTo(yellowValue) > -1) {
            //24小时的大于阈值，进行告警
            setWarnMsg2Socket2(sensorNo, deviceNo, realValuex, yellowValue, time, msgModel, dateFormat.format(tbsQjy24.getTime()), "24小时");
        }

        if (QjyYOne.compareTo(orange) > -1) {
            //与上一条数据相比大于阈值，进行告警
            setWarnMsg2Socket2(sensorNo, deviceNo, realValuey, yellowValue, time, msgModel, dateFormat.format(previousValue5.getTime()), "相邻");
        }
        if (QjyY12.compareTo(yellowValue) > -1) {
            //12小时的大于阈值，进行告警
            setWarnMsg2Socket2(sensorNo, deviceNo, realValuey, yellowValue, time, msgModel, dateFormat.format(tbsQjy12.getTime()), "12小时");
        }
        if (QjyY24.compareTo(yellowValue) > -1) {
            //24小时的大于阈值，进行告警
            setWarnMsg2Socket2(sensorNo, deviceNo, realValuey, yellowValue, time, msgModel, dateFormat.format(tbsQjy24.getTime()), "24小时");
        }
    }




    /**
     * 获取date之前几小时的时间s
     *
     * @param hours
     * @param date
     * @return
     */
    public Date getWarnTime(double hours, String date) {

        double v = DateUtil.stringToDate(date).getTime() - hours * 60 * 60 * 1000;
        return new Date((long) v);
    }

}
