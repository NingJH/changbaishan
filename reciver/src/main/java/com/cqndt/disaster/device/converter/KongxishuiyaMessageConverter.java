package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TabMonitorMapper;
import com.cqndt.disaster.device.dao.TbsKongxishuiyaMapper;
import com.cqndt.disaster.device.domain.TbsKongxishuiya;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Create by Intellij IDEA
 * 水压tbs_kongxishuiya
 * @Time : 2019-08-09 09:28
 **/
@Service
public class KongxishuiyaMessageConverter implements DeviceMessageConverter {
    @Autowired
    TbsKongxishuiyaMapper tbsKongxishuiyaMapper;
    @Autowired
    private TabMonitorMapper tabMonitorMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 7;
    }

    @Override
    public Object messageConvert(String json) throws Exception {

        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String sensorNo = map.get("number");
        String MonitorNo = tabMonitorMapper.selectMNoBySeonsorNo(sensorNo);

        TbsKongxishuiya tbsKongxishuiya = new TbsKongxishuiya();
        tbsKongxishuiya.setTime(acqTime);
        tbsKongxishuiya.setSensorNo(sensorNo);
        tbsKongxishuiya.setMonitorNo(MonitorNo);
        tbsKongxishuiya.setValue(BigDecimal.valueOf(Double.valueOf(map.get("value").equals(" ")?"0":map.get("value"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsKongxishuiya;
    }
}
