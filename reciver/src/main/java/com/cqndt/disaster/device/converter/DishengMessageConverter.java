package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TbsCishengMapper;
import com.cqndt.disaster.device.dao.TbsDishengMapper;
import com.cqndt.disaster.device.domain.TbsCisheng;
import com.cqndt.disaster.device.domain.TbsDisheng;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * @Time : 2019-09-17 16:35
 **/
@Service
public class DishengMessageConverter implements DeviceMessageConverter{

    @Autowired
    TbsDishengMapper tbsCishengMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 27;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TbsDisheng tbsDisheng = new TbsDisheng();
        tbsDisheng.setTime(acqTime);
        tbsDisheng.setSensorNo(map.get("number"));
        tbsDisheng.setZ(map.get("value").equals(" ")? "0":map.get("value"));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsDisheng;
    }
}
