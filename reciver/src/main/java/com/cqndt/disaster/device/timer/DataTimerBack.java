//package com.cqndt.disaster.device.timer;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import com.cqndt.disaster.device.common.influxdb.InfluxDBConnect;
//import com.cqndt.disaster.device.common.util.HttpClientUtil;
//import com.cqndt.disaster.device.common.util.UpdatePropertyUtil;
//import com.cqndt.disaster.device.dao.*;
//import com.cqndt.disaster.device.domain.TabAlarmInfo;
//import com.cqndt.disaster.device.domain.TabDeviceException;
//import com.cqndt.disaster.device.domain.TabOnline;
//import com.cqndt.disaster.device.websocket.WebSocketServer;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.data.redis.core.HashOperations;
//import org.springframework.data.redis.core.StringRedisTemplate;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.io.IOException;
//import java.math.BigDecimal;
//import java.text.DateFormat;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
///**
// * @author qingqinchao
// */
//@Component
//public class DataTimerBack {
//
//    @Autowired
//    private RabbitTemplate rabbitTemplate;
//    @Resource
//    private StringRedisTemplate stringRedisTemplate;
//    @Autowired
//    private DataCmccMapper dataCmccMapper;
//    @Autowired
//    private TabWarnSettingMapper tabWarnSettingMapper;
//    @Autowired
//    private TabMonitorMapper tabMonitorMapper;
//    @Autowired
//    private TabAlarmInfoMapper tabAlarmInfoMapper;
//    @Autowired
//    private TabDeviceMapper tabDeviceMapper;
//    @Autowired
//    private TabOnlineMapper tabOnlineMapper;
//    @Autowired
//    private InfluxDBConnect influxDBConnect;
//    @Autowired
//    private WebSocketServer webSocketServer;
//    @Autowired
//    private TabSensorMapper tabSensorMapper;
//    @Autowired
//    private TabDeviceExceptionMapper tabDeviceExceptionMapper;
//
//    @Value("${path-record-time}")
//    private String recordTimePath;
//    @Value("${token-url}")
//    private String tokenUrl;
//    @Value("${gnss-data-url}")
//    private String gnssDataUrl;
//    @Value("${add-time}")
//    private int addTime;
//    @Value("${lgd-token-url}")
//    private String lgdTokenUrl;
//    @Value("${lgd-push-url}")
//    private String lgdPushUrl;
//
//    private static Map<String, String> map = new HashMap<>(16);
//
////    @Async("taskExecutor")
////    @Scheduled(fixedRate = 60000)
////    public void dataTimer() {
////        List<Map<String, String>> mapList = dataCmccMapper.selectNo();
////        List<Map<String, String>> allList = new ArrayList(16);
////        for (Map<String, String> map : mapList) {
////            String sensorNo = map.get("imei");
////            String deviceNo = map.get("dev_id");
////            String type = map.get("value");
////            if (sensorNo != null && deviceNo != null && type != null) {
////                int oldCount;
////                int newCount;
////                switch (type) {
////                    case "QINGXIE":
////                        oldCount = tbsDmQxyMapper.selectCountBySensorNo(sensorNo);
////                        newCount = dataCmccMapper.selectCountBySensorNo(sensorNo);
////                        if (oldCount != newCount) {
////                            int start = oldCount;
////                            int limit = newCount - oldCount;
////                            List<String> xList = dataCmccMapper.selectQingXieX(sensorNo, start, limit);
////                            List<String> yList = dataCmccMapper.selectQingXieY(sensorNo, start, limit);
////                            List<String> zList = dataCmccMapper.selectQingXieZ(sensorNo, start, limit);
////                            List<String> tList = dataCmccMapper.selectQingXieT(sensorNo, start, limit);
////                            for (int i = 0; i < xList.size(); i++) {
////                                Map<String, String> qxyMap = new HashMap(16);
////                                qxyMap.put("sensor_no", sensorNo);
////                                try {
////                                    qxyMap.put("x_jd", xList.get(i));
////                                    qxyMap.put("y_jd", yList.get(i));
////                                    qxyMap.put("z_jd", zList.get(i));
////                                    qxyMap.put("time", tList.get(i));
////                                    qxyMap.put("type", "41");
////                                } catch (Exception e) {
////                                    qxyMap.put("x_jd", "0");
////                                    qxyMap.put("y_jd", "0");
////                                    qxyMap.put("z_jd", "0");
////                                    qxyMap.put("time", "2019-05-14 12:24:53");
////                                    qxyMap.put("type", "41");
////                                }
////                                allList.add(qxyMap);
////                            }
////                        }
////                        break;
////                    case "BIAOQING":
////                        oldCount = tbsPtqcbxyMapper.selectCountBySensorNo(sensorNo);
////                        newCount = dataCmccMapper.selectCountBySensorNo(sensorNo);
////                        if (oldCount != newCount) {
////                            int start = oldCount;
////                            int limit = newCount - oldCount;
////                            List<String> wXList = dataCmccMapper.selectQingXieWX(sensorNo, start, limit);
////                            List<String> wYList = dataCmccMapper.selectQingXieWY(sensorNo, start, limit);
////                            List<String> wZList = dataCmccMapper.selectQingXieWZ(sensorNo, start, limit);
////                            List<String> xList = dataCmccMapper.selectQingXieX(sensorNo, start, limit);
////                            List<String> yList = dataCmccMapper.selectQingXieY(sensorNo, start, limit);
////                            List<String> zList = dataCmccMapper.selectQingXieZ(sensorNo, start, limit);
////                            List<String> tList = dataCmccMapper.selectQingXieT(sensorNo, start, limit);
////                            for (int i = 0; i < xList.size(); i++) {
////                                Map<String, String> ptqcbxyMap = new HashMap(16);
////                                ptqcbxyMap.put("sensor_no", sensorNo);
////                                try {
////                                    ptqcbxyMap.put("x", wXList.get(i));
////                                    ptqcbxyMap.put("y", wYList.get(i));
////                                    ptqcbxyMap.put("z", wZList.get(i));
////                                    ptqcbxyMap.put("x_jd", xList.get(i));
////                                    ptqcbxyMap.put("y_jd", yList.get(i));
////                                    ptqcbxyMap.put("z_jd", zList.get(i));
////                                    ptqcbxyMap.put("time", tList.get(i));
////                                    ptqcbxyMap.put("type", "42");
////                                } catch (Exception e) {
////                                    ptqcbxyMap.put("x", "0");
////                                    ptqcbxyMap.put("y", "0");
////                                    ptqcbxyMap.put("z", "0");
////                                    ptqcbxyMap.put("x_jd", "0");
////                                    ptqcbxyMap.put("y_jd", "0");
////                                    ptqcbxyMap.put("z_jd", "0");
////                                    ptqcbxyMap.put("time", "2019-05-14 12:24:53");
////                                    ptqcbxyMap.put("type", "42");
////                                }
////                                allList.add(ptqcbxyMap);
////                            }
////                        }
////                        break;
////                    case "DLF":
////                        oldCount = tbsLiefengLxMapper.selectCountBySensorNo(sensorNo + "拉线".hashCode());
////                        newCount = dataCmccMapper.selectCountBySensorNo(sensorNo);
////                        if (oldCount != newCount) {
////                            int start = oldCount;
////                            int limit = newCount - oldCount;
////                            List<String> xList = dataCmccMapper.selectQingXieX(sensorNo, start, limit);
////                            List<String> yList = dataCmccMapper.selectQingXieY(sensorNo, start, limit);
////                            List<String> zList = dataCmccMapper.selectQingXieZ(sensorNo, start, limit);
////                            List<String> tList = dataCmccMapper.selectQingXieT(sensorNo, start, limit);
////                            List<String> lXList = dataCmccMapper.selectLX(sensorNo, start, limit);
////
////                            for (int i = 0; i < xList.size(); i++) {
////                                Map<String, String> dblfqjMap = new HashMap(16);
////                                Map<String, String> dblflxMap = new HashMap(16);
////                                dblfqjMap.put("sensor_no", sensorNo + "倾斜".hashCode());
////                                dblflxMap.put("sensor_no", sensorNo + "拉线".hashCode());
////                                try {
////                                    dblfqjMap.put("x_jd", xList.get(i));
////                                    dblfqjMap.put("y_jd", yList.get(i));
////                                    dblfqjMap.put("z_jd", zList.get(i));
////                                    dblfqjMap.put("time", tList.get(i));
////                                    dblfqjMap.put("type", "40");
////                                } catch (Exception e) {
////                                    dblfqjMap.put("x_jd", "0");
////                                    dblfqjMap.put("y_jd", "0");
////                                    dblfqjMap.put("z_jd", "0");
////                                    dblfqjMap.put("time", "2019-05-14 12:24:53");
////                                    dblfqjMap.put("type", "40");
////                                }
////                                try {
////                                    dblflxMap.put("lx", lXList.get(i));
////                                    dblflxMap.put("time", tList.get(i));
////                                    dblflxMap.put("type", "43");
////                                } catch (Exception e) {
////                                    dblflxMap.put("lx", "0");
////                                    dblfqjMap.put("time", "2019-05-14 12:24:53");
////                                    dblflxMap.put("type", "43");
////                                }
////                                allList.add(dblfqjMap);
////                                allList.add(dblflxMap);
////                            }
////                        }
////                        break;
////                    default:
////                        break;
////                }
////            }
////        }
////        if (allList.size() > 0) {
////            String s = JSON.toJSONString(allList);
////            rabbitTemplate.convertAndSend("test2", s);
////        }
////    }
//
//    @Async("taskExecutor")
//    @Scheduled(initialDelay = 5000, fixedRate = 20000)
//    public void dataTimer() {
//        List<Map<String, String>> mapList = dataCmccMapper.selectNo();
//        for (Map<String, String> map : mapList) {
//            String sensorNo = map.get("imei");
////            String deviceNo = map.get("dev_id");
//            String type = map.get("value");
//            if (sensorNo != null && type != null) {
//                HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
//                switch (type) {
//                    case "QINGXIE":
//                        String measurementXQX = "tbs_dm_qxy_x";
//                        String dsIdXQX = "3345_0_5702";
//                        int oldCountXQX = influxDBConnect.countSensorData(sensorNo, measurementXQX);
//                        int newCountXQX = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdXQX);
//                        if (oldCountXQX != newCountXQX && newCountXQX > oldCountXQX) {
//                            List<Map<String, Object>> fieldsX = dataCmccMapper.selectQingXieX(sensorNo, oldCountXQX, newCountXQX - oldCountXQX);
//                            List<Map<String, String>> lList = new ArrayList<>(16);
//                            Map<String, Object> lAllMap = new HashMap<>(16);
//                            Map<String, Object> post = HttpClientUtil.post(lgdTokenUrl, "", "");
//                            String token = post.get("token").toString();
//                            for (Map<String, Object> mapField : fieldsX) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                value = value > 180.0 ? (value - 360.0) : value;
//                                //告警判断
//                                String currentTime = mapField.get("currentTime").toString();
//                                String time = timestampToString(currentTime);
//                                vo.put(sensorNo, "time", time);
//                                vo.put(sensorNo, "value", value.toString());
//                                warnPush(sensorNo, "", currentTime, value);
//                                String format = String.format("%.3f", value);
//                                mapField.put("value", format);
//
//                                //数据推送 理工大
//                                Map<String, String> lMap = new HashMap<>(16);
//                                String sensorId = tabMonitorMapper.getMonitorNobySensorNo(sensorNo);
//                                lMap.put("dataSource", "hh");
//                                lMap.put("sensorType", "QX");
//                                lMap.put("time", time);
//                                lMap.put("v1", value.toString());
//                                lMap.put("sensorId", sensorId);
//                                lList.add(lMap);
//                            }
//                            lAllMap.put("dataSet", lList);
//                            String s = JSONArray.toJSONString(lAllMap);
//                            Map<String, Object> resultMap = HttpClientUtil.post(lgdPushUrl, s, token);
//                            System.out.println(resultMap);
//                            saveData(sensorNo, measurementXQX, fieldsX);
//                        }
//
//                        String measurementYQX = "tbs_dm_qxy_y";
//                        String dsIdYQX = "3345_0_5703";
//                        int oldCountYQX = influxDBConnect.countSensorData(sensorNo, measurementYQX);
//                        int newCountYQX = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdYQX);
//                        if (oldCountYQX != newCountYQX && newCountYQX > oldCountYQX) {
//                            List<Map<String, Object>> fieldsY = dataCmccMapper.selectQingXieY(sensorNo, oldCountYQX, newCountYQX - oldCountYQX);
//                            List<Map<String, String>> lList = new ArrayList<>(16);
//                            Map<String, Object> lAllMap = new HashMap<>(16);
//                            Map<String, Object> post = HttpClientUtil.post(lgdTokenUrl, "", "");
//                            String token = post.get("token").toString();
//                            for (Map<String, Object> mapField : fieldsY) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                value = value > 180.0 ? (value - 360.0) : value;
//                                //告警判断
//                                String currentTime = mapField.get("currentTime").toString();
//                                String time = timestampToString(currentTime);
//                                vo.put(sensorNo, "time", time);
//                                vo.put(sensorNo, "value", value.toString());
//                                warnPush(sensorNo, "", currentTime, value);
//                                String format = String.format("%.3f", value);
//                                mapField.put("value", format);
//
//                                //数据推送 理工大
//                                Map<String, String> lMap = new HashMap<>(16);
//                                String sensorId = tabMonitorMapper.getMonitorNobySensorNo(sensorNo);
//                                lMap.put("dataSource", "hh");
//                                lMap.put("sensorType", "QX");
//                                lMap.put("time", time);
//                                lMap.put("v1", value.toString());
//                                lMap.put("sensorId", sensorId);
//                                lList.add(lMap);
//                            }
//                            lAllMap.put("dataSet", lList);
//                            String s = JSONArray.toJSONString(lAllMap);
//                            Map<String, Object> resultMap = HttpClientUtil.post(lgdPushUrl, s, token);
//                            System.out.println(resultMap);
//
//                            saveData(sensorNo, measurementYQX, fieldsY);
//                        }
//
//                        String measurementZQX = "tbs_dm_qxy_z";
//                        String dsIdZQX = "3345_0_5704";
//                        int oldCountZQX = influxDBConnect.countSensorData(sensorNo, measurementZQX);
//                        int newCountZQX = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdZQX);
//                        if (oldCountZQX != newCountZQX && newCountZQX > oldCountZQX) {
//                            List<Map<String, Object>> fieldsZ = dataCmccMapper.selectQingXieZ(sensorNo, oldCountZQX, newCountZQX - oldCountZQX);
//                            List<Map<String, String>> lList = new ArrayList<>(16);
//                            Map<String, Object> lAllMap = new HashMap<>(16);
//                            Map<String, Object> post = HttpClientUtil.post(lgdTokenUrl, "", "");
//                            String token = post.get("token").toString();
//                            for (Map<String, Object> mapField : fieldsZ) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                value = value > 180.0 ? (value - 360.0) : value;
//                                //告警判断
//                                String currentTime = mapField.get("currentTime").toString();
//                                String time = timestampToString(currentTime);
//                                vo.put(sensorNo, "time", time);
//                                vo.put(sensorNo, "value", value.toString());
//                                warnPush(sensorNo, "", currentTime, value);
//                                String format = String.format("%.3f", value);
//                                mapField.put("value", format);
//
//                                //数据推送 理工大
//                                Map<String, String> lMap = new HashMap<>(16);
//                                String sensorId = tabMonitorMapper.getMonitorNobySensorNo(sensorNo);
//                                lMap.put("dataSource", "hh");
//                                lMap.put("sensorType", "QX");
//                                lMap.put("time", time);
//                                lMap.put("v1", value.toString());
//                                lMap.put("sensorId", sensorId);
//                                lList.add(lMap);
//                            }
//                            lAllMap.put("dataSet", lList);
//                            String s = JSONArray.toJSONString(lAllMap);
//                            Map<String, Object> resultMap = HttpClientUtil.post(lgdPushUrl, s, token);
//                            System.out.println(resultMap);
//
//                            saveData(sensorNo, measurementZQX, fieldsZ);
//                        }
//                        break;
//                    case "BIAOQING":
//                        String measurementXPQ = "tbs_ptqcbxy_wy_x";
//                        String dsIdXPQ = "3345_0_5702";
//                        int oldCountXPQ = influxDBConnect.countSensorData(sensorNo + "倾斜".hashCode(), measurementXPQ);
//                        int newCountXPQ = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdXPQ);
//                        if (oldCountXPQ != newCountXPQ && newCountXPQ > oldCountXPQ) {
//                            List<Map<String, Object>> fieldsX = dataCmccMapper.selectQingXieX(sensorNo, oldCountXPQ, newCountXPQ - oldCountXPQ);
//                            for (Map<String, Object> mapField : fieldsX) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                value = value > 180.0 ? (value - 360.0) : value;
//                                //告警判断
//                                String currentTime = mapField.get("currentTime").toString();
//                                String time = timestampToString(currentTime);
//                                vo.put(sensorNo + "倾斜".hashCode(), "time", time);
//                                vo.put(sensorNo + "倾斜".hashCode(), "value", value.toString());
//                                warnPush(sensorNo + "倾斜".hashCode(), "", currentTime, value);
//                                String format = String.format("%.3f", value);
//                                mapField.put("value", format);
//                            }
//                            saveData(sensorNo + "倾斜".hashCode(), measurementXPQ, fieldsX);
//                        }
//
//                        String measurementYPQ = "tbs_ptqcbxy_wy_y";
//                        String dsIdYPQ = "3345_0_5703";
//                        int oldCountYPQ = influxDBConnect.countSensorData(sensorNo + "倾斜".hashCode(), measurementYPQ);
//                        int newCountYPQ = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdYPQ);
//                        if (oldCountYPQ != newCountYPQ && newCountYPQ > oldCountYPQ) {
//                            List<Map<String, Object>> fieldsY = dataCmccMapper.selectQingXieY(sensorNo, oldCountYPQ, newCountYPQ - oldCountYPQ);
//                            for (Map<String, Object> mapField : fieldsY) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                value = value > 180.0 ? (value - 360.0) : value;
//                                //告警判断
//                                String currentTime = mapField.get("currentTime").toString();
//                                String time = timestampToString(currentTime);
//                                vo.put(sensorNo + "倾斜".hashCode(), "time", time);
//                                vo.put(sensorNo + "倾斜".hashCode(), "value", value.toString());
//                                warnPush(sensorNo + "倾斜".hashCode(), "", currentTime, value);
//                                String format = String.format("%.3f", value);
//                                mapField.put("value", format);
//                            }
//                            saveData(sensorNo + "倾斜".hashCode(), measurementYPQ, fieldsY);
//                        }
//
//                        String measurementZPQ = "tbs_ptqcbxy_wy_z";
//                        String dsIdZPQ = "3345_0_5704";
//                        int oldCountZPQ = influxDBConnect.countSensorData(sensorNo + "倾斜".hashCode(), measurementZPQ);
//                        int newCountZPQ = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdZPQ);
//                        if (oldCountZPQ != newCountZPQ && newCountZPQ > oldCountZPQ) {
//                            List<Map<String, Object>> fieldsZ = dataCmccMapper.selectQingXieZ(sensorNo, oldCountZPQ, newCountZPQ - oldCountZPQ);
//                            for (Map<String, Object> mapField : fieldsZ) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                value = value > 180.0 ? (value - 360.0) : value;
//                                //告警判断
//                                String currentTime = mapField.get("currentTime").toString();
//                                String time = timestampToString(currentTime);
//                                vo.put(sensorNo + "倾斜".hashCode(), "time", time);
//                                vo.put(sensorNo + "倾斜".hashCode(), "value", value.toString());
//                                warnPush(sensorNo + "倾斜".hashCode(), "", currentTime, value);
//                                String format = String.format("%.3f", value);
//                                mapField.put("value", format);
//                            }
//                            saveData(sensorNo + "倾斜".hashCode(), measurementZPQ, fieldsZ);
//                        }
//
//                        String measurementWXLX = "tbs_ptqcbxy_lx_x";
//                        String dsIdWXLX = "3330_0_5700";
//                        int oldCountWXLX = influxDBConnect.countSensorData(sensorNo + "拉线".hashCode(), measurementWXLX);
//                        int newCountWXLX = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdWXLX);
//                        if (oldCountWXLX != newCountWXLX && newCountWXLX > oldCountWXLX) {
//                            List<Map<String, Object>> fieldsZ = dataCmccMapper.selectQingXieWX(sensorNo, oldCountWXLX, newCountWXLX - oldCountWXLX);
//                            for (Map<String, Object> mapField : fieldsZ) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                value = value > 4000 ? (value - 5000) : value;
//                                //告警判断
//                                String currentTime = mapField.get("currentTime").toString();
//                                String time = timestampToString(currentTime);
//                                vo.put(sensorNo + "拉线".hashCode(), "time", time);
//                                vo.put(sensorNo + "拉线".hashCode(), "value", value.toString());
//                                warnPush(sensorNo + "拉线".hashCode(), "", currentTime, value);
//                                String format = String.format("%.3f", value);
//                                mapField.put("value", format);
//                            }
//                            saveData(sensorNo + "拉线".hashCode(), measurementWXLX, fieldsZ);
//                        }
//
//                        String measurementWXLY = "tbs_ptqcbxy_lx_y";
//                        String dsIdWXLY = "3330_1_5700";
//                        int oldCountWXLY = influxDBConnect.countSensorData(sensorNo + "拉线".hashCode(), measurementWXLY);
//                        int newCountWXLY = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdWXLY);
//                        if (oldCountWXLY != newCountWXLY && newCountWXLY > oldCountWXLY) {
//                            List<Map<String, Object>> fieldsY = dataCmccMapper.selectQingXieWY(sensorNo, oldCountWXLY, newCountWXLY - oldCountWXLY);
//                            for (Map<String, Object> mapField : fieldsY) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                value = value > 4000 ? (value - 5000) : value;
//                                //告警判断
//                                String currentTime = mapField.get("currentTime").toString();
//                                String time = timestampToString(currentTime);
//                                vo.put(sensorNo + "拉线".hashCode(), "time", time);
//                                vo.put(sensorNo + "拉线".hashCode(), "value", value.toString());
//                                warnPush(sensorNo + "拉线".hashCode(), "", currentTime, value);
//                                String format = String.format("%.3f", value);
//                                mapField.put("value", format);
//                            }
//                            saveData(sensorNo + "拉线".hashCode(), measurementWXLY, fieldsY);
//                        }
//
//                        String measurementWXLZ = "tbs_ptqcbxy_lx_z";
//                        String dsIdWXLZ = "3330_2_5700";
//                        int oldCountWXLZ = influxDBConnect.countSensorData(sensorNo + "拉线".hashCode(), measurementWXLZ);
//                        int newCountWXLZ = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdWXLZ);
//                        if (oldCountWXLZ != newCountWXLZ && newCountWXLZ > oldCountWXLZ) {
//                            List<Map<String, Object>> fieldsZ = dataCmccMapper.selectQingXieWZ(sensorNo, oldCountWXLZ, newCountWXLZ - oldCountWXLZ);
//                            for (Map<String, Object> mapField : fieldsZ) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                value = value > 4000 ? (value - 5000) : value;
//                                //告警判断
//                                String currentTime = mapField.get("currentTime").toString();
//                                String time = timestampToString(currentTime);
//                                vo.put(sensorNo + "拉线".hashCode(), "time", time);
//                                vo.put(sensorNo + "拉线".hashCode(), "value", value.toString());
//                                warnPush(sensorNo + "拉线".hashCode(), "", currentTime, value);
//                                String format = String.format("%.3f", value);
//                                mapField.put("value", format);
//                            }
//                            saveData(sensorNo + "拉线".hashCode(), measurementWXLZ, fieldsZ);
//                        }
//                        break;
//                    case "DLF":
//                        String measurementDLX = "tbs_liefeng_qj_x";
//                        String dsIdDLX = "3345_0_5702";
//                        int oldCountDLX = influxDBConnect.countSensorData(sensorNo + "倾斜".hashCode(), measurementDLX);
//                        int newCountDLX = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdDLX);
//                        if (oldCountDLX != newCountDLX && newCountDLX > oldCountDLX) {
//                            List<Map<String, Object>> fieldsX = dataCmccMapper.selectQingXieX(sensorNo, oldCountDLX, newCountDLX - oldCountDLX);
//                            List<Map<String, String>> lList = new ArrayList<>(16);
//                            Map<String, Object> lAllMap = new HashMap<>(16);
//                            Map<String, Object> post = HttpClientUtil.post(lgdTokenUrl, "", "");
//                            String token = post.get("token").toString();
//                            System.out.println("MYMAP=================================================" + fieldsX);
//                            System.out.println("xxxxx=新数据===================" + newCountDLX + "   旧数据===================" + oldCountDLX + "   size======" + fieldsX.size());
//                            System.out.println("传感器编号==========================" + sensorNo);
//                            for (Map<String, Object> mapField : fieldsX) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                //告警判断
//                                String currentTime = mapField.get("currentTime") + "";
//                                String time = timestampToString(currentTime);
//                                warnPush(sensorNo + "倾斜".hashCode(), "x", currentTime, value);
//
//                                vo.put(sensorNo + "倾斜".hashCode() + "x", "time", time);
//                                vo.put(sensorNo + "倾斜".hashCode() + "x", "value", value.toString());
////                                String deviceNo = tabSensorMapper.selectDeviceNoBySensorNo(sensorNo + "倾斜".hashCode());
////
////                                if (deviceNo.equals("0F030000000000A6")) {
////                                    Map<String, Object> mapPull = new HashMap(16);
////                                    mapPull.put("sensorNo", sensorNo + "倾斜".hashCode());
////                                    mapPull.put("deviceNo", deviceNo);
////                                    mapPull.put("type", 40);
////                                    mapPull.put("x", value);
////                                    mapPull.put("time", time);
////                                    mapPull.put("key", "x");
////                                    String s = JSON.toJSONString(mapPull);
////                                    try {
////                                        webSocketServer.sendMsgToAll(s, "10");
////                                    } catch (IOException e) {
////                                        e.printStackTrace();
////                                    }
////                                }
//
//                                //数据推送 理工大
//                                Map<String, String> lMap = new HashMap<>(16);
//                                String sensorId = tabMonitorMapper.getMonitorNobySensorNo(sensorNo);
//                                lMap.put("dataSource", "hh");
//                                lMap.put("sensorType", "LF");
//                                lMap.put("time", time);
//                                lMap.put("v1", value.toString());
//                                lMap.put("sensorId", sensorId);
//                                lList.add(lMap);
//                            }
//                            lAllMap.put("dataSet", lList);
//                            String s = JSONArray.toJSONString(lAllMap);
//                            Map<String, Object> resultMap = HttpClientUtil.post(lgdPushUrl, s, token);
//                            System.out.println(resultMap);
//                            saveData(sensorNo + "倾斜".hashCode(), measurementDLX, fieldsX);
//                        }
//
//                        String measurementDLY = "tbs_liefeng_qj_y";
//                        String dsIdDLY = "3345_0_5703";
//                        int oldCountDLY = influxDBConnect.countSensorData(sensorNo + "倾斜".hashCode(), measurementDLY);
//                        int newCountDLY = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdDLY);
//                        if (oldCountDLY != newCountDLY && newCountDLY > oldCountDLY) {
//                            List<Map<String, Object>> fieldsY = dataCmccMapper.selectQingXieY(sensorNo, oldCountDLY, newCountDLY - oldCountDLY);
//                            List<Map<String, String>> lList = new ArrayList<>(16);
//                            Map<String, Object> lAllMap = new HashMap<>(16);
//                            Map<String, Object> post = HttpClientUtil.post(lgdTokenUrl, "", "");
//                            String token = post.get("token").toString();
//                            System.out.println("yyyy新数据===================" + newCountDLY + "   旧数据===================" + oldCountDLY + "   size======" + fieldsY.size());
//                            System.out.println("MYMAP=================================================" + fieldsY);
//                            System.out.println("传感器编号==========================" + sensorNo);
//                            for (Map<String, Object> mapField : fieldsY) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                //告警判断
//                                String currentTime = mapField.get("currentTime") + "";
//                                String time = timestampToString(currentTime);
//                                warnPush(sensorNo + "倾斜".hashCode(), "y", currentTime, value);
//                                vo.put(sensorNo + "倾斜".hashCode() + "y", "time", time);
//                                vo.put(sensorNo + "倾斜".hashCode() + "y", "value", value.toString());
//
////                                String deviceNo = tabSensorMapper.selectDeviceNoBySensorNo(sensorNo + "倾斜".hashCode());
////
////                                if (deviceNo.equals("0F030000000000A6")) {
////                                    Map<String, Object> mapPull = new HashMap(16);
////                                    mapPull.put("sensorNo", sensorNo + "倾斜".hashCode());
////                                    mapPull.put("deviceNo", deviceNo);
////                                    mapPull.put("type", 40);
////                                    mapPull.put("y", value);
////                                    mapPull.put("time", time);
////                                    mapPull.put("key", "y");
////                                    String s = JSON.toJSONString(mapPull);
////                                    try {
////                                        webSocketServer.sendMsgToAll(s, "10");
////                                    } catch (IOException e) {
////                                        e.printStackTrace();
////                                    }
////                                }
//                                //数据推送 理工大
//                                Map<String, String> lMap = new HashMap<>(16);
//                                String sensorId = tabMonitorMapper.getMonitorNobySensorNo(sensorNo);
//                                lMap.put("dataSource", "hh");
//                                lMap.put("sensorType", "LF");
//                                lMap.put("time", time);
//                                lMap.put("v1", value.toString());
//                                lMap.put("sensorId", sensorId);
//                                lList.add(lMap);
//                            }
//                            lAllMap.put("dataSet", lList);
//                            String s = JSONArray.toJSONString(lAllMap);
//                            Map<String, Object> resultMap = HttpClientUtil.post(lgdPushUrl, s, token);
//                            System.out.println(resultMap);
//                            saveData(sensorNo + "倾斜".hashCode(), measurementDLY, fieldsY);
//                        }
//
//                        String measurementDLZ = "tbs_liefeng_qj_z";
//                        String dsIdDLZ = "3345_0_5704";
//                        int oldCountDLZ = influxDBConnect.countSensorData(sensorNo + "倾斜".hashCode(), measurementDLZ);
//                        int newCountDLZ = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdDLZ);
//                        if (oldCountDLZ != newCountDLZ && newCountDLZ > oldCountDLZ) {
//                            List<Map<String, Object>> fieldsZ = dataCmccMapper.selectQingXieZ(sensorNo, oldCountDLZ, newCountDLZ - oldCountDLZ);
//                            List<Map<String, String>> lList = new ArrayList<>(16);
//                            Map<String, Object> lAllMap = new HashMap<>(16);
//                            Map<String, Object> post = HttpClientUtil.post(lgdTokenUrl, "", "");
//                            String token = post.get("token").toString();
//                            System.out.println("zzzz新数据===================" + newCountDLZ + "   旧数据===================" + oldCountDLZ + "   size======" + fieldsZ.size());
//                            System.out.println("MYMAP=================================================" + fieldsZ);
//                            System.out.println("传感器编号==========================" + sensorNo);
//                            for (Map<String, Object> mapField : fieldsZ) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                //告警判断
//                                String currentTime = mapField.get("currentTime") + "";
//                                String time = timestampToString(currentTime);
//                                warnPush(sensorNo + "倾斜".hashCode(), "z", currentTime, value);
//                                vo.put(sensorNo + "倾斜".hashCode() + "z", "time", time);
//                                vo.put(sensorNo + "倾斜".hashCode() + "z", "value", value.toString());
//
////                                String deviceNo = tabSensorMapper.selectDeviceNoBySensorNo(sensorNo + "倾斜".hashCode());
////
////                                if (deviceNo.equals("0F030000000000A6")) {
////                                    Map<String, Object> mapPull = new HashMap(16);
////                                    mapPull.put("sensorNo", sensorNo + "倾斜".hashCode());
////                                    mapPull.put("deviceNo", deviceNo);
////                                    mapPull.put("type", 40);
////                                    mapPull.put("z", value);
////                                    mapPull.put("time", time);
////                                    mapPull.put("key", "z");
////                                    String s = JSON.toJSONString(mapPull);
////                                    try {
////                                        webSocketServer.sendMsgToAll(s, "10");
////                                    } catch (IOException e) {
////                                        e.printStackTrace();
////                                    }
////                                }
//                                //数据推送 理工大
//                                Map<String, String> lMap = new HashMap<>(16);
//                                String sensorId = tabMonitorMapper.getMonitorNobySensorNo(sensorNo);
//                                lMap.put("dataSource", "hh");
//                                lMap.put("sensorType", "LF");
//                                lMap.put("time", time);
//                                lMap.put("v1", value.toString());
//                                lMap.put("sensorId", sensorId);
//                                lList.add(lMap);
//                            }
//                            lAllMap.put("dataSet", lList);
//                            String s = JSONArray.toJSONString(lAllMap);
//                            Map<String, Object> resultMap = HttpClientUtil.post(lgdPushUrl, s, token);
//                            System.out.println(resultMap);
//                            saveData(sensorNo + "倾斜".hashCode(), measurementDLZ, fieldsZ);
//                        }
//
//                        String measurementDLLX = "tbs_liefeng_lx";
//                        String dsIdDLLX = "3330_0_5700";
//                        int oldCountDLLX = influxDBConnect.countSensorData(sensorNo + "拉线".hashCode(), measurementDLLX);
//                        int newCountDLLX = dataCmccMapper.selectCountBySensorNo(sensorNo, dsIdDLLX);
//                        if (oldCountDLLX != newCountDLLX && newCountDLLX > oldCountDLLX) {
//                            List<Map<String, Object>> fieldsLX = dataCmccMapper.selectLX(sensorNo, oldCountDLLX, newCountDLLX - oldCountDLLX);
//                            List<Map<String, String>> lList = new ArrayList<>(16);
//                            Map<String, Object> lAllMap = new HashMap<>(16);
//                            Map<String, Object> post = HttpClientUtil.post(lgdTokenUrl, "", "");
//                            String token = post.get("token").toString();
//                            System.out.println("llllll新数据===================" + newCountDLLX + "   旧数据===================" + oldCountDLLX + "   size======" + fieldsLX.size());
//                            System.out.println("MYMAP=================================================" + fieldsLX);
//                            System.out.println("传感器编号==========================" + sensorNo);
//                            for (Map<String, Object> mapField : fieldsLX) {
//                                Double value = Double.valueOf(mapField.get("value").toString());
//                                //告警判断
//                                String currentTime = mapField.get("currentTime") + "";
//                                String time = timestampToString(currentTime);
//                                warnPush(sensorNo + "拉线".hashCode(), "", currentTime, value);
//
//                                vo.put(sensorNo + "拉线".hashCode(), "time", time);
//                                vo.put(sensorNo + "拉线".hashCode(), "value", value.toString());
//
////                                String deviceNo = tabSensorMapper.selectDeviceNoBySensorNo(sensorNo + "拉线".hashCode());
////
////                                if (deviceNo.equals("0F030000000000A6")) {
////                                    Map<String, Object> mapPull = new HashMap(16);
////                                    mapPull.put("sensorNo", sensorNo + "拉线".hashCode());
////                                    mapPull.put("deviceNo", deviceNo);
////                                    mapPull.put("type", 43);
////                                    mapPull.put("l", value);
////                                    mapPull.put("time", time);
////                                    mapPull.put("key", "l");
////                                    String s = JSON.toJSONString(mapPull);
////                                    try {
////                                        webSocketServer.sendMsgToAll(s, "10");
////                                    } catch (IOException e) {
////                                        e.printStackTrace();
////                                    }
////                                }
//                                //数据推送 理工大
//                                Map<String, String> lMap = new HashMap<>(16);
//                                String sensorId = tabMonitorMapper.getMonitorNobySensorNo(sensorNo);
//                                lMap.put("dataSource", "hh");
//                                lMap.put("sensorType", "QX");
//                                lMap.put("time", time);
//                                lMap.put("v1", value.toString());
//                                lMap.put("sensorId", sensorId);
//                                lList.add(lMap);
//                            }
//                            lAllMap.put("dataSet", lList);
//                            String s = JSONArray.toJSONString(lAllMap);
//                            Map<String, Object> resultMap = HttpClientUtil.post(lgdPushUrl, s, token);
//                            System.out.println(resultMap);
//                            saveData(sensorNo + "拉线".hashCode(), measurementDLLX, fieldsLX);
//                        }
//                        break;
//                    default:
//                        break;
//                }
//            }
//        }
//    }
//
////    @Async("taskExecutor")
////    @Scheduled(initialDelay = 8000, fixedRate = 5000)
//    public void deviceOnline() {
//        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
//        Set<String> keys = stringRedisTemplate.keys("*");
//        for (String key : keys) {
//            String deviceNo = tabDeviceMapper.selectDeviceNoBySensorNo(key);
//            if (null != deviceNo) {
//                Map<Object, Object> entries = vo.entries(key);
//                String time = entries.get("time").toString();
//                //离线天数
//                long l = timeDifference(time);
////                if (null == map.get(deviceNo) || "0".endsWith(map.get(deviceNo))) {
//                map.put(deviceNo, l + "");
////                } else {
////                    String day = map.get(deviceNo);
////                    Long aLong = Long.valueOf(day);
////                    map.put(deviceNo, (aLong > l ? l : day) + "");
////                }
//            }
//        }
////        vo.putAll("deviceOnline", map);
//        Set<String> strings = map.keySet();
//        for (String string : strings) {
//            TabOnline tabOnline = new TabOnline();
//            tabOnline.setDeviceNo(string);
//            tabOnline.setOfflineTime(BigDecimal.valueOf((Long.valueOf(map.get(string)))));
//            TabOnline tabOnlineInfo = tabOnlineMapper.selectDeviceByNo(string);
//            if (tabOnlineInfo != null) {
//                tabOnline.setId(tabOnlineInfo.getId());
//                tabOnlineMapper.updateByPrimaryKeySelective(tabOnline);
//            } else {
//                tabOnlineMapper.insertSelective(tabOnline);
//                int i = tabDeviceExceptionMapper.selectDevice(string);
//                if (i == 0) {
//                    TabDeviceException tabDeviceException = new TabDeviceException();
//                    tabDeviceException.setDeviceNo(string);
//                    tabDeviceException.setDealState("1");
//                    tabDeviceException.setExceptionType("1");
//                    tabDeviceException.setCreateTime(new Date());
//                    tabDeviceExceptionMapper.insert(tabDeviceException);
//                }
//            }
//        }
//    }
//
////    @Async("taskExecutor")
////    @Scheduled(initialDelay = 10000, fixedRate = 300000)
//    public void gnssTimer() {
//        List<Map<String, String>> mapList = new ArrayList<>(16);
//        String account = "{\"ClientType\":\"web\",\"password\":\"f934ac120abba45c1430ed23e13b3635\",\"userNameOrEmailAddress\":\"TrialUser\",\"vercode\":\"\"}";
//        Map<String, Object> post = HttpClientUtil.post(tokenUrl, account, "");
//        Map<String, String> result = (Map<String, String>) post.get("result");
//        String accessToken = result.get("accessToken");
//
//        String deviceUrl = "http://mon.zhdbds.com:30007/api/services/app/Project/GetDevicesByProjectID?ProjectId=49&MonType=13&getType=0&serchType=-1&timeRange=2019-05-01%2000:00:00%20-%202019-05-01%2000:10:00&tmieType=0&deviceId=0";
//        Map<String, Object> deviceMap = HttpClientUtil.get(deviceUrl, "Bearer " + accessToken);
//        List<Map<String, Object>> deviceList = (List<Map<String, Object>>) deviceMap.get("jsonDevices");
//
//        Map<String, String> recordTimeMap = null;
//        String newRecordTime = null;
//        try {
//            recordTimeMap = UpdatePropertyUtil.getValue(recordTimePath, "recordTime");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        String recordTime = recordTimeMap.get("recordTime");
//        try {
//            newRecordTime = getNewRecordTime(recordTime);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//
//        for (Map<String, Object> device : deviceList) {
//            String id = device.get("id").toString();
//            String deviceName = device.get("device_name").toString();
//
//            if (null != newRecordTime && isOverTime(newRecordTime)) {
//                String replaceRecordTime = recordTime.replace(" ", "%20");
//                String replaceNewRecordTime = newRecordTime.replace(" ", "%20");
//                Map<String, Object> dataMap = HttpClientUtil.get(gnssDataUrl + id + "&timeRange=" + replaceRecordTime + "%20-%20" + replaceNewRecordTime, "Bearer " + accessToken);
//                List<Map<String, Object>> contentsList = (List<Map<String, Object>>) dataMap.get("contents");
//                if (contentsList.size() > 0) {
//                    List<List<String>> list = (List<List<String>>) contentsList.get(0).get("toShowTable");
//                    for (List<String> strings : list) {
//                        Map temp = new HashMap(16);
//                        for (int i = 0; i < strings.size(); i++) {
//                            temp.put(getGnssKey(i), strings.get(i));
//                        }
//                        temp.put("sensor_no", deviceName);
//                        temp.put("type", "4");
//                        mapList.add(temp);
//                    }
//                }
//            }
//        }
//        if (isOverTime(newRecordTime)) {
//            try {
//                UpdatePropertyUtil.setValue(recordTimePath, newRecordTime, "recordTime");
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            String s = JSON.toJSONString(mapList);
//            rabbitTemplate.convertAndSend("test2", s);
//        }
//    }
//
//
//    public long timeDifference(String time) {
//
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date d1 = null;
//        try {
//            d1 = df.parse(time);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        //这样得到的差值是微秒级别
//        long diff = System.currentTimeMillis() - d1.getTime();
//        long days = diff / (1000 * 60 * 60 * 24);
//        long minute = diff / (1000 * 60 * 60);
//        return minute;
//    }
//
//    public String getNewRecordTime(String recordTime) throws ParseException {
//        //得到当前时间
//        Calendar nowTime = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        //设置成这个时间
//        nowTime.setTime(sdf.parse(recordTime));
//        nowTime.add(Calendar.MINUTE, addTime);
//        String newRecordTime = sdf.format(nowTime.getTime());
//        return newRecordTime;
//    }
//
//    public boolean isOverTime(String newRecordTime) {
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        Date d1 = null;
//        try {
//            d1 = df.parse(newRecordTime);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        //这样得到的差值是微秒级别
//        long diff = System.currentTimeMillis() - d1.getTime();
//        long minute = diff / (1000 * 60);
//        if (minute >= addTime) {
//            return true;
//        }
//        return false;
//    }
//
//    public static void main(String[] args) {
////        String url = "http://mon.zhdbds.com:30004/api/services/app/Project/GetDevicesByProjectID?ProjectId=4&MonType=13&getType=0&serchType=-1&tmieType=0&timeRange=2019-04-28%2010:27:28%20-%202019-05-01%2010:27:28&deviceId=386";
////        String token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjkxIiwiaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvd3MvMjAwNS8wNS9pZGVudGl0eS9jbGFpbXMvbmFtZSI6IlRyaWFsVXNlciIsIkFzcE5ldC5JZGVudGl0eS5TZWN1cml0eVN0YW1wIjoiMzcxYjI2YWUtOGE3OS1iOTU0LWM1ODYtMzllYzFjYWRkN2FhIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiQWRtaW4iLCJodHRwOi8vd3d3LmFzcG5ldGJvaWxlcnBsYXRlLmNvbS9pZGVudGl0eS9jbGFpbXMvdGVuYW50SWQiOiIxIiwic3ViIjoiOTEiLCJqdGkiOiIyYjYyNzZmNS04YjNkLTRhYWYtYjZiMC0xMjZlYjQxZWI1ZTciLCJpYXQiOjE1NTc4MjM1NjgsIm5iZiI6MTU1NzgyMzU2OCwiZXhwIjoxNTU3ODM0MzY4LCJpc3MiOiJzYWZlTW9uaXRvciIsImF1ZCI6InNhZmVNb25pdG9yIn0.kswTo0cMeKXTxX8Sqg5L0WDhRB4xAOBo_ddVtgBXgFU";
////        Map<String, Object> map = HttpClientUtil.get(url, token);
////        List<Map<String, String>> mapList = new ArrayList<>(16);
////        List<Map<String, Object>> map1 = (List<Map<String, Object>>) map.get("contents");
////        List<List<String>> list = (List<List<String>>) map1.get(0).get("toShowTable");
////        for (List<String> strings : list) {
////            Map map2 = new HashMap(16);
////            for (int i = 0; i < strings.size(); i++) {
////                map2.put(getGnssKey(i), strings.get(i));
////            }
////            mapList.add(map2);
////        }
////        String s = JSON.toJSONString(mapList);
////        System.out.println(s);
//
////        String account = "{\"ClientType\":\"web\",\"password\":\"f934ac120abba45c1430ed23e13b3635\",\"userNameOrEmailAddress\":\"TrialUser\",\"vercode\":\"\"}";
////        Map<String, Object> post = HttpClientUtil.post("http://mon.zhdbds.com:30007/api/TokenAuth/Authenticate", account, "");
////        System.out.println(post);
//
//
//        Map<String, Object> map6 = HttpClientUtil.post("http://183.224.17.135:1080/api/api/security/token?username=datasync&password=datasync123456", "", "");
//        String token = map6.get("token").toString();
//        Map<String, Object> map = new HashMap<>(16);
////        List<Map<String, String>> list = new ArrayList<>(16);
////        Map<String, String> map2 = new HashMap<>(16);
////        Map<String, String> map3 = new HashMap<>(16);
////        map2.put("sd", "sda");
////        map2.put("sdd", "sdad");
////        map3.put("ff", "ff");
////        map3.put("ww", "ww");
////        list.add(map2);
////        list.add(map3);
////        map.put("dataSet", list);
////        String s = "{\"dataSet\":[{\"dataSource\":\"tt\",\"sensorId\":\"test\",\"sensorType\":\"YL\",\"time\":\"2019-06-20 10:33:33\",\"v1\":\"0\"},{\"dataSource\":\"tt\",\"sensorId\":\"test2\",\"sensorType\":\"CL\",\"time\":\"2019-06-20 10:33:44\",\"v1\":\"2\"}]}";
////        System.out.println(s);
////        Map<String, Object> post = HttpClientUtil.post("http://183.224.17.135:1080/api/api/datasync/push", s, token);
////        System.out.println(post);
//
//    }
//
//    public static String getGnssKey(int i) {
//        switch (i) {
//            case 0:
//                return "time";
//            case 1:
//                return "X";
//            case 2:
//                return "Y";
//            case 3:
//                return "H";
//            case 4:
//                return "ax";
//            case 5:
//                return "ay";
//            case 6:
//                return "ah";
//            default:
//                return "";
//        }
//    }
//
//    public void saveData(String sensorNo, String measurement, List<Map<String, Object>> fields) {
//        Map<String, String> tags = new HashMap<>(16);
//        tags.put("sensor_no", sensorNo);
//        for (Map<String, Object> field : fields) {
//            influxDBConnect.insert(measurement, tags, field);
//        }
//    }
//
//
//    public void warnPush(String sensorNo, String temp, String time, Double value) {
//
//        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
//        Map<Object, Object> entries = vo.entries(sensorNo + temp);
//        if (entries.size() != 0) {
//            String time1 = entries.get("time").toString();
//            Double value1 = Double.valueOf(entries.get("value").toString());
//
//            Map<String, String> map = new HashMap<>(16);
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            String format = timestampToString(time);
//
//            System.out.println("当前时间=======" + format + "   上一次时间==========" + time1);
//
//            map.put("sensorNo", sensorNo);
//            map.put("time", format);
//            Map<String, BigDecimal> alarmMap = tabWarnSettingMapper.selectAlarm(sensorNo);
//            Date warnTime = null;
//            try {
//                warnTime = sdf.parse(format);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//            int level = getCuAlarmLevel(alarmMap, value);
//
//            if (level > 0) {
//                int monitorId = tabMonitorMapper.selectIdBySeonsorNo(sensorNo);
//                map.put("level", String.valueOf(level));
//                map.put("monitorId", String.valueOf(monitorId));
//                map.put("value", String.valueOf(value));
//                map.put("type", "1");
//                String s = JSON.toJSONString(map);
//                try {
//                    webSocketServer.sendMsgToAll(s, "10");
//                    insertAlarm(sensorNo, warnTime, level, value, monitorId, 1);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            int adAlarmLevel = getAdAlarmLevel(alarmMap, value, value1);
//            if (adAlarmLevel > 0) {
//                if (adAlarmLevel == 0) {
//                    System.out.println(adAlarmLevel);
//                }
//                int monitorId = tabMonitorMapper.selectIdBySeonsorNo(sensorNo);
//                map.put("level", String.valueOf(adAlarmLevel));
//                map.put("monitorId", String.valueOf(monitorId));
//                map.put("value", String.valueOf(value));
//                map.put("type", "2");
//                String s = JSON.toJSONString(map);
//                try {
//                    webSocketServer.sendMsgToAll(s, "10");
//                    insertAlarm(sensorNo, warnTime, adAlarmLevel, value, monitorId, 2);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            Map<String, Object> spMap = getSpAlarmLevel(alarmMap, value, value1, format, time1);
//            System.out.println("spMap======================================" + spMap);
//            if (null != spMap) {
//                if (Integer.valueOf(spMap.get("level").toString()) > 0) {
//                    String deviceNo = tabSensorMapper.selectDeviceNoBySensorNo(sensorNo);
////                int i = tabDeviceExceptionMapper.selectDevice(deviceNo);
////                if (i == 0) {
//                    TabDeviceException tabDeviceException = new TabDeviceException();
//                    tabDeviceException.setCreateTime(new Date());
//                    tabDeviceException.setExceptionType("2");
//                    tabDeviceException.setDeviceNo(deviceNo);
//                    tabDeviceException.setDataException("1");
//                    tabDeviceException.setDealState("1");
//                    tabDeviceException.setExceptionTimeFrom(time1);
//                    tabDeviceException.setExceptionTimeTo(format);
//                    tabDeviceException.setExceptionDataCurrent(value.toString());
//                    tabDeviceException.setExceptionDataPre(value1.toString());
//                    tabDeviceException.setRateChange(spMap.get("rate").toString());
//                    tabDeviceExceptionMapper.insert(tabDeviceException);
////                }
//                }
//            }
//        }
//    }
//
//    private int getCuAlarmLevel(Map<String, BigDecimal> alarmMap, Double value) {
//        Double cuAlarm1;
//        Double cuAlarm2;
//        Double cuAlarm3;
//        Double cuAlarm4;
//        try {
//            cuAlarm1 = Double.valueOf(alarmMap.get("cu_alarm1").toString());
//            cuAlarm2 = Double.valueOf(alarmMap.get("cu_alarm2").toString());
//            cuAlarm3 = Double.valueOf(alarmMap.get("cu_alarm3").toString());
//            cuAlarm4 = Double.valueOf(alarmMap.get("cu_alarm4").toString());
//        } catch (Exception e) {
//            return 0;
//        }
//
//        return value > cuAlarm1 ? 1 : (value > cuAlarm2 ? 2 : (value > cuAlarm3 ? 3 : (value > cuAlarm4 ? 4 : 0)));
//    }
//
//    private int getAdAlarmLevel(Map<String, BigDecimal> alarmMap, Double value, Double lastValue) {
//        Double adAlarm1;
//        Double adAlarm2;
//        Double adAlarm3;
//        Double adAlarm4;
//        try {
//            adAlarm1 = Double.valueOf(alarmMap.get("ad_alarm1").toString());
//            adAlarm2 = Double.valueOf(alarmMap.get("ad_alarm2").toString());
//            adAlarm3 = Double.valueOf(alarmMap.get("ad_alarm3").toString());
//            adAlarm4 = Double.valueOf(alarmMap.get("ad_alarm4").toString());
//        } catch (Exception e) {
//            return 0;
//        }
//
//        value = value - lastValue;
//
//        return value > adAlarm1 ? 1 : (value > adAlarm2 ? 2 : (value > adAlarm3 ? 3 : (value > adAlarm4 ? 4 : 0)));
//    }
//
//    private Map<String, Object> getSpAlarmLevel(Map<String, BigDecimal> alarmMap, Double value, Double lastValue, String time, String lastTime) {
//        Map<String, Object> map = new HashMap<>(16);
//        Double v;
//        Double spAlarm1;
//        Double spAlarm2;
//        Double spAlarm3;
//        Double spAlarm4;
//        try {
//            spAlarm1 = Double.valueOf(alarmMap.get("sp_alarm1").toString());
//            spAlarm2 = Double.valueOf(alarmMap.get("sp_alarm2").toString());
//            spAlarm3 = Double.valueOf(alarmMap.get("sp_alarm3").toString());
//            spAlarm4 = Double.valueOf(alarmMap.get("sp_alarm4").toString());
//        } catch (Exception e) {
//            return null;
//        }
//        long diff = 0;
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        try {
//            Date myTime = df.parse(time);
//            Date myLastTime = df.parse(lastTime);
//            diff = myTime.getTime() - myLastTime.getTime();
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        Double s = diff / (1000.0 * 60 * 60);
//        if (s == 0) {
//            return null;
//        }
//        v = (value - lastValue) / s;
//        int i = v > spAlarm1 ? 1 : (v > spAlarm2 ? 2 : (v > spAlarm3 ? 3 : (v > spAlarm4 ? 4 : 0)));
//
//        map.put("level", i);
//        map.put("value", value);
//        map.put("lastValue", lastValue);
//        map.put("time", time);
//        map.put("lastTime", lastTime);
//        map.put("rate", v);
//
//        return map;
//    }
//
//
//    private void insertAlarm(String sensorNo, Date warnTime, int level, Double value, int monitorId, int type) {
//        TabAlarmInfo tabAlarmInfo = new TabAlarmInfo();
//        tabAlarmInfo.setSensorNo(sensorNo);
//        tabAlarmInfo.setMonitoringId(monitorId);
//        tabAlarmInfo.setTime(warnTime);
//        tabAlarmInfo.setValue(value);
//        tabAlarmInfo.setLevel(level);
//        tabAlarmInfo.setAlarmType(type);
//        tabAlarmInfoMapper.insertSelective(tabAlarmInfo);
//    }
//
//    private static String timestampToString(String timestamp) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String format = sdf.format(new Date(Long.valueOf(timestamp) * 1000));
//        return format;
//    }
//
//
//}
