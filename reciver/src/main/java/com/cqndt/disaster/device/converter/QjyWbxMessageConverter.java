package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TbsQjyWbxMapper;
import com.cqndt.disaster.device.domain.TbsQjyWbx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Service
public class QjyWbxMessageConverter implements DeviceMessageConverter{
    @Autowired
    TbsQjyWbxMapper tbsQjyWbxMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 2;
    }

    @Override
    public Object messageConvert(String json) throws Exception{
        System.out.println("device type is "+getDeviceType()+" "+json);

        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TbsQjyWbx tbsQjyWbx = new TbsQjyWbx();
        tbsQjyWbx.setTime(acqTime);
        tbsQjyWbx.setSensorNo(map.get("number"));
        tbsQjyWbx.setQxjd(BigDecimal.valueOf(Double.valueOf(map.get("value").equals(" ")?"0":map.get("value"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsQjyWbx;
    }
}
