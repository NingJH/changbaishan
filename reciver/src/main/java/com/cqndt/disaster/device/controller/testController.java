package com.cqndt.disaster.device.controller;

import com.alibaba.fastjson.JSON;
import com.cqndt.disaster.device.dao.SysMenuMapper;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.utils.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author qingqinchao
 */
@RestController
public class testController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @GetMapping(value = "test")
    public void setMessage() {
        Map<String, Object> allMap = new HashMap(16);
        Map<String, Object> map1 = new HashMap(16);
        map1.put("name", "啊啊的");
        map1.put("age", 12);
        Map<String, Object> map2 = new HashMap(16);
        map2.put("wwwww", "Marc");
        map2.put("ddddd", 32);
        List<Map<String, Object>> mapList = new ArrayList(16);
        mapList.add(map1);
        mapList.add(map2);
        allMap.put("all", mapList);
        byte[] serialize = SerializationUtils.serialize(allMap);
        rabbitTemplate.convertAndSend("" +
                "", serialize);
    }

//    @RabbitListener(queues = "test")
//    public void getMessage(Message message) {
//        System.out.println(message);
//        byte[] body = message.getBody();
//        Map map = (Map) org.apache.commons.lang.SerializationUtils.deserialize(body);
//        System.out.println(map);
//    }

    public static void main(String[] args) {

        Map<String, Object> map1 = new HashMap(16);
        map1.put("name", "啊啊的");
        map1.put("age", 12);
        Map<String, Object> map2 = new HashMap(16);
        map2.put("wwwww", "Marc");
        map2.put("ddddd", 32);
        List<Map<String, Object>> mapList = new ArrayList(16);
        mapList.add(map1);
        mapList.add(map2);
        String s = JSON.toJSONString(mapList);
        System.out.println(s);
    }

}
