package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TbsQjyMapper;
import com.cqndt.disaster.device.domain.TbsQjy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Create by Intellij IDEA
 * 倾角计tbs_qjy
 * @Time : 2019-08-09 15:34
 **/
@Service
public class QjyMessageConverter implements DeviceMessageConverter {
    @Autowired
    TbsQjyMapper tbsQjyMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 25;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //（监测值-初始值）* 60
        BigDecimal a = new BigDecimal(60);
        BigDecimal x = BigDecimal.valueOf(Double.valueOf(map.get("x").equals(" ")?"0":map.get("x")));//（监测值-初始值）厂商已计算
        BigDecimal y = BigDecimal.valueOf(Double.valueOf(map.get("y").equals(" ")?"0":map.get("y")));//（监测值-初始值）厂商已计算
        TbsQjy tbsQjy = new TbsQjy();
        tbsQjy.setTime(acqTime);
        tbsQjy.setSensorNo(map.get("number"));
        //厂商推过来的值乘60
        tbsQjy.setX(x.multiply(a));//*60
        tbsQjy.setY(y.multiply(a));//*60
        tbsQjy.setZ(BigDecimal.valueOf(Double.valueOf(map.get("z").equals(" ")?"0":map.get("z"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsQjy;
    }
}
