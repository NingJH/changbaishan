package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TbsDbbxHpMapper;
import com.cqndt.disaster.device.domain.TbsDbbxHp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Service
public class DbbxHpMessageConverter implements DeviceMessageConverter {
    @Autowired
    TbsDbbxHpMapper tbsDbbxHpMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 3;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TbsDbbxHp tbsDbbxHp = new TbsDbbxHp();
        tbsDbbxHp.setTime(acqTime);
        tbsDbbxHp.setSensorNo(map.get("number"));
        tbsDbbxHp.setWyl(BigDecimal.valueOf(Double.valueOf(map.get("value").equals(" ")?"0":map.get("value"))));
        tbsDbbxHp.setX(BigDecimal.valueOf(Double.valueOf(map.get("x").equals(" ")?"0":map.get("x"))));
        tbsDbbxHp.setY(BigDecimal.valueOf(Double.valueOf(map.get("y").equals(" ")?"0":map.get("y"))));
        tbsDbbxHp.setZ(BigDecimal.valueOf(Double.valueOf(map.get("z").equals(" ")?"0":map.get("z"))));
        tbsDbbxHp.setTemp(BigDecimal.valueOf(Double.valueOf(map.get("temperature").equals(" ")?"0":map.get("temperature"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsDbbxHp;
    }
}
