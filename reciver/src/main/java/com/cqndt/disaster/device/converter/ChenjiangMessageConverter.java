package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TbsChenjiangMapper;
import com.cqndt.disaster.device.domain.TbsChenjiang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Service
public class ChenjiangMessageConverter implements DeviceMessageConverter {
    @Autowired
    TbsChenjiangMapper tbsChenjiangMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 6;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        TbsChenjiang tbsChenjiang = new TbsChenjiang();
        tbsChenjiang.setTime(acqTime);
        tbsChenjiang.setSensorNo(map.get("number"));
        tbsChenjiang.setX(BigDecimal.valueOf(Double.valueOf(map.get("x").equals(" ")?"0":map.get("x"))));
        tbsChenjiang.setY(BigDecimal.valueOf(Double.valueOf(map.get("y").equals(" ")?"0":map.get("y"))));
        tbsChenjiang.setZ(BigDecimal.valueOf(Double.valueOf(map.get("z").equals(" ")?"0":map.get("z"))));
        tbsChenjiang.setTemp(BigDecimal.valueOf(Double.valueOf(map.get("temperature").equals(" ")?"0":map.get("temperature"))));

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsChenjiang;
    }
}
