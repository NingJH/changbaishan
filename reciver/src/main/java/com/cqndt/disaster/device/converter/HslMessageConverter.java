package com.cqndt.disaster.device.converter;

import com.alibaba.fastjson.JSONArray;
import com.cqndt.disaster.device.dao.TabMonitorMapper;
import com.cqndt.disaster.device.dao.TbsHslMapper;
import com.cqndt.disaster.device.domain.TbsHsl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class HslMessageConverter implements DeviceMessageConverter {

    @Autowired
    TbsHslMapper tbsHslMapper;
    @Autowired
    private TabMonitorMapper tabMonitorMapper;
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Integer getDeviceType() {
        return 39;
    }

    @Override
    public Object messageConvert(String json) throws Exception {
        Map<String, String> map = (Map<String, String>) JSONArray.parse(json);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date acqTime = null;
        try {
            acqTime = sdf.parse(map.get("acqTime"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String sensorNo = map.get("number");
        String MonitorNo = tabMonitorMapper.selectMNoBySeonsorNo(sensorNo);

        TbsHsl tbsHsl = new TbsHsl();
        tbsHsl.setTime(acqTime);
        tbsHsl.setSensorNo(sensorNo);
        tbsHsl.setHsl(BigDecimal.valueOf(Double.valueOf(map.get("hsl").equals(" ")?"0":map.get("hsl"))));
        tbsHsl.setDeep(BigDecimal.valueOf(StringUtils.isEmpty(map.get("deep")) || " ".equals(map.get("deep")) ? 0 : Double.valueOf(map.get("deep"))));
        tbsHsl.setMonitorNo(MonitorNo);

        HashOperations<String, Object, Object> vo = stringRedisTemplate.opsForHash();
        vo.put(map.get("number"), "time", map.get("acqTime"));
        return tbsHsl;
    }
}
