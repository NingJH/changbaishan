package com.cqndt.changbaishan.changbaishan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cqndt.changbaishan.changbaishan.DTO.RemoteDataRecordDTO;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Mapper
public interface RemoteDataRecordMapper extends BaseMapper<RemoteDataRecord> {


    @Select("SELECT\n" +
            "     DISTINCT  a.create_time,a.update_time,a.station_name,a.station_code,a.station_type,a.serial_num,a.sensor_num,a.result_count,a.unit\n" +
            "FROM\n" +
            "    remote_data_record a\n" +
            "INNER JOIN (\n" +
            "    SELECT\n" +
            "        station_name,\n" +
            "        MAX (create_time) 'create_time'\n" +
            "    FROM\n" +
            "        remote_data_record\n" +
            "    WHERE CONVERT(VARCHAR(10),create_time,121) <= GETDATE()\n" +
            "    GROUP BY\n" +
            "        station_name\n" +
            ") b ON a.station_name = b.station_name\n" +
            "AND a.create_time = b.create_time \n" +
            "AND CONVERT(VARCHAR(10),b.create_time,121) <= GETDATE()\n" +
            "ORDER BY\n" +
            "    a.create_time DESC;")
    List<RemoteDataRecord> listDeviceRecord();


    @Select("SELECT\n" +
            "\t*\n" +
            "FROM\n" +
            "\tremote_data_record\n" +
            "WHERE\n" +
            "\tDateDiff( s, upload_time, getDate()) <= 3600\n" +
            "ORDER BY\n" +
            "dbo.remote_data_record.upload_time ASC;")
//    @Select("SELECT\n" +
//            "\t*\n" +
//            "FROM\n" +
//            "\tremote_data_record_copy\n" +
//            "WHERE\n" +
//            "\tDateDiff( s, upload_time, getDate()) <= 3600\n" +
//            "ORDER BY\n" +
//            "dbo.remote_data_record_copy.upload_time ASC;")
    List<RemoteDataRecord> listDeviceRecordLatest();



}
