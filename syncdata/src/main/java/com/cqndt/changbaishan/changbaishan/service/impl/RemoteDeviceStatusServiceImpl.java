package com.cqndt.changbaishan.changbaishan.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDeviceStatus;
import com.cqndt.changbaishan.changbaishan.mapper.RemoteDeviceStatusMapper;
import com.cqndt.changbaishan.changbaishan.service.IRemoteDeviceStatusService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@DS("db2")
@Service
public class RemoteDeviceStatusServiceImpl extends ServiceImpl<RemoteDeviceStatusMapper, RemoteDeviceStatus> implements IRemoteDeviceStatusService {

    @Override
    public List<RemoteDeviceStatus> listDeviceStatusLatest() {
        return baseMapper.listDeviceStatusLatest();
    }
}
