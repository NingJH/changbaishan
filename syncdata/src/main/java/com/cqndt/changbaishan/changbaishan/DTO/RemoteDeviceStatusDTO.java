package com.cqndt.changbaishan.changbaishan.DTO;

import com.cqndt.changbaishan.changbaishan.entity.RemoteDeviceStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
public class RemoteDeviceStatusDTO extends RemoteDeviceStatus {




}
