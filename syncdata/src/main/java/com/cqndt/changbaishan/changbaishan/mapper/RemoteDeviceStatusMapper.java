package com.cqndt.changbaishan.changbaishan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDeviceStatus;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Mapper
public interface RemoteDeviceStatusMapper extends BaseMapper<RemoteDeviceStatus> {


@Select(" SELECT\n" +
        "        a.*\n" +
        "        FROM\n" +
        "        remote_device_status a\n" +
        "        INNER JOIN (\n" +
        "        SELECT\n" +
        "        station_name,\n" +
        "        MAX (create_time) 'create_time'\n" +
        "        FROM\n" +
        "        remote_device_status\n" +
        "        WHERE CONVERT(VARCHAR(10),create_time,121) <= GETDATE()\n" +
        "        GROUP BY\n" +
        "        station_name\n" +
        "        ) b ON a.station_name = b.station_name\n" +
        "        AND a.create_time = b.create_time\n" +
        "        AND CONVERT(VARCHAR(10),b.create_time,121) <= GETDATE()\n" +
        "        ORDER BY\n" +
        "        a.create_time DESC;")
    List<RemoteDeviceStatus> listDeviceStatus();




    @Select("SELECT\n" +
            "\t*\n" +
            "FROM\n" +
            "\tremote_device_status\n" +
            "WHERE\n" +
            "\tDateDiff(s, upload_time, getDate()) <= 3600\n" +
            "ORDER BY\n" +
            "\tdbo.remote_device_status.upload_time ASC;")
//@Select("SELECT\n" +
//        "\t*\n" +
//        "FROM\n" +
//        "\tremote_device_status_copy\n" +
//        "WHERE\n" +
//        "\tDateDiff(s, upload_time, getDate()) <= 3600\n" +
//        "ORDER BY\n" +
//        "\tdbo.remote_device_status_copy.upload_time ASC;")
    List<RemoteDeviceStatus> listDeviceStatusLatest();



}
