package com.cqndt.changbaishan.changbaishan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDeviceStatus;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
public interface IRemoteDeviceStatusService extends IService<RemoteDeviceStatus> {

    List<RemoteDeviceStatus> listDeviceStatusLatest();

}
