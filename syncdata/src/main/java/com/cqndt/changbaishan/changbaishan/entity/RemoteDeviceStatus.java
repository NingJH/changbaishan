package com.cqndt.changbaishan.changbaishan.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class RemoteDeviceStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    private String  id;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private String delFlag;

    private String stationName;

    private String stationCode;

    private String stationType;

    private String serialNum;

    private LocalDateTime uploadTime;

    private Float voltage;

    private Float storage;

    private Float temperature;

    private Float humidity;

    private String status;

    private String warnType;

    private String warnMsg;

    private Integer signal;


}
