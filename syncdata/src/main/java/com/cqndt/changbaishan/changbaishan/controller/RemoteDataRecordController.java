package com.cqndt.changbaishan.changbaishan.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishan.service.IRemoteDataRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@RestController
@Slf4j
//@RequestMapping("/changbaishan/remote-data-record")
@RequestMapping("/changbaishan")
public class RemoteDataRecordController {
    @Autowired
    IRemoteDataRecordService remoteDataRecordService;

    @RequestMapping("/test")
    public void test(){
        QueryWrapper qw = new QueryWrapper();
//        qw.eq("id","000029698d264009abe2f9936a894ab9");
        qw.between("create_time","2020-06-10 20:59:52.000","2020-06-11 00:00:00.000");

       int count =  remoteDataRecordService.count();
        log.info("remoteDataRecordService count : {}" ,count);
//        List<RemoteDataRecord> remoteDataRecordList =  remoteDataRecordService.getAll(null);
//        log.info("remoteDataRecordList : {}" ,remoteDataRecordList);
        List<RemoteDataRecord> remoteDataRecordList  = remoteDataRecordService.list(qw);

        List<RemoteDataRecord> remoteDataRecordList1  = remoteDataRecordService.listDeviceRecordLatest();


        log.info("remoteDataRecordList size : {}" ,remoteDataRecordList.size());

    }

}
