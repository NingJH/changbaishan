package com.cqndt.changbaishan.changbaishan.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishan.DTO.RemoteDataRecordDTO;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
public interface IRemoteDataRecordService extends IService<RemoteDataRecord> {


    List<RemoteDataRecord> listDeviceRecordLatest();





}
