package com.cqndt.changbaishan.changbaishan.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.DTO.RemoteDataRecordDTO;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishan.mapper.RemoteDataRecordMapper;
import com.cqndt.changbaishan.changbaishan.service.IRemoteDataRecordService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@DS("db2")
@Service
public class RemoteDataRecordServiceImpl extends ServiceImpl<RemoteDataRecordMapper, RemoteDataRecord> implements IRemoteDataRecordService {

    @Override
    public List<RemoteDataRecord> listDeviceRecordLatest() {
        return baseMapper.listDeviceRecordLatest();
    }
}
