package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabDxsw;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabGnss;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
public interface ITabDxswService extends IService<TabDxsw> {

    void saveFromRecode(RemoteDataRecord dataRecord);

}
