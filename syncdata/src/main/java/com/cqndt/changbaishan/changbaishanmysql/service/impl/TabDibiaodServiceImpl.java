package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabDibiaod;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabDibiaodMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabDibiaodService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabDibiaodServiceImpl extends ServiceImpl<TabDibiaodMapper, TabDibiaod> implements ITabDibiaodService {

}
