package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabTuyali;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabTuyaliMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabTuyaliService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 土地压力计 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabTuyaliServiceImpl extends ServiceImpl<TabTuyaliMapper, TabTuyali> implements ITabTuyaliService {

}
