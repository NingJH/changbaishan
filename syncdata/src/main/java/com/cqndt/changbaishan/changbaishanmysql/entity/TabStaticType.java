package com.cqndt.changbaishan.changbaishanmysql.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 静态值表
 * </p>
 *
 * @author jobob
 * @since 2020-06-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TabStaticType implements Serializable {

    private static final long serialVersionUID = 1L;

    private String staticName;

    private Integer staticNum;

    private String staticType;

    @TableField("static_keyVal")
    private String staticKeyval;

    private String staticLevel;

    private String staticLength;

    @TableField("static_totalNum")
    private Integer staticTotalnum;

    @TableField("static_totalType")
    private String staticTotaltype;

    @TableField("totalNum_num")
    private Integer totalnumNum;


}
