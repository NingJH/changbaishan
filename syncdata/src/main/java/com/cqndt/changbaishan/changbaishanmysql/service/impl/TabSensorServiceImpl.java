package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDeviceStatus;
import com.cqndt.changbaishan.changbaishanmysql.entity.*;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabLxdbdMapper;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabSensorMapper;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabTuyaliMapper;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabYlMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabSensorService;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.DoubleToLongFunction;

/**
 * <p>
 * 传感器表 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabSensorServiceImpl extends ServiceImpl<TabSensorMapper, TabSensor> implements ITabSensorService {

    @Autowired
    TabLxdbdMapper lxdbdMapper;

    @Autowired
    TabYlMapper ylMapper;

    @Autowired
    TabTuyaliMapper tuyaliMapper;

    @Autowired
    TabSensorMapper sensorMapper;



    @Override
    public void syncSensors(List<RemoteDataRecord> remoteDataRecordList) {
        for(RemoteDataRecord remoteDataRecord:remoteDataRecordList){
            QueryWrapper qw = new QueryWrapper();
            qw.eq("device_no",remoteDataRecord.getStationName());
            qw.eq("sensor_no",remoteDataRecord.getSensorNum());
            if(sensorMapper.selectCount(qw) <= 0 ){
                TabSensor sensor = new TabSensor();
                sensor.setDeviceNo(remoteDataRecord.getStationName());
                sensor.setSensorNo(remoteDataRecord.getSensorNum());
                sensor.setState(1);
                sensorMapper.insert(sensor);
            }


//            if(remoteDataRecord.getSensorNum().equals("009_1")){
//
//            }
//            if(remoteDataRecord.getSensorNum().equals("009_2")){
//
//            }
//            if(remoteDataRecord.getSensorNum().equals("009_3")){
//
//            }
            if(remoteDataRecord.getSensorNum().equals("024_1")){
                TabRecordNsldx recordNsldx = new TabRecordNsldx();
                recordNsldx.setTime(remoteDataRecord.getCreateTime());
                recordNsldx.setValue(remoteDataRecord.getResult());
                recordNsldx.setDeviceNo(remoteDataRecord.getStationName());

            }
            if(remoteDataRecord.getSensorNum().equals("004_1")){
                TabRecordCsj recordCsj = new TabRecordCsj();
                recordCsj.setTime(remoteDataRecord.getCreateTime());
                recordCsj.setDeviceNo(remoteDataRecord.getStationName());
//                recordCsj.setValue(remoteDataRecord.getResult());
            }
            if(remoteDataRecord.getSensorNum().equals("021_1")){
                TabRecordNwj recordNwj = new TabRecordNwj();
                recordNwj.setTime(remoteDataRecord.getCreateTime());
                recordNwj.setDeviceNo(remoteDataRecord.getStationName());
                recordNwj.setValue(remoteDataRecord.getResult());
            }
            if(remoteDataRecord.getSensorNum().equals("003_1")){
             TabYl yl = new TabYl();
             yl.setValue(BigDecimal.valueOf(Integer.valueOf(remoteDataRecord.getResult())));
             yl.setSensorNo(remoteDataRecord.getStationName());
             yl.setTime(remoteDataRecord.getCreateTime());
            }
            else {
                TabRecordTrhsl recordTrhsl = new TabRecordTrhsl();
                recordTrhsl.setTime(remoteDataRecord.getCreateTime());
                recordTrhsl.setDeviceNo(remoteDataRecord.getStationName());
                recordTrhsl.setValue(remoteDataRecord.getResult());

            }


//            TabSensor sensor = new TabSensor();
//            sensor.setDeviceNo(remoteDataRecord.getStationName());
//            sensor.setSensorNo(remoteDataRecord.getSensorNum());
//            if(remoteDataRecord.getStationType().equals("加速度")){
//                sensor.setSensorType(222);
//
//            }else if (remoteDataRecord.getStationType().equals("拉线")){
//                sensor.setState(11);
//                TabLxdbd lxdbd = new TabLxdbd();
//                lxdbd.setAdatetime(remoteDataRecord.getCreateTime());
//                lxdbd.setDeviceNo(remoteDataRecord.getStationName());
//                if (!StringUtils.isEmpty(remoteDataRecord.getResult())){
//                    lxdbd.setJsd(Double.valueOf(remoteDataRecord.getResult()));
//                }
//                lxdbdMapper.insert(lxdbd);
//            }else if (remoteDataRecord.getStationType().equals("倾角")){
//                sensor.setSensorType(333);
//            }
//            else if (remoteDataRecord.getStationType().equals("水位")){
//                sensor.setSensorType(444);
//            }
//            else if (remoteDataRecord.getStationType().equals("雨量")){
//                sensor.setSensorType(3);
//                TabYl yl = new TabYl();
//                yl.setAdatetime(remoteDataRecord.getCreateTime());
//                yl.setDeviceNo(remoteDataRecord.getStationCode());
//                if (!StringUtils.isEmpty(remoteDataRecord.getResult())){
//                    yl.setX(BigDecimal.valueOf(Integer.valueOf(remoteDataRecord.getResult())));
//                }
//
//                ylMapper.insert(yl);
//            }
//            baseMapper.insert(sensor);
////            deviceList.add(device);



        }

    }
}
