package com.cqndt.changbaishan.changbaishanmysql.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "tbs_lfwy")
public class TabLfwy implements Serializable {

    private static final long serialVersionUID = 1L;

    private LocalDateTime time;

    private BigDecimal value;

    private String sensorNo;

    private String deviceNo;

    private String monitorNo;

    private BigDecimal temp;


}
