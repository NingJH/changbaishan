package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabOnline;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabOnlineMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabOnlineService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设备在线离线表 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabOnlineServiceImpl extends ServiceImpl<TabOnlineMapper, TabOnline> implements ITabOnlineService {


}
