package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordCsj;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordNsldx;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabRecordNsldxMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabRecordNsldxService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-18
 */
@Service
public class TabRecordNsldxServiceImpl extends ServiceImpl<TabRecordNsldxMapper, TabRecordNsldx> implements ITabRecordNsldxService {

    @Override
    public void saveFromRecode(RemoteDataRecord dataRecord) {
        TabRecordNsldx recordNsldx = new TabRecordNsldx();
        recordNsldx.setValue(dataRecord.getResult());
        recordNsldx.setTime(dataRecord.getUploadTime());
        recordNsldx.setDeviceNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordNsldx.setSensorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordNsldx.setMonitorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        baseMapper.insert(recordNsldx);
    }
}
