package com.cqndt.changbaishan.changbaishanmysql.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 告警记录表
 * </p>
 *
 * @author jobob
 * @since 2020-06-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "tab_msg_record")
public class TabMsgRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    /**
     * 设备编号
     */
    private String deviceNo;

    /**
     * 电话
     */
    private String phone;

    /**
     * 内容
     */
    private String content;

    /**
     * 发送时间
     */
    private LocalDateTime sendTime;

    /**
     * 监测点名称
     */
    private String monitorName;

    /**
     * 人员名称
     */
    private String name;

    /**
     * 告警等级
     */
    private int warnLevel;

    /**
     * 告警类型
     */
    private String warnType;

    /**
     * 告警值
     */
    private String warnValue;

}
