package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabMsgRecord;

/**
 * <p>
 * 告警记录表 服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-23
 */
public interface ITabMsgRecordService extends IService<TabMsgRecord> {

    int addTabMsgRecord(TabMsgRecord tabMsgRecord);
}
