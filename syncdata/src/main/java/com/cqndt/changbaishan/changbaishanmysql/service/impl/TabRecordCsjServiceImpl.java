package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordCsj;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabRecordCsjMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabRecordCsjService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-18
 */
@Service
public class TabRecordCsjServiceImpl extends ServiceImpl<TabRecordCsjMapper, TabRecordCsj> implements ITabRecordCsjService {

    @Override
    public void saveFromRecode(RemoteDataRecord dataRecord) {
        if(dataRecord.getResultCount()!=2){
            return;
        }
        TabRecordCsj recordCsj = new TabRecordCsj();
        String[] value = dataRecord.getResult().split(",");
        recordCsj.setAmplitude(value[0]);
        recordCsj.setFrequency(value[1]);
        recordCsj.setTime(dataRecord.getUploadTime());
        recordCsj.setDeviceNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordCsj.setSensorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordCsj.setMonitorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        baseMapper.insert(recordCsj);
    }
}
