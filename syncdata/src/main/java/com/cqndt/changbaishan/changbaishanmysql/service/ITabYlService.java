package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabYl;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
public interface ITabYlService extends IService<TabYl> {

//    void syncLxRecords(List<RemoteDataRecord> remoteDataRecordList);

    void saveFromRecode(RemoteDataRecord dataRecord);

}
