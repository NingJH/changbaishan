package com.cqndt.changbaishan.changbaishanmysql.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 土壤含水率009_2
 * </p>
 *
 * @author jobob
 * @since 2020-06-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TabRecordTrhslt implements Serializable {

    private static final long serialVersionUID = 1L;

    private LocalDateTime ctime;

    private String value;

    private String deviceNo;


}
