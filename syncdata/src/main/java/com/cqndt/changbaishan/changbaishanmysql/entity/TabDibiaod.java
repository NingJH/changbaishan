package com.cqndt.changbaishan.changbaishanmysql.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TabDibiaod implements Serializable {

    private static final long serialVersionUID = 1L;

    private LocalDateTime datetime;

    private Double x;

    private Double y;

    private Double h;

    private Double x1;

    private Double y1;

    private Double h1;

    private String angel;

    private Double swy;

    private Double cwy;

    private Double swysd;

    private Double cwysd;

    private Double swyjs;

    private Double cwyjs;

    private String tableName;

    private String degree;

    private Integer isError;


}
