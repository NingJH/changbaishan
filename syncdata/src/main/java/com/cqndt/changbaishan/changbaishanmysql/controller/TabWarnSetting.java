package com.cqndt.changbaishan.changbaishanmysql.controller;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 阈值表
 * </p>
 *
 * @author jobob
 * @since 2020-06-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TabWarnSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 初始值
     */
    private BigDecimal initialValue;

    /**
     * 相邻告警阈值1
     */
    private BigDecimal adAlarm1;

    /**
     * 相邻告警阈值2
     */
    private BigDecimal adAlarm2;

    /**
     * 相邻告警阈值3
     */
    private BigDecimal adAlarm3;

    /**
     * 相邻告警阈值4
     */
    private BigDecimal adAlarm4;

    /**
     * 累计告警1
     */
    private BigDecimal cuAlarm1;

    /**
     * 累计告警2
     */
    private BigDecimal cuAlarm2;

    /**
     * 累计告警3
     */
    private BigDecimal cuAlarm3;

    /**
     * 累计告警4
     */
    private BigDecimal cuAlarm4;

    private Integer monitorId;

    /**
     * 0 不发送,1发送 默认0
     */
    private Integer isSend;

    /**
     * 多个号码用,隔开
     */
    private String alarmMobiles;

    /**
     * 传感器编号
     */
    private String sensorNo;

    /**
     * 速率告警1
     */
    private BigDecimal spAlarm1;

    /**
     * 速率告警2
     */
    private BigDecimal spAlarm2;

    /**
     * 速率告警3
     */
    private BigDecimal spAlarm3;

    /**
     * 速率告警4
     */
    private BigDecimal spAlarm4;

    /**
     * 阈值1
     */
    private BigDecimal threshold1;

    /**
     * 阈值2
     */
    private BigDecimal threshold2;

    /**
     * 阈值3
     */
    private BigDecimal threshold3;


}
