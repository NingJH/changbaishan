package com.cqndt.changbaishan.changbaishanmysql.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "tbs_gnss")
public class TabGnss implements Serializable {

    private static final long serialVersionUID = 1L;

    private LocalDateTime time;

    private String sensorNo;

    private String deviceNo;

    private String monitorNo;

    private BigDecimal x;

    private BigDecimal y;

    private BigDecimal z;

    private BigDecimal x1;

    private BigDecimal y1;

    private BigDecimal z1;

    private BigDecimal angel;

    private BigDecimal swy;

    private BigDecimal cwy;

    private BigDecimal swysd;

    private BigDecimal cwysd;

    private BigDecimal swyjs;

    private BigDecimal cwyjs;

}
