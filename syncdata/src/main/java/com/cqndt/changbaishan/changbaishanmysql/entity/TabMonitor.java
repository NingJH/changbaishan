package com.cqndt.changbaishan.changbaishanmysql.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * <p>
 * 监测点表
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TabMonitor implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 监测点编号
     */
    private String monitorNo;

    /**
     * 监测点名称
     */
    private String monitorName;

    /**
     * 监测点类型（关联tab_static_type中的static_keyVal）
     */
    private Integer monitorType;

    /**
     * 创建时间
     */
    private LocalDate createDate;

    /**
     * 属于哪一个项目，关联tab_project_info中的id
     */
    private String projectId;

    private String remark;

    /**
     * 监测点关联的设备，关联tab_device中的id
     */
    private Long deviceId;

    private Integer monitorId;

    private Integer isMonitor;

    private String sensorId;

    private Integer monitorHz;

    private String timePre;

    private String timeNext;

    private String gpsX;

    private String gpsY;

    private String gpsH;

    private BigDecimal bdsX;

    private BigDecimal bdsY;

    private BigDecimal bdsH;

    private BigDecimal ggbX;

    private BigDecimal ggbY;

    private BigDecimal ggbH;

    private String password;

    private BigDecimal initJd;

    private String holeNo;

    private BigDecimal holeHeight;

    private BigDecimal aperture;

    private BigDecimal holeDeep;

    private BigDecimal rockDeep;

    private BigDecimal slideDeep;

    private BigDecimal groundWater;

    private String monitorSm;

    private String videoMobile;

    private BigDecimal lon;

    private BigDecimal lat;

    /**
     * 灾害点id
     */
    private Integer basicId;


}
