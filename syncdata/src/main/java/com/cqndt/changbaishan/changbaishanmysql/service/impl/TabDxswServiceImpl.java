package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabDxsw;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabGnss;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabDxswMapper;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabGnssMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabDxswService;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabGnssService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabDxswServiceImpl extends ServiceImpl<TabDxswMapper, TabDxsw> implements ITabDxswService {

    @Override
    public void saveFromRecode(RemoteDataRecord dataRecord) {
        TabDxsw recordYl = new TabDxsw();
        recordYl.setValue(BigDecimal.valueOf(Double.valueOf(dataRecord.getResult())));
        recordYl.setTime(dataRecord.getUploadTime());
        recordYl.setDeviceNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordYl.setSensorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordYl.setMonitorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        baseMapper.insert(recordYl);
    }
}
