package com.cqndt.changbaishan.changbaishanmysql.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author jobob
 * @since 2020-06-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "tbs_nsldx")
public class TabRecordNsldx implements Serializable {

    private static final long serialVersionUID = 1L;

    private LocalDateTime time;

    private String value;

    private String deviceNo;

    private String sensorNo;

    private String monitorNo;
}
