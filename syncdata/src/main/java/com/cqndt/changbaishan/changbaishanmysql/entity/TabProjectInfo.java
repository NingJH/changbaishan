package com.cqndt.changbaishan.changbaishanmysql.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * <p>
 * 项目信息表
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TabProjectInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 灾害点编号，一个灾害点可能对应多个项目
     */
    private String disNo;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目开始时间
     */
    private LocalDate startDate;

    /**
     * 项目坐标_x
     */
    private BigDecimal coordinateX;

    /**
     * 项目坐标_y
     */
    private BigDecimal coordinateY;

    /**
     * 经度(用于百度地图)
     */
    private BigDecimal longitude;

    /**
     * 纬度(用于百度地图)
     */
    private BigDecimal latitude;

    /**
     * 项目地点
     */
    private String projectAdd;

    /**
     * 项目联系人,多个用逗号隔开
     */
    private String projectMan;

    /**
     * 项目所属单位
     */
    private Integer unitId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 状态(1未删除，0已删除)
     */
    private Integer state;

    /**
     * 编号
     */
    private String projectNo;

    /**
     * 是否发送(0 不发送 1发送)
     */
    private Integer isSend;

    /**
     * 项目关联静态值表七种报表类型 1 关联 其他不关联
     */
    private Integer flag;


}
