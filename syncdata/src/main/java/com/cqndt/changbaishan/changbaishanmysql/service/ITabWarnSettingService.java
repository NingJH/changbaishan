package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishanmysql.controller.TabWarnSetting;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 阈值表 服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-23
 */
public interface ITabWarnSettingService extends IService<TabWarnSetting> {

    List<Map<String,Object>> TabWarnSetting(String sensorNo);

    Map<String, Object> alarmInfo(String sensorNo);
}
