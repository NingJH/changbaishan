package com.cqndt.changbaishan.changbaishanmysql.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 阈值表 前端控制器
 * </p>
 *
 * @author jobob
 * @since 2020-06-23
 */
@RestController
@RequestMapping("/changbaishanmysql/tab-warn-setting")
public class TabWarnSettingController {

}
