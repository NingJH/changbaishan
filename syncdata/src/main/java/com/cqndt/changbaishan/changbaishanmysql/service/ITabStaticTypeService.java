package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabStaticType;

/**
 * <p>
 * 静态值表 服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-19
 */
public interface ITabStaticTypeService extends IService<TabStaticType> {

}
