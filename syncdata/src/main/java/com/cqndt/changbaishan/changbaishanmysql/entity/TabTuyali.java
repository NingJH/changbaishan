package com.cqndt.changbaishan.changbaishanmysql.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 土地压力计
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TabTuyali implements Serializable {

    private static final long serialVersionUID = 1L;

    private LocalDateTime mtime;

    private String devId;

    private Double pa;


}
