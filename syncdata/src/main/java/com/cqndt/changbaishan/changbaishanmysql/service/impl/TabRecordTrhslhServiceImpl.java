package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordNwj;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordTrhsl;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordTrhslh;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabRecordTrhslMapper;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabRecordTrhslhMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabRecordTrhslService;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabRecordTrhslhService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-18
 */
@Service
public class TabRecordTrhslhServiceImpl extends ServiceImpl<TabRecordTrhslhMapper, TabRecordTrhslh> implements ITabRecordTrhslhService {

    @Override
    public void saveFromRecode(RemoteDataRecord dataRecord) {
        TabRecordTrhslh recordTrhslh = new TabRecordTrhslh();
        recordTrhslh.setValue(dataRecord.getResult());
        recordTrhslh.setDeviceNo(dataRecord.getStationName());
        recordTrhslh.setCtime(dataRecord.getUploadTime());
        baseMapper.insert(recordTrhslh);
    }
}
