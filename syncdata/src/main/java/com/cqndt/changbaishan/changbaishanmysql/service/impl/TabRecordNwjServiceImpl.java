package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordNsldx;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordNwj;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabRecordNwjMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabRecordNwjService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-18
 */
@Service
public class TabRecordNwjServiceImpl extends ServiceImpl<TabRecordNwjMapper, TabRecordNwj> implements ITabRecordNwjService {

    @Override
    public void saveFromRecode(RemoteDataRecord dataRecord) {
        TabRecordNwj recordNwj = new TabRecordNwj();
        recordNwj.setValue(dataRecord.getResult());
        recordNwj.setTime(dataRecord.getUploadTime());
        recordNwj.setDeviceNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordNwj.setSensorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordNwj.setMonitorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        baseMapper.insert(recordNwj);
    }
}
