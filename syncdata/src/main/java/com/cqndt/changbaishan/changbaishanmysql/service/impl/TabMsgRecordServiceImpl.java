package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabMsgRecord;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabMsgRecordMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabMsgRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 告警记录表 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabMsgRecordServiceImpl extends ServiceImpl<TabMsgRecordMapper, TabMsgRecord> implements ITabMsgRecordService {

    @Autowired
    TabMsgRecordMapper tabMsgRecordMapper;

    @Override
    public int addTabMsgRecord(TabMsgRecord tabMsgRecord) {
        return tabMsgRecordMapper.addTabMsgRecord(tabMsgRecord);
    }
}
