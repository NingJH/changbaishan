package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDeviceStatus;
import com.cqndt.changbaishan.changbaishan.mapper.RemoteDeviceStatusMapper;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabDevice;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabDeviceMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabDeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 设备表，与传感器表关联，一个设备可能多个传感器 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
@Slf4j
public class TabDeviceServiceImpl extends ServiceImpl<TabDeviceMapper, TabDevice> implements ITabDeviceService {



}
