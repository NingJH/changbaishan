package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabLxdbd;

import java.util.List;

/**
 * <p>
 * 地表位移 服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
public interface ITabLxdbdService extends IService<TabLxdbd> {

//    void syncLxRecords(List<RemoteDataRecord> remoteDataRecordList);

}
