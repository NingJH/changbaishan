package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordTrhslt;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabYl;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabYlMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabYlService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabYlServiceImpl extends ServiceImpl<TabYlMapper, TabYl> implements ITabYlService {

    @Override
    public void saveFromRecode(RemoteDataRecord dataRecord) {
        TabYl recordYl = new TabYl();
        recordYl.setValue(BigDecimal.valueOf(Double.valueOf(dataRecord.getResult())));
        recordYl.setTime(dataRecord.getUploadTime());
        recordYl.setDeviceNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordYl.setSensorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordYl.setMonitorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        baseMapper.insert(recordYl);
    }
}
