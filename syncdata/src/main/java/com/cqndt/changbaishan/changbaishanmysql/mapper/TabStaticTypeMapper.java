package com.cqndt.changbaishan.changbaishanmysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabStaticType;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 静态值表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-06-19
 */
@Mapper
public interface TabStaticTypeMapper extends BaseMapper<TabStaticType> {

}
