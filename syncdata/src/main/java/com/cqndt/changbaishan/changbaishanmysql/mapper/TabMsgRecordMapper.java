package com.cqndt.changbaishan.changbaishanmysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabMsgRecord;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 告警记录表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Mapper
public interface TabMsgRecordMapper extends BaseMapper<TabMsgRecord> {

    int addTabMsgRecord(TabMsgRecord tabMsgRecord);
}
