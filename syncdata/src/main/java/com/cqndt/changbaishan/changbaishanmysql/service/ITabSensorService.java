package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDeviceStatus;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabSensor;

import java.util.List;

/**
 * <p>
 * 传感器表 服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
public interface ITabSensorService extends IService<TabSensor> {

    void syncSensors(List<RemoteDataRecord> remoteDataRecordList);


}
