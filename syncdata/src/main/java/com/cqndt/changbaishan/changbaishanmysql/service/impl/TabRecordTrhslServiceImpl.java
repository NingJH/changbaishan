package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordTrhsl;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordTrhslh;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabRecordTrhslMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabRecordTrhslService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-18
 */
@Service
public class TabRecordTrhslServiceImpl extends ServiceImpl<TabRecordTrhslMapper, TabRecordTrhsl> implements ITabRecordTrhslService {

    @Override
    public void saveFromRecode(RemoteDataRecord dataRecord) {
        TabRecordTrhsl recordTrhsl = new TabRecordTrhsl();
        recordTrhsl.setHsl(dataRecord.getResult());
        recordTrhsl.setTime(dataRecord.getUploadTime());
        recordTrhsl.setDeviceNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordTrhsl.setSensorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordTrhsl.setMonitorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        baseMapper.insert(recordTrhsl);
    }
}
