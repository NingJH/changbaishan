package com.cqndt.changbaishan.changbaishanmysql.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 设备表，与传感器表关联，一个设备可能多个传感器
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TabDevice implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id",type = IdType.AUTO)
    private Long id;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备模型
     */
    private String deviceModel;

    /**
     * 设备编号
     */
    private String deviceNo;

    /**
     * 硬件版本
     */
    private String hardwareVersion;

    /**
     * 软件版本
     */
    private String softVersion;

    /**
     * 注册日期
     */
    private LocalDate registerDate;

    private String nickName;

    /**
     * sim卡
     */
    private String sim;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否在线离线(1:在线 0:离线)
     */
    private Integer state;

    /**
     * 设备类型（关联tab_static_type中的static_keyVal）
     */
    private Integer deviceType;

    private String tableName;

    private BigDecimal x;

    private BigDecimal y;

    private BigDecimal h;

    private Integer isHave;

    private BigDecimal ggbX;

    private BigDecimal ggbY;

    private BigDecimal ggbH;

    private BigDecimal bdsX;

    private BigDecimal bdsY;

    private BigDecimal bdsH;

    @TableField("interval_Z")
    private Integer intervalZ;

    private Integer lostNum;

    private Integer unitId;

    private String accuracy;

    private String installation;

    private String manufacturer;

    private String gatherNo;

    private String smCard;

    private String powerMode;

    private String installData;

    private String chuHz;

    private String xianHz;

    private BigDecimal installValue;

    @TableField("lxs_initVal")
    private BigDecimal lxsInitval;

    private BigDecimal initJd;

    private BigDecimal inclineSize;

    private BigDecimal holeDeep;

    private BigDecimal biaoHeight;

    private Integer videoFre;

    private String isNewDevice;

    private String newDeviceNo;

    @TableField("qxy_HoleDeep")
    private BigDecimal qxyHoledeep;

    /**
     * 地矿院 -参数1
     */
    @TableField("lParam")
    private String lParam;

    /**
     * 地矿院 -参数2
     */
    @TableField("wParam")
    private String wParam;

    private String code;

    private String success;

    /**
     * 设备厂商
     */
    private String deviceManufacturer;

    /**
     * 是否自动化
     */
    private String automationEquipment;

    private Integer equipment;

    /**
     * 是否为常规监测设备：0：常规   1：应急
     */
    private Integer isStandard;

    /**
     * 设备所在位置的经度
     */
    private String lon;

    /**
     * 设备所在位置的纬度
     */
    private String lat;

    /**
     * 最新数据时间
     */
    private LocalDateTime lastTime;

    /**
     * 最新数据值
     */
    private String lastData;

    /**
     * 所属区域
     */
    private Integer areaId;


}
