package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishanmysql.controller.TabWarnSetting;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabWarnSettingMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabWarnSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 阈值表 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-23
 */
@Service
public class TabWarnSettingServiceImpl extends ServiceImpl<TabWarnSettingMapper, TabWarnSetting> implements ITabWarnSettingService {

    @Autowired
    TabWarnSettingMapper tabWarnSettingMapper;

    @Override
    public List<Map<String, Object>> TabWarnSetting(String sensorNo) {
        List<Map<String,Object>>TabWarnSetting = tabWarnSettingMapper.TabWarnSetting(sensorNo);
        return TabWarnSetting;
    }

    @Override
    public Map<String, Object> alarmInfo(String sensorNo) {
        return tabWarnSettingMapper.alarmInfo(sensorNo);
    }
}
