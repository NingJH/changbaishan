package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabLfwy;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabLfwyMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabLfwyService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabLfwyServiceImpl extends ServiceImpl<TabLfwyMapper, TabLfwy> implements ITabLfwyService {

    @Override
    public void saveFromRecode(RemoteDataRecord dataRecord) {
        TabLfwy recordYl = new TabLfwy();
        recordYl.setValue(BigDecimal.valueOf(Double.valueOf(dataRecord.getResult())));
        recordYl.setTime(dataRecord.getUploadTime());
        recordYl.setDeviceNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordYl.setSensorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordYl.setMonitorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        baseMapper.insert(recordYl);
    }
}
