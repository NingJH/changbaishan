package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordTrhsl;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordTrhslt;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabRecordTrhslMapper;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabRecordTrhsltMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabRecordTrhslService;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabRecordTrhsltService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-18
 */
@Service
public class TabRecordTrhsltServiceImpl extends ServiceImpl<TabRecordTrhsltMapper, TabRecordTrhslt> implements ITabRecordTrhsltService {

    @Override
    public void saveFromRecode(RemoteDataRecord dataRecord) {
        TabRecordTrhslt recordTrhslt = new TabRecordTrhslt();
        recordTrhslt.setValue(dataRecord.getResult());
        recordTrhslt.setDeviceNo(dataRecord.getStationName());
        recordTrhslt.setCtime(dataRecord.getUploadTime());
        baseMapper.insert(recordTrhslt);
    }
}
