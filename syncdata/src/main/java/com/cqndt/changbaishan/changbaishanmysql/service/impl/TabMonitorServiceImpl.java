package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabMonitor;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabMonitorMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabMonitorService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 监测点表 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabMonitorServiceImpl extends ServiceImpl<TabMonitorMapper, TabMonitor> implements ITabMonitorService {

}
