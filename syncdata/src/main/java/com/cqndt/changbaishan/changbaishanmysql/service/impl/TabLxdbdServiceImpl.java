package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabLxdbd;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabLxdbdMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabLxdbdService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 地表位移 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabLxdbdServiceImpl extends ServiceImpl<TabLxdbdMapper, TabLxdbd> implements ITabLxdbdService {

}
