package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabGnss;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabGnssMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabGnssService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabGnssServiceImpl extends ServiceImpl<TabGnssMapper, TabGnss> implements ITabGnssService {

    @Override
    public void saveFromRecode(RemoteDataRecord dataRecord) {
        if(dataRecord.getResultCount()!=3){
            return;
        }
        TabGnss recordYl = new TabGnss();
        String[] result = dataRecord.getResult().split(",");
        recordYl.setX(BigDecimal.valueOf(Double.valueOf(result[0])));
        recordYl.setY(BigDecimal.valueOf(Double.valueOf(result[1])));
        recordYl.setZ(BigDecimal.valueOf(Double.valueOf(result[2])));
        recordYl.setTime(dataRecord.getUploadTime());
        recordYl.setDeviceNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordYl.setSensorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        recordYl.setMonitorNo(dataRecord.getStationName() + "_" + dataRecord.getSensorNum());
        baseMapper.insert(recordYl);
    }
}
