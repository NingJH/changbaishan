package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabProjectInfo;

/**
 * <p>
 * 项目信息表 服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
public interface ITabProjectInfoService extends IService<TabProjectInfo> {

}
