package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabStaticTypeService;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabStaticType;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabStaticTypeMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 静态值表 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-19
 */
@Service
public class TabStaticTypeServiceImpl extends ServiceImpl<TabStaticTypeMapper, TabStaticType> implements ITabStaticTypeService {

}
