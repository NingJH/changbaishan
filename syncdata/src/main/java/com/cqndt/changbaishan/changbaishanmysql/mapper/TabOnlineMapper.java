package com.cqndt.changbaishan.changbaishanmysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabMsgRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabOnline;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 告警记录表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Mapper
public interface TabOnlineMapper extends BaseMapper<TabOnline> {

}
