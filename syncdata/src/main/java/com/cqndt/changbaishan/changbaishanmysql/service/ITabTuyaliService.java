package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabTuyali;

/**
 * <p>
 * 土地压力计 服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
public interface ITabTuyaliService extends IService<TabTuyali> {

}
