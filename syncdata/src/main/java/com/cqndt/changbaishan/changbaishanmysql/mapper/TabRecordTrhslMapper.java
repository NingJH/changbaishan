package com.cqndt.changbaishan.changbaishanmysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordTrhsl;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-06-18
 */
@Mapper
public interface TabRecordTrhslMapper extends BaseMapper<TabRecordTrhsl> {

}
