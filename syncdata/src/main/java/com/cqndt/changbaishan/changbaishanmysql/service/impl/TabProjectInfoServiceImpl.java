package com.cqndt.changbaishan.changbaishanmysql.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabProjectInfo;
import com.cqndt.changbaishan.changbaishanmysql.mapper.TabProjectInfoMapper;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabProjectInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 项目信息表 服务实现类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Service
public class TabProjectInfoServiceImpl extends ServiceImpl<TabProjectInfoMapper, TabProjectInfo> implements ITabProjectInfoService {

}
