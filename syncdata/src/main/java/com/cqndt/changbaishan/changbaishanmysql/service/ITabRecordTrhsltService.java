package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordTrhsl;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabRecordTrhslt;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-18
 */
public interface ITabRecordTrhsltService extends IService<TabRecordTrhslt> {

    void saveFromRecode(RemoteDataRecord dataRecord);

}
