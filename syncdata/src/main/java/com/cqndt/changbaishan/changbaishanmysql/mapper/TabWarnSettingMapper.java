package com.cqndt.changbaishan.changbaishanmysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cqndt.changbaishan.changbaishanmysql.controller.TabWarnSetting;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 阈值表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-06-23
 */
@Mapper
public interface TabWarnSettingMapper extends BaseMapper<TabWarnSetting> {

    List<Map<String, Object>> TabWarnSetting(String sensorNo);

    Map<String, Object> alarmInfo(String sensorNo);
}
