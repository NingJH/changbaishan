package com.cqndt.changbaishan.changbaishanmysql.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 告警记录表
 * </p>
 *
 * @author jobob
 * @since 2020-06-23
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "tab_online")
public class TabOnline implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设备编号
     */
    private String deviceNo;

    /**
     * 离线时间
     */
    private LocalDateTime offlineTime;

    /**
     * 状态
     */
    private int state;

}
