package com.cqndt.changbaishan.changbaishanmysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabProjectInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 项目信息表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Mapper
public interface TabProjectInfoMapper extends BaseMapper<TabProjectInfo> {

}
