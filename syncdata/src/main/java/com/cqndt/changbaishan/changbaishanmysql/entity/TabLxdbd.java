package com.cqndt.changbaishan.changbaishanmysql.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 地表位移
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TabLxdbd implements Serializable {

    private static final long serialVersionUID = 1L;

    private LocalDateTime adatetime;

    private LocalDateTime rdatetime;

    private Double xlwy;

    private Double ljwy;

    /**
     * 传感器编号（tab_sensor表中的sensor_no）
     */
    private String deviceNo;

    private Double sd;

    private Double jsd;


}
