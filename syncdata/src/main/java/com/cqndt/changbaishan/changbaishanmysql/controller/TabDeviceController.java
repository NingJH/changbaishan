package com.cqndt.changbaishan.changbaishanmysql.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishan.service.IRemoteDataRecordService;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabDevice;
import com.cqndt.changbaishan.changbaishanmysql.service.ITabDeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 设备表，与传感器表关联，一个设备可能多个传感器 前端控制器
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@RestController
@RequestMapping("/changbaishanmysql")
@Slf4j
public class TabDeviceController {

    @Autowired
    ITabDeviceService deviceService;

    @RequestMapping("/test")
    public void test(){
        QueryWrapper qw = new QueryWrapper();
        qw.eq("id","3851");
        List<TabDevice> deviceList =  deviceService.list(qw);
        log.info("deviceList : {}" ,deviceList);

    }



}
