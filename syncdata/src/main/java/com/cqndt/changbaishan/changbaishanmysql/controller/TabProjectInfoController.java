package com.cqndt.changbaishan.changbaishanmysql.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 项目信息表 前端控制器
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@RestController
@RequestMapping("/changbaishanmysql/tab-project-info")
public class TabProjectInfoController {

}
