package com.cqndt.changbaishan.changbaishanmysql.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabDevice;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 设备表，与传感器表关联，一个设备可能多个传感器 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
@Mapper
public interface TabDeviceMapper extends BaseMapper<TabDevice> {

}
