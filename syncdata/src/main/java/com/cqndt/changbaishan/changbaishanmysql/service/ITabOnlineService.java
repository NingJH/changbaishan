package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabOnline;

/**
 * <p>
 * 在线离线表 服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-23
 */
public interface ITabOnlineService extends IService<TabOnline> {

}
