package com.cqndt.changbaishan.changbaishanmysql.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cqndt.changbaishan.changbaishanmysql.entity.TabDibiaod;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author jobob
 * @since 2020-06-11
 */
public interface ITabDibiaodService extends IService<TabDibiaod> {

}
