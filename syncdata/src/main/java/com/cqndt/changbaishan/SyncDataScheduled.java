package com.cqndt.changbaishan;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDataRecord;
import com.cqndt.changbaishan.changbaishan.entity.RemoteDeviceStatus;
import com.cqndt.changbaishan.changbaishan.service.IRemoteDataRecordService;
import com.cqndt.changbaishan.changbaishan.service.IRemoteDeviceStatusService;
import com.cqndt.changbaishan.changbaishanmysql.controller.TabWarnSetting;
import com.cqndt.changbaishan.changbaishanmysql.entity.*;
import com.cqndt.changbaishan.changbaishanmysql.service.*;
import com.cqndt.changbaishan.websocket.WebSocketData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
@EnableScheduling
@Slf4j
public class SyncDataScheduled {
    @Autowired
    IRemoteDataRecordService remoteDataRecordService;
    @Autowired
    IRemoteDeviceStatusService remoteDeviceStatusService;
    @Autowired
    ITabDeviceService deviceService;
    @Autowired
    ITabSensorService sensorService;
    @Autowired
    ITabLxdbdService lxdbdService;
    @Autowired
    ITabYlService ylService;
    @Autowired
    ITabLfwyService tabLfwyService;
    @Autowired
    ITabGnssService tabGnssService;
    @Autowired
    ITabDxswService tabDxswService;
    @Autowired
    ITabRecordCsjService recordCsjService;
    @Autowired
    ITabRecordNsldxService recordNsldxService;
    @Autowired
    ITabRecordNwjService recordNwjService;
    @Autowired
    ITabRecordTrhslhService recordTrhslhService;
    @Autowired
    ITabRecordTrhslService recordTrhslService;
    @Autowired
    ITabRecordTrhsltService recordTrhsltService;
    @Autowired
    ITabStaticTypeService staticTypeService;
    @Autowired
    ITabMonitorService monitorService;
    @Autowired
    ITabWarnSettingService warnSettingService;
    @Autowired
    WebSocketData webSocketData;
    @Autowired
    ITabMsgRecordService iTabMsgRecordService;


    //程序启动运行定时任务（每隔1小时同步一次数据）
//    @Scheduled(fixedRate = 1000 * 60 * 60,initialDelay = 5000)
    @Scheduled(fixedRate = 1000 * 60 * 60)
    @Transactional
    public void syncData() {
        List<RemoteDataRecord> dataRecordList = remoteDataRecordService.listDeviceRecordLatest();
        List<RemoteDeviceStatus> deviceStatusList = remoteDeviceStatusService.listDeviceStatusLatest();
//        List<TabDevice> deviceList = deviceService.list();
        if (dataRecordList.size() > 0) {
//            syncDeviceInfo(deviceList,deviceStatusList, dataRecordList);
//            syncSensor(dataRecordList);
//            saveWarinSetting(dataRecordList);
            syncRecordData(dataRecordList);
        }
    }

    public void syncDeviceInfo(List<TabDevice> deviceList,  List<RemoteDeviceStatus> deviceStatusList,List<RemoteDataRecord> dataRecordList) {
        deviceList.forEach(d -> {
            if (d.getState() != 0) {
                d.setState(0);
            }
        });
        deviceService.updateBatchById(deviceList);

        deviceStatusList.forEach(d->{
            if(d.getStatus().equals("online")){
                QueryWrapper qw = new QueryWrapper();
                qw.eq("device_name", d.getStationName());
                List<TabDevice> devices = deviceService.list(qw);
                devices.forEach(dd->{
                    dd.setState(1);
                    deviceService.saveOrUpdate(dd);
                });
            }
        });

        dataRecordList.forEach(r -> {
            QueryWrapper qw = new QueryWrapper();
            qw.eq("device_no", r.getStationName() + "_" + r.getSensorNum());
            TabDevice devicer = deviceService.getOne(qw);
            if (devicer == null) {
                QueryWrapper qwx = new QueryWrapper();
                qwx.eq("nick_name", r.getStationCode());
                qwx.eq("sim", r.getSensorNum());
                TabDevice devicen = deviceService.getOne(qwx);
                if (devicen != null) {
                    String deviceNo = devicen.getDeviceNo();
                    QueryWrapper qws = new QueryWrapper();
                    qws.eq("device_no", deviceNo);
                    sensorService.remove(qws);
                    devicen.setState(1);
                    devicen.setDeviceName(r.getStationName());
                    devicen.setDeviceNo(r.getStationName() + "_" + r.getSensorNum());
//                        deviceList.add(devicen);
                    deviceService.saveOrUpdate(devicen);
                } else {
                    devicen = new TabDevice();
                    devicen.setState(1);
                    devicen.setDeviceName(r.getStationName());
                    devicen.setDeviceNo(r.getStationName() + "_" + r.getSensorNum());
                    devicen.setLastTime(r.getUploadTime());
                    devicen.setNickName(r.getStationCode());
                    devicen.setSim(r.getSensorNum());
                    //复制默认 经纬度，区域id
                    devicen.setLat("42.035");
                    devicen.setLon("128.056");
                    devicen.setAreaId(220888);
                    switch (r.getSensorNum()) {
                        //土壤含水率
                        case "009_1":
                            devicen.setRemark("土壤含水率");
                            devicen.setDeviceType(6);
                            break;
                        //土壤含水率
                        case "009_2":
                            devicen.setRemark("土壤含水率");
                            devicen.setDeviceType(6);
                            break;
                        //土壤含水率
                        case "009_3":
                            devicen.setRemark("土壤含水率");
                            devicen.setDeviceType(6);
                            break;
                        //次声计
                        case "004_1":
                            devicen.setRemark("次声计");
                            devicen.setDeviceType(8);
                            break;
                        //泥位计
                        case "021_1":
                            devicen.setRemark("泥位计");
                            devicen.setDeviceType(5);
                            break;
                        //泥石流断线
                        case "024_1":
                            devicen.setRemark("泥石流断线");
                            devicen.setDeviceType(10);
                            break;
                        //地下水
                        case "003_1":
                            devicen.setRemark("地下水");
                            devicen.setDeviceType(9);
                            break;
                    }
                    deviceService.save(devicen);
                    Long deviceId = devicen.getId();

                    TabMonitor monitor = new TabMonitor();
                    monitor.setDeviceId(deviceId);
                    monitor.setMonitorNo(r.getStationCode() + "_" + r.getSensorNum());
                    monitorService.save(monitor);
                }
            }
        });
    }


    public void syncSensor(List<RemoteDataRecord> dataRecordList) {

//            设备对应传感器
        List<TabSensor> sensorList = sensorService.list();
        sensorList.forEach(s -> {
            if (s.getState() != 0) {
                s.setState(0);
                sensorService.updateById(s);
            }
        });

        dataRecordList.forEach(r -> {
            QueryWrapper qw = new QueryWrapper();
            String deviceSensor = r.getStationName() + "_" + r.getSensorNum();
            qw.eq("sensor_no", deviceSensor);
            TabSensor sensor = sensorService.getOne(qw);
            if (sensor == null) {
                sensor = new TabSensor();
                sensor.setState(1);
                sensor.setDeviceNo(deviceSensor);
                sensor.setSensorNo(deviceSensor);
                sensor.setLon(BigDecimal.valueOf(Double.valueOf("128.056")));
                sensor.setLat(BigDecimal.valueOf(Double.valueOf("42.035")));
                switch (r.getSensorNum()) {
                    //土壤含水率
                    case "009_1":
                        sensor.setSensorType(38);
                        break;
                    //土壤含水率
                    case "009_2":
                        sensor.setSensorType(38);
                        break;
                    //土壤含水率
                    case "009_3":
                        sensor.setSensorType(38);
                        break;
                    //次声计
                    case "004_1":
                        sensor.setSensorType(29);
                        break;
                    //泥位计
                    case "021_1":
                        sensor.setSensorType(41);
                        break;
                    //泥石流断线
                    case "024_1":
                        sensor.setSensorType(42);
                        break;
                    //地下水
                    case "003_1":
                        sensor.setSensorType(9);
                        break;
                }
                sensorService.save(sensor);
            } else {
                sensor.setState(1);
                sensorService.updateById(sensor);

            }
        });
    }


    public void saveWarinSetting(List<RemoteDataRecord> dataRecordList) {
//        告警值设置
        dataRecordList.forEach(r -> {
            QueryWrapper qw = new QueryWrapper();
            String sensorNo = r.getStationName() + "_" + r.getSensorNum();
            qw.eq("sensor_no", sensorNo);
            TabWarnSetting warnSetting = warnSettingService.getOne(qw);
            if (warnSetting == null) {
                QueryWrapper qwd = new QueryWrapper();
                qwd.eq("device_no", sensorNo);
                QueryWrapper qwm = new QueryWrapper();
                qwm.eq("device_id", deviceService.getOne(qwd).getId());
                TabMonitor monitor = monitorService.getOne(qwm);
                warnSetting = new TabWarnSetting();
                warnSetting.setMonitorId(monitorService.getOne(qwm).getMonitorId());
                warnSetting.setSensorNo(r.getStationName() + "_" + r.getSensorNum());
                warnSettingService.save(warnSetting);
            }
        });
    }


    public void syncRecordData(List<RemoteDataRecord> dataRecordList) {
        //采集数据保存
        dataRecordList.forEach(r -> {
            if (r.getSensorNum().contains("009")) {
                //土壤含水率(tbs_hsl)
                if(r.getResultCount()==1){
                    setWarnMsg2Socket(r,r.getSensorNum());
                    recordTrhslService.saveFromRecode(r);
                }
            } else if (r.getSensorNum().contains("021")) {
                //泥位计(tbs_niweiji)
                if(r.getResultCount()==1) {
                    setWarnMsg2Socket(r, r.getSensorNum());
                    recordNwjService.saveFromRecode(r);
                }
            } else if (r.getSensorNum().contains("004")) {
                //次声计(tbs_cisheng)
                if(r.getResult().contains(",")){
                    setWarnMsg2Socket(r,r.getSensorNum());
                    recordCsjService.saveFromRecode(r);
                }
            } else if (r.getSensorNum().contains("024")) {
                //泥石流断线(tbs_nsldx)
                if(r.getResultCount()==1) {
                    setWarnMsg2Socket(r, r.getSensorNum());
                    recordNsldxService.saveFromRecode(r);
                }
            } else if (r.getSensorNum().contains("003")) {
                //雨量计(tbs_yl)
                if(r.getResultCount()==1) {
                    setWarnMsg2Socket(r, r.getSensorNum());
                    ylService.saveFromRecode(r);
                }
            }else if(r.getSensorNum().contains("007")){
                //裂缝计(tbs_lfwy)
                if(r.getResultCount()==1) {
                    setWarnMsg2Socket(r, r.getSensorNum());
                    tabLfwyService.saveFromRecode(r);
                }
            }
            else if(r.getSensorNum().contains("008")){
                //Gnss(tbs_gnss)
                if(r.getResult().contains(",")){
                    tabGnssService.saveFromRecode(r);
                }
            }else if(r.getSensorNum().contains("014")){
                //地下水位(tbs_dxsw)
                if(r.getResultCount()==1) {
                    setWarnMsg2Socket(r, r.getSensorNum());
                    tabDxswService.saveFromRecode(r);
                }
            }

        });
    }

    private void setWarnMsg2Socket(RemoteDataRecord RemoteDataRecord,String staticType) {
        String result = RemoteDataRecord.getResult();
        String sensorNo = RemoteDataRecord.getStationName() + "_" + RemoteDataRecord.getSensorNum();
        List<Map<String, Object>> warnSetting = warnSettingService.TabWarnSetting(sensorNo);
        if(warnSetting.isEmpty()){
            return;
        }
        String alarm1 = warnSetting.get(0).get("red").toString();
        String alarm2 = warnSetting.get(0).get("orange").toString();
        String alarm3 = warnSetting.get(0).get("yellow").toString();
        String alarm4 = warnSetting.get(0).get("blue").toString();
        TabMsgRecord tabMsgRecord = new TabMsgRecord();
        Map<String, Object> deviceInfo = warnSettingService.alarmInfo(sensorNo);//需要告警的东西
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        deviceInfo.put("warnTime",sdf.format(new Date()));
        if(staticType.contains("004")){
            String resultCisheng1 = RemoteDataRecord.getResult().split(",")[0];
            String resultCisheng2 = RemoteDataRecord.getResult().split(",")[1];
            alarmCisheng(resultCisheng1,resultCisheng2,warnSetting.get(0),sensorNo,tabMsgRecord,deviceInfo);
        }else if(staticType.contains("008")){
            String x = RemoteDataRecord.getResult().split(",")[0];
            String y = RemoteDataRecord.getResult().split(",")[0];
            String z = RemoteDataRecord.getResult().split(",")[0];
            alarmGnss(x,y,z,warnSetting.get(0),sensorNo,tabMsgRecord,deviceInfo);
        }else {
            alarm(result,alarm1,alarm2,alarm3,alarm4,sensorNo,tabMsgRecord,deviceInfo);
        }
    }

    private void alarm(String result,String alarm1,String alarm2,String alarm3,String alarm4,String sensorNo,TabMsgRecord tabMsgRecord,Map<String,Object> deviceInfo){
        tabMsgRecord.setSendTime(LocalDateTime.now());
        tabMsgRecord.setDeviceNo(sensorNo);
        tabMsgRecord.setWarnValue(result);
        deviceInfo.put("warnValue",result);
        try{
            if(Double.parseDouble(result)>=Double.parseDouble(alarm4)&&Double.parseDouble(result)<Double.parseDouble(alarm3)){
                tabMsgRecord.setWarnLevel(4);
                deviceInfo.put("warnSetting","blue");
                deviceInfo.put("warnLevel","4");
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
            }else if(Double.parseDouble(result)>=Double.parseDouble(alarm3)&&Double.parseDouble(result)<Double.parseDouble(alarm2)){
                tabMsgRecord.setWarnLevel(3);
                deviceInfo.put("warnSetting","yellow");
                deviceInfo.put("warnLevel","3");
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
            }else if(Double.parseDouble(result)>=Double.parseDouble(alarm2)&&Double.parseDouble(result)<Double.parseDouble(alarm1)){
                tabMsgRecord.setWarnLevel(2);
                deviceInfo.put("warnSetting","orange");
                deviceInfo.put("warnLevel","2");
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
            }else if(Double.parseDouble(result)>=Double.parseDouble(alarm1)){
                tabMsgRecord.setWarnLevel(1);
                deviceInfo.put("warnSetting","red");
                deviceInfo.put("warnLevel","1");
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private void alarmCisheng(String resultCisheng1,String resultCisheng2,Map<String,Object> warnSetting,String sensorNo,TabMsgRecord tabMsgRecord,Map<String,Object> deviceInfo){
        tabMsgRecord.setSendTime(LocalDateTime.now());
        tabMsgRecord.setDeviceNo(sensorNo);
        try{
                //振幅4级告警
            if(Double.parseDouble(resultCisheng1)>=Double.parseDouble(warnSetting.get("frequency_blue").toString())&&Double.parseDouble(resultCisheng1)<Double.parseDouble(warnSetting.get("frequency_yellow").toString())){
                tabMsgRecord.setWarnLevel(4);
                tabMsgRecord.setWarnValue(resultCisheng1);
                tabMsgRecord.setWarnType("振幅");
                deviceInfo.put("warnSetting","blue");
                deviceInfo.put("warnLevel","4");
                deviceInfo.put("warnType","振幅");
                deviceInfo.put("warnValue",resultCisheng1);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //频率4级告警
            }else if(Double.parseDouble(resultCisheng2)>=Double.parseDouble(warnSetting.get("frequency_blue").toString())&&Double.parseDouble(resultCisheng2)<Double.parseDouble(warnSetting.get("frequency_yellow").toString())){
                tabMsgRecord.setWarnLevel(4);
                tabMsgRecord.setWarnValue(resultCisheng2);
                tabMsgRecord.setWarnType("频率");
                deviceInfo.put("warnSetting","blue");
                deviceInfo.put("warnLevel","4");
                deviceInfo.put("warnType","频率");
                deviceInfo.put("warnValue",resultCisheng2);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //振幅3级告警
            }else if(Double.parseDouble(resultCisheng1)>=Double.parseDouble(warnSetting.get("frequency_yellow").toString())&&Double.parseDouble(resultCisheng1)<Double.parseDouble(warnSetting.get("frequency_orange").toString())){
                tabMsgRecord.setWarnLevel(3);
                tabMsgRecord.setWarnValue(resultCisheng1);
                tabMsgRecord.setWarnType("振幅");
                deviceInfo.put("warnSetting","yellow");
                deviceInfo.put("warnLevel","3");
                deviceInfo.put("warnType","振幅");
                deviceInfo.put("warnValue",resultCisheng1);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //频率3级告警
            }else if(Double.parseDouble(resultCisheng2)>=Double.parseDouble(warnSetting.get("frequency_yellow").toString())&&Double.parseDouble(resultCisheng2)<Double.parseDouble(warnSetting.get("frequency_orange").toString())){
                tabMsgRecord.setWarnLevel(3);
                tabMsgRecord.setWarnValue(resultCisheng2);
                tabMsgRecord.setWarnType("频率");
                deviceInfo.put("warnSetting","yellow");
                deviceInfo.put("warnLevel","3");
                deviceInfo.put("warnType","频率");
                deviceInfo.put("warnValue",resultCisheng2);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //振幅2级告警
            }else if(Double.parseDouble(resultCisheng1)>=Double.parseDouble(warnSetting.get("frequency_orange").toString())&&Double.parseDouble(resultCisheng1)<Double.parseDouble(warnSetting.get("frequency_red").toString())){
                tabMsgRecord.setWarnLevel(2);
                tabMsgRecord.setWarnValue(resultCisheng1);
                tabMsgRecord.setWarnType("振幅");
                deviceInfo.put("warnSetting","orange");
                deviceInfo.put("warnLevel","2");
                deviceInfo.put("warnType","振幅");
                deviceInfo.put("warnValue",resultCisheng1);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //频率2级告警
            }else if(Double.parseDouble(resultCisheng2)>=Double.parseDouble(warnSetting.get("frequency_orange").toString())&&Double.parseDouble(resultCisheng2)<Double.parseDouble(warnSetting.get("frequency_red").toString())){
                tabMsgRecord.setWarnLevel(2);
                tabMsgRecord.setWarnValue(resultCisheng2);
                tabMsgRecord.setWarnType("频率");
                deviceInfo.put("warnSetting","orange");
                deviceInfo.put("warnLevel","2");
                deviceInfo.put("warnType","频率");
                deviceInfo.put("warnValue",resultCisheng2);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //振幅1级告警
            }else if(Double.parseDouble(resultCisheng1)>=Double.parseDouble(warnSetting.get("frequency_red").toString())){
                tabMsgRecord.setWarnLevel(1);
                tabMsgRecord.setWarnValue(resultCisheng1);
                tabMsgRecord.setWarnType("振幅");
                deviceInfo.put("warnSetting","red");
                deviceInfo.put("warnLevel","1");
                deviceInfo.put("warnType","振幅");
                deviceInfo.put("warnValue",resultCisheng1);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //频率1级告警
            }else if(Double.parseDouble(resultCisheng2)>=Double.parseDouble(warnSetting.get("frequency_red").toString())){
                tabMsgRecord.setWarnLevel(1);
                tabMsgRecord.setWarnValue(resultCisheng2);
                tabMsgRecord.setWarnType("频率");
                deviceInfo.put("warnSetting","red");
                deviceInfo.put("warnLevel","1");
                deviceInfo.put("warnType","频率");
                deviceInfo.put("warnValue",resultCisheng2);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private void alarmGnss(String x,String y,String z,Map<String,Object> warnSetting,String sensorNo,TabMsgRecord tabMsgRecord,Map<String,Object> deviceInfo){
        tabMsgRecord.setSendTime(LocalDateTime.now());
        tabMsgRecord.setDeviceNo(sensorNo);
        try{
                //x4级预警
            if(Double.parseDouble(x)>=Double.parseDouble(warnSetting.get("x_blue").toString())&&Double.parseDouble(x)<Double.parseDouble(warnSetting.get("x_yellow").toString())){
                tabMsgRecord.setWarnLevel(4);
                tabMsgRecord.setWarnValue(x);
                tabMsgRecord.setWarnType("x");
                deviceInfo.put("warnSetting","blue");
                deviceInfo.put("warnLevel","4");
                deviceInfo.put("warnType","x");
                deviceInfo.put("warnValue",x);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //y4级预警
            }else if(Double.parseDouble(y)>=Double.parseDouble(warnSetting.get("y_blue").toString())&&Double.parseDouble(y)<Double.parseDouble(warnSetting.get("y_yellow").toString())){
                tabMsgRecord.setWarnLevel(4);
                tabMsgRecord.setWarnValue(y);
                tabMsgRecord.setWarnType("y");
                deviceInfo.put("warnSetting","blue");
                deviceInfo.put("warnLevel","4");
                deviceInfo.put("warnType","y");
                deviceInfo.put("warnValue",y);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //z4级预警
            }else if(Double.parseDouble(z)>=Double.parseDouble(warnSetting.get("z_blue").toString())&&Double.parseDouble(z)<Double.parseDouble(warnSetting.get("z_yellow").toString())){
                tabMsgRecord.setWarnLevel(4);
                tabMsgRecord.setWarnValue(z);
                tabMsgRecord.setWarnType("z");
                deviceInfo.put("warnSetting","blue");
                deviceInfo.put("warnLevel","4");
                deviceInfo.put("warnType","z");
                deviceInfo.put("warnValue",z);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //x3级预警
            }else if(Double.parseDouble(x)>=Double.parseDouble(warnSetting.get("x_yellow").toString())&&Double.parseDouble(x)<Double.parseDouble(warnSetting.get("x_orange").toString())){
                tabMsgRecord.setWarnLevel(3);
                tabMsgRecord.setWarnValue(x);
                tabMsgRecord.setWarnType("x");
                deviceInfo.put("warnSetting","yellow");
                deviceInfo.put("warnLevel","3");
                deviceInfo.put("warnType","x");
                deviceInfo.put("warnValue",x);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //y3级预警
            }else if(Double.parseDouble(y)>=Double.parseDouble(warnSetting.get("y_yellow").toString())&&Double.parseDouble(y)<Double.parseDouble(warnSetting.get("y_orange").toString())){
                tabMsgRecord.setWarnLevel(3);
                tabMsgRecord.setWarnValue(y);
                tabMsgRecord.setWarnType("y");
                deviceInfo.put("warnSetting","yellow");
                deviceInfo.put("warnLevel","3");
                deviceInfo.put("warnType","y");
                deviceInfo.put("warnValue",y);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //z3级预警
            }else if(Double.parseDouble(z)>=Double.parseDouble(warnSetting.get("z_yellow").toString())&&Double.parseDouble(z)<Double.parseDouble(warnSetting.get("z_orange").toString())){
                tabMsgRecord.setWarnLevel(3);
                tabMsgRecord.setWarnValue(z);
                tabMsgRecord.setWarnType("z");
                deviceInfo.put("warnSetting","yellow");
                deviceInfo.put("warnLevel","3");
                deviceInfo.put("warnType","z");
                deviceInfo.put("warnValue",z);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //x2级预警
            }else if(Double.parseDouble(x)>=Double.parseDouble(warnSetting.get("x_orange").toString())&&Double.parseDouble(x)<Double.parseDouble(warnSetting.get("x_red").toString())){
                tabMsgRecord.setWarnLevel(2);
                tabMsgRecord.setWarnValue(x);
                tabMsgRecord.setWarnType("x");
                deviceInfo.put("warnSetting","orange");
                deviceInfo.put("warnLevel","2");
                deviceInfo.put("warnType","x");
                deviceInfo.put("warnValue",x);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //y2级预警
            }else if(Double.parseDouble(y)>=Double.parseDouble(warnSetting.get("y_orange").toString())&&Double.parseDouble(y)<Double.parseDouble(warnSetting.get("y_red").toString())){
                tabMsgRecord.setWarnLevel(2);
                tabMsgRecord.setWarnValue(y);
                tabMsgRecord.setWarnType("y");
                deviceInfo.put("warnSetting","orange");
                deviceInfo.put("warnLevel","2");
                deviceInfo.put("warnType","y");
                deviceInfo.put("warnValue",y);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //z2级预警
            }else if(Double.parseDouble(z)>=Double.parseDouble(warnSetting.get("z_orange").toString())&&Double.parseDouble(z)<Double.parseDouble(warnSetting.get("z_red").toString())){
                tabMsgRecord.setWarnLevel(2);
                tabMsgRecord.setWarnValue(z);
                tabMsgRecord.setWarnType("z");
                deviceInfo.put("warnSetting","orange");
                deviceInfo.put("warnLevel","2");
                deviceInfo.put("warnType","z");
                deviceInfo.put("warnValue",z);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //x1级预警
            }else if(Double.parseDouble(x)>=Double.parseDouble(warnSetting.get("x_red").toString())){
                tabMsgRecord.setWarnLevel(1);
                tabMsgRecord.setWarnValue(x);
                tabMsgRecord.setWarnType("x");
                deviceInfo.put("warnSetting","red");
                deviceInfo.put("warnLevel","1");
                deviceInfo.put("warnType","x");
                deviceInfo.put("warnValue",x);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //y1级预警
            }else if(Double.parseDouble(y)>=Double.parseDouble(warnSetting.get("y_red").toString())){
                tabMsgRecord.setWarnLevel(1);
                tabMsgRecord.setWarnValue(y);
                tabMsgRecord.setWarnType("y");
                deviceInfo.put("warnSetting","red");
                deviceInfo.put("warnLevel","1");
                deviceInfo.put("warnType","y");
                deviceInfo.put("warnValue",y);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
                //z1级预警
            }else if(Double.parseDouble(z)>=Double.parseDouble(warnSetting.get("z_red").toString())){
                tabMsgRecord.setWarnLevel(1);
                tabMsgRecord.setWarnValue(z);
                tabMsgRecord.setWarnType("z");
                deviceInfo.put("warnSetting","red");
                deviceInfo.put("warnLevel","1");
                deviceInfo.put("warnType","z");
                deviceInfo.put("warnValue",z);
                iTabMsgRecordService.addTabMsgRecord(tabMsgRecord);
                deviceInfo.put("warnId",tabMsgRecord.getId());
                webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }


//    @Scheduled(fixedRate = 10000)
//    public void test(){
//        Map<String, Object> deviceInfo = new HashMap<>();
//        try {
//            deviceInfo.put("sensorNo", "111");//传感器编号
//            deviceInfo.put("warnValue", "0.1");//告警值
//            deviceInfo.put("deviceName","kjsdg");//设备名称
//            deviceInfo.put("deviceNo","123");//设备编号
//            deviceInfo.put("deviceType","雨量");//设备类型
//            deviceInfo.put("deviceState",0);//设备状态
//            deviceInfo.put("lon","106.156465527");//经度
//            deviceInfo.put("lat","29.41564596");//纬度
//            deviceInfo.put("monitoeName", "red");//监测点名称
//            deviceInfo.put("warnLevel", "1");//告警等级
//            deviceInfo.put("warnTime", new Date());//告警时间
//            deviceInfo.put("cisheng","pl");
//            deviceInfo.put("gnss","x");
//            webSocketData.sendMsgToAll(JSON.toJSONString(deviceInfo),"");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

}
