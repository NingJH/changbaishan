package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.AppTabProjectService;
import com.cqndt.disaster.device.app.AppTabReportingFileService;
import com.cqndt.disaster.device.common.service.TabAttachmentService;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.domain.TabProject;
import com.cqndt.disaster.device.project.service.TabProjectService;
import com.cqndt.disaster.device.project.service.TabReportingFileService;
import com.cqndt.disaster.device.util.AjaxResult;
import com.cqndt.disaster.device.vo.TabAlarmInfoVo;
import com.cqndt.disaster.device.vo.TabDoc;
import com.cqndt.disaster.device.vo.TabDocCollection;
import com.cqndt.disaster.device.vo.TabReportFileVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


@RestController
@RequestMapping(value = "/api/v2")
@Api(value = "项目文档", description = "项目文档")
@Slf4j
public class TabDocController extends BaseController {

    @Autowired
    private AppTabReportingFileService tabReportingFileService;
    @Autowired
    private AppTabProjectService appTabProjectService;

//    @Autowired
//    private TabProjectService tabProjectService;

//    @Autowired
//    private TabAttachmentService tabAttachmentService;

    @RequestMapping(value = "/report",method = RequestMethod.POST)
    @ApiOperation(value = "根据项目id获取报表", notes = "根据项目id获取报表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "uploadMonth", value = "查询月份", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "uploadYear", value = "查询年份", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "fileType", value = "报表类型", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query")
    })
    public AjaxResult getAllReportByCondition(HttpServletRequest request, TabReportFileVo vo){
        String userId = request.getHeader("id");
        StringBuffer searchTime = new StringBuffer();
        if(null != vo.getUploadYear() && !("").equals(vo.getUploadYear())){
            searchTime.append(vo.getUploadYear());
        }
        if(null != vo.getUploadMonth() && !("").equals(vo.getUploadMonth())){
            searchTime.append("-"+vo.getUploadMonth());
        }
        vo.setSearchTime(searchTime.toString());
        vo.setUserId(userId);
        TabDocCollection collection = tabReportingFileService.listReportingFile(vo);
        return entityResult(collection,"获取报表列表");

    }
    @PostMapping("/data")
    @ApiOperation(value = "根据项目id查询项目文档", notes = "根据项目id查询项目文档")
    @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query")
    public AjaxResult getTabProjectDatum(Integer projectId){
        return entityResult(appTabProjectService.selectProjectDatum(projectId),"项目文档");
    }

    @PostMapping("/picture")
    @ApiOperation(value = "根据项目id查询图片", notes = "根据项目id查询图片")
    @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query")
    public AjaxResult getTabProjectPicture(HttpServletRequest request,Integer projectId){
        String userId = request.getHeader("id");
        return entityResult(appTabProjectService.selectAttachmentImgs(projectId,userId),"项目图片");
    }
    @PostMapping("/video")
    @ApiOperation(value = "根据项目id查询视频", notes = "根据项目id查询视频")
    @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query")
    public AjaxResult getTabProjectVideo(HttpServletRequest request,Integer projectId){
        String userId = request.getHeader("id");
        return entityResult(appTabProjectService.selectAttachmentVideos(projectId,userId),"项目视频");
    }

}
