package com.cqndt.disaster.device.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @Auther : meng
 * @Date : 2018/8/15 15:00
 **/
@Slf4j
@Configuration
public class WebAppConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册自定义拦截器，添加拦截路径和排除拦截路径
        log.info("注册拦截器开始");
        registry.addInterceptor(loginInterceptor()).addPathPatterns("/api/v2/**").excludePathPatterns("/api/v2/login","/api/v2/android/**","/api/v2/upload/**");
    }
    @Bean
    public InterceptorConfig loginInterceptor() {
        return new InterceptorConfig();
    }
}
