package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.AppTabProjectService;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.project.service.TabProjectService;
import com.cqndt.disaster.device.util.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@Api(value = "设备管理", description = "设备管理")
@RequestMapping(value = "/api/v2/project")
public class TabMonitorController extends BaseController{

	@Autowired
	private AppTabProjectService tabProjectService;

    @PostMapping("/getRelatedMonitoring")
    @ApiOperation(value = "通过项目id获取相关设备", notes = "通过项目id获取相关设备")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "search", value = "查询条件(device_name 设备名称)", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "monitorType", value = "设备类型", defaultValue = "", required = true, paramType = "query")
    })
    public AjaxResult getRelatedMonitoring(Integer projectId, String search, Integer monitorType){
		return entityResult(tabProjectService.getByProjectId(projectId,search,monitorType),"获取项目相关设备");
    }

    @PostMapping("/getMonitorDataById")
    @ApiOperation(value = "通过监测点id获取设备详情", notes = "通过监测点id设备详情")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "monitorId", value = "监测点id", defaultValue = "", required = true, paramType = "query")
    })
    public AjaxResult getMonitorDataById(Integer monitorId){
		return entityResult(tabProjectService.getMonitorDataById(monitorId),"获取项目相关监测点设备详情");
    }
}
