package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.AppTabLoginLogService;
import com.cqndt.disaster.device.app.AppTabUserService;
import com.cqndt.disaster.device.common.shrio.ShiroUtils;
import com.cqndt.disaster.device.domain.TabLoginLog;
import com.cqndt.disaster.device.util.AjaxResult;
import com.cqndt.disaster.device.util.DESUtil;
import com.cqndt.disaster.device.util.SHA1Util;
import com.cqndt.disaster.device.vo.TabUserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created By marc
 * Date: 2019/5/10  16:42
 * Description:
 */
@RestController
@RequestMapping(value = "/api/v2")
@Api(value = "用户登录", description = "用户登录")
@Slf4j
public class LoginController extends BaseController{
    @Autowired
    private AppTabUserService tabUserService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AppTabLoginLogService appTabLoginLogService;

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ApiOperation(value = "用户登录", notes = "用户登录")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "userName", value = "用户名", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "encryptedPassword", value = "密码", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "uuid", value = "ime", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult userLogin(HttpServletRequest request, String userName, String encryptedPassword, String uuid) throws Exception {
        String password = DESUtil.decryption(encryptedPassword,"ycmcwin2000");
        //String password = "11111";
        TabUserVo vo = new TabUserVo();
        vo.setUserName(userName);
        vo = tabUserService.getUserByUserName(vo);
        if (vo != null){
            password = ShiroUtils.sha256(password,vo.getSalt());
            if (vo.getPassword().equals(password)){
                vo.setPassword(null);
                String token = SHA1Util.getSha1(vo.getUserName()+password+uuid);
                vo.setToken(token);
                stringRedisTemplate.opsForValue().set("userName"+vo.getId(),vo.getUserName());
                stringRedisTemplate.opsForValue().set(String.valueOf(vo.getId()),token);
                stringRedisTemplate.opsForValue().set("uuid"+vo.getId(),uuid);
                stringRedisTemplate.opsForValue().set("areaCode"+vo.getId(),vo.getAreaCode());
                stringRedisTemplate.opsForValue().set("level"+vo.getId(),vo.getLevel());
                //保存日志
                TabLoginLog loginLog = new TabLoginLog();
                loginLog.setUsername(userName);
                loginLog.setIp(uuid);
                loginLog.setLoginDate(new Date());
                loginLog.setPlatType(2l);
                appTabLoginLogService.insert(loginLog);
                return entityResult(vo,"登录成功");
           }else {
                return entityResult(null,"密码错误");
          }
        }else {
            return entityResult(null,"用户名错误");
        }
    }
}
