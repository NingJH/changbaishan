package com.cqndt.disaster.device;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableScheduling
public class AppApplication {
    //124 端口：19091
    public static void main(String[] args) {
        SpringApplication.run(AppApplication.class, args);
    }
}
