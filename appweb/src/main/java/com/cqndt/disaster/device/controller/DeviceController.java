package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.AppTabDeviceService;
import com.cqndt.disaster.device.util.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v2/device")
@Api(value = "设备管理", description = "设备管理")
@Slf4j
public class DeviceController extends BaseController {
    @Autowired
    private AppTabDeviceService appTabDeviceService;

    @PostMapping("selectOnlineDevice")
    @ApiOperation(value = "所有设备在线率", notes = "所有设备在线率")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "用户id", defaultValue = "1", required = true, paramType = "query")
    })
    //public AjaxResult OnlineRate() {
    public AjaxResult OnlineRate(HttpServletRequest request) {
        String userId = request.getHeader("id");
        //String userId = "2";
        //设备总数
        Integer countDevice = appTabDeviceService.selectCountDevice(userId);
        //设备在线数量
        Integer onlineCountDevice = appTabDeviceService.selectOnlineCountDevice(userId);
        //设备离线数量
        Integer offLineCountDevice = countDevice-onlineCountDevice;
        double q = 0d;
        try {
            q = new BigDecimal((float) onlineCountDevice / countDevice).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        } catch (ArithmeticException e) {
            System.out.println("除数不能为0");
        }
        String OnlineRate = String.valueOf(q * 100);
        Map<String,String> map = new HashMap<>(4);
        map.put("onlineRate",String.valueOf(OnlineRate));
        map.put("countDevice",String.valueOf(countDevice));
        map.put("onlineCountDevice",String.valueOf(onlineCountDevice));
        map.put("offLineCountDevice",String.valueOf(offLineCountDevice));
        return entityResult(map, "获取成功");
    }

    @PostMapping("selectOnlineRateByProject")
    @ApiOperation(value = "查询每个项目所有设备在线情况", notes = "查询每个项目所有设备在线情况")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "seachText", value = "项目名称", defaultValue = "1", required = true, paramType = "query")
    })
    //public AjaxResult selectOnlineRateByProject(String seachText) {
    public AjaxResult selectOnlineRateByProject(HttpServletRequest request,String seachText) {
       String userId = request.getHeader("id");
        //String userId = "2";
        List<Map<String, Object>> mapList = appTabDeviceService.selectOnlineRateByProject(userId,seachText);
        for (Map<String, Object> map : mapList) {
            int totalNum = Integer.parseInt(map.get("totalNum").toString());
            int zx = Integer.parseInt(map.get("zx").toString());
            double q = 0d;
            try {
                if(totalNum !=0){
                    q = new BigDecimal((float) zx / totalNum).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
                }
            } catch (ArithmeticException e) {
                System.out.println("除数不能为0");
            }
            String OnlineRate = String.valueOf(q * 100);
            map.put("OnlineRate", OnlineRate);
        }

        return entityResult(mapList, "获取成功");
    }

    @PostMapping("selectOnlineDeviceByProjectID")
    @ApiOperation(value = "查询某个项目下的设备情况及负责人", notes = "查询某个项目下的设备情况及负责人")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "ProjectID", value = "项目id", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult selectOnlineDeviceByProjectID(Integer ProjectID) {
        Map<String, Object> map = new HashMap<>();
        List<Map<String, Object>> mapList = appTabDeviceService.selectOnlineDeviceByProjectID(ProjectID);
        List<Map<String, Object>> person = appTabDeviceService.selectPerson(ProjectID);
        log.info("查询的人"+person);
        if (!person.isEmpty()){
            map.put("person", person.get(0));
        }else {
            map.put("person", null);
        }
        map.put("ListDevice", mapList);
        return entityResult(map, "获取");
    }

    @PostMapping("deviceCoordinateAll")
    @ApiOperation(value = "根据设备id查询所在项目的监测点", notes = "根据设备id查询所在项目的监测点")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "deviceId", value = "设备id", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult deviceCoordinateAll(Integer deviceId) {
        Map<String, Object> deviceInfo = appTabDeviceService.selectDeviceById(deviceId);
        if (deviceInfo.containsKey("id")) {
            int id = (int) deviceInfo.get("id");
            return entityResult(appTabDeviceService.deviceCoordinateByProjectId(id), "获取成功");
        }else {
            return fallback();
        }
    }

    @PostMapping("selectDeviceById")
    @ApiOperation(value = "根据设备id查询设备信息", notes = "根据设备id查询设备信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "deviceId", value = "设备id", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "sensorType", value = "传感器类型", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "sensorNo", value = "传感器编号", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult selectDeviceById(Integer deviceId,String sensorType,String sensorNo) {
        Map<String, Object> map = new HashMap<String, Object>();
        Double maxData = null;
        Double minData = null;
        Map<String, Object> deviceInfo = appTabDeviceService.selectDeviceById(deviceId);
        map.put("deviceInfo", deviceInfo);
        if (null != deviceInfo) {
            List<Map<String, Object>> mapResult = appTabDeviceService.monitorStatistics(sensorType,"week",sensorNo);
            for (Map<String, Object> maps:mapResult) {
                Double max = new Double(maps.get("maxData").toString());
                Double min = new Double(maps.get("minData").toString());
                maxData = maxData == null? max: maxData>max?maxData:max;
                minData = minData == null? min: minData>min?min:minData;
            }
            map.put("monitor", mapResult);
            map.put("maxTotalData",maxData.toString());
            map.put("minTotalData",minData.toString());
        }
        return entityResult(map, "获取成功");
    }

}
