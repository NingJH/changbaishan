package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.AppTabPhotographyService;
import com.cqndt.disaster.device.app.AppTabTwoRoundService;
import com.cqndt.disaster.device.common.service.TabPhotographyService;
import com.cqndt.disaster.device.common.service.TabTwoRoundService;
import com.cqndt.disaster.device.util.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v2")
@Api(value = "项目实景图", description = "项目实景图")
@Slf4j
public class ProjectPanoramaController extends BaseController{

    @Autowired
    private AppTabTwoRoundService tabTwoRoundService;
    @Autowired
    private AppTabPhotographyService tabPhotographyService;

    /**
     * 获取项目实景图
     * @param type 倾斜摄影 - 1  720全景-2
     * @param projectId 项目id
     * @return
     */
    @ApiOperation(value = "获取项目实景图", notes = "监测项目详情")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "type", value = "倾斜摄影 - 1  720全景-2", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "1", required = true, paramType = "query")
    })
    @GetMapping(value = "/panorama")
    public AjaxResult getProjectPanorama(Integer type,Integer projectId){
        if(type == 1){
            return entityResult(tabPhotographyService.selectByProjectId(projectId,"2"),"获取倾斜摄影");
        }else{
            return entityResult(tabTwoRoundService.selectByProjectId(projectId,"2"),"获取全景图");
        }

    }

}
