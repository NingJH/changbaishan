package com.cqndt.disaster.device.util;

import com.cqndt.disaster.device.common.util.ErrorType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created By marc
 * Date: 2019/5/10  16:12
 * Description:
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
public class ServiceException extends RuntimeException {

    private int errCode;

    private String errMsg;

    public ServiceException(ErrorType errorType){
        this.errCode = errorType.getCode();
        this.errMsg = errorType.getErrorMsg();
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
