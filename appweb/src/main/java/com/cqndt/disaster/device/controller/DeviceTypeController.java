package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.AppTabDeviceService;
import com.cqndt.disaster.device.util.AjaxResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v2/deviceType")
@Api(value = "设备类型及数据曲线管理", description = "设备类型及数据曲线管理")
@Slf4j
public class DeviceTypeController extends BaseController{

    @Autowired
    private AppTabDeviceService appTabDeviceService;

    @RequestMapping(value="/statistics",method = RequestMethod.POST)
    @ApiOperation(value = "设备类型统计", notes = "设备类型统计")
    //public AjaxResult deviceTypeStatistics(){
    public AjaxResult deviceTypeStatistics(HttpServletRequest request){
        String userId = request.getHeader("id");
       // String userId = "2";
        List<Map<String,Object>> list = appTabDeviceService.selectDeviceTypeStatistics(userId);
        Integer num = appTabDeviceService.selectDeviceTypeStatisticsTotal(userId);
        Map<String,Object> map = new HashMap<>();
        map.put("totalCount",num);
        map.put("mapList",list);
        return entityResult(map, "获取成功");
    }

    @RequestMapping(value="/projectByDeviceType",method = RequestMethod.POST)
    @ApiOperation(value = "根据设备类型项目统计", notes = "根据设备类型项目统计")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceTypeId", value = "设备类型", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectName", value = "项目名称", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "设备类型", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "项目名称", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult projectByDeviceType(HttpServletRequest request,String deviceTypeId,String projectName,Integer page,Integer limit){
        String userId = request.getHeader("id");
        PageHelper.startPage(page, limit);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(appTabDeviceService.selectProjectByDeviceType(userId,deviceTypeId,projectName));
        //String userId = "2";
        Integer num = (int)pageInfo.getTotal();
        return pageResult(limit, page, num, pageInfo.getList());
    }

    @RequestMapping(value="/projectByDeviceTypeAndId",method = RequestMethod.POST)
    @ApiOperation(value = "根据设备类型和projectId查看监测点", notes = "根据设备类型和projectId查看监测点")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceTypeId", value = "设备类型", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "projectId", value = "项目id", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult projectByDeviceTypeAndId(String deviceTypeId,String projectId,Integer page,Integer limit){
        page = null==page?1:page;
        limit = null==limit?10:limit;
        PageHelper.startPage(null==page?1:page, null==limit?10:limit);
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<Map<String, Object>>(appTabDeviceService.projectByDeviceTypeAndId(deviceTypeId,projectId));
        return pageResult(limit, page, (int)pageInfo.getTotal(), pageInfo.getList());
    }

    @RequestMapping(value="/getSensorByDeviceId",method = RequestMethod.POST)
    @ApiOperation(value = "获取设备对应的传感器", notes = "获取设备对应的传感器")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "deviceId", value = "设备id", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult getSensorByDeviceId(String deviceId){
        return entityResult(appTabDeviceService.listTabSensor(deviceId), "获取成功");
    }


    @RequestMapping(value="/monitorStatistics",method = RequestMethod.POST)
    @ApiOperation(value = "数据曲线", notes = "数据曲线")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "sensorType", value = "传感器类型", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "type", value = "今天:today, 三天：threeDay, 本周:week, 本月:month, 近三个月:3month", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "sensorNo", value = "传感器编号", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult monitorStatistics(String sensorType,String type,String sensorNo){
        AjaxResult result = new AjaxResult();
        BigDecimal maxData = null;
        BigDecimal minData = null;
        List<Map<String, Object>> mapResult = appTabDeviceService.monitorStatistics(sensorType, type, sensorNo);
        for (Map<String, Object> map:mapResult) {
            BigDecimal max = new BigDecimal(map.get("maxData").toString());
            BigDecimal min = new BigDecimal(map.get("minData").toString());
            maxData = maxData == null? max: maxData.max(max);
            minData = minData == null? min: minData.min(min);
        }
        result.setData(mapResult);
        result.setMaxTotalData(maxData);
        result.setMinTotalData(minData);
        result.setOk(true);
        result.setCode(200);
        return result;
    }
}
