package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.TabAndroidVersionService;
import com.cqndt.disaster.device.util.AjaxResult;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "版本管理", description = "版本管理")
@RequestMapping("/api/v2/android")
public class VersionUpdateController extends BaseController{
    private static Logger logger = LoggerFactory.getLogger(VersionUpdateController.class);
    @Autowired
    private TabAndroidVersionService versionUpdateService;

//    @Value("${android-apk}")
//    private String apkPath;

    @ResponseBody
    @GetMapping("/version/{versionNumber}")
    public AjaxResult findNewVersionNumber(@PathVariable("versionNumber") Integer versionNumber) {
        try {
            List<Map<String, Object>> list = versionUpdateService.findNewVersion();
            if (list != null && Integer.valueOf(list.get(0).get("versionCode").toString())> versionNumber) {
                return success(list,"有新版本");//有新版本
            } else {
                return error("无新版本");//无新版本
            }

        } catch (Exception e) {
            return error("查找新版本失败！");
        }
    }
//    @ResponseBody
//    @RequestMapping(value = "/down", method = RequestMethod.GET)
//    public void down(HttpServletRequest request, HttpServletResponse response) throws Exception {
//
//        String path = apkPath;
//        List<Map<String, Object>> list1 = versionUpdateService.findNewVersion();
//
//        String filename = list1.get(0).get("url").toString();
//        File newdir = new File(path);
//        if (!newdir.exists()) {
//            newdir.mkdirs();
//        }
//        path += File.separator + filename;
//
//        File file = new File(path);
//        if (!file.exists()) {
//            System.out.println("文件不存在");
//            logger.error("文件不存在");
//            return ;
//        }
//
//        //获取输入流
//        @SuppressWarnings("resource")
//        InputStream bis = new BufferedInputStream(new FileInputStream(new File(path)));
//
//        //转码,免得文件名中文乱码
//        filename = URLEncoder.encode(filename, "UTF-8");
//        //设置文件下载头
//        response.addHeader("Content-Disposition", "attachment;filename=" + filename);
//        //1.设置文件ContentType类型,这样设置,会自动判断下载文件类型
//        response.setContentType("multipart/form-data");
//        BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
//        byte[] data = new byte[1024];
//        int len = 0;
//        while ((len = bis.read(data)) != -1) {
//            out.write(data, 0, len);
//            out.flush();
//        }
//        out.close();
//    }

}
