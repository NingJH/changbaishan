package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.AppTabProjectService;
import com.cqndt.disaster.device.project.service.TabProjectService;
import com.cqndt.disaster.device.util.AjaxResult;
import com.cqndt.disaster.device.vo.TabProjectVo;
import com.cqndt.disaster.device.vo.TabUserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(value = "/api/v2/project")
@Api(value = "项目管理", description = "项目管理")
public class ProjectInfoController extends BaseController {

	@Resource
	private StringRedisTemplate stringRedisTemplate;

	@Autowired
	private AppTabProjectService tabProjectService;

    /**
     * 查询监测项目列表
     */
    @PostMapping(value = "/selectAll")
	@ApiOperation(value = "查询监测项目列表", notes = "查询监测项目列表")
	@ApiImplicitParams({
			@ApiImplicitParam(dataType = "String", name = "search", value = "查询条件(项目名称或项目地址)", defaultValue = "", required = true, paramType = "query"),
			@ApiImplicitParam(dataType = "String", name = "id", value = "灾害类型", defaultValue = "", required = true, paramType = "query")
	})
    public AjaxResult selectProjectAll(HttpServletRequest request, String search, String id){
    		Object data=null;
    		try {
    			String userId = request.getHeader("id");
				String areaCode= stringRedisTemplate.opsForValue().get("areaCode"+userId);
				String unitLevel=stringRedisTemplate.opsForValue().get("level"+userId);
				data = tabProjectService.selectByCondition(userId,unitLevel,areaCode,id,search);
			} catch (Exception e) {
				e.printStackTrace();
			}
    	return entityResult(data,"获取项目列表");
    }
    
    /**
     * 查询监测项目置顶的
     */
    @PostMapping(value = "/selectAllPush")
    public AjaxResult selectProjectAllPush(HttpServletRequest request){
    	Object data = null;
		try {
				String userId = request.getHeader("id");
				data = tabProjectService.selectAllPush(userId);
			} catch (Exception e) {
				System.out.println("监测项目置顶查询失败！");
				e.printStackTrace();
    	}
    	return entityResult(data,"获取项目置顶列表");
    }
    
    /**
     * 查询监测项目详情
     */
    @PostMapping(value = "/selectByIdDetails")
	@ApiOperation(value = "监测项目详情", notes = "监测项目详情")
	@ApiImplicitParams({
			@ApiImplicitParam(dataType = "Integer", name = "projectId", value = "项目id", defaultValue = "1", required = true, paramType = "query")
	})
    public AjaxResult selectByIdDetails(HttpServletRequest request,Integer projectId){
    		Object data = null;
			try {
				String userId = request.getHeader("id");
				data = tabProjectService.selectByIdDetails(projectId,userId);
			} catch (Exception e) {
				System.out.println("项目详情查询失败！");
				e.printStackTrace();
			}
    	return entityResult(data,"项目详情查询");
    }

}
