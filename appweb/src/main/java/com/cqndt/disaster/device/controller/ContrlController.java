package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.common.exception.ContrlException;
import com.cqndt.disaster.device.common.util.Constant;
import com.cqndt.disaster.device.common.util.ContrlUtil;
import com.cqndt.disaster.device.common.util.NumberChangeUtil;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.domain.TabOrderLog;
import com.cqndt.disaster.device.equip.service.ContrlService;
import com.cqndt.disaster.device.equip.service.TabOrderLogService;
import com.cqndt.disaster.device.vo.TabOrderLogVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


/**
 * 智能报警器指令
 */
@RestController
@RequestMapping("/api/v2/contrl")
@Api(value = "智能报警器指令", description = "智能报警器指令")
public class ContrlController {
    @Autowired
    private TabOrderLogService tabOrderLogService;

    @Autowired
    private ContrlService contrlService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @PostMapping("order")
    @ApiOperation(value = "智能报警器指令", notes = "智能报警器指令")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "platDeviceId", value = "平台设备id", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "sn", value = "设备sn", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "voiceType", value = "声音类型", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "voiceTypeName", value = "声音类型名称", defaultValue = "1", required = true, paramType = "query")
    })
    public Result order(HttpServletRequest request,String platDeviceId, String sn, String voiceType, String voiceTypeName) {
        String userId = request.getHeader("id");
        try {
            //保存发送日志
            TabOrderLog log = new TabOrderLog();
            log.setOperation("手机发送智能报警指令");
            log.setVoiceTypeName(voiceTypeName);
            log.setPlatType(1);
            log.setSn(sn);
            log.setVoiceType(Integer.valueOf(voiceType));
            log.setDeviceId(platDeviceId);
            log.setCreateDate(new Date());
            log.setUsername(redisTemplate.opsForValue().get("userName"+userId));
            log.setIp(redisTemplate.opsForValue().get("uuid"+userId));
            log.setTime(1l);
            tabOrderLogService.save(log);
            //申请变量
            String setShoudLight =  ContrlUtil.setAlarm(sn,Integer.parseInt(voiceType));//声光告警
            //发送请求onenet
            contrlService.postCommand(platDeviceId, Base64.encodeBase64String(NumberChangeUtil.stringToByte(setShoudLight)));
        } catch (ContrlException e) {
            return new Result().failure(-1,platDeviceId + e.getMessage());
        }
        //响应
        return new Result().success("发送成功");
    }
    @PostMapping("listOrderLog")
    @ApiOperation(value = "已发送指令列表", notes = "已发送指令列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceId", value = "平台设备id", defaultValue = "1", required = true, paramType = "query")
    })
    public Result listOrderLog(TabOrderLogVo vo) {
        PageHelper.startPage(vo.getPage(),vo.getLimit());
        PageInfo<TabOrderLogVo> pageInfo = new PageInfo<TabOrderLogVo>(tabOrderLogService.listOrderLog(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }
}
