package com.cqndt.disaster.device.util;

import com.cqndt.disaster.device.common.util.ErrorInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * user: zhaojianji
 * date: 2017/05/19
 * desc:  描述用途
 */
@Data
//@ApiModel
public class AjaxResult {
   // @ApiModelProperty(value = "成功与失败判定标志",name = "成功与失败判定标志")
    private boolean ok;//成功与失败判定标志
   // @ApiModelProperty(value = "状态代码 约定同http状态码 200 -> ok",name = "状态代码 约定同http状态码 200 -> ok")
    private  int code ; //状态代码 约定同http状态码 200 -> ok
   // @ApiModelProperty(value = "附加消息 供前端控制台输出 用于排查",name = "附加消息 供前端控制台输出 用于排查")
    private  String  msg ;//附加消息 供前端控制台输出 用于排查
   // @ApiModelProperty(value = "具体数据",name = "具体数据")
    private Object data; // 具体数据
    private boolean page; // 是否分页
    private int curPage; // 当前页码
    private int totalRecord; // 总记录数
    private int totalPage; // 总页数
    private int curRecord; //当前返回数 前端可直接使用进行遍历
    private int beginRow; // 起始索引 可不填 前端如果可以更改每页记录数才需要
    private int endRow; // 结束索引 可不填 前端如果可以更改每页记录数才需要
    private int prePageSize; // 每页记录数 可不填 前端如果可以更改每页记录数才需要
    private BigDecimal maxTotalData;//数据曲线查询最大值
    private BigDecimal minTotalData;//数据曲线查询最小值
    private List<ErrorInfo> errors;

    public AjaxResult() {
    }

    public AjaxResult(boolean ok, int code, String msg, Object data) {
        this.ok = ok;
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public AjaxResult(boolean ok, int code, String msg) {
        this.ok = ok;
        this.code = code;
        this.msg = msg;
    }

    public AjaxResult(boolean ok, String msg) {
        this.ok = ok;
        this.msg = msg;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isPage() {
        return page;
    }

    public void setPage(boolean page) {
        this.page = page;
    }

    public int getCurPage() {
        return curPage;
    }

    public void setCurPage(int curPage) {
        this.curPage = curPage;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurRecord() {
        return curRecord;
    }

    public void setCurRecord(int curRecord) {
        this.curRecord = curRecord;
    }

    public int getBeginRow() {
        return beginRow;
    }

    public void setBeginRow(int beginRow) {
        this.beginRow = beginRow;
    }

    public int getEndRow() {
        return endRow;
    }

    public void setEndRow(int endRow) {
        this.endRow = endRow;
    }

    public int getPrePageSize() {
        return prePageSize;
    }

    public void setPrePageSize(int prePageSize) {
        this.prePageSize = prePageSize;
    }

    public List<ErrorInfo> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorInfo> errors) {
        this.errors = errors;
    }
}

