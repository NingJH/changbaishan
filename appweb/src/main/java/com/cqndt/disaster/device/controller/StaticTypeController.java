package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.AppTabStaticTypeService;
import com.cqndt.disaster.device.common.service.TabStaticTypeService;
import com.cqndt.disaster.device.domain.TabStaticType;
import com.cqndt.disaster.device.util.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(value = "静态值获取", description = "静态值获取")
@RequestMapping(value = "/api/v2")
public class StaticTypeController extends BaseController{

    @Autowired
    private AppTabStaticTypeService tabStaticTypeService;

    @RequestMapping(value = "/static/type",method = RequestMethod.GET)
    public AjaxResult getAllReportStaticType(){
        return entityResult(tabStaticTypeService.selectByStaticNum("18"),"获取报表类型");
    }

    /**
     * 获取类型列表
     * @return
     */
    @RequestMapping(value = "/type",method = RequestMethod.GET)
    public AjaxResult getAllType(){
        return entityResult(tabStaticTypeService.selectByStaticNum("4"),"获取所有类型");
    }

    /**
     * 获取监测类型列表
     * @return
     */
    @RequestMapping(value = "/getMonitorAllType")
    public AjaxResult getMonitorAllType(){
        return entityResult(tabStaticTypeService.selectByStaticNum("13"),"获取所有监测方法类型");
    }


//    @RequestMapping(value = "/static/type",method = RequestMethod.GET)
//    @ApiOperation(value = "获取静态值", notes = "获取静态值")
//    @ApiImplicitParams({
//            @ApiImplicitParam(dataType = "String", name = "staticNum", value = "静态值类型值(报表类型 staticNum=18,灾害类型 staticNum=4,监测类型 staticNum=1)", defaultValue = "1", required = true, paramType = "query")
//    })
//    public AjaxResult getAllReportStaticType(@PathVariable("staticNum") String staticNum){
//        return entityResult(tabStaticTypeService.selectByStaticNum(staticNum),"获取报表类型");
//    }

}
