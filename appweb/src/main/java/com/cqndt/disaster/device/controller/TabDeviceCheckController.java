package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.AppTabDeviceCheckService;
import com.cqndt.disaster.device.domain.TabDeviceCheck;
import com.cqndt.disaster.device.util.AjaxResult;
import com.cqndt.disaster.device.vo.TabDeviceCheckVo;
import com.cqndt.disaster.device.vo.TabDeviceVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v2/tabDeviceCheck")
@Api(description = "巡查记录", value = "巡查记录")
public class TabDeviceCheckController extends BaseController{

    @Autowired
    private AppTabDeviceCheckService appTabDeviceCheckService;

    @Value("${file-ip}")
    private String fileIp;

    @RequestMapping(value = "listTabDevice",method = RequestMethod.GET)
    @ApiOperation(value = "根据项目id查询设备列表", notes = "根据项目id查询设备列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "projectNo", value = "项目id",  required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceName", value = "设备名称",  paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "1", required = true, paramType = "query")
    })
    //@RequiresPermissions("sys:tabdevice:list")
    public AjaxResult listTabDevice(String projectNo,Integer page,Integer limit,String deviceName) {
        PageHelper.startPage(page,limit);
        List<Map<String,String>> list = appTabDeviceCheckService.listForDevice(projectNo,deviceName);
        PageInfo<Map<String,String>> pageInfo = new PageInfo<>(list);
        Integer num = (int)pageInfo.getTotal();
        return pageResult(limit, page, num, pageInfo.getList());
    }

    @PostMapping(value = "uploadDeviceCheckImg")
    @ApiOperation(value = "上传巡查图片", notes = "上传巡查图片")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "MultipartFile", name = "files", value = "文件流", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult uploadDeviceCheckImg(MultipartFile[] files){
        StringBuffer sb = new StringBuffer();
        for(MultipartFile file:files){
            sb.append(appTabDeviceCheckService.uploadDeviceCheckImg(file)).append(",");
        }
        System.out.println(sb);
        Map<String,String> map = new HashMap<>();
        map.put("type","img");
        map.put("relativePath",sb.substring(0,sb.length()-1));
        map.put("absolutePath",fileIp);
        return entityResult(map,"巡查上传图片");
    }

    @PostMapping(value = "uploadDeviceCheckVideo")
    @ApiOperation(value = "上传巡查视频", notes = "上传巡查视频")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "MultipartFile", name = "files", value = "文件流", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult uploadDeviceCheckVideo(MultipartFile[] files){
        StringBuffer sb = new StringBuffer();
        StringBuffer sb2 = new StringBuffer();
        for(MultipartFile file:files){
            String[] str = appTabDeviceCheckService.uploadDeviceCheckVideo(file);
            sb.append(str[0]).append(",");
            sb2.append(str[1]).append(",");
        }
        System.out.println(sb2);
        Map<String,String> map = new HashMap<>();
        map.put("type","video");
        map.put("relativePath",sb.substring(0,sb.length()-1));
        map.put("videoImgPath",sb2.substring(0,sb2.length()-1));
        map.put("absolutePath",fileIp);
        return entityResult(map,"巡查上传视频");
    }

    @PostMapping(value = "saveTabDeviceCheck")
    @ApiOperation(value = "新增巡查记录",notes = "新增巡查记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceNo", value="设备编号", dataType = "String",required = true,paramType="query"),
            @ApiImplicitParam(name = "checkDate", value="核实日期", dataType = "Date",paramType="query"),
            @ApiImplicitParam(name = "imgIds", value="现场图片", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "videoIds", value="视频", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "videoImgIds", value="视频截图", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "adviseValue", value="建议阈值", dataType = "BigDecimal",paramType="query"),
            @ApiImplicitParam(name = "deviceState", value="设备状态(1.正常/0.异常)", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "deviceAppearance", value="设备外观(1.完好 2.损毁 3.局部损毁)", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "reason", value="设备现状原因", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "batteryState", value="蓄电池状态", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "faultReason", value="故障原因", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "laterIsRecover", value="后期能否恢复(0、否 1、是)", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "recoverAdvise", value="恢复建议", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "guardName", value="看管人姓名", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "guardPhone", value="看管人电话", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "managerName", value="主管人姓名", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "managerPhone", value="主管人电话", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "sbzlypg", value="设备质量预评", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "buildTime", value="建设年度", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "buildUnit", value="建设厂家", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "checkPerson", value="核实人", dataType = "String",paramType="query"),
            @ApiImplicitParam(name = "remark", value="备注", dataType = "String",paramType="query")
    })
    public AjaxResult saveTabDeviceCheck(TabDeviceCheck tabDeviceCheck){
        return entityResult(appTabDeviceCheckService.save(tabDeviceCheck),"新增巡查记录");
    }

    @PostMapping(value = "listTabDeviceCheck")
    @ApiOperation(value = "巡查记录列表",notes = "巡查记录列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceNo", value="设备编号", dataType = "String",required = true,paramType="query"),
            @ApiImplicitParam(name = "startDate", value="开始日期", dataType = "Date",paramType="query"),
            @ApiImplicitParam(name = "endDate", value="结束日期", dataType = "Date",paramType="query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "1", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult listTabDeviceCheck(TabDeviceCheckVo tabDeviceCheckVo){
        PageHelper.startPage(tabDeviceCheckVo.getPage(),tabDeviceCheckVo.getLimit());
        List<TabDeviceCheckVo> list = appTabDeviceCheckService.listForDeviceCheck(tabDeviceCheckVo);
        PageInfo<TabDeviceCheckVo> pageInfo = new PageInfo<>(list);
        Integer num = (int)pageInfo.getTotal();
        return pageResult(tabDeviceCheckVo.getLimit(), tabDeviceCheckVo.getPage(), num, pageInfo.getList());
    }

    @PostMapping(value = "downloadDeviceCheck")
    @ApiOperation(value = "下载巡查记录",notes = "下载巡查记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deviceNo", value="设备编号", dataType = "String",required = true,paramType="query"),
            @ApiImplicitParam(name = "startDate", value="开始日期", dataType = "Date",paramType="query"),
            @ApiImplicitParam(name = "endDate", value="结束日期", dataType = "Date",paramType="query"),
    })
    public AjaxResult downloadDeviceCheck(TabDeviceCheckVo tabDeviceCheckVo){
        return entityResult(appTabDeviceCheckService.downloadDeviceCheck(tabDeviceCheckVo),"下载巡查记录");
    }
}
