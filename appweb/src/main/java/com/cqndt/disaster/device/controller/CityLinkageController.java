package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.AppTabAreaService;
import com.cqndt.disaster.device.app.AppTabProjectService;
import com.cqndt.disaster.device.util.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@RestController
@RequestMapping(value = "/api/v2")
@Api(value = "区域查询", description = "区域查询")
public class CityLinkageController extends BaseController{

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private AppTabAreaService appTabAreaService;

    @RequestMapping(value = "/city",method = RequestMethod.GET)
    @ApiOperation(value = "获取区域", notes = "获取区域")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "id", value = "用户id", defaultValue = "1", required = true, paramType = "query")
    })
    public AjaxResult cityLinkage(HttpServletRequest request){
        String userId = request.getHeader("id");
        String areaCode= stringRedisTemplate.opsForValue().get("areaCode"+userId);
        String level=stringRedisTemplate.opsForValue().get("level"+userId);
        return entityResult(appTabAreaService.cityLinkage(areaCode,level),"获取城市联动");
    }


}
