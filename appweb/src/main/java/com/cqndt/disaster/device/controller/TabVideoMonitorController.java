package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.equip.service.TabVideoMonitoringService;
import com.cqndt.disaster.device.vo.TabVideoMonitoringVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhangliang
 * @Date 2019/5/15 15:12
 */
@RestController
@Api(value = "视频监控", description = "视频监控")
@RequestMapping(value = "/api/v2/videoMonitor")
public class TabVideoMonitorController extends BaseController{

    @Autowired
    private TabVideoMonitoringService tabVideoMonitoringService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    private List<Map<String,Object>> resultList=new ArrayList<>();


    @PostMapping("getVideoMonitorMsgByAreacode")
    @ApiOperation(value = "设备列表", notes = "设备列表")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "deviceName", value = "设备名称", defaultValue = "", required = false, paramType = "query")
    })
    public Result getVideoMonitorMsgByAreacode(HttpServletRequest request, Integer page, Integer limit, String deviceName){
        String userId = request.getHeader("id");
        String areaCode= redisTemplate.opsForValue().get("areaCode"+userId);
        String unitLevel=redisTemplate.opsForValue().get("level"+userId);
        TabVideoMonitoringVo vo = new TabVideoMonitoringVo();
        vo.setUserId(Integer.parseInt(userId));
        vo.setDeviceName(deviceName);
        vo.setAreaCode(areaCode);
        vo.setAreaCodeSplit(areaCode.substring(0,2));
        vo.setLevel(unitLevel);
        PageHelper.startPage(page,limit);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<Map<String,Object>>(tabVideoMonitoringService.getVideoMonitorMsgByAreacode(vo));
        Result result = new Result();
        result.setCount((int)pageInfo.getTotal());
        result.setData(pageInfo.getList());
        return result;
    }

}
