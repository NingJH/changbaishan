package com.cqndt.disaster.device.controller;

import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

@Controller
@RequestMapping("/api/v2/upload")
@Api(value = "图片以流的形式读取", description = "图片以流的形式读取")
public class UploadImgController {

    @PostMapping("/readImg")
    public void downLoadImg(HttpServletResponse response, String path) {
        // 以流的形式下载文件。
        try {
            BufferedInputStream fis = new BufferedInputStream(new FileInputStream(path));
            byte[] buffer = new byte[fis.available()];
            fis.read(buffer);
            fis.close();
            // 清空response
            response.reset();
            OutputStream os = new BufferedOutputStream(response.getOutputStream());
            response.setContentType("application/octet-stream");
            //response.setHeader("Content-Disposition", "attachment;filename=" + new String(name.getBytes("UTF-8"),"ISO-8859-1")+".zip");
            os.write(buffer);
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
