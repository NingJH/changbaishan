package com.cqndt.disaster.device.controller;

import com.cqndt.disaster.device.app.AppWarnStatisticsService;
import com.cqndt.disaster.device.common.util.Result;
import com.cqndt.disaster.device.util.AjaxResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(value = "预警统计", description = "预警统计")
@RequestMapping(value = "/api/v2")
public class WarnStatisticsController extends BaseController{
    Logger logger=LoggerFactory.getLogger(WarnStatisticsController.class  ) ;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private AppWarnStatisticsService warnStatisticsService;

    @PostMapping("/warnStatics")
    @ApiOperation(value = "查询各预警等级的占比", notes = "查询各预警等级的占比")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "type", value = "1:当天、2：本周、3：当月", defaultValue = "", required = true, paramType = "query")
    })
    public AjaxResult warnStatictis(HttpServletRequest request, String areaCode, String level, String type){
        Map<String,Object> map=new HashMap<String, Object>();
        String userId = request.getHeader( "id" );
        map.put("areaCode",null == areaCode?stringRedisTemplate.opsForValue().get("areaCode" +userId):areaCode);
        map.put("areaLevel",null == level?stringRedisTemplate.opsForValue().get("level" +userId):level);
        map.put( "type",type );
        map.put( "userId",userId );
        return entityResult(warnStatisticsService.findWarnStatictics(map),"获取各预警等级的占比");
    }

    @PostMapping("/findWarnMsg")
    @ApiOperation(value = "查询预警具体信息", notes = "查询预警具体信息")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "type", value = "1:当天、2：本周、3：当月", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "warn_type", value = "告警类型", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "warn_level", value = "告警等级", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "page", value = "页数", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "Integer", name = "limit", value = "条数", defaultValue = "", required = true, paramType = "query")
    })
    public Result findWarnMsg(String areaCode, String level, String type, Integer page, Integer limit,
                              HttpServletRequest request, String warn_type, String warn_level){
        Map<String,Object> map=new HashMap<String, Object>();
        String userId = request.getHeader( "id" );
        map.put("areaCode",null == areaCode?stringRedisTemplate.opsForValue().get("areaCode" +userId):areaCode);
        map.put("areaLevel",null == level?stringRedisTemplate.opsForValue().get("level" +userId):level);
        map.put("userId",userId);
        map.put("type",type);
        map.put("warn_type",warn_type);
        map.put("warn_level",warn_level);
        PageHelper.startPage(page,limit);
        PageInfo<Map<String,Object>> pageInfo = new PageInfo<Map<String,Object>>(warnStatisticsService.findWarnMsg(map));
        Result result = new Result();
        result.setCode(200);
        result.setData(pageInfo.getList());
        return result;
    }

    @PostMapping("/findWarnCount")
    @ApiOperation(value = "查询各告警数和总数", notes = "查询各告警数和总数")
    @ApiImplicitParams({
            @ApiImplicitParam(dataType = "String", name = "areaCode", value = "区域编码", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "level", value = "区域等级", defaultValue = "", required = true, paramType = "query"),
            @ApiImplicitParam(dataType = "String", name = "type", value = "1:当天、2：本周、3：当月", defaultValue = "", required = true, paramType = "query")
    })
    public AjaxResult findWarnCount(String areaCode, String level, String type, HttpSession session, HttpServletRequest request){
        Map<String,Object> map=new HashMap<String, Object>();
        String userId = request.getHeader( "id" );
        map.put("areaCode",null == areaCode?stringRedisTemplate.opsForValue().get("areaCode" +userId):areaCode);
        map.put("areaLevel",null == level?stringRedisTemplate.opsForValue().get("level" +userId):level);
        map.put("userId",userId);
        map.put("type",type);
        return entityResult(warnStatisticsService.findWarnCount(map),"查询预警具体信息");
    }
}
